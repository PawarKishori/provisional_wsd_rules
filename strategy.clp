;@@@ Added by 14anu-ban-01 on (17-10-2014)
;A good strategy is to focus first on the essential features, discover the basic principles and then introduce corrections to build a more refined theory of the phenomenon.[NCERT Corpus]
;एक अच्छी युक्ति वही है कि पहले किसी परिघटना के परमावश्यक लक्षणों पर ध्यान केंद्रित करके उसके मूल सिद्धान्तों को खोजा जाए और फिर संशुद्धियों को सन्निविष्ट करके उस परिघटना के सिद्धान्तों को और अधिक परिशुद्ध बनाया जाए.[NCERT Corpus]
(defrule strategy0
(declare (salience 1))
(id-root ?id strategy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yukwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strategy.clp      strategy0   "  ?id "  yukwi)" crlf)
))

;@@@ Added by 14anu-ban-01 on (17-10-2014)
;It is a defence strategy.[self:with reference to COCA]
;यह एक   सुरक्षा/प्रतिरक्षा रणनीति है.[self]
(defrule strategy1
(declare (salience 1000))
(id-root ?id strategy)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 defence)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raNanIwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strategy.clp      strategy1   "  ?id "  raNanIwi)" crlf)
))

;@@@ Added by 14anu-ban-01 on (17-10-2014)
;It is a successful military strategy.[self:with reference to COCA]
;यह एक  सफल सैन्य रणनीति है.[self]
(defrule strategy2
(declare (salience 1000))
(id-root ?id strategy)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 military)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raNanIwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strategy.clp      strategy2  "  ?id "  raNanIwi)" crlf)
))


;@@@ Added by 14anu-ban-01 on (17-10-2014)
;We are improving our marketing strategy.[self:with reference to oald]
;हम अपनी क्रय-विक्रय कार्यनीति में सुधार कर रहे हैं.[self]
(defrule strategy3
(declare (salience 1000))
(id-root ?id strategy)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 marketing)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryanIwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strategy.clp      strategy3  "  ?id "  kAryanIwi)" crlf)
))

;@@@ Added by 14anu-ban-11 on (20-11-2014)
;The strategy of destroying the food supply of the civilian population in an area of conflict has been banned under Article 54 of Protocol I of the 1977 Geneva Conventions. (agriculture)
;संघर्ष के एक क्षेत्र में नागरिक आबादी की खाद्य आपूर्ति को नष्ट करने की रणनीति पर 1977 में जेनेवा कन्वेन्शन्ज के प्रोटोकॉल आई के अनुच्छेद 54 के तहत प्रतिबंध लगाया गया है .(manual)
(defrule strategy4
(declare (salience 1500))
(id-root ?id strategy)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 destroy)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raNanIwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strategy.clp      strategy4  "  ?id "  raNanIwi)" crlf)
))


;@@@ Added by 14anu-ban-01 on (20-02-2016)
;If you use that strategy, he will wipe you out. 	[sd_verified]
;यदि आप उस रणनीति का उपयोग करते हैं, तो वह आपको मिटा देगा . 	[self]
(defrule strategy5
(declare (salience 1500))
(id-root ?id strategy)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 use)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raNanIwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strategy.clp      strategy5  "  ?id "  raNanIwi)" crlf)
))
