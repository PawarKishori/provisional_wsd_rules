;##############################################################################
;#  Copyright (C) 2013-2014 Pramila (pramila3005@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;Removed by 14anu-ban-09 on (16-02-2015)
;NOTE-This rule is not required because this case is already handled in offence3.
;$$$ Modified by 14anu24
;It is an offence for operators to misdescribe what they offer you .
;सैर आयोजित करने वाले अगर प्रस्तुत सैर के बारे में कोई भी झूठ बात का बयान करते हैं , तो यह एक अपराध है .
;@@@ Added by Pramila(BU) on 29-03-2014
;It is an offence to ride a bicycle at night without light.  ;shiksharthi
;रात में बिना प्रकाश के साइकिल पर सवारी करना अपराध है.
;Explain the cause of offence.   ;shiksharthi
;अपराध का कारण बताइए.
;(defrule offence1
;(declare (salience 4900))
;(id-root ?id offence)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(or(saMjFA-to_kqxanwa  ?id ?id1)(viSeRya-of_saMbanXI  ?id1 ?id)(viSeRya-for_saMbanXI  ?id ?id1));added condition 'viSeRya-for_saMbanXI' 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id aparAXa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence1   "  ?id " aparAXa  )" crlf))
;)


;@@@ Added by Pramila(BU) on 29-03-2014
;He gave offence to his friend.  ;shiksharthi
;उसने अपने मित्र को नाराज कर दिया.
(defrule offence2
(declare (salience 5000))
(id-root ?id offence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nArAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence2   "  ?id " nArAja  )" crlf))
)

;Commented by 14anu-ban-09 on (26-09-2014) as meaning of offence is "अपराध" and the meaning which is given is the meaning of "convicted"
;@@@ Added By 14anu17
;Can be convicted of a criminal offence .
;एक आपराधिक अपराध का दोषी ठहराया जा सकता है. 
;(defrule offence5
;(declare (salience 101))
;(id-root ?id offence)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-viSeRaNa  ?id ? id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id xoRa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence5   "  ?id " xoRa  )" crlf))
;)

;Removed by 14anu-ban-09 on (16-02-2015)
;NOTE-This rule is not required because not showing any logical relations and this case is already handled in offence1.
;$$$ Modified by 14anu-ban-09 on (26-9-2014)
;Changed meaning from "aparAXa_hE" to "aparAXa"
;@@@ Added by 14anu17
;Just allowing anyone on your  premises  to produce or supply illegal drugs to another person is itself an offence .
;बनाने के लिए या सप्लाई करना नियम विरुद्ध दवा एक और व्यक्ति को आपके आधार-वाक्य पर कोई भी जरा अनुमति देती हुई स्वयम् है एक अपराध है. 
;(defrule offence6
;(declare (salience 100))
;(id-root ?id offence)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-det_viSeRaNa  ?id ?id1) 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id aparAXa)) ;Changed meaning from "aparAXa_hE" to "aparAXa" by 14anu-ban-09
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence6   "  ?id " aparAXa  )" crlf))
;)

;@@@ Added by 14anu-ban-09 on (16-02-2015)
;The photo may cause offence to some people. 	[oald]
;यह तस्वीर कुछ लोगों को नाराज कर सकती है . 		[Self]

(defrule offence7
(declare (salience 5200))
(id-root ?id offence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 photo)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nArAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence7   "  ?id " nArAja  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-02-2015)
;No one will take offence if you leave early.	[oald]
;कोई नाराज़ नहीं होगा अगर तुम जल्दी चले जाओगे.		[self]

(defrule offence8
(declare (salience 5200))
(id-root ?id offence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-vAkya_viSeRaNa  ?id1 ?id2)
(id-root ?id2 leave)
(kriyA-kriyA_viSeRaNa  ?id2 ?id3)
(id-root ?id3 early)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nArAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence8   "  ?id " nArAja  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-02-2015)
;Don't be so quick to take offence. [oald]
;बदला लेने की जल्दी मत कीजिए.   [manual]	

(defrule offence9
(declare (salience 5200))
(id-root ?id offence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 take)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence9   "  ?id " baxalA  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-02-2015)
;I'm sure he meant no offence when he said that.	[oald]	
;मुझे विश्वास हैं कि उसका इरादा नाराज़ करना नहीं था जब उसने यह कहा.	[Self]	
;I meant no offence.  ;shiksharthi
;मेरा इरादा नाराज़ करना नहीं था .		[self]		

(defrule offence10
(declare (salience 2000))
(id-root ?id offence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-kriyA_niReXaka  ?id ?id1)			
(id-root ?id1 no)					
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nArAja))		
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence10   "  ?id " nArAja  )" crlf))			
)

;----------------default rules-----------------------------
;$$$ Modified by 14anu-ban-09 on (16-02-2015) --- Added example sentences with translation
;It is an offence for operators to misdescribe what they offer you .	[same clp file]	
;सैर आयोजित करने वाले अगर प्रस्तुत सैर के बारे में कोई भी झूठ बात का बयान करते हैं , तो यह एक अपराध है . [manual]	
;It is an offence to ride a bicycle at night without light.  ;shiksharthi
;रात में बिना प्रकाश के साइकिल पर सवारी करना अपराध है.					
;Explain the cause of offence.   ;shiksharthi			
;अपराध का कारण बताइए.			
;@@@ Added by Pramila(BU) on 29-03-2014
;I meant no offence.  ;shiksharthi		;Removed by 14anu-ban-09 on (16-02-2015) because wrong example sentence 
;मेरा तिरस्कार करने का इरादा नहीं था.			;Removed by 14anu-ban-09 on (16-02-2015) because wrong example sentence 
(defrule offence3
(declare (salience 100))
(id-root ?id offence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aparAXa))		;changed meaning 'wiraskAra' to 'aparAXa' by 14anu-ban-09 on (16-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence3   "  ?id " aparAXa  )" crlf))
)


;@@@ Added by Pramila(BU) on 29-03-2014
(defrule offence4
(declare (salience 0))
(id-root ?id offence)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wiraskAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  offence.clp 	offence4   "  ?id " wiraskAra  )" crlf))
)


