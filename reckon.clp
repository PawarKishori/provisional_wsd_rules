;@@@ Added by 14anu-ban-05 on (25-04-2015)
;Interest is reckoned from the date of lending.	[from reckon.clp]
;ब्याज ऋण देने की तिथि से गिना जाता है.		[MANUAL]
(defrule reckon2
(declare (salience 4903))
(id-root ?id reckon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 interest|charge)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ginA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reckon.clp 	reckon2   "  ?id "  ginA_jA )" crlf))
) 

;@@@ Added by 14anu-ban-05 on (25-04-2015)
;In old days people reckoned on their son's help.	[from reckon.clp]
;पुराने दिनों में लोग अपने बेटों की मदद पर निर्भर होते थे.			[MANUAL]
(defrule reckon3
(declare (salience 4901))
(id-root ?id reckon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBara_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reckon.clp 	reckon3   "  ?id "  nirBara_ho )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-04-2015)
;I reckon that she will finish writing this paper by week end.[from reckon.clp]
;mEM ASA karawA hUz ki vaha sapwAha aMwa waka yaha pepara liKanA Kawama kara legI.[MANUAL]
(defrule reckon4
(declare (salience 4901))
(id-root ?id reckon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ASA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reckon.clp 	reckon4   "  ?id "  ASA_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-04-2015)
;I could see him reckoning the cost as I spoke. [OALD]
;मैं उसको हिसाब लगाते हुये देख सका जब मैं बोल रहा था  .		[MANUAL]
(defrule reckon5
(declare (salience 4901))
(id-root ?id reckon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 cost|boy)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hisAba_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reckon.clp 	reckon5   "  ?id "  hisAba_lagA )" crlf))
)  



(defrule reckon0
(declare (salience 5000))
(id-root ?id reckon)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id reckoning )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kula_xeya_KarcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  reckon.clp  	reckon0   "  ?id "  kula_xeya_KarcA )" crlf))
)

;"reckoning","N","1.kula_xeya_KarcA"
;There will be a heavy reckoning to pay.   
;
(defrule reckon1
(declare (salience 4900))
(id-root ?id reckon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reckon.clp 	reckon1   "  ?id "  mAna )" crlf))
)

;default_sense && category=verb	gina	0
;"reckon","VTI","1.ginanA"
;Interest is reckoned from the date of lending.
;Hire charges are reckoned from the date of delivery.
;--"2.nirBara_honA"   
;In old days people reckoned on their son's help.  
;--"3.mAnanA"
;I reckon she is smart enough to handle the job.
;
;LEVEL 
;Headword : reckon
;
;Examples --
;
;"reckon","V","1.mAnanA/samaJanA" 
;He is reckoned among our best friends.
;hamAre saBI miwroM meM usakI ginawI ucca koti meM howI hE.
;I rekcon that car will be ready by sunday evening.
;mEM samaJawA hUz ki kAra iwavAra kI SAma waka wEyAra ho jAegI.
;I reckon him as a scholar.
;mEM use vixvAna mAnawA hUz.
;--"2.ASA_karanA"
;I reckon that she will finish writing this paper by week end.
;mEM ASA karawA hUz ki vaha sapwAha aMwa waka yaha pepara liKanA Kawama kara legI.
;--"3.hisAba lagAnA"
;You did not reckon the number of boys present. 
;wumane upasWiwa ladZakoM kI saMKyA kI ginawI nahIM kI.
;
;
;'reckon' kA arWa 'mAnanA' se jodZA jA sakawA hE. isameM 'mAnanA' se baDZa kara BI kuCa BAva hE jisameM 'ASA_karanA' SAmila ho jAwA hE. 'hisAba_lagAnA' kuCa alaga lagawA hE. SAyaxa ASA ke sAWa usa ASA ke hone na hone kA hisAba judZa jAwA ho.
;
;anwarnihiwa sUwra ; 
;
;mAnanA -- samaJanA -- ASA_raKanA--hisAba lagAnA
;
;              
;sUwra : mAnanA[_>hisAba_lagAnA] 
;
;
