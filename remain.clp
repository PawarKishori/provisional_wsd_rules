;$$$ Modified by 14anu-ban-10 on (13-01-2015)
;$$$ Modified by 14anu18 (Commented (id-word ?id1 speed))
;The meaning remains the same.
;अर्थ वही बने रहता है .  
(defrule remain0
(declare (salience 5000))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 speed)
(kriyA-subject ?id ?id1)
(id-word ?id1 meaning)                    ;added by 14anu-ban-10 on (13-01-2015)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bane_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain0   "  ?id "  bane_raha )" crlf))
)

;As of now commenting this rule as we are getting mapping problem for the word "mother's"
;Added by Anita-19.12.2013
;;She will remain at her mother's until I return.
;;जब तक मैं लौटता हूँ तब तक वह अपनी माँ के यहाँ बनी रहेगी ।
;(defrule remain1
;(declare (salience 3000))
;(id-root ?id remain)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-subject ?id ?)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id banA_raha))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain1   "  ?id "  banA_raha )" crlf))
;)
;
(defrule remain2
(declare (salience 4700))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
;(id-root ?sub  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "inanimate.gdbm" ?str))) ;Added by Manju Suggested by Chaitanya Sir (17-08-13) ... the problem remains. ... samasyA SeRa rahawI hE
(id-root ?id1 ?sub);As suggested by Chaitanya Sir removed inanimate.gdbm and modified the fact as shown by Roja (03-12-13) 
(test (and (neq (numberp ?sub) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?sub) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SeRa_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain2   "  ?id "  SeRa_raha )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (13-01-2015)
;@@@ Added by 14anu07 0n 01/07/2014
;The exercise remains largely futile. 
;व्यायाम व्यापक रूप से व्यर्थ बना रहता है . 
(defrule remain10
(declare (salience 4800))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 exercise)                         ;added by 14anu-ban-10 on (13-01-2015)
;(subject-subject_samAnAXikaraNa ?id1 ?id2)     ;commented out by 14anu-ban-10 on (13-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain10   "  ?id "  banA_raha )" crlf))
)

;@@@ Added by Anita 19.12.2013
;Despite the chaos around him, he remained calm. [cambridge dictionary]
;उसके इधर-उधर अव्यवस्था के बावजूद वह शान्त रहा ।  (verified-sentence)
;$$$Modified by 14anu19 (18-06-14) (passive word is added in list)
;The social policy followed by the British , however , did not remain passive.
;ब्रिटेने वासी से अनुसरण की हुई सामाजिक नीति, हालाँकि, निष्क्रिय शेष नहीं रही थी . (before modification)
;ब्रिटेन वासियो द्वारा अनुसरण की हुई सामाजिक नीति, हालाँकि, निष्क्रिय नहीं रही थी (after modification)
(defrule remain3
(declare (salience 4800))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 calm|passive)   ;added word passive in list
(kriyA-subject  ?id ?id1)
(subject-subject_samAnAXikaraNa  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain3   "  ?id "  raha )" crlf))
)

;@@@ Added by Anita -18.12.2013
;Only a few hundred of these animals remain today. [cambridge dictionary]
;इनमें से अब कुछ सौ जानवर ही बचे हैं ।
;ina paSuoM kA sirPa kuCa sO Aja bacawA hE. (tam problem & word order problem)
(defrule remain4
(declare (salience 4900))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain4   "  ?id "  baca )" crlf))
)

;@@@ Added by Anita -18.12.2013
;The exact date of the wedding remains a secret. [cambridge dictionary]
;शादी की पक्की तारीख एक राज़ बनी हुई है । (tam problem)
(defrule remain5
(declare (salience 4950))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "time.gdbm" ?str)))
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 wedding|birthday|marriage|event|anniversary)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain5   "  ?id "  banI_ho )" crlf))
)

;Added by Anita -18.12.2013
;Mix in half the butter and keep the remaining 50g for later. [cambridge dictionary]
;आधा मक्खन मिलाएं और शेष ५० ग्राम बाद के लिए रख लें ।
;(defrule remain5
;(declare (salience 5000))
;(id-root ?id remaining)
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 50g)
;(id-root ?id2 later)
;(id-root ?id3 the)
;(and(viSeRya-viSeRaNa  ?id1 ?id)(viSeRya-for_saMbanXI  ?id1 ?id2)(viSeRya-det_viSeRaNa  ?id1 ?id3))
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id SeRa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain5   "  ?id ;"  SeRa )" crlf))
;)

;@@@ Added by Anita -19.3.2014
;Hawaii remains a popular choice for winter vacation travel. [By mail]
;जाड़ा अवकाश के लिये हवाई एक लोकप्रिय विकल्प शेष रहता है।
(defrule remain6
(declare (salience 5000))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SeRa_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain6   "  ?id "  SeRa_raha )" crlf))
)

;@@@ Added by Anita-02-04-2014
;We may call the chosen part of the assembly as the system and the remaining part of the assembly plus any other agencies ;of forces as the environment.  [ncert]
;संयोजन के चुने गए भाग को हम निकाय कह सकते हैं तथा संयोजन के बचे हुए भाग (निकाय पर आरोपित बलों के अन्य साधनों को सम्मिलित करते हुए) को वातावरण कह सकते हैं.
(defrule remain07
(declare (salience 5100))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 part)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain07   "  ?id "  baca )" crlf))
)


;@@@ Added by 14anu19 on 16-June-2014
;This country would not have remained cut off from all the changes.
;यह देश सभी बदलावों से अछूता नहीं रहा होता .
(defrule remain7
(declare (salience 5500))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
(id-word =(+ ?id 1) cut)
(id-word =(+ ?id 2) off)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp   remain7   "  ?id "  raha )" crlf))
)

;@@@ Added by 14anu17 28-06-14
;It is also possible to improve the quality of life for those who remain mentally ill , by improving the availability of sensitive local health and social services .
;वह के लिए जीवन की गुणवत्ता सुधारना सम्भव भी है जो संवेदनशील स्थानीय स्वास्थ्य का और सामाजिक सेवाओं का उपलब्धता सुधारने के द्वारा मानसिक रूप से अस्वस्थ, रहते है . 
(defrule remain8
(declare (salience 4701))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1)  mentally)
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain8   "  ?id "  raha )" crlf))
)

;@@@ Added by 14anu-ban-10 on (11-10-2014)
;added intensity to the list by 14anu-ban-10 on (20-01-2015)
;Rotating P1 has no effect on the transmitted beam and transmitted intensity remains constant.[ncert corpus] ;added by 14anu-ban-10 on (20-01-2015)
;@P 1 ko GumAne para pAragawa kiraNa-puFja para koI praBAva nahIM padawA kyoMki pAragamiwa wIvrawA sWira rahawI hE.[ncert corpus];added by 14anu-ban-10 on (20-01-2015)
;The total energy of the system (stone plus the surroundings) remains unchanged.  [ncert corpus]
;viyukwa nikAya (pawWara waWA prawiveSa) kI kula UrjA aparivarwiwa rahawI hE. [ncert corpus]
;वियुक्त निकाय (पत्थर तथा प्रतिवेश) की कुल ऊर्जा अपरिवर्तित रहती है.
;If velocity has a component along B, this component remains unchanged as the motion along the magnetic field will not be affected by the magnetic field.[ncert corpus]; added by 14anu-ban-10 on (26-11-2014)
;yaxi vega @v kA koI avayava hE, @B ke anuxiSa wo yaha avayava aparivarwiwa rahawA hE, kyoMki cumbakIya kRewra ke anuxiSa gawi ko cumbakIya kRewra praBAviwa nahIM karegA.[ncert corpus]; added by 14anu-ban-10 on (26-11-2014)
;The date of the accident remains engraved on my mind.[oald] ;Added by 14anu-ban-10 on (19-03-2015)
;दुर्घटना की तारीख मेरे मन पर छपी रही .[manual]   ;Added by 14anu-ban-10 on (19-03-2015)
(defrule remain010
(declare (salience 5600))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 sum|energy|component|intensity|date);added component by 14anu-ban-10 on (26-11-2014)  ;removed who by 14anu-ban-10 on (13-01-2015);added date by 14anu-ban-10 on (19-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain010   "  ?id "  raha )" crlf))
)

;@@@ Added by 14anu-ban-10 on (15-11-2014)
;To summarise, if the net external force is zero, a body at rest continues to remain at rest and a body in motion continues to move with a uniform velocity.[ncert corpus]
;sArAMSa meM, yaxi neta bAhya bala SUnya hE wo virAma avasWA meM raha rahA piNda virAmAvasWA meM hI rahawA hE Ora gawiSIla piNda niranwara ekasamAna vega se gawiSIla rahawA hE. [ncert corpus]
(defrule remain11
(declare (salience 5700))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain11   "  ?id "  raha )" crlf))
)

;@@@ Added by 14anu-ban-10 on (28-11-2014)
;Thus, The above equation implies that when a wave gets refracted into a denser medium (v 1 > v 2) the wavelength and the speed of propagation decrease but the frequency remains the same.[ncert corpus]
;awaH uparokwa samIkaraNa meM anwarnihiwa hE ki jaba warafga saGana mAXyama meM apavarwiwa howI hE ( @v 1 > @v 2 ), wo warafgaxErGya waWA saFcaraNa kI cAla kama ho jAwI hE, lekina Avqwwi uwanI hI rahawI hE.[ncert corpus]
(defrule remain12
(declare (salience 5900))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1 )
(id-root ?id1 frequency)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwanI_hI_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain12   "  ?id "  uwanI_hI_raha )" crlf))
)

;@@@ Added by 14anu-ban-10 on (06-12-2014)
;The prothorax is large and broader than the remaining segments.[agriculture domain]
;प्रोव्थॊरैक्स बडा और अधिक विस्तृत है शेष भागों की अपेक्षा  [manual]
(defrule remain13
(declare (salience 6000))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 segment)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  SeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain13   "  ?id "  SeRa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-12-2014)
;The field lines remain continuous, emerging from one face of the solenoid and entering into the other face.  [ncert corpus]
;kRewra reKAez sanwawa banI rahawI hEM, eka sire se bAhara nikalawI hEM Ora xUsare sire se anxara praveSa karawI hEM.  [ncert corpus]
(defrule remain14
(declare (salience 6100))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-vAkyakarma  ?id ?id1 );commented out by 14anu-ban-10 on (03-02-2015)
;(id-root ?id1 emerge)  ;added by 14anu-ban-10 on (13-01-2015) ;commented out by 14anu-ban-10 on (03-02-2015)
(kriyA-subject ?id ?id1) ;added by 14anu-ban-10 on (13-01-2015)
(id-root ?id1 line)  ;added by 14anu-ban-10 on (13-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   banI_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain14   "  ?id "   banI_raha)" crlf))
)

;####################################default-rule#################################

;@@@ Added by Anita -19.12.2013
;The remains of a Buddhist temple. [cambridge dictionary]
;बौद्ध मंदिर के अवशेष
;Three months after he disappeared, his remains were found in a cave.  [cambridge dictionary]
;उसके तीन महीने तक ग़ायब रहने के बाद एक गुफा में उसके अवशेष पाए गए ।
(defrule remain_default_noun
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaSeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain_default_noun   "  ?id "  avaSeRa )" crlf))
)

;Yet the life style of the people continues to remain very much the same. added sentence by Anita-3.6.2014
;तो भी लोगों की जीवन-शैली निरन्तर वैसी ही रहती है । 
(defrule remain_default_verb
(declare (salience 100))
(id-root ?id remain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remain.clp 	remain_default_verb   "  ?id "  raha )" crlf))
)

;"remain","V","1.[SeRa]_raha"
;default_sense && category=verb	[SeRa]_raha	0
;"remain","VI","1.SeRa_rahanA"
;If you take 5 from 9,4 remains.
;After the fire,very little remained of the building.
;Much still remains to be done.
;--"2.bane_rahanA"
;Remain standing until I ask you.      
;I shall remain with you for a week.
;
