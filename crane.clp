;@@@ Added by 14anu01
;They had to use a crane to lift the object.
;उनको वस्तु उठाने के लिए क्रेन का उपयोग करना पडा . 
(defrule crane2
(declare (salience 5500))
(id-root ?id crane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 1) to)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id krena))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crane.clp 	crane2   "  ?id "  krena)" crlf))
)


;@@@ Added by 14anu-ban-03 (09-03-2015)
;Cranes have long legs && long necks. [same clp]
;सारसों की लम्बी टाँगें और लम्बी गरदनें होती हैं . [manual]
(defrule crane3
(declare (salience 5000))
(id-root ?id crane)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(kriyA-object ?id1 ?id2)
(id-root ?id2 leg|neck)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sArasa))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crane.clp 	crane3  "  ?id "  sArasa )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$ Modified by 14anu-ban-03 (09-03-2015)
;The crane is securely anchored at two points.[oald]
;क्रेन  मज़बूती से  दोनों कांटो से बन्धा हुआ है .[self]
(defrule crane0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (09-03-2015)
(id-root ?id crane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id krena))  ;meaning changed from 'sArasa' to 'krena' by 14anu-ban-03 (09-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crane.clp 	crane0   "  ?id "  krena )" crlf))
)                      ;meaning changed from 'sArasa' to 'krena' by 14anu-ban-03 (09-03-2015)


;$$$ Modified by 14anu01
;She had to crane her neck to see the movie.
;उसको चलचित्र को देखने के लिए उसकी गरदन आगे निकालना पडी .
;"crane","VTI","1.krena_se_uTAnA/2.garaxana_nikAlanA"
;Crane your neck forward so you could get a better look. 
(defrule crane1
(declare (salience 4900))
(id-root ?id crane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_nikAlanA));Replaced 'krena_se_utA' by 'Age_nikAlanA' by 14anu01
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crane.clp 	crane1   "  ?id "  Age_nikAlanA)" crlf)); 
)


;"crane","N","1.sArasa"
;Cranes have long legs && long necks.  
;--"2.krena/BArowwolana_yaMwra"
;Cranes can move && lift very heavy weights.

