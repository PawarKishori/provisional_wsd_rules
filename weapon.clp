
;@@@ Added by Anita--18.7.2014
;Education is the only weapon to fight the spread of the disease.  [oxford learner's dictionary]
;केवल शिक्षा रोग के प्रसार से लड़ने का शस्त्र है ।
;The youths were dragged from their car and searched for weapons. [cambridge dictionary]
;युवकों को उनकी कार से घसीटा गया और हथियार को खोजा गया ।
(defrule weapon0
(declare (salience 5000))
(id-root ?id weapon)
?mng <-(meaning_to_be_decided ?id)
(or(subject-subject_samAnAXikaraNa  ? ?id)(kriyA-for_saMbanXI  ? ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Saswra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weapon.clp 	weapon0   "  ?id "  Saswra )" crlf))
)

;@@@ Added by Anita--19.7.2014
;Guilt is the secret weapon for the control of children. [oxford learner's dictionary]
;अपराधबोध बच्चों के नियंत्रण के लिए गुप्त हथियार है ।
(defrule weapon1
(declare (salience 5100))
(id-root ?id weapon)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 secret)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haWiyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weapon.clp 	weapon1   "  ?id "  haWiyAra )" crlf))
)

;@@@ Added by Anita--19.7.2014
;He made me a weapon during election. [self]
;उसने चुनाव के दौरान मुझे मोहरा बनाया ।
(defrule weapon2
(declare (salience 5200))
(id-root ?id weapon)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-during_saMbanXI  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weapon.clp 	weapon2   "  ?id "  moharA )" crlf))
)

;########################################### default-rule #####################################
;@@@ Added by Anita--18.7.2014
;The police still haven't found the murder weapon. [oxford learner's dictionary]
;पुलिस को अभी भी हत्या का हथियार नहीं मिला है ।
;He was charged with carrying an offensive weapon. [oxford learner's dictionary]
;उस पर एक आक्रामक हथियार ले जाने का आरोप था ।
(defrule weapon_default-rule
(declare (salience 0))
(id-root ?id weapon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haWiyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weapon.clp 	weapon_default-rule   "  ?id "  haWiyAra )" crlf))
)

;@@@ Added by 14anu-ban-11 on (15-10-2014)
;The most destructive weapons made by man, the fission and fusion bombs are manifestations of the above equivalence of mass and energy.(Ncert)
;eka ora jahAz mAnava jAwi xvArA nirmiwa awyanwa vinASakArI nABikIya AyuXa, viKaNdana evaM saMlayana bama uparokwa wulyawA [samIkaraNa (6.20)] sambanXa kI aBivyakwi hE.
(defrule weapon6
(declare (salience 5000))
(id-root ?id weapon)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AyuXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weapon.clp 	weapon6   "  ?id "  AyuXa )" crlf))
)
