;@@@ Added by 14anu-ban-06 (28-02-2015)
;It's a favorite hoary memory of all old-time farmers . (COCA)[parser no. 2]
;यह सभी पुराने जमाने के किसानों की एक मनपसन्द पुरानी स्मृति है . (manual)
(defrule hoary0
(declare (salience 0))
(id-root ?id hoary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id purAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hoary.clp 	hoary0   "  ?id "  purAnA )" crlf))
)

;@@@ Added by 14anu-ban-06 (28-02-2015)
;His hair was a hoary white. (OALD)[parser no. 13]
;उसके केश धूसर सफेद थे . (manual)
(defrule hoary1
(declare (salience 2000))
(id-root ?id hoary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(subject-subject_samAnAXikaraNa ?id2 ?id1)
(id-root ?id2 hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XUsara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hoary.clp 	hoary1   "  ?id "  XUsara )" crlf))
)
