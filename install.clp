;##############################################################################
;#  Copyright (C) 2013-2014  Prachi Rathore (prachirathore02@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;;@@@   ---Added by Prachi Rathore
;  She has installed a couple of young academics as her advisers.[oald]
;वह पदारूढ कर चुकी है कुछ उच्च श्रेणी के शिक्षको को उसके सलाहकारों के रूप मे   . 
(defrule install0
(declare (salience 5000))
(id-root ?id install)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxArUDa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  install.clp 	install0   "  ?id "  paxArUDa_kara )" crlf))
)

;;@@@   ---Added by Prachi Rathore
; We installed ourselves in the front row.[oald]
;हम सामने की पंक्ति में हम स्वंय जम गये .  
(defrule install1
(declare (salience 5000))
(id-root ?id install)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jama_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  install.clp 	install1   "  ?id "  jama_jA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (21-08-14)
;@@@ Added by 14anu-ban-04 on 28-07-2014
;White marble is installed on the floor and and the temple .
;ParSa Ora manxira para saPexa sangamaramara lage hE.
;Dada posters are installed on the ceiling. (COCA)
;xAxA vijFApana Cawa para lagAye gaye hEM.
;Jimmy Carter had solar water heaters installed on the roof of the White House in 1979. (COCA)
;jimmI koYrtara ne 1979 meM sOra pAnI hItara vAita hAusa kI Cawa para lagAye We.
(defrule install2
(declare (salience 4200))
(id-root ?id install)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI ?id ?id1)
(id-root ?id1 floor|roof|ceiling|wall);added roof|ceiling|wall by 14anu-ban-06
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  install.clp 	install2   "  ?id " laga )" crlf))
)


;xxxxxxxxxxxx Default Rule xxxxxxxxxx
;;@@@   ---Added by Prachi Rathore
; The hotel chain has recently installed a new booking system.[oald]
;होटल श्रृङ्खला एक नयी बुकिंग व्यवस्था हाल मेँ संस्थापना कर चुकी है . 
;$$$ Modified by 14anu-ban-04 on 27-07-2014  --- changed meaning from saMsWApanA_kara to sWApiwa.
;This oldest temple of Uttarkashi is installed on this street .    [tourism_corpus]
;uttarkashi kA yaha sabase aXika purAnA manxira isa sadaka para sWapiwa hE.
;The statue of Nandi is installed outside .           [tourism_corpus]
;nanxI kI prawimA bAhara sWApiwa hE. 
(defrule install3
(declare (salience 4000))
(id-root ?id install)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWApiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  install.clp 	install3   "  ?id "  sWApiwa)" crlf))
)

;@@@ Added by 14anu-ban-06 (21-08-14)
;Close circuit TVs have also been installed at main places .(Parallel corpus)
;प्रमुख  स्थानों  पर  क्लोज  सर्किट  टीवी  कैमरे  भी  लगाए  गए  हैं  ।
;A milk chilling plant of 4 , 000 litres capacity has been installed at the Dollygunj Farm complex .(Parallel corpus)
;डौलीगंज फार्म विस्तार योजना के अंतगर्त वहां पर चार हजार लीटर क्षमता की एक दूध को ठन्डा करने की मशीन लगाई गई है .
(defrule install4
(declare (salience 4500))
(id-root ?id install)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 TVs|plant|windmill|mill)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  install.clp 	install4   "  ?id "  lagA )" crlf))
)
