;$$$ Modified by 14anu09[16-6-14]
;$$$ Modified by 14anu07 0n 04/07/2014
;It is also worth asking whether a blind numbers approach is the correct - indeed , only - way to ensure a stable population .
;यह भी पूछा जा सकता है कि क्या बच्चों की संया की सीमा तय करना ही जनसंया वृद्धि को रोकने का सही उपाय है .
(defrule whether0
(declare (salience 5000))
(id-root ?id whether)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) ask|decide|depend|discuss|know|tell|question|wonder|check|enquire)
;(id-word ?id1  or)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whether.clp 	whether0   "  ?id "  ki )" crlf))
)

(defrule whether1
(declare (salience 4900))
(id-root ?id whether)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) ask|decide|depend|discuss|know|tell|question|wonder)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA)); Modified ki_kyA as kyA by Manju Ex: I wonder whether we should go.
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whether.clp 	whether1   "  ?id "  kyA )" crlf))
)

;Whether he comes or not I will go.
;cAhe vo Aye yA nA Aye meM jAUmgA
(defrule whether2
(declare (salience 100))
(id-root ?id whether)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id conjunction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAhe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whether.clp 	whether2   "  ?id "  cAhe )" crlf))
)

;"whether","Conj","1.ki"
;I don't know whether he will come .
;--"2.cAheM"
;You will get a momento whether you take part in the quiz or not.
;

 ;Aded by sheetal(1-12-2009).
;$$$ MOdified by Pramila(BU) on 20-02-2014
(defrule whether3
(declare (salience 4700))
(id-root 1 whether)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng 1 kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whether.clp   whether3   "  1 "  kyA )" crlf))
)
;Whether we should go to the party is the important question .


;@@@ Added by Pramila(BU) on 20-02-2014
;Ask the guard whether he knows.   ;
;पहरेदार से पूछिए कि क्या वह जानता है
;$$$Modified by 14anu18 (13-06-2014)
;Modified kyA as yaxi in meaning.
;Commented conditions(id-root ?kri know) and (kriyA-vAkyakarma  ?id1 ?kri
(defrule whether4
(declare (salience 4800))
(id-root ?id whether)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id conjunction)
(kriyA-vAkya_viBakwi  ?kri ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaxi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whether.clp  whether4    "  ?id "  yaxi)" crlf))
)
;@@@ Added by 14anu09[16-6-14]
;They asked him whether he would come.
;उन्होंने उसको कहा कि क्या वह आएगा . 
(defrule whether5
(declare (salience 4700))
(id-root ?id whether)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id conjunction)
(kriyA-vAkya_viBakwi  ?kri ?id)
(kriyA-vAkyakarma  ?id1 ?kri)
;(id-root ?kri ask|decide|depend|discuss|know|tell|question|wonder|check|enquire)remove the comment if overgeneralized
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ki_kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whether.clp 	whether5   "  ?id "  ki_kyA )" crlf))
)

;@@@ Added by 14anu09[23-06-14]
;You must decide on whether to leave.
;तुमें सोचना होगा कि क्या हमे जाना चाहिए.
(defrule whether6
(declare (salience 4900))
(id-root ?id whether)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) on)
(id-cat_coarse ?id conjunction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whether.clp 	whether6   "  ?id "  kyA )" crlf))
)

;@@@ Added by 14anu17
;Check whether your footwear is a comfortable fit.
(defrule whether7
(declare (salience 5000))
(id-root ?id whether)
?mng <-(meaning_to_be_decided ?id)
;((- ?id 1)-cat_coarse ?(- ?id 1) verb)
(id-cat_coarse =(- ?id 1) verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whether.clp     whether7   "  ?id "  ki )" crlf))
)

;@@@Added by 14anu18
;He threatens to go, whether or no.
;वो जाने की धमकी देता चाहे जो हो.
(defrule whether44
(declare (salience 4800))
(id-root ?id whether)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id conjunction)
(id-word =(+ ?id 1) or)
(id-word =(+ ?id 2) no)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cAhe_jo_ho))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  whether.clp     whether44   "  ?id "  cAhe_jo_ho )" crlf))
)

