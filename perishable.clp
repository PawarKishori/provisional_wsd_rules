
;---------------------------------------------DEFAULT RULE-------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on 26-9-14
;You know, he's always believed that his celebrity was perishable. [COCA] 
;wumahe patA hE ki uskA hameSA se yaha mAnanA hE ki usake salebritI bigadanevAle We. [Own Manual]

(defrule perishable1
(id-root ?id perishable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bigadanevAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  perishable.clp  	perishable1   "  ?id "  bigadanevAlA )" crlf))
)

;-------------------------------------------------------------------------------------------------------------------------------------------

;$$$ Modified by 14anu-ban-09 on 26-9-14
;Changed meaning from "vinaSIla" to "vinASIla"
;@@@ Added by 14anu17
; Airplanes and helicopters are employed in agriculture for transporting perishable products and fighting forest fires .
; हवाई जहाज और हेलिकोप्टर विनशील उत्पाद वहन करने के लिए और जङ्गल आग लडाई करना कृषि वर्ग में उपयोग किए गये हैं . 
;हवाई जहाज और हेलिकोप्टर विनाशील उत्पाद वहन करने के लिए और जङ्गल आग लडाई करना कृषि वर्ग में उपयोग किए गये हैं . [Own Manual]

(defrule perishable0
(declare (salience 5000))
(id-root ?id perishable)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id) ;added by 14anu-ban-09
(id-cat_coarse ?id adjective)
(id-root ?id1 product) ;added by 14anu-ban-09 ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vinASIla)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  perishable.clp  	perishable0   "  ?id "  vinASIla )" crlf))
)
