;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 16-dec-2013
;They met Mongla outside and took him along.[gyananidhi]
;वे बाहर मोंगला से मिले और उसको साथ ले गये.
(defrule along7
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?kri ?id)
(id-word ?kri took)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along7   "  ?id "  sAWa )" crlf))
)

;$$$ Modified by 14anu-ban-02(22-11-2014)
;###[COUNTER EXAMPLE]### The only conceivable external force along the road is the force of friction.[ncert]
;सडक के अनुदिश विचारणीय बल घर्षण बल ही है.[ncert]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 30-dec-2013
;Further along the same road lived the old woman who sold grass.[gyananidhi]
;They walked slowly along the road.[oald];along16 is for this sentence.
(defrule along8
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
;(or (viSeRya-along_saMbanXI  ? ?id1) (kriyA-along_saMbanXI  ? ?id1));commented by 14anu-ban-02(22-11-2014)
(viSeRya-along_saMbanXI  ?id2 ?id1);Added by 14anu-ban-02(22-11-2014)
(id-word ?id1 road)
(id-word ?id2 further);Added by 14anu-ban-02(22-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along8   "  ?id "  ke_sAWa_sAWa )" crlf))
)

;@@@ Added by 14anu-ban-02(22-11-2014)
;They walked slowly along the road.[oald]
;वे सडक के किनारे धीरे-धीरे चले थे.[manual]
(defrule along16
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(kriyA-along_saMbanXI  ?id2 ?id1)
(id-word ?id1 road)
(id-root ?id2 walk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_kinAre))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along16   "  ?id "  ke_kinAre )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 30-dec-2013
;I looked along the shelves for the book I needed.[oald]
;जो किताब मुझे चाहिये थी उसके लिये मैंने सभी शेल्फ के एक कोने से दूसरे कोने तक देखा
(defrule along9
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(kriyA-along_saMbanXI  ? ?id1)
(id-root ?id1 shelf)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_eka_kone_se_xUsare_kone_waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along9   "  ?id "  ke_eka_kone_se_xUsare_kone_waka )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 30-dec-2013
;I was just walking along singing to myself.[oald]
;मैं बस अपने आप में गाता हुआ आगे चल रहा था 
(defrule along10
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(kriyA-along_saMbanXI  =(- ?id 1) =(+ ?id 1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along10   "  ?id "  Age )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 30-dec-2013
;He pointed out various landmarks as we drove along.[oald]
;उसने विभिन्न युगान्तरकारी घटनाएँ  दिखाईं जैसे जैसे  हम आगे  बढे.  
(defrule along11
(declare (salience 5500)); salience kept higher so that it does not conflicts with rule12
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id1 ?id)
(id-root ?id1 drive)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id Age_baDe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " along.clp  along11  "  ?id1 "  " ?id "  Age_baDe   )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 30-dec-2013
;Why don't you come along?[oald]
;आप साथ क्यों नहीं चलते
(defrule along12
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(id-last_word ?id along)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along12   "  ?id "  sAWa )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 30-dec-2013
;I'll be along in a few minutes.[oald]
;मैं कुछ मिनटों में साथ होऊँगा . 
(defrule along13
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(kriyA-in_saMbanXI  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along13   "  ?id "  sAWa )" crlf))
)

;@@@ Added by 14anu-ban-02(11-10-2014)
;To describe motion along a straight line, we can choose an axis, say X-axis, so that it coincides with the path of the object.[ncert]
;एक सरल रेखा में किसी वस्तु की गति के विवरण हेतु हम एक अक्ष (मान लीजिए X-अक्ष) को इस प्रकार चुन सकते हैं कि वह वस्तु के पथ के सम्पाती हो  [ncert]
(defrule along14
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(kriyA-along_saMbanXI  ?id1 ?)
(kriyA-object ?id1 ?id2)
(id-word ?id1 describe)
(id-word ?id2 motion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along14   "  ?id "  meM )" crlf))
)

;salience is reduce for this sentence
;They walked slowly along the road.[oald]
;वे सडक के किनारे धीरे-धीरे चले थे.[manual]
;$$$ Modified by 14anu-ban-06 (20-10-2014)
;The right side is a product of the displacement and the component of the force along the displacement.
;समीकरण का दायाँ पक्ष वस्तु पर आरोपित बल का विस्थापन के अनुदिश घटक और वस्तु के विस्थापन का गुणनफल है.(NCERT)
;@@@ Added by 14anu-ban-02 (14-10-2014)
;(kriyA-along_saMbanXI)
;It is convenient to resolve a general vector along the axes of a rectangular coordinate system using vectors of unit magnitude.[ncert]
;एकाङ्क परिमाण के सदिशों की सहायता से समकोणिक निर्देशाङ्क निकाय के अनुदिश किसी सदिश का वियोजन सुविधाजनक होता है .[ncert]
;एकाङ्क परिमाण के सदिशों की सहायता से समकोणिक निर्देशाङ्क निकाय के अक्षों के अनुदिश किसी सदिश का वियोजन सुविधाजनक होता है .[modified]
;(viSeRya-along_saMbanXI)
;The resultant vector R is directed from the common origin O along the diagonal (OS) of the parallelogram [Fig. 4.6(b)].[ncert]
;The resultant vector R is directed from the common origin (O) along the diagonal (OS) of the parallelogram [Fig. 4.6(b)].[modified]
;परिणामी सदिश R की दिशा समान्तर चतुर्भुज के मूल बिंदु O से कटान बिंदु S की ओर खीँचे गए विकर्ण OS के अनुदिश होगी [चित्र 4.6(b)].[ncert]
;If velocity has a component along B, this component remains unchanged as the motion along the magnetic field will not be affected by the magnetic field.[ncert]
;यदि वेग v का कोई अवयव है, B के अनुदिश तो यह अवयव अपरिवर्तित रहता है, क्योंकि चुम्बकीय क्षेत्र के अनुदिश गति को चुम्बकीय क्षेत्र प्रभावित नहीं करेगा.[ncert]
(defrule along15
(declare (salience 4800));salience is reduced from 5000 to 4800
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id));Added by 14anu-ban-02(04-12-2014)
(or(kriyA-along_saMbanXI  ? ?id1)(viSeRya-along_saMbanXI  ? ?id1))
(id-word ?id1 axes|diagonal|displacement|road|field);added 'displacement' by 14anu-ban-06 (20-10-2014); added 'road' by 14anu-ban-02(22-11-2014); added 'field' by 14anu-ban-02(04-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_anuxiSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along15   "  ?id "  ke_anuxiSa )" crlf))
)

;@@@ Added by 14anu-ban-02(04-12-2014)
;If velocity has a component along B, this component remains unchanged as the motion along the magnetic field will not be affected by the magnetic field.[ncert]
;यदि वेग v का कोई अवयव है, B के अनुदिश तो यह अवयव अपरिवर्तित रहता है, क्योंकि चुम्बकीय क्षेत्र के अनुदिश गति को चुम्बकीय क्षेत्र प्रभावित नहीं करेगा.[ncert]
(defrule along17
(declare (salience 4800))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(pada_info (group_head_id ?id1)(preposition ?id))
(viSeRya-along_saMbanXI  ? ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_anuxiSa))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  along.clp       along17   "  ?id "  physics )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along17   "  ?id "  ke_anuxiSa )" crlf))
)


(defrule along0
(declare (salience 5000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) accept|acknowledge|add|admit|agree|allege|announce|answer|argue|arrange|assert|assume|assure|believe|boast|check|claim|comment|complain|concede|conclude|confirm|consider|contend|convince|decide|demonstrate|deny|determine|discover|dispute|doubt|dream|elicit|ensure|estimate|expect|explain|fear|feel|figure|find|foresee|forget|gather|guarantee|guess|hear|hold|hope|imagine|imply|indicate|inform|insist|judge|know|learn|maintain|mean|mention|note|notice|notify|object|observe|perceive|persuade|pledge|pray|predict|pretend|promise|prophesy|prove|read|realize|reason|reassure|recall|reckon|record|reflect|remark|remember|repeat|reply|report|require|resolve|reveal|say|see|sense|show|state|suggest|suppose|swear|teach|tell|think|threaten|understand|vow|warn|wish|worry|write)
(id-word =(+ ?id 1) what|when|where|why|how|who)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isa_ke_barAbara_barAbara_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along0   "  ?id "  isa_ke_barAbara_barAbara_ki )" crlf))
)

(defrule along1
(declare (salience 4900))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) what|when|where|why|how|who)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id usa_ke_barAbara_barAbara_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along1   "  ?id "  usa_ke_barAbara_barAbara_ki )" crlf))
)

;Commented by 14anu-ban-02(07-02-2015)
;correct meaning is comimg from along13 and coming alongis handled in come.
;By Garima Singh ....this rule needs to be modified as there are many sentences in which category of word preceeding along is verb but sense of 'along' differs in each sentence.
; Ex: I'll be along in a few minutes.[oald]
;ex: The book's coming along nicely.[oald]
;(defrule along2
;(declare (salience 4800))
;(id-root ?id along)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse =(- ?id 1) verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ke_sAWa_sAWa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along2   "  ?id "  ke_sAWa_sAWa )" crlf))
;)

(defrule along3
(declare (salience 4700))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(- ?id 1) noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_kinAre))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along3   "  ?id "  ke_kinAre )" crlf))
)


;Added by sheetal for Open Logos
;It is true that you are my friend but I can not go along with you on this issue.
(defrule along6
(declare (salience 4850))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id preposition)
(id-word =(+ ?id 1) with)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp     along6   "  ?id "  ke_sAwa )" crlf))
)

;@@@Added by 14anu-ban-02(07-02-2015)
;Guards have been posted along the border.[oald]
;रक्षक सीमा के पास नियुक्त किए गये हैं .[manual]
(defrule along18
(declare (salience 1000))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-along_saMbanXI  ?id1 ?id2)
(id-root ?id2 border)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp     along18   "  ?id "  ke_pAsa )" crlf))
)

;@@@ Added by 14anu-ban-02(20-02-2015)
;We shall confine ourselves to the study of motion of objects along a straight line, also known as rectilinear motion.[ncert 11_03]
;इस अध्याय में हम अपना अध्ययन वस्तु के एक सरल रेखा के अनुदिश गति तक ही सीमित रखेंगे ;इस प्रकार की गति को सरल रेखीय गति भी कहते हैं .[ncert]
(defrule along19
(declare (salience 4800))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(kriyA-along_saMbanXI  ? ?id1)
(viSeRya-viSeRaNa  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_anuxiSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along19   "  ?id "  ke_anuxiSa )" crlf))
)

;@@@Added by 14anu-ban-02(23-02-2015)
;He panned the camera along the row of faces.     [oald]
;उसने कैमरा चेहरों की पङ्क्ति के साथ-साथ घुमाया .         [self] 
(defrule along20
(declare (salience 4800))
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(kriyA-along_saMbanXI  ? ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along20   "  ?id "  ke_sAWa_sAWa )" crlf))
)

;********************DEFAULT RULES***********************************


(defrule along4
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_baDanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along4   "  ?id "  Age_baDanA )" crlf))
)

;"along","Adv","1.Age_baDanA"
;The work is moving along
;

;$$$ modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 30-dec-2013.
(defrule along5
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id along)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa_sAWa)); meaning changed from 'barabara_barAbara' to 'ke_sAWa_sAWa'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  along.clp 	along5   "  ?id "  ke_sAWa_sAWa )" crlf))
)

;"along","Prep","1.barAbara_barAbara"
;His little sister came along to the movies
;
