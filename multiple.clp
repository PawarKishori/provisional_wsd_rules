;@@@ Added by 14anu-ban-08 (03-12-2014)
;Reliance on knowledge, experience, observation and integration of multiple techniques makes IPM appropriate for organic farming (excluding synthetic pesticides).       [Agriculture]
;ज्ञान, अनुभव, पर्यवेक्षण पर आधारित और कई तकनीकों के समाकलन के  कारण IPM  organic farming (संश्लिष्ट कीटनाशकदवा को छोड कर )के लिए उपयुक्त  हो जाता है . [Self]
(defrule multiple2
(declare (salience 4902))
(id-root ?id multiple)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 technique)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  multiple.clp 	multiple2   "  ?id "  kaI  )" crlf))
)

;---------------- Default rules ----------------

(defrule multiple0
(declare (salience 5000))
(id-root ?id multiple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahu-))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  multiple.clp 	multiple0   "  ?id "  bahu- )" crlf))
)

(defrule multiple1
(declare (salience 4900))
(id-root ?id multiple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  multiple.clp 	multiple1   "  ?id "  bahuwa )" crlf))
)

;"multiple","Adj","1.bahuwa"
;The examiner has given multiple type of questions.
;
;
