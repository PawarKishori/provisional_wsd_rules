;##############################################################################
;#  Copyright (C) 2013-2014 Sonam Gupta(sonam27virgo@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-09 on (09-03-2015)
;Experimentally, it is found to obey the principle of superposition: the magnetic field of several sources is the vector addition of magnetic field of each individual source.	[NCERT CORPUS]	;Added by 14anu-ban-09 on (09-03-2015)
;अध्यारोपण का सिद्धान्त इस प्रकार है-बहुत से स्रोतों का चुम्बकीय क्षेत्र प्रत्येक व्यष्टिगत स्रोत के चुम्बकीय क्षेत्रों का सदिश योग होता है.	[NCERT CORPUS]	;Added by 14anu-ban-09 on (09-03-2015)

;प्रायोगिक रूप से, अध्यारोपण का सिद्धान्त पालन करने के लिए यह पाया जाता है : बहुत से स्रोतों का चुम्बकीय क्षेत्र प्रत्येक व्यष्टिगत स्रोत के चुम्बकीय क्षेत्रों का सदिश योग होता है. [self]	;Added by 14anu-ban-09 on (09-03-2015)
;@@@ Added by Sonam Gupta MTech IT Banasthali 14-3-2014
;A vector quantity is a quantity that has both a magnitude and a direction and obeys the triangle law of 
;addition or equivalently the parallelogram law of addition. [ncert]
;एक सदिश राशि वह राशि है जिसमें परिमाण तथा दिशा दोनों होते हैं तथा वह योग सम्बन्धी त्रिभुज के नियम अथवा समानान्तर चतुर्भुज के योग सम्बन्धी नियम का पालन करती है .
(defrule obey1
(declare (salience 5000))
(id-root ?id obey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 law|command|order|principle|command|rule)	;added 'principle|command|rule' by 14anu-ban-09 on (09-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAlana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obey.clp 	obey1   "  ?id "  pAlana_kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 14-3-2014
;He had always obeyed his parents without question. [oald]
;उसने सदैव बिना प्रश्न किये अपने माता पिता का अाज्ञापालन किया है .
(defrule obey2
(id-root ?id obey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AjFApAlana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obey.clp 	obey2   "  ?id "  AjFApAlana_kara )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 14-3-2014
;The road sign obeys left. [Cambridge]
;सड़क दायी तरफ की ओर इशारा करती है .
(defrule obey3
(id-root ?id obey)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AjFApAlana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obey.clp 	obey3   "  ?id "  AjFApAlana_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-08-2014)
;As mentioned in section 4.2, vectors, by definition, obey the triangle law or equivalently, the parallelogram law of addition. [NCERT CORPUS]
;जैसा कि खण्ड 4.2 में बतलाया जा चुका है कि सदिश योग के त्रिभुज नियम या समान्तर चतुर्भुज के योग के नियम का पालन करते हैं .

(defrule obey4
(id-root ?id obey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id2)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAlana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obey.clp 	obey4   "  ?id "  pAlana_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-11-2014)
;In 1864, the laws obeyed by electricity and magnetism were unified and formulated by James Maxwell who then realized that light was electromagnetic waves. [NCERT CORPUS]
;सन 1864 में विद्युत तथा चुम्बकत्व के सर्वमान्य नियमों को जेम्स मैक्सवेल ने एकीकृत करके नए नियम बनाए और यह स्पष्ट अनुभव किया कि प्रकाश वास्तव में विद्युत चुम्बकीय तरङ्गें हैं. [NCERT CORPUS]
;सन 1864 में, वो नियम जिसने विद्युत तथा चुम्बकत्व का पालन किया जेम्स मैक्सवेल द्वारा एकीकृत और प्रतिपादित होए जिसने फिर सिद्ध् किया कि प्रकाश में विद्युत चुम्बकीय तरङ्गें थी. [Self]

(defrule obey5
(declare (salience 5000))
(id-root ?id obey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 electricity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAlana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obey.clp 	obey5   "  ?id "  pAlana_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-03-2015)
;Rules are rules and they must be obeyed.	[oald.com]
;नियम नियम हैं और उनका पालन करना होगा . 		[self]

(defrule obey6
(declare (salience 5000))
(id-root ?id obey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(conjunction-components  ?id1 ?id2 ?id)
(id-root ?id1 and)
(id-root ?id2 rule)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAlana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obey.clp 	obey6   "  ?id "  pAlana_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-03-2015)
;NOTE-Parser problem. Run on parser no. 2.
;The soldiers refused to obey orders.  [oald]
;सैनिकों ने ओर्डर का आज्ञापालन करने से मना किया . [self]

(defrule obey7
(declare (salience 5001))
(id-root ?id obey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 soldier|teacher)
(kriyA-object  ?id ?id3)
(id-root ?id3 order|law)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AjFApAlana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obey.clp 	obey7   "  ?id "  AjFApAlana_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-03-2015)
;She was so tired her legs just wouldn't obey her any longer.   [oald]
;वह इतनी थकी हुई थी कि उसकी टाँगों के उसके अनुसार चलना बंद कर दिया . 	 [self]

(defrule obey8
(declare (salience 5000))
(id-root ?id obey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 leg)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anusAra_cala))
(assert (kriyA_id-subject_viBakwi ?id ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  obey.clp 	obey8   "  ?id "  anusAra_cala )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " obey.clp       obey8 "  ?id " ke )" crlf)
)
)

