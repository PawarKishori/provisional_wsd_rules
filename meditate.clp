;########################################################################

;#  Copyright (C) 2014-2015 14anu04 Archit Bansal(archit.bansal18@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################`

;$$$Modified by 14anu-ban-08 (07-01-2015)     ;modified in meaning 'socA-samaJA' to 'socA_samaJA'
;@@@ Added by 14anu04 0n 14-June-2014
;It was a pre meditated attack.
;यह एक सोचा-समझा हमला था . 
(defrule meditate_tmp
(declare (salience 200))
(id-root ?id meditate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 pre)
(test (=(- ?id 1) ?id1))
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 socA_samaJA))       ;modify meaning 'socA-samaJA' to 'socA_samaJA' by 14anu-ban-08 (07-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  meditate.clp     meditate_tmp   "  ?id "  " ?id1 "  socA_samaJA  )" crlf))
)

;@@@ Added by 14anu04 0n 14-June-2014
;Meditation is good for health.
;ध्यान स्वास्थ्य के लिए अच्छा है . 
(defrule meditate_noun
(declare (salience 100))
(id-root ?id meditate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  meditate.clp         meditate_noun   "  ?id "  XyAna )" crlf))
)

;@@@ Added by 14anu04 0n 14-June-2014
;They meditate daily in the morning. 
;वे सुबह प्रति दिन ध्यान करते हैं . 
(defrule meditate_verb
(declare (salience 100))
(id-root ?id meditate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  meditate.clp 	meditate_verb   "  ?id "  XyAna_kara )" crlf))
)
