;$$$ Modified by 14anu-ban-02(09-01-2015)   -------Meaning changed from 'gaMBIra_rUpa_se_pareSAna_kara' to 'pareSAna'
;She was assailed by doubts and regrets.
;वह सन्देह और पश्चाताप  से परेशान थी .(manual)
;@@@ Added by 14anu26  [20-06-14]
;She was assailed by doubts and regrets.
;वह सन्देह और पश्चाताप  द्वारा गम्भीर रूप से परेशान की गयी थी . 
(defrule assail2
(declare (salience 5000))
(id-root ?id assail)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 worry|doubt|fear|regret)               ;id-word changed to id-root by 14anu-ban-02(09-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  assail.clp 	assail2   "  ?id "  pareSAna )" crlf))
)


(defrule assail0
(declare (salience 0));reduce from 5000 to 0 by 14anu-ban-02(09-01-2015).unnecessary getting fire.
(id-root ?id assail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bOCAra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  assail.clp 	assail0   "  ?id "  bOCAra_ho )" crlf))
)

(defrule assail1
(declare (salience 4900))
(id-root ?id assail)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bOCAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  assail.clp 	assail1   "  ?id "  bOCAra_kara )" crlf))
)

;"assail","V","1.bOCAra_honA[karanA]"
;The minister was assailed with questions in the press conference.
;--"2.tUta_padanA"
;The army assailed on the enemy .
;The army assailed the enemy in retaliation to the bombing.
;
