
;$$$ Modified by 14anu-ban-09 on (19-11-2014)
;Note-Modified because of Connectivity missing error between ?id and ?id1.
;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;From an iron and steel plant where iron ore was The Crab Nebula. [Gyan nidhi corpus]
;एक लोहा और इस्पात कारखाने से जहाँ लोहे की खनिज मिट्टी क्रैब नेबुला तारा था . 
(defrule plant0
(declare (salience 5000))
(id-root ?id plant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1) ;added by 14anu-ban-09 on (19-11-2014)
(id-root ?id1 iron|steel|metal|nuclear|power)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAraKAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plant.clp 	plant0   "  ?id "  kAraKAnA )" crlf))
)

;$$$ Modified by 14anu21 on 28.06.2014 by changing the wsd_root_mng from vanaspawi to pOXA
;I pulled the plant up by the roots.[oxford]
;मैंने जडों से वनस्पति उखाडी . 
;मैंने जडों से पौधा उखाडा . 
(defrule plant1
(declare (salience 4900))
(id-root ?id plant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pOXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plant.clp 	plant1   "  ?id "  pOXA )" crlf))
)

;"plant","N","1.vanaspawi"
;Plants need light && water.
;--"2.saMyanwra"
;The new plant is decided to develop the tools for artisans.
;
(defrule plant2
(declare (salience 4800))
(id-root ?id plant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ropa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plant.clp 	plant2   "  ?id "  ropa )" crlf))
)

;"plant","V","1.ropanA"
;Each one plant one.
;--"2.basAnA"
;I planted my feet into the clay && walked over.
;
;LEVEL 
;
;
;"plant","N","1.vanaspawi"
;Plants need light && water.
;--"2.saMyanwra"
;The new plant is decided to develop the tools for artisans.
;
;"plant","V","1.ropanA"
;Each one plant one.
;--"2.basAnA"
;I planted my feet into the clay && walked over.
;
;--------------------------------------------------------
;
;sUwra : pOXA/saMyanwra[_ropaNa_karanA]
;
;yaha pOXe ke mUla se saMyanwra arWa meM AyA hE samBavawaH . kAraNa- pOXe ke saBI guNa
;saMyanwra meM A rahe hEM . xonoM kI xeKaBAla va xonoM kA ropA jAnA . inakA kriyArUpa
;ropanA yA basAnA( xonoM kriyAoM kA ekArWa ) hEM.
;
;

;@@@ Added by 14anu-ban-09 on (19-11-2014)
;Zero tillage is a fundamental management practice that promotes crop stubble retention under longer unplanned fallows when crops cannot be planted. [Agriculture]
;शून्य जुताई एक मूलभूत प्रबंधन प्रथा है जो लंबे समय तक अनियोजित जुताई के तहत चारा फसल को बढ़ावा देती है जब फसलें बोयी/बोई नहीं  जा सकतीं. [Manual]

(defrule plant3
(declare (salience 4800))
(id-root ?id plant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 crop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id boI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plant.clp 	plant3   "  ?id "  boI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-11-2014)
;The cover cropped cotton fields were planted to clover, which was left to grow in between cotton rows throughout the early cotton growing season (stripcover cropping). [Agriculture]
;कपास क्षेत्रो की झाडी वाली फसल में तिपतिया घास बोई जाती/गई थी, जिन्हें कपास की पंक्ति के चारों ओर श्रवण कपास के उपजने वाली/बढती/बढने वाले ॠतु में बढने के लिए छोड दिया गया था. [Self]
(defrule plant4
(declare (salience 4800))
(id-root ?id plant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 clover)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id boI_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plant.clp 	plant4   "  ?id "  boI_jA )" crlf))
)
