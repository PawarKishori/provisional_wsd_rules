;##############################################################################
;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;################################################################

;$$$ Modified by 14anu-ban-11 on (03-03-2015)
;We spent part of the time in the museum.  [oald.com]
;हमने सङ्ग्रहालय में कुछ समय बिताया.   [Self] 
;@@@  Added by Jagriti
; We spent the weekend in Paris.[oald]
;हमने पेरिस में सप्ताहांत बिताया.
;I've spent years trying to learn Japanese.[oald]
;मैंने जापानी सीखने के लिए सालों बिताए है.
;No, we'll spend the night at Chirbas.[gyanidhi-corpus]
;नहीं, हम चिरबास में रात बिताएँगे . 
(defrule spend0
(declare (salience 5000))
(id-root ?id spend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object ?id ?id1)(kriyA-kAlavAcI ?id ?id1))
(id-root ?id1 time|year|hour|week|month|minute|weekend|holiday|night|life|day|part) ;Added "part" by 14anu-ban-11 on (03-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spend.clp 	spend0 "  ?id " biwA )" crlf))
)
;@@@ Added by jagriti
; She spends a lot of money on clothes.[oald]
;उसने कपड़ों पर बहुत सारा पैसा खर्च किया.
(defrule spend1
(declare (salience 0));reduced to 0 by 14anu-ban-01.
(id-root ?id spend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Karca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spend.clp 	spend1 "  ?id " Karca_kara  )" crlf))
)

;@@@ Added by 14anu-ban-01 on 26-08-14
;It was around the same time as Galileo, a nobleman called Tycho Brahe (1546-1601) hailing from Denmark, spent his entire lifetime recording observations of the planets with the naked eye.[NCERT corpus]
;लगभग गैलीलियो के ही काल में डेनमार्क के एक कुलीन पुरुष टायको ब्रेह (1546 - 1601) ने अपना समस्त जीवन काल अपनी नङ्गी आङ्खों से सीधे ही ग्रहों के प्रेक्षणों का अभिलेखन करने में लगा दिया.[NCERT corpus]
;Parser failed to parse the above sentence correctly,so i have modified the sentence.
;Tycho Brahe spent his entire lifetime to study planets.
; टायको ब्रेह ने अपना समस्त जीवन काल  ग्रहों को पढ़ने में लगा दिया.
(defrule spend2
(declare (salience 4900))
(id-root ?id spend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 lifetime)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spend.clp 	spend2 "  ?id " lagA_xe  )" crlf))
)
