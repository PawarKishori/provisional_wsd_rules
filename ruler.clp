;@@@ Added by 14anu-ban-10 on (18-02-2015)
;A ruler is a flat piece of wood or plastic with marks on the side .[hinkhoj]
;एक  रूलर फ्लैट टुकड़ा है  लकड़ी या प्लास्टिक का  साथ  निशान के साथ पक्ष पर निशान के साथ लकड़ी या प्लास्टिक का एक फ्लैट टुकड़ा है।[manual]
(defrule ruler1
(declare (salience 200))
(id-root ?id ruler)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rulara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ruler.clp  	ruler1   "  ?id "  rulara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (18-02-2015)
;He was a good ruler.[hinkhoj]
;वह एक अच्छा मापक  था।[manual]
(defrule ruler2
(declare (salience 300))
(id-root ?id ruler)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1 )
(id-root ?id1 good)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ruler.clp  	ruler2   "  ?id "  mApaka)" crlf))
)

;------------------------ Default Rules ----------------------
;@@@ Added by 14anu-ban-10 on (18-02-2015)
;Hitler was a very strict ruler of his time.[hinkhoj]
;हिटलर उसके समय का एक अत्यन्त  सख्त शासक था . [manual]
(defrule ruler0
(declare (salience 100))
(id-root ?id ruler)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAsaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ruler.clp     ruler0   "  ?id "  SAsaka )" crlf))
)

