;@@@Added by 14anu-ban-02(06-04-2015)
;This ball is not very bouncy.[cald]
;यह गेंद अत्यन्त उछलने वाली नहीं है . [self]
(defrule bouncy1 
(declare (salience 100)) 
(id-word ?id bouncy) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 ball)
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id uCalane_vAlA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bouncy.clp  bouncy1  "  ?id "  uCalane_vAlA )" crlf)) 
) 

;---------------- Default_Rules ------------------------------------------------------
;@@@Added by 14anu-ban-02(06-04-2015)
;Sentence: He is always bouncy in the morning.[cald]
;Translation: 	वह सुबह हमेशा उत्साह से भरा रहता है . [self]
(defrule bouncy0 
(declare (salience 0)) 
(id-word ?id bouncy) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id uwsAha_se_BarA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bouncy.clp  bouncy0  "  ?id "  uwsAha_se_BarA )" crlf)) 
) 
