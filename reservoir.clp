;@@@ Added by 14anu-ban-10 on (22-08-2014) 
;A natural amusement park has been constructed together on reservoir , hill and plain field in Khandoli .[tourism corpus] ;added by 14anu-ban-10 on (11-11-2014)
;खंडोली  में  एक  साथ  जलाशय  ,  पहाड़ी  एवं  समतल  मैदान  पर  प्राकृतिक  एम्यूजमेंट  पार्क  का  निर्माण  किया  गया  है  । [tourism corpus] ; added by 14anu-ban-10on (11-11-2014)
(defrule reservoir0
(declare (salience 000))
(id-root ?id reservoir)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalASaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reservoir.clp  reservoir0 "  ?id " jalASaya )" crlf))
)

;@@@ Added by 14anu-ban-10 on (22-08-2014)       
;In the middle of high - low mountain peaks Taptapani is a reservoir of hot water for the power of destruction of skin - diseases .[tourism corpus] 
;ऊँचे-नीचे  पहाड़ी  शिखरों  के  बीच  चर्म-रोगों  के  विनाश  की  शक्ति  के  लिए  तप्तपानी  गरम  पानी  का  एक  कुंड  है  ।
;A reservoir is made in its floor .[tourism corpus] 
;इसके फर्श में एक कुण्ड बना हुआ है ।
(defrule reservoir1
(declare (salience 400))
(id-root ?id reservoir)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id3)
(id-root ?id3 water);added by 14anu-ban-10 on 11-11-14
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reservoir.clp 	reservoir1  "  ?id " kuNda )" crlf))
)

;@@@ Added by 14anu-ban-10 on (22-08-2014)      
;Here the Sun Temple of the 8th century and relics of the reservoir are seen .[tourism corpus] 
;यहाँ  पर  आठवीं  शताब्दी  का  सूर्य  मन्दिर  और  तालाब  के  अवशेष  दिखाई  पड़ते  हैं  ।
(defrule reservoir2
(declare (salience 500))
(id-root ?id reservoir)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-word ?id1 relics)     ;added by 14anu-ban-10 on (11-11-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaSeRa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reservoir.clp 	reservoir2  "  ?id " avaSeRa)" crlf))
)
