
;@@@Added by 14anu-ban-02(21-03-2015)
;Don’t be so beastly to him![oald]
;उसके प्रति इतना क्रूर मत होइए! [self]
(defrule beastly1 
(declare (salience 100)) 
(id-root ?id beastly) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 propN|noun|pronoun)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id krUra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  beastly.clp  beastly1  "  ?id "  krUra )" crlf)) 
) 

;-----------------------default_rules------------------------------------------------------------------

;@@@Added by 14anu-ban-02(21-03-2015)
;Sentence: This car’s been nothing but trouble—I wish I’d never bought the beastly thing![oald]
;Translation: यह गाड़ी सिर्फ परेशानी है,काश मैंने यह बेकार चीज़ कभी नहीं खरीदी होती![self]
(defrule beastly0 
(declare (salience 0)) 
(id-root ?id beastly) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id bekAra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  beastly.clp  beastly0  "  ?id "  bekAra )" crlf)) 
) 
