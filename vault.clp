
;"vaulted","Adj","1.meharAbI_Cawa_kA"
;We looked at the vaulted roof of the Notre Dame in Paris
(defrule vault0
(declare (salience 5000))
(id-root ?id vault)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id vaulted )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id meharAbI_Cawa_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  vault.clp  	vault0   "  ?id "  meharAbI_Cawa_kA )" crlf))
)

;@@@Added by 14anu-ban-07,(26-03-2015)
;Most of her jewellery is stored in bank vaults. (oald)
;उसके अधिकांश गहने   बैंक तिजोरियों में सञ्चित किया हुआ है .  (manual)
(defrule vault3
(declare (salience 5100))
(id-root ?id vault)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 store)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wijorI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vault.clp 	vault3   "  ?id "  wijorI )" crlf))
)

;@@@Added by 14anu-ban-07,(26-03-2015)
;She is to be buried in the family vault. (oald)
;उसे परिवार का शव-कक्ष में दफन किया जायेगा. (manual)
(defrule vault4
(declare (salience 5200))
(id-root ?id vault)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 bury)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sava-kakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vault.clp 	vault4   "  ?id "  Sava-kakRa )" crlf))
)

;@@@Added by 14anu-ban-07,(26-03-2015)
;Last week's changes vaulted the general to the top, over the heads of several of his seniors.(cambridge)
;पिछले सप्ताह के बदलावो ने जनरल को उसके कइ उच्चतर अधिकारियो से   सर्वोच्च स्थान पर,  छलाँग् लगवा दी. (manual)
(defrule vault5
(declare (salience 4900))
(id-root ?id vault)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CalAzga_lagavA_xe))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vault.clp 	vault5   "  ?id "  CalAzga_lagavA_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  vault.clp       vault5   "  ?id " ko )" crlf))
)

;------------------------ Default Rules ----------------------

;"vault","N","1.meharAbI_Cawa"
;Beautiful pictures are carved on the vault
(defrule vault1
(declare (salience 4900))
(id-root ?id vault)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meharAbI_Cawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vault.clp 	vault1   "  ?id "  meharAbI_Cawa )" crlf))
)

;"vault","VTI","1.lagge_ke_sahAre_kUxanA"
;The athelete vaulted over the pole
(defrule vault2
(declare (salience 4800))
(id-root ?id vault)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagge_ke_sahAre_kUxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vault.clp 	vault2   "  ?id "  lagge_ke_sahAre_kUxa )" crlf))
)



;"vault","N","1.meharAbI_Cawa"
;Beautiful pictures are carved on the vault
;--"2.wahaKAnA"
;Wine is stored in the vaults
;--"1.CalAzga"
;The clown took vault over the rope
;

