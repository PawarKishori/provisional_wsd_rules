;@@@ Added by 14anu-ban-06 (16-04-2015)
;He was attracted by the hothouse atmosphere of Britain's top schools. (cambridge)[parser no.- 2]
;वह ब्रिटेन के सर्वश्रेष्ठ विद्यालयों के उत्साह से भरे वातावरण के द्वारा आकर्षित हो गया था . (manual)
(defrule hothouse1
(declare (salience 2000))
(id-root ?id hothouse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 atmosphere)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsAha_se_BarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hothouse.clp 	hothouse1   "  ?id "  uwsAha_se_BarA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (16-04-2015)
;Hothouse flowers. (OALD)[parser no.- 2]
;कांच घर के फूल.(manual)
(defrule hothouse0
(declare (salience 0))
(id-root ?id hothouse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAMca_Gara_ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hothouse.clp 	hothouse0   "  ?id "  kAMca_Gara_ke )" crlf))
)
