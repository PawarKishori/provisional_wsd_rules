
;Added by Meena(17.6.10)
;According to Sarah they are not getting on very well at the moment . 
(defrule at_the_moment0
(declare (salience 500));salience decreased by 14anu20
(id-root ?id moment)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 2) at)
(id-word =(- ?id 1) the)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2) isa_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " moment.clp  at_the_moment0  "  ?id "  " (- ?id 1) " " (- ?id 2) "  isa_samaya  )" crlf))
)

;@@@ Added by 14anu20 on 23/06/2014.
;They doubt for a moment.
;वे कुछ पल के लिए सन्देह करते हैं .
(defrule moment1
(declare (salience 1500))
(id-root ?id moment)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 2) for)
(id-word =(- ?id 1) a|the)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1)  kuCa_pala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " moment.clp  moment1  "  ?id "  " (- ?id 1) "   kuCa_pala  )" crlf))
)
