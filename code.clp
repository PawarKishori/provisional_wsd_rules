;@@@ Added by 14anu-ban-03 (10-03-2015)
;The STD code for Hyderabad is 040. [hinkhoj]
;हैदराबाद के लिए एसटीडी कोड 040 है .  [manual]
(defrule code2
(declare (salience 100))  
(id-root ?id code)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 STD)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  code.clp 	code2   "  ?id "  koda )" crlf))
)


;@@@ Added by 14anu-ban-03 (10-03-2015)
;The Intelligence agency sends messages in codes. [hinkhoj]
;खुफिया एजेंसी गुप्त भाषाओं में सन्देश भेजती है .  [manual]
(defrule code3
(declare (salience 200))  
(id-root ?id code)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id1 ?id)
(id-root ?id1 message)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gupwa_BARA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  code.clp 	code3  "  ?id " gupwa_BARA )" crlf))
)

;@@@ Added by 14anu-ban-03 (10-03-2015)
;The cadets are suppose to follow a disciplinary code. [hinkhoj]
;कैडेटों से अपेक्षा की जाती है कि वह अनुशासनिक नियमावली का पालन करेंगे. [manual]   ;suggested by Chaitanya Sir
(defrule code4
(declare (salience 300))  
(id-root ?id code)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 disciplinary)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyamAvalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  code.clp 	code4  "  ?id " niyamAvalI )" crlf))
)

;@@@ Added by 14anu-ban-03 (10-03-2015)
;There was coded criticism of the government from some party members. [oald]
;कुछ पार्टी के सदस्यों द्वारा सरकार की कूट बद्ध आलोचना हुई थी। [manual]
(defrule code5
(declare (salience 100))   
(id-root ?id code)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 criticism)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kUta_baxXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  code.clp 	code5  "  ?id "  kUta_baxXa )" crlf))
)


;---------------Default Rules-----------

(defrule code0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (10-03-2015)
(id-root ?id code)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMhiwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  code.clp 	code0   "  ?id "  saMhiwA )" crlf))
)

;"code","V","1.kUta_saMkewa_xenA"
;The Intelligence agency coded the messages.
(defrule code1
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (10-03-2015)
(id-root ?id code)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kUta_saMkewa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  code.clp 	code1   "  ?id "  kUta_saMkewa_xe )" crlf))
)

