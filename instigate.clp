;@@@ Added by 14anu-ban-06  (11-02-2015)
;They were accused of instigating racial violence.(OALD)
;उन पर प्रजातीय हिंसा भडकाने के आरोप लगाए गये थे . (manual)
(defrule instigate0
(declare (salience 0))
(id-root ?id instigate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BadakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  instigate.clp 	instigate0   "  ?id "  BadakA )" crlf))
)
;@@@ Added by 14anu-ban-06  (11-02-2015)
;The government has instigated a programme of economic reform. (OALD)
;सरकार ने अर्थशास्त्रीय सुधार का कार्यक्रम प्रारम्भ किया. (manual)
(defrule instigate1
(declare (salience 2000))
(id-root ?id instigate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 programme)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAramBa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  instigate.clp 	instigate1   "  ?id "  prAramBa_kara )" crlf))
)

