
(defrule suppose0
(declare (salience 5000))
(id-root ?id suppose)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id supposing)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id yaxi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  suppose.clp  	suppose0   "  ?id "  yaxi )" crlf))
)

;@@@ Added by Shirisha Manju Suggested by Soma Mam (10-5-17)
;You are not supposed to behave like this!
(defrule suppose00
(declare (salience 4910))
(id-root ?id suppose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-tam_type ?id passive)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cAhie))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  suppose.clp  suppose00   "  ?id "  cAhie )" crlf))
)

;Supposing there was a war, what will you do? -- Vaishnavi
(defrule suppose1
(declare (salience 4900))
(id-root ?id suppose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suppose.clp 	suppose1   "  ?id "  mAna_le )" crlf))
)

;"suppose","V","1.mAna_lenA"
;What makes you support I'am against it.
;


;@@@ Added by 14anu-ban-01 on (30-10-2014)
;Suppose two physical quantities A and B have measured values A ± ΔA, B ± ΔB respectively where ΔA and ΔB are their absolute errors.[NCERT corpus]
;मान लीजिए, कि दो भौतिक राशियों A एवं B के मापित मान क्रमशः A ± ΔA, B ± ΔB हैं ; जहाँ, ΔA एवं ΔB क्रमशः इन राशियों की निरपेक्ष त्रुटियाँ हैं.[NCERT corpus]
;Suppose a length is reported to be 4.700 m.[NCERT corpus]
;मान लीजिए किसी वस्तु की लम्बाई 4.700 m लिखी गई है.[NCERT corpus]
;Suppose the additional weight required is W.[NCERT corpus]
;मान लीजिए आवश्यक अतिरिक्त भार है.[NCERT corpus]
;Suppose we have two vectors A and B.[NCERT corpus]
;मान लीजिए हमारे पास दो सदिश A व B हैं.[NCERT corpus]
(defrule suppose2
(declare (salience 4900))
(id-word 1 suppose)
?mng <-(meaning_to_be_decided 1)
(id-cat_coarse 1 verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng 1 mAna_lIjie))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  suppose.clp 	suppose2   1  mAna_lIjie )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-11-2014)
;Now suppose we change units, then 4.700 m = 470.0 cm = 4700 mm = 0.004700 km Since the last number has trailing zero (s) in a number with no decimal, we would conclude erroneously from observation (1) above that the number has two significant figures, while in fact, it has four significant figures and a mere change of units can not change the number of significant figures.[NCERT corpus]
;अब मान लीजिए हम अपना मात्रक बदल लेते हैं तो 4.700 m = 470.0 cm = 0.004700 km = 4700 mm क्योंकि, अन्तिम सङ्ख्या में दो शून्य, बिना दशमलव वाली सङ्ख्या में अनुगामी शून्य हैं, अतः प्रेक्षण (1) के अनुसार हम इस गलत निष्कर्ष पर पहुँच सकते हैं कि इस सङ्ख्या में 2 सार्थक अङ्क हैं जबकि वास्तव में इसमें चार सार्थक अङ्क हैं, मात्र मात्रकों के परिवर्तन से सार्थक अङ्कों की सङ्ख्या में परिवर्तन नहीं होता.[NCERT corpus]
(defrule suppose3
(declare (salience 4900))
(id-word ?id suppose)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 now)
(test (< ?id1 ?id))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mAna_lIjie))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  suppose.clp 	suppose3   "  ?id "  mAna_lIjie )" crlf))
)
