;@@@Added by 14anu-ban-02(13-03-2015)
;Sentence: As part of the centenary celebrations a chain of beacons was lit across the region.[cald]
;Translation:एक कर्तव्य  के रूप में शतवर्षीय उत्सव में  आकाशदीप की लड़ी पूरे प्रान्त में जलाई गयी थी.[self]
(defrule beacon0 
(declare (salience 0)) 
(id-root ?id beacon) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id AkASaxIpa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  beacon.clp  beacon0  "  ?id "  AkASaxIpa )" crlf)) 
) 


;@@@Added by 14anu-ban-02(13-03-2015)
;He was a beacon of hope for the younger generation.[oald]
;वह युवा पीढ़ी के लिये आशा की  किरण था . [self]
(defrule beacon1 
(declare (salience 100)) 
(id-root ?id beacon) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 hope)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kiraNa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  beacon.clp  beacon1  "  ?id "   kiraNa )" crlf)) 
) 
