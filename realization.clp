;@@@ Added by 14anu-ban-10 on (26-8-14)
;After disappearance of the boy the public felt the divine realization there.[tourism corpus]
;ladake ke oJala ke bAxa janawA ne vahAz para xivya prawyakRIkaraNa mahasUsa kiyA .
(defrule realization0
(declare (salience 0000))
(id-root ?id realization)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawyakRIkaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization0 "  ?id "  prawyakRIkaraNa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (26-8-14)
;On seeing the tall and beautiful trees and plants your heart will fill with the realization of completeness .[parallel corpus]
;लंबे और खूबसूरत पेड़-पौधों को देखकर आपका दिल पूर्णता के अहसास से भर जाएगा ।
(defrule realization1
(declare (salience 200))
(id-root ?id realization)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI ?id ?id1)(viSeRya-viSeRaNa ?id ?id2))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ahasAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization1 "  ?id "  ahasAsa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (26-8-14)
;I'd been conducting for 20 years , and I suddenly had a realization .[parallel corpus]
;मैं २० साल से संगीत निर्देशक था , और अचानक मुझे एक अनुभूति हुई .
(defrule realization2
(declare (salience 400))
(id-root ?id realization)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 have)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBUwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization2 "  ?id "  anuBUwi )" crlf))
)

;@@@ Added by 14anu-ban-10 on (26-8-14)
;Self realization - boys and girls dot com online paper.[parallel corpus]
;अनुनाद - चौरीचौरा डॉट काम की ऑन्लाइन पत्रिका
(defrule realization3
(declare (salience 600))
(id-root ?id realization3)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa ?id ?id1)(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id2))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anunAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization3 "  ?id "  anunAxa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (26-8-14)
;Likewise , the Upanishads declare that soul - realization is born of the knowledge of the soul atma - vidya .[parallel corpus]
;इसी तरह उपनिषद् यह घोषणा करती है कि आत़्मा - विद्या से ही आत़्मज्ञान की प्राप़्ति होती है .
(defrule realization4
(declare (salience 800))
(id-root ?id realization)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject ?id1 ?id)(kriyA-karma ?id1 ?id))
(id-root ?id1 born)
(id-cat_coarse ?id noun)
=>
(retract ?mng)

(assert (id-wsd_root_mng ?id viGA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization4 "  ?id "  viGA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (26-8-14)
;The imaginations of'Brahma'is the central point of self realization and is a great offering of the Hindu religion to the world .[parallel corpus]
;ब्रह्म की परिकल्पना वेदान्त दर्शन का केन्द्रीय स्तम्भ है और हिन्दू धर्म की विश्व को अनुपम देन है ।
(defrule realization5
(declare (salience 1000))
(id-root ?id realization)
?mng <-(meaning_to_be_decided ?id)
(or(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)(viSeRya-of_saMbanXI ?id2 ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swamBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization5 "  ?id "  swamBa )" crlf))
)

;@@@ Added by 14an-10 on (26-8-14)
;With the blessings of Paramhansji , he attained self realization and the light of consciousness and became the leader among all the students of Paramhansji .[parallel corpus]
;परमहंसजी की कृपा से इनको आत्म - साक्षात्कार हुआ फलस्वरूप नरेंद्र परमहंसजी के शिष्यों में प्रमुख हो गए ।
(defrule realization6
(declare (salience 3000))
(id-root ?id realization)
?mng <-(meaning_to_be_decided ?id)
(or(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)(kriyA-object ?id ?id2))
(id-root ?id2 attain)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAkRAwkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization6 "  ?id "  sAkRAwkAra )" crlf))
)

;@@@ Added by 14anu-ban-10 on (26-8-14)
;Swami Vivekanand January 12 , 1863 - July 4 , 1902 was a famous and popular Self - realization Teacher in Indian philosophy .[parallel corpus]
;स्वामी विवेकानन्द १२ जनवरी १८६३ - ४ जुलाई १९०२ वेदान्त के विख्यात और प्रभावशाली आध्यात्मिक गुरु थे ।
(defrule realization7
(declare (salience 4000))
(id-root ?id realization)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAvaSAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization7 "  ?id "  praBAvaSAlI )" crlf))
)

;@@@ Added by 14anu-ban-10 on (26-8-14)
;that realization , that recognition .[parallel corpus]
;वह बोध , वह स्वीकृति
(defrule realization8
(declare (salience 5000))
(id-root ?id realization)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-det_viSeRaNa ?id ?)(saMjFA-saMjFA_samAnAXikaraNa ?id ?))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id boXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realization.clp 	 realization8 "  ?id " boXa )" crlf))
)

