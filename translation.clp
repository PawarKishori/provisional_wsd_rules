;@@@ Added by 14anu-ban-07, 19-08-2014
;An error in translation.(oald)
;अनुवाद में एक गलती . 
(defrule translation0
(id-root ?id translation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuvAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   translation.clp 	 translation0   "  ?id "  anuvAxa )" crlf))
)

;@@@ Added by 14anu-ban-07, 28-08-2014
;In one case, Fig. 7.6(a), the motion is a pure translation; in the other case Fig. 7.6(b) it is a combination of translation and rotation. 
;चित्र 7.6(a) में दर्शाई गई गति शुद्ध स्थानान्तरीय है, जबकि चित्र 7.6(b) में दर्शाई गई गति स्थानान्तरण एवं घूर्णी दोनों प्रकार की गतियों का संयोजन है. 
(defrule translation1
(declare (salience 1000))
(id-root ?id translation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-viSeRaNa  ?id ?)(subject-subject_samAnAXikaraNa  ?id1 ?id)(viSeRya-of_saMbanXI  ?id2 ?id))
(id-root ?id1 motion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAnAMwaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   translation.clp 	 translation1   "  ?id "  sWAnAMwaraNa )" crlf))
)


