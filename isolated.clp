;@@@ Added by 14anu-ban-06 (20-02-2015)
;Moving to this beautiful but isolated place was not easy for Zanap.(COCA)
;इस सुन्दर परन्तु एकान्त स्थान पर जाना ज़ानाप् के लिए आसान नहीं था . (manual)
(defrule isolated0
(declare (salience 0))
(id-root ?id isolated)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  isolated.clp 	isolated0   "  ?id "  ekAnwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (20-02-2015)
;There were only a few isolated cases of violent behaviour.(cambridge)
;हिंसक व्यवहार की सिर्फ कुछ छिट-पुट घटनाएँ थी . (manual)
(defrule isolated1
(declare (salience 2000))
(id-root ?id isolated)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-of_saMbanXI ?id1 ?)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 case|incident)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cita-puta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  isolated.clp 	isolated1   "  ?id "  Cita-puta )" crlf))
)
