(defrule long0
(declare (salience 5000))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id longing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lAlasA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  long.clp  	long0   "  ?id "  lAlasA )" crlf))
)

;"longing","N","1.lAlasA"
;He died with a longing to see his estranged son.
;

;$$$ Modified by Bhagyashri Kulkarni (26-10-2016)
;For how long have you been here?(rapidex)
;आप यहाँ कितने समय तक के लिए रह चुके हैं?
;Modified by Meena(25.9.09),added (kriyA-for_saMbanXI ....) and commented (id-cat_coarse ?id adj...) and (id-cat_coarse =(+ ?id 1)....)
;I did not wait for long.
(defrule long1
(declare (salience 4900))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective )
;(id-cat_coarse =(+ ?id 1) ~noun)
(kriyA-for_saMbanXI ?id1 ?id)
(not (id-word =(- ?id 1) how)) ;added  by Bhagyashri.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambA_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long1   "  ?id "  lambA_samaya )" crlf))
)





(defrule long2
(declare (salience 4800))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laMbA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long2   "  ?id "  laMbA )" crlf))
)

;"long","Adj","1.lambA"
;A long stick. A long road. A long poem. A long time.


;$$$ Modified by Bhagyashri Kulkarni (26-10-2016)
;How long will it last ?(rapidex)
;वह कितना समय चलता रहेगा? 
;How long will it take?(rapidex)
;वह कितना समय लेगा?
;For how long have you been here?(rapidex)
;आप यहाँ कितने समय के लिए रह चुके हैं?
;Added by  Meena(20.3.10)
;How long will it last ? 
(defrule long3
(declare (salience 4800)) ;salience increased by Bhagyashri from '4700' to '4800'
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-object ?id1 ?id) ;commented by Bhagyashri
;(id-root ?id1 last) ;commented by Bhagyashri
(id-cat ?id adjective) ;added by Bhagyashri
(viSeRya-viSeRaka ?id ?id1)  ;added by Bhagyashri
(id-root ?id1 how) ;added by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp      long3   "  ?id "  samaya )" crlf))
)



;$$$ Modified by Bhagyashri Kulkarni (26-10-2016)
;For how long have you been here? (rapidex)
;आप यहाँ कितने समय तक के लिए रह चुके हैं? 
;Salience reduced by Meena(20.3.10)
(defrule long4
(declare (salience 0))
;(declare (salience 4700))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaya_waka)) ;meaning changed from 'lambe_samaya_waka' to 'samaya_waka' by Bhagyashri
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long4   "  ?id "  samaya_waka )" crlf))
)

;"long","Adv","1.lambe_samaya_waka"
;Stay here as long as you like.
;
(defrule long5
(declare (salience 4800))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambA_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long5   "  ?id "  lambA_samaya )" crlf))
)

;"long","N","1.lambA_samaya"
;It shouldn't take long for you to solve the problem.
;
(defrule long6
(declare (salience 4500))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id icCA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long6   "  ?id "  icCA_kara )" crlf))
)

;"long","V","1.icCA_karanA"
;I long to be there for the rest of my life.
;

;@@@ Added by Nandini(1-1-14)
;The milkman's long face grew longer.
(defrule long7
(declare (salience 4850))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?kri ?id1)
(id-root ?kri grow)
(or (subject-subject_samAnAXikaraNa ?id1 ?id) (kriyA-kriyA_viSeRaNa  ?kri ?id))
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika_laMbA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long7   "  ?id "  aXika_laMbA )" crlf))
)

;$$$ Modified by 14anu-ban-08 (16-01-2015)       ;constraint added
;@@@ Added by 14anu05 GURLEEN BHAKNA on 24.06.14
;I think the contest is only three weeks long.
;मैं सोचता हूँ कि प्रतियोगिता सिर्फ तीन सप्ताह लम्बी है .
(defrule long11
(declare (salience 5500))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 week|year|month|hour|minute|second|day|metre)    ;added 'metre' by 14anu-ban-08 (16-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long11   "  ?id "  lambA )" crlf))
)

;Remove rule because long11 and long12 have same condition, so long12 is merge in long11 by 14anu-ban-08 (16-01-2015)
;@@@ Added by 14anu11
;The vadyam is very simple and is made of a bow , nearly two to three meters long 
;वाद्यम् बहुत सादा है , और यह प्राय : दो से तीन मीटर लंबे धनुष से बना होता है .
;(defrule long12
;(declare (salience 4700))
;(id-root ?id long)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adverb)
;(viSeRya-viSeRaNa  ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id laMbe))     
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long12   "  ?id "  laMbe )" crlf))
;)

;@@@Added by 14anu-ban-08 (16-02-2015)
;Her face was disfigured by a long red scar.       [olad]
;उसका चेहरा अधिक लाल दाग से खराब हो गया था .   [self]
;One reason is that the cricketer allows a longer time for his hands to stop the ball.  [NCERT]
;इसका एक कारण यह है कि अभ्यस्त खिलाडी, अपने हाथों से गेंद को लपक कर, उसे रोकने में अधिक समय लगाता है .  [NCERT]
(defrule long13
(declare (salience 4801))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 time|scar)         ;added 'scar' by 14anu-ban-08 (18-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long13   "  ?id "  aXika )" crlf))
)

;@@@Added by 14anu-ban-08 (26-02-2015)         ;Run on parser 28
;The island has long golden beaches fringed by palm trees.    [cald]
;द्वीप  पर बड़े सुनहरे समुद्रतट के किनारे ताड़ के पेड़ लगे है .     [self]
(defrule long14
(declare (salience 4801))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 beach)
=>
(retract ?mng)                          
(assert (id-wsd_root_mng ?id badZA))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long14   "  ?id "  badZA )" crlf))
)

;@@@Added by 14anu-ban-08 (12-03-2015) 
;Long time ago very cruel king name Virat Singh was ruling the city of Vijay Nagar. [Set7-76-77pdf]
;बहुत साल पहले अत्यन्त निर्दयी राजा नाम विराट सिंह विजय नगर के शहर पर शासन करता था. [self]
(defrule long15
(declare (salience 4801))
(id-root ?id long)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 time)
=>
(retract ?mng)                          
(assert (id-wsd_root_mng ?id bahuwa))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  long.clp 	long15   "  ?id "  bahuwa )" crlf))
)
