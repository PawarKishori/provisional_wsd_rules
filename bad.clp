
(defrule bad0
(declare (salience 5000))
(id-root ?id bad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KZarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bad.clp 	bad0   "  ?id "  KZarAba )" crlf))
)

(defrule bad1
(declare (salience 4900))
(id-root ?id bad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bad.clp 	bad1   "  ?id "  KarAba )" crlf))
)

;"bad","Adj","1.KarAba"
;Foreigners' pronunciation of Hindi is mostly bad.
;vixeSiyoM kA hiMxI uccAraNa aXikawara burA howA hE.
;Cricketers can't play if the light is bad.
;yaxi bawwI KarAba ho wo kriketarsa nahIM Kela sakawehEM.
;--"2.burA"
;He is going through a bad period.
;vaha bure xOra se gujZara rahA hE.
;--"3.gamBIra"
;Ravi's accident was quite a bad one.
;ravi kI xurGatanA kAPI gamBIra WI.
;
;

;$$$Modified by 14anu-ban-02(12-01-2015)
;###[counter statement] Some other costumes were really bad.[sd_verified]
;###[counter statement]कुछ अन्य पहनावे वास्तव में खराब थे .[self]
;###[counter statement]The food is bad.[discussion]
;###[counter statement]आहार खराब है.[self]
;Totally different environment  , if the climber does not find his favourite food after the tiredness exerted through continuous work then he feels even worse  .(tourism corpus)
;एकदम भिन्न वातावरण , निरंतर काम करने से आने वाली थकावट के बाद जब मनपंसद खाना नहीं मिलता तो आरोही को और भी बुरा लगता है ।
;Meaning changed from 'burA' to 'Ora_burA' by 14anu-ban-02(12-01-2015)
;@@@ Added by 14anu-ban-11 on 8.9.14
;Totally different environment  , if the climber does not find his favourite food after the tiredness exerted through continuous work then he feels even worse  .(tourism corpus)
;एकदम भिन्न वातावरण , निरंतर काम करने से आने वाली थकावट के बाद जब मनपंसद खाना नहीं मिलता तो आरोही को और भी बुरा लगता है ।
(defrule bad2
(declare (salience 4950))             ;salience reduce by 14anu-ban-02(12-01-2015)
(id-root ?id bad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
;(subject-subject_samAnAXikaraNa ?id1 ?id)	;commented by 14anu-ban-02(28-02-2015)
(viSeRya-viSeRaka  ?id ?id1)	;added by 14anu-ban-02(28-02-2015)
(id-root ?id1 even)	;added by 14anu-ban-02(05-02-2016)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ora_burA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bad.clp 	bad2   "  ?id "  Ora_burA )" crlf))
)

;$$$Modified by 14anu-ban-02(12-01-2015)
;@@@Added by 14anu24 on 4th July 2014
;But this cure is worse than the disease .
;लेकिन यह इलाज तो बीमारी से भी घातक है 
(defrule bad3
(declare (salience 5000))
(id-root ?id bad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-than_saMbanXI  ?id ?id1)
(id-root ?id1 disease)                 ;added by 14anu-ban-02(12-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GAwaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bad.clp      bad3   "  ?id "  GAwaka )" crlf))
)


;"bad","Adj","1.KarAba"
;Foreigners' pronunciation of Hindi is mostly bad.
;vixeSiyoM kA hiMxI uccAraNa aXikawara burA howA hE.
;Cricketers can't play if the light is bad.
;yaxi bawwI KarAba ho wo kriketarsa nahIM Kela sakawehEM.
;--"2.burA"
;He is going through a bad period.
;vaha bure xOra se gujZara rahA hE.
;--"3.gamBIra"
;Ravi's accident was quite a bad
