

;@@@ Added by 14anu04 on 21-June-2014
;I'd like to give a toast to the bride and groom. 
;मैं नववधू और सईस को शुभकामना देना पसन्द करूँगा.  
(defrule toast_tmp2
(declare (salience 5500))
(id-root ?id toast)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 to)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SuBakAmanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  toast.clp 	toast_tmp2   "  ?id " SuBakAmanA )" crlf))
)


;@@@ Added by 14anu04 on 21-June-2014
;The performance made her the toast of the festival. 
;प्रदर्शन ने उसको त्यौहार का आकर्षण बनाया. 
(defrule toast_tmp
(declare (salience 5500))
(id-root ?id toast)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 of)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  toast.clp 	toast_tmp   "  ?id " AkarRaNa )" crlf))
)

;$$$Modified by 14anu-ban-07,(10-01-2015)
;@@@ Added by 14anu23 14/06/2014		
;Happy families toasting each other’s health
;एक दूसरे के स्वास्थ्य या शुभकामना के लिए शराब पीता हुआ  सुखी परिवार.
(defrule toast3
(declare (salience 5000)) ;salience increased from 4900 by 14anu-ban-07
(id-root ?id toast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 health)     ;added by 14anu-ban-07 on(10-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAsWya_yA_SuBakAmanA_ke_lie_SarAba_pInA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  toast.clp 	toast3   "  ?id "  svAsWya_yA_SuBakAmanA_ke_lie_SarAba_pInA )" crlf))
)

;@@@ Added by 14anu-ban-07,(09-09-2014)
;The toasted almonds enhance the grains nuttiness and lend a satisfying crunch.(AnusaarakaGoogle_Group_1_3Jul2014)
;भुने हुए बादाम बढ़ाते हैं अनाज़ की नटीनैस और देता हैं एक सन्तोषजनक चर्वण.(manually)
(defrule toast4
(declare (salience 5000))
(id-root ?id toast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 almond|groundnut|nut|cashew|chickpea|gram)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Buna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  toast.clp 	toast4   "  ?id "  Buna )" crlf))
)

;---------------------- Default rules ----------------------
(defrule toast0
(declare (salience 5000))
(id-root ?id toast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tosta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  toast.clp 	toast0   "  ?id "  tosta )" crlf))
)

;"toast","N","1.tosta{sikI_breda}"
;Rama prepared toasts for breakfast.
;--"2.tosta{kisI_ke_svAsWya_yA_SuBakAmanAoM_ke_liye_SarAba_pIne_kI_kriyA}"
;They all proposed a toast for her birthday.
;
(defrule toast1
(declare (salience 4900))
(id-root ?id toast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  toast.clp 	toast1   "  ?id "  sika )" crlf))
)

;"toast","VI","1.sikanA"
;Standing in the hot sun for full two hours she was toasted completely.
;
(defrule toast2
(declare (salience 4800))
(id-root ?id toast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  toast.clp 	toast2   "  ?id "  BUna )" crlf))
)

;"toast","VT","1.BUna/seMka"
;She always toasts bread for breakfast.
;--"2.svAsWya_yA_SuBakAmanA_ke_lie_SarAba_pInA"
;Let us toast the birthday girl!.
;
