
(defrule wise0
(declare (salience 5000))
(id-root ?id wise)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAvaXAna_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " wise.clp wise0 " ?id "  sAvaXAna_kara )" crlf)) 
)

(defrule wise1
(declare (salience 4900))
(id-root ?id wise)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sAvaXAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wise.clp	wise1  "  ?id "  " ?id1 "  sAvaXAna_kara  )" crlf))
)

;@@@ Added by shirisha Manju 6-10-16 Suggested by Rajini
;But in case of short term job oriented courses, the duration may vary institute wise.
;paranwu TiganA vyavasAyonmuKa pATyakramoM kI xaSA meM, avaXi saMsWA ke anusAra parivarwiwa_ho sakawI hE. (anu translation)
(defrule wise4
(declare (salience 1000))
(id-root ?id wise)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) institute|company|bank|group|class|instrument|item|variety)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_anusAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wise.clp     wise4   "  ?id "  ke_anusAra )" crlf))
)

;--------------------------- Default rules --------------------------------------------

;"wise","Adj","1.jFAnI"
;Wise people think before they act.
(defrule wise2
(declare (salience 800)) ;salience reduced from 4800 to 800
(id-root ?id wise)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jFAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wise.clp 	wise2   "  ?id "  jFAnI )" crlf))
)

;"wise","N","1.rIwi"
;This curtain should be put lengthwise.
(defrule wise3
(declare (salience 800)) ;salience reduced from 4700 to 800
(id-root ?id wise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rIwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wise.clp 	wise3   "  ?id "  rIwi )" crlf))
)

