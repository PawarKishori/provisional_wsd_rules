
;@@@ Added by 14anu-ban-05 on (03-03-2015)
;How a supremely powerful fort finally became the victim of its own pride , inaction and illusion and how it went into the hands of an indifferent and selfish company and later the British rulers .[TOURISM]
;kEse eka sarvocca SakZwiSAlI kilA aMwawaH apane hI ahaMkAra , niRZkriyawA waWA Brama kA SikAra huA Ora kEse yaha uxAsIna evaM svArWI kaMpanI ke waWA bAxa meM aMgreja SAsakoM ke hAWoM meM calA gayA .[TOURISM]
(defrule finally0
(declare (salience 100))
(id-root ?id finally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwawaH))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finally.clp 	finally0   "  ?id "  aMwawaH )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-03-2015)
;Finally the lion would tear the slave into pieces and the audience used to dance with happiness .[TOURISM]
;AKirakAra Sera gulAma ke ciWadZe - ciWadZe kara xewA WA Ora xarSaka KuSI se nAcane lagawe We .	[TOURISM]
;Finally one day I departed for Putlikhoh with that friend of mine .[TOURISM]
;AKirakAra eka xina apane usI miwra ko sAWa lekara mEM puwalIKoha ke lie nikala padZA .[MANUAL]
(defrule finally1
(declare (salience 101))
(id-root ?id finally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 tear|depart|accomplish|find)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AKirakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finally.clp 	finally1   "  ?id "  AKirakAra )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-03-2015)
;That lady took me around and finally showed me the hall where all people read Namaz prayers sitting side by side .[TOURISM]
;usa mahilA ne muJe cAroM ora GumAyA Ora AKira meM vaha hoYla xiKAyA jahAz saba loga kaMXe se kaMXA milAkara namAja paDZawe hEM .[TOURISM]
(defrule finally2
(declare (salience 101))
(id-root ?id finally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 show)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AKira))
(assert  (id-wsd_viBakwi   ?id  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finally.clp 	finally2   "  ?id "  AKira )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  finally.clp 	finally2   "  ?id " meM )" crlf))
)

