;@@@ Added by 14anu-ban-10 on (21-08-2014)
;If the load is increased further, the stress developed exceeds the yield strength and strain increases rapidly even for a small change in the stress.  [ncert corpus]
;yaxi BAra ko Ora baDZA xiyA jAe wo uwpanna prawibala parABava sAmarWya se aXika ho jAwA hE Ora Pira prawibala meM WodZe se aMwara ke lie BI vikqwi wejZI se baDZawI hE.[ncert corpus]
(defrule rapidly0
(declare (salience 0000))
(id-root ?id rapidly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapidly.clp 	rapidly0   "  ?id "  wejI_se )" crlf))
)

;@@@ Added by 14anu-ban-10 on (21-08-2014)
;In a turbulent flow the velocity of the fluids at any point in space varies rapidly and randomly with time. [ncert corpus]  
;vikRubXa pravAha meM kisI biMxu para warala kA vega xruwa waWA yAxqcCika rUpa se samaya meM baxalawA rahawA hE.[ncert corpus] 
(defrule rapidly1
(declare (salience 200))
(id-root ?id rapidly)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id1 ?id) 
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xruwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapidly.clp 	rapidly1   "  ?id "  xruwa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-09-2014)
;The greater the value of K for a material, the more rapidly will it conduct heat.  
;kisI paxArWa ke lie @K kA mAna jiwanA aXika howA hE uwanI hI SIGrawA se vaha URmA cAlana karawA hE.
(defrule rapidly2
(declare (salience 400))
(id-root ?id rapidly)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ? ) 
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIGrawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapidly.clp 	rapidly2   "  ?id "  SIGrawA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (12-12-2014) 
;The world's resources are rapidly diminishing. [By mail]
;विश्व के संसाधन शीघ्रता से कम हो रहे हैं ।
(defrule rapidly3
(declare (salience 4800))
(id-root ?id rapidly)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ? ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIGrawA_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rapidly.clp 	rapidly3   "  ?id "  SIGrawA_se )" crlf))
)
