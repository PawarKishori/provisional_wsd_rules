;@@@Added by 14anu-ban-02(26-02-2015)
;That dress is really ageing on her.[collins]   ;run this sentence on parser no.4
;उस लिबास में वह वास्तव में अधिक उम्र की लगती है.[self]
(defrule ageing2
(declare (salience 100))
(id-word ?id ageing)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-on_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aXika_umra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  ageing.clp  ageing2  "  ?id " aXika_umra)" crlf))
)


;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(26-02-2015)
;Sentence: Signs of ageing.[oald] ;run on parser no.2
;Translation: बुढ़ापे के लक्षण.[self]
(defrule ageing0 
(declare (salience 0)) 
(id-root ?id age) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id buDZApA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ageing.clp  ageing0  "  ?id "  buDZApA )" crlf)) 
)

;@@@Added by 14anu-ban-02(26-02-2015)
;The sullen looks, the pain,the scars on older women's sagging breasts and aging men's sunken chests, are haunting.[coca] 
;The sullen looks, the pain,the scars on older women's sagging breasts and ageing men's sunken chests, are haunting.[modified spelling of aging as ageing]
;वे उदास निगाहें, दर्द,बूढ़ी स्त्रियों के ढीली-ढाली छातियों एवं बूढ़े पुरुषों की  धँसी हुई/सिकुडी हुई छातियों पर पड़े हुए दाग/निशान  बार बार याद आते हैं. [self]
(defrule ageing1 
(declare (salience 0)) 
(id-word ?id ageing) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id bUDZA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  ageing.clp  ageing1  "  ?id "  bUDZA)" crlf)) 
)  

