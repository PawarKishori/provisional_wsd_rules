;@@@Added by 14anu-ban-02(05-03-2015)
;When will I see you again?[oald]
;मैं आपसे फिर कब मिलूँगा? [self]
(defrule again1 
(declare (salience 100)) 
(id-root ?id again) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-aXikaraNavAcI  ?id1 ?id)
(id-root ?id1 see)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id Pira)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  again.clp  again1  "  ?id "  Pira )" crlf)) 
) 

;@@@Added by 14anu-ban-02(05-03-2015)
;Just as static charges produce an electric field, the currents or moving charges produce (in addition) a magnetic field, denoted by B(r), again a vector field.[ncert12_04]
;जिस प्रकार स्थिर आवेश विद्युत क्षेत्र उत्पन्न करते हैं, विद्युत धाराएँ अथवा गतिमान आवेश (विद्युत क्षेत्र के साथ-साथ) चुम्बकीय क्षेत्र उत्पन्न करते हैं जिसे B(r) द्वारा निर्दिष्ट किया जाता है तथा यह भी एक सदिश क्षेत्र है.[ncert]
(defrule again2 
(declare (salience 100)) 
(id-root ?id again) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaka  ?id1 ?id)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id BI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  again.clp  again2  "  ?id "  BI )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(05-03-2015)
;Sentence: Could you say it again, please?[oald]
;Translation: कृपया,क्या आप फिर से  यह कह सकेंगे?[self] 
(defrule again0
(declare (salience 0))
(id-root ?id again)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pira_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  again.clp  again0  "  ?id "  Pira_se )" crlf))
)

