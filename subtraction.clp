;@@@ Added by 14anu-ban-01 on 31-8-14.
;Our brain as well as computers also can do the actions like multiplication , division , addition and subtraction .
;गुणा , भाग ,जोड़, घटाव आदि क्रियाएँ हमारा मस्तिष्क भी कर सकता है और कंप्यूटर भी ।[improvised:changed 'जोड़ना , घटाना ' to 'जोड ,घटाव']
;Default meaning from dictionary:in general domain.
(defrule subtraction0
(declare (salience 0))
(id-root ?id subtraction)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtraction.clp 	subtraction0   "  ?id "  GatAva )" crlf))
)

;@@@Added by 14anu-ban-01 on 31-8-2014.
;For addition and subtraction, the rule is in terms of decimal places.  [NCERT corpus]
;सङ्कलन और व्यवकलन के लिए यह नियम दशमलव स्थान के पदों में है.[NCERT corpus]
(defrule subtraction01
(declare (salience 1000)) 
(id-root ?id subtraction)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ?id1 ?id2 ?id)
(id-root ?id1 and|or)
(id-root ?id2 addition)
(Domain physics)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavakalana))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtraction.clp 	subtraction01   "  ?id "  vyavakalana )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  subtraction.clp  subtraction01   "  ?id "  physics )" crlf))
)


;@@@ Added by 14anu-ban-01 on (30-10-2014)
;Her income after the subtraction of living expenses and taxes was enough.[Self:with reference to COCA]
;उसकी आय निर्वाह खर्च और टैक्स काटने के बाद भी पर्याप्त थी.[self]
(defrule subtraction1
(declare (salience 100))
(id-root ?id subtraction)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta))
(assert (make_verbal_noun ?id))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtraction.clp 	subtraction1   "  ?id "  kAta )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  subtraction.clp 	subtraction1    "  ?id " )" crlf))
)


