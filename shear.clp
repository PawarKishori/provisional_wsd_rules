;##############################################################################
;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################
;@@@ Added by jagriti(25.1.2014)
;He was shorn of all powers.
;उसे सब अधिकारों से वंचित कर दिया गया था . 
(defrule shear0
(declare (salience 5000))
(id-root ?id shear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-of_saMbanXI ?id ?id1)
(id-root ?id1 property|power)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaMciwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shear.clp 	shear0  "  ?id "  vaMciwa_kara)" crlf))
)

;@@@ Added by jagriti(25.1.2014)
;He was shorn of his entire money.
;उसके सम्पूर्ण पैसे लूट लिए गए. 
(defrule shear1
(declare (salience 4900))
(id-root ?id shear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-of_saMbanXI ?id ?id1)
(id-root ?id1 money)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shear.clp 	shear1  "  ?id "  lUta)" crlf))
)

;....default rule.....
;@@@ Added by jagriti(25.1.2014)
;Sheeps are shorn once every year.
;भेड़े प्रत्येक वर्ष एक बार कतरी जाती हैं. 
(defrule shear2
(declare (salience 100))
(id-root ?id shear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  kawara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shear.clp 	shear2  "  ?id "  kawara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-10-2014)
;The key property of fluids is that they offer very little resistance to shear stress; their shape changes by application of very small shear stress[Ncert Corpus]
;तरलों का मूल गुण यह है कि वह विरूपण प्रतिबल का बहुत ही न्यून प्रतिरोध करते हैं ; फलतः थोडे से विरूपण प्रतिबल लगाने से भी उनकी आकृति बदल जाती है.[Ncert Corpus]
(defrule shear3
(declare (salience 1000))
(id-root ?id shear)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 stress)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  aparUpaNa/virUpaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shear.clp 	shear3  "  ?id "  aparUpaNa/virUpaNa)" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-10-2014)
;The shearing stress of fluids is about million times smaller than that of solids.
;ठोसों की अपेक्षा तरलों का अपरूपक प्रतिबल लगभग दस लाखवाँ कम होता है.
(defrule shear4
(declare (salience 1100))
(id-word ?id shearing)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aparUpaka/virUpaka))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* " shear.clp 	shear4  "  ?id "  physics )" crlf)

(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  shear.clp 	shear4   "  ?id " aparUpaka/virUpaka)" crlf)
)
)

;@@@ Added by 14anu-ban-11 on (20-03-2015)
;The bolts holding the wheel in place sheared off. (oald)
;पहिये को पकडे हुए तीर टूट गये .(self)
(defrule shear5
(declare (salience 110))
(id-root ?id shear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1 tUta_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shear.clp shear5  "  ?id "  " ?id1 "  tUta_jA )" crlf))
)

;@@@ Added by 14anu-ban-11 on (20-03-2015)
;Note:- Working properly on parser no. 2.
;The prisoners’ hair was shorn.(oald)
;बन्दियों के केश काटे गये थे . (self)
(defrule shear6
(declare (salience 103))
(id-root ?id shear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shear.clp 	shear6   "  ?id "  kAta)" crlf))
)

