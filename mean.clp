
(defrule mean0
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) that )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arWa_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean0   "  ?id "  arWa_hE )" crlf))
)

;"means","N","1.sAXana"
;The means are more important than the achievements.
(defrule mean1
(declare (salience 4900))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id means )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sAXana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mean.clp  	mean1   "  ?id "  sAXana )" crlf))
)

;"meaning","N","1.arWa"
;The meaning of the phrase was not very clear to me.
(defrule mean2
(declare (salience 4800))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id meaning )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id arWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mean.clp  	mean2   "  ?id "  arWa )" crlf))
)

(defrule mean3
(declare (salience 4700))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 harm)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzcAne_kA_prayawna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean3   "  ?id "  pahuzcAne_kA_prayawna_kara )" crlf))
)

;He does not intend to mean any harm
(defrule mean4
(declare (salience 4600))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id infinitive)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id irAxA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean4   "  ?id "  irAxA_ho )" crlf))
)

(defrule mean6
(declare (salience 4400))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) that )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arWa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean6   "  ?id "  arWa_ho )" crlf))
)

(defrule mean7
(declare (salience 4300))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) it )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arWa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean7   "  ?id "  arWa_ho )" crlf))
)


;$$$ Modified by Nandini(17-12-13)
;"Go" also means earth.[via mail]
(defrule mean8
(declare (salience 4550))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 go)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arWa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean8   "  ?id "  arWa_ho )" crlf))
)


;@@@ Added by 14anu-ban-08 on 26-08-2014
;Later when we asked the guide then he said that both the ears of the elephant had totally tensed which meant that he got angry . 
;बाद  में  हमने  गाइड  से  पूछा  तो  उसने  बताया  कि  उस  हाथी  के  दोनों  कान  एकदम  तन  गए  थे  इसका  मतलब  वह  गुस्सा  हो  गया  था  ।
(defrule mean10
(declare (salience 4200))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-word = ( - ?id 1) which)      ;more constraints can be added
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mawalaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean10   "  ?id "  mawalaba )" crlf))
)

;@@@ Added By 14anu-ban-08 (10-11-2014)
;This means that solid bodies are not perfectly rigid.	[NCERT]
;इसका अर्थ है कि ठोस पिंड हैं नहीं पूर्णतः दृढ.  [NCERT]
(defrule mean11
(declare (salience 4600))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 this)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arWa_hE))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean11   "  ?id "  arWa_hE )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  mean.clp      mean11   "  ?id " kA )" crlf)
)

;@@@ Added 14anu-ban-08 (22-11-2014)
;In absence of any other method of knowing true value, we considered arithmatic mean as the true value.    [NCERT]
;क्योंकि, हमें किसी राशि का वास्तविक मान ज्ञात करने की कोई विधि पता नहीं है, इसलिए हम समान्तर माध्य को ही राशि का वास्तविक मान स्वीकार कर लेते हैं.   [NCERT]
(defrule mean12
(declare (salience 4700))
(Domain physics)
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAXya))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean12   "  ?id "  mAXya )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  mean.clp 	mean12   "  ?id "  mAXya )" crlf)
)

;@@@ Added by 14anu09
; It is a tree that was meant to be timeless .
;यह एक ऐसा पेडे है , जो अनवरत चलते रहने के लिए ही लगाया गया था .(corpus)
;used as उद्देश्य in anusaaraka translation
(defrule mean010
(declare (salience 4550))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kqxanwa_karma  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxxeSya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean010   "  ?id "  uxxeSya )" crlf))
)

;@@@ Added 14anu-ban-08 (22-11-2014)
;(b) The arithmetic mean of all the absolute errors is taken as the final or mean absolute error of the value of the physical quantity a.    [NCERT] 
;(b) भौतिक राशि की निरपेक्ष त्रुटियों के परिमाणों के समान्तर माध्य को भौतिक राशि a के मान की अन्तिम या माध्य निरपेक्ष त्रुटि कहा जाता है.    [NCERT]
(defrule mean13
(declare (salience 4703))
(Domain physics)
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 error)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAXya))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean13   "  ?id "  mAXya )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  mean.clp 	mean13   "  ?id "  mAXya )" crlf)
)

;@@@ Added by 14anu-ban-08 (24-11-2014)
;As the periods are measured to a resolution of 0.01 s, all times are to the second decimal; it is proper to put this mean period also to the second decimal.    [NCERT]
;क्योंकि, सभी काल 0.01 s के विभेदन तक मापे गए हैं, इसलिए समय की सभी मापें दूसरे दशमलव स्थान तक हैं;इस औसत काल को भी दूसरे दशमलव स्थान तक लिखना उचित है.   [NCERT]
(defrule mean14
(declare (salience 4704))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 period)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Osawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean14   "  ?id "  Osawa )" crlf))
)

;@@@ Added by 14anu20 on 17/06/2014
;The mean value is found .
;औसत मूल्य पाया जाता है . 
(defrule mean15
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) value)
(viSeRya-viSeRaNa  =(+ ?id 1) ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Osawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean15   "  ?id " Osawa  )" crlf))
)

;@@@Added by 14anu-ban-08 (07-03-2015)
;Money means nothing to him.  [oald]
;पैसा उसके लिए महत्व नहीं रखता.  [self]
(defrule mean16
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 money)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawva_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean16   "  ?id " mahawva_raKa  )" crlf))
)

;@@@Added by 14anu-ban-08 (07-03-2015)
;Spending too much now will mean a shortage of cash next year.  [oald]
;अभी ज्यादा पैसे खर्च का परिमाणस्वरुप अगले साल पैसे की कमी होगी.  [self]
(defrule mean17
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id1 spend)
(id-root ?id2 shortage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parimANasvarupa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean17   "  ?id " parimANasvarupa  )" crlf))
)


;@@@Added by 14anu-ban-08 (17-03-2015)
;Your friendship means a great deal to me.  [oald]
;तुम्हारी दोस्ती मेरे लिए बहुत मायने रखती हैं.  [self]
(defrule mean18
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 friendship)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAyane_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean18   "  ?id " mAyane_raKa  )" crlf))
)

;@@@Added by 14anu-ban-08 (17-03-2015)     ;Run on parser 11
;She's always been mean with money. [oald]
;वे पैसे के मामले में बहुत कंजूस हैं.  [self]
(defrule mean19
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-with_saMbanXI ?id ?id1)
(id-root ?id1 money)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaMjUsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean19   "  ?id " kaMjUsa  )" crlf))
)

;@@@Added by 14anu-ban-08 (17-03-2015)    
;Don't be so mean to your little brother!  [oald]
;अपने छोटे भाई के प्रति ज्यादा निर्दयी ना बनो.  [self]
(defrule mean20
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirxayI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean20   "  ?id " nirxayI  )" crlf))
)

;@@@Added by 14anu-ban-08 (17-03-2015)
;He's a mean tennis player.  [oald]
;वह टेनिस का मंझला हुआ खिलाड़ी हैं.  [self]
(defrule mean21
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2)
(id-root ?id1 player)
(id-root ?id2 tennis|basketball|hockey|cricket|volleyball|chess|golf|football)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maMJalA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean21   "  ?id " maMJalA  )" crlf))
)

;@@@Added by 14anu-ban-08 (17-03-2015)
;These rights apply even to the meanest labourer.  [oald]
;यह अधिकार मध्यम वर्गीय पर भी लागू कर सकते हैं.  [self]
(defrule mean22
(declare (salience 5000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 labourer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maXyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean22   "  ?id " maXyama  )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$ Modified By 14anu-ban-08 on 07-08-2014
;Changed meaning from "wAwparya_ho" to "wAwparya_hE"
;In principle, this means that the laws for 'derived' forces (such as spring force, friction) are not independent of the laws of fundamental forces in nature.
;sixXAnwa rUpa meM isakA wAwparya hE ki vyuwpanna baloM(jEse kamAnI,bala,GarRaNa)ke niyama prakqwi ke mUla baloM ke niyamoM se svawanwra nahIm hE.
(defrule mean5
(declare (salience 4000))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAwparya_hE))
(assert (kriyA_id-subject_viBakwi ?id kA))
(assert (id-H_vib_mng ?id 0))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean5   "  ?id "  wAwparya_hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  mean.clp      mean5   "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* "  mean.clp      mean5   "  ?id " 0 )"  crlf))
)


;"mean","Adj","1.kamInA"
;He is a very mean person.
(defrule mean9
(declare (salience 4100))
(id-root ?id mean)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamInA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mean.clp 	mean9   "  ?id "  kamInA )" crlf))
)



;"means","N","1.sAXana"
;The means are more important than the achievements.
;--"2.pEsA
;He does not have the means to support the prifessional education of his children.
;;"meaning","N","1.arWa"
;The meaning of the phrase was not very clear to me.
