
;"voting","N","1.mawaxAna"
;Voting for this constituency is over
(defrule vote0
(declare (salience 5000))
(id-root ?id vote)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id voting )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mawaxAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  vote.clp  	vote0   "  ?id "  mawaxAna )" crlf))
)

;@@@ Added by 14anu-ban-07 (31-03-2015) 
;He was voted in as treasurer.(oald)(parse no.2)
;वह कोषाध्यक्ष के रूप में चुनाव जीत गया था . (manual)
(defrule vote3
(declare (salience 5000))
(id-root ?id vote)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cunAva_jIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " vote.clp	vote3  "  ?id "  " ?id1 "  cunAva_jIwa  )" crlf))
)

;@@@ Added by 14anu-ban-07 (31-03-2015) 
;He was voted out of office.(oald)(parse problem)
;वह दफ्तर से बरखास्त किया गया था . (manual)
(defrule vote4
(declare (salience 5100))
(id-root ?id vote)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baraKAswa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " vote.clp	vote4  "  ?id "  " ?id1 "  baraKAswa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07 (31-03-2015) 
;She was voted onto the board of governors.(oald)(parse problem)
;वह गवर्नर मंडल का चुनाव जीत गया था.(manual)
(defrule vote5
(declare (salience 5200))
(id-root ?id vote)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 onto)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cunAva_jIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " vote.clp	vote5  "  ?id "  " ?id1 "  cunAva_jIwa  )" crlf))
)

;@@@ Added by 14anu-ban-07 (31-03-2015) 
;The proposal to build a new road through the forest was voted down by the local council.(cambridge)
;जङ्गल में एक नयी सडक बनाने के लिए इस प्रस्ताव को स्थानीय परिषद के द्वारा अस्वीकार कर दिया गया था . (manual)
(defrule vote6
(declare (salience 5300))
(id-root ?id vote)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 asvIkAra_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " vote.clp	vote6  "  ?id "  " ?id1 "  asvIkAra_kara_xe  )" crlf))
)

;@@@ Added by 14anu-ban-07 (31-03-2015) 
;A proposal to merge the two companies was voted through yesterday.(oald)
;दो कम्पनियाँ को विलय करने के प्रस्ताव को कल पारित किया गया था .  (manual)
(defrule vote7
(declare (salience 5400))
(id-root ?id vote)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pAriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " vote.clp	vote7  "  ?id "  " ?id1 "  pAriwa_kara  )" crlf))
)

;------------------------ Default Rules ----------------------

;"vote","N","1.mawa/vota"
;There were only seventeen  votes in favor of the motion
(defrule vote1
(declare (salience 4900))
(id-root ?id vote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vote.clp 	vote1   "  ?id "  mawa )" crlf))
)

;"vote","VTI","1.mawa_xenA"
;He voted for the motion
(defrule vote2
(declare (salience 4800))
(id-root ?id vote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mawa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vote.clp 	vote2   "  ?id "  mawa_xe )" crlf))
)

