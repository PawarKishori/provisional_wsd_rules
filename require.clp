;@@@ Added by Anita-18.1.2014
;Please telephone this number if you require any further information. [Cambridge dictionary]
;अगर आपको और आगे की सूचना की जरूरत है तो कृपया इस नंबर पर फ़ोन करें.
(defrule require1
(declare (salience 5000))
(id-root ?id require)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 information|message|news)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jarUrawa_ho))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp 	require1   "  ?id "  jarUrawa_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  require.clp      require1   "  ?id " kA )" crlf)
)
)

;@@@ Added by Anita-18.1.2014
;The rules require that you bring only one guest to the dinner. [Cambridge dictionary]
;नियमों की माँग है कि आप रात के खाने के लिए केवल एक अतिथि को ही लाए ।  
(defrule require2
(declare (salience 4950))
(id-root ?id require)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 rule|law)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAzga_ho))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp  require2   "  ?id "  mAzga_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  require.clp      require2   "  ?id " kA )" crlf)
)
)

;Commented by Soma 22-11-2014
;@@@ Added by Anita--22.2.2014 
;The student was also required to obtain a working knowledge of two of the languages which form the foundation of the Indian vernaculars such as Pali Prakrit and Persian. [gyanidhi ]
;योजना के अंतर्गत यह अपेक्षित हो गया था कि विद्यार्थी को दो देशी भाषाओं का ज्ञान हो यानी अपनी मातृभाषा का सपूर्ण ज्ञान और  ;दूसरी देशी भाषा का कुछ कम विस्तृत ज्ञान।
;(defrule require3
;(declare (salience 4850))
;(id-root ?id require)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-kriyArWa_kriyA  ?id ?)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id apekRiwa))
;;(assert (kriyA_id-subject_viBakwi ?id kA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp  require3   "  ?id "  apekRiwa )" crlf)
;;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  require.clp      require3   "  ?id " kA )" crlf)
;)
;)

;@@@ Added by Anita 14.3.2014
;Accommodation , therefore, was required for its new functions of teaching and research. 
;लिहाज़ा , शिक्षण और अनुसंधान के नए कार्यों के लिए आवास की जरूरत थी ।
(defrule require4
(declare (salience 5100))
(id-root ?id require)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jarUrawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp 	require4   "  ?id "  jarUrawa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (14-11-2014)
;Is an external force required to keep a body in uniform motion? [ncert corpus]
;kyA kisI piNda kI ekasamAna gawi banAe raKane ke lie koI bAhya bala AvaSyaka hE ?[ncert corpus]
;In government-regulated agriculture, farmers can be required to destroy crops that exceed their production quota.[agriculture domain]
;सरकार  विनियमित कृषि में  किसानों को  फसलों को नष्ट करना आवश्यक हो सकता  है जो उनके उत्पादन नियतांश से  अधिक हो/अधिक होती हैं |   [manual] 
(defrule require5
(declare (salience 5200))
(id-root ?id require)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-karma ?id ? )(kriyA-kriyArWa_kriyA ?id ? )) ;relation added by 14anu-ban-10 on (19-11-2014) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp 	require5   "  ?id "  AvaSyaka)" crlf))
)


;@@@ Added by 14anu-ban-10 on (14-11-2014)
;If m is the mass of the satellite and V its speed, the centripetal force required for this orbit is directed towards the center.[ncert corpus]
;yaxi upagraha kA xravyamAna @m waWA @V isakI cAla hE, wo isa kakRA ke lie AvaSyaka aBikenxra bala waWA yaha bala kakRA ke kenxra kI ora nixeSiwa hE.[ncert corpus]
(defrule require6
(declare (salience 5400))
(id-root ?id require)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 orbit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp 	require6   "  ?id " AvaSyaka)" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-11-2014)
;The only major difficulty was that since it was thought that a wave required a medium for its propagation, how could light waves propagate through vacuum.[ncert corpus]
;sabase badI kaTinAI usa mAnyawA ke kAraNa WI, jisake anusAra yaha samaJA jAwA WA ki warafga saFcaraNa ke lie kisI mAXyama kI AvaSyakawA howI hE, wo Pira, prakASa warafgeM nirvAwa meM kEse saFcariwa ho sakawI hEM.[ncert corpus]
(defrule require7
(declare (salience 5400))
(id-root ?id require)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 wave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaSyakawA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp 	require7   "  ?id " AvaSyakawA_ho)" crlf))
)

;@@@ Added by Soma (22-11-14)
(defrule require_default_animate
(declare (salience 100))
(id-root ?id require)
(kriyA-subject  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaSyakawA_ho))
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp  require_default_animate   "  ?id "  AvaSyakawA_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  require.clp      require_default_animate   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  require.clp      require_default_animate   "  ?id " kA )" crlf)
)
)

;@@@ Added by Bhagyashri Kulkarni (15-09-2016)
;The man is required to give a test.
;उस आदमी को परीक्षा देना जरुरी है. 
(defrule require8
(declare (salience 5200))
(id-root ?id require)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kqxanwa_karma  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jarurI))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp 	require8   "  ?id "  jarurI)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  require.clp      require8   "  ?id " ko )" crlf))
)


;###########################default-rule######################################
;$$$ Modified by Soma (22-11-14)
;@@@ Added by Anita-18.1.2014
;Skiing at 80 miles per hour requires total concentration. [Cambridge dictionary]
;८० मील प्रतिघंटे स्कींग करने के लिए पूरी एकाग्रता की आवश्कता होती है. ( जरूरत)
;Bringing up children often requires you to put their needs first.
;बच्चों को बड़ा करने में अक्सर उनकी जरूरतों को प्राथमिकता देने की आवश्यकता होती है ।
(defrule require-default_rule
(id-root ?id require)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaSyakawA_ho))
;(assert (kriyA_id-subject_viBakwi ?id ko)) This constraint works only when the subject is animate Modified by Soma (22-11-14)
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  require.clp 	require-default_rule   "  ?id "  AvaSyakawA_ho )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  require.clp      require-default_rule   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  require.clp      require-default_rule   "  ?id " kA )" crlf)
)
)
