;@@@ Added  by 14anu-ban-04 (27-03-2015)
;Grammatical information helps learners to decode sentences.           [cald]
;वाक्यों को समझने के लिए व्याकरणिक सूचना शिक्षार्थियों की सहायता करती है   .                  [self]
; I'm trying to decode the expression on her face.                    [merriam-webster]
;मैं उसके चेहरे पर अभिव्यक्ति को समझने का प्रयास कर रहा हूँ .                             [self]
(defrule decode1
(declare (salience 20))                              
(id-root ?id decode)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 sentence|expression)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(assert (kriyA_id-object_viBakwi ?id ko)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  decode.clp     decode1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decode.clp 	decode1  "  ?id "  samaJa )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added  by 14anu-ban-04 (27-03-2015)
;I was involved in decoding enemy documents.                     [oald]
;मैं शत्रु के दस्तावेजों का कूटानुवाद (कूट का अनुवाद) करने में सम्मिलित था .             [self[
;The script was decoded by a group of policemen .                    [hinkhoj]
;सिपाहियों के समूह के द्वारा लिखावट  का कूटानुवाद किया गया था .                           [self]
(defrule decode0
(declare (salience 10))                              
(id-root ?id decode)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kUtAnuvAxa_kara))  
(assert (kriyA_id-object_viBakwi ?id kA)) 
(assert (kriyA_id-subject_viBakwi ?id kA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  decode.clp     decode0   "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  decode.clp     decode0   "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decode.clp 	decode0   "  ?id "  kUtAnuvAxa_kara )" crlf))
)

