
;$$$ Modified by 14anu-ban-06 (13-04-2015)
; She gave me one of those irresistible smiles and I just had to agree. (cambridge)
;उसने मुझे उन अत्यन्त सम्मोहक मुस्कराहटो में से एक दी और मुझे सहमत होना पडा . (manual)
(defrule irresistible1
(declare (salience 5100))
(id-root ?id irresistible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 smile)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyanwa_sammohaka));meaning changed from 'aprawiroXya' to 'awyanwa_sammohaka' by 14anu-ban-06 (13-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irresistible.clp 	irresistible1   "  ?id "  awyanwa_sammohaka )" crlf))
)

;@@@ Added by 14anu-ban-06 (13-04-2015)
;On such a hot day, the water was irresistible . (OALD)
;ऐसे गरम दिन पर, पानी अत्यन्त सम्मोहक था . (manual)
(defrule irresistible2
(declare (salience 5200))
(id-root ?id irresistible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyanwa_sammohaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irresistible.clp 	irresistible2   "  ?id "  awyanwa_sammohaka )" crlf))
)

;------------------------ Default Rules ----------------------

;"irresistible","Adj","1.aprawiroXya"
;He felt an irresistible urge to slap her.
(defrule irresistible0
(declare (salience 5000))
(id-root ?id irresistible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aprawiroXya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irresistible.clp 	irresistible0   "  ?id "  aprawiroXya )" crlf))
)


;"irresistible","Adj","1.aprawiroXya"
;He felt an irresistible urge to slap her.
;--"2.luBAvanA"
;Children find their desire for ice cream irresistible.
;
;
