;@@@ Added by 14anu-ban-04 (03-02-2015)
;He was excoriated for his mistakes.             [free dictionary]
;वह अपनी गलतियों के लिए फटकारा गया था .                         [self]
(defrule excoriate1
(declare (salience 20))
(id-root ?id excoriate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI ?id ?id1)
(id-root ?id1 mistake)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PatakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  excoriate.clp 	excoriate1   "  ?id "   PatakAra)" crlf))
)

;@@@ Added by 14anu-ban-04 (03-02-2015)
;The president excoriated the Western press for their biased views.           [cald]
;उनके पक्षपातपूर्ण विचारों के लिए अध्यक्ष ने वेस्टर्न प्रैस की कड़ी आलोचना की . 
(defrule excoriate0
(declare (salience 10))
(id-root ?id excoriate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadI_AlocanA_kara))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  excoriate.clp     excoriate0   "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  excoriate.clp 	excoriate0   "  ?id "  kadI_AlocanA_kara)" crlf))
)


