
;@@@ Added by 14anu-ban-03 (30-03-2015)
;The President was obliged to concede power to the army. [oald]
;अध्यक्ष सेना की शक्ति छोड देने पर मजबूर हो गया था . [manual]
(defrule concede1
(declare (salience 10))
(id-root ?id concede)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 power)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Coda_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concede.clp 	concede1   "  ?id "  Coda_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (30-03-2015)
;After losing this decisive battle, the general was forced to concede. [oald]
;यह निर्णायक लडाई खोने के बाद, जनरल हार मानने के लिए मजबूर किया गया था .  [manual]
(defrule concede2
(declare (salience 10))
(id-root ?id concede)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA ?id1 ?id)
(id-root ?id1 force)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAra_mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concede.clp 	concede2   "  ?id "  hAra_mAna )" crlf))
)

;@@@ Added by 14anu-ban-03 (30-03-2015)
;The team conceded two goals in the first five minutes of the game. [oald]
;दल ने खेल के पहले पाँच मिनटों में दो गोल किए .  [manual]
(defrule concede3
(declare (salience 10))
(id-root ?id concede)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 goal)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concede.clp 	concede3  "  ?id "  kara )" crlf))
)

;--------------------------------------- Default rules ---------------------------------
;@@@ Added by 14anu-ban-03 (30-03-2015)
;I had to concede the logic of this. [oald]
;मुझे इसके तर्क मानने पडे . [manual]
(defrule concede0
(declare (salience 00))
(id-root ?id concede)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concede.clp  concede0   "  ?id "  mAna )" crlf))
)


