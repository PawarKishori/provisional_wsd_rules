;@@@ Added by 14anu-ban-07,(07-03-2015)
;They face a torrid time in tonight's game.(oald)
;उन्होनें आज रात के खेल में बहुत कठिन समय का सामना किया  हैं . (manual)
(defrule torrid1
(declare (salience 1000))
(id-root ?id torrid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_kaTina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  torrid.clp 	torrid1   "  ?id "  bahuwa_kaTina )" crlf))
)

;@@@ Added by 14anu-ban-07,(07-03-2015)
;A torrid love affair.(oald)
;एक कामुक प्रेम सम्बन्ध . (manual)
(defrule torrid2
(declare (salience 1100))
(id-root ?id torrid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 affair|love|romance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAmuka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  torrid.clp 	torrid2   "  ?id "  kAmuka )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-07,(07-03-2015)
;A torrid summer.(oald)
;एक बहुत गरम ग्रीष्म . (manual)
(defrule torrid0
(id-root ?id torrid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_garama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  torrid.clp   torrid0   "  ?id "  bahuwa_garama )" crlf))
)

