;@@@ Added by 14anu-ban-04 (19-02-2015)
;She was suspected of being a drug dealer.                [cald]
;उस पर  एक ड्रग व्यापारी होने का सन्देह किया गया था.                           [self]
(defrule drug2
(declare (salience 5010))
(id-root ?id drug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id draga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drug.clp 	drug2   "  ?id "  draga )" crlf))
)


;@@@ Added by 14anu-ban-04 (19-02-2015)
;Her drink must have been drugged.                            [oald]
;उसके पेय पदार्थ में दवा मिलायी गयी होगी .                                  [self]
(defrule drug3
(declare (salience 4910))
(id-root ?id drug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1) 
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-H_vib_mng ?id1 meM))
(assert (id-wsd_root_mng ?id xavA_milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  drug.clp    drug3  "  ?id1 "  meM )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drug.clp 	drug3   "  ?id "  xavA_milA )" crlf))
)

;@@@ Added by 14anu-ban-04 (21-02-2015)
;The killer confessed that he often drugged his victims before he killed them.             [oald]
;हत्यारे ने दोष स्वीकार किया कि उसने अपने शिकारों को मारने  से पहले  उसने उनको    अक्सर बेहोशी की दवा दी  .                   [self]
(defrule drug4
(declare (salience 4910))
(id-root ?id drug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?kri ?id1)
(id-root ?id1 killer)
(kriyA-vAkyakarma ?kri ?id)
(kriyA-object ?id ?id3) 
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id behoSI_kI_xavA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drug.clp 	drug4   "  ?id "  behoSI_kI_xavA_xe)" crlf))
)

;@@@ Added by 14anu-ban-04 (21-02-2015)
;Work is a drug for him.                           [oald]
;कार्य उसका शौक है .                                    [self]
(defrule drug5
(declare (salience 5010))
(id-root ?id drug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 work|playing|dancing) 
(viSeRya-for_saMbanXI ?id ?id2)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SOka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drug.clp 	drug5   "  ?id "  SOka )" crlf))
)

;------------------------ Default Rules ----------------------

;"drug","N","1.xavA"
(defrule drug0
(declare (salience 5000))
(id-root ?id drug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xavA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drug.clp 	drug0   "  ?id "  xavA )" crlf))
)

;"drug","VT","1.xavA_xenA/behoSI_kI_xavA_xenA"
;They drugged the kidnapped tourist
(defrule drug1
(declare (salience 4900))
(id-root ?id drug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xavA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drug.clp 	drug1   "  ?id "  xavA_xe )" crlf))
)

