
;usane apanI Aja kI klAsa CodZa xI
(defrule chuck0
(declare (salience 5000))
(id-root ?id chuck)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CodZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " chuck.clp	chuck0  "  ?id "  " ?id1 "  CodZa_xe  )" crlf))
)

;------------------------ Default Rules ----------------------

;"chuck","N","1.nikAla_xenA"
;He got the chuck from his job for absenteism.
(defrule chuck1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (19-02-2015)
(id-root ?id chuck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xenA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chuck.clp 	chuck1   "  ?id "  nikAla_xenA )" crlf))
)

;$$$ Modified by 14anu-ban-03 (19-02-2015)
;"chuck","V","1.beparavAha_PeMkanA"
;He chucked the bits of paper in the bin.[same clp file]
;उसने डिब्बे में कागज के टुकडे फेंक दिए . [manual]
(defrule chuck2
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (19-02-2015)
(id-root ?id chuck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PeMka_xe))  ;meaning changed from 'beparavAha_PeMka' to 'PeMka_xe' by 14anu-ban-03 (19-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chuck.clp 	chuck2   "  ?id "  PeMka_xe )" crlf))
)

