;@@@ Added by 14anu-ban-06 (25-03-2015)
;The hotel was homely and comfortable. (cambridge)
;होटल घर जैसा और आरामदायक था . (manual)
(defrule homely1
(declare (salience 2000))
(id-root ?id homely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 restaurant|hotel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gara_jEsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  homely.clp 	homely1   "  ?id "  Gara_jEsA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx


;@@@ Added by 14anu-ban-06 (25-03-2015)
;His landlady was a kind, homely woman.(OALD)
;उसकी मकान मालकिन एक दयालु, घरेलू स्त्री थी .(manual) 
(defrule homely0
(declare (salience 0))
(id-root ?id homely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GarelU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  homely.clp 	homely0   "  ?id "  GarelU )" crlf))
)
