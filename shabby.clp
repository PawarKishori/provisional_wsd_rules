;@@@ Added by 14anu-ban-11 on (20-02-2015)
;She wore shabby old jeans and a T-shirt.(oald)
;वह फटे  पुरानी  जीन्स और छोटी आस्तीन वाली कमीज पहना हुए था. (self)
(defrule shabby1
(declare (salience 20))
(id-root ?id shabby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 jeans)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shabby.clp 	shabby1   "  ?id "  Pata)" crlf))
)


;@@@ Added by 14anu-ban-11 on (20-02-2015)
;It was a shabby way to treat visitors. (oald)
;यह दर्शकों से व्यवहार करने का एक बुरा तरीका था . (self)
(defrule shabby2
(declare (salience 30))
(id-root ?id shabby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 way)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id burA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shabby.clp 	shabby2   "  ?id "  burA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (20-02-2015)
;It was shabby and dull. (coca)
;मलीन और धुंधला था .(self)
(defrule shabby3
(declare (salience 40))
(id-root ?id shabby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 I|it)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id malIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shabby.clp 	shabby3   "  ?id "  malIna)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (20-02-2015)
;The outside of the house was beginning to look shabby.(oald)
;घर के बाहर जर्जर दिखना शुरु हो रहा था . (self)
(defrule shabby0
(declare (salience 10))
(id-root ?id shabby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jarjara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shabby.clp   shabby0   "  ?id "  jarjara)" crlf))
)

