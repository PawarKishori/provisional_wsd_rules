;Added by Meena(15.9.10)
;A more massive object changes its motion more slowly in response to a given force.
(defrule in_response_to0
(declare (salience 5000))
(id-root ?id response)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) in )
(id-word =(+ ?id 1) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(+ ?id 1) kI_prawikriyA_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " response.clp  in_response_to0  "  ?id "  " (- ?id 1) " " (+ ?id 1) "  kI_prawikriyA_meM  )" crlf))
)


;@@@ Added by 14anu-ban-10 on (19-11-2014)
;One kind of response from the earliest times has been to observe the physical environment carefully, look for any meaningful patterns and relations in natural phenomena, and build and use new tools to interact with nature. [ncert corpus]
;Axi kAla - se prawikriyA eka prakAra kA sAvaXAnI se BOwika paryAvaraNa xeKane vAlA, prAkqwika pariGatanA meM kisI arWapUrNa namUne Ora sambanXa DUzDanA, Ora banAnA Ora upayoga karanA naye OjAra prakqwi ke sAWa prawikriyA karane ke lie huA hE .[ncert corpus]
(defrule response1
(declare (salience 5020))
(id-root ?id response)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-from_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawikriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " response.clp     response1  "  ?id " prawikriyA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (19-11-2014)
;The motion of a current-carrying conductor in a magnetic field, the response of a circuit to an ac voltage (signal), the working of an antenna, the propagation of radio waves in the ionosphere, etc., are problems of electrodynamics.[ncert corpus]
;kisI XArAvAhI cAlaka kI cumbakIya kRewra meM gawi, kisI vixyuwa paripaWa kI prawyAvarwI voltawA (siganala) se anukriyA, kisI EntenA kI kAryapraNAlI, Ayana maNdala meM rediyo warafgoM kA saFcaraNa Axi vExyuwa gawikI kI samasyAez hEM.[ncert corpus]
(defrule response2
(declare (salience 5030))
(id-root ?id response)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " response.clp     response2  "  ?id " anukriyA)" crlf))
)


;@@@ Added by 14anu-ban-10 on (26-11-2014)
; It is determined by the ability of a material to polarise in response to an applied field, and thereby to cancel, partially, the field inside the material.[ncert corpus]
;isakA nirXAraNa anuprayukwa kRewra ke prawyuwwara meM mAXya ke Xruviwa hone ke guNa, jisake xvArA yaha kisI paxArWa ke BIwara ke kRewra ko AMSika rUpa se nirasiwa karawA hE, se kiyA jAwA hE.[ncert corpus]
(defrule response3
(declare (salience 5040))
(id-root ?id response)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) in)
(id-root =(+ ?id 1) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (+ ?id 1) prawyuwwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " response.clp	 response3  "  ?id "  " (- ?id 1)" " (+ ?id 1) "   prawyuwwara)" crlf))
)


;@@@ Added by 14anu-ban-05 on (06-04-2015)
;There has been little response to our appeal for funds. [OALD]
;धन के लिए हमारी अपील पर  नहीॅ के बराबर प्रतिक्रिया हुई है.  [MANUAL]
(defrule response4
(declare (salience 5050))
(id-root ?id response)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 little|encourage|angry)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawikriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " response.clp     response4  "  ?id " prawikriyA)" crlf))
)
