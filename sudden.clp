;@@@ Added by 14anu-ban-01 on (27-02-2016)
;She flew back to London from Kenya after the sudden death of her father. 	[COCA]
;अपने पिता की अकाल मृत्यु के कारण वह [विमान द्वारा] केन्या से लन्दन वापिस लौटी. 			[self]
(defrule sudden1
(declare (salience 1000))
(id-root ?id sudden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 death)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id akAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sudden.clp 	sudden1   "  ?id "  akAla )" crlf))
)

;@@@ Added by 14anu-ban-01 on (27-02-2016)
;The sudden rise in test scores stirred concern.	[self: wrt COCA]
;परीक्षा प्राप्तांक में तेज वृद्धि ने चिंता बढ़ाई.				[self] 	
;Marlen felt a curtain flutter, as if a sudden wind had entered the house.	[COCA]
;मॉर्लन ने पर्दे की फड़फड़ाहट महसूस की ,मानो तेज़ हवा भीतर आई हो .	[self]
(defrule sudden2
(declare (salience 1000))
(id-root ?id sudden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 rise|wind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sudden.clp 	sudden2  "  ?id "  wejZa )" crlf))
)

;His death was very sudden.	[OALD]

;@@@ Added by 14anu-ban-01 on (27-02-2016) 
;School libraries can lead schools to embrace innovation, think outside the box, engage in interdisciplinary and community collaboration, embrace sudden learning opportunities, and address real-world problems.	[COCA:original sentence]
;School libraries can lead schools to embrace sudden learning opportunities.	[COCA: self-improvised]	
;पाठशालाओं के पुस्तकालय विद्यालयों को शीघ्र विद्या प्राप्ति के अवसरों को अपनाने की ओर ले जा सकते हैं . 	[self]
(defrule sudden3
(declare (salience 1000))
(id-root ?id sudden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 opportunity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIGra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sudden.clp 	sudden3  "  ?id "  SIGra )" crlf))
)


;------------------------------------ Default rules -----------------------

;@@@ Added by 14anu-ban-01 on (27-02-2016)
;It was quite a sudden change.  [quite.clp]
;यह काफी आकस्मिक बदलाव था .   [self]
(defrule sudden0
(declare (salience 0))
(id-root ?id sudden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akasmika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sudden.clp   sudden0   "  ?id "  Akasmika )" crlf))
)

