
;@@@ Added by 14anu-ban-05 on (14-04-2015)
;She stroked the baby’s fuzzy hair.	[OALD]
;उसने  शिशु के घुँघराले बालों को सहलाया.	[MANUAL]
(defrule fuzzy1
(declare (salience 101))
(id-root ?id fuzzy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 head|hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GuzGarAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  fuzzy.clp  	fuzzy1   "  ?id "  GuzGarAlA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (14-04-2015)
;The screen suddenly went fuzzy.	[OALD]
;स्क्रीन अचानक  धुँधली हो गई.			[MANUAL]
(defrule fuzzy2
(declare (salience 102))
(id-root ?id fuzzy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XuzXalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  fuzzy.clp  	fuzzy2   "  ?id "  XuzXalA )" crlf))
)

;-------------------------------- Default Rules --------------------------------

;@@@ Added by 14anu-ban-05 on (14-04-2015)
;A somewhat fuzzy definition of ‘in the national interest’.[OALD]
;'राष्ट्रीय हित में' की किंचित अस्पष्ट परिभाषा. [MANUAL]
(defrule fuzzy0
(declare (salience 100))
(id-root ?id fuzzy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aspaRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  fuzzy.clp     fuzzy0   "  ?id "  aspaRta )" crlf))
)


