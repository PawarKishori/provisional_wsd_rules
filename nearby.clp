
(defrule nearby0
(declare (salience 5000))
(id-root ?id nearby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nearby.clp 	nearby0   "  ?id "  pAsa_vAlA )" crlf))
)

;"nearby","Adj","1.pAsa_vAlA"
;you will get that in the nearby shop.
;
(defrule nearby1
(declare (salience 4900))
(id-root ?id nearby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nearby.clp 	nearby1   "  ?id "  pAsa_meM )" crlf))
)

;"nearby","Adv","1.pAsa_meM"
;The boy lives nearby.
;

;@@@Added by 14anu-ban-08 (02-03-2015)
;Since there are no nearby stars to exert gravitational force on him and the small spaceship exerts negligible gravitational attraction on him, the net force acting on the astronaut, once he is out of the spaceship, is zero.  [NCERT]
;जिस क्षण वह यात्री यान से बाहर आता है, उसी क्षण से अन्तरिक्षयात्री पर कोई बाह्य बल कार्यरत नहीं रहता क्योंकि हमने यह माना है कि यात्री पर गुरुत्वाकर्षण बल आरोपित करने के लिए उसके निकट कोई तारा नहीं हैं तथा अन्तरिक्ष यान छोटा होने के कारण इसके द्वारा यात्री पर लग रहा गुरुत्वाकर्षण बल उपेक्षणीय है.  [NCERT]
(defrule nearby2
(declare (salience 5001))
(id-root ?id nearby)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 star)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nearby.clp 	nearby2   "  ?id "  nikata )" crlf))
)
