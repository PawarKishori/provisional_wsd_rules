;$$$Modified by 14anu-ban-02(10-04-2015)     ;'sense' is added in the list.
;She's got a brilliant sense of humour.[cald]
;उसकी  हास की योग्यता बहुत बढिया  है . [self]
;Modified by Meena(6.9.11)
;He had spend a brilliant day.
;It is a brilliant news, I am absolutely delighted.
(defrule brilliant0
(declare (salience 5000))
(id-root ?id brilliant)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 night|evening|time|hour|start|season|news|idea|form|sense)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_baDiya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brilliant.clp  	brilliant0   "  ?id "  bahuwa_baDiya )" crlf))
)


;$$$Modified by 14anu-ban-02(10-04-2015)       ;'eye' is removed from the list.
;Brilliant blue eyes. [oald]
;चमकीली नीली आँखें . [self]
;Modified by Meena(6.9.11)
;Meanwhile, brilliant weather continued unbroken.
;The sun was brilliant;
(defrule brilliant1
(declare (salience 5000))
(id-root ?id brilliant)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 day|morning|light|sun|moon|daylight|colour|color|weather|sunshine|star|star|smile)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejomaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brilliant.clp  	brilliant1   "  ?id "  wejomaya )" crlf))
)


;@@@Added by 14anu-ban-02(10-04-2015)
;A brilliant career.[oald]
;उज्जवल कैरियर . [self]
(defrule brilliant3
(declare (salience 5000))
(id-root ?id brilliant)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 career)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ujjavala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brilliant.clp  	brilliant3   "  ?id "  ujjavala )" crlf))
)


;@@@Added by 14anu-ban-02(10-04-2015)
;Brilliant blue eyes. [oald]
;चमकीली नीली आँखें . [self]
(defrule brilliant4
(declare (salience 5000))
(id-root ?id brilliant)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 eye)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camakIlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brilliant.clp  	brilliant4   "  ?id "  camakIlI )" crlf))
)



;--------------------------- Default Rules-------------------------------------

;Modified by Meena(6.9.11)
;He was one of the most brilliant defensive players.
;She achieved a brilliant academic career.
(defrule brilliant2
(declare (salience 0))
(id-root ?id brilliant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiBASAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  brilliant.clp  	brilliant2   "  ?id "  prawiBASAlI )" crlf))
)

