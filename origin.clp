;##############################################################################
;#  Copyright (C) 2013-2014 Sonam Gupta(sonam27virgo@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-09 on 25-8-14
;The detailed microscopic origin of these forces is, however, complex and not useful for handling problems in mechanics at the macroscopic scale.  [NCERT CORPUS]
;इन बलों की विस्तृत सूक्ष्म उत्पत्ति के विषय में जानकारी जटिल है तथा स्थूल स्तर पर यान्त्रिकी की समस्याओं को हल करने की दृष्टि से उपयोगी नहीं है .
;@@@Added by Sonam Gupta MTEch IT Banasthali 2013
;The origin of the universe. [Cambridge]
;ब्रह्माण्ड की उत्पत्ति . 
(defrule origin0
(declare (salience 5000))
(id-root ?id origin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(id-cat_coarse ?id1 noun)  ;commented by 14anu-ban-09
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 universe|force)    ;Added by 14anu-ban-09 on 6-9-14 [more constraints can be added]
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  origin.clp 	origin0   "  ?id "  uwpawwi )" crlf))
)


;##########################################################Default rule#######################################################################

;@@@Added by Sonam Gupta MTEch IT Banasthali 2013
;She is of irish origin. [Cambridge]
;वह आयरलैंड मूल की है . 
(defrule origin1
(id-root ?id origin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  origin.clp 	origin1   "  ?id "  mUla )" crlf))
)

;@@@ Added by 14anu-ban-09 on 25-8-14
;Table1.1 lists some of the great physicists, their major contribution and the country of origin. [NCERT CORPUS]
;सारणी 1.1 में कुछ महान भौतिक विज्ञानियों, उनके प्रमुख योगदानों तथा उनके मूल देशों की सूची दी गई है.

(defrule origin2
(declare (salience 5000))
(id-root ?id origin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 some)
(id-word =(- ?id 1) of)
(id-word =(- ?id 2) country)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  origin.clp 	origin2   "  ?id "  mUla )" crlf))
)

;@@@ Added by 14anu-ban-09 on 25-8-14
;The origin of these derived forces is, however, very complex.  [NCERT CORPUS]
;तथापि इन व्युत्पन्न बलों का उद्भव अत्यन्त जटिल है.

(defrule origin3
(id-root ?id origin)
?mng <-(meaning_to_be_decided ?id)
;(Domain physics)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 force)  ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  origin.clp 	origin3   "  ?id "  uxBava )" crlf))
)



