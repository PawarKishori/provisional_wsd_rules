;@@@ Added by 14anu-ban-04 (28-04-2015)
;Police say the body had been dismembered.            [oald]
;पुलिस कहती है कि शरीर को  काटा गया था .                        [self]
(defrule dismember1
(declare (salience 20))
(id-root ?id dismember)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 body)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))  
(assert (id-wsd_root_mng ?id kAta))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  dismember.clp    dismember1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dismember.clp 	dismember1   "  ?id "  kAta)" crlf))
)

;@@@ Added by 14anu-ban-04 (28-04-2015)
;The police found the dismembered body of a young man in the murderer's freezer.            [cald]
;हत्यारे के फ्रीज़र में  पुलिस को युवक का कटा हुआ शरीर मिला .                                                  [self]
(defrule dismember2
(declare (salience 30))
(id-root ?id dismember)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id katA_huA))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dismember.clp 	dismember2   "  ?id "  katA_huA)" crlf))
)

;@@@ Added by 14anu-ban-04 (28-04-2015)
;The UN protested at the dismembering of Bosnia.              [cald]
;बोस्निआ के विभाजन पर यू एन ने  विरोध किया .                              [self]
(defrule dismember3
(declare (salience 30))
(id-root ?id dismember)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBAjana))      
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dismember.clp 	dismember3   "  ?id " viBAjana)" crlf))
)

;------------------------------- Default Rules ----------------------------------------

;@@@ Added by 14anu-ban-04 (28-04-2015)
;The British railway network has gradually been dismembered.             [oald]
;बर्तानिया रेलवे नेटवर्क को धीरे धीरे विभाजित किया गया है . 
(defrule dismember0
(declare (salience 10))
(id-root ?id dismember)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id viBAjiwa_kara))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  dismember.clp    dismember0   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dismember.clp 	dismember0   "  ?id "  viBAjiwa_kara)" crlf))
)

