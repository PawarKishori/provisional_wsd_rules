;@@@ Added by 14anu-ban-06  (01-10-2014)
;The dome gives the edges of the peak insertion. (parallel corpus)
;यह गुम्बद के किनारों को शिखर पर सम्मिलन देता है ।
;Its architectural style person , Tukr , Indian and Islamic architecture Hutkao is a unique insertion .(parallel corpus)
;इसकी वास्तु शैली फारसी तुर्क भारतीय एवं इस्लामिक वास्तुकला के घटकों का अनोखा सम्मिलन है ।
;While Wikipedia articles generally attain a good standard after editing, it is important to note that fledgling articles and those monitored less well may be susceptible to vandalism and insertion of false information.(wiki)
;जब कि विकिपीडिया लेख आम तौर पर सम्पादन के बाद एक अच्छा मानक प्राप्त करते हैं, यह देखना महत्त्वपूर्ण है कि फ्लेज्लिङ लेख और जिन्हे कम मोनीटर किया  वे झूठी सूचना के सम्मिलन और ध्वंस के आशुप्रभावित हो सकते हैं.(manual)
(defrule insertion0
(declare (salience 0))
(id-root ?id insertion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammilana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insertion.clp 	insertion0   "  ?id "  sammilana )" crlf))
)

;@@@ Added by 14anu-ban-06  (01-10-2014)
;The insertion of an extra paragraph.  (OALD)
;एक अतिरिक्त अनुच्छेद को जोडना.(manual)
(defrule insertion1
(declare (salience 2000))
(id-root ?id insertion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 paragraph|page|row|column|line)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insertion.clp 	insertion1   "  ?id "  jodZa )" crlf))
)
