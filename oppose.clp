
;--------------------------------------------DEFAULT RULE----------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-09 on (10-10-2014)
;He intends to oppose the prime minister in the leadership election. [OALD]
;vaha newqwva cunAva meM praXAnamaMwrI kA viroXa karanA cAhawA hE. [Self]

(defrule oppose0
;(declare (salience 5000))
(id-root ?id oppose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  oppose.clp 	oppose0   "  ?id "  viroXa_kara )" crlf))
)

;-----------------------------------------------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (10-10-2014)
;We have learned in Mechanics (see Class XI book, Chapter 6) that a force on a particle does work if the force has a component along (or opposed to) the direction of motion of the particle. [NCERT CORPUS] ;added by 14anu-ban-09 on (24-11-2014)
;hamane yAnwrikI (kakRA 11 kI pATyapuswaka kA aXyAya 6 xeKie ) meM yaha sIKA hE ki yaxi kisI bala kA kaNa kI gawi kI xiSA meM (aWavA usake viparIwa) koI avayava hE wo vaha bala usa kaNa BOwikI para kArya karawA hE. [NCERT CORPUS] ;added by 14anu-ban-09 on (24-11-2014)
;hamane yAnwrikI (kakRA 11 kI pATyapuswaka kA aXyAya 6 xeKie ) meM yaha sIKA hE ki yaxi kisI bala kA kaNa kI gawi kI xiSA meM (aWavA usake viroXI) koI avayava hE wo vaha bala usa kaNa BOwikI para kArya karawA hE. [Self] ;added by 14anu-ban-09 on (24-11-2014)
;The opposing forces such as friction (solids) and viscous forces (for fluids) are always present in the natural world. [NCERT CORPUS]
;prakqwi meM saxEva hI viroXI GarRaNa bala (TosoM ke bIca) aWavA SyAna bala (waraloM ke bIca) Axi upasWiwa rahawe hEM . [NCERT CORPUS]

(defrule oppose1
(declare (salience 5000))
(id-root ?id oppose)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 force|component) ;added 'component' by 14anu-ban-09 on (24-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  oppose.clp 	oppose1   "  ?id "  viroXI )" crlf))
)

