;Modified by Meena(12.5.11)
;The suspect was about 2 meters tall.
(defrule suspect0
(declare (salience 5000))
(id-root ?id suspect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanxigXa))
;(assert (id-wsd_root_mng ?id sanxehayukwa_manuRya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suspect.clp 	suspect0   "  ?id "  sanxigXa )" crlf))
)

;"suspect","Adj","1.sanxehayukwa_manuRya"
;Two suspects ran away after seeing the police.


;@@@ Added by 14anu-ban-01 on (23-02-2015)
;She was suspected of being a drug dealer.[cald]
;उस पर  एक ड्रग व्यापारी होने का सन्देह किया गया था. [self]
(defrule suspect2
(declare (salience 4900))
(id-root ?id suspect)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-karma  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanxeha_kara))
(assert (id-wsd_viBakwi ?id1 para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* " suspect.clp 	suspect2 "  ?id " para)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suspect.clp 	suspect2   "  ?id "  sanxeha_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-02-2015)
;Three suspected terrorists have been arrested.[cald]-->Run on parse no.2
;तीन  संदिग्ध आतङ्कवादी पकडे गये हैं . [self]
(defrule suspect3
(declare (salience 0))
(id-word ?id suspected)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanxigXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suspect.clp 	suspect3 "  ?id "  sanxigXa )" crlf))
)



;@@@ Added by 14anu-ban-01 on (23-02-2015)
;We had no reason to suspect he might try to kill himself.[cald:'that' is dropped here]
;हमें यह लगने का कोई कारण नहीं था कि वह स्वयं को मारने का प्रयास कर सकता है . [self]
(defrule suspect4
(declare (salience 4900))
(id-root ?id suspect)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ? ?id)
(not(kriyA-object ?id ?))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suspect.clp 	suspect4 "  ?id "  laga )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-02-2015)
;We had no reason to suspect that he might try to kill himself.[cald]
;हमें यह लगने का कोई कारण नहीं था कि वह स्वयं को मारने का प्रयास कर सकता है . [self]
(defrule suspect5
(declare (salience 4900))
(id-root ?id suspect)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 that)
(kriyA-vAkyakarma  ?id ?id1)
(kriyA-vAkya_viBakwi  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suspect.clp 	suspect5 "  ?id "  laga )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-02-2015)
;No one knows who killed her, but the police suspect her husband..[cald]
;उसको किसने मारा कोई भी नहीं जानता है, परन्तु पुलिस को उसके पति पर सन्देह है .  [self:more natural]
;उसको किसने मारा कोई भी नहीं जानता है, परन्तु पुलिस उसके पति पर सन्देह करती है . [self:more faithful]
(defrule suspect6
(declare (salience 4900))
(id-root ?id suspect)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanxeha_kara))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  suspect.clp 	suspect6   "  ?id " para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suspect.clp 	suspect6   "  ?id "  sanxeha_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;"suspect","V","1.sanxeha_karanA"
;Police suspects the Ram when he was seen with the robberers.
(defrule suspect1
(declare (salience 4900))
(id-root ?id suspect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanxeha_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suspect.clp 	suspect1   "  ?id "  sanxeha_kara )" crlf))
)

;
