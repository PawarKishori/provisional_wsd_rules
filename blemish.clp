;@@@Added by 14anu-ban-02(07-02-2015)
;Sentence: The incident blemished his reputation. [mw]
;Translation: घटना ने उसकी प्रतिष्ठा में दाग लगाया . [anusaaraka]
(defrule blemish0 
(declare (salience 0)) 
(id-root ?id blemish) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id xAga_lagA))
(assert (kriyA_id-object_viBakwi ?id meM)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  blemish.clp  blemish0    "  ?id " meM)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blemish.clp  blemish0  "  ?id "  xAga_lagA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(07-02-2015)
;Sentence: A series of burn marks blemish the table's surface.[mw]
;Translation: जलने  के निशानों की  शृंखला  मेज की सतह को खराब करती हैं . [manual]
(defrule blemish1 
(declare (salience 100)) 
(id-root ?id blemish) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)	;added by 14anu-ban-02(23-02-2015)
(viSeRya-of_saMbanXI  ?id1 ?id2)	;added by 14anu-ban-02(23-02-2015)
(id-root ?id2 mark)	;added by 14anu-ban-02(23-02-2015)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id KarAba_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  blemish.clp  blemish1    "  ?id " ko)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blemish.clp  blemish1  "  ?id "  KarAba_kara )" crlf)) 
) 

;@@@Added by 14anu-ban-02(07-02-2015)
;This latest revelation has seriously blemished the governor's reputation.[cambridge]
;इस नवीनतम रहस्योद्घाटन ने राज्यपाल की प्रतिष्ठा को गम्भीर रूप से कलंकित किया.[manual]
(defrule blemish2 
(declare (salience 100)) 
(id-root ?id blemish) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id ?id1)	;needs more example for restriction
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kalaMkiwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  blemish.clp  blemish2    "  ?id " ko)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blemish.clp  blemish2  "  ?id "  kalaMkiwa_kara )" crlf)) 
) 
