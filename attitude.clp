;@@@Added by Gourav Sahni 14anu15 (MNNIT ALLAHABAD) on 14.06.2014 email-id:sahni.gourav0123@gmail.com
;Youth is simply an attitude of mind. 
;युवा अवस्था मन का वास्तव में एक दृष्टिकोण है . 
(defrule attitude0
(declare (salience 5000))
(id-root ?id attitude)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xqRtikoNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attitude.clp 	attitude0   "  ?id "  xqRtikoNa )" crlf)))

;$$$ Modified by 14anu-ban-02(15-01-2015)
;meaning changed from svaBAva to ravEyA
;The government attitude towards single parents.
;सरकार का रवैया एकल अभिभावक की ओर. 
;@@@Added by Gourav Sahni 14anu15 (MNNIT ALLAHABAD) on 14.06.2014 email-id:sahni.gourav0123@gmail.com
;The government attitude towards single parents. 
;एक ही माँ बाप की ओर सरकारी स्वभाव .

(defrule attitude1
(declare (salience 4900))
(id-root ?id attitude)
?mng <-(meaning_to_be_decided ?id)
;(or (viSeRya-towards_saMbanXI ?id ?id1)(viSeRya-viSeRaNa ?id ?id1))	;commented by 14anu-ban-02(15-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ravEyA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  attitude.clp 	attitude1   "  ?id "  ravEyA )" crlf)))


