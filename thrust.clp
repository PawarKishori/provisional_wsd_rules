
;@@@ Added by Prachi Rathore
;Rajvir carefully [wrapped] the clay in a piece of paper and[ thrust] it into his pocket.[gyan-nidhi]
;राजवीर ने उस मिट्टी को कागज के एक टुकड़े में सावधानी पूर्वक लपेटा और उसे अपनी जेब में खिसका लिया।
(defrule thrust2
(declare (salience 5000))
(id-root ?id thrust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KisakA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thrust.clp 	thrust2   "  ?id "  KisakA_le )" crlf))
)

;@@@ Added by 14anu23 13/06/2014
;Honesty is the main thrust of their policy.
;ईमानदारी उनकी नीति का मुख्य विषय है.
(defrule thrust3
(declare (salience 5100)) ; salience increased from 4900 to 5100, by 14anu-ban-07 (16-12-2014), due to thrust0
(id-root ?id thrust)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya_viRaya ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thrust.clp 	thrust3   "  ?id "  muKya_viRaya  )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-03-2016)
;Here we remark on two principal thrusts in physics: unification and reduction.	[NCERT corpus]
;यहाँ हम भौतिकी के दो प्रमुख विचारों-एकीकरण तथा न्यूनीकरण पर ही टिप्पणी करेँगे.	[NCERT corpus]
(defrule thrust4
(declare (salience 5100))
(id-root ?id thrust)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id1 ?id)
(id-root ?id1 remark)	
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thrust.clp 	thrust4   "  ?id " vicAra)" crlf))
)
;----------------------- Default rules --------------------

;"thrust","N","1.bala"
;He enlivened his editorials with barbed thrusts at politicians.
(defrule thrust0
(declare (salience 5000))
(id-root ?id thrust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thrust.clp 	thrust0   "  ?id "  bala )" crlf))
)


;"thrust","VT","1.TelanA"
;He thrust his chin forward.
(defrule thrust1
(declare (salience 4900))
(id-root ?id thrust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Tela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thrust.clp 	thrust1   "  ?id "  Tela )" crlf))
)

