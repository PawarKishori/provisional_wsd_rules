;@@@ Added by 14anu-ban-01 on (30-10-2014)
;So, if gravity and other forces (e.g. air resistance) were ineffective, liquid drops would be spherical.[NCERT corpus]
;अतः यदि गुरुत्व बल तथा अन्य बल (उदाहरणार्थ वायु-प्रतिरोध) निष्प्रभावी हों तो द्रव की बूँदें गोल होती हैं.[NCERT corpus]
(defrule spherical0
(declare (salience 0))
(id-root ?id spherical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spherical.clp 	spherical0   "  ?id "  gola )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-10-2014)
;The following rule can be generalized for 'feminine' gender when the fact for it will be decided later on.
;Some problems in spherical trigonometry are cumbersome.[self:with reference to COCA]
;गोलीय त्रिकोणमीति में कुछ सवाल जटिल होते हैं.[self]
(defrule spherical1
(declare (salience 100))
(id-root ?id spherical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 trigonometry)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spherical.clp 	spherical1   "  ?id "  golIya )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-10-2014)
;The following rule can be generalized for 'masculine' gender when the fact for it will be decided later on.
;Spherical aberration is an optical effect.[self:with reference to COCA]
;गोलाकार विपथन एक प्रकाश संबंधी परिणाम है.[self]
(defrule spherical2
(declare (salience 100))
(id-root ?id spherical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 aberration)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golAkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spherical.clp 	spherical2   "  ?id "  golAkara )" crlf))
)
