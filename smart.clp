
;$$$ Modified by 14anu-ban-11 on (07-03-2015)
;He could not dare to act smart
(defrule smart0
(declare (salience 5000))
(id-root ?id smart)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)         ;Added by 14anu-ban-11 on (07-03-2015)
(id-word ?id1 act)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cawurAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smart.clp 	smart0   "  ?id "  cawurAI )" crlf))
)



;;@@@ Added by 14anu-ban-11 on (04-03-2015)
;You look very smart in that suit.(oald)
;आप उस सूट में बहुत अच्छे दिखते हैं . (self)
(defrule smart4
(declare (salience 4901))
(id-root ?id smart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
(id-root ?id1 very)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smart.clp 	smart4   "  ?id "  acCA)" crlf))
)


;Note:- working properly on parser no.8.
;@@@ Added by 14anu-ban-11 on (05-03-2015)
;She's smarter than her brother.(oald)
;वह अपने भाई की अपेक्षा अधिक बुद्धिमान है . (self)
(defrule smart5
(declare (salience 5001))
(id-root ?id smart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-than_saMbanXI  ?id ?id1)
(id-root ?id1 brother)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buxXimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smart.clp 	smart5   "  ?id "  buxXimAna)" crlf))
)

;@@@ Added by 14anu-ban-11 on (05-03-2015)
;He was struck with a smart crack on the head.(oald)
;वह सिर पर एक जोरदार चोट से मर गया था . (self)
(defrule smart6
(declare (salience 5002))
(id-root ?id smart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 crack)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joraxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smart.clp 	smart6   "  ?id "  joraxAra)" crlf))
)


;@@@ Added by 14anu-ban-11 on (05-03-2015)
;They are still smarting from the 4–0 defeat last week.(oald)
;वे पिछले सप्ताह  4–0 की हार से अभी भी दुःखी महसूस कर रहे हैं . (self)
(defrule smart7
(declare (salience 4901))
(id-root ?id smart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 they)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuHKI_mahasUsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smart.clp 	smart7   "  ?id "  xuHKI_mahasUsa_kara)" crlf))
)

;------------------------ Default Rules ----------------------

(defrule smart1
(declare (salience 4900))
(id-root ?id smart)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cuswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smart.clp 	smart1   "  ?id "  cuswa )" crlf))
)

(defrule smart2
(declare (salience 4800))
(id-root ?id smart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cuswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smart.clp 	smart2   "  ?id "  cuswa )" crlf))
)

;"smart","V","1.tIsanA"
;Young lovers smart with pangs of separation which more often than not becomes unbearable.
(defrule smart3
(declare (salience 4700))
(id-root ?id smart)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tIsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  smart.clp 	smart3   "  ?id "  tIsa )" crlf))
)

