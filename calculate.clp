;"calculating","Adj","1.mawalabI"
;Rana is the most calculating and selfish boy in our class.

(defrule calculate0
(declare (salience 5000))
(id-root ?id calculate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id calculating )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mawalabI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  calculate.clp  	calculate0   "  ?id "  mawalabI )" crlf))
)

;Ritesh Srivastava 12-june-2011
;His last words were calculated to wound her
;usake piCale Sabxa usako/use GAyala karane ke liye "racanA karAye/karAe" gaye We.


(defrule calculate1
(declare (salience 4900))
(id-root ?id calculate)
(id-root ?id1 to)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?)
(to-infinitive ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id racanA_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  calculate.clp 	calculate1   "  ?id "  racanA_karA )" crlf))
)


; 
;"calculation","N","1.hisAba_kiwAba/leKA"
;All your calculations about the firm's profit are wrong.
;
(defrule calculate2
(declare (salience 4800))
(id-root ?id calculate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hisAba_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  calculate.clp 	calculate2   "  ?id "  hisAba_lagA )" crlf))
)

;default_sense && category=verb	hisAba_lagA	0
;"calculate","VT","1.hisAba_lagAnA"
;We have to calculate the estimated cost of our journey.
;
;

;Ritesh Srivastava 12-june-2011

;calculated
;That was a calculated attempt to embarrass the Chancellor.
;vaha maMwrI embarras ke liye eka "AMkalana" prayAsa WA.
;It was a cruel, calculated crime with absolutely no justification.

(defrule calculate3
(declare (salience 4700))
(id-root ?id calculate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AMkalana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  calculate.clp 	calculate3   "  ?id "  AaMklana )" crlf))
)

;@@@ Added by 14anu-ban-03 (21-11-2014)
;Thus the practice of calculating the fertilizer equivalent value of the nutrients in crop residue is a reasonable guide to estimating the partial value of crop residues. [agriculture]
;इस प्रकार फसल अवशिष्ट में पोषक तत्वों के उर्वरक तुल्य मूल्य की जाँच करने की कार्यप्रणाली है जो फसल अवशिष्टों की असामान्य मूल्य को मूल्यांकन करने के लिए एक उचित मार्गदर्शक गाइड हैं.[Manual]
(defrule calculate4
(declare (salience 4800))
(id-root ?id calculate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id calculating )
(viSeRya-of_saMbanXI ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  calculate.clp 	calculate4   "  ?id "  jAzca_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (27-11-2014)
;From the wave equation, Maxwell could calculate the speed of electromagnetic waves in free space and he found that the theoretical value was very close to the measured value of speed of light.[ncert]
;मैक्सवेल तंरग समीकरणों का उपयोग कर मुक्त आकाश में, वैद्युतचुम्बकीय तरङ्गों के वेग की गणना कर पाए और उन्होंने पाया कि तरङ्ग वेग का यह सैद्धान्तिक मान, प्रकाश की चाल के मापे गए मान के अत्यन्त निकट है.[ncert]
;In Section 4.6 of the previous chapter we have calculated the magnetic field on the axis of a circular current loop. [ncert]
;piCale aXyAya ke anuBAga 4.6 meM hamane eka vqwwAkAra lUpa ke akRa para cumbakIya kRewra kI gaNanA kI WI. [ncert]
(defrule calculate5
(declare (salience 4800))
(id-root ?id calculate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 speed|field)                   ;added 'field' by 14anu-ban-03 (08-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaNanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  calculate.clp    calculate5   "  ?id "  gaNanA_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (29-11-2014)
;The positions of maximum and minimum intensities can be calculated by using the analysis given in Section 10.4 where we had shown that for an arbitrary point P on the line GG ′ (Fig. 10.12 (b)) to correspond to a maximum.[ncert]
;अधिकतम तथा न्यूनतम तीव्रता की स्थितियों की गणना अनुच्छेद 10.4 में दिए गए विश्लेषण का उपयोग करके की जा सकती है, जहाँ पर हमने रेखा GG ′ [चित्र 10.12(b) ] पर एक यथेच्छ बिंदु P लिया जो अधिकतम तीव्रता के सङ्गत करता है. [ncert]
(defrule calculate6
(declare (salience 4800))
(id-root ?id calculate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 position)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaNanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  calculate.clp    calculate6   "  ?id "  gaNanA )" crlf))
)


