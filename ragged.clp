;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;These ragged children need help. [self: with reference to oald]
;ये फटे पुराने वस्त्र पहने हुए बच्चों को मदद की ज़रूरत है .   [self]
(defrule ragged1
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pate_purAne_vaswra_pahanA_huA/PatIcara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged1   "  ?id "  Pate_purAne_vaswra_pahanA_huA/PatIcara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;Once nails are dry, smooth down ragged cuticles with oil.[COCA]
;जब नाखून सूखे हों,खुरखुरी उपत्वचा को तेल से चिकनी कीजिए .  [self]
(defrule ragged2
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 cuticle) 	;list can be added
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KuraKurA/KuraxarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged2   "  ?id "  KuraKurA/KuraxarA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;I sat on a rock beside the ragged coastline.[self: with reference to oald]
;मैं विषम समुद्री किनारे के पास चट्टान पर बैठा . [self]
(defrule ragged3
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 cloud|coastline) 	;list can be added
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged3   "  ?id "  viRama )" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;I could hear the sound of his ragged breathing. [oald]
;मैं उसके अनियमित श्वसन की आवाज सुन पा रहा था.  	[self]
;मैं उसकी उखड़ी हुई साँसों की आवाज सुन पा रहा था. 	[self]
(defrule ragged5
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 breathing|cough) 	;list can be added
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aniyamiwa/uKadA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged5   "  ?id "  aniyamiwa/uKadA_huA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;The old photograph looked a little ragged at the edges.[oald]
;पुराना फोटो  किनारों पर थोडा खुरदुरा लगा . [self]
(defrule ragged6
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?)
(kriyA-object  ?id1 ?id)
(id-root ?id2 corner|edge|side|surface)
(kriyA-at_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KuraxurA/KuraKurA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged6   "  ?id "  KuraxurA/KuraKurA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;The thought of climbing on those ragged cliffs frightened me.[self:with reference to learnersdictionary]
;उन नुकीली/दाँतेदार चट्टानों पर चढने के विचार ने मुझे डरा दिया . [self]
(defrule ragged7
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 cliff) 	
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nukIlA/xAzwexAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged7   "  ?id "  nukIlA/xAzwexAra )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;He looked ragged, so I told him to go to bed.[ http://www.ldoceonline.com/dictionary/]
;वह थका हुआ दिखा, इसलिए मैंने उसको सोने के लिये जाने के लिए कहा . [self]
(defrule ragged8
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id1 look|seem|be)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(kriyA-object  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WakA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged8   "  ?id "  WakA_huA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;Their performance was still very ragged.[oald]
;उनका प्रदर्शन अभी भी अत्यन्त घटिया था .  [self]
(defrule ragged9
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 performance|presentation)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged9   "  ?id "  GatiyA )" crlf))
)



;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;We are running on the ragged edge financially.[learnersdictionary]
;हम आर्थिक रूप से विफल/असफल चल रहे हैं . [self]
(defrule ragged10
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 edge)
(kriyA-on_saMbanXI  ? ?id1)
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id3)
(pada_info (group_head_id ?id1)(preposition ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 ?id3 viPala/asaPala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  ragged.clp  	ragged10    " ?id "  " ?id1 " " ?id2 "  " ?id3 "  viPala/asaPala )" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;Our budget is on the ragged edge as it is.[learnersdictionary]
;हमारा बजट  जैसा का तैसा तङग है  . [self]
(defrule ragged11
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 edge)
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-on_saMbanXI  ? ?id1)
(viSeRya-det_viSeRaNa  ?id1 ?id3)
(pada_info (group_head_id ?id1)(preposition ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 ?id3 wafga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  ragged.clp  	ragged11    " ?id "  " ?id1 " " ?id2 "  " ?id3 "  wafga )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;All this travel is running me ragged![learnersdictionary]
;यह यात्रा न मुझे क्लान्त  कर रही है! [self]
(defrule ragged12
(declare (salience 5000))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(viSeRya-viSeRaNa  ?id1 ?id)
(kriyA-object  ?id2 ?id1)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id klAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp  	ragged12   "  ?id "  klAnwa )" crlf))
)

;--------------------------- Default Rules ------------------------------------

;@@@ Added by 14anu-ban-01 on (18-04-2015) 
;He has a ragged coat.[rag.clp]
;उसके पास फटा पुराना कोट है .  [self]
(defrule ragged0
(declare (salience 0))
(id-root ?id ragged)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PatA_purAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ragged.clp    ragged0   "  ?id "  PatA_purAnA )" crlf))
)


