;@@@Added by 14anu-ban-02(16-03-2015)
;‘Ambulance’ is written backwards so you can read it in the mirror.[oald]
;'ऐम्ब्यलन्स' उल्टा लिखा हुआ है जिससे आप  इसको  दर्पण में  पढ़ सकते हैं .[self]
(defrule backwards1 
(declare (salience 100)) 
(id-root ?id backwards) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-word ?id2 ambulance)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id ultA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  backwards.clp  backwards1  "  ?id "  ultA )" crlf)) 
) 


;@@@Added by 14anu-ban-02(16-03-2015)
;Rutherford argued that, to deflect the α-particle backwards, it must experience a large repulsive force.[ncert 12_12]
;रदरफोर्ड ने तर्क किया कि ऐल्फा-कणों को विपरीत दिशा में विक्षेपित करने के लिए, इन पर बहुत अधिक प्रतिकर्षण बल लगना चाहिए.[ncert]
(defrule backwards2 
(declare (salience 100)) 
(id-root ?id backwards) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 deflect)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id viparIwa_xiSA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  backwards.clp  backwards2  "  ?id "  viparIwa_xiSA )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(16-03-2015)
;Sentence: He took a step backwards.[oald]
;Translation: उसने एक कदम पीछे की ओर लिया. [self]
(defrule backwards0 
(declare (salience 0)) 
(id-root ?id backwards) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adverb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pICe_kI_ora)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  backwards.clp  backwards0  "  ?id "  pICe_kI_ora )" crlf)) 
) 



