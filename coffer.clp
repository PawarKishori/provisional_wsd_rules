;@@@ Added by 14anu-ban-03 (11-03-2015)
;The mahajan hoarded his wealth in a coffer. [hinkhoj]
;महाजन ने तिजोरी में अपने धन का संचय किया . [manual]
(defrule coffer0
(declare (salience 00)) 
(id-root ?id coffer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wijorI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coffer.clp 	coffer0   "  ?id " wijorI )" crlf))
)



;@@@ Added by 14anu-ban-03 (11-03-2015)
;Bad economic policy of the government can leave the nations coffer empty.[oald]
;सरकार की खराब आर्थिक नीति राष्ट्र धन को खाली कर सकती है . [manual]
(defrule coffer1
(declare (salience 10)) 
(id-root ?id coffer)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 nation)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coffer.clp 	coffer1  "  ?id "  Xana )" crlf))
)
