;Added by Pramendra Rathi : MNNIT student(7-6-11)
;This is a scrap of paper.
(defrule scrap0
(declare (salience 2500))
(id-root ?id scrap)
?mng <- (meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id1 ?id)
;(or(viSeRya-of_saMbanXI 	?id 	 ?)(id-root =(+ ?id 1) of))
(id-cat_coarse ?id1 verb)
;(not(id-cat_coarse ?id verb)) 	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tukadA))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrap.clp 	scrap0  " ?id "  tukadA )" crlf)))

;Added by Pramendra Rathi : MNNIT student(7-6-11)
;This is a scrap.
(defrule scrap1
(declare (salience 3000))
(id-root ?id scrap)
?mng <- (meaning_to_be_decided ?id)
(not(id-cat_coarse ?id verb))
(or(subject-subject_samAnAXikaraNa ?	?id)(samAsa_viSeRya-samAsa_viSeRaNa ? ?id) (id-cat_coarse ?id adjective)(id-root =(+ ?id 1) material|iron|copper|matel|timber|dealer|wood|book)
(id-root =(+ ?id 1) in)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raxxI))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrap.clp 	scrap1  " ?id "  raxxI )" crlf)))
;-----------------------------------------------------------------------------------------
;Added by Pramendra Rathi : MNNIT student(7-6-11)
;we should scrap the old airplane and sell the parts.
(defrule scrap2
(declare (salience 3200))
(id-root ?id scrap)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id2 airplane|machine|tank|car|truck|vehicle|missile|bomb|frigates|weapon|aircraft|carrier|BRM|shell|helicopter|submarine|ship|aeroplane|squadron)
(or(kriyA-object 	?id ?id2)
(kriyA-subject ?id ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kabAda_me_baxala))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrap.clp 	scrap2  " ?id "  kabAda_me_baxala))" crlf)))
;-----------------------------------------------------------------------------------------
;Added by Pramendra Rathi : MNNIT student(7-6-11)(This rule is not working fine because i couldnot catogrize for this rule.i tried a lot to add object but they are a lot and have no similarity)
;He is scrapping proposals to privatise all the nuclear reactors of the country.
(defrule scrap3
(declare (salience 3100))
(id-root ?id scrap)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raxxa_kara))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrap.clp 	scrap3  " ?id "  raxxa_kara)" crlf)))

;------------------------------------------------------------------------------------------------------------------------
;Added by Pramendra Rathi : MNNIT student(7-6-11)
(defrule scrap8
(declare (salience 3150))
(id-root ?id scrap)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object 	?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raxxa_kara))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrap.clp 	scrap8  " ?id "  raxxa_kara)" crlf)))
;------------------------------------------------------------------------------------------------------------------------
;Added by Pramendra Rathi : MNNIT student(7-6-11)
;THis is a scrap heap.
(defrule scrap4
(declare (salience 4000))
(id-root ?id scrap)
(id-root =(+ ?id 1) heap) 
?mng <- (meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) raxxI_kA_Dera))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* "  scrap.clp	scrap4  " ?id " "=(+ ?id 1)"  raxxI_kA_Dera)" crlf)
))

;--------------------------------------------------------------------------------------------------------------------------
;Added by Pramendra Rathi : MNNIT student(7-6-11)
;The creatures of Woodrow's imagination remain attached to their scrap metal hosts, making every sculpture a parasite. 
(defrule scrap5
(declare (salience 4100))
(id-root ?id scrap)
(id-root =(+ ?id 1) metal) 
?mng <- (meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) raxxI_XAwu))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* "  scrap.clp	 scrap5  " ?id " "=(+ ?id 1)"  raxxI_XAwu))" crlf)
))
;--------------------------------------------------------------------------------------------------------------------------
;Added by Pramendra Rathi : MNNIT student(7-6-11)
;This is a scrap book.
(defrule scrap6
(declare (salience 4200))
(id-root ?id scrap)
(id-root =(+ ?id 1) book) 
?mng <- (meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) kawarana_rajistara))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* "  scrap.clp	 scrap6  " ?id " "=(+ ?id 1)"  kawarana_rajistara))" crlf)
))
;---------------------------------------------------------------------------------------------------------------------------
;Added by Pramendra Rathi : MNNIT student(7-6-11)
;This is a scrap yard.
(defrule scrap7
(declare (salience 4300))
(id-root ?id scrap)
(id-root =(+ ?id 1) yard) 
?mng <- (meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) raxxIKAnA))
(if ?*debug_flag*
then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* "  scrap.clp	 scrap7  " ?id " "(+ ?id 1)"  raxxIKAnA))" crlf)
)
)

;$$$ Modified by 14anu-ban-01 on (27-12-2014)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 27.06.14
;There's not a scrap of evidence to support his claim.
;उसके दावे को सहारा देने के लिए थोडा सा सबूत नहीं है .
;उसके दावे को सहारा देने के लिए थोडा भी सबूत नहीं है .[Translation improved by 14anu-ban-01 on (27-12-2014)]
(defrule scrap9
(declare (salience 3500))
(id-root ?id scrap)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 evidence|proof|authentic|attest|difference)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA_BI/WodA_sA_BI));changed "WodA_sA" to "WodA_BI" by 14anu-ban-01 on (27-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrap.clp 	scrap9   "  ?id "  WodA_BI/WodA_sA_BI )" crlf));changed "WodA_sA" to "WodA_BI" by 14anu-ban-01 on (27-12-2014)
)

;$$$ Modified by 14anu-ban-01 on (27-12-2014)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 27.06.14
;We sold the car for scrap.
;हमने रद्दी के लिए गाडी बेची .
;हमने गाडी को कबाड़ में बेच दिया [Translation improved by 14anu-ban-01 on (27-12-2014)]
(defrule scrap10
(declare (salience 3500))
(id-root ?id scrap)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kabAdZa));changed "raxxI " to "kabAdZa" by 14anu-ban-01 on (27-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrap.clp 	scrap10   "  ?id "  kabAdZa)" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 27.06.14
;He is always getting into scraps at school.
;वह हमेशा विद्यालय में झगडों में पड रहा है .
;वह हमेशा विद्यालय में झगडों में उलझता /पड़ता रहता है.[Translation improved by 14anu-ban-01 on (27-12-2014)] 
(defrule scrap11
(declare (salience 3500))
(id-root ?id scrap)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id1 ?id)
(viSeRya-at_saMbanXI  ?id ? )
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JagadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrap.clp 	scrap11   "  ?id "  JagadZA )" crlf))
)

