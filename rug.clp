

;@@@ Added by Anita--29.5.2014
;My dog loves lying on the rug in front of the fire. [cambridge dictionary]
;मेरा कुत्ता आग के सामने कालीन पर लेटना पसन्द करता है ।
;merA kuwwA Aga ke sAmane kAlIna para letanA pasanxa karawA hE. [Anusaarak out-put ][verified sentence]
;Persian rugs are characterized by their geometric designs and rich colours. 
;फारसी कालीन उनकी ज्यामितीय डिजाइनों और गहरे रंगों के लिए जाने जाते हैं ।
(defrule rug0
(declare (salience 1000))
(id-root ?id rug)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-on_saMbanXI  ?kri ?id)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rug.clp 	rug0   "  ?id "  kAlIna )" crlf))
)

;@@@ Added by Anita--29.5.2014
;Why don't we hide that stain on the carpet with a scatter rug?
;हम गलीचे के दाग को तितर-बितर कारपेट से क्यों नहीं छुपाते हैं ?
(defrule rug1
(declare (salience 2000))
(id-root ?id rug)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galIcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rug.clp 	rug1   "  ?id "  galIcA )" crlf))
)

;@@@ Added by Anita--29.5.2014
;Sheepskin rug is in the corner of our bedroom. [yourdictionary.com]
;भेड़ की चर्म का रग हमारे शयन-कक्ष के कोने में है ।  
(defrule rug2
(declare (salience 3000))
(id-root ?id rug)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 corner)
(and(kriyA-subject  ?kri ?id)(kriyA-in_saMbanXI  ?kri ?id1))
;(samAsa_viSeRya-samAsa_viSeRaNa  ?id  ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rug.clp 	rug2   " ?id "  raga )" crlf))
)


;####################################default-rule##############################
;@@@ Added by Anita--29.5.2014
;I have a costly rug . 
;मेरे पास एक कीमती रग है ।
(defrule rug_default-rule
(declare (salience 0))
(id-root ?id rug)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rug.clp 	rug_default-rule   "  ?id "  raga )" crlf))
)
