;@@@ Added by 14anu-ban-04 (20-01-2015)
;They decorated the room with flowers and balloons.         [oald]
;उन्होंने फूलों और गुब्बारों से कमरा सजाया .                              [self]
(defrule decorate0
(declare (salience 10))
(id-root ?id decorate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decorate.clp 	decorate0   "  ?id "  sajA )" crlf))
)



;@@@ Added by 14anu-ban-04 (20-01-2015)
;She was decorated for her efforts during the war.           [oald]
;वह युद्ध के समय अपने योगदान के लिए सम्मानित की गयी थीं.                     [self]
;They were decorated for their part in the rescue.            [cald]
;वे उद्धार में उनके भाग के लिए सम्मानित किए गये थे .                           [self]
;All four firefighters were decorated for bravery.            [cald]
;चारों ही फ़ायरमैन  साहस के लिए सम्मानित किए गये थे .                         [self]
(defrule decorate1
(declare (salience 20))
(id-root ?id decorate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))         ;added by 14anu-ban-04 (22-01-2015)
(kriyA-for_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammAniwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decorate.clp 	decorate1   "  ?id "  sammAniwa_kara )" crlf))
)


