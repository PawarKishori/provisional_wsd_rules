;@@@ Added by 14anu-ban-10 on  (11-02-2015)
;He used to rib her mercilessly about her accent.[oald]
;वह उसके स्वर उच्चारण के बारे में निर्दयता से उसकी हँसी उड़ाता है .[self]
(defrule rib2
(declare (salience 5100))
(id-root ?id rib)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hazsI_udA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rib.clp 	rib2   "  ?id "  hazsI_udA )" crlf))
)
;@@@ Added by 14anu-ban-10 on  (11-02-2015)
;A rib cotton sweater.[oald]
;एक उभरी हुई धारी वाली कपास स्वेटर .[self]
(defrule rib3
(declare (salience 5200))
(id-root ?id rib)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uBarI_huI_XArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rib.clp 	rib3   "  ?id "  uBarI_huI_XArI )" crlf))
)
;@@@ Added by 14anu-ban-10 on  (11-02-2015)
;A piece of meat with one or more bones from the ribs of an animal.[oald]
;एक जानवर की पसली का मांस से एक या एक से अधिक हड्डियों के साथ मांस का एक टुकड़ा ।[self]
(defrule rib4
(declare (salience 5300))
(id-root ?id rib)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pasalI_kA_mAMsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rib.clp 	rib4   "  ?id "  pasalI_kA_mAMsa)" crlf))
)
;@@@ Added by 14anu-ban-10 on  (11-02-2015)
;A roof with rib vaulting.[oald]
;मेहराबी छत की तिल्ली के साथ एक छत ।[self]
(defrule rib5
(declare (salience 5400))
(id-root ?id rib)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 vaulting)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id willI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rib.clp 	rib5   "  ?id "  willI)" crlf))
)


;----------------- Default Rules ----------------
;"rib","N","1.pasalI_kI_haddI"
;There are 24 ribs in our body.
(defrule rib0
(declare (salience 5000))
(id-root ?id rib)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pasalI_kI_haddI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rib.clp 	rib0   "  ?id "  pasalI_kI_haddI )" crlf))
)

;"rib","VT","1.waMga_karanA/majAka_banAnA"
;He is ribbed for being shy.
(defrule rib1
(declare (salience 4900))
(id-root ?id rib)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waMga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rib.clp 	rib1   "  ?id "  waMga_kara )" crlf))
)

;"rib","N","1.pasalI_kI_haddI"
;There are 24 ribs in our body.
;--"2.pawwI_kI_nasa"
;The ribs of a leaf keep it straight.
;
;"rib","VT","1.waMga_karanA/majAka_banAnA"
;He is ribbed for being shy.
;--"2.pasalI_caDAnA/jodZanA"
;The doctor is going to join his cracked ribs.
