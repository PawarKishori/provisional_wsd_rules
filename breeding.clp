;@@@Added by 14anu-ban-02(07-04-2015)
;The breeding of horses.[oald]
;घोडों की नस्ल . [self]
(defrule breeding1 
(declare (salience 100)) 
(id-root ?id breeding) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 horse|plant|dog)
 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nasla)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breeding.clp  breeding1  "  ?id "  nasla )" crlf)) 
) 


;------------------------ Default Rules---------------------------------------------

;@@@Added by 14anu-ban-02(07-04-2015)
;Sentence: A sign of good breeding.[oald]
;Translation: अच्छे शिक्षण के लक्षण . [self]
(defrule breeding0 
(declare (salience 0)) 
(id-root ?id breeding) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id SikRaNa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breeding.clp  breeding0  "  ?id "  SikRaNa )" crlf)) 
) 


