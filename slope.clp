
(defrule slope0
(declare (salience 0));reduced from 5000 to 0 by 14anu-ban-01 on 26-08-14.
(id-root ?id slope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slope.clp 	slope0   "  ?id "  DAla )" crlf))
)

;"slope","N","1.DAla"
;You can find high slopes in hilly regions.
;
(defrule slope1
(declare (salience 0));reduced from 4900 to 0 by 14anu-ban-01 on 26-08-14.
(id-root ?id slope)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAlU_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slope.clp 	slope1   "  ?id "  DAlU_ho )" crlf))
)

;"slope","V","1.DAlU_ho"

;@@@ Added by 14anu-ban-01 on 26-08-14.
;Crores of years ago the rocks of the down side river formed of moon-ruins due to some earthquake sloped down.[tourism corpus]
;करोड़ो  वर्ष  पहले  किसी  भूकंप  से  बने  चन्द्र-भ्रंस  से  नदी  के  डाउन  साइड  की  चट्टाने  नीचे  धसक  गई  थीं  ।[tourism corpus]
(defrule slope2
(declare (salience 4900))
(id-root ?id slope)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 down)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xasaka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slope.clp 	slope2   "  ?id "  Xasaka_jA )" crlf))
)
