;@@@ Added by jagriti(11.2.2014)
;The robbers stripped all his money.[rajpal]
;डाकुओं ने उसका सब पैसा छीन लिया . 
(defrule strip0
(declare (salience 5000))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 money|goods|right|honour|property)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CIna_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip0   "  ?id "  CIna_le )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (05-03-2015)
;@@@ Added by jagriti(11.2.2014)
;According to the court order he has to strip the house.[rajpal]
;न्यायालय के आदेश के अनुसार उसको घर खली करना है . 
(defrule strip1
(declare (salience 4900))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 room|house|hostel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KAlI_kara))	;corrected "KalI_kara" to "KAlI_kara"  by 14anu-ban-01 (05-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip1   "  ?id "  KAlI_kara )" crlf))
)						;corrected "KalI_kara" to "KAlI_kara"  by 14anu-ban-01 (05-03-2015)



;$$$ Modified by 14anu-ban-01 on (05-03-2015)
;He strips and cleans his rifle every morning.[oald]
;वह प्रत्येक सुबह अपनी राईफल को खोलता और साफ करता है . [self]
;@@@ Added by jagriti(11.2.2014)
;To strip an engine.[rajpal]
; इंजन खोलना . [Translation changed from "इंजन खोल देना " to "इंजन खोलना " by 14anu-ban-01]
(defrule strip2
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 engine|machine|gun|rifle|pistol)	;added  "gun|rifle|pistol" by 14anu-ban-01 (05-03-2015)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kola))		;changed meaning from "खोल दे " to "खोल" by 14anu-ban-01 (05-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip2   "  ?id "  Kola )" crlf))
)						;changed meaning from "खोल दे " to "खोल" by 14anu-ban-01 (05-03-2015)

;@@@ Added by 14anu-ban-01 on (03-03-2015)
;The floors had been stripped.[self: with reference to oald]
;फर्श काटे गये थे . [self]
(defrule strip9
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 floor|surface|crust|ground)
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip9   "  ?id "  kAta)" crlf))
)

;....Default rule.......
(defrule strip3
(declare (salience 100))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pattI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip3   "  ?id "  pattI )" crlf))
)

;"strip","N","1.pattI"
;cut the paper into strips for decoration.
;--"2.Puta-bAla_tIma_ke_saxasyoM_ke_kapadZe"
;Indians are playing in blue && white strips.
;--"3._He_owns_a_cloth_store_out_on_the_strip"
;


;@@@ Added by 14anu-ban-01 on (05-03-2015)
;He stood there stripped to the waist .[oald]----Parse no 165
;वह वहाँ पर कमर तक निर्वस्त्र/नग्न खडा हुआ था. [self]
(defrule strip03
(declare (salience 100))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirvaswra/nagna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip03   "  ?id "  nirvaswra/nagna )" crlf))
)

(defrule strip4
(declare (salience 100))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kapadZe_uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip4   "  ?id "  kapadZe_uwAra )" crlf))
)

;@@@ Added by 14anu-ban-11 on (28-02-2015)
;Cut the meat into strips.(oald)
;माँस को टुकडों में  काट डालिए . (self)
(defrule strip5
(declare (salience 101))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-into_saMbanXI  ?id1 ?id)
(id-root ?id1 cut)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tukadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip5   "  ?id "  tukadZA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (28-02-2015)
;After the guests had gone, I stripped all the bedsheets.(oald)
;अतिथि के जाने के बाद ,मैंने सभी चादरे उतारली . (self)
(defrule strip6
(declare (salience 200))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-word ?id1 bedsheets)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwAra_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip6   "  ?id " uwAra_xe)" crlf))
)

;@@@ Added by 14anu-ban-11 on (28-02-2015)
;A tiny strip of garden.(oald)
;उद्यान का एक बहुत छोटा भूभाग . (self)
(defrule strip7
(declare (salience 110))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 garden)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUBAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip7   "  ?id "  BUBAga)" crlf))
)

;@@@ Added by 14anu-ban-11 on (28-02-2015)
;He was disgraced and stripped of his title.(oald)
;उसे अपमानित किया गया और उसके  पद से निकाल दिया गया .(self) 
(defrule strip8
(declare (salience 230))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(conjunction-components  ?id1 ?id2 ?id)
(kriyA-of_saMbanXI  ?id2 ?id3)
(id-root ?id1 and)
(id-root ?id2 disgrace)
(id-root ?id3 title)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip8   "  ?id "  nikAla_xe)" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-03-2015)
;We stripped off and ran down to the water.[oald]
;हमने कपडे उतारे और पानी की तरफ भागे . [self]
(defrule strip04
(declare (salience 100))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 off)
(kriyA-upasarga  ?id ?id1)
(not(kriyA-object ?id ?))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kapadZe_uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " strip.clp 	strip04    "  ?id "  "?id1 "  kapadZe_uwAra  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (03-03-2015)
;The floors had been stripped.[self: with reference to oald]
;फर्श काटे गये थे . [self]
(defrule strip05
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 floor|surface|crust|ground)
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip05   "  ?id "  kAta)" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-03-2015)
;I stripped off bark to use for kindling.[coca]
;आग जलाने में प्रयोग करने के लिए मैंने वृक्ष की छाल को छील लिया .[self: more natural]
(defrule strip06
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id3)
(id-root ?id3 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id1 bark)
(kriyA-object  ?id ?id1)
(id-root ?id2 off)
(kriyA-upasarga  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 CIla_le))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  strip.clp 	strip06   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " strip.clp 	strip06    "  ?id "  "?id2 "  CIla_le  )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-03-2015)
;Deer had stripped all the bark off the tree.[oald]
;हिरन ने पेड की सारी छाल छील दी थी. [self]
(defrule strip07
(declare (salience 4700))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 bark)
(kriyA-object  ?id ?id1)
(kriyA-off_saMbanXI  ?id ?id3)
(id-root ?id2 off)
(pada_info (group_head_id ?id3)(preposition ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 CIla_xe))
(assert (id-wsd_viBakwi ?id3 kA))	;viBakwi should be 'kI'.It's root is 'kA'.Henceforth I have added 'kA'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  strip.clp 	strip07   "  ?id3 " kA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  strip.clp 	strip07 "  ?id "  "?id3 "   CIla_xe )" crlf)
)
)


;@@@ Added by 14anu-ban-01 on (05-03-2015)
;The inspector was stripped off his rank.[strip.clp]
;निरीक्षक को उसकी पदवी से निकाल दिया गया था .  [self]
(defrule strip08
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 rank|position)
(kriyA-object ?id ?id1)
(id-root ?id2 off)
(kriyA-upasarga  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 nikAla_xe))
(assert (kriyA_id-object_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  strip.clp 	strip08   "  ?id " se )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " strip.clp 	strip08    "  ?id "  "?id2 "  nikAla_xe  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (05-03-2015)
;She stripped off her clothes and got into bed.[oald]
;उसने अपने  कपड़े/वस्त्र उतारे और बिस्तर में चली गई .  [self]
(defrule strip09
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 off)
(kriyA-upasarga  ?id ?id1)
(id-root ?id2 clothes|dress|frock|pant|shirt|suit|watch)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " strip.clp 	strip09    "  ?id "  "?id1 "  uwAra  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (05-03-2015)
;He was stripped naked and left in a cell.[oald]
;उसे निर्वस्त्र करके कोठरी में छोड दिया गया था .   [self:more natural]
(defrule strip10
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) naked)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) nirvaswra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " strip.clp 	strip10    "  ?id "  "(+ ?id 1) "  nirvaswra_kara )" crlf))
)


;@@@ Added by 14anu-ban-01 on (05-03-2015)
;After the guests had gone, I stripped all the beds.[oald]
;अतिथियों के जाने के बाद ,मैंने सभी बिछौनों पर से चादर हटा दिए .   [self]
(defrule strip11
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 bed|cot)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cAxara_hatA_xe))
(assert (kriyA_id-object_viBakwi ?id para_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  strip.clp 	strip11   "  ?id " para se )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  strip.clp 	strip11    "  ?id "  "?id1 "  cAxara_hatA_xe )" crlf))
)


;@@@ Added by 14anu-ban-01 on (05-03-2015)
;The worker stripped off all the existing paint from the wall.[self: with respect to oald]
;कार्यकर्ता ने दीवार से सारा मौजूदा रङ्ग खुरच कर निकाल दिया[self]
(defrule strip12
(declare (salience 4800))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 paint)
(kriyA-object ?id ?id1)
(id-root ?id2 off)
(kriyA-upasarga  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 Kuraca_kara_nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  strip.clp 	strip12    "  ?id "  "?id2 "   Kuraca_kara_nikAla_xe )" crlf))
)


;@@@ Added by 14anu-ban-01 on (05-03-2015)
;WWe had to strip out all the old wiring and start again.[oald]
;हमने कपडे उतारे और पानी की तरफ भागे . [self]
(defrule strip13
(declare (salience 200))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 out)
(kriyA-upasarga  ?id ?id1)
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hatA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " strip.clp 	strip13   "  ?id "  "?id1 "  hatA  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (05-03-2015)
;They taught us how to strip down a car engine and put it back together again.[oald]
;उन्होंने हमें सिखाया कि कार इंजन कैसे खोलना चाहिए और वापस उसे फिर से  एक साथ कैसे  लगाना चाहिए.  [self]
(defrule strip14
(declare (salience 4900))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 down)
(kriyA-upasarga  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " strip.clp 	strip14    "  ?id "  "?id1 "  Kola)" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-03-2015)
;He was stripped of his title.[self: with respect to oald]
;उससे उसका ख़िताब छीन लिया गया था /उससे उसकी पदवी छीन ली गयी थी . [self]
(defrule strip15
(declare (salience 4900))
(id-root ?id strip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 title|money|goods|right|honour|property)
(kriyA-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CIna_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  strip.clp 	strip15   "  ?id "  CIna_le )" crlf))
)



;"strip","V","1.kapadZe_uwAranA"
;Everybody stripped off in bathroom.
;--"2.naMgA_karanA"
;Our land is stripped off trees.
;--"3.haTAnA"
;The inspector was stripped off his rank.
;
