;@@@ Added by 14anu-ban-05 on (03-02-2015)
;Fury over tax increases.[oald]
;कर के ऊपर तीव्र क्रोध में वृद्धि होता है.[manual]

(defrule fury0
(declare (salience 100))
(id-root ?id fury)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIvra_kroXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  fury.clp  	fury0   "  ?id "  wIvra_kroXa )" crlf))
)


;@@@ Added by 14anu-ban-05 on (03-02-2015)
;Lock Gate ' stops the water for saving freight ships from the fury of tidal floods .[tourism]
;lAka geta  mAlavAhaka jahAjoM ko jvAra-BAtoM ke prakopa se bacAne ke lie pAnI ko roka xewA hE .[tourism]

(defrule fury1
(declare (salience 101))
(id-root ?id fury)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-from_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakopa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  fury.clp  	fury1   "  ?id "  prakopa )" crlf))
)
