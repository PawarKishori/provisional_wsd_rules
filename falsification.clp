;@@@ Added by 14anu-ban-01 on (07-11-2014).
;This is a complex case of falsification of the company's records. [self:with reference to oald]
;यह कम्पनी के अभिलेखों की जालसाज़ी का एक उलझा हुआ/जटिल मामला है  .[self]
(defrule falsification0
(declare (salience 0))
(id-root ?id falsification)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAlasAjZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  falsification.clp  	falsification0 "  ?id " jAlasAjZI )" crlf))
)

;@@@ Added by 14anu-ban-01 on (07-11-2014).
;The scientific method involves several interconnected steps: Systematic observations, controlled experiments, qualitative and quantitative reasoning, mathematical modelling, prediction and verification or falsification of theories. [NCERT corpus]
;' वैज्ञानिक विधि ' में बहुत से अन्तःसम्बन्ध-पद : व्यवस्थित प्रेक्षण , नियन्त्रित प्रयोग , गुणात्मक तथा मात्रात्मक विवेचना , गणितीय प्रतिरूपण , भविष्य कथन , सिद्धान्तों का सत्यापन अथवा अन्यथाकरण सम्मिलित होते हैं .[NCERT corpus]
;The sentence above is improvised because parser failed at cascading use of conjunctions 'and','or':prediction and verification or falsification of theories. 
;The scientific method involves several interconnected steps: Systematic observations, controlled experiments, qualitative and quantitative reasoning, mathematical modelling, prediction and verification/falsification of theories.[improvised]
;वैज्ञानिक- विधि कई अन्तःसम्बन्ध पद: व्यवस्थित प्रेक्षण, नियन्त्रित प्रयोग, गुणात्मक और मात्रात्मक विवेचना, गणित सम्बन्धी प्रतिरूपण, भविष्य कथन और सिद्धान्तों का सत्यापन/अन्यथाकरण सम्मिलित करती है . 

(defrule falsification1
(declare (salience 100))
(id-root ?id falsification)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 theory)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anyaWAkaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  falsification.clp  	falsification1  "  ?id " anyaWAkaraNa )" crlf))
)

