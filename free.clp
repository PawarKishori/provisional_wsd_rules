;$$$ Modified by 14anu22
;The space is free.
;जगह खाली है.
(defrule free0
(declare (salience 5000))
(id-root ?id free)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 bed|space);added 'space' by 14annu22
;(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective|adjective_comparative|adjective_superlative);changed cat to cat_coarse;changed cat to cat_coarse
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  free.clp 	free0   "  ?id "  KAlI )" crlf))
)

;$$$ Modified by 14anu22
;I am free
;मैं स्वतंत्र हूँ.
(defrule free1
(declare (salience 4900))
(id-root ?id free)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id) ;added this relation by 14anu22
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)));added the line by 14anu22
;(id-cat_coarse ?id adjective|adjective_comparative|adjective_superlative);changed cat to cat_coarse
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svawaMwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  free.clp 	free1   "  ?id "  svawaMwra )" crlf))
)

;$$$ Modified by 14anu22
;The product is free.
;उत्पात मुफ्त है.
(defrule free2
(declare (salience 4800))
(id-root ?id free)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective);changed cat to cat_coarse and noun to adjective
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  free.clp 	free2   "  ?id "  mukwa )" crlf))
)

(defrule free3
(declare (salience 4700))
(id-root ?id free)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 wheel)
(kriyA-wheel_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukwa_rUPa_se_calA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " free.clp free3 " ?id "  mukwa_rUPa_se_calA )" crlf)) 
)

(defrule free4
(declare (salience 4600))
(id-root ?id free)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 wheel)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mukwa_rUPa_se_calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " free.clp	free4  "  ?id "  " ?id1 "  mukwa_rUPa_se_calA  )" crlf))
)

;You have to free the space.
;तुम्हे जगह मुक्त करनी है.
(defrule free5
(declare (salience 5000));changed salience by 14anu22
(id-root ?id free)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  free.clp 	free5   "  ?id "  mukwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-10-2014)
;Yet, if the total external force acting on the system is zero, the center of mass moves with a constant velocity, i.e., moves uniformly in a straight line like a free particle.[NCERT]
;Pira BI, yaxi nikAya para lagane vAlA kula bAhya bala SUnya ho wo xravyamAna keMxra acara-vega se hI calawA hE, arWAw, mukwa kaNa kI waraha samagawi se sarala reKIya paWa para calawA hE.[NCERT]
;If there are no external forces, the center of mass of a double star moves like a free particle, as shown in Fig.7.14 (a).[NCERT]
;yaxi koI bAhya bala na lagA ho wo kisI yugmiwa nakRawra kA xravyamAna keMxra eka mukwa-kaNa kI waraha calawA hE jEsA ciwra 7.14 (@a) meM xarSAyA gayA hE.[NCERT]
;Drops and Bubbles One consequence of surface tension is that free liquid drops and bubbles are spherical if effects of gravity can be neglected.[NCERT]
;pqRTa wanAva kA eka mahawwva yaha BI hE ki yaxi guruwva bala ke praBAva kI upekRA kI jA sake wo xrava kI mukwa bUzxeM waWA bulabule golAkAra howe hEM.[NCERT]

(defrule free6
(declare (salience 5000))
(Domain physics)
(id-root ?id free)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 particle|drop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukwa))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  free.clp 	free6  "  ?id "   mukwa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  free.clp       free6   "  ?id "  physics )" crlf))
)


;default_sense && category=verb	nikAla	0
;"free","V","1.nikAlanA"
;It took the rescuers two hours to free the people from the debris.
;--"2.mukwa_karanA"
;Physical exercises can free your body && mind of tension.
;--"3.kisI_viSeRa_kAraNa_ke_lie_anumawi_xenA"
;The A.P. government intends to free more funds for educational purpose.
;--"4.svawaMwrawA_xenA"
;Winning the contest has freed her to take full-time practice.
;
;
;LEVEL 
;
;
;                    free   sUwra (nibanXa)
;                    ---- 
;
;"free","Adj","1.svawaMwra"
;The slaves were set free.
;We belong to a free nation.
;--"2.DZIlA"          -------- < svawanwra
;One end of the rope is free.
;--"3.mukwa_karanA"     ------ < mukwa_honA < svawanwra
;Thousands of prisoners have been set free.
;
;--"4.binA_mUlya_ke"     ----  < mUlya se mukwa_honA < svawanwra          
;He got free tickets for the magic show.
;--"5.KAlI"        ------ < vyaswa_na_honA < mukwa_honA < svawanwra_rUpa_se
;The bathroom is free now.
;I tried to telephone her but her line was never free.
;--"6.svecCA_se_xene_ko_wawpara"  ----< svawanwrawA(binA_kisI_ke_niyanwraNa)_se
;                                           xenA < svawanwrawApUrvaka
;Her brother is always free with a piece of advice for everybody.
;--"7.sabake_sAWa_sImA_se_aXika_Kula_jAnA"  --- < Kula_jAnA < mukwa_honA < svawanwra
;No one likes Hindi lecturer,she is too free in her language && behaviour.
;
;"free","Adv","1.muPwa/binA_mUlya_ke" --- < svawanwrawA(binA_kisI_ke_niyanwraNa)
;                                      se_xenA < svawanwra 
;Children under five usually travel free on buses && trains.
;Beauty parlour lady cuts my hair for free.
;--------------------------------------------------------------
;
;sUwra: muPwa[>svawanwra`]
;--------------------
;
;vyAKyA :
;upariliKiwa saBI vAkyoM meM svawanwrawA ke BAva ko sUwra-rUpa meM anwarnihiwa xeKA
;jA sakawA hE .  
;--- isa Sabxa ke viswAra ko upariliKiwa vAkyoM ke AXAra para isa prakAra
;    samaJA jA sakawA hE--
;
;-- svawanwra se wAwparya apane wanwra se honA samaJA jAwA hE . 
;
;-- svawanwra hone meM kisI BI prakAra kI aXInawA va JaMJata AxiyoM se mukwa samaJA jAwA 
;   hE . isase mukwa hone ke BAva meM aMgrejI meM kriyAoM ke hone kA BAva karane  
;   meM sAmAnyawaH parivarwiwa howA hE . awaH mukwa karane ke arWa meM .
; 
;-- mukwa hone se wAwparya hara prakAra ke niyamoM va mUlyoM se samaJA jAkara binA 
;   mUlya ke(muPwa) hone ke rUpa meM parivarwana .
; 
;-- mukwa hone se KAlI samaJA jA sakawA hE . kAraNa- mukwa honA kAryoM se BI 
;   howA hE . binA kisI kAma kA honA KAlI honA hI howA hE . 
;
;-- svawanwra se svawanwrawA se xene ke arWa meM parivarwana socA jA sakawA hE . 
;   svawanwrawA se xene kA mawalaba binA kisI ke niyanwraNa se xenA howA hE 
;   Ora yahI svecCA se xenA howA hE .
;
;-- svawanwra honA arWAw isase pahale jina niyamoM va jinake aXikAra meM We,
;   unase mukwi . arWAw unake banXanoM se mukwa ho jAnA yA Kula jAnA 
;   isase Kula jAnA samaJA jA sakawA hE .  
;
;
