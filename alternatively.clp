;---------------------------Default Rule-----------------------------------
;@@@ Added by 14anu-ban-01 on (19-10-2014)
;Alternatively,you can do your homework.[self:with reference to oald]
;इसके बदले में आप अपना ग्रह-कार्य कर सकते हैं. [self]                                                          
(defrule alternatively0
(declare (salience 0))
(id-root ?id alternatively)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isake_baxale_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  alternatively.clp 	alternatively0   "  ?id "  isake_baxale_meM )" crlf))
)
;----------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on (19-10-2014)
;Yet your muscles are alternatively contracting and relaxing and internal energy is being used up and you do get tired.[NCERT corpus]
;इस प्रक्रिया में आपकी मांसपेशियों का बारी-बारी से सङ्कुचन और शिथिलीकरण हो रहा है और आन्तरिक ऊर्जा लगातार व्यय हो रही है और आप थक जाते हैं. [NCERT corpus]                                                       
(defrule alternatively1
(declare (salience 0))
(id-root ?id alternatively)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 2) and|or) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArI_bArI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  alternatively.clp 	alternatively1   "  ?id "  bArI_bArI_se )" crlf))
)

