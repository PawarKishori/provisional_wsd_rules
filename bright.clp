
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 24/03/2014
;He is a bright student.[shiksharthi]
;vaha eka hoSiyAra viXyArWI hE
(defrule bright3
(declare (salience 5000))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 boy|girl|student)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hoSiyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp    bright3   "  ?id "  hoSiyAra )" crlf))
)

;$$$Modified by 14anu-ban-02(08-02-2016)
;She is full of bright ideas .[sd_verified]
;vaha wejasvI vicAroM se BarI huI hE.[sd_ver_output]
;$$$ Modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)
;The King was very pleased and rewarded his Prime Minister for his bright idea.[gyanannidhi]
;Added by Sheetal(17-09-09).
(defrule sh-bright
(declare (salience 5000))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-of_saMbanXI  ?id2 ?id1)	;added by 14anu-ban-02(08-02-2016)
(id-root ?id1 idea)
;(id-root =(+ ?id 1) idea);commented by Garima Singh
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejasvI)); meaning changed from 'wejasvI' to 'baDZiyA' by Garima Singh	;meaning changed from 'baDZiyA' to wejasvI' by 14anu-ban-02(08-02-2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp    sh-bright   "  ?id "  wejasvI )" crlf))
)
;She is full of bright ideas . 

(defrule bright0
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camakIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp 	bright0   "  ?id "  camakIlA )" crlf))
)

(defrule bright1
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camakIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp 	bright1   "  ?id "  camakIlA )" crlf))
)

;"bright","Adj","1.camakIlA"
;The sun was bright && hot
;Bright greens
;Bright silver candlesticks
;--"2.wejZa"
;Some children are brighter in one subject than another
;The bright sound of the trumpet section
;--"3.ujjvala"
;The room was bright && airy
;Had a bright future in publishing
;--"4.xIpwimAna"
;The bright stars of stage && screen
;Bright faces
;
;


;A bright career.
;She is an excellent student with a bright future.
;vaha eka #ujjavala BaviRya ke sAWa eka uwwama #vixyArWI hE
;Add by samapika(20.4.10)
(defrule bright2
(declare (salience 5400))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 future|career)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ujjvala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp    bright2   "  ?id "  ujjvala )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_bright3
(declare (salience 5000))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 boy|girl|student)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hoSiyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bright.clp   sub_samA_bright3   "   ?id " hoSiyAra )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_bright3
(declare (salience 5000))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 boy|girl|student)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hoSiyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bright.clp   obj_samA_bright3   "   ?id " hoSiyAra )" crlf))
)

;$$$ Modified by 14anu-ban-02 (02-12-2014)
;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_sh-bright
(declare (salience 5000))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 idea)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZiyA)); meaning changed from 'wejasvI' to 'baDZiyA' by Garima Singh
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bright.clp  sub_samA_sh-bright  "   ?id " baDZiyA )"  crlf))
)   ;Added file name by 14anu-ban-02 (02-12-2014)

;$$$ Modified by 14anu-ban-02 (02-12-2014)
;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_sh-bright
(declare (salience 5000))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 idea)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZiyA)); meaning changed from 'wejasvI' to 'baDZiyA' by Garima Singh
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bright.clp  obj_samA_sh-bright " ?id " baDZiyA )" crlf))
)    ;Added file name by 14anu-ban-02 (02-12-2014)

;@@@ Added by 14anu-ban-02 (02-12-2014)
;If we look clearly at the shadow cast by an opaque object, close to the region of geometrical shadow, there are alternate dark and bright regions just like in interference.[ncert]
;यदि हम किसी अपारदर्शी वस्तु के द्वारा बनने वाली छाया को ध्यानपूर्वक देखें तो हम पाएँगे कि ज्यामितीय छाया के क्षेत्र के समीप व्यतिकरण के समान बारी-बारी से उदीप्त तथा दीप्त क्षेत्र आते हैं.[ncert]
(defrule bright4
(declare (salience 100))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ?id2 ?id1 ?id)
(id-root ?id2 and|or)
(id-root ?id1 dark)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xIpwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp    bright4   "  ?id "  xIpwa )" crlf))
)


;@@@Added by 14anu-ban-02(07-04-2015)
;You're very bright and breezy today![oald]
;आज आप  अत्यन्त प्रसन्नचित और प्रफुल्लित हैं! [self]
(defrule bright5
(declare (salience 100))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(conjunction-components  ?id2 ?id ?id3)
(id-root ?id2 and|or)
(id-root ?id3 breezy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praPulliwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp    bright5   "  ?id "  praPulliwa )" crlf))
)


;@@@Added by 14anu-ban-02(08-04-2015)
;The brightest pupil in the class.[oald]
;कक्षा में सबसे अधिक तेज विद्यार्थी .[self] 
(defrule bright6
(declare (salience 100))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 pupil|student)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp    bright6   "  ?id "  wejZa )" crlf))
)
;@@@Added by 14anu-ban-02(08-02-2016)
;The King was very pleased and rewarded his Prime Minister for his bright idea.[bright.clp]
;राजा अत्यन्त खुश हुये और अपने प्रधान मन्त्री को उनके अच्छे सु्झाव के लिए पुरुस्कार दिया.[self] 
(defrule bright7
(declare (salience 100))
(id-root ?id bright)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(kriyA-for_saMbanXI  ?id2 ?id1)
(id-root ?id1 idea)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bright.clp    bright7   "  ?id "  acCe )" crlf))
)

