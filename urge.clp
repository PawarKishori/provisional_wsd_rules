
(defrule urge0
(declare (salience 5000))
(id-root ?id urge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avega))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  urge.clp 	urge0   "  ?id "  Avega )" crlf))
)

;"urge","N","1.Avega/preraNA"
;He has a strong urge to go to space.
;
(defrule urge1
(declare (salience 4900))
(id-root ?id urge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ukasA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  urge.clp 	urge1   "  ?id "  ukasA )" crlf))
)

;"urge","V","1.ukasAnA"
;I urged him to finish his studies
;

;$$$ Modified by Rajini (15-07-2016)
;@@@ Added by 14anu-ban-07 (21-08-2014)
;Prince Shreyanshkumar urged Adinatha Prabhu to accept sugar cane juice for ending the fast which he accepted .(tourism corpus)
;राजकुमार  श्रेयंषकुमार  ने  आदिनाथ  प्रभु  से  व्रत  के  समापन  के  लिए  इक्षु  रस  ग्रहण  करने  का  अनुरोध  किया  जिसे  उन्होंने  स्वीकार  किया .(manually)
(defrule urge2
(declare (salience 5000)) ;Salience was increased by Rajini on 15-07-2016
(id-root ?id urge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-kriyArWa_kriyA  ?id ?id1) ;commented by Rajini on 15-07-2016
(kriyA-kqxanwa_karma  ?id ?id1) ;added by Rajini on 15-07-2016
(to-infinitive  ?id2 ?id1) ;added by Rajini on 15-07-2016
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  urge.clp 	urge2   "  ?id "  anuroXa_kara )" crlf))
)

;$$$ Modified by 14anu-ban-07 (15-12-2014)
;@@@ Added 14anu11
;उसका क्षत्रिय धर्म एक ओर उससे यह मांग करता है कि वह अपना कर्तव्य करे और सत्य तथा न्याय की स्थापना के लिए युद्ध करें तथा दूसरी ओर प्रेम और आत्मीयता की भावना मांग करती है कि वह 
;अपने बंधु बांधवों पर आक्रमण नहीं करे .
;On the one hand , his Kshatriya dharma urges him to do his duty and fight in order to uphold truth and justice and on the other hand 
;, love and fellow - feeling demand that he should not strike against his own brethren .
(defrule urge3
(declare (salience 4900))
(id-root ?id urge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(kriyA-kqxanwa_karma  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAMga_kara))  ;meaning changed from mAMga_karawA_hE to mAMga_kara by 14anu-ban-07 (15-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  urge.clp 	urge3   "  ?id "  mAMga_kara )" crlf))
)

;@@@ Added by 14anu-ban-07, (25-03-2015)
;She could hear him urging her on as she ran past.[oald] (parser no. 9)
;वह  उसको प्रोत्साहित करते हुई  सुन सकती थी   जब वह पास से दौडती हुई गयी . (manual)
(defrule urge4
(declare (salience 5200))
(id-root ?id urge)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 prowsAhiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " urge.clp	urge4  "  ?id "  " ?id1 "  prowsAhiwa_kara  )" crlf))
)
