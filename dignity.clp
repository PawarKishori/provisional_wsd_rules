;@@@ Added by 14anu-ban-04 (07-04-2015)
;It's difficult to preserve your dignity when you have no job and no home.     [oald]
;आपकी प्रतिष्ठा बनाए रखना मुश्किल है जब आपके पास कोई  काम और कोई घर नहीं  हैं .                    [self]
(defrule dignity1
(declare (salience 20))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id dignity)
(id-cat_coarse ?id noun)
(or(kriyA-object ?id1 ?id)(viSeRya-of_saMbanXI ?id ?id1)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiRTA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  dignity.clp       dignity1  "  ?id "  prawiRTA )" crlf))
)

;@@@ Added by 14anu-ban-04 (07-04-2015)
;I think everyone should be able to die with dignity.             [cald]
;मैं सोचता हूँ कि हर कोई को गौरव के साथ मरने के लिए समर्थ रहना चाहिए .                 [self]
(defrule dignity2
(declare (salience 40))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id dignity)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI ?id1 ?id)
(id-root ?id1 die)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gOrava)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  dignity.clp       dignity2  "  ?id "  gOrava )" crlf))
)

;@@@ Added by 14anu-ban-04 (07-04-2015)
;He conducted himself with grace and dignity throughout the trial.       [OALD]
;वह परीक्षण के दौरान शिष्टता और गरिमा के साथ पेश आया.                                 [MANUAL]
(defrule dignity3
(declare (salience 30))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id dignity)
(id-cat_coarse ?id noun)
(kriyA-object ?kri ?id1)
(kriyA-with_saMbanXI ?kri ?id)
(id-root ?id1 himself|herself|themselves|ourselves|myself)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garimA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  dignity.clp       dignity3  "  ?id "  garimA )" crlf))
) 

;------------------------ Default Rules ---------------------

;@@@ Added by 14anu-ban-04 (07-04-2015)
;She accepted the criticism with quiet dignity.       [oald]
;उसने निस्तब्ध बडप्पन से आलोचना को स्वीकार किया .                  [self]
(defrule dignity0
(declare (salience 10))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id dignity)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badappana)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  dignity.clp       dignity0  "  ?id "  badappana )" crlf))
)

