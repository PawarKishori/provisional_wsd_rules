;@@@ Added by 14anu-ban-06 (28-02-2015)
;A long list of amendments have hobbled the new legislation.(cambridge)
;संशोधनों की एक लम्बी सूची नये कानून को सीमाबद्ध कर रही है . (manual)
(defrule hobble1
(declare (salience 2000))
(id-root ?id hobble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 legislation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImAbaxXa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hobble.clp 	hobble1   "  ?id "  sImAbaxXa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hobble.clp       hobble1   "  ?id " ko )" crlf))
)

;@@@ Added by 14anu-ban-06 (10-03-2015)
;The horse’s hind legs had been hobbled.(OALD)
;घोडे की पिछली टाँगें  बाँध दी गयीं थीं .  (manual)
(defrule hobble2
(declare (salience 2500))
(id-root ?id hobble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 leg)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzXa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hobble.clp 	hobble2   "  ?id "  bAzXa_xe )" crlf))
)

;---------------------- Default Rules -------------------
;@@@ Added by 14anu-ban-06 (28-02-2015)
;The last time I saw Rachel she was hobbling around with a stick. (cambridge)
;आखिरी बार मैं रशेल से मिला वह हर ओर लाठी के साथ लँगडा कर चल रही थी . (manual)
(defrule hobble0
(declare (salience 0))
(id-root ?id hobble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lazgadA_kara_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hobble.clp 	hobble0   "  ?id "  lazgadA_kara_cala )" crlf))
)

