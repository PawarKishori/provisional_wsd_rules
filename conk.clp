;@@@ Added by 14anu-ban-03 (11-04-2015)
;His mother conked last year. [oald]
;उसकी माँ पिछले वर्ष गुजर गई .  [manual]
(defrule conk1
(declare (salience 10))  
(id-root ?id conk)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-kAlavAcI ?id ?id2)
(id-root ?id2 year)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gujara_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conk.clp 	conk1   "  ?id "  gujara_jA )" crlf))
)


;@@@ Added by 14anu-ban-03 (11-04-2015)
;She conked him on the head with her umbrella. [oald]
;उसने अपने छाते से उसके सिर पर प्रहार किया . [manual]
(defrule conk2
(declare (salience 10))  
(id-root ?id conk)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI ?id ?id1)
(id-root ?id1 head)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sira_para_prahAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " conk.clp 	conk2  "  ?id "  " ?id1 "  sira_para_prahAra_kara  )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (11-04-2015)
;We conked enemy at that time when they least expected it. [oald]
;हमने उस समय  शत्रुओं पर प्रहार किया जब उन्होंने उसकी कम से कम आशा की थी. [manual]
(defrule conk0
(declare (salience 00))
(id-root ?id conk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prahAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conk.clp     conk0   "  ?id "  prahAra_kara )" crlf))
)

