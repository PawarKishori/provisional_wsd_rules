
(defrule cover0
(declare (salience 5000))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AdZa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " cover.clp cover0 " ?id "  AdZa )" crlf)) 
)

(defrule cover1
(declare (salience 4900))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " cover.clp	cover1  "  ?id "  " ?id1 "  AdZa  )" crlf))
)

(defrule cover2
(declare (salience 4800))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dazka_le));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " cover.clp cover2 " ?id "  Dazka_le )" crlf)) 
)

(defrule cover3
(declare (salience 4700))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Dazka_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " cover.clp	cover3  "  ?id "  " ?id1 "  Dazka_le  )" crlf))
)

(defrule cover4
(declare (salience 4600))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cover.clp 	cover4   "  ?id "  AvaraNa )" crlf))
)

;@@@ Added by 14anu-ban-03 (04-08-2014)
;If an object moving along the straight line covers equal distances in equal intervals of time, it is said to be in uniform motion along a straight line. [ncert]
;yaxi koI vaswu samAna samaya anwarAla meM samAna xUrI waya karawI hE, wo usa vaswu kI gawi ekasamAna gawi kahalAwI hE .[ncert]
(defrule cover7
(declare (salience 4600))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)  ;Added by 14anu-ban-03 (05-12-2014)
(id-root ?id1 distance)  ;Added by 14anu-ban-03 (05-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waya_kara)) ;meaning change from waya to waya_kara by 14anu-ban-03 (14-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cover.clp 	cover7   "  ?id "  waya_kara)" crlf))
)


;@@@ Added by 14anu-ban-03 (15-11-2014)
;A farmer ploughing the field, a construction worker carrying bricks, a student studying for a competitive examination,an artist painting a beautiful landscape, all are said to be working;In physics, however, the word Work covers a definite and precise meaning.[ncert]
;yaxi koI kisAna Kewa jowawA hE,koI miswrI INta DowA hE,koI CAwra parIkRA ke lie paDawA hE yA koI ciwrakAra sunxara xqSyaBUmi kA ciwra banAwA hE wo hama kahawe hEM ki saBI kArya kara rahe hEM paranwu BOwikI meM kArya Sabxa ko pariSuxXa rUpa se pariBARiwa karawe hEM.[ncert]
(defrule cover8
(declare (salience 4600))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-subject ?id ?id1)
(and(kriyA-subject ?id ?) (samAsa_viSeRya-samAsa_viSeRaNa ? ?id1))  ;Added by 14anu-ban-03 (05-12-2014)
(id-root ?id1 word)      ;Added by 14anu-ban-03 (05-12-2014)
;(id-word ?id1 work)     ;commented by 14anu-ban-03 (05-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariBARiwa_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cover.clp 	cover8   "  ?id "  pariBARiwa_kara)" crlf))
)


;$$$  Modified by Preeti(19-12-13) meaning 'samAviRta_kara' to 'Daka'
;They hurriedly rearranged the shrubs to cover the boat and set off for home. 
;unhoMne jalxI se nAva Dakane ke liye JAdiyoM ko punaH_vyavasWiwa kiyA Ora Gara ke liye ravAnA hue.
(defrule cover5
(declare (salience 4500))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Daka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cover.clp 	cover5   "  ?id "  Daka )" crlf))
)

;@@@ Added by Preeti(3-12-13)
;The players were soon covered in mud. [ Oxford Advanced Learner's Dictionary]
;KilAdI SIGra hI kIcada meM laWapaWa ho gaye We.
(defrule cover6
(declare (salience 4600))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI   ?id ?id1)
(id-root ?id1 mud|sweat|blood|dust)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laWapaWa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cover.clp 	cover6   "  ?id "  laWapaWa_ho)" crlf))
)

;@@@Added by 14anu24
;Get a written agreement or contract covering all the important points : the work to be done , the price , how long the job will take .
;सभी जरूरी बातें जैसे कि कराए जाने वाले काम के विवरण , उसके दाम , लगने वाला कुल समय इत्यादि , को शामिल करने वाला एक लिखित अनुबंध या समझौता मांगिए .
(defrule cover10
(declare (salience 5000))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 point)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAmila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cover.clp    cover10   "  ?id "  SAmila )" crlf))
)


;@@@ Added by 14anu-ban-03 (08-04-2015)
;In a total eclipse, the moon completely covers the disc of the sun.               [oald]
;खग्रास ग्रहण में, चन्द्रमा सूर्य के चक्र को  पूरी तरह से  ढक लेता है .                                       [self]
(defrule cover11
(declare (salience 5000))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 completely)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Daka_le))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  cover.clp    cover11    "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cover.clp    cover11   "  ?id "  Daka_le )" crlf))
)


;@@@ Added by 14anu-ban-05 on (28-08-2015)
;An overview of the goals of five year plans and a critical appraisal of the merits and limitations of planned development has been covered in this unit. [social]
;पाँच वर्षीय योजनाएँ के लक्ष्य और नियोजित विकास में योग्यता के और सीमाएँ विवेचनात्मक मूल्याङ्कन का एक सिंहावलोकन इस अध्याय में  सम्मिलित किया गया है .[self]

(defrule cover12
(declare (salience 4601))
(id-root ?id cover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI   ?id ?id1)
(id-root ?id1 unit)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammiliwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cover.clp 	cover12   "  ?id "  sammiliwa_kara)" crlf))
)


;################### Need to be handled ####################
;Matteo arrived at the top of the hill, panting and covered in sweat.
;########################Old Example##############

;default_sense && category=verb	AcCAxiwa_karanA/DAzka	0
;"cover","VT","1.AcCAxiwa_karanA/DAzkanA"
;She covered her face with her hands.
;--"2.CipanA"
;The grass covered the grave.
;--"3.paryApwa_honA"
;The grant doesn't cover my salary.
;--"4.waya_karanA"
;The caravan covered almost 100 miles each day.
;--"5.xabA_xenA"
;The President covered the fact that he bugged the offices in the White House
;--"6.nukasAna_BaranA"
;The insurance won't cover the colossal loss of property.  
;--"7.kisI_ke_sWAna_para_kArya_karanA"
;She is covering for our secretary who is ill.
;
;
;LEVEL 
;
;
;
;               cover  sUwra (nibanXa)
;               ----- 
; 
;
;
;`cover' Sabxa ke viviXa prayoga--
;-------------------------
; 
;"cover","N","1.Dakkana"  
;             ---- < AcCAxana karanevAlA
;The computer was covered with a plastic cover.
;--"2.bacAva/SaraNa" 
;            ---- < kisI ke surakRArUpI AcCAxana meM honA 
;The desert didnot give any cover to the troops.
;
;--"3.liPZAPZA"  
;           ----- < AcCAxana karanevAlA
;He forgot to past a postage stamp on the cover.
;--"4.cAxara"  
;            ------ < AcCAxana ke kAma meM AnevAlA 
;He pulled the cover over his head && went off to sleep.
;--"5.puTTA" 
;              ----- < AcCAxana karanevAlA
;The notebook had a brown cover.
;--"6.surakRA_rASI"  
;              ----- < surakRArUpI AcCAxana 
;He took a policy that gives extra cover against theft.
;
;"cover","VT","1.AcCAxiwa_karanA/DAzkanA" 
;She covered her face with her hands.
;--"2.CipanA"   
;    ----- < kisI vaswu kA AcCAxana ke rUpa meM prayoga kara usake ota meM honA
;The grass covered the grave.
;--"3.paryApwa_honA"  
;           ----- < binA kisI kamI yA avaSiRtawA ke AcCAxana-yogya honA 
;The grant doesn't cover my salary.
;--"4.waya_karanA" 
;             ----- < yAwrA xvArA va anya kisI sAXana xvArA AcCAxiwa karanA 
;The caravan covered almost 100 miles each day.
;---"5.xabA_xenA"  
;             ---- < vicArapUrvaka yA balapUrvaka AcCAxiwa kara xenA
;The President covered the fact that he bugged the offices in the White House
;--"6.nukasAna_BaranA"  
;             ----- < kI huI hAni kA AcCAxana karanA 
;The insurance won't cover the colossal loss of property.
;--"7.kisI_ke_sWAna_para_kArya_karanA" 
;              ---- < kisI kI rikwawA kA AcCAxana karanA 
;She is covering for our secretary who is ill.
;---------------------------------------------------------
;
;sUwra-vivaraNa--
;  `cover' ke jiwane BI viviXa prayoga hinxI meM howe hEM . saBI ko `AcCaxana' kriyA
;ke viswAra ke rUpa meM xeKA jA sakawA hE . nIce viviXa prayogoM ke sAWa xI gayI
;tippaNiyoM se yaha bAwa saralawayA samaJI jA sakawI hE . parivarwiwa prawiBAsiwa honevAlA
;prawyeka paxa spaRtarUpa se AcCAxana kriyA se sambaxXa hE .
;
;sUwra : kavara[AcCAxana`]
;---------------
;
