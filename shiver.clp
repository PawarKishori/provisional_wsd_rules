;@@@ Added by 14anu-ban-01 on (18-03-2015) 
;A delicious shiver of excitement ran through his body.		[oald]
;उत्तेजना की एक सुखद लहर उसके शरीर में  उठी .	 			[self] 
(defrule shiver2
(declare (salience 5500))
(id-root ?id shiver)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 excitement|delight|pleasure|impatience|unease|warmth|ease)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shiver.clp 	shiver2   "  ?id "  lahara)" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-03-2015) 
;I don't like him,he gives me the shivers.[cald]
;मैं उसको पसन्द नहीं करता हूँ, वह मुझे घबराहट देता है . [self]
(defrule shiver3
(declare (salience 5000))
(id-root ?id shiver)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 give)
(or(kriyA-object_2  ?id1 ?id)(kriyA-object  ?id1 ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GabarAhata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shiver.clp 	shiver3   "  ?id "  GabarAhata )" crlf))
)

;@@@ Added by 14anu-ban-01 on (18-03-2015)
;He shivered at the thought of the cold, dark sea.[oald]
;वह ठण्डे, काले समुद्र के विचार से काँप उठा . [self]
(defrule shiver4
(declare (salience 4900))
(id-root ?id shiver)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAzpa_uTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shiver.clp 	shiver4  "  ?id "  kAzpa_uTa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (18-03-2015)
;His cruel and callous comments made me shiver. [oald]
;उसकी निर्दयी एवं संवेदनाहीन/कठोर टिप्पणियों ने मुझे थरथरा/कँपकँपा दिया . [self]
(defrule shiver5
(declare (salience 4900))
(id-root ?id shiver)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 make)
(kriyA-preraka_kriyA  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WaraWarA_xe/kazpakazpA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shiver.clp 	shiver5  "  ?id "  WaraWarA_xe/kazpakazpA_xe )" crlf))
)

;------------------------ Default Rules ----------------------

;"shiver","N","1.kampana"
;The frightening incident gave him a terrible shivers.
(defrule shiver0
(declare (salience 5000))
(id-root ?id shiver)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kampana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shiver.clp 	shiver0   "  ?id "  kampana )" crlf))
)

;"shiver","V","1.kAzpanA"
;She shivered in horror.
(defrule shiver1
(declare (salience 4900))
(id-root ?id shiver)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAzpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shiver.clp 	shiver1   "  ?id "  kAzpa )" crlf))
)


