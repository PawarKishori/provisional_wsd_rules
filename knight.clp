;@@@Added by 14anu-ban-07,(11-04-2015)
;He hopes to be made a knight for his work at the Bank of England.(cambridge)
;वह इंग्लैंड के बैंक में उसके कार्य के लिए एक पदवी बनाने की  आशा करता है . (manual)
(defrule knight1
(declare (salience 1000))
(id-root ?id knight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_paxavI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knight.clp 	knight1   "  ?id "  eka_paxavI )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-07,(11-04-2015)
;Knights in black armour.(cambridge)
;काले कवच में योद्धा . (manual)
(defrule knight0
(id-root ?id knight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yoxXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knight.clp   knight0   "  ?id "  yoxXA )" crlf))
)

