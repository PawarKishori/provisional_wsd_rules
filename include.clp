
(defrule include0
(declare (salience 5000))
(id-root ?id include)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id including )
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sammiliwa_karawe_hue))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  include.clp  	include0   "  ?id "  sammiliwa_karawe_hue )" crlf))
)

;"including","Prep","1.sammiliwa_karawe_hue"
;The band played a number of songs, including some of my favorites.
;
(defrule include1
(declare (salience 4900))
(id-root ?id include)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammiliwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  include.clp 	include1   "  ?id "  sammiliwa_kara )" crlf))
)

;"include","V","1.sammiliwa karanA"
;They included me in the hockey team.
;
;

;;@@@ Added by Prachi Rathore 4-1-12
;They adamantly insisted upon being included in the meeting.[m-w]
;उन्होंने अडिग रूप से बैठक में सम्मिलित होने के ऊपर जोर दिया . 
(defrule include2
(declare (salience 5000))
(id-root ?id include)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammiliwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  include.clp 	include2   "  ?id "  sammiliwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-11-2014) suggested by Soma mam
;In a broader sense, agriculture includes cultivation of soil and breeding and raising livestocks.(agriculture)
;मोटे तौर पर कृषि में भूमि की जुताई, पशु-प्रजनन और पालन सम्मिलित होता हैं।(manual)
(defrule include3
(declare (salience 5000))
(id-root ?id include)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammiliwa_ho))
(assert  (id-wsd_viBakwi   ?id1  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  include.clp  include3   "  ?id "  sammiliwa_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  include.clp      include3   "  ?id1 " meM )" crlf)
)
)

;$$$ Modified by 14anu-ban-06 (11-12-2014) 
;@@@ Added by avni(14anu11)
;Ask other mums and dads ( including your own ) for tips .
;अन्य माता - पिताओं ( सहित  अपने माता - पिता ) से उन की राय लें .
;अन्य माताओं और पिताओं से (आपके अपने सहित) सुझावों के लिए पूछिए . (manual);added by 14anu-ban-06 (11-12-2014) 
(defrule include4
(declare (salience 5000))
(id-root ?id include)
(id-root = (+ ?id 2) own)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahiwa));meaning chnaged from 'shiw' to 'sahiwa' by 14anu-ban-06 (11-12-2014) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  include.clp 	include4   "  ?id "  sahiwa )" crlf))
)


