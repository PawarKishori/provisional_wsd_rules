;$$$ Modified by 14anu-ban-07,7-8-14
;copied this rule from trim.clp because root word is trimming not trim.
;modified root as trimming  
;removed -- (id-word ?id trimming )

(defrule trimming0
(declare (salience 0))
(id-root ?id trimming)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id upakaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* " trimming.clp  	trimming0   "  ?id "  upakaraNa )" crlf))
)

;"trimming","N","1.upakaraNa"
;Baked potato && all the trimmings were on the table.
;सेंका हुआ आलू और सभी उपकरण मेज पर थे .(manually 14anu-ban-07)

;@@@ Added by 14anu-ban-07 ,7-8-14
;छँटाई  :
;The trimming of palm and dates trees is done properly and regularly with the help of cranes. (tourism corpus)
;पाम  और  खजूर  के  वृक्षों  की  छँटाई  ,  बाकायदा  क्रेन  के  सहारे  समय  -  समय  पर  की  जाती  है  ।
(defrule trimming1
(declare (salience 1000))
(id-root ?id trimming)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id CaztAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  trimming.clp  	trimming1   "  ?id "  CaztAI )" crlf))
)


