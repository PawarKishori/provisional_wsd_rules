
(defrule flash0
(declare (salience 5000))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id flashing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id avaroXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  flash.clp  	flash0   "  ?id "  avaroXa )" crlf))
)

;"flashing","N","1.avaroXa"
;Flashings are put up on buildings to prevent ingress of water through leaks.
;

;@@@ Added by 14anu09[25-06-14]
;key information such as the aircraft's speed and altitude flashed on the screen.(translation from parallel corpus)
;वायुयान की  गति और ऊँचाई जैसे सूचनाएं स्करीन पे दिखीं . 
(defrule flash4
(declare (salience 4700))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI ?id ?id1)
(id-word ?id1 screen)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash4   "  ?id "  xiKa )" crlf))
)

;@@@ Added by 14anu09[25-06-14]
;He's always flashing his money around. 
;वह हमेशा हर ओर उसका पैसा शान दिखाता रहता है . 
(defrule flash5
(declare (salience 4900))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 car|bike|skill|money)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAna_xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash5   "  ?id "  SAna_xiKA )" crlf))
)


;@@@ Added by 14anu09[25-06-14]
;Her thoughts flashed back to their wedding day.
; उसके विचारों ने उनके विवाह के दिन को अतीतावलोकन  किया. 
(defrule flash6
(declare (salience 4900))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 back) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) awIwAvalokana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flash.clp  flash6  "  ?id "  " (+ ?id 1) "  awIwAvalokana_kara  )" crlf))
)

;@@@ Added by 14anu09[25-06-14]
;I flashed on an argument I had with my sister when we were kids.
;मैंने एक बहस पर विचार किया जो मेरा मेरी बहन के साथ थी जब हम बच्चे थे . 
(defrule flash7
(declare (salience 4900))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 argument|thought|idea|concept|experience|belief|sin|all) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id smaraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash7   "  ?id "  smaraNa_kara )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 24.06.2014 email-id:sahni.gourav0123@gmail.com
;The weekend seemed to be over in a flash. 
;सप्ताह अंत में एक क्षण में खत्म हो रहा था.
(defrule flash8
(declare (salience 5000))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash8   "  ?id "  kRaNa )" crlf))
)

;commented by 14anu-ban-05 on (12-12-2014)
;Sentence example is a phrase part showing wrong parse for 'flash'. Part-of-speech of 'flash' should be noun

;@@@Added by Gourav Sahni 14anu15 (MNNIT ALLAHABAD) on 24.06.2014 email-id:sahni.gourav0123@gmail.com
;A digital camera with a built-in flash.
;एक निर्मित में फ्लैश के साथ एक कैमरा.
;(defrule flash9
;(declare (salience 5000))
;(id-root ?id flash)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(id-root ?id1 camera)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id PlESa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash9   "  ?id "  PlESa )" crlf))
;)

;@@@Added by 14anu-ban-05 on (12-12-2014)
;I brought a camera with a built-in flash.[Suggested by Chaitanya Sir]
;मैं अन्तर्निहित फ्लैश के साथ एक कैमरा ले आया।[MANUAL]
(defrule flash09
(declare (salience 5000))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PlESa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash09   "  ?id "  PlESa )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 24.06.2014 email-id:sahni.gourav0123@gmail.com
;I just need a flash of my fiance. 
;मैं सिर्फ अपने मंगेतर की एक  झलक की जरूरत है.
(defrule flash10
(declare (salience 5000))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
(viSeRya-det_viSeRaNa ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jalaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash10   "  ?id "  Jalaka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (12-12-2014)
;In olden days lightning was considered as an atmospheric flash of supernatural origin.[NCERT]
;purAwana kAla meM wadiwa ko alOlika uxgama kI vAyumaNdalIya sPura kRaNaxIpwi samaJA gayA.[NCERT]
(defrule flash11
(declare (salience 5000))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) atmospheric|lightning)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sPura_kRaNaxIpwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash11   "  ?id "  sPura_kRaNaxIpwi )" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-01-2015)
;They flashed by on a motorcycle.	[cald]
;वे एक मोटरसाइकिल पर तीव्र गति से गये.		[manual]
(defrule flash12
(declare (salience 5000))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) by|past)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) wIvra_gawi_se_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flash.clp  flash12  "  ?id "  " (+ ?id 1) "  wIvra_gawi_se_jA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-01-2015)
;The thought suddenly flashed through my mind that she didn't want to be here.	[cald]
;यह सोच अचानक मेरे मन में कौंधा कि वह यहाँ नहीं होना चाहती थी.					[manual]
(defrule flash13
(declare (salience 5000))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-through_saMbanXI  ?id ?id1)
(id-root ?id1 mind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kOMXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash13   "  ?id "  kOMXa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-01-2015)
;Within moments of an event happening, the news can be flashed around the world.	[cald]
;घटना घटित होने के कुछ क्षणों के भीतर खबर दुनिया भर में  तेज़ी से प्रसारित किया जा सकता है.			[manual]
(defrule flash14
(declare (salience 5000))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejZI_se_prasAriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash14   "  ?id "  wejZI_se_prasAriwa_kara)" crlf))
)

;-------------- Default rules -----------------

;"flash","Adj","1.AkarRaka_vsawu"
;He has a big flash house.
(defrule flash1
(declare (salience 4900))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRaka_vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash1   "  ?id "  AkarRaka_vaswu )" crlf))
)

(defrule flash2
(declare (salience 4800))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash2   "  ?id "  camaka )" crlf))
)

;"flash","N","1.camaka"
;There was a sudden flash of lightening in the sky.
;--"2.PlESa_yaMwra"
;In low light a flash is necessary for taking good photographs.
;--"3.paxa_kA_saMkewa"
;He wears the flash of his rank at all functions.
;
(defrule flash3
(declare (salience 4700))
(id-root ?id flash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flash.clp 	flash3   "  ?id "  camaka )" crlf))
)

;"flash","V","1.camakanA"
;When she is annoyed her eyes flash angrily.
;He was flashing the torch to stop the car.
;--"2.wIvra_gawi_se_jAnA"
;The cars were flashing by at high speed on the highway.
;--"3.samAcAra_xenA"
;There was a flash in the news about the devastating floods.
;--"4.xiKAnA"
;Please flash your identification before you can be allowed into the restricted area.
;
