;@@@ Added by 14anu-ban-06 (12-02-2015)
;The hood over the air vent is loose. (cambridge)
;हवा छिद्र के ऊपर का ढक्कन ढीला है .  (manual)
(defrule hood2
(declare (salience 5000))
(id-root ?id hood)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-over_saMbanXI ?id ?id1)
(id-root ?id1 vent)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dakkana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hood.clp 	hood2   "  ?id "  Dakkana )" crlf))
)

;------------------------- Default Rules --------------------

;"hooded","Adj","1.DakanA lagA huA"
;varRA se bacane ke liye 'hooded'(topI vAlI) barasAwI pahananA cAhiye.
(defrule hood0
(declare (salience 5000))
(id-root ?id hood)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id hooded )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id DakanA_lagA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hood.clp  	hood0   "  ?id "  DakanA_lagA_huA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (12-02-2015)
;The coat has a detachable hood.(cambridge)
;कोट में एक अलग हो जाने वाली टोपी है . (manual)
;"hood","N","1.sira garxana Dakane ke liye topI yA cogA"
;dijala eMjina meM cAlaka kakRa Sorta 'hood'kahalAwA hE.
(defrule hood1
(declare (salience 4900))
(id-root ?id hood)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id topI));meaning changed from 'sira_garxana_Dakane_ke_liye_topI_yA_cogA' to 'topI' by 14anu-ban-06 (12-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hood.clp 	hood1   "  ?id "  topI )" crlf))
)

