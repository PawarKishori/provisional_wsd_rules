;[NOTE] [In this sentence there is a parser problem and it is sent for parser correction.Parser treating 'dilute' as a 'adjective' but here it is a 'verb'] The rule is working properly on parser no. 3.
;$$$ Modified by 14anu-ban-04 (12-12-2014)      ;meaning changed from 'kama_ho_gaye' to ' kama_ho_jA'
;@@@ Added by 14anu06 on 11/6/2014 
;His share in the company was diluted.
;कम्पनी में उसका हिस्सा कम हो गया था .     ;translation corrected by 14anu-ban-04(12-12-2014)
(defrule dilute1
(declare (salience 5010))                ;salience increased from 5000 to 5010 by 14anu-ban-04
(id-root ?id dilute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
;(id-word ?id1 share|stock|stocks|shares|stake|stakes)            ;commented by 14anu-ban-04 (12-12-2014)
(id-root ?id1 share|stock|stake)                                  ;added by 14anu-ban-04 (12-12-2014)  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dilute.clp 	dilute1   "  ?id " kama_ho_jA )" crlf))
)

;$$$ Modified by 14anu-ban-04 (12-12-2014)      ;meaning changed from 'kama' to 'kama_kara_xe'
;The government diluted your stock.         [same clp file]
;सरकार ने आपका तिजारती माल कम कर दिया .             [improved translation by 14anu-ban-04]
;@@@ Added by 14anu06 on 11/6/2014***********
;This will dilute your stocks value.
;यह आपका भण्डार मूल्य कम कर देगा.
(defrule dilute2
(declare (salience 5500))
(id-root ?id dilute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-subject ?id ?id1)                           ;commented by 14anu-ban-04 (12-12-2014)  
(kriyA-object ?id ?id2)
;(id-word ?id1 this|them|they|you|that)                 ;commented by 14anu-ban-04 (12-12-2014)  
;(id-word ?id2 value|values|price|share)        ;commented by 14anu-ban-04 (12-12-2014)  
(id-root ?id2 value|price|share|stock|stake)                ;added by 14anu-ban-04 (12-12-2014)  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dilute.clp 	dilute2   "  ?id "  kama_kara_xe )" crlf))
)


;NOTE: [This rule is commented by 14anu-ban-04 on (12-12-2014) because this rule is merged with rule 'dilute2' as both have the same meaning and the meaning 'kama_ho_gaye' is incorrect for this sentence ]
;@@@ Added by 14anu06 on 11/6/2014
;The government diluted your stock.
;सरकार ने आपका तिजारती माल कम हो गये .
;(defrule dilute3
;(declare (salience 5000))              
;(id-root ?id dilute)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-object ?id ?id1)
;(id-word ?id1 share|stock|stocks|shares|stake|stakes)        
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kama_ho_gaye))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dilute.clp 	dilute3   "  ?id " kama_ho_gaye  )" crlf))
;)

;----------------- Default rules -------------------

(defrule dilute4
(declare (salience 5000))
(id-root ?id dilute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dilute.clp 	dilute4   "  ?id "  pawalA_kara )" crlf))
)

;"dilute","V","1.pawalA_kara{pAnI_iwyAxi_se}"
(defrule dilute5
(declare (salience 4900))
(id-root ?id dilute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAnI_se_pawalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dilute.clp 	dilute5   "  ?id "  pAnI_se_pawalA_kara )" crlf))
)

;"dilute","VT","1.pAnI_se_pawalA_karanA"
;Some shampoos shouldn't be diluted.
;
