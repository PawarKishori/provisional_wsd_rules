;@@@ Added by 14anu-ban-06  (17-10-2014)
;This illustrates the conservation of mechanical energy.(NCERT)
;यह  यान्त्रिक ऊर्जा के संरक्षण के सिद्धान्त का उदाहरण प्रस्तुत करता है.(NCERT-improvised)
(defrule illustrate0
(declare (salience 0))
(id-root ?id illustrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxAharaNa_praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illustrate.clp 	illustrate0   "  ?id "  uxAharaNa_praswuwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06  (17-10-2014)
;This is illustrated in Example 6.9.(NCERT)
;इसे उदाहरण 6.9 में स्पष्ट किया गया है.(NCERT)
;इसे उदाहरण 6.9 में प्रस्तुत किया गया है.(NCERT-improvised)
;He illustrated with an example.(COCA)
;उसने एक उदाहरण के साथ प्रस्तुत किया .(manual) 
(defrule illustrate1
(declare (salience 2000))
(id-root ?id illustrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 example|Example)
(or(kriyA-in_saMbanXI ?id ?id1)(kriyA-with_saMbanXI ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illustrate.clp 	illustrate1   "  ?id "  praswuwa_kara )" crlf))
)
