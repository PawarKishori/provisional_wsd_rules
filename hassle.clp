;@@@ Added by 14anu-ban-06 (31-03-2015)
;Try not to get into a hassle with this guy.(OALD)
;इस आदमी के साथ वाद-विवाद में पड़ने का प्रयास न कीजिए.  (manual)
(defrule hassle2
(declare (salience 5200))
(id-root ?id hassle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-with_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAxa-vivAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hassle.clp 	hassle2   "  ?id "  vAxa-vivAxa )" crlf))
)


;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;"hassle","N","1.pareSAnI"
;It is really a hassle to manage small kids .
;Cote baccoM ko samBAlanA vAswava meM eka pareSAnI kA kAma hE .
(defrule hassle0
(declare (salience 5000))
(id-root ?id hassle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hassle.clp 	hassle0   "  ?id "  pareSAnI )" crlf))
)

;"hassle","V","1.pareSAna_karanA"
;wuma muJe pareSAna kyoM karawe ho ?

(defrule hassle1
(declare (salience 4900))
(id-root ?id hassle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hassle.clp 	hassle1   "  ?id "  pareSAna_kara )" crlf))
)

