
(defrule edge0
(declare (salience 4000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge0   "  ?id "  XAra )" crlf))
)


;"edge","N","1.XAra"
;--"2.kinArA"
;The boy stood on the edge of the cliff.
;

;added by Pramila(BU) on 30-11-2013
;He fell over the cliff's edge.               ;problem sentence
;वह चट्टान के किनारे पर गिर गया.
;The boy stood on the edge of the cliff.           ;sentence of this file
;लड़का चट्टान के किनारे पर खड़ा है.
(defrule edge1
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-on_saMbanXI  ?id1 ?id)(kriyA-over_saMbanXI  ?id1 ?id)(kriyA-near_saMbanXI ?id1 ?id))
;@@@Modified by Gourav Sahni (MNNIT ALLAHABAD) on 23.06.2014 email-id:sahni.gourav0123@gmail.com
; Don't put that glass so near the edge of the table.
;मेज के किनारे के पास उस कांच मत डालो. 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge1   "  ?id "  kinArA )" crlf))
)

;added by Pramila(BU) on 30-11-2013
;Many birds in India are on the edge of extinction.                ;sentence of this file
;BArawa kI bahuwa sI cidZiyAz samApwa ho jAne ke kagAra para hEM.
(defrule edge2
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id1 ?id)
(viSeRya-of_saMbanXI  ?id ?id2)
(id-word ?id2 extinction)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kagAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge2   "  ?id "  kagAra )" crlf))
)


;$$$ Modified by 14anu-ban-04 on 03-09-2014
;### COUNTER EXAMPLE ### The plate is balanced by weights on the other side, with its horizontal edge just over water.  
;### COUNTER EXAMPLE ###  प्लेट के क्षैतिज निचले किनारे को पानी से थोड़ा ऊपर रखकर, तुला के दूसरी ओर बाट रखकर संतुलित कर लेते हैं.
;added by Pramila(BU) on 30-11-2013
;So far as professional qualifications go Meera has an edge over Ravi.       ;sentence of this file
;jahAz waka yogyawA kA praSna hE mIrA ravi se WodZA Age hE.
(defrule edge3
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-over_saMbanXI  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))   ;added by 14anu-ban-04
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodZA_Age))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge3   "  ?id "  WodZA_Age )" crlf))
)

;added by Pramila(BU) on 30-11-2013
;Meera kept her calm but her voice showed a distinct edge.          ;sentence of this file
;mIrA SAnWa rahI para usakI AvAjZa meM WodZI wIkRNawA WI.
(defrule edge4
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 distinct)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIkRNawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge4   "  ?id "  wIkRNawA )" crlf))
)

;added by Pramila(BU) on 30-11-2013
;The garden has a brick path edged with small grass.             ;sentence of this file
;bagIce meM GAsa ke sAWa lagA huA eka IMtoM kA mAraga hE.
(defrule edge5
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge5   "  ?id "  lagA_ho )" crlf))
)

;added by Pramila(BU) on 30-11-2013
;The man edged slowly towards her.            ;sentence of this file
;vaha AxamI XIre-XIre usakI ora Kisaka rahA WA.
(defrule edge6
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(kriyA-towards_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kisaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge6   "  ?id "  Kisaka )" crlf))
)


(defrule edge7
(declare (salience 4000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge7   "  ?id "  kinArA_lagA )" crlf))
)

;@@@ added by Pramila(BU) on 16-12-2013
;Gripping the edge firmly, he pulled the entire portion up. 
(defrule edge8
(declare (salience 4800))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(AjFArWaka_kriyA  ?id1)
(viSeRya-kqxanwa_viSeRaNa  ?id2 ?id1)
(id-root ?id2 pull)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge8   "  ?id "  kinArA )" crlf))
)

;@@@ Added by 14anu-ban-04 on 03-09-2014
;The plate is balanced by weights on the other side, with its horizontal edge just over water.  
;प्लेट के क्षैतिज निचले किनारे को पानी से थोड़ा ऊपर रखकर, तुला के दूसरी ओर बाट रखकर संतुलित कर लेते हैं.
(defrule edge9
(declare (salience 4010))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 horizontal|vertical)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge9   "  ?id "  kinArA )" crlf))
)

;@@@ Added by 14anu-ban-04 on 03-09-2014
;If the connecting string is cut at this instant, the bob will execute a projectile motion with horizontal projection akin to a rock kicked horizontally from the edge of a cliff.  [ncert-corpus]
;यदि इस क्षण पर डोरी को काट दिया जाए तो गोलक एक क्षैतिज प्रक्षेप की भान्ति प्रक्षेप्य गति ठीक उसी प्रकार दर्शाएगा जैसा कि चट्टान के किनारे से क्षैतिज दिशा में किसी पत्थर को फेकने पर होता है.
;The phase of the supply is adjusted so that when the positive ions arrive at the edge of D1, D2 is at a lower potential and the ions are accelerated across the gap.                      [ncert-corpus]
;स्रोत का कला का समायोजन इस प्रकार किया जाता है कि जब धनायन D1 के किनारे पर पहुँचता है तो उस समय D2 निम्न विभव पर होता है तथा आयन इस रिक्त स्थान में त्वरित होते हैं.     [ncert-corpus] 
(defrule edge10
(declare (salience 4020))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
;(id-root ?id1 cliff)                                ;commented by 14anu-ban-04 (09-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA/Cora))               ;added 'Cora' by 14anu-ban-04 (09-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge10  "  ?id "  kinArA/Cora)" crlf))                                                 ;added 'Cora' by 14anu-ban-04 (09-02-2015)
)                             

;"edge","VT","1.kinArA_lagAnA"
;The table-cloth is edged with lace.
;
;LEVEL 
;Headword : edge
;
;Examples --
;
;--"1.kinArA"
;He kept the glass on the edge of the table.
;usane gilAsa mejZa ke kinAre para raKa xiyA.
;She was standing on the edge of the cliff.
;vaha cattAna ke kinAre para KadZI WI.
;--"2.XAra"
;To dress the chicken I need a knife with a sharp edge.
;murge ko sAPa karane ke liye muJe eka wejZa XAra vAlA cAkU cAhie.
;--"3.kagAra"
;Many birds in India are on the edge of extinction.
;BArawa kI bahuwa sI cidZiyAz samApwa ho jAne ke kagAra para hEM.
;--"4.WodZA_sA_Age"
;So far as professional qualifications go Meera has an edge over Ravi.
;jahAz waka yogyawA kA praSna hE mIrA ravi se WodZA Age hE.
;--"5.wIkRNawA"
;Meera kept her calm but her voice showed a distinct edge.
;mIrA SAnWa rahI para usakI AvAjZa meM WodZI wIkRNawA WI.
;--"6.kinArevAlA"
;The garden has a brick path edged with small grass.
;bagIce meM GAsa ke kinArevAlI eka IMtoM kI pagadaNdI hE.
;--"7.KisakanA" 
;The man edged slowly towards her.
;vaha AxamI XIre-XIre usakI ora Kisakane lagA.
;
;anwarnihiwa sUwra ;
;
;kinArA - mahIna kinAre jiwanA wIkRNa -XAra -kinArA pAra kara lenA -WodA sA Age baDZa jAnA-KisakanA
;
;
;sUwra : [wIkRNa]_kinArA`[>KisakanA]

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_edge4
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 distinct)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIkRNawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " edge.clp   sub_samA_edge4   "   ?id " wIkRNawA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_edge4
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 distinct)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIkRNawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " edge.clp   obj_samA_edge4   "   ?id " wIkRNawA )" crlf))
)

;@@@ Added by 14anu23 on 27/6/14
;They have the edge on us.
;उन्हें हमे पर बढत हासिल  है . 
(defrule edge11
(declare (salience 4000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZawa_hAsila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge11   "  ?id " baDZawa_hAsila )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 23.06.2014 email-id:sahni.gourav0123@gmail.com
;Be careful—it has a sharp edge.
;सावधान रहना -इसमे एक तेज धार है. 
(defrule edge12
(declare (salience 4800))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge12   "  ?id "  XAra )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 23.06.2014 email-id:sahni.gourav0123@gmail.com
;They had brought the country to the edge of disaster.
;वे तबाही के कगार के लिए देश लाया था .  
(defrule edge13
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 disaster|revolution|dispute|war)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kagAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge13   "  ?id "  kagAra )" crlf))
)

;$$$ Modified by 14anu-ban-04 (09-01-2015)              ---meaning changed from 'wIvrawA' to baDawa'
;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 23.06.2014 email-id:sahni.gourav0123@gmail.com
;The company needs to improve its competitive edge.
;कंपनी ने अपने प्रतियोगिकं  तीव्रता में सुधार की जरूरत है. 
;कंपनी को अपनी प्रतिस्पर्धात्मक  बढ़त में सुधार करने की जरूरत है।             ;translation corrected by 14anu-ban-04 (09-01-2015)
(defrule edge14
(declare (salience 5000))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)         ;relation changed from 'viSeRya-RaRTI_viSeRaNa'to 'viSeRya-viSeRaNa' by 14anu-ban-04 (09-01-2015) 
(id-root ?id1 competitive)          ;addded by 14anu-ban-04 (09-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge14   "  ?id "  baDawa )" crlf))
)


;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 23.06.2014 email-id:sahni.gourav0123@gmail.com
;He did his best to remain calm, but there was a distinct edge to his voice. 
;उसने शान्त रहने के लिए उसका सर्वोत्तम किया, परन्तु उसकी आवाज को एक सामान्य से भिन्न तीव्रता थी . 
;उसने शान्त रहने के लिए उसका सर्वोत्तम प्रयास किया,परन्तु उसकी आवाज में  एक  भिन्न तीव्रता थी .      ;translation corrected by 14anu-ban-04 (09-01-2015)
(defrule edge15
(declare (salience 5200))
(id-root ?id edge)
?mng <-(meaning_to_be_decided ?id)
(kriyA-aBihiwa ? ?id)
(viSeRya-to_saMbanXI ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIvrawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  edge.clp 	edge15   "  ?id "  wIvrawA )" crlf))
)
