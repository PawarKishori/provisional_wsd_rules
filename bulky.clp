;@@@Added by 14anu-ban-02(20-04-2015)
;The key felt bulky in his pocket.[oald]
;उसकी जेब में चाबी भारी महसूस हुई .[self] 
(defrule bulky1
(declare (salience 100)) 
(id-root ?id bulky) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 pocket)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id BArI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bulky.clp  bulky1  "  ?id "  BArI )" crlf)) 
) 
;@@@Added by 14anu-ban-02(20-04-2015)
;The bulky figure of Inspector Jones appeared at the door.[oald]
;इंस्पेक्टर जोन्स का भारी स्वरूप दरवाजे पर दिखा .[self] 
(defrule bulky2
(declare (salience 100)) 
(id-root ?id bulky) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 figure)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id BArI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bulky.clp  bulky2  "  ?id "  BArI )" crlf)) 
) 
;-----------------------------default_rules--------------------------------
;@@@Added by 14anu-ban-02(20-04-2015)
;Bulky items will be collected separately.[oald]
;बडी चीजें अलग से ली जाएँगी .[self] 
(defrule bulky0
(declare (salience 0)) 
(id-root ?id bulky) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id badZA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bulky.clp  bulky0  "  ?id "  badZA )" crlf)) 
) 
