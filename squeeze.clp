;@@@ Added by 14anu-ban-01 on (16-01-2015)
;The children squeezed through a gap in the wall.[cambridge]
;बच्चे दीवार में खुली जगह/दरार के माध्यम से निकले .[self]
(defrule squeeze1
(declare (salience 5000))
(id-root ?id squeeze)
?mng <-(meaning_to_be_decided ?id)
(not(kriyA-object ?id ?))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squeeze.clp 	squeeze1   "  ?id "  nikala )" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-01-2015)
;To squeeze the juice from a lemon.[oald]
;नीबू से रस निचोड़ने/निकालने के लिये.[self]
(defrule squeeze2
(declare (salience 5000))
(id-root ?id squeeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 juice|water|cloth)
(kriyA-from_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nicodZa/nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squeeze.clp 	squeeze2  "  ?id "  nicodZa/nikAla )" crlf))
)


;@@@ Added by 14anu-ban-01 on (16-01-2015)
;He took off his wet clothes and squeezed the water out.
;उसने अपने गीले कपड़े उतारे और उनसे पानी निचोड़ा. [self]
(defrule squeeze3
(declare (salience 5000))
(id-root ?id squeeze)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 out)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nicodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " squeeze.clp 	squeeze3  "  ?id "  "?id1 " nicodZa)" crlf))
)


;@@@ Added by 14anu-ban-01 on (16-01-2015)
;I like freshly squeezed orange juice.[self: with reference to oald]
;मुझे ताज़ा निकाला हुआ संतरे का रस पसन्द है. [self]
(defrule squeeze4
(declare (salience 5000))
(id-root ?id squeeze)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAlA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squeeze.clp 	squeeze4  "  ?id "  nikAlA_huA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (16-01-2015)
;She felt as if every drop of emotion had been squeezed from her.[oald]
;उसने महसूस किया (मानो/जैसे कि) उसके मनोभाव की हर एक बूँद निचोड़ ली गई  थी. [self]
(defrule squeeze5
(declare (salience 5000))
(id-root ?id squeeze)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-subject  ?id ?)
(kriyA-from_saMbanXI ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nicodZa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squeeze.clp 	squeeze5  "  ?id "  nicodZa_le )" crlf))
)

;---------------------Default rule ----------------------

;@@@ Added by 14anu-ban-01 on (16-01-2015)
;He squeezed a tube of toothpaste. [self: with reference to oald]
; उसने दन्तमन्जन का  ट्यूब दबाया. [self]
(defrule squeeze0
(declare (salience 0))
(id-root ?id squeeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squeeze.clp  squeeze0   "  ?id "  xabA )" crlf))
)

