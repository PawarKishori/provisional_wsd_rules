;@@@ Added by 14anu-ban-06 (24-03-2015)
;The cakes looked appetizing but heftily priced.(OALD)
;केक स्वादिष्ट  दिख रहे थे परन्तु बहुत ज्यादा मूल्य के थे. (manual)
(defrule heftily1
(declare (salience 2000))
(id-root ?id heftily)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 price)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heftily.clp 	heftily1   "  ?id "  bahuwa_jyAxA )" crlf))
)
;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (24-03-2015)
;Shake the potatoes quite heftily in the saucepan.(OALD)[parser no. -9]
;आलू ढेगची में काफी जोर से हिलाइए . (manual) 
(defrule heftily0
(declare (salience 0))
(id-root ?id heftily)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heftily.clp 	heftily0   "  ?id "  jora_se )" crlf))
)

