
(defrule haze0
(declare (salience 5000))
(id-root ?id haze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulaJana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  haze.clp 	haze0   "  ?id "  ulaJana )" crlf))
)

;"haze","N","1.ulaJana"
;Her mind was in a haze regarding her marriage.
;usa ke mana meM apane vivAha ke viRaya meM ulaJana WI.
;
(defrule haze1
(declare (salience 4900))
(id-root ?id haze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  haze.clp 	haze1   "  ?id "  pareSAna_kara )" crlf))
)

;"haze","V","1.pareSAna_karanA"
;The old students haze the new ones in the college .
;kAleja meM naye vixyArWI ko purAne vixyArWI pareSAna karawe hEM
;

;@@@ Added by 14anu-ban-06    (5-08-14)
;Rains  , snow or hailstorm  , haze or fog  , snowy storm  , darkness  , cold  , heat  , storm or lightening etc . all are different parts of weather  . (Parallel Corpus)
;वर्षा , बर्फ़ अथवा ओलों का गिरना , धुंध अथवा कोहरा , बर्फ़ीले तूफान , अँधेरा , सर्दी , गर्मी , आँधी अथवा बिजली का चमकना या गिरना इत्यादि सभी मौसम के ही विभिन्न अंग हैं ।
;If it starts hailing or snow storms comes or high wind starts blowing or the fog or haze gets very dense then you should stop at a safe place and wait for the weather to get clear  .  (Parallel Corpus)
;यदि मार्ग में ओले पड़ने लगें , बर्फ़ीला तूफान आ जाए अथवा तेज आँधी चलने लगे , धुंध अथवा कोहरा बहुत घना हो जाए तो किसी सुरक्षित स्थान पर रुककर मौसम साफ होने की प्रतीक्षा करें ।
;Kevin said he wasn't going out with her in that haze. (COCA)
;केविन ने कहा कि वह उस धुन्ध में उसके साथ बाहर नहीं जा रहा था . 
(defrule haze2
(declare (salience 5100))
(id-root ?id haze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(conjunction-components $? ?id $?)(and(kriyA-in_saMbanXI ? ?id)(id-root ?id1 go)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XuMXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  haze.clp 	haze2   "  ?id "  XuMXa )" crlf))
)
