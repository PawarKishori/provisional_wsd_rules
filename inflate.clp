;##############################################################################
;#  Copyright (C) 2013-2014 Prachi Rathore (prachirathore02 at gmail dot com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;He has an inflated sense of his own importance.
(defrule inflate0
(declare (salience 4000))
(id-root ?id inflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garvaSIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflate.clp 	inflate0   "  ?id "  garvaSIla )" crlf))
)

;It will then become so inflated that it will swallow the closer planets Venus and Mercury and also gobble up the Earth!
;फिर वह इतना फैल जायेगा कि वह अपने पास के बुध ग्रह और शुक्र ग्रह को अपने में समेट लेगा और पृथ्वी को भी हजम कर जायेगा।
(defrule inflate1
(declare (salience 5000))
(id-root ?id inflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-vAkyakarma  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEla_jAyegA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflate.clp 	inflate1   "  ?id "  PEla_jAyegA )" crlf))
)

;The media have grossly inflated the significance of this meeting. 
(defrule inflate2
(declare (salience 5000))
(id-root ?id inflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA_cadA_kara_bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflate.clp 	inflate2   "  ?id "  badA_cadA_kara_bawA )" crlf))
)

;The balloons had been inflated with helium. 
(defrule inflate3
(declare (salience 4000))
(id-root ?id inflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflate.clp 	inflate3   "  ?id "  PulA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (25-11-2014)
;The principal effect of the demand for new houses was to inflate prices.(OALD)
;नये घरों की माँग का प्रमुख प्रभाव मूल्यो को  बढाना था .(manual) ;translation added by 14anu-ban-06 (25-11-2014)
(defrule inflate4
(declare (salience 5000))
(id-root ?id inflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ? ?id);added by 14anu-ban-06 (25-11-2014)
;(id-root ?id1 price|salary|rate|number);commented by 14anu-ban-06 (25-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDA));meaning changed from 'bada' to 'baDA' by 14anu-ban-06 (25-11-2014) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflate.clp 	inflate4   "  ?id "  baDA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (25-11-2014)
;I was willing to pay the grossly inflated prices.(COCA);added by 14anu-ban-06 (25-11-2014)
;मैं अत्यंत  बढे हुए मूल्यो को चुकाने के लिए  उद्यत था . (manual)
;Inflated prices.
(defrule inflate5
(declare (salience 5000))
(id-root ?id inflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id);added by 14anu-ban-06 (25-11-2014)
(id-root ?id1 price|salary|rate|margin|profit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDe_hue));meaning changed from 'bade_huye' to 'baDe_hue' by 14anu-ban-06 (25-11-2014) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflate.clp 	inflate5   "  ?id "  baDe_hue )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-09-2014)
;On the other hand, a fully inflated balloon when immersed in cold water would start shrinking due to contraction of the air inside.(NCERT)
;इसके विपरीत, जब किसी पूर्णतः  फूले  हुए गुब्बारे को शीतल जल में डुबाते हैं तो वह भीतर की वायु के सिकुड़ने के कारण सिकुड़ना आरंभ कर देता है.
(defrule inflate6
(declare (salience 5050))
(id-root ?id inflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 balloon)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflate.clp 	inflate6   "  ?id "  PUla )" crlf))
)

;@@@ Added by 14anu-ban-06 (20-02-2015)
;They inflated their part in the rescue every time they told the story. (cambridge)
;हर बार जब उन्होंने कहानी सुनाई  उन्होंने बचाव में अपनी भागीदारी को बढा चढा दिया . (manual)
(defrule inflate7
(declare (salience 5100))
(id-root ?id inflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 part)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZA_caDZA_xe))
(assert  (id-wsd_viBakwi   ?id1  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflate.clp 	inflate7   "  ?id "  baDZA_caDZA_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  inflate.clp      inflate7   "  ?id1 " ko )" crlf)
)
)
