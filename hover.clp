;##############################################################################
;#  Copyright (C) 2013-2014  Prachi Rathore (prachirathore02@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-06 (04-12-2014)
;@@@ Added by 14anu13 on 28-06-14
;In evening time they are said to hover around rivers , lakes and deserted gardens and forests and so the people shun these places after dark .
;प्राय : ये भूत तथा चुडैल श्मशानों , नदी - तटों निर्जन वन - वाटिकाओं में  मँडराते है , अंत : लोग ऐसे स्थानों पर अकेले जाने में भय अनुभव करते है .
(defrule hover6
(declare (salience 6000))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(kriyA-around_saMbanXI 	?id ?id1);added 'id1' by 14anu-ban-06 (04-12-2014)
(id-cat_coarse ?id verb)
(id-root ?id1 lake|garden|forest|river);added by 14anu-ban-06 (04-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mazdaZrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hover.clp 	hover6   "  ?id "  mazdaZrA )" crlf))
)

;@@@ Added by Prachi Rathore
;She was hovering between life and death.[oald]
;वह जीवन और मृत्यु के बीच झूल रही थी . 
(defrule hover0
(declare (salience 5500))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-between_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hover.clp 	hover0   "  ?id "  JUla )" crlf))
)

;@@@ Added by Prachi Rathore
;A hawk hovered over the hill.[oald]
;बाज ने पहाडी के ऊपर एक जगह चक्कर काटा . 
(defrule hover1
(declare (salience 5000))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 moon|hawk|bird)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_jagaha_cakkara_kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hover.clp 	hover1   "  ?id "  eka_jagaha_cakkara_kAta )" crlf))
)

;@@@ Added by Prachi Rathore
;He hovered on the edge of consciousness.[oald]
;वह चेतना की अवस्था के आस पास था . 
(defrule hover2
(declare (salience 5000))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-word ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Asa_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hover.clp 	hover2   "  ?id "  Asa_pAsa )" crlf))
)

;$$$ Modified by 14anu-ban-06 (04-12-2014)
;In early September, as the temperature hovered around 97 degrees. (COCA)
;प्रारम्भिक सितम्बर में, पहले की तरह तापमान 97 अंश के आस पास था. (manual)
;The outside temperature hovered around zero. (COCA)
;बाह्य तापमान शून्य के आस पास था. (manual)
;@@@ Added by Prachi Rathore
;Temperatures hovered around freezing.
;तापमान जमा देने के आस पास . 
(defrule hover3
(declare (salience 5000))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI  ?id ?id1)
(id-root ?id1 freeze|percent|temparature|degree|zero);added 'degree|zero' by 14anu-ban-06 (04-12-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) Asa_pAsa))
(assert  (id-wsd_viBakwi   ?id1  ke));added by 14anu-ban-06 (04-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hover.clp	hover3  "  ?id "  " (+ ?id 1) "  Asa_pAsa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  hover.clp      hover3   "  ?id1 " ke )" crlf);added by 14anu-ban-06 (04-12-2014)
)
)

;@@@ Added by Prachi Rathore
;You must not hover on right matters.[SikSArWI_kosh]
;आपको सही विषयों पर नहीं हिचकिचाना चाहिए . 
(defrule hover4
(declare (salience 5500))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 matter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hicakicA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hover.clp 	hover4   "  ?id "  hicakicA )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-12-2014)
;While mortgage rates have hovered between 5.27 and 7.47 percent.(COCA)
;जबकि गिरवी दरें 5.27 और 7.47 प्रतिशत के बीच अस्थिर हैं  . (manual)
(defrule hover7
(declare (salience 5500))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-between_saMbanXI ?id ?id1)
(id-cat_coarse ?id1 number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asWira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hover.clp 	hover7   "  ?id "  asWira )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-12-2014)
;A smile hovered on her lips.(OALD)
;मुस्कराहट उसके ओंठ पर आई . (manual)
;Sudden tears hovered on her eyelashes.(COCA)
;अचानक आँसू उसकी बरौनियों पर आए. (manual)
;The sun hovered on the horizon.(COCA) 
;सूरज क्षितिज पर आया.(manual)
(defrule hover8
(declare (salience 5500))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI ?id ?id2)
(kriyA-subject  ?id ?id1)
(id-root ?id1 smile|tears|sun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hover.clp 	hover8   "  ?id "  A )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx
;@@@ Added by Prachi Rathore
;Bees hovered around the hive.  [m-w]
;मधुमक्खियाँ मधुमक्खियों का छत्ते पर मँड़राती हैं.  
(defrule hover5
(declare (salience 4000))
(id-root ?id hover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mazdaZrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hover.clp 	hover5   "  ?id "  mazdaZrA )" crlf))
)
