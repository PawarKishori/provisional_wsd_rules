;##############################################################################
;#  Copyright (C) 2013-2014 Pramila (pramila3005@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-04 (13-04-2015)            ------changed meaning from 'bahakA' to ' pakada_le'
;@@@ Added by Pramila(Banasthali University) on 30-01-2014
;Some goondas ensnared the boys.       ;shiksharthi
;कुछ गुंडो ने लड़को को बहकाया.
;कुछ गुंडों ने लड़कों को पकड़ लिया.            ;modified translation by 14anu-ban-04 (13-04-2015)
(defrule ensnare0
(declare (salience 4010))           ;salience reduced from '5000' to '4010' by 14anu-ban-04 on (13-04-2015)   
(id-root ?id ensnare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-object  ?id ?id1)
;(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))        ;commented by 14anu-ban-04 (13-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakada_le))
;(assert (kriyA_id-subject_viBakwi ?id ne))      ;commented by 14anu-ban-04 (13-04-2015) 
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ensnare.clp 	ensnare0   "  ?id "  pakada_le )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  ensnare.clp      ensnare0   "  ?id " ne )" crlf)                                ;commented by 14anu-ban-04 (13-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  ensnare.clp     ensnare0   "  ?id " ko )" crlf)
)
)

;$$$ Modified by 14anu-ban-04 (13-04-2015)          ------changed meaning from 'bahaka' to 'bahakA' 
;@@@ Added by Pramila(Banasthali University) on 30-01-2014
;He was ensnared by his charm.       ;shiksharthi
;वह उसके सौन्दर्य से बहक गया.
;उसको उसके  सौंदर्य के द्वारा बहकाया गया था . 
(defrule ensnare1
(declare (salience 5000))
(id-root ?id ensnare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-karma  ?id ?id1)           
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-by_saMbanXI ?id ?id2)                          ;added by 14anu-ban-04 (13-04-2015) 
(id-root ?id2 charm)                                  ;added by  14anu-ban-04 (13-04-2015) 
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id bahakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  ensnare.clp     ensnare1   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ensnare.clp 	ensnare1   "  ?id "  bahakA )" crlf)
)
)

;@@@ Added by 14anu-ban-04 (13-04-2015)
;He became ensnared in the complexities of the legal system.              [oald]
;वह कानूनी प्रणाली की जटिलता में उलझा हुआ है .                                         [self]
(defrule ensnare4
(declare (salience 4010))
(id-root ?id ensnare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(subject-subject_samAnAXikaraNa   ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-in_saMbanXI ?id ?id2)
(id-root ?id2 complexity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ensnare.clp 	ensnare4   "  ?id "  ulaJa )" crlf)
)
)

;@@@ Added by 14anu-ban-04 (13-04-2015)
;Spiders ensnare flies and other insects in their webs.    [cald]       ;run on parse no. 6
;मकड़ियाँ  मक्खियों और अन्य कीड़ों को अपने जाल  में फँसाती हैं .                [self]
(defrule ensnare5
(declare (salience 5010))
(id-root ?id ensnare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(viSeRya-in_saMbanXI ?id1 ?id2)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id PazsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  ensnare.clp     ensnare5   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ensnare.clp 	ensnare5   "  ?id "  PazsA )" crlf)
)
)

;@@@ Added by 14anu-ban-04 (13-04-2015)
;The animals got ensnared in the net.                       [merriam-webster]
;पशुओं को जाल में फँसाया गया .                                         [self]
(defrule ensnare6
(declare (salience 4020))
(id-root ?id ensnare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(kriyA-in_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id PazsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  ensnare.clp     ensnare6   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ensnare.clp 	ensnare6  "  ?id "  PazsA)" crlf)
)
)


;--------------- Default Rules------------------------------------------

;$$$ Modified by 14anu-ban-04 (13-04-2015)
;@@@ Added by Pramila(Banasthali University) on 30-01-2014
;A deer was ensnared by hunters.         ;shiksharthi
;एक हिरन को शिकारियों द्वारा फंदे में फँसाया गया. 
;एक हिरन को शिकारियों के द्वारा  फंदे में फँसाया गया था.          ;modified translation by 14anu-ban-04 (13-04-2015)
(defrule ensnare2
(declare (salience 4000))
(id-root ?id ensnare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PMxe_meM_PazsA))
(assert (kriyA_id-subject_viBakwi ?id ko))         ;changed 'object_viBakwi' to 'subject_viBakwi'  by 14anu-ban-04 (13-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ensnare.clp 	ensnare2   "  ?id "  PMxe_meM_PazsA )" crlf)      
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  ensnare.clp     ensnare2   "  ?id " ko )" crlf))                                 ; changed 'object_viBakwi' to 'subject_viBakwi'  by 14anu-ban-04 (13-04-2015)
)

;@@@ Added by Pramila(Banasthali University) on 30-01-2014
(defrule ensnare3
(declare (salience 0))
(id-root ?id ensnare)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PMxe_meM_PazsA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ensnare.clp 	ensnare3   "  ?id "  PMxe_meM_PazsA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  ensnare.clp     ensnare3   "  ?id " ko )" crlf)
)
)
