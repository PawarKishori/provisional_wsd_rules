;@@@Added by 14anu-ban-02(17-02-2016)
;Science is ever dynamic.[ncert 11_01]
;विज्ञान सदैव गतिशील है.[ncert 11_01]
(defrule ever1
(declare (salience 100))
(id-root ?id ever)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka  ?id1 ?id)
(id-root ?id1 dynamic|after)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saxEva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ever.clp 	ever1   "  ?id "  saxEva )" crlf))
)

;--------------------Default_rule------------------

;@@@Added by 14anu-ban-02(17-02-2016)
;Nothing ever happens here in the evening.[cald]
;यहां सन्ध्या को  कभी भी कुछ नही होता है . [self]
(defrule ever0
(declare (salience 0))
(id-root ?id ever)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaBI_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ever.clp 	ever0   "  ?id "  kaBI_BI )" crlf))
)
