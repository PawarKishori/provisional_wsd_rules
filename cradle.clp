
;@@@ Added by (14anu06) Vivek Agarwal, BTech in CS, MNNIT Allahabad on 31 May 2014
; vivek17.agarwal@gmail.com
; This place was the cradle of the civilisation.
; इस जगह सभ्यता का जन्मस्थान था.
(defrule cradle2
(declare (salience 5000))
(id-root ?id cradle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id2)
(id-word ?id2 civilisation)                 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jnamsaWAn))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cradle.clp     cradle2   "  ?id "  jnamsaWAn )" crlf))
)

;$$$ Modified by 14anu-ban-03 (07-01-2015)
;@@@ Added by (14anu06) Vivek Agarwal, BTech in CS, MNNIT Allahabad on 31 May 2014
; vivek17.agarwal@gmail.com
; He cradled the infant in his arms.
; उसने उसकी बाहों मे शिशु को न्मर्ता से पकडा. 
(defrule cradle3
(declare (salience 4900))
(id-root ?id cradle)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1) 	;added by 14anu-ban-03 (07-01-2015)
(id-root ?id1 arms) 		;added by 14anu-ban-03 (07-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id namrawA_se_pakada))  ;meaning changed from 'nmarwA_se_pkdA' to 'namrawA_se_pakada' by 14anu-ban-03 (07-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cradle.clp 	cradle3   "  ?id "  namrawA_se_pakada )" crlf))
)

;---------------------- Default rules -------------------------
(defrule cradle0
(declare (salience 5000))
(id-root ?id cradle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hiMdolA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cradle.clp 	cradle0   "  ?id "  hiMdolA )" crlf))
)

;"cradle","N","1.hiMdolA"
;He was taught from the cradle never to cry
;
(defrule cradle1
(declare (salience 4900))
(id-root ?id cradle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hiMdole_meM_litA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cradle.clp 	cradle1   "  ?id "  hiMdole_meM_litA )" crlf))
)

;"cradle","VT","1.hiMdole_meM_litAnA"
;He cradled the infant in his arms
;

