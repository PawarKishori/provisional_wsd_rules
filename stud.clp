;@@@ Added by 14anu-ban-11 on (24-02-2015)  ;Note:-- Working properly on parser no. 3.
;Horses at stud.(oald)
;पशुशाला मे घोडे .(self) 
(defrule stud1
(declare (salience 10))
(id-root ?id stud)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-at_saMbanXI  ?id1 ?id)
(id-root ?id1 horse)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paSuSAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stud.clp 	stud1   "  ?id "  paSuSAlA )" crlf))
)


;@@@ Added by 14anu-ban-11 on (24-02-2015)
;Please stitch this stud on my coat.(oald)
;कृपया मेरे कोट पर यह बटन सीजिए . (self)
(defrule stud2
(declare (salience 20))
(id-root ?id stud)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 stitch)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id batana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stud.clp 	stud2   "  ?id "  batana )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (24-02-2015)
;She wore diamond studs in her ears. (hinkhoj)
;उसने अपने कानों में हीरे के बुन्दे पहने .(self) 
(defrule stud0
(declare (salience 00))
(id-root ?id stud)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bunxe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stud.clp     stud0  "  ?id "  bunxe)" crlf))
)

;@@@ Added by 14anu-ban-11 on (24-02-2015)
;Stars studded the sky. (oald)
;तारों ने आसमान को भर दिया . (self)
(defrule stud3
(declare (salience 30))
(id-root ?id stud)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stud.clp 	stud3  "  ?id "  Bara_xe )" crlf))
)


