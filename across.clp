
(defrule across0
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) accept|acknowledge|add|admit|agree|allege|announce|answer|argue|arrange|assert|assume|assure|believe|boast|check|claim|comment|complain|concede|conclude|confirm|consider|contend|convince|decide|demonstrate|deny|determine|discover|dispute|doubt|dream|elicit|ensure|estimate|expect|explain|fear|feel|figure|find|foresee|forget|gather|guarantee|guess|hear|hold|hope|imagine|imply|indicate|inform|insist|judge|know|learn|maintain|mean|mention|note|notice|notify|object|observe|perceive|persuade|pledge|pray|predict|pretend|promise|prophesy|prove|read|realize|reason|reassure|recall|reckon|record|reflect|remark|remember|repeat|reply|report|require|resolve|reveal|say|see|sense|show|state|suggest|suppose|swear|teach|tell|think|threaten|understand|vow|warn|wish|worry|write)
(id-word =(+ ?id 1) what|when|where|why|how|who)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isa_ke_pAra_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp 	across0   "  ?id "  isa_ke_pAra_ki )" crlf))
)

; He told us about who killed whom.
(defrule across1
(declare (salience 4900))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) what|when|where|why|how|who)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id usa_ke_oAra_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp 	across1   "  ?id "  usa_ke_oAra_ki )" crlf))
)

;$$$Modified by 14anu-ban-02(11-02-2015)
;The phase of the supply is adjusted so that when the positive ions arrive at the edge of D1, D2 is at a lower potential and the ions are accelerated across the gap.[ncert 12_04]
;स्रोत का कला का समायोजन इस प्रकार किया जाता है कि जब धनायन D1 के छोर पर पहुँचता है तो उस समय D2 निम्न विभव पर होता है तथा आयन इस रिक्त स्थान में त्वरित होते हैं.[ncert]
;Added by Meena(15.01.10)
;The jet zoomed across the sky.
(defrule across3
(declare (salience 4800))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id));added by 14anu-ban-02(11-02-2015)
;(kriyA-across_saMbanXI  =(- ?id 1) ?id1)	;commented by 14anu-ban-02(11-02-2015)
(kriyA-across_saMbanXI ?id1 ?id2)		;added by 14anu-ban-02(11-02-2015)
(id-root ?id2 sky|fog|gap)			;id-word changed into id-root and 'gap' is added by 14anu-ban-02(11-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across3   "  ?id "  meM )" crlf))
)

;$$$ Modified by 14anu-ban-02(13-03-2015)
;The swimmer swam across the river. [sd_verified]
;wErAka naxI ke usa pAra wErA.[sd_verified]
;###[COUNTER STATEMENT]As part of the centenary celebrations a chain of beacons was lit across the region.[cald]
;###[COUNTER STATEMENT]एक कर्तव्य  के रूप में शतवर्षीय उत्सव में  आकाशदीप की लड़ी पूरे प्रान्त में जलाई गयी थी.[self]
;$$$ modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;In her hurry she stepped on a spot of oil and slid right up to him across the floor of the shop.
;जल्दबाजी में नीचे गिरे तेल के ऊपर वह फिसल गयी और  फर्श के उस पार उसके पास जा गिरी।
;Added by Aditya and Hardik (21-06-2013),IIT(BHU) batch 2012-2017.
;I am going across the border.
(defrule across5
(declare (salience 4000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
;(kriyA-across_saMbanXI  =(- ?id 1) ?id1)
;(or	;commented by 14anu-ban-02(13-03-2015)
(kriyA-across_saMbanXI  ?id1 ?id2)
;(viSeRya-across_saMbanXI  ?id1 ?id2)	;commented by 14anu-ban-02(13-03-2015)
(id-root ?id1 go|slide|swim)	;added by 14anu-ban-02(13-03-2015)	;'swim' is added by 14anu-ban-02(31-03-2015) 
;)	;commented by 14anu-ban-02(13-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_usa_pAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across5   "  ?id "  ke_usa_pAra )" crlf))
)



;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;I drew a line across the page.[oald]
;मैंने पन्ने के आरपार रेखा खींची
(defrule across6
(declare (salience 4600))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(kriyA-across_saMbanXI  ?kri ?id1)
(kriyA-object  ?kri ?obj)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_ArapAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across6   "  ?id "  ke_ArapAra )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;A grin spread across her face.[oald]
;उसके चेहरे पर एक मुस्कराहट फैल गयी
;He hit him across the face.[oald]
;उसने उसे चेहरे पर मारा.
;It's too tight across the back.[oald]
;यह पीठ पर बहुत तंग है.
(defrule across7
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-across_saMbanXI  ?kri ?id1)(viSeRya-across_saMbanXI  ? ?id1))
(id-root ?id1 face|back)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across7   "  ?id "  para )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;This view is common across all sections of the community.[oald]
;यह दृश्य समुदाय के सभी वर्गों में समान है.
(defrule across8
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-across_saMbanXI  ?id1 ?id2)
(id-root ?id2 section|country)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across8   "  ?id "  meM )" crlf))
)




;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;We can't swim across.[oald]
;हम तैर कर पार नहीं कर सकते
(defrule across9
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?kri ?id)
(id-root ?kri swim|help)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isa_Cora_se_usa_Cora_waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across9   "  ?id "  isa_Cora_se_usa_Cora_waka )" crlf))
)
;note: a better meaning is required.


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;I helped the old lady across.[oald]
;मैंने बूढ़ी औरत की इस छोर से उस छोर तक मदद की
;(defrule across10
;(declare (salience 5000))
;(id-root ?id across)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-upasarga  ?kri ?id)
;(id-root ?kri help)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id isa_Cora_se_usa_Cora_waka))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across10   "  ?id "  isa_Cora_se_usa_Cora_waka )" crlf))
;)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;When my name was called, he looked across at me.[oald]
;जब मेरा नाम कहा पुकारा गया तो उसने मेरी ओर देखा . 
(defrule across11
(declare (salience 5500))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?kri ?id)
(kriyA-at_saMbanXI  ?kri ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across11   "  ?id "  - )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;He leaned across to pick up his wallet.[oald]
;वह उसका झोला उठाने के लिये नीचे कि ओर झुका  
(defrule across12
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?kri ?id)
(id-root ?kri lean)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIce_kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across12   "  ?id "  nIce_kI_ora )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;There's a school just across from our house.[oald]
;हमारे घर से ठीक दूसरी तरफ विद्यालय है . 
(defrule across13
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) from)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUsarI_waraPa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across13   "  ?id "  xUsarI_waraPa )" crlf))
)

;Modified by 14anu-ban-02(13-03-2015) ;correct syntax error.
;@@@ Added by  14anu-ban-06 (26-7-2014)
;For this same reason our country full of diversity is often attracting the tourists across the world today .(Parallel Corpus)
;यही वजह है कि विविधताओं से भरा हमारा देश आज दुनिया भर के पर्यटकों को बराबर आकर्षित कर रहा है ।
;The result of that is more nuclear weapons programs all across the world.  (COCA)
;usakA pariNAma pUre xuniyA Bara meM aXika nABikIya aswra progrAma hE.
(defrule across14
(declare (salience 5500))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) the)
(id-root =(+ ?id 2) world)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) xuniyA_Bara_meM))
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " across.clp	 across14  "  ?id "  " =(+ ?id 1) =(+ ?id 2) " xuniyA_Bara_meM  )" crlf))	;commented by 14anu-ban-02(13-03-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " across.clp	 across14  "  ?id "  " (+ ?id 1) (+ ?id 2) " xuniyA_Bara_meM  )" crlf))	;added by 14anu-ban-02(13-03-2015)
)

;$$$Modified by 14anu-ban-02(02-02-2015)
;He will put our views across to the minister. 
;वह प्रधान मन्त्री के सामने हमारे विचार रखेगा .
;@@@ Added by 14anu07 0n 01/07/2014
;He will put our views across to the Prime Minister. 
;वह प्रधान मन्त्री के सामने हमारे विचार रखेगा . 
(defrule across15
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(kriyA-across_to_saMbanXI  ?id1 ?id2)	;added by 14anu-ban-02(02-02-2015)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))	;added by 14anu-ban-02(02-02-2015)	
;(id-root =(+ ?id 1) to)	;commented by 14anu-ban-02(02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAmane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across15   "  ?id "  ke_sAmane)" crlf))
)

;@@@ Added by 14anu-ban-02 (04-12-2014)
;We consider a source which produces sinusoidally varying potential  difference across its terminals.[ncert]
;यहाँ हम एक ऐसे स्रोत की बात कर रहे हैं जो अपने सिरों के बीच ज्यावक्रीय रूप में परिवर्तनशील विभवान्तर उत्पन्न करता है.[ncert]
(defrule across16
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(viSeRya-across_saMbanXI  ?id1 ?id2)
(id-root ?id1 difference)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bIca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across16   "  ?id "  ke_bIca)" crlf))
)

;@@@ Added by 14anu-ban-02(03-02-2015)
;The rabbit hopped across the grass.[cambridge]
;खरगोश घास के ऊपर कूदा .[manual] 
(defrule across17
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-across_saMbanXI  ?id1 ?id2)
(id-root ?id2 grass)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_Upara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across17   "  ?id " ke_Upara)" crlf))
)

;@@@Added by 14anu-ban-02(13-03-2015)
;As part of the centenary celebrations a chain of beacons was lit across the region.[cald]
;शतवर्षीय उत्सव के अन्तरगत आकाशदीप की लड़ी  क्षत्र में सर्वत्र प्रकाशित की गयी थी . .[self]
(defrule across18
(declare (salience 5000))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-across_saMbanXI  ?id1 ?id2)
(id-root ?id2 region|country|world)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_sarvawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp    across18   "  ?id " meM_sarvawra)" crlf))
)

;**************DEFAULT RULES*****************************************



;Salience reduced by Meena(18.01.10)
(defrule across4
(declare (salience 0))
;(declare (salience 4700))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_saBI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp 	across4   "  ?id "  ke_saBI_ora )" crlf))
)


;"across","Prep","1.saBI_ora"
;The President's address to the nation was broadcasted across the country.
;--"2.usa_pAra_se"
;My friend shouted at me across the road.
;--"3.ke_Upara_yA_sAmane"
;She folded her arms across her chest while talking.
;--"4.vahAz_se_yahAz"
;The student drew a line across the sheet.
;


(defrule across2
(declare (salience 4800))
(id-root ?id across)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArapAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  across.clp 	across2   "  ?id "  ArapAra )" crlf))
)

;"across","Adv","1.ArapAra"
;I helped the old lady across.
;--"2.usa_pAra"
;We shall leave Delhi today && we should be across in Wagah by night.
;--"3.koI_eka_xiSA_meM"
;He leaned across to pick up his wallet.
