
;@@@ Added by Garima Singh(M.tech-C.S, Banasthali Vidyapith) 15/03/2014
;When two quantities are added or subtracted the absolute error in the final result is the sum of the absolute errors in the individual quantities[ncert]
;जब दो राशियों को सङ्कलित या व्यवकलित किया जाता है, तो अन्तिम परिणाम में निरपेक्ष त्रुटि उन राशियों की निरपेक्ष त्रुटियों के योग के बराबर होती है..
(defrule add6
(declare (salience 4750))
(id-word ?id added)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 quantities)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkaliwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " add.clp  add6  " ?id "   safkaliwa_kara  )" crlf))
)

;@@@ Added by Garima Singh(M.tech-C.S, Banasthali Vidyapith) 15/03/2014
;In other words we can add or subtract similar physical quantities.[ncert]
;दूसरे शब्दों में, हम केवल एक ही प्रकार की राशियों का सङ्कलन या व्यवकलन कर सकते हैं
(defrule add7
(declare (salience 4750))
(id-root ?id add)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 quantities)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkalana_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " add.clp  add7  " ?id "   safkalana_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  add.clp      add7   "  ?id " kA )" crlf)
)
)

;@@@ Added by Garima Singh(M.tech-C.S, Banasthali Vidyapith) 18/03/2014
;Scalars can be added subtracted multiplied and divided just as the ordinary numbers.[ncert]
;अदिशों को हम ठीक वैसे ही जोड सकते हैं, घटा सकते हैं, गुणा या भाग कर सकते हैं जैसा कि हम सामान्य सङ्ख्याओं के साथ करते हैं .
(defrule add8
(declare (salience 4750))
(id-root ?id add)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 scalars|numbers|vectors)
(or(kriyA-subject  ?id ?id1)(kriyA-object  ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZa))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " add.clp  add8  " ?id "   jodZa  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  add.clp      add8   "  ?id " ko )" crlf)
)
)

(defrule add0
(declare (salience 5000))
(id-root ?id add)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-back_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pira_jodZa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " add.clp add0 " ?id "  Pira_jodZa )" crlf)) 
)

(defrule add1
(declare (salience 4900))
(id-root ?id add)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Pira_jodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " add.clp	add1  "  ?id "  " ?id1 "  Pira_jodZa  )" crlf))
)



(defrule add2
(declare (salience 4800))
(id-root ?id add)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yoga_ho));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " add.clp add2 " ?id "  yoga_ho )" crlf)) 
)

(defrule add3
(declare (salience 4750))
(id-root ?id add)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 yoga_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " add.clp	add3  "  ?id "  " ?id1 "  yoga_ho  )" crlf))
)

;$$$ Modified by July workshop participants under Aditi and Soma guidance (10-07-14)
;removed --- (not (id-cat_coarse ?id1 number));added by Shirisha Manju (13-06-13) Suggested by Sukhada
;Changed 'wsd_word_mng' as 'wsd_root_mng' => milAo  as milA
;Add some sugar.
;kuCa cInI milAiye. 
(defrule add4
(declare (salience 4700))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id add)
(kriyA-object ?id ?id1)
(id-root ?id1 air|water|milk|juice|oil|sand|sugar|salt|mineral|acid|spices) ;added by July workshop participants on 10-07-14
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp    add4   "  ?id "  milA )" crlf))
)

;@@@ Added by Shirisha Manju Suggested by Sukhada (16-7-14)
;Add one spoonful of sugar.
;eka cammaca cInI milAiye. 
(defrule add10
(declare (salience 4700))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id add)
(kriyA-object ?id ?obj)
(viSeRya-of_saMbanXI  ?obj ?id1)
(id-root ?id1 air|water|milk|juice|oil|sand|sugar|salt|mineral|acid|spices)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp    add10   "  ?id "  milA )" crlf))
)



;@@@ Added by July workshop participants under Aditi and Soma guidance [11-07-14]
;"I bought a pen", he added.
;usane Age kahA, "mEMne kalama KarIxI".
;He added that the revolutionaries will not fall into the trap.
;usane Age kahA kI krAnwikArI jAla meM nahIM PaseMge.
(defrule add9
(declare (salience 4700))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id add)
(kriyA-vAkyakarma ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp    add9   "  ?id "  Age_kaha )" crlf))
)

;@@@ Added by 14anu-ban-02 (17-11-2014)
;Water proofing agents on the other hand are added to create a large angle of contact between the water and fibers.[ncert]
;पानी तथा रेशों के बीच सम्पर्क कोण बडा करने के लिए पानी में जल सहकारक को मिलाया जाता है.[ncert]
;The first clear proof that light added to light can produce darkness.[ncert]
;यह पहला स्पष्ट प्रमाण था कि प्रकाश से प्रकाश को मिलाने पर अँधेरा पैदा हो सकता है.[ncert]
(defrule add11
(declare (salience 4700))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id add)
(kriyA-subject ?id ?sub)
(id-root ?sub agent|light)    ;'light' is added by 14anu-ban-02(02-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp    add11   "  ?id "  milA )" crlf))
)

;@@@ Added by14anu-ban-02 (19-11-2014)
;Soil aggregation allows greater nutrient retention and utilization, decreasing the need for added nutrients.[agriculture]
;भूमि सङ्ग्रह अतिरिक्त पोषक के लिए जरूरत कम करके बढिया पोषक अवधारण और उपयोग की अनुमति देता है.[manual]
(defrule add12
(declare (salience 4700))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id add)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 nutrient)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awirikwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp    add12   "  ?id " awirikwa )" crlf))
)

;@@@ Added by 14anu-ban-02 (24-11-2014)
;Weights are added till the plate just clears water.[ncert]
;अब दूसरी ओर कुछ बाट रखते हैं जब तक कि प्लेट द्रव से कुछ अलग न हो जाए.[ncert]
(defrule add13
(declare (salience 4700))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id add)
(kriyA-subject  ?id ?id1)
(id-word ?id1 weights)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp    add13   "  ?id " raKa )" crlf))
)

;commented by 14anu-ban-02(07-01-2015)
;correct meaning is coming from add9
;@@@ Added by 14anu22
;"children should not go out in the night", he added.
;बच्चों को रात को बाहर नहीं जाना चाहिये ,उसने आगे कहा
;(defrule add14
;(declare (salience 6000))
;(id-root ?id add)
;?mng <-(meaning_to_be_decided ?id)
;(id-root =(- ?id 1)  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
;(id-last_word ?id added)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Age_kaha))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp 	add14   "  ?id "  Age_kaha )" crlf))
;)

;@@@ Added by 14anu-ban-02(06-12-2014)
;If there are more charges the fields add vectorially.[ncert]
;यदि एक से अधिक आवेश हैं तो उनके कारण उत्पन्न क्षेत्र सदिश रूप से संयोजित हो जाते हैं.[ncert]
;यदि एक से अधिक आवेश हैं तो  क्षेत्र सदिश रूप से संयोजित हो जाते हैं.[modified]
(defrule add15
(declare (salience 4700))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id add)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 vectorially )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMyojiwa_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp    add15   "  ?id " saMyojiwa_ho_jA)" crlf))
)

;@@@Added by 14anu-ban-02(13-03-2015)
;So we're going to add a little lemon zest.(zest.clp) 
;इसलिए हम थोडे नींबू के छिलके डालने जा रहे हैं .(self) 
(defrule add16
(declare (salience 4700))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id add)
;(kriyA-kriyArWa_kriyA  ?id1 ?id)	;commented by 14anu-ban-02(10-04-2015)
(kriyA-object  ?id ?id2)
(id-root ?id2 zest|salt|tomato|onion|meat)	;'tomato''onion'and 'meat' added by 14anu-ban-02(10-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp    add16   "  ?id " dAla)" crlf))
)



;------------------------- Default rules -------------------------
;$$$ Modified by July workshop participants under Aditi and Soma guidance [10-7-14]
;Changed meaning from 'Age_kaha' to 'jodZa'
;It is added to their blacklist.        COCA
;yaha unakI kAlI-sUcI meM jodZA gayA.
;A new wing was added to the building.  OALD
;Bavana meM eka nayA viNga jodZA gayA.
;New features can be added to the existing systems. COCA
;mOjUxA praNaliyoM meM nayI viSeRawAyeM jodZI jA sakawI hEM.
(defrule add5
(declare (salience 4600))
(id-root ?id add)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  add.clp 	add5   "  ?id "  jodZa )" crlf))
)

;Added by Aditya and Hardik,
;"milAo"
;Add one spoonful of sugar.
;Add a cup of water.
;kriyA-object relation between "cup" and "Add". Same relation in following.
;I want to add a cup to my collection.

