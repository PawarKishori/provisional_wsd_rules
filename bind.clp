;"binding","Adj","1.AvaSyaka"
;A binding contract
(defrule bind0
(declare (salience 5000))
(id-root ?id bind)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id binding )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id AvaSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bind.clp  	bind0   "  ?id "  AvaSyaka )" crlf))
)

;"binding","N","1.jilxa"
;The book had a leather binding
(defrule bind1
(declare (salience 4900))
(id-root ?id bind)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id binding )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jilxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bind.clp  	bind1   "  ?id "  jilxa )" crlf))
)

;$$$Modified by 14anu-ban-02(16-02-2015)
;The strong nuclear force binds protons and neutrons in a nucleus.[ncert 11_01]
;नाभिक में प्रबल नाभिकीय बल प्रोटॉनों तथा न्यूट्रॉनों को बान्धे रखता है.[ncert 11_01]
;"bind","VT","1.bAzXa"
;The Chinese would bind the feet of their women
(defrule bind2
(declare (salience 4800))
(id-root ?id bind)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzXa))
(assert (kriyA_id-object_viBakwi ?id ko))	;added by 14anu-ban-02(16-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bind.clp 	bind2   "  ?id "  bAzXa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  bind.clp 	bind2   "  ?id " ko )" crlf))
)	;added by 14anu-ban-02(16-02-2015)


;"binding","Adj","1.AvaSyaka"
;A binding contract
;--"2.baMXanavAlA"
;Tight garments are uncomfortably binding
;
