;@@@Added by 14anu-ban-02(22-04-2015)
;An abandoned car.[oald]
;बिगडी हुई गाडी . [self]
(defrule abandoned1 
(declare (salience 100)) 
(id-root ?id abandoned) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id) 
(id-root ?id1 car|scooter|bike)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id bigadZA_huA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  abandoned.clp  abandoned1  "  ?id "  bigadZA_huA )" crlf)) 
) 


;@@@Added by 14anu-ban-02(22-04-2015)
;There was an abandoned supermarket trolley in the middle of the road.[oald]	;run the sentence on parser no. 4
;सडक के मध्य में खाली सुपर बाजार की ट्राली थी . [self]
(defrule abandoned2 
(declare (salience 100)) 
(id-root ?id abandoned) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id) 
(id-root ?id1 trolley|house)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id KAlI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  abandoned.clp  abandoned2  "  ?id "  KAlI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(22-04-2015)
;He led a reckless and abandoned life and died young.[mw]	;;run the sentence on parser no. 5
;उसने अचेत और परित्यक्त जीवन व्यतीत किया और जल्द ही  मर गया .[self] 
(defrule abandoned3 
(declare (salience 100)) 
(id-root ?id abandoned) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id) 
(id-root ?id1 life)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pariwyakwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  abandoned.clp  abandoned3  "  ?id "  pariwyakwa )" crlf)) 
) 
;------------------------default_rules---------------------------------------------------

;@@@Added by 14anu-ban-02(22-04-2015)
;Sentence: An abandoned baby was found on the hospital steps.[oald]	;run the sentence on parser no. 2
;Translation: एक छोड़ा हुआ शिशु अस्पताल सीढ़ीयों पर पाया गया था . [self] 
(defrule abandoned0 
(declare (salience 0)) 
(id-root ?id abandoned) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id CodZA_huA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  abandoned.clp  abandoned0  "  ?id "  CodZA_huA )" crlf)) 
) 


