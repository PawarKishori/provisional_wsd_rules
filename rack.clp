;$$$ Modified by 14anu-ban-10 on (11-12-2014)
;@@@ Added by Vivek Agarwal(14anu06), MNNIT Allahabad on 1/7/2014*****
;Pain racked his entire body.(Source: thefreedictionary.com )
;दरद ने उसके पूरे शरीर को झकझोर दिया.
(defrule rack2
(declare (salience 5000))
(id-root ?id rack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 body|him)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JakaJora_xe))  ;meaning changed  from JakaJora to 	JakaJora_xe by 14anu-ban-10 on (11-12-2014)	;**please delete the default meaning of 'racked' from shabdanjali for the rule to work**
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rack.clp 	rack2   "  ?id "  JakaJora_xe )" crlf))
)

;@@@ Added by Vivek Agarwal(14anu06), MNNIT Allahabad on 1/7/2014*****
;Rack up points.(Source: thefreedictionary.com )
;अंक इकठ्ठे करो.
(defrule rack3
(declare (salience 5000))
(id-root ?id rack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikatTA_kara))		
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rack.clp 	rack3   "  ?id "  ikatTA_kara )" crlf))
)

;@@@ Added by14anu-ban-10 on (10-03-2015)
;Put the slave on the rack.[hinkhoj]
;किंकर को पुराने समय मे यातना देने का यंत्र  पर रखिए .[manual] 
(defrule rack4
(declare (salience 5100))
(id-root ?id rack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id purAne_samaya_me_yAwanA_xene_kA_yaMwra))		
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rack.clp 	rack4   "  ?id "   purAne_samaya_me_yAwanA_xene_kA_yaMwra)" crlf))
)

;@@@ Added by14anu-ban-10 on (10-03-2015)
;Rack pool balls.[hinkhoj]
;बुरी तरह से पछाड़ देना पूल बल मे.[manual]
(defrule rack5
(declare (salience 5200))
(id-root ?id rack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 ball)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id burI_waraha_se_paCAdZa_xe ))		
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rack.clp 	rack5   "  ?id " burI_waraha_se_paCAdZa_xe)" crlf))
)

;@@@ Added by14anu-ban-10 on (10-03-2015)
;A rack is a part of a machine consisting of a bar with teeth into which those of a wheel or gear fit.[hinkhoj]
;दन्तुरदन्डक्  मशीन का भाग है  जिसमे  बार के साथ दाँत जो कि पहिये या गियर मे समाए है . [manual]
(defrule rack6
(declare (salience 5300))
(id-root ?id rack)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xanwuraxandak))		
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rack.clp 	rack6   "  ?id " xanwuraxandak )" crlf))
)

;@@@ Added by14anu-ban-10 on (10-03-2015)
;Ive been racking my brains trying to remember her name.[hinkhoj]
;मैरे दिमाग  ने उसका नाम याद  करने  का भरसक प्रयास करा .[manual]
(defrule rack7
(declare (salience 5400))
(id-root ?id rack)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 brain)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Barasaka_prayAsa_kara))		
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rack.clp 	rack7   "  ?id " Barasaka_prayAsa_kara)" crlf))
)

;------------------------ Default Rules ----------------------

;"rack","VT","1.honA"
;Severe pain racked her body. 
(defrule rack1
(declare (salience 4900))
(id-root ?id rack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rack.clp 	rack1   "  ?id "  ho )" crlf))
)

;"rack","N","1.rEka"
;She settled the books neatly on the rack. 
;She reached up && put her briefcase in the rack.
(defrule rack0
(declare (salience 5000))
(id-root ?id rack)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rEka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rack.clp 	rack0   "  ?id "  rEka )" crlf))
)

;"rack","N","1.rEka"
;She settled the books neatly on the rack. 
;She reached up && put her briefcase in the rack.
;--"2.purAne_samaya_me_yAwanA_xene_kA_yaMwra"
;Put the slave on the rack.  
;--"3.xanwuraxandak"
;A part of a machine consisting of a bar with teeth into which those of a wheel or gear fit.
;
;"rack","VT","1.honA"
;Severe pain racked her body. 
;--"2.Barasaka_prayAsa_karanA"
;I've been racking my brains trying to remember her name.
;

