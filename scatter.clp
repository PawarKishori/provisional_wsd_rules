;@@@ Added by 14anu-ban-01 on (23-03-2015)
;At the first gunshot, the crowd scattered.[oald]
;पहली ही गोली पर, भीड तितर बितर हो गयी.  (self)
(defrule scatter0
(declare (salience 0))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wiwara_biwara_ho))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  scatter.clp   scatter0   "  ?id "  wiwara_biwara_ho)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (23-03-2015)
;They scattered his ashes at sea.[oald]
;उन्होंने समुद्र पर उसकी राख बिखरा दी.  (self)
(defrule scatter1
(declare (salience 1000))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biKarA_xe))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  scatter.clp   scatter1   "  ?id "  biKarA_xe)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (24-03-2015)
;Scatter the grass seed over the lawn. [oald]
;लॉन पर घास के बीज फैला दीजिए .  (self)
(defrule scatter2
(declare (salience 1000))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-over_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA_xe))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  scatter.clp   scatter2   "  ?id "  PElA_xe)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (24-03-2015)
;If you like, scatter flowers on top.[coca]
;यदि आपको पसन्द हो, तो  ऊपरी सतह पर फूल बिखरा दीजिए .  (self)
(defrule scatter3
(declare (salience 1000))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biKarA_xe/PElA_xe))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  scatter.clp   scatter3   "  ?id "  biKarA_xe/PElA_xe)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (24-03-2015)
;Scatter parsley on top.[coca]
;सबसे ऊपर अजमोद छिडक दीजिए .   (self)
(defrule scatter4
(declare (salience 1000))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 parsely|oregano|olive|honey|water|oil)
(kriyA-object ?id ?id1)
(kriyA-on_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cidaka_xe))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  scatter.clp   scatter4  "  ?id "  Cidaka_xe)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (24-03-2015)
;Scatter butter on top and put the bowl in the freezer for 15 minutes.[coca]
;सबसे ऊपर  मक्खन लगा दीजिए और 15 मिनटों के लिए कटोरे को फ्रीजर में  रखिए .    (self)
(defrule scatter5
(declare (salience 1000))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 butter|margarine|spread|cheese)
(kriyA-object ?id ?id1)
(kriyA-on_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA_xe))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  scatter.clp   scatter5 "  ?id "  lagA_xe)" crlf))	
)

;@@@ Added by 14anu-ban-01 on (24-03-2015)
;He banged his fist on the table and the crayons got scattered over the floor.  [self: with reference to oald]--Parse no. 640
;उसने मेज पर जोर से अपनी मुठ्ठी  मारी और [सभी] क्रेयान फर्श पर छितरा गये .   (self)
(defrule scatter6
(declare (salience 1000))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 crayon)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str)	
(id-root =(- ?id 1) get)		
;(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE))) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) CiwarA_jA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "   scatter.clp   scatter6   "  ?id "  " (- ?id 1) "  CiwarA_jA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (24-03-2015)
;Scatter the lawn with grass seed.[oald]
;लॉन को घास के बीजों से आच्छादित कर दीजिए . [self]
(defrule scatter7
(declare (salience 1000))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(kriyA-with_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AcCAxiwa_kara_xe))	
(assert (kriyA_id-object_viBakwi ?id1 ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  scatter.clp   scatter7  "  ?id " ko)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  scatter.clp   scatter7   "  ?id "  AcCAxiwa_kara_xe)" crlf))	
)


;@@@ Added by 14anu-ban-01 on (24-03-2015)
;Scatter tomatoes around it.[coca]
;इसके चारों ओर टमाटर फैला दीजिए .  (self)
(defrule scatter8
(declare (salience 1000))
(id-root ?id scatter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-around_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA_xe/dAla_xe))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  scatter.clp   scatter8   "  ?id "  PElA_xe/dAla_xe)" crlf))	
)
