;@@@ Added by 14anu-ban-04 (11-03-2015)
;I didn't want to dampen his spirits.                [cald]
;मैंने उसके  उत्साह  को कम नहीं  करना  चाहा था .               [self]
(defrule dampen1
(declare (salience 20))
(id-root ?id dampen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 enthusiasm|spirit)    
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id kama_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " dampen.clp     dampen1   "  ?id " ko  )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dampen.clp 	dampen1   "  ?id "  kama_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (11-03-2015)
;Their spirits dampened after their introduction to the new teacher.          [damp.clp]
;उनके नये शिक्षक के  परिचय के बाद उनका उत्साह   कम  हुआ .                                       [self]
(defrule dampen2
(declare (salience 30))
(id-root ?id dampen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 enthusiasm|spirit)    
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dampen.clp 	dampen2    "  ?id "  kama_ho )" crlf))
)


;@@@ Added by 14anu-ban-04 (11-03-2015)
;Perspiration dampened her face and neck.                     [oald]
;पसीने ने उसके चेहरे और गरदन को गीला किया .                             [self]
(defrule dampen0
(declare (salience 10))
(id-root ?id dampen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id gIlA_kara/nama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " dampen.clp     dampen0   "  ?id " ko  )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dampen.clp 	dampen0   "  ?id "  gIlA_kara/nama_kara )" crlf))
)
