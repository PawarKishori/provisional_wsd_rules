;@@@Added by 14anu-ban-08 (23-02-2015)
;She was a legend in her own lifetime.  [oald]
;वह अपने समय की दिग्गज व्यक्ति थी. [self]
(defrule legend0
(declare (salience 0))
(id-root ?id legend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiggaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  legend.clp 	legend0   "  ?id "  xiggaja )" crlf))
)


;@@@Added by 14anu-ban-08 (23-02-2015)   ;Run on parser 207
;A Chinese legend narrates the tale of the victory of the emperor Huang-ti about four thousand years ago, which he owed to his craftsmen (whom nowadays you would call engineers).  [NCERT]
;एक चीनी आख्यान में, लगभग 4000 वर्ष पुरानी, सम्राट ह्वेंग-ती की विजय गाथा है, जिसमें उसको अपने शिल्पकारों (जिन्हें आज की भाषा में आप इंजीनियर कहते हैं) के कारण विजय प्राप्त हुई थी.  [NCERT]
(defrule legend1
(declare (salience 10))
(id-root ?id legend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 Chinese)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AKyAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  legend.clp 	legend1   "  ?id "  AKyAna)" crlf))
)

