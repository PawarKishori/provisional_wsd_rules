;$$$Modified by 14anu-ban-08 (17-02-2015)  ;removed relation, modify printout statement,added hindi translation
;Added by Meena(5.02.10)
;They are building a gigantic mall five miles from here .
;यहाँ से 5 मील पर बहुत बडा माल बना रहे हैं.   [self]    ;added translation
(defrule mile0
(declare (salience 5000))
(id-root ?id mile)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-saMKyA_viSeRaNa  ?id ?);'viSeRya-det_viSeRaNa' added by sheetal    ;remove 'viSeRya-det_viSeRaNa' relation by 14anu-ban-08 (17-02-2015)
;(saMjFA-from_viBakwi  ?id ?id1);Commented by Sukhada. This fact doesn't exist anymore.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mIla))  ;modify from 'mIla_xUra' to 'mIla' by 14anu-ban-08 (17-02-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jam.clp       jam0   "  ?id "  mIla_xUra )" crlf));Commented by Sukhada
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mile.clp       mile0 "  ?id "  mIla )" crlf))  ;modify from 'mIla_xUra' to 'mIla' by 14anu-ban-08 (17-02-2015)
)

;Remove by 14anu-ban-08 (17-02-2015) because it fires from mile1
;$$$Modified by 14anu-ban-08 (07-02-2015)        ;modify 'printout statement'
;@@@ Added by Nandini(8-1-14)
;The beach is a mile away. [via mail]
;samuxrawata mIla xUra hE.
;(defrule mile2
;(declare (salience 5050))
;(id-root ?id mile)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id ?id1)
;(id-word ?id1 a)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 eka_mIla))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " mile.clp	mile2  "  ?id "  " ?id1 "  eka_mIla)" crlf))     ;modified 'printout statement' by 14anu-ban-08 (07-02-2015) 
;)

;@@@Added by 14anu-ban-08 (07-02-2015)
;The bus deposited me miles from anywhere.    [oald]
;बस ने मुझे किसी जगह से कई मील दूर छोड़ दिया.   [self]  
(defrule mile3
(declare (salience 5051))
(id-root ?id mile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(not(viSeRya-saMKyA_viSeRaNa ?id ?))
(not (viSeRya-viSeRaka ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaI_mIla_xUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mile.clp       mile3 "  ?id "  kaI_mIla_xUra)" crlf))
)

;@@@Added by 14anu-ban-08 (07-02-2015)
;I'm not walking—it's miles away.    [oald]
;मैं चल नहीं सकता वह कई मील दूर है .    [self]
(defrule mile4
(declare (salience 5051))
(id-word ?id miles)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka ?id ?id1)
(id-root ?id1 away)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kaI_mIla_xUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " mile.clp	mile4  "  ?id "  " ?id1 "  kaI_mIla_xUra)" crlf))     
)

;===========Default Rules =====================
;@@@ Added by Nandini (22-11-13)
;Deserts extend to many miles.
;regiswAna bahuwa sAre mIloM ko baDawe hEM.
;I have miles to go.
;muJe mIloM jAna hE.
(defrule mile1
(declare (salience 50))
(id-root ?id mile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mile.clp       mile1 "  ?id "  mIla )" crlf))
)

