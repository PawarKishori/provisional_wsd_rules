;@@@ Added by 14anu-ban-04 (31-03-2015)
;A famous movie director.             [cald]      
;एक प्रसिद्ध फिल्म निर्देशक .                  [self]
(defrule director1
(declare (salience 20))
(id-root ?id director)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI ?id ?id1)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1))
(id-root ?id1 movie|play|film|tale|story)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirxeSaka)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  director.clp     director1  "  ?id "  nirxeSaka )" crlf))
)

;@@@ Added by 14anu-ban-04 (31-03-2015)
;The director’s brilliant conceit was to film this tale in black and white. [oald]
;निर्देशक  की प्रतिभाशाली सोच थी इस कहानी को ब्लैक एण्ड वाइट में  फिल्माना . [manual]
(defrule director2
(declare (salience 20))
(id-root ?id director)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id2)
(id-root ?id2 movie|play|film|tale|story)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirxeSaka)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  director.clp     director2  "  ?id "  nirxeSaka )" crlf))
)
;--------------------------------- DEFUALT RULE ---------------------------------------------

;@@@ Added by 14anu-ban-04 (31-03-2015)
;She has become the director of the new information centre.               [cald]
;वह नये सूचना केंद्र की निदेशक बनी है .                                            [self]
(defrule director0
(declare (salience 10))
(id-root ?id director)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nixeSaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  director.clp     director0  "  ?id "  nixeSaka )" crlf))
)


