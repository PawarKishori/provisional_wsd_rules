;@@@ Added by 14anu-ban-06 (30-03-2015)
;He gave an immaculate performance as the aging hero. (cambridge)
;उसने वयस्क वीर के जैसे त्रुटिहीन प्रदर्शन दिया . (manual)
(defrule immaculate1
(declare (salience 2000))
(id-root ?id immaculate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 performance|record)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wrutihIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immaculate.clp 	immaculate1   "  ?id "  wrutihIna )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (30-03-2015)
;An immaculate garden.(cambridge)
;साफ-सुथरा उद्यान .(manual)
;An immaculate room. (OALD)
;साफ-सुथरा कमरा . (manual)
(defrule immaculate0
(declare (salience 0))
(id-root ?id immaculate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAPa-suWarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immaculate.clp 	immaculate0   "  ?id "  sAPa-suWarA )" crlf))
)
