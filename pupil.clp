;##############################################################################
;#  Copyright (C) 2014-2015 Gurleen Bhakna (gurleensingh@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################


;@@@ Added by 14anu05 GURLEEN BHAKNA on 20.06.14
;She now teaches only private pupils.
;वह अब सिर्फ व्यक्तिगत विद्यार्थियों को सिखाती है .
(defrule pupil0
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyArWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil0   "  ?id "  vixyArWI )" crlf))
)


;@@@ Added by 14anu05 GURLEEN BHAKNA on 20.06.14
;Her pupils were dilated.
;उसके आंख की पुतली फैलाई गई थी.
(defrule pupil1
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 dilate|contract)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AMKa_kI_puwalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil1   "  ?id "  AMKa_kI_puwalI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-03-2015)
;Daniel is the star pupil at school.	[oald]
;डैन्यल विद्यालय का सर्वोतम विद्यार्थी है . 		[self]
(defrule pupil3
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-at_saMbanXI  ?id ?id1)
(id-root ?id1 school|School)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyArWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil3   "  ?id "  vixyArWI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-03-2015)
;The painting is believed to be by a pupil of Titian. 	[cald]
;कलाकृति टिशन के शागिर्द की मानी जाती है . 			[self]
(defrule pupil4
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 PropN)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAgirxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil4   "  ?id "  SAgirxa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-03-2015)
;The parents of secondary school pupils. 	 	[oald]
;माध्यमिक विद्यालय के विद्यार्थी के माँ बाप .  			[self]
(defrule pupil5
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 parent)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyArWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil5  "  ?id "  vixyArWI )" crlf))
)


;$$$ Modified by 14anu-ban-05 on (22-04-2015); added "average" to the list
;These complex formulae are beyond the grasp of the average pupil.	[OALD]
;ये जटिल सूत्र औसत  छात्रो की पकड के बाहर हैं .					[MANUAL] ;added 'average' by 14anu-ban-05 on (22-04-2015)
;@@@ Added by 14anu-ban-09 on (16-03-2015)
;Her school report described her as a very promising pupil.	[oald]	;added by 14anu-ban-09 on (07-04-2015)	 	
;उसके विद्यालय प्रतिवेदन ने एक अत्यन्त होनहार विद्यार्थी के जैसा उसको व्यक्त किया . 	[Manual]	;added by 14anu-ban-09 on (07-04-2015)	 	
;What measures can we take to deal with disruptive pupils? 	[oald]
;हम विघटनकारी विद्यार्थियों के साथ व्यवहार करने के लिए क्या उपाय अपना सकते हैं? 	[self]
(defrule pupil6
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 disruptive|promising|average)	;added 'promising' by 14anu-ban-09 on (07-04-2015) 	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyArWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil6  "  ?id "  vixyArWI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-04-2015)
;The brightest pupil in the class.	[oald]
;कक्षा में सबसे अधिक तेज विद्यार्थी .		[self] 

(defrule pupil7
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 class|school)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyArWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil7  "  ?id "  vixyArWI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-04-2015)
;We expect pupil numbers to increase next year. [oald]
;हम अगले वर्ष विद्यार्थी के अङ्को में वृद्धि होने की उम्मीद करते हैं . 	[Manual]

(defrule pupil8
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 number)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyArWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil8  "  ?id "  vixyArWI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-04-2015)
;A school with over 1,000 pupils. [http://www.thefreedictionary.com]
; विद्यालय में लगभग 1,000 विद्यार्थी हैं. 	[Manual]

(defrule pupil9
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyArWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil9  "  ?id "  vixyArWI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (07-04-2015)
;There is a very relaxed atmosphere between staff and pupils at the school.  [cald]
;विद्यालय में कर्मचारियों और विद्यार्थियों के बीच एक अत्यन्त तनाव मुक्त वातावरण है . 	[Manual]


(defrule pupil10
(declare (salience 5000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-between_saMbanXI  ?id1 ?id)
(viSeRya-at_saMbanXI  ?id1 ?id2)
(id-root ?id2 school|college)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixyArWI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil10  "  ?id "  vixyArWI )" crlf))
)



;--------------------------------------DEFAULT RULE---------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (16-03-2015)
;NOTE-Example sentence need to be added.

(defrule pupil2
(declare (salience 0000))
(id-root ?id pupil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id puwalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pupil.clp 	pupil2   "  ?id "  puwalI )" crlf))
)

;-----------------------------------------------------------------------------------------------------------------------------

