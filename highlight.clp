;@@@ Added by 14anu-ban-06 (25-03-2015)
;She’s had blonde highlights put into her hair.(OALD)
;उसने अपने केश में सुनहरी चमक दी थीं . (manual)
(defrule highlight1
(declare (salience 2000))
(id-root ?id highlight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-kqxanwa_viSeRaNa ?id ?id1)
(kriyA-into_saMbanXI ?id1 ?id2)
(id-root ?id2 hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  highlight.clp 	highlight1   "  ?id "  camaka )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (25-03-2015)
;Highlights of the match will be shown after the news. (OALD)
;मैच के मुख्य अंश समाचार के बाद दिखाए जाएँगे . (manual)
(defrule highlight0
(declare (salience 0))
(id-root ?id highlight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya_aMSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  highlight.clp 	highlight0   "  ?id "  muKya_aMSa )" crlf))
)


;@@@ Added by 14anu-ban-06 (25-03-2015)
;I've highlighted the important passages in yellow. (OALD)
;मैं पीले रङ्ग में महत्त्वपूर्ण पैसेज विशिष्ट रूप से दर्शा चुका हूँ .(manual) 
(defrule highlight2
(declare (salience 0))
(id-root ?id highlight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta_rUpa_se_xarSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  highlight.clp 	highlight2   "  ?id "  viSiRta_rUpa_se_xarSA )" crlf))
)

