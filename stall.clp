;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 26.06.14
;The thief broke into the office while his accomplice stalled off the security guard.[Cambridge:source added by 14anu-ban-01]
;चोर दफ्तर में जबरदस्ती घुस गया जब कि उसके सहअपराधी ने सुरक्षा पहरेदार का ध्यान बटाया.
(defrule stall3
(declare (salience 3500))
(id-root ?id stall)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 off)
(kriyA-object ?id ?);added by 14anu-ban-01 on (12-01-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XyAna_batA))
(assert (kriyA_id-object_viBakwi ?id kA));added by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  stall.clp     stall3   "  ?id "  " ?id1 "  XyAna_batA  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  stall.clp     stall3   "  ?id " kA )" crlf);added by 14anu-ban-01 on (12-01-2015)
)

;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 26.06.14
;She says she'll give me the money next week but I think she's just stalling.
;वह कहती है कि अगले सप्ताह मुझे पैसा देगी परन्तु मैं सोचता हूँ कि वह टाल रही है . 
(defrule stall2
(declare (salience 3500))
(id-root ?id stall)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tAla));changed "tAla_raha" to "tAla" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stall.clp 	stall2   "  ?id "  tAla )" crlf));changed "tAla_raha" to "tAla" by 14anu-ban-01 on (12-01-2015)
)

;Commented by 14anu-ban-01 on (12-01-2015) because required meaning is coming from shower2[shower.clp] for the compund 'shower stall'
;@@@ Added by 14anu05 GURLEEN BHAKNA on 26.06.14
;There was one bathroom with a shower stall in the corner.
;वहाँ एक स्नानघर था जिसके कोने में एक वर्षा स्टाल था . 
;(defrule stall4
;(declare (salience 4500))
;(id-root ?id stall)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 shower)
;(test (=(- ?id 1) ?id1))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id stAla))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stall.clp 	stall4   "  ?id "  stAla )" crlf))
;)


;### Salience decreased by 14anu05
;Eg : There was one bathroom with a shower [stall] in the corner.
;Anusaaraka : वहाँ एक स्नानघर था जिसके कोने में एक फुहारा [दुकान] था .
;Man : वहाँ एक स्नानघर था जिसके कोने में एक फुहारा [स्टाल] था .
(defrule stall0
(declare (salience 3000))
(id-root ?id stall)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xukAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stall.clp 	stall0   "  ?id "  xukAna )" crlf))
)

;"stall","N","1.xukAna"
;Ramu runs a vegetable stall.
;--"2.aSvaSAlA_ke_alaga_alaga_kamareM"
;His stable has different stalls for different hosrses.
;--"3.maMca_ke_samIpa_vAlI_kursiyAz"
;Stalls in a theatre are usually reserved for VIP's
;--"4.carcA_meM_bETane_kA_niSciwa_sWAna"
;There are specific stalls for christians in the church.
;

;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;Example added :Our car stalled at the main crossing.[stall.clp]
;हमारी कार मुख्य चौराहे पर बन्द हो गयी./हमारी कार का इन्जन मुख्य चौराहे पर बन्द हो गया.[self]
;Note-the counter example mentioned by 14anu05 below is handeled in stall2.So,Removed from here.
;$$$ Modified by 14anu05 GURLEEN BHAKNA on 26.06.14

(defrule stall1
(declare (salience 3900))
(id-root ?id stall)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1) ; added by  14anu05
(id-root ?id1 car|bike|train|motor|truck|vehicle);added  by 14anu-ban-01 on (12-01-2015)
(id-cat_coarse ?id verb)
;(id-root ?id1 ?str&:(and (not (numberp ?str))(not (gdbm_lookup_p "animate.gdbm" ?str)))) ;condition added by 14anu05 ;commented by 14anu-ban-01 on (12-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_ho_jA));changed "iMjana_kA_banxa_ho_jA" to "banxa_ho_jA" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stall.clp 	stall1   "  ?id "  banxa_ho_jA )" crlf));changed "iMjana_kA_banxa_ho_jA" to "banxa_ho_jA" by 14anu-ban-01 on (12-01-2015)
)

;"stall","V","1.iMjana_kA_banxa_ho_jAnA"
;Our car stalled at the main crossing.
;--"2.kArya_ko_bAXiwa_karanA"
;Less rainfall stalls the developing of crops.
;--"3.pragawi_ko_rokanA"
;Financial crises stalls the development of the countyry.
;
