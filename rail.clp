
(defrule rail0
(declare (salience 5000))
(id-root ?id rail)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id railing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kaTaGarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  rail.clp  	rail0   "  ?id "  kaTaGarA )" crlf))
)

;"railing","N","1.kaTaGarA"
;M.C.H. has put the beautiful park railings for the safety of the children.
;
(defrule rail1
(declare (salience 4900))
(id-root ?id rail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rail.clp 	rail1   "  ?id "  rela )" crlf))
)

;"rail","N","1.rela"
;--"2.trena"
;Send your son by rail.
;What is the rail fare to Bombay.
;--"3.patarI"
;Hold on to the rail at the edge of the water fall.
;She has a beautiful curtain rail.  
;
(defrule rail2
(declare (salience 4800))
(id-root ?id rail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rela_para_saPZara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rail.clp 	rail2   "  ?id "  rela_para_saPZara_kara )" crlf))
)

;;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 05.07.2014 email-id:sahni.gourav0123@gmail.com
;She railed against the injustice of it all.
;उन्होंने अन्याय के खिलाफ आवाज उठाई.
(defrule rail3
(declare (salience 4800))
(id-root ?id rail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-against_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvAja_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rail.clp 	rail3   "  ?id "  AvAja_uTA )" crlf))
)

;;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 05.07.2014 email-id:sahni.gourav0123@gmail.com
;The machine was railed off as a safety precaution. 
;2 मशीन एक सुरक्षा पूर्वोपाय  के लिए बन्द की गयी थी . 
(defrule rail4
(declare (salience 4800))
(id-root ?id rail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 off)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rail.clp 	rail4   "  ?id "  banxa_kara )" crlf))
)



;"rail","VTI","1.rela_para_saPZara_karanA"
