;@@@ Added by 14anu-ban-06 (04-04-2015)
;There were hidden microphones in the room to record their conversation.(cambridge)[parser no.- 2]
;उनका वार्तालाप दर्ज करने के लिए कमरे में माइक छिपाए हुए  थे . (manual)
(defrule hidden1
(declare (salience 2000))
(id-root ?id hidden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 microphone|camera)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CipAyA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hidden.clp 	hidden1   "  ?id "  CipAyA_huA )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-04-2015)
;Hidden taxes. (cambridge)[parser no.- 11]
;अप्रत्यक्ष कर . (manual)
(defrule hidden2
(declare (salience 2200))
(id-root ?id hidden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 cost|tax)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aprawyakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hidden.clp 	hidden2   "  ?id "  aprawyakRa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (04-04-2015)
;A hidden valley. (cambridge)
;एक छिपी हुई घाटी . (manual)
(defrule hidden0
(declare (salience 0))
(id-root ?id hidden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CipA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hidden.clp 	hidden0   "  ?id "  CipA_huA )" crlf))
)

