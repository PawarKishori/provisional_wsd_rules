;$$$Modified by 14anu-ban-02(26-02-2016)
;###[COUNTER STATEMENT] The parents documented every step of their child's development .[sd_verified]
;###[COUNTER STATEMENT]mAz bApa ne unake bacce ke vikAsa kA prawyeka caraNa liKA.[sd_verified]
;@@@ Added by 14anu-ban-04 on (27-03-2015)
;Can you document the claims you're making?                  [oald]
;क्या आप उन दावों का लिखित प्रमाण दे सकते हैं जिन्हें आप  लगा रहे हैं?          [self]  
(defrule document2 
(declare (salience 30))
(id-root ?id document)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 claim)	;added by 14anu-ban-02(26-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKiwa_pramANa_xe))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  document.clp     document2  "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  document.clp 	document2   "  ?id "  liKawa_pramANa_xe )" crlf))
)

;@@@ Added by 14anu-ban-04 on (27-03-2015)
;The results are documented in Chapter 3.          [oald]
;परिणाम चैप्टर 3 में प्रस्तत किए जाते हैं .                       [self]
(defrule document3 
(declare (salience 20))
(id-root ?id document)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 result)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  document.clp    document3   "  ?id "  praswawa_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 on (27-03-2015)
;His exploits have been well documented by the national press.             [oald]
;राष्ट्रीय प्रैस के द्वारा उसके कारनामों का  अच्छा प्रमाण प्रस्तुत किया गया है .                       [self]
(defrule document4
(declare (salience 20))
(id-root ?id document)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1  exploit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramANa_praswuwa_kara))   
(if ?*debug_flag* then
(assert (kriyA_id-subject_viBakwi ?id kA))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  document.clp     document4  "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  document.clp    document4  "  ?id " pramANa_praswuwa_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;default_sense && category=noun	leKya_pawra	0
;"document","N","1.leKya_pawra/xaswAvejZa"
(defrule document0
(declare (salience 5000))
(id-root ?id document)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaswAveja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  document.clp 	document0   "  ?id "  xaswAveja )" crlf))
)

;"document","VT","1.liKanA"
;The parents documented every step of their child's development
(defrule document1
(declare (salience 10))                ;salience from '4900' to '10' reduced by 14anu-ban-04 on (27-03-2015)
(id-root ?id document)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  document.clp 	document1   "  ?id "  liKa )" crlf))
)

;"document","VT","1.liKanA"
;The parents documented every step of their child's development
;--"2.liKiwa_pramANa_xenA/pramANa_pawra_xenA"
;Can you document your claims?
;
