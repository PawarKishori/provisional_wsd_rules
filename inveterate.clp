
(defrule inveterate0
(declare (salience 5000))
(id-root ?id inveterate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inveterate.clp 	inveterate0   "  ?id "  pakke )" crlf))
)

;$$$ Modified by 14anu-ban-06 (10-09-2014)
;I was an inveterate reader.(COCA)
;मैं एक अभ्यस्त पाठक था
;And my father was an inveterate numbers player.(COCA)
;और मेरा पिता अभ्यस्त खिलाडी थे. 
(defrule inveterate1
(declare (salience 5050)) 
(id-root ?id inveterate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id);added by 14anu-ban-06
(id-root ?id1 player|talker|walker|reader);added by 14anu-ban-06
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aByaswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inveterate.clp 	inveterate1   "  ?id "  aByaswa )" crlf))
)

;"inveterate","Adj","1.aByaswa"
;She is an inveterate musician.
;--"2.niranwara[burI_AxaweM]"
;His inveterated behaviour brought his downfall.
;
;
;@@@ Added 14anu-ban-06 (10-09-2014)
;Somanath would cure persons of every inveterate illness and heal every desperate and incurable disease .
;(parallel corpus)
;उनका विश़्वास था कि सोमनाथ का लिंग प्रत़्येक पुराने रोग के व्यक्तियों को स्वस्थ करेगा  और हर प्रकार के असाध़्य रोग का उपचार करेगा .
(defrule inveterate2
(declare (salience 5100))
(id-root ?id inveterate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 illness|disease)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id purAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inveterate.clp 	inveterate2   "  ?id "  purAnA )" crlf))
)

;@@@ Added 14anu-ban-06 (10-09-2014)
;Their scattered remains cherish , of course , the most inveterate aversion towards all Muslims .(parallel corpus)
;उनके बिखरे हुए अवशेष आज तक मुसलमानों के प्रति घोर घृणा का भाव रखते हैं .
(defrule inveterate3
(declare (salience 5200))
(id-root ?id inveterate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 aversion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inveterate.clp 	inveterate3   "  ?id "  Gora )" crlf))
)

