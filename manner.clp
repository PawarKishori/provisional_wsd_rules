;$$$ Modified by Nandini(21-11-2013)
;Added by sheetal(6-10-09)
;It is not a good manner to eat alone.
;akelA KAnA acCA AcaraNa nahIM hE.
;His manner was polite but cool.[oxford advanced lerner dictionary]
;usakA AcaraNa namra paranwu SIwala WA.
(defrule manner0
;(declare (salience 5000))
(declare (salience 160))		;salience changed from 100 to 160 by 14anu-ban-02(08-02-2016)
(id-root ?id manner)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject ? ?id)(saMjFA-to_kqxanwa  ?id ?))
(id-cat_coarse ?id noun)
;(id-word =(- ?id 1) good)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AcaraNa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manner.clp       manner0   "  ?id "  AcaraNa )" crlf))
)

;$$$Modified by 14anu-ban-08 (27-03-2015)   ;changed meaning
;@@@ Added by Nandini (21-11-2013)
;He has no manners.[oxford advanced lerner dictionary]
;use wamIjaz nahim hE.
(defrule manner2
(declare (salience 150))      ;salience increased by 14anu-ban-08 (27-03-2015)
(id-root ?id manner)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wamIja))  ;changed meaning from 'wamIjaz' to 'wamIja' by 14anu-ban-08 (27-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manner.clp       manner2   "  ?id "  wamIja)" crlf))  ;changed meaning from 'wamIjaz' to 'wamIja' by 14anu-ban-08 (27-03-2015)
)

;$$$Modified by 14anu-ban-08 (30-03-2015)   ;commented relation, added relation, changed meaning
;@@@ Added by Nandini (21-11-2013)
;She could at least have the good manners to let me know she won't be able to attend.[oxford advanced lerner dictionary]
;usame iwanA wo SiStAcAra honA cAhiye ki vaha muJe bawAye ki vaha  A nahiM sakawI. 
;उसमें इतने शिष्टाचार होने चाहिए कि वह मुझे बताए कि वह आ नहीं सकती.  [self]  ;modified by 14anu-ban-08 (30-03-2015)
(defrule manner3
(declare (salience 151))  ;salience increased by 14anu-ban-08 (30-03-2015)
(id-root ?id manner)
?mng <-(meaning_to_be_decided ?id)
;(or(kriyA-anaBihiwa_subject  ? ?id)(kriyA-prayojya_karwA  ? ?id))  ;commented by 14anu-ban-08 (30-03-2015)
(viSeRya-viSeRaNa ?id ?id1)       ;added by 14anu-ban-08 (30-03-2015)
(id-root ?id1 good|bad)          ;added by 14anu-ban-08 (30-03-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiRtAcAra))  ;changed meaning from 'SiStAcAra' to 'SiRtAcAra'by 14anu-ban-08 (30-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manner.clp       manner3   "  ?id "  SiRtAcAra)" crlf))    ;changed meaning from 'SiStAcAra' to 'SiRtAcAra'by 14anu-ban-08 (30-03-2015)
)

;@@@Added by 14anu-ban-08 (11-02-2015)
;Her daughter is endowed with charming manners. [hinkhoj]
;उसकी बेटी मोहक अंदाज से संपन्न है  . [self]
(defrule manner4
(declare (salience 100))
(id-root ?id manner)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 charming)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMxAjZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manner.clp       manner4   "  ?id "  aMxAjZa)" crlf))
)

;===============default-rule==================

;@@@ Added by Nandini (21-11-2013)
;She answered in a businesslike manner.[oxford advanced lerner dictionary]
;usane vyAvahArika warIke se uwwara xiyA.
(defrule manner1
(declare (salience 50))
(id-root ?id manner)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warIkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  manner.clp       manner1   "  ?id "  warIkA )" crlf))
)
