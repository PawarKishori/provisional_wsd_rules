;@@@ Added by 14anu-ban-07 (13-10-2014)
;Let us know if you're unable to come.(oald)
;आप आने में असमर्थ हैं, तो हमें बताएं.(manual)
(defrule unable0
(id-root ?id unable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asamarWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unable.clp     unable0  "  ?id" asamarWa )" crlf)
)
)

;@@@ Added by 14anu-ban-07 (13-10-2014)
;Occasionally, however, the existing theory is simply unable to explain new observations.(ncert corpus)
;तथापि, यदा-कदा सुप्रचलित सिद्धान्त नए प्रेक्षणों का स्पष्टीकरण करने में असमर्थ होते हैं.(ncert corpus)
(defrule unable1
(declare (salience 1000))
(id-root ?id unable)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) to)
(to-infinitive  =(+ ?id 1) ?id1)
(id-cat_coarse ?id adjective)
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asamarWa))
(assert  (id-wsd_viBakwi   ?id1  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unable.clp     unable1  "  ?id" asamarWa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  unable.clp      unable1   "  ?id1 " meM )" crlf)
)
)
