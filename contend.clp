;@@@ Added by 14anu-ban-03 (22-04-2015)
;She is contending for the prize. [same clp]
;वह पुरस्कार के लिए प्रतियोगिता में भाग ले रही है . [manual]
(defrule contend2
(declare (salience 4900))
(id-root ?id contend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiyogiwA_meM_BAga_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contend.clp 	contend2   "  ?id "  prawiyogiwA_meM_BAga_le )" crlf))
)

;@@@ Added by 14anu-ban-03 (22-04-2015)
;Three armed groups were contending for power. [oald]
;तीन शस्त्रधारी समूह शक्ति के लिए सङ्घर्ष कर रहे थे . [manual]
(defrule contend3
(declare (salience 4900))
(id-root ?id contend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?id1)
(id-root ?id1 power)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMGarRa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contend.clp 	contend3   "  ?id "  saMGarRa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (22-04-2015)
;I don't contend to be an expert. [oald]
;मैं दावे के साथ नहीं कहता कि मैं एक निपुण  हूँ .  [manual]
(defrule contend4
(declare (salience 4900))
(id-root ?id contend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA ?id ?id1)
(id-root ?id1 expert)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAve_ke_sAWa_kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contend.clp 	contend4  "  ?id "  xAve_ke_sAWa_kaha )" crlf))
)


;$$$ Modified by 14anu-ban-03 (22-04-2015)
;Soldiers had to contend with heavy rain during the war. [same clp]   ;working on parser no.- 13
;sipAhiyoM ko yuxXa ke xOrAna BArI varRA kA sAmanA karanA padZA. [same clp]
(defrule contend0
(declare (salience 5000))
(id-root ?id contend)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 with)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))  ;added by 14anu-ban-03 (22-04-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sAmanA_kara))  ;meaning changed from 'saMGarRa_kara' to 'sAmanA_kara' by 14anu-ban-03 (22-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  contend.clp	contend0  "  ?id " kA  )" crlf)  ;added by 14anu-ban-03 (22-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " contend.clp	contend0  "  ?id "  " ?id1 "  sAmanA_kara  )" crlf))
)

;-----------------------------------------Default rule----------------------------------------------

(defrule contend1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (22-04-2015)
(id-root ?id contend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivAxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contend.clp 	contend1   "  ?id "  vivAxa_kara )" crlf))
)

;"contend","VT","1.vivAxa_karanA"
;He contended that Communism had no future.
;--"2.prawisparXA_meM_honA"  
;She is contending for the prize.
;
;
