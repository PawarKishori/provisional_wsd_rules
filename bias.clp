;@@@ Added by 14anu-ban-02 (04-10-2014)
;Sentence: He should be allowed to complete his term of five years so that he can work without any bias . [karan singla]
;Translation:  उसे सामान्यत अपने पाँच वर्ष का कार्यकाल पूरा करने दिया जाये ताकि वह पक्षपात के बिना काम कर सके.[karan singla]
;Sentence:You have to separate fact from bias , right .[karan singla]
;Translation: आपको पक्षपात से तथ्य को अलग करना होगा , हैं ना.[karan singla]
(defrule bias1 
(declare (salience 100)) 
(id-root ?id bias) 
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-without_saMbanXI  ? ?id)(kriyA-from_saMbanXI  ? ?id)) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pakRapAwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bias.clp  bias1  "  ?id "  pakRapAwa )" crlf)) 
) 


;@@@ Added by 14anu-ban-02 (04-10-2014)
;Sentence:Usman Bin Affan was accused of biased appointment of officers.[karan singla]
;Translation: उसमान बिन अफ्फान पर उनके विरोधियों ने ये आरोप लगाने शुरु किये कि वो पक्षपातपूर्ण नियुक्तियाँ करते हैं [karan singla]
(defrule bias3 
(declare (salience 100)) 
(id-root ?id bias) 
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 appointment)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pakRapAwapUrNa )) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bias.clp  bias3  "  ?id "  pakRapAwapUrNa  )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-02 (04-10-2014)
;Sentence: Systematic errors can be minimized by improving experimental techniques, selecting better instruments and removing personal bias as far as possible. [ncert]
;Translation: सुधरी हुई प्रायोगिकी तकनीकों के उपयोग, प्रयोग के लिए अपेक्षाकृत अच्छे मापन यन्त्रों का चयन एवं यथासम्भव व्यक्तिगत पूर्वाग्रहों को दूर करके क्रमबद्ध त्रुटियों क] कम किया जा सकता है.[ncertt
(defrule bias0
(declare (salience -1))
(id-root ?id bias)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pUrvAgrahoM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bias.clp  bias0  "  ?id " pUrvAgrahoM )" crlf))
)

;@@@ Added by 14anu-ban-02 (04-10-2014)
;Sentence:we're all biased.[karan singla]
;Translation: हम सभी पक्षपाती हैं.[karan singla]
(defrule bias2
(declare (salience -1))
(id-root ?id bias)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective);parser is treating it as verb 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRapAwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bias.clp  bias2  "  ?id "  pakRapAwI )" crlf))
)

