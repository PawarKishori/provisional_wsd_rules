;#############################################################################
;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;######################################################################


;@@@ Added by jagriti(4.1.2013)
;The country fell under the sway of powerful invaders. [iit-bombay]
;देश शक्तिशाली आक्रमणकारियों के शासन के अधीन हो गया.
;Her parents no longer seem to have much sway over her.[cambridge dict]
;लगता है उसके माता पिता का अब उस पर ज्यादा शासन नहीं है.	;translation should be "लगता है उसके माता पिता का अब उस पर ज्यादा नियंत्रण नहीं है."
(defrule sway0
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-under_saMbanXI ? ?id)(viSeRya-over_saMbanXI ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAsana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway0   "  ?id "  SAsana)" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-03-2016)
;He still felt her sway over him.	[COCA]
;उसने अब भी उसपर उसका प्रभाव महसूस किया . 	[self]
(defrule sway0_1
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-over_saMbanXI  ?id1 ?id2)
(kriyA-object  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway0_1  "  ?id "  praBAva)" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-03-2016)
;Her parents no longer seem to have much sway over her. [cambridge dict]
;लगता है उसके माता पिता का अब उस पर ज्यादा नियंत्रण नहीं है.	[self]
(defrule sway0_2
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 parent)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyanwraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway0_2  "  ?id "  niyanwraNa)" crlf))
)


;@@@ Added by jagriti(4.1.2013)
;Her speech failed to sway her colleagues into supporting the plan.
;अपने भाषण की योजना के समर्थन में उनके सहयोगियों को प्रभावित करने में विफल रहा है.
;A speech that swayed many voters. 
; एक भाषण ने कई मतदाताओं को प्रभावित किया .
(defrule sway1
(declare (salience 4900))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAviwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway1   "  ?id "  praBAviwa_kara)" crlf))
) 

;$$$ Modified by 14anu-ban-01 on (06-03-2016)
;Don’t allow yourself to be swayed by emotion.	[OALD]
;खुदको भावना से प्रभावित मत होने दीजिए . 	[self]
;The panel was not swayed by his arguments.	[OALD]
; दल/दिल्ला उसकी बहस से प्रभावित नहीं हुआ था . 	[self]
;@@@ Added by jagriti(14.2.2014)
;Don't be swayed by false promises.[rajpal]
;झूठे वादों से मत विचलित हो . 
(defrule sway2
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAviwa_ho/vicaliwa_ho))	;added meaning "praBAviwa_ho" by 14anu-ban-01
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway2   "  ?id "  praBAviwa_ho/vicaliwa_ho)" crlf))	;added meaning "praBAviwa_ho" by 14anu-ban-01
)

;$$$ Modified by 14anu-ban-01 on (06-03-2016)
;Couples were swaying to the rhythm of the music.	[OALD]
;युगल सङ्गीत की ताल पर झूम रहे थे . 	[self]
;@@@ Added by jagriti(14.2.2014)
;Dancers are swaying.[rajpal]
;नर्तक झूम रहे हैं . 
(defrule sway3
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 dancer|couple)	;added 'couple' in the list
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway3   "  ?id "  JUma)" crlf))
) 

;@@@ Added by 14anu-ban-01 on (06-03-2016)
;The sway of the yacht was making her feel sick. 	[OALD]
;नौका का हिलाव उसे अस्वस्थ/बीमार_सा महसूस करवा रहा था . 	[self: ]
;नौका के हिलाव से उसका जी मिचला रहा था . 	[self: closer]
(defrule sway6
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 yacht|boat|ship)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway6   "  ?id "  hilAva)" crlf))
) 

;@@@ Added by 14anu-ban-01 on (06-03-2016)
;Vicky swayed and fell.[oald]
;विक्की डोला और गिर गया.	[translation improved by 14anu-ban-01]
(defrule sway7
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 and)
(id-root ?id2 fall)
(conjunction-components  ?id1 ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway7   "  ?id "  dola)" crlf))
) 

;@@@ Added by 14anu-ban-01 on (06-03-2016)
;The flowers were gently swaying in the breeze.[oald]
;फूल मन्द हवा में हल्के से लहरा रहे थे.[self]
(defrule sway8
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 flower|curtain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway8   "  ?id "  laharA)" crlf))
) 

;@@@ Added by 14anu-ban-01 on (06-03-2016)
;The stage swayed alarmingly under their weight.	[oald]
;मञ्च उनके वजन से खतरनाक ढङ्ग से झुका . [self]
(defrule sway9
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 stage|roof|bridge)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Juka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway9   "  ?id "  Juka)" crlf))
) 

;@@@ Added by 14anu-ban-01 on (06-03-2016)
;His previous experience of playing against New Zealand probably swayed the decision to select him.	[oald]
;सम्भवतः न्यूजीलैंड के विरुद्ध खेल के उसके पिछले अनुभव ने उसको चुनने के निर्णय को प्रभावित किया .  [self]
(defrule sway10
(declare (salience 5000))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 decision|opinion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAviwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi  " ?*prov_dir* "  sway.clp 	sway10   " ?id " ko )")
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway10   "  ?id "  praBAviwa_kara)" crlf))
) 


;....default rule..... 
;$$$ Modified by 14anu-ban-01 on (06-03-2016)	;corrected meaning "lacak" to "lacaka"
;@@@ Added by jagriti(4.1.2013)
;Walk with the sway of hips.
;कूल्हों को लचकाकर  चलो. 
(defrule sway4
(declare (salience 1))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lacaka))	;corrected meaning "lacak" to "lacaka"
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway4   "  ?id "  lacaka)" crlf))	;corrected meaning "lacak" to "lacaka"
) 

;$$$ Modified by 14anu-ban-01 on (06-03-2016)	:corrected meaning from "hilA" to "hila"
;@@@ Added by jagriti(4.1.2013)
;Vicky swayed and fell.[oald]
;विक्की हिला और गिर गया. 	;translation should be "विक्की डोला और गिर गया."
;The branches were swaying in the wind.[oald]
;शाखाएँ हवा में हिल रही थी.
(defrule sway5
(declare (salience 1))
(id-root ?id sway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hila))	;corrected meaning from "hilA" to "hila"
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sway.clp 	sway5   "  ?id "  hila)" crlf))	;corrected meaning from "hilA" to "hila"
) 

