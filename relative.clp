;@@@ Added by Anita-- 3.7.2014
;In the presence of the councillor and the police, Soni Devi took her daughter from her relative's place. [news-dev]
;सभासद व पुलिस की उपस्थिति में सोनी देवी अपने रिश्तेदार के यहां से बच्ची को अपने साथ ले गए।
;Her mother said that they sent their daughter to her relative, Sushil Vishwakarma and his wife Anjali. [news-dev]
; मां का कहना है कि उन्होने पुत्री को रिश्तेदार के यहां भेजा था, सुशील विश्वकर्मा व उनकी पत्नी अंजली विश्वकर्मा । 
;The relative of Sushil Vishwakarma, Soni Devi's 14 years old daughter, came from Giridiha to his home.;[news-dev];add sentence-7.7.2014
;सुशील विश्वकर्मा के घर गिरिडीह से उनके रिश्तेदार सोनी देवी की चौदह वर्षीय बेटी आई थी।
(defrule relative2
(declare (salience 4950))
(id-root ?id relative)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-RaRTI_viSeRaNa  ? ?id)(kriyA-to_saMbanXI  ?kri ?id)(subject-subject_samAnAXikaraNa  ? ?id)(viSeRya-of_saMbanXI  ?id ?sam))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id riSwexAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relative.clp 	relative2   "  ?id "  riSwexAra )" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-08-2014)
;Relative to the bus, therefore, we are thrown backward.  
;isIlie, basa ke ApekRa hama pICe kI ora Pefka xie jAwe hEM.
(defrule relative3
(declare (salience 5000))
(id-root ?id relative)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ApekRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relative.clp 	relative3   "  ?id " ApekRa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (08-12-2014)
;One of the earliest childhood memories of the famous physicist Albert Einstein was that of a magnet gifted to him by a relative.[ncert corpus]
;prasixXa BOwika vijFAnI albarta AiMsatIna ke awi prAramBika bacapana kI smqwiyoM meM se eka usa cumbaka se judI WI, jo unheM unake eka sambanXI ne BeMta kiyA WA.[ncert corpus]
(defrule relative4
(declare (salience 5100))
(id-root ?id relative)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambanXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relative.clp 	relative4   "  ?id " sambanXI)" crlf))
)

;-------------------- Default rules -------------------
(defrule relative0
(declare (salience 5000))
(id-root ?id relative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMbaMXiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relative.clp 	relative0   "  ?id "  saMbaMXiwa )" crlf))
)

;"relative","Adj","1.saMbaMXiwa"
;The file relative to the case is missing.   
;--"2.wulanAwmaka"
;The relative advantages of gas && electricity are different.
;--"3.sambanXa_vAcaka_kriyA-viReSaNa"
;This is the place where we met.  
;--"4.sambanXa_vAcaka_sarvanAma"
;The boy whom I met.' whom'is a relative pronoun.
;
(defrule relative1
(declare (salience 4900))
(id-root ?id relative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relative.clp 	relative1   "  ?id "  sagA )" crlf))
)

;"relative","N","1.sagA"
;She is my close relative.
