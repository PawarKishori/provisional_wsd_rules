;@@@ Added by 14anu-ban-10 on (04-03-2015)
;To plant reed beds.[oald]
;बेंत का पौधा लगाना.[manual]
(defrule reed1
(declare (salience 200))
(id-root ?id reed)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?  ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bewa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  reed.clp  	reed1   "  ?id "  bewa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (04-03-2015)
;The reed is used for thatching.[hinkhoj]
;नरकट को छाजन के लिए प्रयोग किया जाता है . [manual]
(defrule reed2
(declare (salience 300))
(id-root ?id reed)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma   ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id narakata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  reed.clp  	reed2   "  ?id "  narakata)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (04-03-2015)
;The clarinetist fitted a new reed onto his mouthpiece.[hinkhoj]
;बाजे का वह हिस्सा जिसमें मुहं लगाया जाए उसमे क्लेरनेटिस्ट ने एक नयी  बाँसुरी  फ़िट कियी. [manual]
(defrule reed0
(declare (salience 100))
(id-root ?id reed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzsurI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  reed.clp      reed0   "  ?id "   bAzsurI)" crlf))
)

