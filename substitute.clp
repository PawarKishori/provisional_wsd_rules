;;Example:-You can substitute oil for butter in this recipe.

(defrule substitute1
(declare (salience 0));salience reduced to 0 from 5000 by 14anu-ban-01 on (15-11-2014)
(id-root ?id substitute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  substitute.clp     substitute1   "  ?id " prayoga_kara)" crlf))
)


;@@@ Added by 14anu-ban-01 on (17-11-2014)
;The local bus service was a poor substitute for their car.[oald]
; स्थानीय बस सेवा उनकी कार के बदले में/का एक खराब विकल्प था.[self]
;These substitutes are readily available in most supermarkets.[agriculture corpus]
;यह विकल्प बहुत से सुपर बाजारों में इच्छापूर्वक/सरलता से उपलब्ध हैं.[manual]
(defrule substitute2
(declare (salience 0))
(id-root ?id substitute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikalpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  substitute.clp     substitute2   "  ?id " vikalpa))" crlf))
)


;@@@ Added by 14anu-ban-01 on (17-11-2014)
;Ram was the only substitute for his dead friend.[self:with reference to oald]
;राम उसके मृत मित्र के लिए एकमात्र स्थानापन्न व्यक्ति था . [self]
;NOTE:I could not find a better meaning for 'substitute' in this case,so it can be improved later.
(defrule substitute3
(declare (salience 5000))
(id-root ?id substitute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 friend|brother|sister|child|boy|girl|man|woman|uncle|aunt|father|mother);list is long
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAnApanna_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  substitute.clp     substitute3   "  ?id " sWAnApanna_vyakwi))" crlf))
)


;@@@ Added by 14anu-ban-01 on (17-11-2014)
;He was brought on as (a) substitute after half-time.[oald]
;वह मध्यान्तर के बाद प्रतिनिधि के रूप में लाया गया था . [self]
(defrule substitute4
(declare (salience 5000))
(id-root ?id substitute)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ? ?sub)
(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiniXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  substitute.clp     substitute4   "  ?id " prawiniXi))" crlf))
)

;@@@ Added by 14anu-ban-01 on (17-11-2014)
;Barley can be a good rice substitute.[self:with reference to oald]
; जौ चावल का एक अच्छा विकल्प हो सकता है.[self]
;For example, soybeans are the primary ingredient in many dairy product substitutes (e.g., soy milk, margarine, soy ice cream, soy yogurt, soy cheese, and soy cream cheese) and meat alternatives (e.g. veggie burgers).[agriculture corpus]
;उदाहरण के लिए, सोयाबीन अनेक दैनिक उत्पाद के विकल्प की मुख्य सामग्री है (उदाहरण, सोया दूध, सोया का दही, कृत्रिम मक्खन, सोया पनीर, और सोया मलाई पनीर) और माँस के विकल्प (उदाहरण, वेजी बर्गर).
(defrule substitute5
(declare (salience 0))
(id-root ?id substitute)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikalpa))
(assert (id-wsd_viBakwi ?id1 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  substitute.clp     substitute5  "  ?id1 " kA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  substitute.clp     substitute5   "  ?id " vikalpa))" crlf))
)

