;@@@ Added by 14anu-ban-08 (19-11-2014)
;Nobody can prevent him from taking part in the competition.     [Hinkhoj]
;उसे प्रतियोगिता में भाग लेने से कोई नहीं रोक सकता .       [Self]
(defrule nobody0
(declare (salience 0)) 
(id-root ?id nobody) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id koI_nahIM)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  nobody.clp  nobody0  "  ?id "  koI_nahIM )" crlf)) 
) 


;@@@ Added by 14anu-ban-08 (19-11-2014)
;Back in the 1960s, nobody anticipated the coming of budget airlines, when airplanes would be required to make five or six flights a day.[Given By Soma Mam] 
;पहले 1960 के दशक में, जब हवाई जहाज की एक दिन में पांच या छह उड़ानों की आवश्यकता होने वाली थी, तब किसी ने आने वाले बजट एयरलाइन का पूर्वानुमान नहीं किया था.   [Self]  
(defrule nobody1
(declare (salience 100)) 
(id-root ?id nobody) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject ?id1 ?id)
(id-root ?id1 anticipate)
(id-cat_coarse ?id noun)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kisI_ne_nahIM)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  nobody.clp  nobody1  "  ?id "  kisI_ne_nahIM )" crlf)) 
) 

