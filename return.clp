;$$$ Modified by 14anu-ban-04 (22-04-2015)
;###[COUNTER EXAMPLE ### After an exile of five years,he returned to India.         [EXILE.CLP]
;###[COUNTER EXAMPLE ### पाँच वर्षों के  निर्वासन  के बाद, वह भारत में लौटा .                          [self]
;$$$ Modified by Anita--20.2.2014
;We must return them to their rightful owner, How can you do that? [by mail]
;हमें उनके असली मालिक को ये मोती लौटा देना चाहिए,आप ऐसा कैसे कर सकते हैं?
(defrule return0
(declare (salience 5000))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?);Commented by Anita         ;uncommented by 14anu-ban-04 on (22-04-2015)
(kriyA-to_saMbanXI  ?id ?) ; Added by Anita
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lOtA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return0   "  ?id "  lOtA )" crlf))
)

(defrule return1
(declare (salience 4000)) ;salience reduced by Bhagyashri Kulkarni (8.11.2016)
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lOta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return1   "  ?id "  lOta )" crlf))
)

(defrule return2
(declare (salience 4800))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vApasI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return2   "  ?id "  vApasI )" crlf))
)

;"return","N","1.vApasI"
;Her return journey was memorable.
;--"2.AnA-jAnA"
;He bought the ticket a weekend return.
;--"3.prawyaparNa"
;The deposit is refunded on return of the vehicle.
;Her return of service in T.T.was very accurate
;--"4.punarAvqwwi"
;Many happy returns of the day.
;--"5.lABa{pUzjI_yA_lAgawa_para}"
;Have you submitted your returns of income for the year? 
;--"6.binA_bikA_yA_bikane_yogya_sAmAna_kI_vApasI"
;Merchandise returned to a retailer by a consumer.
;--"7.lOtawI_dAka_se"
;Kindly reply by return of post.
;
(defrule return3
(declare (salience 4700))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lOtA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return3   "  ?id "  lOtA )" crlf))
)

(defrule return4
(declare (salience 4600))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lOta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return4   "  ?id "  lOta )" crlf))
)

;"return","V","1.lOtanA[lOtAnA]"
(defrule return5
(declare (salience 4500))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lOta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return5   "  ?id "  lOta )" crlf))
)

;"return","VI","1.lOtanA"
;She returned home from an excursion trip.
;--"2.purAnI_sWiwi_meM_lOta_jAnA"
;He has returned to his old bad habits after his wife's death.
;
(defrule return6
(declare (salience 4400))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vApisa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return6   "  ?id "  vApisa_kara )" crlf))
)

;@@@ Added by Anita--9.7.2014
;Even after obtaining Canadian citizenship, thousands of people from the Punjab have not declared ;their income from properties and pension benefits, etc. in the Punjab, in their Canadian income tax returns.
;पंजाब से भी हजारों लोगों ने कैनेडियन नागरिकता लेने के बावजूद पंजाब में जमीन जायदाद से होने वाली आय और पेंशन आदि ; 
;लाभ का जिक्र कैनेडियन इनकम टैक्स विवरण में नहीं किया है।
(defrule return7
(declare (salience 4900))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id2)
(id-word ?id1 income)
(id-word ?id2 tax)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return7   "  ?id "  vivaraNa )" crlf))
)


;$$$ Modified by Bhagyashri Kulkarni (8-11-2016)
;### No, I can not return them yet. (rapidex)
;### 	नहीं, मैं अभी तक उनको नहीं लौटा सकता हूँ . 
;@@@ Added by 14anu-ban-10 on (17-11-2014)
;Nevertheless, the body still returns to its original dimension when the load is removed.[ncert corpus]
;Pira BI BAra hatAne para piNda aBI BI apanI prAraMBika vimAoM para vApasa A jAwA hE.[ncert corpus]
(defrule return8
(declare (salience 5000))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-kriyA_viSeRaNa  ?id ? ) ;commented by Bhagyashri 
(kriyA-to_saMbanXI ?id ?) ;Added by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vApasa_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return8   "  ?id "  vApasa_A )" crlf))
)


;@@@ Added by 14anu21 on 04.07.2014
;I left a message but he didn't return my call .[oxford]
;मैंने सन्देश छोडा परन्तु वह मेरा बुलावा नहीं लौटा था . 
;मैंने सन्देश छोडा परन्तु  उसने मेरे फोन का प्रत्युत्तर नहीं दिया था . 
(defrule return9
(declare (salience 4990))
(id-root ?id return)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?idobj)
(id-root ?idobj call)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawyuwwara_xe))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  return.clp 	return9   "  ?id "  prawyuwwara_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  return.clp   return9   "  ?id " kA )" crlf))


)



;"return","VT","1.vApisa_karanA/lOtAnA"
;I hope you will return my book soon.
;She never returned me my books.
;Good men return good for evil.
;She could not return his affection.
;Only an expert could have returned that ball.
;--"2.vApasa_AnA"
;The train service has returned to normal.
;The money i gave him returns to me in the event of his death.
;--"3.vApasa_raKanA"
;After reading returned the book to the shelf.
;--"4.lABa_praxAna_karanA"
;Our joint venture returned a net saving of five thousand rupees.
;--"5.vApisa_xenA"
;She taunted him but he returned with a witty sally.
;
