
;@@@ Added by 14anu-ban-09 on (09-04-2015)
;She was keen to take on the role of producer. 	[oald]
;वह निर्माता की भूमिका को लेकर इच्छुक थी . 	[Manual]
(defrule producer1
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 role)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer1   "  ?id "  nirmAwA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-04-2015)
;He’s been a top producer and mixer for the past decade. [oald]
;वह पहले की दशक से एक श्रेष्ठ निर्माता और मिक्सर रह चूँका है .  [Manual]
(defrule producer2
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id2)
(id-root ?id2 top)	
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer2   "  ?id "  nirmAwA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-04-2015)
;Hollywood producers and movie stars.  [oald]
;हॉलीवुड निर्माता और चलचित्र का मुख्य कलाकार है . 	[Manual]
(defrule producer4
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 Hollywood)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer4   "  ?id "  nirmAwA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-04-2015)
;He is a producer of motion pictures.   [collinsdictionary.com]
;वह गति चित्रों का निर्माता है . 	[Manual]
;He is the director and producer. [mw]
;वह निदेशक और निर्माता है .    [Manual]
(defrule producer5
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer5   "  ?id "  nirmAwA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-04-2015)
;Steadily, however, producer and dramatist have worked together to make the stage as illusive as possible. [http://dictionary.reference.com]
;स्थिरतापूर्वक, जैसे भी, निर्माता और नाटककार जितना सम्भव उतना मञ्च को मायावी बनाने के लिए एक साथ काम कर चुके हैं . 	[Manual]

(defrule producer6
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(kriyA-kriyArWa_kriyA  ?id1 ?id2)
(kriyA-object  ?id2 ?id3)
(id-root ?id3 stage)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer6   "  ?id "  nirmAwA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-04-2015)
;An obvious example of this would be engaging a producer for a recording session. [oxforddictionaries.com]
;इसका स्पष्ट उदाहरण एक रिकॉर्डिंग सत्र निर्माता को व्यस्त रखना होगा .	[Manual] 

(defrule producer7
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 session)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id2 record)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer7   "  ?id "  nirmAwA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-04-2015)
;London is the first of 10 stops on its quest to help modern producers and musical innovators gain some exposure.		[oxforddictionaries.com]
;लन्दन 10 स्टाप में से सर्वप्रथम है जो आधुनिक निर्माता और संगीत प्रवर्तक की सहायता करने के लिए उनपर पर कुछ विवरण प्राप्त करते हैं.	 [Manual]

(defrule producer8
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 modern)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer8   "  ?id "  nirmAwA )" crlf))
)


;@@@ Added by 14anu-ban-09 on (13-04-2015)
;Litmus will now spend a day in a recording studio with a producer and their future looks very bright.  [oxforddictionaries.com]
;लिटमस स्टुडियो में निर्माता के साथ अब दिन बिताएगा और उनका भविष्य बहुत अच्छा दिख रहा हैं . 	[Manual]

(defrule producer9
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-with_saMbanXI  ?id1 ?id)
(id-root ?id1 studio)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirmAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer9   "  ?id "  nirmAwA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-04-2015)
;The mould is the producer of the toxin aflatoxin. [oxforddictionaries.com]
;जीवविष ऐफ्लटॉक्सिन को बनाने वाला मोलड है . 	[Manual]

(defrule producer10
(declare (salience 1000)) 
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 aflatoxin)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banAne_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp 	producer10   "  ?id "  banAne_vAlA )" crlf))
)

;---------------------DEFAULT RULE--------------------------------------------

;@@@ Added by 14anu-ban-09 on (09-04-2015)
;Libya is a major oil producer. [oald]
;लिब्या एक मुख्य तेल उत्पादक है .        [Manual]
(defrule producer0
;(declare (salience 0000)) ;commented by 14anu-ban-09
(id-root ?id producer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpAxaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  producer.clp         producer0   "  ?id "  uwpAxaka )" crlf))
)

