;;@@@ Added by 14anu23 13/06/2014
;His work chronicles 20th-century migration. 
;उनका काम 20वीं सदी का कालक्रम से अभिलेखन  करता है .
(defrule chronicle2
(declare (salience 4900))
(id-root ?id chronicle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlakrama_se_aBileKana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chronicle.clp 	chronicle2   "  ?id "  kAlakrama_se_aBileKana_kara )" crlf))
)

;;@@@ Added by 14anu23 19/06/2014
;A chronicle of his life during the war years . 
; युद्ध के वर्षों  के दौरान अपने जीवन का एक कालक्रम से अभिलेखन.
(defrule chronicle3
(declare (salience 5000))
(id-root ?id chronicle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAlakrama_se_aBileKana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chronicle.clp 	chronicle3   "  ?id "  kAlakrama_se_aBileKana )" crlf))
)

;------------------------ Default Rules ----------------------

;;$$$  Modified by Preeti(31-07-2014)
;Her latest novel is a chronicle of life in a Devon village. [Oxford Advanced Learner's Dictionary]
;usakA navInawama upanyAsa dIvOna gAzva ke jIvana kA vqwwAMwa hE.
;"chronicle","N","1.iwihAsa/vqwwAnwa"
;He consulted the chronicles of the period to find the facts.
(defrule chronicle0
(declare (salience 5000))
(id-root ?id chronicle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqwwAMwa/iwihAsa)) ;added vqwwAMwa by Preeti(31-07-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chronicle.clp 	chronicle0   "  ?id "  iwihAsa )" crlf))
)

;"chronicle","VT","1.iwihAsa_meM_liKanA"
;That institute chronicles the events.
(defrule chronicle1
(declare (salience 4900))
(id-root ?id chronicle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iwihAsa_meM_liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chronicle.clp 	chronicle1   "  ?id "  iwihAsa_meM_liKa )" crlf))
)

