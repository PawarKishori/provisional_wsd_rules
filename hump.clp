;@@@ Added by 14anu-ban-06     (11-08-14)
;In the visit of Ladakh the tourists even get the chance to ride on the double hump that is a camel with two humps .  (tourism corpus)
;लद्दाख  की  यात्रा  में  तो  सैलानियों  को  डबल  कूबड़  यानी  दो  कूबड़  वाले  ऊँट  की  सवारी  का  मौका  भी  मिलता  है  ।
(defrule hump3
(declare (salience 5100))
(id-root ?id hump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(subject-subject_samAnAXikaraNa ?id ?id1)(viSeRya-with_saMbanXI ?id1 ?id))
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kUbadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hump.clp  	hump3   "  ?id "  kUbadZa )" crlf))
)
;-------------------- Default rules ------------------------

(defrule hump0
(declare (salience 5000))
(id-root ?id hump)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id humped )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kubadA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hump.clp  	hump0   "  ?id "  kubadA )" crlf))
)

;"humped","Adj","1.kubadA"
;usa 'humped' bUDZI ko bacce ciDZAwe hEM.
;
(defrule hump1
(declare (salience 4900))
(id-root ?id hump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mittI_kA_tIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hump.clp 	hump1   "  ?id "  mittI_kA_tIlA )" crlf))
)

;"hump","N","1.mittI_kA_tIlA"
;kone meM 'hump'padA hE.
;
(defrule hump2
(declare (salience 4800))
(id-root ?id hump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kUbadZa_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hump.clp 	hump2   "  ?id "  kUbadZa_uTA )" crlf))
)

;"hump","V","1.kUbadZa_uTAnA/prayAsa_karanA"
;mEne ParnIcara ko sArA xina 'humping'karawA rahA.
;
