
(defrule skip0
(declare (salience 5000))
(id-root ?id skip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kUxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  skip.clp 	skip0   "  ?id "  kUxa )" crlf))
)

;"skip","V","1.uCala-kUxa karanA"
;Frogs were skipping in the pond.
(defrule skip1
(declare (salience 4900))
(id-root ?id skip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uCala-kUxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  skip.clp 	skip1   "  ?id "  uCala-kUxa_kara )" crlf))
)
;@@@ Added by 14anu21 on 29.05.2014 
;We woke up in the morning late,so we had to skip breakfast.
;हम सुबह देर से जगे, इसलिए हमें जलपान उछल-कूद करना पड़ा.
;हम सुबह देर से जगे, इसलिए हमें जलपान छोड़ना पड़ा.
(defrule skip2
(declare (salience 5000))
(id-root ?id skip)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-word ?id1 breakfast|chance|question|answer|dinner|lunch)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Coda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  skip.clp 	skip2   "  ?id "  Coda )" crlf))
)


;--"2.viRaya baxalawA rahanA"  
;He was skipping from one subject to another.
;--"3.CodZa jAnA/CodZa xenA"
;In a hurry he skip breakfast.
;Skipping on rope is good for healthy person.
;
;
