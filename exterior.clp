;@@@ Added by 14anu-ban-04 (06-10-2014)
;Each of these is equipped with its own set of organs — on either side, leg-like projections sometimes equipped with bristles and another pair of feathery appendages through which oxygen is absorbed; and within the body wall, a pair of tubes opening to the exterior from which waste is secreted .      [bnc_gold]
;इनमें से हरेक अपने खुद के अंगों के संग्रह से लैस है - दोनों तरफ, पैर जैसे उभार कभी कभी शूक से /कड़े बालों से सुसज्जित/लैस और एक और उपांगों/उपकरणों का जोड़ा जिससे ऑक्सीजन अंदर लिया जाता है; और शारीरिक दीवार के अंदर, बाहर की ओर खुलता हुआ नालियों का एक जोड़ा जिससे गन्दगी बाहर निकलती है .      [manual]
(defrule exterior2
(declare (salience 4910))
(id-root ?id exterior)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhara_kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exterior.clp 	exterior2  "  ?id "  bAhara_kI_ora )" crlf))
)

;------------------------ Default Rules ----------------------

;"exterior","Adj","1.bAharI"
;Carvings were done on the exterior surface of the building.
(defrule exterior0
(declare (salience 5000))
(id-root ?id exterior)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAharI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exterior.clp 	exterior0   "  ?id "  bAharI )" crlf))
)

;"exterior","N","1.bAhya_rUpa"
;He is a man with a rough exterior.
(defrule exterior1
(declare (salience 4900))
(id-root ?id exterior)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhya_rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exterior.clp 	exterior1   "  ?id "  bAhya_rUpa )" crlf))
)

