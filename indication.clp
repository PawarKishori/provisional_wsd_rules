;@@@ Added by 14anu-ban-06  (07-11-2014)
;Railway stock increased significantly and composition changed more rapidly an indication of the technological changes in the railway system .(parallel corpus)
;रेलवे के भंडारों में महत़्वपूर्ण वृद्धि हुई और इसके रूप रंग में भी तेजी से परिवर्तन हुए जो रेलवे व़्यवस़्था में टेक़्नोलौजिकल परिवर्तनों का संकेत था .(parallel corpus)
;In the Ved Sahinta in Mantraakshras , there are indication of high , medium or low melodious intonation by making standing and slanting lines .(parallel corpus)
;वेद की संहिताओं में मंत्राक्षरॊं में खड़ी तथा आड़ी रेखायें लगाकर उनके उच्च मध्यम या मन्द संगीतमय स्वर उच्चारण करने के संकेत किये गये हैं ।(parallel corpus)
;Indications are that this trend will continue because of the medical advances and adoption of more hygienic practices coupled with family planning .(parallel corpus)
;ऐसे संकेत मिले हैं कि परिवार नियोजन के साथ चिकित़्सा विज्ञान के क्षेत्र में हुई प्रगति और अधिक स़्वास़्थ़्यकर विधियां अपनाए जाने के कारण औसत आयु में वृद्धि की यह प्रवृत्ति चलती रहेगी .(parallel corpus)
;Even more sensational than his indication of his motives was Sheikh's claim that Pearl had been killed .(parallel corpus)
;अपने उद्देश्यों का संकेत देने से कहीं ज्यादा सनसनीखेज शेख का यह दावा था कि पर्ल मारे जा चुके हैं .(parallel corpus)
;A clear indication that the shark populace around the islands had dwindled - and drastically so .(parallel corpus)
;यह स्पष्ट संकेत है कि इन द्वीपों के इर्दगिर्द शार्क मछलियों की संया काफी घट गई है और इसमें नाटकीय गिरावट आई है .(parallel corpus)
;Whenever there is any indication of illness , the animal should be given sufficient rest .(parallel corpus)
;जब कभी रोग का संकेत मिले तो हाथी को पर्याप़्त विश्राम दिया जाना चाहिए .(parallel corpus)
;If the recent controversy over BALCO is any indication , the Government persuaded the cognoscenti but didn't quite win the political battle .(parallel corpus)
;बाल्को के ताजा विवाद से बस यही संकेत मिलता है कि सरकार आशंकित लगों को तो समज्ह - बुज्ह सकी है लेकिन राजनैतिक मोर्चा फतह नहीं कर पाई है .
;(parallel corpus)

(defrule indication0
(declare (salience 0))
(id-root ?id indication)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkewa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indication.clp 	indication0   "  ?id "  saMkewa )" crlf))
)

;@@@ Added by 14anu-ban-06  (07-11-2014)
;He has believed that Munshi word is indication of respect , which had given by Premchand's fans .(parallel corpus)
;उनका यह भी मानना है कि मुंशी शब्द सम्मान सूचक है जिसे प्रेमचंद के प्रशंसकों ने कभी लगा दिया होगा ।(parallel corpus)
;Temperature is a relative measure, or indication of hotness or coldness.(NCERT)
;ताप तप्तता अथवा शीतलता की आपेक्षिक माप अथवा सूचक  होता है.(NCERT)
(defrule indication1
(declare (salience 0))
(id-root ?id indication)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 coldness|hotness|respect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUcaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indication.clp 	indication1   "  ?id "  sUcaka )" crlf))
)


;### examples
;The indications of heat in the mare are restlessness , relaxation of the external genital organs , frequent urination , mucous discharges from the vulva and display of keen sexual desire .
;घोड़ी में गर्मी के लक्षण हैं : बेचैनी , बाहरी प्रजनन अंगों का ढीला पड़ना , बार बार पेशाब आना , भग से श़्लेष़्मक स्राव निकलना और मेल की तीव्र इच़्छा व़्यक़्त करना .
;Nature gives indications for climate changes.
;प्रकृति जलवायु परिवर्तन की सूचना देती है
;Some indications of the need for root canal treatment may be .
;रूट कनाल उपचार की आवश्यकता के कुछ लक्षण निम्न हो सकते हैं .
;On the surface of the pupa we may notice indications of the legs , wings , etc , of the future butterfly .
;उसकी सतह पर भावी तितली की टांगों , पंखों आदि के चिह्न दिखाई देते हैं .


