
;--------------------------DEFAULT RULE------------------------------------------------------------
;@@@ Added by 14anu-ban-09 on (19-9-2014)
;In the preceding Chapter, our concern was to describe the motion of a particle in space quantitatively. [NCERT CORPUS]
;पिछले अध्याय में हमारा सम्बन्ध दिक्स्थान में किसी कण की गति का मात्रात्मक वर्णन करने से था.
;piCale aXyAya meM hamArA sambanXa xiksWAna meM kisI kaNa kI gawi kA mAwrAwmaka varNana karane se WA.

(defrule our0  
(id-root ?id our)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  hamArA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " our.clp	our0  "  ?id "   hamArA  )" crlf))
)

;---------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (19-9-2014)
;Let us first guess the answer based on our common experience.  [NCERT CORPUS]
;आइए, सबसे पहले हम अपने सामान्य अनुभवों के आधार पर इस प्रश्न के उत्तर का अनुमान लगाएँ.
;Aie, sabase pahale hama apane sAmAnya anuBavoM ke AXAra para isa praSna ke uwwara kA anumAna lagAez.

(defrule our1  
(declare (salience 2000))
(id-root ?id our)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?id)
(id-root ?id1 experience)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  apanA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " our.clp	our1  "  ?id "   apanA  )" crlf))
)
