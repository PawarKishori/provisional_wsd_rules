;"touched","Adj","1.praBAviwa_honA"
;She was deeply touched by his sad story.
;
(defrule touch0
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " touch.clp touch0 " ?id "  TIka_kara )" crlf)) 
)

(defrule touch1
(declare (salience 4900))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TIka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " touch.clp	touch1  "  ?id "  " ?id1 "  TIka_kara  )" crlf))
)

(defrule touch2
(declare (salience 4800))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id touched )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id praBAviwa_honA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  touch.clp  	touch2   "  ?id "  praBAviwa_honA )" crlf))
)

(defrule touch3
(declare (salience 4700))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sparSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch3   "  ?id "  sparSa_kara )" crlf))
)

;default_sense && category=verb	sparSa_kara	0
;"touch","VTI","1.sparSa_kara"
;The two creepers are touching each other.
;--"2.CUnA"
;Touch the floor.
;He is the best actor.No one can touch him.
;The water touched the danger mark .
;Whatver Midas touched became gold.
;--"3.halakI_kRawi_pahuzcAnA"
;The museum was not touched by the fire.
;--"4.asara_karanA"
;The news of his father's death touched him deeply.
;
(defrule touch4
(declare (salience 4600))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id touching )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id karuNAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  touch.clp  	touch4   "  ?id "  karuNAwmaka )" crlf))
)

;"touching","Adj","1.karuNAwmaka"
;Charles Dickens stories are touching.
;
(defrule touch5
(declare (salience 4500))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " touch.clp touch5 " ?id "  TIka_kara )" crlf)) 
)

(defrule touch6
(declare (salience 4400))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TIka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " touch.clp	touch6  "  ?id "  " ?id1 "  TIka_kara  )" crlf))
)

(defrule touch7
(declare (salience 4300))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sparSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch7   "  ?id "  sparSa_kara )" crlf))
)

;default_sense && category=verb	sparSa_kara	0
;"touch","VTI","1.sparSa_kara"
;The two creepers are touching each other.
;--"2.CUnA"
;Touch the floor.
;He is the best actor.No one can touch him.
;The water touched the danger mark .
;Whatver Midas touched became gold.
;--"3.halakI_kRawi_pahuzcAnA"
;The museum was not touched by the fire.
;--"4.asara_karanA"
;The news of his father's death touched him deeply.
;
;


;@@@ Added by Prachi Rathore[28-1-14]
;She played the piano with a light touch.[oald]
;उसने एक हल्के स्पर्श के साथ पियानो बजाया. 
(defrule touch8
(declare (salience 4300))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sparSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch8   "  ?id "  sparSa )" crlf))
)


;@@@ Added by Prachi Rathore[28-1-14]
;There was a touch of sarcasm in her voice.[oald]
;उसकी आवाज में व्यङ्ग्योक्ति का भाव था . 
(defrule touch9
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch9   "  ?id "  BAva )" crlf))
)

;@@@ Added by Prachi Rathore[28-1-14]
;The gentle touch of his hand on her shoulder made her jump.[oald]
;उसके कन्धे पर उसके हाथ के हल्के स्पर्श से वह कूद पड़ी . 
(defrule touch10
(declare (salience 5050))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sparSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch10   "  ?id "  sparSa )" crlf))
)

;$$$ Modified by 14anu-ban-07,(10-03-2015)
;She prefers to answer any fan mail herself for a more personal touch.(oald)
;वह ज़्यादा वैयक्तिक रूप देने लिए किसी प्रशंसक की डाक को स्वयं उत्तर देना पसन्द करती है . (manual)
;@@@ Added by Prachi Rathore[28-1-14]
;He put the finishing touches to his painting.[oald]
;उसने उसकी कलाकृति को अंतिम रूप दिया.
(defrule touch11
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 final|finish|decorative|professional|personal)   ;added 'personal' by 14anu-ban-07,(10-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch11   "  ?id "  rUpa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_touch11
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 final|finish|decorative|professional)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " touch.clp   sub_samA_touch11   "   ?id " rUpa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_touch11
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 final|finish|decorative|professional)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " touch.clp   obj_samA_touch11   "   ?id " rUpa )" crlf))
)


;@@@ Added by 14anu18 (13-06-14)
;There are some addresses in this booklet to put you in touch with the right people.
;ठीक लोगों के साथ सम्पर्क में आपको डालने के लिए इस छोटी पुस्तक में कुछ पते हैं . 

(defrule touch12
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMparka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch12   "  ?id "  saMparka )" crlf))
)

;@@@ Added by 14anu-ban-09 on (30-07-2014)
;Dhauladhar mountain peak is a good place to see deep slopes and mountains touching open sky. [Parallel Corpus]
;धौलाधार  पर्वत  शिखर  गहरी  ढ़लानों  और  खुले  आसमान  को  छूते  पर्वतों  को  देखने  के  लिए  अच्छी  जगह  है  ।
;It is said that till 1937 the Ganges used to flow touching the steps of the Ganges Temple of Garh Mukteshwar. [Parallel Corpus]
;बताते  हैं  कि  1937  तक  गंगा  जी  गढ़  मुक्तेश्वर  के  गंगा  मंदिर  की  सीढ़ियों  को  छूते  हुए  बहती  थीं  ।
;The Arabian sea touching the lands of Daman has provided Daman with unique natural beauty and greenery.
;दमन  की  जमीन  को  छूते  अरब  सागर  ने  दमन  को  अप्रतिम  प्राकृतिक  सुंदरता  व  हरियाली  प्रदान  की  है  ।

(defrule touch13
(declare (salience 4800))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CUwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch13   "  ?id "  CUwA )" crlf))
)

;@@@ Added by 14anu-ban-07 ,(08-09-2014)
;On the spread feet of Urdhvabahu Baba common Indians are repeatedly touching their heads in respect .(tourism corpus)
;उर्ध्वबाहु बाबा के पसरे पाँवों पर आम हिन्दुस्तानी बारी-बारी मत्था टेक रहे हैं ।
(defrule touch14
(declare (salience 6000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) their)
(id-root =(+ ?id 2) head)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2) mawWA_teka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " touch.clp	 touch14  "  ?id "  " (+ ?id 1) (+ ?id 2) " mawWA_teka  )" crlf))
)

;@@@ Added by 14anu-ban-07 ,(09-03-2015)
;No one can touch him when it comes to interior design.(oald)
;कोई भी  उसका मुकाबला नहीं कर सकता है जब बात इन्टेरिओर् डिजाइन की आती है . (manual)
(defrule touch15
(declare (salience 4800))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(kriyA-kriyA_niReXaka  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukAbalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch15   "  ?id "  mukAbalA_kara )" crlf))
)

;@@@ Added by 14anu-ban-07 ,(09-03-2015)
;The speedometer was touching 90.(oald)
;चालमापी 90 को छू रहा था . (manual)
(defrule touch16
(declare (salience 4900))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 speedometer)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CU))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch16   "  ?id "  CU )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " touch.clp touch16  "  ?id " ko)" crlf)
)
)

;@@@ Added by 14anu-ban-07,(09-03-2015)
;Tornadoes touched down in Alabama and Louisiana.(oald)
;टॊर्नॆडोव्ज ऐलबैम और लूईजीऐन की जमीन पर उतरा . (manual)
(defrule touch17
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jamIna_para_uwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " touch.clp	touch17  "  ?id "  " ?id1 "  jamIna_para_uwara  )" crlf))
)

;@@@ Added by 14anu-ban-07,(09-03-2015)
;He tried to touch me for 50 bucks.(oald) (parser problem)
;उसने मुझसे 50 बक लेने केलिए मनाने का प्रयास किया . (manual)
(defrule touch18
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lene_kelie_manA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " touch.clp	touch18  "  ?id "  " ?id1 "  lene_kelie_manA  )" crlf))
)

;@@@ Added by 14anu-ban-07,(09-03-2015)
;Her comments touched off a wave of protests.(oald)
;उसकी टिप्पणियों ने विरोधों की लहर शुरू करवा दी . (manual)
(defrule touch19
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SurU_karavA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " touch.clp	touch19  "  ?id "  " ?id1 "  SurU_karavA_xe  )" crlf))
)

;@@@ Added by 14anu-ban-07,(09-03-2015)
;The talk was about educational opportunities for adults, and the speaker also touched upon sources of finance.(cambridge)(parser no. 12)
;बातचीत बालिगों के लिए शैक्षिक मौकों के बारे में थी, और वक्ता ने आर्थिक व्यवस्था के स्रोत  पर भी प्रकाश डाला . (manual)
(defrule touch20
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 upon)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 para_prakASa_dAla))
(assert (kriyA_id-subject_viBakwi ?id ne))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " touch.clp	touch20  "  ?id "  " ?id1 "  para_prakASa_dAla  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* " touch.clp touch20  "  ?id " ne)" crlf)
)
)

;@@@ Added by 14anu-ban-07,(09-03-2015)
;The central issue of the debate was hardly touched on in his speech.(oald)
;बहस के केंद्रीय विषय पर उसके भाषण में  मुश्किल से  प्रकाश डाला गया था . (manual)
(defrule touch21
(declare (salience 5000))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakASa_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch21   "  ?id "  prakASa_dAla  )" crlf))
)

;@@@ Added by 14anu-ban-07,(10-03-2015)
;The music was a touch too loud for my liking.(oald)
;सङ्गीत मेरी पसन्द के लिए  थोडा ज्यादा ही ऊँचे स्वर वाला था . (manual)
(defrule touch22
(declare (salience 4400))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 loud|cold)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch22   "  ?id "  WodZA )" crlf))
)

;@@@ Added by 14anu-ban-07,(10-03-2015)
;Tragedy touched their lives when their son was 16.(cambridge)
;दुःखद घटना ने उनके जीवन पर प्रभाव डाला जब उनका बेटा 16 का था . (manual)
(defrule touch23
(declare (salience 5100))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 tragedy)
(kriyA-object  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva_dAla))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch23   "  ?id "  praBAva_dAla  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " touch.clp touch23  "  ?id " para)" crlf)
)
)

;@@@ Added by 14anu-ban-07,(10-03-2015)
;The speech had several comic touches.(cambridge)
;भाषण में कई हास्यप्रद भाव थे . (manual)
(defrule touch24
(declare (salience 5200))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 comic)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch24   "  ?id "  BAva )" crlf))
)

;@@@ Added by 14anu-ban-07,(10-03-2015)
;We lost touch over the years. (cambridge)
;हम वर्षों से संपरक में नहीं हैं . (manual)
(defrule touch25
(declare (salience 5100))
(id-root ?id touch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 lose)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMparka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  touch.clp 	touch25   "  ?id "  saMparka )" crlf))
)

