
;@@@ Added by 14anu-ban-04 (03-12-2014)
;The following species are some examples of families representing the most devastating members of Coleoptera in Persia.   [agriculture]
;निम्नलिखित प्रजातियाँ फारस में चोलेओप्तेराके के सबसे हानिकारक सदस्यों का प्रतिनिधित्व करने वाले कुलों के कुछ उदाहरण हैं।                 [manual]
(defrule devastate2
(declare (salience 5010))
(id-root ?id devastate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 member)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id hAnikAraka))         
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  devastate.clp  devastate2   "  ?id "  hAnikAraka)" crlf))
)

;------------------ Default rules -----------------------


(defrule devastate0
(declare (salience 5000))
(id-root ?id devastate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id devastating )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ujAdZane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  devastate.clp  	devastate0   "  ?id "  ujAdZane_vAlA )" crlf))
)

;"devastating","Adj","1.ujAdZane_vAlA"
;Orissa has been hit by devastating cyclonic storm.
;
(defrule devastate1
(declare (salience 4900))
(id-root ?id devastate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ujAdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devastate.clp 	devastate1   "  ?id "  ujAdZa )" crlf))
)

;"devastate","VT","1.ujAdZanA"
;Cyclone has devastated many places of Orissa.
;
;
