
(defrule design0
(declare (salience 5000))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id designing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cAlAka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  design.clp  	design0   "  ?id "  cAlAka )" crlf))
)

;"designing","Adj","1.cAlAka"
;A selfish && designing nation obsessed with the dark schemes of European intrigue
;

;Added by Pramila(Banasthali University) on 19-11-2013
;The design was made for a dress.                     ;sentence of this clip file
; poSAka ke lie dijAina banAI gaI WIM.
(defrule design1
(declare (salience 4900))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(or(and(kriyA-subject  ?id1 ?id)(kriyA-for_saMbanXI  ?id1 ?id2))(viSeRya-viSeRaNa  ?id ?id1))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dijAina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design1   "  ?id "  dijAina )" crlf))
)

;Added by Pramila(Banasthali University) on 19-11-2013
;We don't know whether it was done by accident or by design.                    ;sentence of this clip file
;hameM yaha pawA nahIM ki vaha kisI rxuGatanA se huI yA kisI uxxeSya se huI.
(defrule design2
(declare (salience 4900))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxxeSya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design2   "  ?id "  uxxeSya )" crlf))
)


;Added by Pramila(Banasthali University) on 19-11-2013
;I have no design to go to Bombay.               ;sentence of this clip file
;merA baMbaI jAne kA koI irAxA nahIM .
;
(defrule design3
(declare (salience 4900))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id irAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design3   "  ?id "  irAxA )" crlf))
)

;;COMMENTED BY 14anu-ban-04 on (23-03-2015) BECAUSE IT IS FIRING FROM DEFAULT RULE design7
;$$$ Modified by 14anu-ban-04 on (02-03-2015)
;Added by Pramila(Banasthali University) on 19-11-2013
;This dictionary is designed for advanced learners of English.   ;cald
;यह शब्दकोश आरम्भिक अंग्रेजी सीखने वालों के उद्देश्य से बनाया गया है 
;These measures are designed to reduce pollution.             ;cald
;ये उपाय प्रदूषण कम करने के लिए बनाए गए हैं .
;(defrule design4
;(declare (salience 5000))
;(id-root ?id design)
;?mng <-(meaning_to_be_decided ?id)
;(or(kriyA-for_saMbanXI  ?id ?id1)(kriyA-kriyArWa_kriyA  ?id ?id1))        
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id banA))              ;changed 'wsd_word_mng'  to 'wsd_root_mng'  by 14anu-ban-04 on (02-03-2015)
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  design.clp  	design4   "  ?id "  banA )" crlf))                                ;changed 'wsd_word_mng'  to 'wsd_root_mng'  by 14anu-ban-04 on (02-03-2015)
;)

;@@@ Added by Pramila(BU) on 01-02-2014
;A design impressed on the book's cover.            ;m-w
;पुस्तक के आवरण पर एक आकृति छपी है.
(defrule design5
(declare (salience 5000))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?kri ?id)
(kriyA-on_saMbanXI  ?kri ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akqwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design5   "  ?id "  Akqwi )" crlf))
)

;@@@ Added by 14anu-ban-04 on 26-08-2014
;Electron beams can be focussed by properly designed electric and magnetic fields.  [NCERT-CORPUS]
;इलेक्ट्रॉन पुञ्जों को उचित रीति से अभिकल्पित वैद्युत एवं चुम्बकीय क्षेत्रों द्वारा फोकसित किया जा सकता है.                 [NCERT-CORPUS]
(defrule design8
(declare (salience 3100))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-kriyA_viSeRaNa ?id ?id1)                   ;commented by 14anu-ban-04 on (02-03-2015)
(viSeRaNa-viSeRaka ?id ?id1)                       ;uncommented by 14anu-ban-04 on (02-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBikalpiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design8  "  ?id "  aBikalpiwa)" crlf))
)

;@@@ Added by 14anu-ban-06 (21-10-2014)
;For example, while designing a building, knowledge of elastic properties of materials like steel, concrete etc. is essential.(NCERT)
;उदाहरण के लिए, किसी भवन को डिजाइन करते समय इस्पात, काङ्क्रीट आदि द्रव्यों की प्रत्यास्थ प्रकृति का ज्ञान आवश्यक है.(NCERT)
;One could also ask — Can we design an aeroplane which is very light but sufficiently strong?(NCERT)
;यह प्रश्न भी पूछा जा सकता है कि क्या हम ऐसा वायुयान डिजाइन कर सकते हैं जो बहुत हलका फिर भी बहुत मजबूत हो ?(NCERT)
;Can we design an artificial limb which is lighter but stronger?(NCERT)
;क्या हम एक ऐसा कृत्रिम अङ्ग डिजाइन कर सकते हैं जो अपेक्षाकृत हलका किंतु अधिक मजबूत हो ?(NCERT)
(defrule design9
(declare (salience 3000))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-word ?id1 building|aeroplane|limb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dijAina_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design9   "  ?id "  dijAina_kara )" crlf))
)

;COMMENTED BY 14anu-ban-04 on (23-03-2015) BECAUSE IT IS FIRING FROM DEFAULT RULE design7
;@@@ Added by 14anu-ban-04 on (02-03-2015)
;A formal garden is carefully designed and kept according to a plan, and it is not allowed to grow naturally.      [cald]
;एक सुव्यवस्थित उद्यान सावधानी से बनाया गया है और रखा एक योजना के अनुसार रखा गया है, और इसे स्वाभाविक रूप से विकसित करने के लिए अनुमति नहीं है.        [self]
;(defrule design10
;(declare (salience 3100))
;(id-root ?id design)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-subject ?id ?id1)
;(id-root ?id1 garden)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id banA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design10  "  ?id "  banA)" crlf))
;)


;@@@ Added by 14anu-ban-04 on (02-03-2015)
;The richness of the many stories, the complexity of thought, and the intricacy of design ultimately can not be summarized but must be discovered for themselves.                                           [COCA]
;बहुत सारी कहानियों की धनाढ्यता, विचारो  की जटिलता, और डिज़ाइन की बारीकी अंततः संक्षिप्त नहीं हो सकती है परन्तु खुद के लिए आविष्कृत करनी होगी .      [self]
(defrule design11
(declare (salience 4010))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dijAina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design11  "  ?id "  dijAina)" crlf))
)

;@@@ Added by 14anu-ban-04 on (02-03-2015)
;She designed to go far in the world of business.                      [same clp file]
;उद्योग के क्षेत्र  में   सफ़ल होने  के लिए उसने  योजना बनाई .                                  [self]
(defrule design12
(declare (salience 5010))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-kriyArWa_kriyA ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yojanA_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design12  "  ?id "  yojanA_banA)" crlf))
)

;@@@ Added by 14anu-ban-04 on (23-03-2015)
;He designed and built his own house.                [oald]
;उसने  अपने  घर का नक्शा बनाया और उसका  निर्माण किया .             [self]
(defrule design13
(declare (salience 300))                  
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(conjunction-components ? ?id ?id1)
(kriyA-object ?id1 ?id2)
(id-root ?id1 build) 
(id-root ?id2 company|house) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakSA_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design13   "  ?id "  nakSA_banA )" crlf))
)

;@@@ Added by 14anu-ban-04 (23-03-2015)
;They asked me to design a poster for the campaign.             [oald]
;उन्होंने मुझे अभियान के लिए विज्ञापन की रूपरेखा बनाने को कहा .                     [self]
(defrule design14
(declare (salience 200))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpareKA_banA))
(assert (kriyA_id-object_viBakwi ?id kI)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   design.clp      design14   "  ?id " kI  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design14   "  ?id "  rUpareKA_banA )" crlf))
)

;-----------------------------default rules------------------------------------------------------------
(defrule design6
(declare (salience 4000))
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id racanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design6   "  ?id "  racanA )" crlf))
)

;$$$ Modified by 14anu-ban-04 (23-03-2015)              ------changed meaning from 'nakSA_banA' to 'banA'
;This dictionary is designed for advanced learners of English.   [cald]
;यह शब्दकोश  अंग्रेजी के उच्च शिक्षार्थियों के लिए  बनाया गया है .                  [manual]            
(defrule design7
(declare (salience 100))                  ;salience reduced from '3000' to '100' by 14anu-ban-04 (23-03-2015)
(id-root ?id design)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  design.clp 	design7   "  ?id "  banA )" crlf))
)

;default_sense && category=verb	rUpAkAra_xe	0
;"design","V","1.rUpAkAra_xenA"
;Design a better mouse trap.
;This room is not designed for work
;--"2.yojanA_banAnA
;She designed a good excuse for not attending classes that day
;She designed to go far in the world of business
;
;
;LEVEL 
;
;
;
;"design"
;N
;1.dijAina/rUpareKA [sketch/drawing]
;The design was made for a dress.
; poSAka ke lie dijAina banAI gaI WIM.
;
;2.aBikalpa/dijAina [decoration]
;The dress had a beautifull floral design.
;vaswra para eka suMxara PUlo kA dijAina WA.
;
;3.uxxeSya [purpose]
;We don't know whether it was done by accident or by design.
;hameM yaha pawA nahIM ki vaha kisI rxuGatanA se huI yA kisI uxxeSya se huI.
;
;4.irAxA [an intention]
;I have no design to go to Bombay.
;merA baMbaI jAne kA koI irAxA nahIM .
;
;V
;1.dijAina banAnA [to decide to work/look]
;We design kitchens for today's needs.
;hamane Aja ke lie rasoIGaro kA dijAina banAyA.
;
;2. rUpareKA banAnA
;The course is designed to improve their communication skills.
;unakI vicAra prakata karane kI kRamawAoM ko aXika acCA banAne ke anusAra korsa kI rUpareKA
;banI hE
;
;
;yahAz isa Sabxa kA mUla arWa "racanA"ke rUpa meM sAmane AwA hEM.jEse
;
;a.poSAka ke lie dijAina banAI gayI WIM.   
;b.vahA eka suxaMra PUloM kA dijAina WA.     <--racanA WI 
;c.hameM yaha pawA nahIM ki vaha kisI xurGatanA se huI yA kisI uxxeSya se huI.
;                                        <--koI_niSciwa_irAxA<--koI_racanA(vyUharacanA)
;d.merA baMbaI jAne kA koI irAxA nahIM.   <---koI aBikalpanA nahIM hE
;e.hamane Aja kI jZarUrawoM ke lie rasoIGaro kA dijAina banAyA . <--racanA [aBikalpanA]
;f.unakI vicAra prakata karane kI kRamawAoM ko aXika acCA banAne ke anusAra korsa kI rUpareKA banI hE.    <--aBikalpanA_kI_gayI_hE
;
;ina uXAharaNo ke AXAra para isakA sUwra hogA,
;                    
;sUwra : racanA[<aBikalpanA] 
;
;


