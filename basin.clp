;@@@Added by 14anu-ban-02(17-03-2015)
;I broke two eggs into a basin for my cooking. [self]
;मैंने  अपने पकाने के लिए दो अण्डे प्याले में  तोड़े . [self]
(defrule basin1
(declare (salience 100)) 
(id-root ?id basin) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-into_saMbanXI  ?id1 ?id)
(id-root ?id1 break)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pyAlA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  basin.clp  basin1  "  ?id "  pyAlA )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(17-03-2015)
;Sentence: A basin of cold water.[mw]
;Translation:ठण्डे पानी का जलकुण्ड.[self]
(defrule basin0 
(declare (salience 0)) 
(id-root ?id basin) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id jalakuNda)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  basin.clp  basin0  "  ?id "  jalakuNda )" crlf)) 
) 



