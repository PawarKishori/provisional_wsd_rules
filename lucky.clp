;@@@ Added by 14anu-ban-08 on (20-09-2014)
;The caravan would be very lucky to reach the oasis .   [karan singla]
;हम बड़े भाग्यशाली होंगे , अगर हमारा कारवां नखलिस्तान तक सही सलामत पहुच सका , तो !
(defrule lucky0
(declare (salience 0))     
(id-root ?id lucky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAgyaSAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lucky.clp       lucky0   "  ?id "  BAgyaSAlI )" crlf))
)

;@@@ Added by 14anu-ban-08 on (20-09-2014)
;Well , lucky for me , a lot of people answered that call .    [karan singla]
;मेरा सौभाग्य था कि कई लोगों ने मेरे इस बुलावे का जवाब दिया .
(defrule lucky1
(declare (salience 500))
(id-root ?id lucky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 ?call)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sOBagya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lucky.clp       lucky1   "  ?id "  sOBagya )" crlf))
)

;@@@ Added by 14anu-ban-08 on (20-09-2014)
;See a woman doctor , if she is lucky to have one as a neighbour , or die.     [karan singla]
;यदि वह खुशकिस्मत है और उसके पड़ेस में कोई महिल चिकित्सक है तो वहां जा सकती है या फिर मौत के मुंह में .
(defrule lucky2
(declare (salience 450))
(id-root ?id lucky)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa ?id ?id1)
(id-root ?id1 have)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KuSakismawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lucky.clp       lucky2   "  ?id "  KuSakismawa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (20-09-2014)
;Those days are lucky when the planets migrate from one sign into the other , especially the sun .   [karan singla]
;संऋआंति उन दिनों को शुभ माना जाता है जब ग्रह विशेषतः सूर्य एक राशि से दूसरी राशि की ओर प्रसऋ - ऊण्श्छ्ष् - थान करता है .
(defrule lucky3
(declare (salience 502))
(id-root ?id lucky)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 day|it)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SuBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lucky.clp       lucky3   "  ?id "  SuBa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (20-09-2014)
;Not withstanding the nature of the punyakala is such as here explained , some of them are considered as lucky , others as unlucky days .  [karan Singla]
;इसके बावजूद कि पुण़्य काल का स़्वरूप वही होता है जैसा कि यहां बताया गया है कुछ दिन शुभ और कुछ दिन अशुभ माने गए हैं .
(defrule lucky4
(declare (salience 503))
(id-root ?id lucky)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 some)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SuBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lucky.clp       lucky4   "  ?id "  SuBa )" crlf))
)
