;$$$ Modified by 14anu-ban-04 (21-02-2015)
;Industry in Britain has been in decline since the 1970s.              [oald]
;ब्रिटन में उद्योग 1970s से  में हो गया है . 
;"decline","N","1.pawana/kRaya"
(defrule decline0
(declare (salience 4000))
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?kri ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
(kriyA-in_saMbanXI ?kri ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp 	decline0   "  ?id "  pawana )" crlf))
)


;@@@ Added by Pramila(Banasthali university) on 04-01-2014
;The minister has declined to make a statement.     ;shiksharthi
;मंत्री ने बयान देने से इंकार कर दिया है.
;He might decline to accept the offer.        ;old clp
;वह प्रस्ताव स्वीकार करने से इंकार कर सकता है.
(defrule decline3
(declare (salience 5000))
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id inakAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp 	decline3   "  ?id "  inakAra_kara )" crlf))
)

;$$$ Modified by 14anu-ban-04 (21-02-2015)            ;----meaning changed from 'gira' to 'KarAba_ho'
;@@@ Added by Pramila(Banasthali university) on 04-01-2014
;His health has declined.       ;shiksharthi
;उसका स्वास्थय गिर गया है.     
;उसका स्वास्थ्य खराब हुआ है .            [modified translation]  by 14anu-ban-04
(defrule decline4
(declare (salience 5000))
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 health)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba_ho))          
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp 	decline4   "  ?id "  KarAba_ho )" crlf))
)

;@@@ Added by 14anu-ban-04 (21-02-2015)
;He declined my offer.                         [cald]
;उसने मेरा प्रस्ताव अस्वीकार किया .                       [self]
(defrule decline5
(declare (salience 5000))
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asvIkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp 	decline5   "  ?id "  asvIkAra_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (21-02-2015)
;I invited him to the meeting but he declined.                        [cald]
;मैंने उसे बैठक में  आमंत्रित  किया परन्तु उसने इन्कार कर दिया .                        [self]
(defrule decline6
(declare (salience 5000))
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(conjunction-components  ?id2 ?id3 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id inkAra_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp 	decline6   "  ?id "  inkAra_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-04 (21-02-2015)
;She met with a big accident in the declining years of her life.               [oald]
;उसके जीवन के आखिरी वर्षों में उसके साथ  एक बड़ी दुर्घटना हुई .                                    [self]
(defrule decline7
(declare (salience 5000))
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id gerund_or_present_participle)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 year)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AKirI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp 	decline7   "  ?id "  AKirI )" crlf))
)


;@@@ Added by Manasa (26-8-2015) Arsha Sodh Sansthan
;He mourned the perennial poverty of Calcutta and the Socio-economic decline of Bengal.
;vaha kalakawwA ke cirasWAyI xarixrawa ora beMgAla ke sAmAjika-ArWika xurxaSA ka Soka karawA heM.
(defrule decline8
(declare (salience 5000))
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurxaSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp  decline8   "  ?id "  xurxaSA )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$ Modified by Pramila(Banasthali university) on 04-01-2014
;Price are declining.
;कीमतें कम हो रही है.
;meaning changed from "manA_kara" to "kama_ho"
;"decline","V","1.manA_karanA"
;He might decline to accept the offer.
(defrule decline1
(declare (salience 4000))
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp 	decline1   "  ?id "  kama_ho )" crlf))
)

;$$$ Modified by 14anu-ban-04 (21-02-2015)
;@@@ Added by Pramila(Banasthali university) on 04-01-2014
;There is a sharp decline in the rate of interest.   ;shiksharthi
;ब्याज की दर में भारी गिरावट आई है.
(defrule decline2
(declare (salience 0))                            ;salience decreased from '5000' to '100'  by 14anu-ban-04 on (21-02-2015)              
(id-root ?id decline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-in_saMbanXI  ?id ?id1)              ;commented by 14anu-ban-04 on (21-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id girAvata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decline.clp 	decline2   "  ?id "  girAvata )" crlf))
)



;"decline","V","1.manA_karanA"
;He might decline to accept the offer.
;--"2.kRINa_ho_jAnA"
;She met with a big accident in the declining years of her life.
;
;"decline","N","1.pawana/kRaya"
;A decline was noticed in the sales of the engine.
;

