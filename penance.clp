;----------------------------DEFAULT RULE------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (21-08-2014)
;They are doing penance for their sins. [Cambridge Advanced Learner's Dictionary]
;apane gunAho kA prAyaSrciwa kara rahe hE. [Own Manual]
;as a penance, she said she would buy them all a box of chocolates. [Cambridge Advanced Learner's Dictionary]
;prAyaSrciwa ke lie usane kahA ki use sabake lie cAkaleta ke dibbe KarIxane hoge. [Own Manual]

(defrule penance0
(declare (salience 0000))
(id-root ?id penance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAyaSrciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  penance.clp 	penance0   "  ?id "  prAyaSrciwa )" crlf))
)

;----------------------------------------------------------------------------------------------------

;NOTE- "Brahma" is not in the file "human.gdbm". So, i already send the request to mam to add it in the list. For now, if you want this rule to work on subject place take a entry from human.gdbm. Sorry for the inconvinence. ;Removed this note by 14anu-ban-09 on (02-11-2014)
;@@@ Added by 14anu-ban-09 on (21-08-2014)
;The hearsay is that at this very place Parvati did the penance to achieve Mahadev . [Tourism Corpus] ;added by 14anu-ban-09 on (04-12-2014)
;जनश्रुति है कि इसी स्थान पर पार्वती ने महादेव को पाने के लिए तपस्या की थी . [Tourism Corpus] ;added by 14anu-ban-09 on (04-12-2014)
;In ancient times the Pitamah Brahma had done difficult penance to see Mother Bhagawati. [Tourism Corpus]
;प्राचीन  काल  में  पितामह  ब्रह्मा  जी  ने  कांची  में  माँ  भगवती  के  दर्शन  के  लिए  दुष्कर  तपस्या  की  थी  ।


(defrule penance1
(declare (salience 3500))
(id-root ?id penance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id1 do)
(id-cat_coarse ?id2 PropN) ;added by 14anu-ban-09 on (02-11-2014)
;(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) ;commented by 14anu-ban-09 on (02-12-2014)
;(and(kriyA-by_saMbanXI ?id1 ?id)(id-root ?id1 achieve)))
;(id-root ?id2 Brahma)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wapasyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  penance.clp 	penance1   "  ?id "  wapasyA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (21-08-2014)
;This is the place where Shakya Kumar had seen the “light” for the first time through hard penance by sitting and in the shade of the pure Bodhi  - tree in 531 B .C . [Tourism Corpus]
;यही  वह  स्थान  है  जहाँ  शाक्य  कुमार  ने  ईसा  पूर्व  531  में  पवित्र  बोधि-वृक्ष  की  छाया  में  बैठकर  कठोर  तपश्चर्या  के  द्वारा  ज्ञान  के  ``  प्रकाश  ``  का  प्रथम  साक्षात्कार  किया  था  ।

(defrule penance2
(declare (salience 3050))
(id-root ?id penance)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 hard)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wapaScaryA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  penance.clp 	penance2   "  ?id "  wapaScaryA )" crlf))
)


;@@@ Added by 14anu-ban-09 on (21-08-2014)
;Located 3600 feet above the sea level Amarkantak has been famous as the penance place of sages and ascetics , abode of Laxmiji and backyard of Uma Maheshwara. [Tourism Corpus]
;समुद्र  से  3600  फुट  की  ऊँचाई  पर  स्थित  अमरकंटक  प्राचीन  काल  से  ही  ऋषि  मुनियों  की  तपस्थली  ,  लक्ष्मी  जी  की  शरण  स्थली  और  उमा  महेश्वर  के  विहार  स्थल  के  रूप  में  प्रसिद्ध  है  ।

(defrule penance3
(declare (salience 3500))
(id-root ?id penance)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) place)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) wapasWalI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " penance.clp  penance3  "  ?id "  " + ?id 1 "  wapasWalI  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (21-08-2014)
;The Bodhi tree is the descendant of that pure tree under which Prince Siddhartha had achieved enlightenment by penance. [Tourism Corpus] 
;बोधिवृक्ष  पीपल  के  उस  पवित्र  वृक्ष  का  वंशज  है  जिसके  नीचे  तप  करके  राजकुमार  सिद्धार्थ  ने  बुद्धत्व  प्राप्त  किया  था  ।

(defrule penance4
(declare (salience 4000))
(id-root ?id penance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(or(and(kriyA-by_saMbanXI ?id1 ?id)(id-root ?id1 achieve))(and(kriyA-object  ?id1 ?id2)(saMjFA-to_kqxanwa  ?id ?id1)(id-root ?id1 achieve)(id-root ?id2 Mahadev))) ;commented by 14anu-ban-09 on (04-12-2014)
(kriyA-by_saMbanXI ?id1 ?id) 	;added by 14anu-ban-09 on (04-12-2014)
(id-root ?id1 achieve) 		;added by 14anu-ban-09 on (04-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wapasyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  penance.clp 	penance4   "  ?id "  wapasyA )" crlf))
)

