;@@@ Added by 14anu-ban-04 (09-04-2015)
;Our handsome hero manages to dispatch another five villains.                   [oald]
;हमारे सुन्दर  हीरो  अन्य पाँच बदमाशों को हराने में सफल हुए है .                                       [self]
(defrule dispatch1
(declare (salience 20))             
(id-root ?id dispatch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id2)
(id-root ?id2 hero)
(kriyA-object ?id ?id1)
(id-root ?id1 villain)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id harA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  dispatch.clp    dispatch1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dispatch.clp 	dispatch1  "  ?id "  harA )" crlf))
)


;@@@ Added by 14anu-ban-04 (09-04-2015)
;He dispatched the guard with one bullet.             [merriam-webster]
;उसने  गोली से पहरेदार को मार डाला .                              [self] 
(defrule dispatch2
(declare (salience 30))           
(id-root ?id dispatch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-object ?id ?id2)
(id-root ?id2  ?str1&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(kriyA-with_saMbanXI ?id ?id3)
(id-root ?id3 bullet)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAra_dAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dispatch.clp 	dispatch2  "  ?id "  mAra_dAlA )" crlf))
)

;@@@ Added by 14anu-ban-04 (09-04-2015)
;He was mentioned in dispatches.                [oald]
;उसका बहादुरी के कार्यों में उल्लेख किया गया था .               [self]
(defrule dispatch4
(declare (salience 20))           
(id-root ?id dispatch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?kri ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-in_saMbanXI ?kri ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahAxurI_ke_kArya))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dispatch.clp 	dispatch4  "  ?id "  bahAxurI_ke_kArya )" crlf))
)

;---------------------------------------------------------DEFAULT RULE -------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (09-04-2015)
;Troops have been dispatched to the area.             [oald]
;दल क्षेत्र में भेजे गये हैं .                                      [self]
(defrule dispatch0
(declare (salience 10))           
(id-root ?id dispatch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Beja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dispatch.clp 	dispatch0  "  ?id "  Beja )" crlf))
)

;@@@ Added by 14anu-ban-04 (09-04-2015)
;More food supplies are ready for immediate dispatch.         [oald]
;अधिक आहार सामग्री तात्कालिक प्रेषण के लिए तैयार हैं .                        [self]
(defrule dispatch3
(declare (salience 10))           
(id-root ?id dispatch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id preRaNa))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dispatch.clp 	dispatch3  "  ?id "  preRaNa )" crlf))
)


