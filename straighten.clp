;@@@ added by 14anu22.
;Straighten your back.
;अपनी पीठ सीधी करो.
(defrule straighten0
(declare (salience 3700))
(id-root ?id straighten)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  straighten.clp 	straighten0   "  ?id "  sIXA_kara )" crlf))
)

;@@@ added by 14anu22.
;Straighten up your hair.
;अपने बाल सीधे करो.
(defrule straighten1
(declare (salience 5100))
(id-root ?id straighten)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) up )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) sIXA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  straighten.clp  straighten1 " ?id " " (+ ?id 1) "  sIXA_kara)" crlf)))


;$$$Modified by 14anu-ban-01 on (12-01-2015)
;@@@ added by 14anu22
;I asked her to straighten out the matter.
;मैने उसे बात समझाने के लिये कहा.
(defrule straighten2
(declare (salience 5100))
(id-root ?id straighten)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) samaJA))  ;corrected "samJA" to "samaJA" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  straighten.clp  straighten2 " ?id " " (+ ?id 1) "  samaJA)" crlf)))          ;corrected "samJA" to "samaJA" by 14anu-ban-01 on (12-01-2015)
