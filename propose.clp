;--------------------DEFAULT RULE------------------------------------------

;@@@ Added by 14anu-ban-09 on (28-11-2014)
;The senator proposed to abolish the sales tax. [Hinkhoj]
;आरोग्यकर ने बिक्री कर को बन्द करने के लिए प्रस्ताव रखा. [Self]

(defrule propose0
(id-root ?id propose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAva_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  propose.clp 	propose0  "  ?id "  praswAva_raKa )" crlf))
)

;----------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (28-11-2014)
;In 1906, he proposed a classic experiment of scattering of these α-particles by atoms to investigate the atomic structure.	[NCERT CORPUS]	;added by 14anu-ban-09 on (26-02-2015)
;परमाणु की संरचना का अन्वेषण करने के लिए उन्होंने सन् 1906 में परमाणुओं द्वारा ऐल्फा-कणों के प्रकीर्णन से सम्बन्धित एक क्लासिकी प्रयोग प्रस्तावित किया.	[NCERT CORPUS]			;added by 14anu-ban-09 on (26-02-2015)
;The Austrian physicist Johann Christian Doppler first proposed the effect in 1842. [NCERT CORPUS]
;AstriyA ke BOwikavixa johAna kriSciyana doYplara ne sarvapraWama san 1842 I. meM isa praBAva ko praswAviwa kiyA . [NCERT CORPUS]

(defrule propose1
(declare (salience 1000))
(id-root ?id propose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 effect|experiment)	;added 'experiment' by 14anu-ban-09 on (26-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAviwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  propose.clp 	propose1  "  ?id "  praswAviwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (28-11-2014)
;You may perhaps have heard of this model which was proposed by the Danish physicist Niels Bohr in 1911 and was a stepping stone to a new kind of mechanics, namely, quantum mechanics. [NCERT CORPUS]
;kaxAciwa Apane isa moYdala ke bAre meM sunA hogA ; jise denamArka ke BOwika vijFAnI nIla bora ne san 1911 meM praswAviwa kiyA WA Ora jo nae prakAra kI yAnwrikI jise kvANtama yAnwrikI kahawe hEM, ke lie mIla kA eka pawWara WA. [NCERT CORPUS]

(defrule propose2
(declare (salience 1000))
(id-root ?id propose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAviwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  propose.clp 	propose2  "  ?id "  praswAviwa_kara )" crlf))
)

