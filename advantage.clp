;@@@ Added by 14anu-ban-11 on (13-11-2014)
;For example, whenever we tune our radio to a favorite station, we are taking advantage of a special property of ac circuits â€” one of many that you will study in this chapter.(NCERT) 
;उदाहरणार्थ, जब हम अपने रेडियो को अपने मनपसन्द स्टेशन से समस्वरित करते हैं तो ac परिपथों के एक विशिष्ट गुण का लाभ उठाते हैं जो उन अनेक गुणों में से एक है जिनका अध्ययन आप इस अध्याय में करेंगे.(NCERT)
(defrule advantage2
(declare (salience 5100))
(id-root ?id advantage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 property)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lABa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  advantage.clp 	advantage2   "  ?id "  lABa)" crlf))
)

;--------------------------- Default rules -------------------------
(defrule advantage0
(declare (salience 5000))
(id-root ?id advantage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PAyaxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  advantage.clp 	advantage0   "  ?id "  PAyaxA )" crlf))
)

;"advantage","N","1.PAyaxA"
;Raju took full advantage of shopping.
;--"2.baDZawa"
;Her knowledge of several languages gave her an advantage over her competitors.
;--"3.BalAI"
;Is there any advantage in our following the traditional festivals in this 21st century.
;--"4.edavAMteja{tEnisa_ke_kela_meM_dyusa_uparAMwa_arjiwa_pahalA_pvAiMta}"
;Mahesh Bhupati reached advantage point quite easily.
;--"5.pUrA_lABa_uTAnA"
;A basketball player should take advantage of his height.
;--"6.galawa_PAyaxA_uTAnA"
;He took advantage of her trust by keeping her in the dark about the deal.


;$$$ Modified by Shirisha Manju 19-12-2014 Suggested by Chaitanya Sir
;changed meaning 'pUrA_PAyaxA_uTA' as 'lABaxAyaka_sWiwi_meM_pahuzcA'
;"advantage","V","1.pUrA_PAyaxA_uTAnA"
;He advantaged his classmates by solving their problems. 
;usane apane sahapAtiyoM ko unakI samasyA hala karake lABaxAyaka sWiwi meM pahuzcAyA(pahuzcA_xiyA). 
(defrule advantage1
(declare (salience 4900))
(id-root ?id advantage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lABaxAyaka_sWiwi_meM_pahuzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  advantage.clp 	advantage1   "  ?id "  lABaxAyaka_sWiwi_meM_pahuzcA )" crlf))
)

;
;LEVEL 
;
;
;"advantage","N","1.baDZawa"<--lABa
;Her knowledge of several languages gave her an advantage over her competitors.
;usakI bahuwa sI BARAoM ke jFAna se usane apane prawixvanxvI para baDZawa hAsila kara lI. 
;--"2.PAyaxA"<--lABa
;Raju took full advantage of free staying in the guest house.
;rAjU ne niHSulka gesta hAusa meM Taharane kA pUrA PAyaxA uTAyA.
;--"3.lABa"<--lABa
;Is there any advantage in our traditional festivals in this 21st century.
;ina pAramparika wyohAroM kA isa ikkIsavIM saxI meM kyA koI lABa hE?
;
;--"3.edavAMteja{tEnisa_ke_Kela_meM_dyusa_uparAMwa_arjiwa_pahalA_pvAiMta}"<--lABa
;Mahesh Bhupati reached advantage point quite easily.
;maheSa BUpawi ko bahuwa AsAnI se dyusa pvAiMta kA lABa milA.
;
;--"4.pUrA_lABa_uTAnA"<--lABa
;A basketball player should take advantage of his height.
;eka bAsketabAla KilAdZI ko apanI lambAI kA pUrA lABa uTAnA cAhiye.
;
;--"5.anuciwa_PAyaxA_uTAnA"<--lABa
;He took advantage of her trust by keeping her in the dark.
;usane usake viSvAsa kA (anuciwa) lABa uTAyA,use azXere meM raKa kara.
;
;"advantage","V","1.pUrA_PAyaxA_uTAnA"<--lABa
;He took full advantage of her friendship. 
;usane usakI xoswI kA pUrA PAyaxA uTAyA.
;
;       nota:--uparyukwa 'advantage'ke saBI vAkyoM ko xeKane para    
;           hama isa niRkarRa para pahuzca sakawe hEM ki isa Sabxa kA arWa
;          kevala eka hI arWa se nikAlA jA sakawA hE awaH isake liye
;          sUwra nimna xe sakawe hEM
;                  
;sUwra : lABa
; 
;
