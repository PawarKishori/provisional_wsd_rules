;Added by Shirisha Manju Suggested by Chaitanya Sir (23-09-13)
;In all respects, the displacement current has the same physical effects as the conduction current.
;
(defrule respect00
(declare (salience 5000))
(id-root ?id respect)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id respects)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aMSoM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  respect.clp    respect00   "  ?id "  aMSoM )" crlf))
)
	

;"respecting","Prep","1.ke_bAre_meM"
;Do you have any information respecting the child's whereabouts?
(defrule respect0
(declare (salience 5000))
(id-root ?id respect)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id respecting )
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ke_bAre_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  respect.clp  	respect0   "  ?id "  ke_bAre_meM )" crlf))
)


;@@@ Added by 14anu-ban-10 on (14-10-2014)
;Though it is out of the scope of this book, but you can check that a sphere is better than at least a cube in this respect! [ncert corpus]
;hama isa waWya ko isa puswaka meM sawyApiwa nahIM kara sakawe paranwu Apa svayaM yaha jAzca kara sakawe hEM ki isa saMxarBa meM golA kama se kama eka Gana kI wulanA meM behawara hE.[ncert corpus]
(defrule respect4
(declare (salience 5400))
(id-root ?id respect)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wulanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  respect.clp 	respect4   "  ?id "  wulanA )" crlf))
)

;@@@Added by 14anu-ban-10 on (25-11-2014)
;This means that the mass of the body does not change, the body remains rigid and also the axis does not change its position with respect to the body. [ncert corpus]
;yAni ki piNda kA xravyamAna sWira rahawA hE waWA piNda xqDa banA rahawA hE Ora isake sApekRa GUrNana akRa kI sWiwi nahIM baxalawI.[ncert corpus] 
(defrule respect5
(declare (salience 5500));salience increased
(id-root ?id respect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) with)
(id-root =(+ ?id 1) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (+ ?id 1) wulanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " respect.clp	 respect5  "  ?id "  " (- ?id 1)(+ ?id 1) "  wulanA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (13-03-2015)
;With respect to your letter.[hinkhoj]
;आपका पत्र के संदर्भ मै . [manual]
(defrule respect6
(declare (salience 5600))
(id-root ?id respect)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) with)
(id-word =(+ ?id 1) to)
(viSeRya-to_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1)(+ ?id 1) saMxarBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " respect.clp  respect6  "  ?id "  "(- ?id 1)(+ ?id 1) "  saMxarBa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (13-03-2015)
;I respect you for your honesty.[hinkhoj]
;मैं आपकी ईमानदारी के लिए आपकी प्रशंसा करता हूँ . [manual]
(defrule respect7
(declare (salience 5700))
(id-root ?id respect)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  praSaMsA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  respect.clp 	respect7  "  ?id "   praSaMsA_kara)" crlf))
)

;------------------------ Default Rules ----------------------

;"respect","VT","1.sammAna_karanA"
;We must respect our elders.
(defrule respect2
(declare (salience 4800))
(id-root ?id respect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  respect.clp 	respect2   "  ?id "  sammAna_kara )" crlf))
)

;"respect","N","1.sammAna"
;Have some respect for your teachers.
(defrule respect1
(declare (salience 4900))
(id-root ?id respect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  respect.clp 	respect1   "  ?id "  sammAna )" crlf))
)

;"respect","N","1.sammAna"
;Have some respect for your teachers.
;--"2.KyAla"
;A good journalist must have respect for the needs of the reader. 
;--"3.saMbaMXa_yA_saMxarBa"
;With respect to your  letter.
;Your article is praiseworthy in respect of style.
;--"4.kuCa_aMSa_waka"
;In some respect your article is better than mine.
;
;"respect","VT","1.sammAna_karanA"
;We must respect our elders.
;--"2.praSaMsA_karanA"
;I respect you for your honesty.
;--"3.AwmasammAna_raKanA"
;If you don't respect your self,how can you expect others to respect you.


