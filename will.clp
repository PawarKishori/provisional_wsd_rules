
(defrule will0
(declare (salience 5000))
(id-root ?id will)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id willing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id uxyawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  will.clp  	will0   "  ?id "  uxyawa )" crlf))
)

;"willing","Adj","1.uxyawa"
;The carpenter is willing to do the work.
;

;Commented by Meena(1.11.10)
;(defrule will2
;(declare (salience 4800))
;(id-root ?id will)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id gA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  will.clp 	will2   "  ?id "  gA )" crlf))
;)

; more frequently 'will' will occur as an auxiliary. However, it is necessary to list instances where 'will' will have the meaning of 'cAhanA'.
;default_sense && category=verb	cAha	0
;"will","VTI","1.cAhanA"
;He willed && every thing went accordingly.
;
;


;@@@ Added by 14anu-ban-11 on (02-02-2015)
;His will was executed by his lawyers in 2008.(oald)
;2008 में उसकी   वसीयत उसके वकीलों के द्वारा वैध की गयी थी (self)
(defrule will3
(declare (salience 4920))
(id-root ?id will)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 execute)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vasIyawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  will.clp 	will3   "  ?id "  vasIyawa )" crlf))
)


;@@@ Added by 14anu-ban-11 on (02-02-2015)
;It is the will of God.(hinkhoj)
;यह ईश्वर की अभिलाषा है . (anusaaraka)
(defrule will4
(declare (salience 4910))
(id-root ?id will)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBilARA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  will.clp 	will4   "  ?id "  aBilARA )" crlf))
)

;@@@ Added by 14anu-ban-11 on (02-02-2015)
;The ascetic showed great strength of will.(hinkhoj)
;तपस्वी ने आत्म संयम की बडी शक्ति दिखाई . (anusaaraka)
(defrule will5
(declare (salience 4930))
(id-root ?id will)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 strength)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Awma_saMyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  will.clp 	will5   "  ?id "  Awma_saMyama )" crlf))
)

;@@@ Added by 14anu-ban-11 on (02-02-2015)
;I don't want to go against your will.(oald)
;मैं आपकी इच्छा के विरुद्ध  नहीं  जाना चाहता हूँ .(self) 
(defrule will6
(declare (salience 4940))
(id-root ?id will)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-against_saMbanXI  ?id1 ?id)
(id-root ?id1 go)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id icCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  will.clp 	will6   "  ?id "  icCA )" crlf))
)

;------------------------- Default Rules --------------------

(defrule will1
(declare (salience 4900))
(id-root ?id will)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xqDZa_icCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  will.clp 	will1   "  ?id "  xqDZa_icCA )" crlf))
)

