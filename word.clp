
(defrule word0
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id wording )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id SabxoM_meM_prakata_karanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  word.clp  	word0   "  ?id "  SabxoM_meM_prakata_karanA )" crlf))
)

;"wording","N","1.SabxoM_meM_prakata_karanA"
;The wording in the song was not clear.
;
(defrule word1
(declare (salience 4900))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sabxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  word.clp 	word1   "  ?id "  Sabxa )" crlf))
)

;"word","N","1.Sabxa"
;Write the essay in your own words.
;--"2.kaWana"
;Great people give words of wisdom.
;--"3.vacana"
;I give you my word that this will not happen again.
;
(defrule word2
(declare (salience 4800))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SabxoM_meM_prakata_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  word.clp 	word2   "  ?id "  SabxoM_meM_prakata_kara )" crlf))
)

;"word","VT","1.SabxoM_meM_prakata_karanA"
;I was unable to word my feelings at the Ritu's husband's death.
;

;$$$ Modified by 14anu-ban-11 (30-08-2014)
;@@@ Added by 14anu02 on 25.06.14
;I give you my word that this will not happen again.(old)
;मैं आपको मेरा वचन देता हूँ कि यह फिर से ना हो . 
(defrule word3
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object_2 ?id1 ?id) ;Added by 14anu-ban-11
 ;(viSeRya-RaRTI_viSeRaNa ?id ?) ;commented by 14anu-ban-11
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vacana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  word.clp 	word3   "  ?id "  vacana )" crlf))
)

;@@@ Added by 14anu02 on 25.06.14
;She sent word that she would be late.(old)
;उसने सन्देश भेजा कि वह देर से आने वाली है.
(defrule word5
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 spread|send)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMxeSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  word.clp 	word5   "  ?id " saMxeSa )" crlf))
)

;@@@ Added by 14anu02 on 25.06.14
;There's been no word from them since Christmas.(old-modified)
;क्रिस्मस से उनसे कोई समाचार प्राप्त नही हुआ.
(defrule word6
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 1) from)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) samAcAra_prApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " word.clp	word6  "  ?id "  " (+ ?id 1) " samAcAra_prApwa   )" crlf))
)

;@@@ Added by 14anu02 on 25.06.14
;The word is they've split up.(old)
;खबर है की वे अलग हो चुके है.
(defrule word7
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) the)
(id-root =(+ ?id 1) be)  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KZabara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  word.clp 	word7   "  ?id " KZabara )" crlf))
)


;@@@ Added by 14anu02 on 25.06.14
;If word gets out about the affair, he will have to resign.(old)
;यदि प्रेम सम्बंध के बारे मे अफवाह  फैल जाती है तो उसे त्याग पत्र देना पड़ेगा.
(defrule word8
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)
(id-root ?id1 get)
(id-root =(+ ?id1 1) around|out)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aPZavAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  word.clp 	word8   "  ?id " aPZavAha )" crlf))
)

;@@@ Added by 14anu02 on 30.06.14
;She got the information by word of mouth.(thefreedictionary.com)
;उसने सुन कर सूचना प्राप्त की . 
(defrule word9
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1) by)
(id-word =(+ ?id 1) of)
(id-root =(+ ?id 2) mouth)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (+ ?id 1) (+ ?id 2) suna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " word.clp   word9  "  ?id "  " (- ?id 1) " " (+ ?id 1) " " (+ ?id 2) " suna_kara  )" crlf))
)

;@@@ Added by 14anu02 on 30.06.14
;In a word, I don't like him. (thefreedictionary.com)
;सङ्क्षेप में, मैं उसको पसन्द नहीं करता हूँ . 
(defrule word10
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 2) in)
(id-word =(- ?id 1) a)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1)  saMkRepa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " word.clp   word10  "  ?id "  " (- ?id 1) "  saMkRepa  )" crlf))
)

;@@@ Added by 14anu02 on 30.06.14
;That's precisely what he told me, word for word.(thefreedictionary.com)
;वह ठीक वही है जो उसने मुझे बताया, शब्दशः .
(defrule word11
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 1) for)
(id-word =(+ ?id 2) word)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) SabZxaSa:))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " word.clp   word11  "  ?id "  " (+ ?id 1) " " (+ ?id 2) "  SabZxaSa:  )" crlf))
)

;@@@ Added by 14anu02 on 30.06.14
;When Mary starts talking, no one else can get a word in edgeways. (old)
;जब मैरी बातचीत करना शुरु करती है, अन्य कोई बातचीत में बीच में टपक नहीं सकता. (meaning taken from 'thefreedictionary.com')
(defrule word12
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 2) get)
(id-root =(- ?id 1) a)
(id-root =(+ ?id 1) in)
(id-root =(+ ?id 2) edgeways)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 2) (- ?id 1) (+ ?id 1) (+ ?id 2) bAwacIwa_meM_bIca_meM_tapaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " word.clp   word12  "  ?id "  " (- ?id 2) " " (- ?id 1) " " (+ ?id 1) " " (+ ?id 2) "  bAwacIwa_meM_bIca_meM_tapaka  )" crlf))
)


;$$$ Modified by 14anu-ban-11 on (30-08-2014)
;Note:-Changed the meaning from "bAwacIwa_kara" to "bAwa_kara".  
;@@@ Added by 14anu02 on 1.07.14
;Have a word with Pat and see what she thinks.
;पैट के साथ बातचीत कीजिए और देखिए वह क्या सोचती है . 
(defrule word13
(declare (salience 5000))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa  ?id ?id1)
(id-word ?id1 a)
(kriyA-object  ?id2 ?id)
(id-root ?id2 have)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id2 ?id1 ?id bAwa_kara));Modified the meaning  from "bAwacIwa_kara" to "bAwa_kara" by 14anu-ban-11
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " word.clp   word13  " ?id2 " "  ?id1 "  " ?id "  bAwa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-11 on (08-12-2014)
;She twisted my words.(twisted.clp)
;उसने मेरे शब्दों का अर्थ विकृत किया . (twisted.clp)
(defrule word14
(declare (salience 5100))
(id-root ?id word)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 twist)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sabxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  word.clp 	word14   "  ?id "  Sabxa )" crlf))
)
