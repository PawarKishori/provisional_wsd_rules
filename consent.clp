;@@@ Added by 14anu-ban-03 (15-04-2015)
;She was chosen as leader by common consent.  [oald]
;वह सार्वजनिक सहमति के द्वारा नेता के रूप में चुनी गयी थी . [manual]
(defrule consent2
(declare (salience 5000))
(id-root ?id consent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahamawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consent.clp 	consent2   "  ?id "  sahamawi )" crlf))
)


;@@@ Added by 14anu-ban-03 (15-04-2015)
;He reluctantly consented to his daughter's marriage. [oald]
;उसने अनिच्छा से अपनी बेटी की शादी को स्वीकृती दी. [manual]
(defrule consent3
(declare (salience 5000))   
(id-root ?id consent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 marriage)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svIkqwI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consent.clp 	consent3   "  ?id "  svIkqwI_xe )" crlf))
)

;@@@ Added by 14anu-ban-03 (15-04-2015)
;She finally consented to answer our questions. [oald]
;वह अन्ततः हमारे प्रश्नों के उत्तर देने के लिए राजी हुई . [manual]
(defrule consent4
(declare (salience 5000))   
(id-root ?id consent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 finally)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAjI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consent.clp 	consent4   "  ?id "  rAjI_ho )" crlf))
)

;-------------------------------------------------default rules----------------------------------

(defrule consent0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (15-04-2015)
(id-root ?id consent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consent.clp 	consent0   "  ?id "  anumawi )" crlf))
)

(defrule consent1
(declare (salience 00))    ;salience reduced by 14anu-ban-03 (15-04-2015)
(id-root ?id consent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consent.clp 	consent1   "  ?id "  mAna )" crlf))
)

;"consent","VT","1.mAnanA"
;My parents consented with my desire to marry the man of my choice.
;
;
