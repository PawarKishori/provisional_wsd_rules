;@@@Added by 14anu-ban-02(20-03-2015)
;His murder was an outrageous and barbarous act.[cald]
;उसका खून चौंका देनेवाला और बर्बर कृत्य था . [self]
(defrule barbarous1 
(declare (salience 100)) 
(id-root ?id barbarous) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 act)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id barbara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barbarous.clp  barbarous1  "  ?id "  barbara )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(20-03-2015)
;Sentence: How can they forgive such barbarous behaviour?[cald]	;run the sentence on parser no. 3
;Translation: वे ऐसा असभ्य व्यवहार कैसे माफ कर सकते हैं?  [self]
(defrule barbarous0 
(declare (salience 0)) 
(id-root ?id barbarous) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id asaBya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barbarous.clp  barbarous0  "  ?id "  asaBya )" crlf)) 
) 
