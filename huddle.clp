
;"huddled","Adj","1.CallexAra"
;eka BiKArI'huddled'peda ke nIce so rahA hE
(defrule huddle0
(declare (salience 5000))
(id-root ?id huddle)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id huddled )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id CallexAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  huddle.clp  	huddle0   "  ?id "  CallexAra )" crlf))
)

(defrule huddle2
(declare (salience 4800))
(id-root ?id huddle)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 together)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 simata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " huddle.clp	huddle2  "  ?id "  " ?id1 "  simata_jA  )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$ Modified by 14anu-ban-06 (24-02-2015)
;It was so cold that we huddled together for warmth.(cambridge)
;इतनी ठण्ड थी कि हम गर्माहट के लिए एक साथ सिमट गये . (manual)
(defrule huddle1
(declare (salience 2000));decreased salience from '4900' by 14anu-ban-06 (24-02-2015)
(id-root ?id huddle)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 together);commented by 14anu-ban-06 (24-02-2015)
;(kriyA-together_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. ;commented by 14anu-ban-06 (24-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id simata_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " huddle.clp huddle1 " ?id "  simata_jA )" crlf)) 
)

;$$$ Modified by 14anu-ban-06 (24-02-2015)
;A small group of people stood in a huddle at the bus stop.(cambridge)
;लोगों का एक छोटा समूह बस अड्डे पर झुण्ड में खडा हुआ था. (manual)
;"huddle","N","1.BIdZa lagAnA"
;newA ko xeKane ke lie vahAz 'huddle' hE.
(defrule huddle3
(declare (salience 4700))
(id-root ?id huddle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JuNda));meaning changed from 'BIdZa_lagAnA' to 'JuNda' by 14anu-ban-06 (24-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  huddle.clp 	huddle3   "  ?id "  JuNda )" crlf))
)

