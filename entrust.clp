;@@@ Added by 14anu-ban-04 (03-03-2015)
;He entrusted his nephew with the task.                 [oald]
;उसने काम के साथ साथ  अपने भतीजे को भी सँभाला .                          [self]
(defrule entrust1
(declare (salience 20))
(id-root ?id entrust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) 
(kriyA-with_saMbanXI ?id ?id2) 
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko_BI)) 
(assert (id-wsd_root_mng ?id sazBAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  entrust.clp     entrust1  "  ?id " ko_BI  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  entrust.clp 	entrust1   "  ?id "  sazBAla )" crlf))
)

;----------------------------------------------------------DEFAULT RULE--------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (03-03-2015)
;He entrusted the task to his nephew.                       [oald]           
;उसने अपने  भतीजे को काम सौंप दिया .                                 [self]
(defrule entrust0
(declare (salience 10))
(id-root ?id entrust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sOMpa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  entrust.clp 	entrust0   "  ?id "  sOMpa_xe )" crlf))
)
