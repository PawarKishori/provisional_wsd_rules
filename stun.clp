;@@@ Added by  14anu-ban-01 on (05-02-2015)
;The animals are stunned before slaughter.[oald]
;पशु हत्या से पहले अचेत/बेहोश किए जाते हैं . [self]
(defrule stun0
(declare (salience 0))
(id-root ?id stun)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acewa_kara/behoSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stun.clp 	stun0   "  ?id "  acewa_kara/behoSa_kara )" crlf))
)

;@@@ Added by  14anu-ban-01 on (05-02-2015)
;She was stunned by the amount of support she received from well-wishers.[cald]
;शुभचिंतकों से मिले प्रोत्साहन से वह  चकित हुई/भौंचक्की रह गयी .[self] 
(defrule stun1
(declare (salience 1000))
(id-root ?id stun)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakiwa_ho/BOMcakkA_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stun.clp 	stun1   "  ?id "  cakiwa_ho/BOMcakkA_raha )" crlf))
)

;@@@ Added by  14anu-ban-01 on (05-02-2015)
;News of the disaster stunned people throughout the world.[cald]
;तबाही के समाचार ने विश्व भर में लोगों को स्तंभित कर दिया . [self]
(defrule stun2
(declare (salience 1000))
(id-root ?id stun)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 news)
(kriyA-subject  ?id ?id1)
(id-root ?id2 disaster|drought|flood|murder|violence|death|crime)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swaMBiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stun.clp 	stun2   "  ?id "  swaMBiwa_kara )" crlf))
)

;@@@ Added by  14anu-ban-01 on (05-02-2015)
;This rule will run when 'rhinoceros' will be added as a new entry in animate.txt.[mail sent to manju mam]
;This injection stuns the rhinoceros, so we can examine it.[cald]
;यह इंजेक्शन गेंडे को अचेत कर देता है ताकि  हम उसकी जाँच कर सकें.[self:more natural]
;यह इंजेक्शन गेंडे को अचेत कर देता है,तो हम उसकी जाँच कर सकते हैं.[self:more faithful]
(defrule stun3
(declare (salience 100))
(id-root ?id stun)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acewa_kara_xe))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stun.clp 	stun3  "  ?id "  acewa_kara_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* " stun.clp 	stun3  "  ?id " ko )" crlf)
)
)
