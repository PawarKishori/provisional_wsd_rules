;@@@ Added by 14anu-ban-06 (31-03-2015)
;He's a good teacher, but inclined to be a bit impatient with slow learners. (cambridge)[parser no.- 2]
;वह एक अच्छा शिक्षक है परन्तु कमजोर शिक्षार्थियों से थोडा सा परेशान रहता हैं. (manual)
(defrule impatient1
(declare (salience 2000))
(id-root ?id impatient)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-with_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impatient.clp 	impatient1   "  ?id "  pareSAna )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (31-03-2015)
;People are increasingly impatient for change in this country. (cambridge)
;लोग इस देश में बदलाव के लिए बहुत ज्यादा उत्सुक हैं . (manual)
(defrule impatient0
(declare (salience 0))
(id-root ?id impatient)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsuka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impatient.clp 	impatient0   "  ?id "  uwsuka )" crlf))
)
