
(defrule queue0
(declare (salience 5000))
(id-root ?id queue)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAina_lagA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " queue.clp queue0 " ?id "  lAina_lagA )" crlf)) 
)

(defrule queue1
(declare (salience 4900))
(id-root ?id queue)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lAina_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " queue.clp	queue1  "  ?id "  " ?id1 "  lAina_lagA  )" crlf))
)

(defrule queue2
(declare (salience 4800))
(id-root ?id queue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAina_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  queue.clp 	queue2   "  ?id "  lAina_lagA )" crlf))
)

(defrule queue3
(declare (salience 4700))
(id-root ?id queue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  queue.clp 	queue3   "  ?id "  lAina )" crlf))
)

;"queue","N","1.lAina"
;Children stand in a queue to attend the prayer.

;@@@ Added by 14anu-ban-11 on (25/8/14)
;The circular path made for queues was even enhancing our curiosity of visiting Baba. 
;कतारों  हेतु  बनाया  गया  चक्करदार  रास्ता  हमारी  बाबा  के  दर्शनों  की  उत्सुकता  को  ओर  अधिक  बढ़ा  रहा  था  ।
(defrule queue4
(declare (salience 4800))
(id-root ?id queue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-viSeRaNa ?id1 ?id2)(viSeRya-of_saMbanXI ?id1 ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kawAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  queue.clp 	queue4   "  ?id "  kawAra )" crlf))
)
