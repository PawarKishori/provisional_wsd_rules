;@@@ Added by 14anu-ban-03 (31-03-2015)
;The director’s brilliant conceit was to film this tale in black and white. [oald]
;निर्देशक  की प्रतिभाशाली सोच थी इस कहानी को ब्लैक एण्ड वाइट में  फिल्माना . [manual]
(defrule conceit1
(declare (salience 10))
(id-root ?id conceit)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id soca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conceit.clp 	conceit1   "  ?id "  soca )" crlf))
)


;@@@ Added by 14anu-ban-03 (31-03-2015)
;The idea of the wind singing is a romantic conceit. [oald]
;हवा गायन का विचार एक कल्पित बुद्धिमानी है . [manual]
(defrule conceit2
(declare (salience 10))
(id-root ?id conceit)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 idea)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buxXimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conceit.clp 	conceit2  "  ?id "  buxXimAna )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (31-03-2015)
;I can say without conceit that I have talent. [oald]
;मैं घमण्ड के बिना कह सकता हूँ कि मुझमें प्रतिभा है . [manual]
(defrule conceit0
(declare (salience 00))
(id-root ?id conceit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GamaMda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conceit.clp  conceit0   "  ?id "  GamaMda )" crlf))
)

