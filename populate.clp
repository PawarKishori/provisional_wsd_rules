;-----------------------DEFAULT RULE-----------------------------------------------------
;@@@ Added by 14anu-ban-09 on (24-02-2015)
;The settlers began to move inland and populate the river valleys.	[oald]
;अधिवासियों ने देश के अन्दर बढ़ना और नदी घाटी में बसना शुरू किया.			[self]


(defrule populate0
(declare (salience 000))
(id-root ?id populate)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id verb_base_form)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id basa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  populate.clp 	populate0   "  ?id "  basa )" crlf))
)

;----------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (24-02-2015)
;There are densely populated areas in all directions around Kaziranga National Park . 	[Total_tourism]
;काजीरंगा राष़्ट्रीय उद्यान में चारों ओर घनी आबादी के क्षेत्र हैं .	[Total_tourism]
;काजीरंगा राष़्ट्रीय उद्यान में चारों ओर घनी आबादी वाला क्षेत्र हैं .	[Self]

(defrule populate1
(declare (salience 1000))
(id-root ?id populate)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id verb_past_participle)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AbAxI_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  populate.clp 	populate1   "  ?id " AbAxI_vAlA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (24-02-2015)
;The government wanted to populate the remote area of the country.  	[http://www.cfilt.iitb.ac.in]
;सरकार ने देश के दूरवर्ती क्षेत्र को बसाना चाहा . 	[Self]

(defrule populate2
(declare (salience 1000))
(id-root ?id populate)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id verb_base_form)
(kriyA-object  ?id ?id1)
(id-root ?id1 land|area)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id basA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  populate.clp 	populate2   "  ?id " basA )" crlf))
)

