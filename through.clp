;$$$Modified by 14anu-ban-07,(17-01-2015)
;The thought suddenly flashed through my mind that she did not want to be here.(cambridge)
;यह विचार अचानक मेरे मन में अाया कि वह यहाँ रहना  नहीं चाहती थी.(manual)
;$$$ Modified by Anita--04-08-2014
;A thread of self-pity runs through his autobiography. [By mail] ; sentence added by Anita-4.8.2014
;आत्मदया का सूत्र उसकी आत्मकथा में समाया हुआ है ।

;;Added by Meena(1999999999.10.09)
;Fanned by a strong wind, the fire spread rapidly through the city.
;तेज़ हवा से भड़की हुई आग पूरे शहर में फैल गई ।;
(defrule through0
(declare (salience 5500))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI  ?id1 ?id2)
(id-root ?id2 city|jungle|forest|autobiography|mind) ;added autobiography by Anita ;added 'mind' by 14anu-ban-07,(17-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp   through0   "  ?id "  meM )" crlf))
)



;Added by Meena(13.10.09)
;It struggled to force its body through that little hole . 
(defrule through1
(declare (salience 5000))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp   through1   "  ?id "  meM_se )" crlf))
)


;$$$ Modified by Bhagyashri Kulkarni (4-11-2016)
;The treatment of cataract is possible through surgery only. (health)
;मोतियाबिंद का इलाज सिर्फ शल्य चिकित्सा के माध्यम से सम्भव है . 
;AIDS does not spread through touching or mutual interaction. (health)
;एड्स स्पर्श या पारस्परिक व्यवहार से नहीं फैलता है .
;Camping is done through government, non-government and voluntary organizations after comprehensive propagation by District Blindness Control Committees. (health)
;जिला अन्धता निवारण समितियों द्वारा व्यापक प्रचार प्रसार कर सरकारी , गैरसरकारी , निजी एवं स्वैच्छिक संस्थाओं के माध्यम से शिविर लगाये जाते हैं .
;$$$Modified by 14anu-ban-07,(21-01-2015)
;### [COUNTER EXAMPLE] ### We galloped through the woods.(cambridge)
;### [COUNTER EXAMPLE] ### हम जंगल में से तेजी से निकले.
;$$$ Modified by 14anu19 (17-06-2014)
;Through trade and travel she had established channels of communication with the countries of Asia .
; व्यापार और यात्रा के मे से उसने एशिया के देशों के साथ सूचना के माध्यम स्थापित किए थे (before modification)
; व्यापार और यात्रा के माध्यम से उसने एशिया के देशों के साथ सूचना के माध्यम स्थापित किए थे (after modification)
(defrule through3
(declare (salience 5100)) ;salience increased from 5000 by by 14anu-ban-07,(21-01-2015) because of through1 rule
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(or(kriyA-through_saMbanXI  ?id1 ?id2)(viSeRya-through_saMbanXI  ?id1 ?id2))   	;"kriyA-through_saMbanXI" realtion is added ;Added 'viSeRya-through_saMbanXI' by Bhagyashri
(id-root ?id2 trade|travel|organization|surgery|interaction) 		;added by 14anu-ban-07,(21-01-2015) ; Added 'organization|surgery|interaction' by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_mAXyama_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through3   "  ?id "  ke_mAXyama_se )" crlf))
)

;modified by Sukhada. Ex. We wish to invite participants from all across the world to participate through this site
;"through","Prep","1.meM_se"
;The thief entered the house through the window.
;--"2.se_waka"
;You stay in Birmingham Monday through Friday.
;--"3.ke_xvArA"
;You can get good results through hard work.
;--"4.Suru_se_anwa_waka"
;The children sat through the long concert.
;--"5.ke_kAraNa"
;The accident ocurred through his careless driving.
;

;$$$ Modified by 14anu07
;;@@@   ---Added by Prachi Rathore
;It has been [through] a nuclear furnace several billion degrees hot![gyannidhi]
;यह एक नाभिकीय भट्ठी से होकर गुजरी है जिसका तापमान कई अरब डिग्री था।
;counter example-It is better to talk to Pakistan directly rather than through their proxies. 
;पाकिस्तान से उसकी पुतलियों के माध्यम से  बातचीत करने की बजाए सीधे बात करना बेहतर है .
(defrule through4
(declare (salience 5000))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) been|went);Added by 14anu07
(id-cat_coarse ?id preposition)
(viSeRya-through_saMbanXI  ?id2 ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hokara_gujara))
(assert (kriyA_id-subject_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through4   "  ?id "  hokara_gujara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  through.clp     through4   "  ?id "  se )" crlf))
)


;;@@@   ---Added by Prachi Rathore
;The song changes key halfway through. [cambridge]
; गाना  बीच मे सुर बदलता है .  
(defrule through5
(declare (salience 5000))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through5   "  ?id "  meM )" crlf))
)

;@@@  Added by Prachi Rathore
;A curious and puzzling feature about galaxies is that, whether singly or in clusters, they seem to contain a lot more say, ten times more unseen matter in and around them than is visible through stars, gas and dust.[gyannidhi]
;आकाशगंगाओं के बारे में हैरान कर देने वाली एक विशेषता है कि वे अकेली हों या समूह उनके बीच और आसपास अदृश्य पदार्थ की मात्रा उनसे काफी अधिक 10 गुना कह सकते हैं होती है, जो तारों में दिखायी देती है-गैस और धूल।
(defrule through6
(declare (salience 5100))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(viSeRya-through_saMbanXI  ?id2 ?id1)
(id-root ?id2 visible)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through6   "  ?id "  meM )" crlf))
)

;@@@ Added by Prachi Rathore[20-1-14]
;It is not possible to infect another person through kissing.[oald]
;किसी और व्यक्ति को चुम्बन के द्वारा संक्रमित करना सम्भव नहीं है . 
(defrule through7
(declare (salience 5100))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI  ?id1 ?)
(kriyA-object  ?id1 ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_xvArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp   through7   "  ?id "  ke_xvArA )" crlf))
)

;@@@ Added by Prachi Rathore[3-3-14]
;When you hold a pencil in front of you against some specific point on the background a wall and look at the pencil first through your left eye A closing the right eye and then look at the pencil through your right eye B closing the left eye you would notice that the position of the pencil seems to change with respect to the point on the wall. [ncert]
;जब आप किसी पेंसिल को अपने सामने पकडते हैं और पृष्ठभूमि (माना दीवार) के किसी विशिष्ट बिन्दु के सापेक्ष पेंसिल को पहले अपनी बायीं आँख A से (दायीं आँख बन्द रखते हुए) देखते हैं, और फिर दायीं आँख B से (बायीं आँख बन्द रखते हुए), तो आप पाते हैं, कि दीवार के उस बिन्दु के सापेक्ष पेंसिल की स्थिति परिवर्तित होती प्रतीत होती है.
(defrule through8
(declare (salience 5100))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id2)
(viSeRya-through_saMbanXI  ?id2 ?)
(id-root ?id1 look|view|see)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp   through8   "  ?id " se )" crlf))
)

;@@@ Added by Anita-06-06-2014
;Rotate the wheel through 180 degrees. [oxford learner's dictionary]
;पहिया १८० डिग्री तक घुमाइये ।
(defrule through09
(declare (salience 5200))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(AjFArWaka_vAkya)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp   through09   "  ?id " waka )" crlf))
)


;@@@ Added by 14anu-ban-07,(20-08-2014)
;A direct line from the Prayag Station of Allahabad also goes to Banaras Cantonment through Kashi .(tourism corpus)
;इलाहाबाद  के  प्रयाग  स्टेशन  से  भी  एक  सीधी  लाइन  काशी  होते  हुए  बनारस  छावनी  तक  जाती  है  ।
(defrule through9
(declare (salience 5100))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(pada_info  (group_head_id ?id2) (preposition ?id))
(kriyA-through_saMbanXI ?id1 ?id2)
(id-root ?id1 go)
(id-cat_coarse ?id2 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id howe_hue))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through9   "  ?id "  howe_hue )" crlf))
)


;@@@ Added by 14anu-ban-07 (03-11-2014)
;It is the angle between the two directions when two diametrically opposite points of the planet are viewed through the telescope.(ncert)
;यह ग्रह के दो व्यासतः विपरीत (व्यास के विपरीत सिरों पर स्थित) बिंदुओं को दूरदर्शक द्वारा देखने पर प्राप्त दो दिशाओं के बीच बना कोण है.(ncert)
(defrule through10
(declare (salience 5100))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(pada_info  (group_head_id ?id2) (preposition ?id))
(kriyA-through_saMbanXI ?id1 ?id2)
(id-root ?id1 view)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xvArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through10   "  ?id "  xvArA )" crlf))
)

;@@@ Added by 14anu-ban-07 (20-11-2014)
;A traditional element of crop rotation is the replenishment of nitrogen through the use of green manure in sequence with cereals and other crops.(agriculture)
;सस्यावर्तन का परम्परागत मूलतत्व  हरी खाद  को खाद्यान्नों और अन्य फसलों के अनुक्रम में  उपयोग करके नाइट्रोजन की भरपाई करना है ।(manual)
(defrule through11
(declare (salience 5200))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(viSeRya-through_saMbanXI  ? ?id1)
(pada_info  (group_head_id ?id1) (preposition ?id))
(id-root ?id1 use)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karake))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through11   "  ?id "  karake )" crlf))
)

;@@@ Added by 14anu-ban-07 (28-01-2015)
;The fragrance of flowers permeated through the drawing room.  (Hinkhoj.com) 
;फूलो की सुगंध बैठक में फ़ैल गई.(manual)
(defrule through12
(declare (salience 5600))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(pada_info  (group_head_id ?id2) (preposition ?id))
(kriyA-through_saMbanXI ?id1 ?id2)
(id-root ?id1 permeate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through12   "  ?id "  meM )" crlf))
)

;@@@ Added by 14anu-ban-07 (16-02-2015)
;A child's vocabulary expands through reading.(oald )        
;बच्चे की शब्दावली अध्ययन से बढ़ती है .(manual)  
(defrule through13
(declare (salience 5700))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(pada_info  (group_head_id ?id2) (preposition ?id))
(kriyA-through_saMbanXI ?id1 ?id2)
(id-root ?id2 reading)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through13   "  ?id "  se )" crlf))
)

;@@@ Added by 14anu-ban-07 (16-02-2015)
;The company lost the order through production delays.(cambridge)
;कम्पनी ने उत्पादनों में विलम्ब के कारण आर्डर  गँवाया . (manual)
(defrule through14
(declare (salience 5800))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(pada_info  (group_head_id ?id2) (preposition ?id))
(kriyA-through_saMbanXI ?id1 ?id2)
(id-root ?id1 lose)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through14   "  ?id "  ke_kAraNa )" crlf))
)


;@@@ Added by 14anu-ban-07 (12-03-2015)
;One day, he was crossing through the forest carrying some woods.(report- Set7-76-77.pdf)
;एक दिन,कुछ लकडियाँ उठा कर वह जङ्गल में से गुज़र  रहा था .(manual)
(defrule through15
(declare (salience 5900))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(pada_info  (group_head_id ?id2) (preposition ?id))
(kriyA-through_saMbanXI ?id1 ?id2)
(id-root ?id1 cross)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through15   "  ?id "  meM_se )" crlf))
)


;------------------------ Default Rules ----------------------

;"through","Adv","1.eka_ora_se_xUsarI_ora"
;Put the chemical in the filter && let it pass through.
(defrule through2
(declare (salience 5000))
(id-root ?id through)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_ora_se_xUsarI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  through.clp 	through2   "  ?id "  eka_ora_se_xUsarI_ora )" crlf))
)

;"through","Adv","1.eka_ora_se_xUsarI_ora"
;Put the chemical in the filter && let it pass through.
;--"2.SurU_se_anwa_waka"
;After the day's work the labourer slept right through.
;--"3.prawibanXa_ko_wodawe_hue"
;There was red signal but the car drove straight through'
;--"4.anwa_waka"
;We drove straight through from Chicago to Marshall.
;

