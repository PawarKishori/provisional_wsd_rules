
(defrule spice0
(declare (salience 5000))
(id-root ?id spice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id masAle))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spice.clp 	spice0   "  ?id "  masAle )" crlf))
)

;"spice","N","1.masAle"
;Ginger, pepper etc. are among common spices.
;--"2.uwsAha_yA_uwsukawA"
;One should add a bit spice in his life.
;
(defrule spice1
(declare (salience 4900))
(id-root ?id spice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa_naI_cIjZa_milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spice.clp 	spice1   "  ?id "  kuCa_naI_cIjZa_milA )" crlf))
)

;"spice","V","1.kuCa_naI_cIjZa_milAnA"
;Currently released movies of English are spiced with dark humour.
;


;@@@ Added by 14anu-ban-11 on (02-03-2015)
;We need an exciting trip to add some spice to our lives.(oald)
;हमें आवश्यकता है रोमाञ्चक यात्राओ  की हमारे जीवन मे कुछ उत्साह लाने  के लिए  . (self)
(defrule spice2
(declare (salience 5001))
(id-root ?id spice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-root ?id1 add)
(id-root ?id2 life)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spice.clp 	spice2   "  ?id "  uwsAha )" crlf))
)


;@@@ Added by 14anu-ban-11 on (02-03-2015)
;Highly spiced dishes.(oald)
;अत्यधिक मसालेदार थालियाँ  . (self) 
(defrule spice3
(declare (salience 4901))
(id-root ?id spice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 dish)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id masAlexAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spice.clp 	spice3  "  ?id "  masAlexAra)" crlf))
)


;@@@ Added by 14anu-ban-11 on (02-03-2015)
;He exaggerated the details to spice up the story.(oald)
;उसने जानकारी बढा चढाकर कहीं  कहानी को रोचक बनाने  के लिये . (self)
(defrule spice4
(declare (salience 4902))
(id-root ?id spice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id   ?id1 rocaka_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  spice.clp    spice4 "  ?id "   " ?id1 "   rocaka_banA)" crlf))
)

