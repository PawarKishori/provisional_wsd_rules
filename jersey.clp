;##############################################################################
;#  Copyright (C) 2002-2005 
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################


;@@@Added by 14anu24
;He wore a blue jersey in his first meeting.
;वह अपनी पहली बैठक में एक नीले रंग की जर्सी पहनी थी. 
(defrule jersey1
(declare (salience 1000))
(id-root ?id jersey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(viSeRya-viSeRaNa  ?id ?)
(id-root ?id1 wear)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svetara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jersey.clp 	jersey1  "  ?id "  svetara )" crlf))
)


;$$$ Modified by 14anu-ban-06 (02-12-2014)
;@@@ Added by 14anu24
;Jersey is a type of light brown cow that produces high quality milk.
;जर्सी नस्ल एक हल्के भूरे रंग  की गाय  का प्रकार  है जो उच्च मात्रा में दूध देती  है.
(defrule jersey2
(declare (salience 2000))
(id-root ?id jersey|Jersey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id  noun)
;(less_upameya-than_upamAna  ?id ?)     	;commented by 14anu-ban-06 (02-12-2014)
;(viSeRya-det_viSeRaNa  ?id1 ?id)          	;commented by 14anu-ban-06 (02-12-2014)
;(id-root ?id1 the)           			;commented by 14anu-ban-06 (02-12-2014)
(subject-subject_samAnAXikaraNa ?id ?id1) 	;added by 14anu-ban-06 (02-12-2014)
(viSeRya-of_saMbanXI ?id1 ?id2)         	;added by 14anu-ban-06 (02-12-2014)
(id-root ?id2 cow)             			;added by 14anu-ban-06 (02-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jarsI_nasla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jersey.clp 	jersey2 "  ?id "  jarsI_nasla )" crlf))
)


;************************DEFAULT RULES*******************************************


;@@@Added by 14anu24
;These animals are less rugged than the Holstein , but more rugged than the Jersey .
;इस नस्ल के जानवर हालस्टीन नस्ल के जानवरों की तुलना में कम परन्तु जरसी नस्ल के जानवरों की तुलना में अधिक मजबूत होते हैं .
(defrule jersey0
(declare (salience 0))
(id-root ?id jersey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jarsI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jersey.clp 	jersey0   "  ?id "  jarsI )" crlf))
)

