;@@@ Added by 14anu17
;From a societal point of view[http://en.wikipedia.org/wiki/Marketing].
;दृष्टिकोण की एक सामाजिक आदत से
(defrule societal0
(declare (salience 5000))
(id-root ?id societal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmAjika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  societal.clp 	societal0   "  ?id "  sAmAjika)" crlf))
)

