;$$$ Modified by 14anu-ban-07 (13-10-2014)
;meaning changed from anwya_rogI to anwima
;The terminal or trailing zero (s) in a number without a decimal point are not significant. (ncert corpus)
;ऐसी सङ्ख्या जिसमें दशमलव नहीं है के अन्तिम अथवा अनुगामी शून्य सार्थक अङ्क नहीं होते.(ncert corpus)
(defrule terminal0
(id-root ?id terminal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  terminal.clp 	terminal0   "  ?id "  anwima )" crlf))
)

;"terminal","Adj","1.anwya_rogI"
;She has terminal illness.
;
(defrule terminal1
(declare (salience 4900))
(id-root ?id terminal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AKirI_sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  terminal.clp 	terminal1   "  ?id "  AKirI_sWAna )" crlf))
)

;"terminal","N","1.AKirI_sWAna"
;You have to catch the flight from the terminal.
;--"2.vixyuwa_cakra_kA_anwima_sirA"
;There are positive && negative terminals in the battery.
;


;@@@ Added by 14anu-ban-07 (13-10-2014)
;She has terminal illness. [note: parse problem]
(defrule terminal2
(declare (salience 5000))
(id-root ?id terminal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)	
(or(viSeRya-viSeRaNa  ?id1 ?id)(kriyA-subject  ?id2 ?id1)(kriyA-object  ?id2 ?id1))	
(id-root ?id1 illness)		
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwya_rogI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  terminal.clp 	terminal2   "  ?id "  anwya_rogI )" crlf))
)


;@@@ Added by 14anu-ban-07 (14-11-2014)
;We consider a source which produces sinusoidally varying potential  difference across its terminals.(NCERT)
;यहाँ हम एक ऐसे स्रोत की बात कर रहे हैं जो अपने सिरों के बीच ज्यावक्रीय रूप में परिवर्तनशील विभवान्तर उत्पन्न करता है.(NCERT)
(defrule terminal3
(declare (salience 4900))
(id-root ?id terminal)
(Domain physics)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sirA))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  terminal.clp 	terminal3   "  ?id "  sirA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  terminal.clp 	terminal3   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-07 (31-01-2015)
;The nearest bus terminal and railway station from Padanna are at a distance of 9 K.M. .(tourism corpus)
;पडन्ना  से  निकटतम  बस  अड्डा  एवं  रेलवे  स्टेशन  9  कि.मी.  की  दूरी  पर  है  ।(tourism corpus)
(defrule terminal4
(declare (salience 5000))
(id-root ?id terminal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 bus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id addA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  terminal.clp 	terminal4   "  ?id "  addA )" crlf))
)

