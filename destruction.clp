;@@@ Added by 14anu-ban-04 (20-11-2014)
;There can be numerous reasons for crop destruction.                              [agriculrure]
;फसल विनाश के  कई कारण  हो सकते है .                                            [manual]
(defrule destruction0
(declare (salience 100))
(id-root ?id destruction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinASa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  destruction.clp 	destruction0   "  ?id " vinASa  )" crlf))
)


;@@@ Added by 14anu-ban-04 (20-11-2014)
;After watching the spectacle of destruction by the falling rocks from varunaavat people are evacuating houses proactively . [tourism-corpus]
;वरुणावत  से  गिरते  पत्थरों  की  तबाही  का  मंजर  देख  अब  लोग  स्व- स्फूर्त  घर  खाली  कर  रहें  है  ।                   [tourism-corpus]
(defrule destruction1
(declare (salience 200))
(id-root ?id destruction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wabAhI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  destruction.clp 	destruction1   "  ?id " wabAhI  )" crlf))
)
