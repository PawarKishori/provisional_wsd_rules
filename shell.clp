
(defrule shell0
(declare (salience 5000))
(id-root ?id shell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CilakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shell.clp 	shell0   "  ?id "  CilakA )" crlf))
)

(defrule shell1
(declare (salience 4900))
(id-root ?id shell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CilakA_uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shell.clp 	shell1   "  ?id "  CilakA_uwAra )" crlf))
)


;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 30/6/2014*****
;A shell blew our headquarters apart. (Source: thefreedictionary.com/shell )
;एक बम के गोले ने हमारे  मुख्यालय को उडाया.
(defrule shell2
(declare (salience 5000))
(id-root ?id shell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 blow|destroy|blast)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bama_kA_golA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shell.clp 	shell2   "  ?id "  bama_kA_golA )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014):"affecting_id-affected_ids" fact added
;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 30/6/2014*****
;An artillery shell blew our headquarters apart. (Source: thefreedictionary.com/shell )
;तोप के गोले ने हमारे  मुख्यालय को उडाया.
(defrule shell3
(declare (salience 5100))
(id-root ?id shell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 military|artillery|army)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id wopa_kA_golA)) ;Commented by 14anu-ban-01 on (30-12-2014)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wopa_kA_golA)) ;added by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shell.clp 	shell3   "  ?id "  " ?id1"  wopa_kA_golA  )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;The sound ' box ' is a large pumpkin of about ninety centimeters in girth ; this fruit with a particularly hard shell is grown
;extensively in Maharashtra , near Pandharpur , though a certain amount was imported from African countries like Zanzibar ( Zaire ) 
;महाराष्ट्र में पंडरपुर के निकट विशेष रूप से कडे छिलके वाला यह फल बहुतायत में उगता है हालांकि कुछ मात्रा में जाजिबार ( जायर ) से इसका आयात भी किया जाता था .
;सूखने के लिए फल को धुंधुआती आंच के वाद्य यंत्र काफी ऊपर लटका दिया जाता है , कई वर्षों में यह पर्याप्त सूख जाता है .
;@@@ Added by 14anu11
(defrule shell4
(declare (salience 5000))
(id-root ?id shell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(viSeRya-det_viSeRaNa  ?id ?id2)
(viSeRya-with_saMbanXI  ?id3 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CilakA));changed "Cilake" to "CilakA" by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shell.clp 	shell4   "  ?id "  CilakA )" crlf));changed "Cilake" to "CilakA" by 14anu-ban-01 on (30-12-2014)
)


;@@@ Added by 14anu-ban-11 on (16-03-2015)
;We collected shells on the beach. (oald)
;हमने समुद्रतट पर शङ्ख इकट्ठे किये. (self)
(defrule shell5
(declare (salience 5200))
(id-root ?id shell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-on_saMbanXI  ?id1 ?id2)
(id-root ?id1 collect)
(id-root ?id2 beach)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SaMKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shell.clp 	shell5   "  ?id " SaMKa)" crlf))
)


;"shell","V","1.CilakA uwAranA"
;He ate the nuts after shelling them off.
;--"2.golAbArI karanA/golI barasAnA"
;Continous shelling in the border regions has made the life of local villagers absolutely miserable.
;
;LEVEL 
;Headword : shell
;
;
;Examples --
;
;"shell","N","1.CilakA"
;It is dificult to break the shell of a coconut.
;nAriyala ke Cilake ko wodZanA AsAna nahIM hE.
;--"2.AvaraNa"
;The walls have a concrete shell.
;xivAloM para kaMkrIta kA AvaraNa hE.
;The tortoise hid behind his shell.
;kaCuA apane AvaraNa ke pICe Cipa gayA.
;--"3.DAzcA{bAharI}"
;Only the shell of the building was left after the fire.
;Aga ke bAxa basa imArawa kA DAzcA baca gayA. 
;--"4.SaMKa/sIpa"
;I collected many colorful shells on the beach.
;samuxrawata para mEMne bahuwa se raMgIna SaMKa Ora sIpiyAz ekawriwa_kiye.
;--"5.golA{bArUxa}"
;The room was full of artillary shells.
;kamarA bArUxI goloM se BarA WA.
;
;"shell","V","1.CilakA_uwAranA"
;The monkey ate the peanuts after shelling them.
;banxara ne mUzgaPalI CilakA_uwAra_kara KAyI.
;--"2.golAbArI karanA/golI barasAnA"
;Continous shelling in the border regions has made the life of local villagers ab
;solutely miserable.
;sImAvarwI ilAkoM meM barAbara golAbArI hone kI vajaha se vahAz ke gAzvavAsiyoM kA jIvana kaRtapraxa ho gayA hE.
;
;ukwa uxAharaNoM se 'shell' Sabxa kA eka arWa jo uBara kara AwA hE vaha 'kisI BI vaswu 
;ke Upara lage mote AvaraNa' kA hE. EsA AvaraNa jo wodZanA kaTina ho. isI arWa se 'bArUxa ke gole kA arWa' vikasiwa huA lagawA hE. bArUxa ko eka XAwu ke gole meM Bara kara usakA golA wEyA kiyA jAwA hE. yahAz se kriyA 'golA barasAnA' ke arWa meM isakA viswAra ho gayA.
;awaH isa Sabxa kA anwarnihiwa sUwra kuCa EsA hogA --
;
;anwarnihiwa_sUwra ;
;
;                       *motA_AvaraNa
;                           |
;                           |
;                  |---------------------|
;                  |                     |(AvaraNavAlA)
;               sIpa, CilakA               |
;                     |                 golA{bArUxa_kA}------|(kriyA)
;                     |                 |      -----------|
;                     |                 |      |          |    
;                     |                 |   CilakA_uwAranA   golA_barasAnA
;                     |-----------------|------^             ^ 
;                                       |                    |
;                                       |--------------------|
;
;
;
;isake AXAra para isakA sUwra hogA
;
;sUwra : CilakA`[>sIpa]^golA{bArUxa_kA}
;
;
;isI arWa ke viswAra se kuCa anya samAsika Sabxa prajaniwa ho gaye lagawe hEM.
;
;"shellbean","N","1.PalI"
;He ate all the shellbeans kept in the refrigerator.
;usane Prija meM raKIM PaliyAz KA lIM.
;
;"shellshock","N","1.manovikqwi{golAbArI_kI_AvAjZa_ke_kAraNa}"
;She is suffering from shellshock.
;vaha golAbArI kI AvAjZa ko sunawe-sunawe ho jAnevAlI manovikqwi kA SikAra hE.
;
;"shellshocked","Adj","1.manovikqwigraswa"
;He was shellshocked.
;
;anwima xo Sabxa bahuwa rocaka hE. yaha golAbArI kI Xvani ko lagAwAra sunawe rahane ke
;kAraNa huI manovikqwi kI ora saMkawe karawe hEM. isakA pUrNa viswaara hogA 'Reaching
;a state of shock by continously listening to the sound of shelling.'
;yAni golAbArI kI AvAjZa lagAwAra sunawe sunawe kisI kA EsI mAnasika avasWA meM pahuzca jAnA ki vaha manovikAra bana jAe. 
