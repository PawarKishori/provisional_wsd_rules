;@@@ Added by 14anu-ban-09 on (03-04-2015)
;The team was given a roasting by manager Alex Feguson. 	[http://dictionary.reverso.net/english-cobuild/roasting]
;दल को प्रबन्धक ऐलक्स फेगुसोन के द्वारा दोष दिया गया था . 		[Manual]

(defrule roasting1
(declare (salience 1000))
(id-root ?id roasting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 give)
(kriyA-by_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id2 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xoRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  roasting.clp       roasting1   "  ?id "  xoRa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-04-2015)
;He was given a roasting from his boss.  	[oald]
;उसे उसके बॉस के द्वारा दण्ड दिया गया था . 		[Manual]

(defrule roasting2
(declare (salience 1000))
(id-root ?id roasting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 give)
(kriyA-from_saMbanXI  ?id1 ?id2)
(id-root ?id2 boss)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  roasting.clp       roasting2   "  ?id "  xaNda )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-09 on (03-04-2015)
;NOTE-Example sentence need to be added.

(defrule roasting0
(declare (salience 0000))
(id-root ?id roasting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BunanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  roasting.clp       roasting0   "  ?id "  BunanA )" crlf))
)

