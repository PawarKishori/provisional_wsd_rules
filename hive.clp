;$$$ Modified by 14anu-ban-06 (04-03-2015)
;### [COUNTER EXAMPLE] ### The company decided to hive off some of its less profitable concerns.(OALD)
;### [COUNTER EXAMPLE] ### कम्पनी ने उसके कम लाभदायक कारखानो में में से कुछ विभाजित करने का फैसला किया . (manual)
;We can't meet the deadline in two days,so we'll have to hive off some work to others.
;hama xo xinoM meM yaha kAma pUrA nahIM kara sakawe awa: hameM kuCa kAma xUsaroM ko BI xenA hogA
(defrule hive0
(declare (salience 5000))
(id-root ?id hive)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)	;added 'id2' by 14anu-ban-06 (04-03-2015) 
(id-root ?id2 work)	;added by 14anu-ban-06 (04-03-2015) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hive.clp	hive0  "  ?id "  " ?id1 "  xe  )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-03-2015)
;The company decided to hive off some of its less profitable concerns.(OALD)
;कम्पनी ने उसके कम लाभदायक कारखानो में में से कुछ विभाजित करने का फैसला किया . (manual)
(defrule hive2
(declare (salience 4800))
(id-root ?id hive)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viBAjiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hive.clp	hive2  "  ?id "  " ?id1 "  viBAjiwa_kara  )" crlf))
)

;------------------------ Default Rules ----------------------

;"hive","N","1.maXumakKiyoM kA CawwA"
;usane guNdoM ke saraxAra se xuSmanI karake'hive'(maXaXumakKiyoM ke Cawwe meM)hAWa dAlA hE.  
(defrule hive1
(declare (salience 4900))
(id-root ?id hive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maXumakKiyoM_kA_CawwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hive.clp 	hive1   "  ?id "  maXumakKiyoM_kA_CawwA )" crlf))
)

