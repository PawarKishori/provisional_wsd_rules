;@@@ Added by 14anu03 on 19-june-2014
;By contrast, 73 percent accepted the sentence Ironically.
;इसके विपरीत 73 प्रतिशत ने व्यङ्ग्यपूर्वक वाक्य को स्वीकार किया .
(defrule by100
(declare (salience 5500))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 contrast)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 isake_viparIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  by.clp     by100   "  ?id "  " ?id1 "  isake_viparIwa )" crlf))
)

(defrule by0
(declare (salience 5000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 pass)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kawarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " by.clp	by0  "  ?id "  " ?id1 "  kawarA  )" crlf))
)



(defrule by1
(declare (salience 4800))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) jolt|tarnish)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by1   "  ?id "  ke_kAraNa )" crlf))
)

(defrule by2
(declare (salience 4700))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) is|are|be|was|were|been|am)
(id-word =(+ ?id 1) integer|whole number|number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by2   "  ?id "  ka )" crlf))
)

(defrule by3
(declare (salience 4600))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) miss )
(id-word =(+ ?id 1) measure|step|action|quantity|amount|measurement|meter|metre|bar|unit|dimension|dimensions|meters)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by3   "  ?id "  waka )" crlf))
)

(defrule by4
(declare (salience 4500))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) swear|vow)
(id-word =(+ ?id 1) god|bible|geeta|gita)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by4   "  ?id "  kI )" crlf))
)



;Modified by sheetal
;My many female friends were angered by the hearings .
(defrule by5
(declare (salience 4400))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(or (id-word =(- ?id 1) join)(id-root =(+ ?id 2) hearing));'hearing' is added by sheetal 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by5   "  ?id "  se )" crlf))
)

(defrule by6
(declare (salience 4300))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) walk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by6   "  ?id "  pAsa_se )" crlf))
)

(defrule by7 
(declare (salience 4200))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) be|sit|sleep|stand);Replaced the list 'is|are|was|were|am 'with 'be' by Shirisha Manju Suggested by Chaitanya Sir (2-11-13)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by7   "  ?id "  pAsa_meM )" crlf))
)

;He was standing by me
(defrule by8
(declare (salience 4100))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) measure|step|action|quantity|amount|measurement|meter|metre|bar|unit|dimension);Replaced 'word' fact with 'root' fact by Shirisha Manju (6-11-13)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_xvArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by8   "  ?id "  ke_xvArA )" crlf))
)

; It gets replaced by a new one.
(defrule by9
(declare (salience 4000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) down )
(id-word =(+ ?id 1) weight)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by9   "  ?id "  ke_kAraNa )" crlf))
)

(defrule by10
(declare (salience 3900))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) time|time period|period of time|period|moment|minute|second|instant|point in time|clock time|hour)
(id-word =(+ ?id 1) time|period|moment|minute|second|instant|point|clock|hour);commented the above fact by Manju(20-6-13) 
;								              bcoz in id-word fact word field has no spaces.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by10   "  ?id "  waka )" crlf))
)

; time is subset of measure, && hence order is important.



;Re-Modified by Sukhada 16-10-13
;Modified by Meena(17.8.11);added "|verb|verbal_noun" in the list to stop this rule for By going there you can earn more money.
;Modified by Meena(2.8.11) added{(not(id-cat_coarse =(+ ?id 1) PropN)) and (kriyA-by_saMbanXI ?kri =(+ ?id 1))},deleted (id-cat_coarse =(+ ?id 1) noun) and commented (not (or (kriyA-by_saMbanXI  ?id1 ... )(... ... ...)))
;Modified by Meena(20.11.10); commented (kriyA-by_saMbanXI...) and added (id-cat_coarse =(+ ?id 1) noun)(id-root ?id1....)
;Added by Meena(12.11.09)
;By 2012 the task will be completed.
(defrule by11
(declare (salience 4000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(not(id-cat_coarse =(+ ?id 1) PropN|verb|verbal_noun|pronoun))
(or (and (id-cat =(+ ?id 1) <num>)(kriyA-by_saMbanXI ?kri =(+ ?id 1)))
    (and(kriyA-by_saMbanXI ?kri =(+ ?id 1))(id-root =(+ ?id 1) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "time.gdbm" ?str)))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by11   "  ?id "  waka )" crlf))
)




(defrule by12
(declare (salience 3800))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) measure|step|action|quantity|amount|measurement|meter|metre|bar|unit|dimension|dimensions|meters )
(id-word =(+ ?id 1) measure|step|action|quantity|amount|measurement|meter|metre|bar|unit|dimension|dimensions|meters)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by12   "  ?id "  guNA )" crlf))
)

(defrule by13
(declare (salience 3700))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) measure|step|action|quantity|amount|measurement|meter|metre|bar|unit|dimension|dimensions|meters)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_hisAba_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by13   "  ?id "  ke_hisAba_se )" crlf))
)

(defrule by14
(declare (salience 3600))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) watch|law)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_anusAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by14   "  ?id "  ke_anusAra )" crlf))
)

(defrule by15
(declare (salience 3500))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samIpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by15   "  ?id "  samIpa )" crlf))
)

;"by","Adv","1.samIpa"
;He took the pen when noone was by.

;$$$ Modified by 14anu-ban-02 (04-09-2014) -- added 'blow' to the list
;She was alarmed by his violent explosion.[explosion.clp]	;by 14anu-ban-02(04-02-2015)
;वह उसके भयंकर गुस्से  से भयभीत हो गयी थी . 	[manual]	;by 14anu-ban-02(04-02-2015)
;He is travelling by Rajdhani express. [express.clp]	;by 14anu-ban-02(02-02-2015)
;वह राजधानी एक्सप्रेस से यात्रा कर रहा है .[manual	;by 14anu-ban-02(02-02-2015)
;I pulled the plant up by the roots.[oxford]
;मैंने जडों से वनस्पति ऊपर उठा ली .
;By blowing in that hole water from well comes outside .[ncert]
;उस  छेद  में  फूँक  मारने  से  कुएँ  से  पानी  बाहर  आ  जाता  है  ।[ncert]
;By making this adhoc assumption, Huygens could explain the absence of the backwave.[karan singla][03-12-2014]
;इस तदर्थ कल्पना से हाइगेंस पश्च तरङ्गों की अनुपस्थिति को समझा पाए.[karan singla]
;Modified by Meena(2.8.11);added river|forest|soil| in the list Ex.Uttar pradesh is a land of cultural and geographical diversity, which is blessed by an innumerable perennial rivers, dense forests, and fertile soil. 
;Modified by Meena(19.4.11) ;added "misdeed" in the list
;His reputation was tarnished by his misdeeds.
;Modified by Meena(19.2.11);added the list for ?id2 and relation (viSeRya-by_saMbanXI  ?id1 ?id2)and commmented (or(..)) 
;I told him by telephone that I was coming by car. 
;Mysore also known as the city of palaces is just 139 kms by road from Bangalore. 
;Added by Meena(9.11.09)
;They were awakened by the sound of a gun.
;I was awakened by a gun's sound . 
(defrule by16
(declare (salience 4000))
;(declare (salience 3500))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-by_saMbanXI  ?id1 ?id2)(viSeRya-by_saMbanXI  ?id1 ?id2)) ;in ol we get this relation which most probably is not correct, once this is sorted out, we can delete (viSe..) and can get the correct output with only (kriyA-by_saMbanXI  ?id1 ?id2)
(id-root ?id2 radio|river|forest|soil|sound|road|telephone|car|bus|letter|misdeed|exposure|fire|blow|make|root|express|explosion|mistake);added "exposure" in the list (The silver was tarnished by the long exposure to the air.)     ;'make' is added in the list by 14anu-ban-02(03-12-2014)	;added 'root' in the list by 14anu-ban-02(15-01-2015)	;'express' is added in the list by 14anu-ban-02(02-02-2015)	;'explosion' is added in the list by 14anu-ban-02(04-02-2015)	;'mistake' is added by 14anu-ban-02(18-04-2016)
;Added fire in the list by Manju Suggested by Preeti(LTRC)(21-08-13) Ex: The building was completely destroyed by fire.              
;(or(viSeRya-of_saMbanXI  ?id2 ?id3)(viSeRya-RaRTI_viSeRaNa  ?id2 ?id3))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by16   "  ?id "  se )" crlf))
)



;Added by Meena(12.11.10)
;Cats sleep by day and hunt by night.
(defrule by17
(declare (salience 4300))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  =(- ?id 1) ?id1)
(id-root ?id1 day|night|candlelight)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by17   "  ?id "  meM )" crlf))
)


;Salience increased by Meena(17.8.11)
;Added by Meena(24.4.10)
;I count the cars as they pass by the office .
(defrule by18
(declare (salience 4000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) pass)
(kriyA-by_saMbanXI   =(- ?id 1) ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_hokara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by18   "  ?id "  se_hokara )" crlf))
)

;$$$ Modified by 14anu26      [05-07-14]  ;the rule is removed because there is a default rule which is covering all the conditions.
;She was fed by the dove.
;वह पवित्र आत्मा के द्वारा पाली गयी थी . 
;$$$ Modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 09-jan-2014
;Note: condition about category is added by Garima Singh.This can also be replaced by animate condition.
;conflicting example: The keen interest taken by Asutosh in legal studies is shown by the fact that he attended the Tagore Law Lectures for three successive years. 
;Added by Meena(10.11.09)
;The book was read by Meera.
;A record similar to the one outlined above may also have been achieved by many other students.
;(defrule by19
;(declare (salience 3900))
;(declare (salience 3400))
;(id-root ?id by)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-by_saMbanXI  ?id1 ?id2)
;(or
;(id-cat_coarse ?id2 PropN|pronoun);this condition is added by Garima Singh
;(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
;)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ke_xvArA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by19   "  ?id "  ke_xvArA )" crlf))
;)

;@@@ Added by 14anu-ban-02 (19-11-2014)
;It can be made by burning, grinding, dumping into water, or application of chemicals.[agriculture]
;यह पानी में  तपने, घिसने , पाटने से  या रासायनिक वस्तुएँ के  अनुप्रयोग  से बना हैं.[manual]
;Crop rotation also mitigates the build-up of pathogens and pests that often occurs when one species is continuously cropped, and can also improve soil structure and fertility by alternating deep-rooted and shallow-rooted plants.[agriculture]
;सस्यावर्तन  रोगाणुओं और कीटों की वृद्धि को भी कम करता है जो अक्सर घटित होता है जब एक प्रजाति  फसल की लगातार पैदावार  होती है ,और  मृदा  संरचना और उपजाऊपन को भी बारी-बारी से   गहरे-जड़वत् और सतही-जड़वत्  पौधों को  लगाने से  सुधार सकता है ।[manual]
;For example, if we multiply a constant velocity vector by duration (of time), we get a displacement vector.[ncert]
;उदाहरणस्वरूप, यदि हम किसी अचर वेग सदिश को किसी (समय) अन्तराल से गुणा करें तो हमें एक विस्थापन सदिश प्राप्त होगा .[ncert]
(defrule by29
(declare (salience 5000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-by_saMbanXI  ?id1 ?id2)
(id-root ?id1 make|improve|keep|multiply)  ;'improve' and 'keep' is added by 14anu-ban-02(20-11-2014) ;'multiply' is added by 14anu-ban-02(25-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by29   "  ?id "  se )" crlf))
)


;Salience reduced by Meena(9.11.09)
(defrule by20
(declare (salience 0));salience reduced by Garima Singh
;(declare (salience 3400))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_xvArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by20   "  ?id "  ke_xvArA )" crlf))
)

;xvArA is more frequent, followed by 'se' followed by 'pAsa_meM'
;default_sense && category=preposition	pAsa_meM	0
;"by","Prep","1.pAsa_meM/sAWa_meM"
;She came && sat by me.
;--"2.xvArA"
;The book was given to him by Ram.
;--"3.se"
;He was holding the child by hand.
;



;Salience reduced by Meena(10.11.09)
(defrule by21
(declare (salience -1000))
;(declare (salience 3300))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by21   "  ?id "  se )" crlf))
)


;Added by Aditya and Hardik,IIT(BHU)
; "kriyA-by_saMbanXI"
;"ke_xvArA"
;Hardik was killed by gun.
;"waka"
;Maybe you will get it by delhi and you will not need to go to Lucknow.

;commented by 14anu-ban-02(14-01-2015).
;need to be handle in flash.
;@@@ Added by 14anu09[25-06-14]
; The morning has just flashed by.
;	सुबह अभी चमकी है . 
;(defrule by022
;(declare (salience 3400))
;(id-root ?id by)
;(id-root =(- ?id 1) flash|come)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id preposition)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id -))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by022   "  ?id " -  )" crlf))
;)

;$$$Modified by 14anu-ban-02(05-02-2015)
;Radio waves were discovered by Hertz, and produced by J.C. Bose and G. Marconi by the end of the 19th century.[ncert 12_02]
;हर्ट्ज ने रेडियो तरङ्गों की खोज की तथा 19वीं शताब्दी के अन्त तक सर जे.सी. बोस तथा मार्कोनी ने इन तरङ्गों को उत्पन्न किया.[ncert]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)06-jan-2014
;By the nineteenth century, enough evidence had accumulated in favor of atomic hypothesis of matter.
;She had promised to be back by five o'clock. 
;The application must be in by the 31st to be accepted.
(defrule by22
(declare (salience 3900))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))	;added by 14anu-ban-02(05-02-2015)
(kriyA-by_saMbanXI  ? ?id1)
(or(id-cat_coarse ?id1 number)(id-word ?id1 century|end))	;'end'   is added in the list by14anu-ban-02(05-02-2015) .
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by22   "  ?id "  waka )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)02-jan-2014
;She kept a long stick by her to chase away the goats and cows that tried to snatch at the bundles of grass and hay. 
;वह उसके पास एक छड़ी रखती थी जिससे वह बकरियों और गायों को भगाया करती है जो घास में मुँह मारने के लिए आ जाती है।
(defrule by23
(declare (salience 4000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id1 ?id2)
(id-word ?id2 him|her)
(kriyA-object  ?id1 ?obj)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by23   "  ?id "  ke_pAsa )" crlf))
)
;$$$ Modified by 14anu26      [05-07-14]            ;the rule is removed because there is no need of this as the given translation will also be ok for the given example. 
;To know a person by his actions.[shiksharthi kosh]
;व्यक्ति को उसके कर्मों के द्वारा जानना.
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)07-jan-2014
;To know a person by his actions.[shiksharthi kosh]
;व्यक्ति को उसके कर्मों से जानना.
;(defrule by24
;(declare (salience 3500))
;(id-root ?id by)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-by_saMbanXI  ?id1 ?id2)
;(viSeRya-RaRTI_viSeRaNa  ?id2 ?id3)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id se))
;(if ?*debug_flag* then(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by24   "  ?id "  se )" crlf))
;)

;$$$Modified by14anu-ban02(15-01-2015)
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)09-jan-2014
;He took up his position by the door.[olad]
;उसने दरवाजे के पास उसका स्थान लिया.
;$$$ Modified by 14anu21 by adding relation (not(id-root ?id1 pull))
;I pulled the plant up by the roots.[oxford]
;मैंने जडों के पास वनस्पति ऊपर उठा ली . (Translation before modification)
;मैंने जडों से वनस्पति ऊपर उठा ली . [Rule by24 was fired after modification ]
(defrule by25
(declare (salience 4000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))	;added by 14anu-ban-02(15-01-2015)
(viSeRya-det_viSeRaNa  ?id2 ?det)
(id-word ?det the)
(kriyA-by_saMbanXI  ?id1 ?id2)
(id-root ?id1 take)	;added by 14anu-ban-02(15-01-2015)
;(not(id-root ?id1 pull));Added by 14anu21 	;commented by 14anu-ban-02(15-01-2015)
;(not(id-cat_coarse ?id2 PropN|pronoun));added by Garima Singh	;commented by 14anu-ban-02(15-01-2015)
;(kriyA-object  ?id1 ?obj)	;commented by 14anu-ban-02(15-01-2015)
;(id-root ?obj ?str)	;commented by 14anu-ban-02(15-01-2015)
;(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE))) ;commented by 14anu-ban-02(15-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by25   "  ?id "  ke_pAsa )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)09-jan-2014
;There's a pad and pencil by the phone.
;फोन के पास में एक पैड और कलम है
(defrule by26
(declare (salience 4000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-by_saMbanXI  ?id1 ?id2)
(viSeRya-det_viSeRaNa  ?id2 ?)
(id-root ?id2 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp        by26   "  ?id "  ke_pAsa )" crlf))
)

;@@@ Added by 14anu-ban-02 (27-09-2014)
;For example, if you, by habit, always hold your head a bit too far to the right while reading the position of a needle on the scale, you will introduce an error due to parallax.[ncert] 
;उदाहरण के लिए, प्रकाशीय मञ्च पर सुई की स्थिति का पैमाने पर पाठ्याङ्क लेते समय यदि आप स्वभाव के कारण अपना सिर सदैव सही स्थिति से थोडा दाईं ओर रखेङ्गे, तो पाठन में लम्बन के कारण त्रुटि आ जाएगी.[ncert]
;These charged particles get separated by updrafts in the clouds and gravity.[ncert]
;ये आवेशित कण बादलों के ऊर्ध्ववाह एवं गुरुत्व के कारण पृथक हो जाते हैं.[ncert]
(defrule by27
(declare (salience 4800))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) habit|updrafts) ; added 'updrafts' by 14anu-ban-02 (07-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp 	by27   "  ?id "  ke_kAraNa )" crlf))
)

;@@@ Added by 14anu-ban-02 (12-11-2014)
;Work done on the cycle by the road is the work done by the stopping (frictional) force on the cycle due to the road.[ncert]
;सडक द्वारा साइकिल पर किया गया कार्य सडक द्वारा साइकिल पर लगाए गए विरोधी (घर्षण बल) द्वारा किया किया कार्य है.[ncert]
(defrule by28
(declare (salience 4000))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(kriyA-by_saMbanXI  ? ?id1)
(id-root ?id1 force)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_xvArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp       by28   "  ?id "  ke_xvArA )" crlf))
)

;@@@ Added by 14anu-ban-02(10-12-2014)
;He is by any account an honest man.[mnit from account 012]
;वो हर पैमाने पर एक ईमांदार व्यक्ति है.[mnit account012]
(defrule by30
(declare (salience 4200))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(kriyA-by_saMbanXI  ? ?id1)
(id-root ?id1 account)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp       by30   "  ?id "  para )" crlf))
)

;@@@Added by 14anu-ban-02(06-02-2016)
;By going there you can earn more money[sd_verified]
;वहाँ पर जाने से आप अधिक पैसा कमा सकते हैं . [self]
(defrule by31
(declare (salience 4200))
(id-root ?id by)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(kriyA-vAkya_viBakwi  ?id1 ?id) 	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  by.clp       by31   "  ?id "  se )" crlf))
)
