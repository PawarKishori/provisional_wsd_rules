;@@@Added by 14anu-ban-02(02-03-2015)
;Sentence: I don't know if the painting is authentic.[oald]
;Translation:मैं नहीं जानता हूँ कि यदि कलाकृति असली है तो.[self]
(defrule authentic0 
(declare (salience 0)) 
(id-root ?id authentic) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id asalI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  authentic.clp  authentic0  "  ?id "  asalI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(02-03-2015)
;An authentic model of the ancient town.[oald]
;प्राचीन नगर का एक वास्तविक प्रतिरूप.[self]
(defrule authentic1 
(declare (salience 100)) 
(id-root ?id authentic) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 model)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id vAswavika)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  authentic.clp  authentic1  "  ?id "  vAswavika )" crlf)) 
) 

;@@@Added by 14anu-ban-02(03-03-2015)
;We saw authentic examples of ancient Roman sculpture.[mw]
;हमने प्राचीन रोमवासी मूर्तिकला के अधिप्रमाणित उदाहरणों को देखा . [self]
(defrule authentic2
(declare (salience 100)) 
(id-root ?id authentic) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 example)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id aXipramANiwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  authentic.clp  authentic2  "  ?id "  aXipramANiwa )" crlf)) 
) 
