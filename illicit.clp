;@@@ Added by 14anu-ban-06 (20-04-2015)
;An illicit love affair. (OALD)
;नाजायज प्रेम सम्बन्ध . (manual)
(defrule illicit1
(declare (salience 2000))
(id-root ?id illicit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 affair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAjAyaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illicit.clp 	illicit1   "  ?id "  nAjAyaja )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (20-04-2015)
;Illicit drugs such as cocaine and cannabis. (cambridge)
;अवैध दवा जैसे कि कोकेन{नशीली वस्तु} और भाँग .(manual)
(defrule illicit0
(declare (salience 0))
(id-root ?id illicit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avEXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illicit.clp 	illicit0   "  ?id "  avEXa )" crlf))
)
