;########################################################################
;#  Copyright (C) 2014-2015 14anu26 (noopur.nigam92@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################
;@@@ Added by 14anu26   [23-06-14]
;It's amazing how soon you adapt.
;यह आश्चर्यजनक है कि कितना शीघ्र आप ढलते हैं . 
(defrule adapt0
(declare (salience 5000))
(id-root ?id adapt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  adapt.clp 	adapt0   "  ?id " Dala )" crlf))
)

;@@@ Added by 14anu26   [23-06-14]
;A large organization can be slow to adapt to change.
;एक बडा सङ्गठन बदलाव के अनुकूल बनने मे़ँ धीमा हो सकता है . 
(defrule adapt1
(declare (salience 5000))
(id-root ?id adapt)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive 	?to ?id)
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukUla_bananA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  adapt.clp 	adapt1   "  ?id " anukUla_bananA )" crlf))
)

;@@@ Added by 14anu26   [23-06-14]
;It took him a while to adapt himself to his new surroundings. 
;उसे स्वयं को उसके नये परिवेश के अनुकूल बनाने मे़ थोड़ा समय लगा. 
(defrule adapt2
(declare (salience 5100))
(id-root ?id adapt)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive 	?to ?id)
(kriyA-object ?id  ?)
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukUla_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  adapt.clp 	adapt2   "  ?id " anukUla_banA )" crlf))
)

;@@@ Added by 14anu26    [23-06-14]
;The film was adapted from a hindi short story.
;सिनेमा एक  हिंदी सङ्क्षिप्त  कहानी से ढाला गया था . 
(defrule adapt3
(declare (salience 5000))
(id-root ?id adapt)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-for_saMbanXI ?id ?)(kriyA-from_saMbanXI ?id ?))
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  adapt.clp 	adapt3   "  ?id " DAla )" crlf))
)
