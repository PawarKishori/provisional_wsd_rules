
(defrule study0
(declare (salience 5000))
(id-root ?id study)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id studied )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXyayana_kara))
(assert (id-H_vib_mng ?id yA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  study.clp 	study0   "  ?id "  aXyayana_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng    " ?*prov_dir* "  study.clp     study0   "  ?id " yA )" crlf))
)

(defrule study1
(declare (salience 4900))
(id-root ?id study)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id studied )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id socA-samaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  study.clp  	study1   "  ?id "  socA-samaJA )" crlf))
)

;"studied","Adj","1.socA-samaJA"
;He replied with studied indifference.
;--"2.jAnabUJakara kiyA huA"
;He observed the studied slowness of his movements.
;
;
(defrule study2
(declare (salience 4800))
(id-root ?id study)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXyayana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  study.clp 	study2   "  ?id "  aXyayana )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (20-02-2016)	:changed viBakwi from "ke_bAre_meM" to "kA" as suggested by Chaitanya Sir
;अध्याय 7 में हमनें पिण्डों के घूर्णन का अध्ययन किया और समझा कि किसी पिण्ड की गति इस बात पर कैसे निर्भर करती है कि पिण्ड के अंदर द्रव्यमान किस प्रकार वितरित है [improvised translation]
;"study","N","1.aXyayana"
;A study of grammar is important for everyone.
;$$$ Modified by 14anu-ban-01 on (09-01-2015)	:changed viBakwi from "kA" to "ke_bAre_meM"
;$$$ Modified by 14anu-ban-01 on (19-09-14). added Eng and manual Translation  
;In Chapter 7, we studied the rotation of the bodies and then realized that the motion of a body depends on how mass is distributed within the body.[NCERT corpus] 
;aXyAya 7 meM hamaneM piNdoM ke GUNana ke bAre meM paDZA Ora samaJA ki kisI piNda kI gawi isa bAwa para kEse nirBara karawI hE ki piNda ke aMxara xravyamAna kisa prakAra viwariwa hE.[NCERT corpus] 
(defrule study3
(declare (salience 5000));salience increased from 4700 to 5000 because study0 was firing in my sentence unnecessarily because of difference in salience.
(id-root ?id study)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXyayana_kara))
(assert (kriyA_id-object_viBakwi ?id kA))	;changed viBakwi from "ke_bAre_meM" to "kA" by 14anu-ban-01 on (20-02-2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  study.clp 	study3   "  ?id "  aXyayana_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  study.clp     study3   "  ?id " kA )" crlf)
)	;changed viBakwi from "ke_bAre_meM" to "kA" by 14anu-ban-01 on (20-02-2016)
)

;$$$Modified by 14anu-ban-01 on (12-01-2015)
;@@@ added by 14anu11
;कभी कभी हमारे बडे बडे मशहूर वैज्ञानिक जब अपने स्टडी या लेबोरेटरी से बाहर आते हैं , तब वे जिंदगी के बाकी क्षेत्रों में विज्ञान के इस उद्देश्य और लक्ष्य को भूल जाते हैं .
;Somehow eminent men of science when they come out of their study or laboratory forget the approach and method of science in other fields of life.[source added by 14anu-ban-01 :Foundations of Education for Free India: Toward a New Quality of Life ]
(defrule study4
(declare (salience 5000))
(id-root ?id study)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-of_saMbanXI  ?id1 ?id)		;added by 14anu-ban-01 on (12-01-2015)
(id-root ?id1 come|go|sit|talk)		;added by 14anu-ban-01 on (12-01-2015)
;(viSeRya-RaRTI_viSeRaNa  ?id ?id1)	;commented by 14anu-ban-01
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id stadI));corrected spelling of 'stadI' by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  study.clp 	study4   "  ?id "  stadI )" crlf));corrected spelling of 'stadI' by 14anu-ban-01 on (12-01-2015)
)


;"study","V","1.aXyayana_karanA"
;I study in class Xth.
;
