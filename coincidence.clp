;@@@ Added by 14anu-ban-03(6-9-2014)
;If I could , I'd write a huge encyclopedia just about the words luck and coincidence . [karna singla]
;अगर मैं कर सकता तो एक बहुत विश्वकोष लिखता , केवल दो शब्दों को लेकर — भाग्य और संयोग ।
(defrule coincidence0
(declare (salience 000))
(id-root ?id coincidence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMyoga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coincidence.clp 	coincidence0   "  ?id "  saMyoga )" crlf))
)

;@@@ Added by 14anu-ban-03(6-9-2014)
;Is this a curious coincidence between these large numbers purely accidental? [NCERT CORPUS]
;kyA ina viSAla safKyAoM kI yaha AScaryajanaka, anurUpawA mAwra saMyoga hE?
;You may notice that there is an interesting coincidence between the numbers appearing in Tables 2.3 and 2.5. [ncert]
;सारणी 2.3 एवं 2.5 में दर्शायी गई सङ्ख्याओं में आश्चर्यजनक अनुरूपता है. [ncert]
(defrule coincidence1
(declare (salience 200))
(id-root ?id coincidence)
?mng <-(meaning_to_be_decided ?id)
(or(subject-subject_samAnAXikaraNa  ? ?id) (viSeRya-viSeRaNa ?id ?)) ;added relation (viSeRya-viSeRaNa ?id ?) by 14anu-ban-03 (16-10-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anurUpawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coincidence.clp 	coincidence1   "  ?id "  anurUpawA )" crlf))
)
