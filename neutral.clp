
(defrule neutral0
(declare (salience 5000))
(id-root ?id neutral)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id watasWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  neutral.clp 	neutral0   "  ?id "  watasWa )" crlf))
)

;"neutral","Adj","1.watasWa"
;He is a neutral person.
;
(defrule neutral1
(declare (salience 4900))
(id-root ?id neutral)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niRpakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  neutral.clp 	neutral1   "  ?id "  niRpakRa )" crlf))
)

;"neutral","N","1.niRpakRa"
;That country supports all the countries which are neutral.
;

;@@@ Added by 14anu-ban-08 (08-11-2014)
;Suppose we connect one end of a copper wire to a neutral pith ball and the other end to a negatively charged plastic rod.   [NCERT]
;मान लीजिए हम किसी ताँबे के तार के एक सिरे को उदासीन सरकण्डे की गोली से जोड देते हैं तथा दूसरे सिरे को ऋणावेशित प्लास्टिक-छड से जोड देते हैं    [NCERT]
(defrule neutral2
(declare (salience 5001))
(id-root ?id neutral)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 ball)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxAsIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  neutral.clp 	neutral2   "  ?id "  uxAsIna )" crlf))
)

