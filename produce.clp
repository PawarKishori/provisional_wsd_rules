;Added by Meena(18.02.10)
;The native speakers of English do not produce a variable mishmash of words . 
(defrule produce0
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id2 word)
(viSeRya-of_saMbanXI ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp   produce0   "  ?id "  kara )" crlf))
)




;Salience reduced by Meena(24.5.10)
(defrule produce1
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce1   "  ?id "  upaja )" crlf))
)

;"produce","N","1.upaja"
;This year a large part of paddy produce got ruined by heavy rains.
;--"2.uwpAxa"
;The factory sells its produce in the local markets.
;



;$$$ Modified by 14anu-ban-09 on (13-11-2014)
;Soybeans can be processed to produce a texture and appearance similar to many other foods. [Agriculture]
;सोयाबीन अन्य अनेक आहारो के समान बनावट और प्रतीति देने के लिए ढाले जा सकते हैं. [Self]
;Females of birds produce eggs .
(defrule produce2
(declare (salience 4900))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-root ?obj egg|texture) ;Added 'texture' by 14anu-ban-09 on (13-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp   produce2   "  ?id "  xe )" crlf))
)

;$$$ Modified by 14anu-ban-09 on 02-08-2014
;Added by Sonam Gupta MTech IT Banasthali 2013
;Our cat produced four kittens during the course of the night. [Veena mam Translation]
;हमारी बिल्ली ने रात के दौरान चार बिल्ली के बच्चे पैदा किए . 
(defrule produce3
(declare (salience 4800))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-during_saMbanXI  ?id ?));commented by 14anu-ban-09
(kriyA-object  ?id ?id2) ;added by 14anu-ban-09
(kriyA-subject  ?id ?id1)  ;added by 14anu-ban-09
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))  ;added by 14anu-ban-09
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))  ;added by 14anu-ban-09
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp   produce3   "  ?id "  pExA_kara )" crlf))
)

;Added by Sonam Gupta MTech IT Banasthali 2013
;He produced a letter from his desk which he asked me to read.  [Veena mam Translation]
;उसने उसके डेस्क से पत्र निकाला कि जिसे उसने मुझे पढने के लिये पूछा . 
(defrule produce4
(declare (salience 4700))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-kriyA_viSeRaNa  ?id ?)(kriyA-from_saMbanXI  ?id ?))(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-from_saMbanXI  ?id ?)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp   produce4   "  ?id "  nikAlA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (13-10-2014)
;[COUNTER EXAMPLE] #### Therefore, these materials require a large force to produce small change in length. ##### [NCERT CORPUS]
;isalie ina paxArWoM meM laMbAI meM WodZA hI aMwara uwpanna karane ke lie bahuwa aXika bala kI AvaSyakawA howI hE. [NCERT CORPUS]
;Added by Meena(20.5.10)
;The factory typically produces 500 chairs a week . 
;PEktarI viSiRta rUpa se sapwAha me 500 kursIyAz banAwI hE. [translation given by  14anu-ban-09]
(defrule produce5
(declare (salience 4600))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 chair|table|cot);comment removed by 14anu-ban-09 on (13-10-2014) 
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp   produce5   "  ?id "  banA )" crlf))
)

;Added by Sonam Gupta MTech IT Banasthali 2013
;The drug produces a feeling of excitement. [OALD]
;दवा हलचल की संवेदना पैदा करती  है .
(defrule produce06
(declare (salience 4500))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 feeling|sensation|excitement)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp   produce06   "  ?id "  pExA_kara )" crlf))
)


;Modified by Sonam Gupta MTech IT Banasthali 2013
;Salience reduced by Meena(20.5.10)
(defrule produce6
(declare (salience 0))
;(declare (salience 4800))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce6   "  ?id "  uwpanna_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-09-2014)
;[COUNTER EXAMPLE] #### The factory typically produces 500 chairs a week .##### [Same CLP file]
;PEktarI viSiRta rUpa se sapwAha me 500 kursIyAz banAwI hE. [Self]
;Just as force produces acceleration, torque produces angular acceleration in a body.  [NCERT CORPUS]
;TIka vEse hI jEse bala piNda meM reKIya wvaraNa uwpanna karawA hE, bala AGUrNa isameM koNIya wvaraNa pExA karawA hE. [NCERT CORPUS]

(defrule produce7
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(kriyA-subject  ?id ?id1)
;(id-cat_coarse ?id1 noun);commented by 14anu-ban-09 on (14-10-2014)
;(id-cat_coarse ?id2 noun);commented by 14anu-ban-09 on (14-10-2014)
(id-root ?id1 force|torque) ;more constraints can be added
(id-root ?id2 acceleration) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce7   "  ?id "  uwpanna_kara )" crlf))
)
;@@@ Added by 14anu-ban-09 on (14-10-2014)
;Robert Hooke, an English physicist (1635-1703 A. D) performed experiments on springs and found that the elongation (change in the length) produced in a body is proportional to the applied force or load. [NCERT CORPUS]
;eka aMgrejZa BOwika SAswrI rAbarta huka (san 1635 - 1703) ne spriMgoM para prayoga kie Ora yaha pAyA ki kisI piNda meM uwpanna viswAra (laMbAI meM vqxXi) prawyAropiwa bala yA loda ke anukramAnupAwI howA hE. [NCERT COROUS]

(defrule produce8
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 elongation) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce8   "  ?id "  uwpanna )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-10-2014)
;The strain so produced is known as shearing strain and it is defined as the ratio of relative displacement of the faces Δx to the length of the cylinder L. [NCERT CORPUS]
;usI anuprasWa paricCexa ke Eluminiyama, pIwala waWA wAzbe ke wAroM meM uwanI hI vikqwi uwpanna karane ke lie AvaSyaka bala kramaSaH 690 @N, 900 @N waWA 1100 @N howe hEM. [NCERT CORPUS]

(defrule produce9
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 strain) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce9   "  ?id "  uwpanna )" crlf))
)


;@@@ Added by 14anu-ban-09 on (14-10-2014)
;Beyond this point, additional strain is produced even by a reduced applied force and fracture occurs at point E. [NCERT CORPUS]
;isa biMxu ke Age prawyAropiwa bala ko GatAne para BI awirikwa vikqwi uwpanna howI hE Ora biMxu @E para viBaMjana ho jAwA hE. [NCERT CORPUS]

(defrule produce10
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id1 strain) ;more constraints can be added
(id-root ?id2 additional) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce10   "  ?id "  uwpanna_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (25-11-2014)
;So no work is done and no change in the magnitude of the velocity is produced (though the direction of momentum may be changed). [NCERT CORPUS]
;awaH koI kArya nahIM howA waWA vega ke parimANa meM BI koI parivarwana nahIM howA (yaxyapi saMvega kI xiSA meM parivarwana ho sakawA hE). [NCERT CORPUS]

(defrule produce11
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma  ?id ?id1)
(id-root ?id1 change) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpanna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce11   "  ?id "  uwpanna_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (01-12-2014)
;The first lens produces an image at I 1. [NCERT CORPUS]
;pahalA leMsa biMxu @I 1 para prawibiMba banAwA hE. [NCERT CORPUS]

(defrule produce12
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 image) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce12   "  ?id "  banA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-04-2015)
;She produced a long silver whistle and placed it firmly between her lips. [oald]
;उसने एक लम्बी चाँदी सीटी निकाली और स्थिरता से उसके होंठो के बीच में रखा . 	[Manual]
(defrule produce13
(declare (salience 5000))
(id-root ?id produce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 whistle) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  produce.clp 	produce13   "  ?id "  nikAla )" crlf))
)

;"produce","V","1.uwpAxana_karanA"
;My uncle's factory produce shoes.
;--"2.racanA_karanA"
;Monalisa was the best work produced by Picasso.
;--"3.parisWiwi_uwpanna_karanA"
;My arrival at home produced many problems for him.
;--"4.praswuwa_karanA"
;The operator produced the bill immediately.
;
