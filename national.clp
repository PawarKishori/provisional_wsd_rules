;$$$Modified by 14anu-ban-08 (23-01-2015)          ;changed meaning
;$$$ Modified by 14anu19(05-07-2014)
;wx notatin of  rARtrIya is corrected (rARtriya)
(defrule national0                                                        
(declare (salience 5000))
(id-root ?id national)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rARtrIya))  ;wx notation is corrected by 14anu19
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  national.clp 	national0   "  ?id "  rARtrIya )" crlf));inconsistency in the mng in assert & print statement has been corrected by Sukhada (15.3.10))       ;modified meaning from 'rARtriya' to 'rARtrIya' by 14anu-ban-08 (23-01-2015)
)
; instead cat coarse( adj)  used relation (viSeRya-viSeRaNa)
;"national","Adj","1.rARtrIya"
;The national flower of India is lotus.    by Meena
;
(defrule national1
(declare (salience 4900))
(id-root ?id national)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAgarika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  national.clp 	national1   "  ?id "  nAgarika )" crlf))
)
; instead cat coarse( noun)  used relation (viSeRya-viSeRaNa)
;"national","N","1.nAgarika"
;All the foreign nationals have been asked to produce their passports.
;
