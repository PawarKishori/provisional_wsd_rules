;@@@Added by 14anu-ban-02(16-03-2015)
;We plan to have a barbecue for the whole family.[mw]
;हम पूरे परिवार के लिए पिकनिक की योजना बनाते हैं . [self]
(defrule barbecue1 
(declare (salience 100)) 
(id-root ?id barbecue) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object  ?id1 ?id)	;need more examples to restrict the rule.	 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pikanika)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barbecue.clp  barbecue1  "  ?id "  pikanika )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(16-03-2015)
;Sentence: I put another steak on the barbecue.[oald]
;Translation: मैंने सींक पर एक और माँस का टुकड़ा रखा . [self]
(defrule barbecue0 
(declare (salience 0)) 
(id-root ?id barbecue) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sIfka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  barbecue.clp  barbecue0  "  ?id "  sIfka )" crlf)) 
) 
