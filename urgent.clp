;@@@ Added by 14anu03 on 21-june-14
;You need to come urgent.
;आपको तुरन्त आने की जरूरत है .
(defrule urgent100
(declare (salience 5500))
(id-root ?id urgent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-word ?id1 come)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wuraMwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  urgent.clp 	urgent100   "  ?id "  wuraMwa )" crlf))
)
;@@@ Added by Prachi Rathore[24-2-14]
;The fossil fuels of the planet are dwindling fast and there is an urgent need to discover new and affordable sources of energy.[ncert]
;.हमारे ग्रह के जीवाश्मी ईन्धन त्वरित क्षीयमान हैं तथा नवीन एवं सस्ते ऊर्जा स्रोतों की खोज अत्यावश्यक है.".
;$$$ Modified by 14anu03 on 21-june-14  meaning changed "awyAvaSyaka" --> "kI_wawkAla_jarUrawa"
(defrule urgent2
(declare (salience 5000))
(id-root ?id urgent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 need)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kI_wawkAla_jarUrawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " urgent.clp 	urgent2   "  ?id "  " ?id1 "  kI_wawkAla_jarUrawa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_urgent2
(declare (salience 5000))
(id-root ?id urgent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 need)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 awyAvaSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng " ?*prov_dir* " urgent.clp   sub_samA_urgent2   "   ?id " " ?id1 " awyAvaSyaka )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_urgent2
(declare (salience 5000))
(id-root ?id urgent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 need)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 awyAvaSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng " ?*prov_dir* " urgent.clp   obj_samA_urgent2   "   ?id " " ?id1 " awyAvaSyaka )" crlf))
)


;@@@ Added by 14anu-ban-07, (25-03-2015)
;His urgent pleas of innocence made no difference to the judge's decision.(cambridge)
;उसके निर्दोषता के पुनरावृत्त निवेदन  ने न्यायाधीश के निर्णय में कोई अन्तर नहीं  लाया . (manual)
(defrule urgent3
(declare (salience 5100))
(id-root ?id urgent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 plea)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id punarAvqwwa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  urgent.clp 	urgent3   "  ?id "  punarAvqwwa )" crlf))
)

;------------------------ Default Rules ----------------------

;There is a need for urgent talks.
;तत्काल  बातचीत की जरूरत है . [added by 14anu3]
;$$$ Modified by 14anu03 changed the meaning from "awyAvaSyaka" to  "wawkAla"
(defrule urgent0
(declare (salience 5000))
(id-root ?id urgent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wawkAla)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  urgent.clp 	urgent0   "  ?id "  wawkAla )" crlf))
)

;"urgent","Adj","1.awyAvaSyaka"
;An urgent telephone call came for her.
(defrule urgent1
(declare (salience 4900))
(id-root ?id urgent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyAvaSyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  urgent.clp 	urgent1   "  ?id "  awyAvaSyaka )" crlf))
)

