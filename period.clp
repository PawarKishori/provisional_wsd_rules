;##############################################################################
;#  Copyright (C) 2013-2014 Sonam Gupta(sonam27virgo@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;Modified by 14anu-ban-09 on (06-10-2014)
;### COUNTER EXAMPLE ### The nationalist groups in Mozambique, like those across Africa during the period, received training and equipment from the Soviet Union. ### [wiki-mozambican_war]
;$$$ Modified by 14anu-ban-03 (26-8-2014)
;Removed 'or' condition for '(samAsa_viSeRya-samAsa_viSeRaNa ? ?id)' relation
;### ;Indelible mark of the Mughal period architecture and culture can still be seen here . (tourism corpus)
;Mughal avaXi kI vAswukalA kA Ora saMskqwi kA sWAyI niSAna yahAz aBI BI xeKA jA sakawA hEM.
;@@@ Added by Sonam Gupta MTech IT Banasthali 30-1-2014
;When did you last have a period? [OALD]
;आपको पिछली बार मासिकधर्म कब हुए थे ?
(defrule period1
(declare (salience 5500))
(id-root ?id period)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-det_viSeRaNa  ?id ?) ;commented by 14anu-ban-09 
(kriyA-object  ?id1 ?id) ;added by 14anu-ban-09
(id-root ?id1 have) ;added by 14anu-ban-09
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAsikaXarma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period1   "  ?id "  mAsikaXarma )" crlf))
)

;Removed by 14anu-ban-09 on (04-11-2014)
;NOTE-This sentence is already handled by the above rule. So, not required.
;$$$ Modified by 14anu24
;@@@ Added by Sonam Gupta MTech IT Banasthali 30-1-2014
;When did you last have a period? [OALD]
;आपको पिछली बार मासिकधर्म कब हुए थे ?
;(defrule period001
;(declare (salience 5500))
;(id-root ?id period)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(or(viSeRya-det_viSeRaNa  ?id ?)(samAsa_viSeRya-samAsa_viSeRaNa  ? ?id))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id mAsikaXarma))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period001   "  ?id "  mAsikaXarma )" crlf))
;)


;@@@ Added by 14anu24
;This will enable her to get sufficient rest before the next lactation period .
;इस तरह आगामी स्तनपान की अवधि के लिए उसे पर्याप्त विश्राम मिल जाता है .
(defrule period01
(declare (salience 6000))
(id-root ?id period)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1  lactation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp   period01   "  ?id "  avaXi )" crlf))
)

;@@@ Added by 14anu20 on 20/06/2014
;This is a period of life which I desire.
;यह जीवन का एक ऐसा काल है जो मैं चाहता हूँ .
(defrule period04
(declare (salience 6500))
(id-root ?id period)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period04   "  ?id "  kAla )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 30-1-2014
;Period furniture. [M-W]
;प्राचीन फर्नीचर .
(defrule period2
(id-root ?id period)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAcIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period2   "  ?id "  prAcIna )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 30-1-2014
;A 24 hour period. [Cambridge]
;२४ घण्टे की एक अवधि .
;Indelible mark of the Mughal period architecture and culture can still be seen here . (tourism corpus)
;Mughal avaXi kI vAswukalA kA Ora saMskqwi kA sWAyI niSAna yahAz aBI BI xeKA jA sakawA hEM.
(defrule period3
(id-root ?id period)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period3   "  ?id "  avaXi )" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-08-2014)
;It is said that this place was meeting place of the Jarasandh  , the character of the olden Mahabharat period. [Tourism Corpus]
;कहा  जाता  है  कि  यह  स्थान  पौराणिक  महाभारत-काल  के  पात्र  जरासन्ध  की  बैठक  थी  ।

(defrule period4
(declare (salience 5050))
(id-root ?id period)		;added by 14anu-ban-09 on (06-02-2015)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 PropN)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period4   "  ?id "  kAla )" crlf))
)

;@@@ Added by 14anu-ban-09 on (01-10-2014)
;IF ONLY the woman had paused for a moment to inspect the hastily scrawled felt tip notice at the top of the stairs: "I am sorry for any inconvenience caused to all our customers during the period that the down escalator has been out of service. [wiki-bnc_gold]
;सिर्फ/काश अगर वह महिला एक पल के लिए सीढ़ियों के ऊपर लगे उस जल्दीबाज़ी में लिखे हुए "फेल्ट टिप" सूचना को देखने के लिए रुकी होती: " हमारे सभी उपभोक्ताओं को उस समय के दौरान हुई परेशानी के लिए मुझे खेद है, जब नीचे जाने वाली स्वचालित सीढ़ी ख़राब है." [translation by Shukhada] ;added on (07-10-2014)

(defrule period5
(declare (salience 5550))
(id-root ?id period)		;added by 14anu-ban-09 on (06-02-2015)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-during_saMbanXI  ?id1 ?id)
(id-root ?id1 cause)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period5   "  ?id "  samaya )" crlf))
)

;@@@ Added by 14anu-ban-09 on (15-10-2014)
;If the circular orbit is in the equatorial plane of the earth, such a satellite, having the same period as the period of rotation of the earth about its own axis would appear stationery viewed from a point on earth. [NCERT CORPUS]
;yaxi vqwwIya kakRA pqWvI ke viRuvawa vqwwa ke wala meM hE, wo isa prakAra kA upagraha, jisakA AvarwakAla pqWvI ke apane akRa para GUrNana karane ke AvarwakAla ke barAbara ho, pqWvI ke kisI binxu se xeKane para sWira prawIwa hogA. [NCERT CORPUS]
;yaxi vqwwIya kakRA pqWvI ke viRuvawa vqwwa ke wala meM hE, wo isa prakAra kA upagraha, jisakI avaXi pqWvI ke apane akRa para GUrNana karane kI avaXi ke barAbara ho, pqWvI ke kisI binxu se xeKane para sWira prawIwa hogA. [Self]

(defrule period6
(declare (salience 5550))
(id-root ?id period)		;added by 14anu-ban-09 on (06-02-2015)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 same)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period6   "  ?id "  avaXi )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-12-2014)
;Dr . Ramnath and several other scholars have tried to identify this mansion and its construction period with the Tribhuvan Narayan temple built by the Parmar ruler Bhoja . [Tourism Corpus]
;डॉ. रामनाथ तथा कई अन्य विद्वानों ने इस प्रासाद की पहचान तथा निर्माण का काल परमार शासक भोज द्वारा निर्मित त्रिभूवन नारायण से करने का प्रयास किया है . [Tourism Corpus]

(defrule period7
(declare (salience 5050))
(id-root ?id period)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 construction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp 	period7   "  ?id "  kAla )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;@@@ Added by 14anu19 on 16-June-2014
;The industries soon entered a period of stagnation. 
;उद्योग ने शीघ्र गतिहीनता की अवधि में प्रवेश किया 
(defrule period8
(declare (salience 6500)) ;increased salience '6000' to '6500' by 14anu-ban-09 on (01-01-2015)
(id-root ?id period)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(id-word =(+ ?id 1) of) ;commented by 14anu-ban-09 on (01-01-2015)
(viSeRya-of_saMbanXI  ?id ?id1) ;Added by 14anu-ban-09 on (01-01-2015)
(id-root ?id1 stagnation) ;Added by 14anu-ban-09 on (01-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  period.clp   period8   "  ?id "  avaXi )" crlf))
)
