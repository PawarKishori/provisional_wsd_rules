
;$$$ Modified by 14anu-ban-03 (29-01-2015)
;Couples drenched in merrymaking sway to and fro on snow and the clapping of audiences start reverberates in the hall. [tourism]
;युगल मस्ती में सराबोर होकर बर्फ पर झूमते जाते हैं , और दर्शकों की तालियाँ हॉल में गूँजने लगती हैं .[tourism]
(defrule clap0
(declare (salience 5000))
(id-root ?id clap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAlI)) ;meaning changed from 'gadagadAhata' to 'wAlI' by 14anu-ban-03 (29-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clap.clp 	clap0   "  ?id "  wAlI )" crlf))
)


(defrule clap1
(declare (salience 4900))
(id-root ?id clap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAlI_bajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clap.clp 	clap1   "  ?id "  wAlI_bajA )" crlf))
)

;"clap","V","1.wAlI_bajAnA"
;The children clapped their hands while singing.
;
;
