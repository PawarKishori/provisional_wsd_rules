;@@@ Added by 14anu-ban-02(20-02-2015)
;Sentence: The enforced abasement of workers before the corporate ethos.[oald]
;Translation: कंपनी सम्बन्धी सिद्धान्त से पहले कार्यकर्ताओं का  प्रचलित अपमान.[self]
(defrule abasement0 
(declare (salience 0)) 
(id-root ?id abasement) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id apamAna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  abasement.clp  abasement0  "  ?id "  apamAna )" crlf)) 
) 

;@@@ Added by 14anu-ban-02(20-02-2015)            ;-------meaning suggested by "Sukhada Mam"
;The pilgrims knelt in self-abasement.[cambridge]
;The pilgrims knelt in self abasement.[modified]
;तीर्थयात्री अपनी दशा पर झुकते हैं.[self]
(defrule abasement1 
(declare (salience 100)) 
(id-root ?id abasement) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-in_saMbanXI  ?id1 ?id)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id xaSA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  abasement.clp  abasement1  "  ?id "  xaSA )" crlf)) 
) 
