;@@@Added by 14anu17
;Who is the leading role.[oxford]
;मुख्य  किरदार  कौन है . 

(defrule role1
(declare (salience 100))
(id-root ?id role)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiraxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  role.clp    role1   "  ?id "  kiraxAra )" crlf))
)

;@@@Added by 14anu17
;The media play a major role in influencing people.[oxford]
;सञ्चार लोगों को प्रभावित करने में एक मुख्य कार्य निभाता हैं . 
(defrule role2
(declare (salience 100))
(id-root ?id role)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kArya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  role.clp    role2   "  ?id "  kArya )" crlf))
)


;@@@Added by 14anu-ban-10 on (27-01-2015)
;The city of Jhansi is famous for its history and Rani Laxmibai had played a significant role in giving it its name and fame.[tourism corpus]
;झाँसी  शहर  अपने  इतिहास  के  लिए  प्रसिद्ध  है  और  इसे  नाम  व  मान  देने  में  रानी  लक्ष्मीबाई  ने  अहम  भूमिका  अदा  की  थी  । [tourism corpus]
(defrule role3
(declare (salience 200))
(id-root ?id role)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 significant)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUmikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  role.clp    role3   "  ?id "  BUmikA )" crlf))
)



;@@@Added by 14anu17
;The role of the teacher in the classroom.[oxford]
;कक्षा में शिक्षक की भूमिका . 
(defrule role0
(declare (salience 0))
(id-root ?id role)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUmikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  role.clp    role0   "  ?id "  BUmikA )" crlf))
)
