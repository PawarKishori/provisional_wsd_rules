
;Keep yourself a little free when I am there.
(defrule little0
(declare (salience 5000))
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) the )
(id-word =(+ ?id 1) one )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp 	little0   "  ?id "  CotA )" crlf))
)

; The little one
;previous_word=a && category=adjective && following_category=adjective	WodAsA	0
;previous_word=a && category=adjective && following_category=noun	CotAsA	0
; Since examples are not there, these rules have been blocked.
; Also it gave wrong answer in 'a little antique shop' as 'WodAsA ..'
(defrule little1
(declare (salience 4900))
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) crown )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp 	little1   "  ?id "  CotA )" crlf))
)

; Refine the above rule: Add countable/uncountable to handle the cases
; I want a little sugar
;a little afraid Vs a little crown
; the rule is modified as below: if countable, it will have sg/pl forms
(defrule little2
(declare (salience 4800))
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) pl)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA-))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp 	little2   "  ?id "  CotA- )" crlf))
)

;Remove this rule by 14anu-ban-08 (25-02-2015) because little3 and little4 are same with conditions. 
;(defrule little3
;(declare (salience 4700))
;(id-root ?id little)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 job)
;(viSeRya-viSeRaNa ?id1 ?id)
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id CotA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp 	little3   "  ?id "  CotA )" crlf))
;)

;$$$ Modified by 14anu-ban-02(13-03-2016)
;Once there were three little pigs, Sonu, Monu and Gonu.[sd_verified]
;eka bAra wIna Cote suara, sonU, monU Ora gonu We.[sd_verified]
;$$$ Modified by 14anu-ban-08 (25-02-2015)      ;added constraint
;Beneath his brash exterior, he's still a little boy inside.  [oald]
;उसके ढीठ बाह्य रूप के नीचे, वह अन्दर से अभी भी  छोटा लड़का  है .   [self]
;This little stream can become a deluge when it rains heavily.  [cald]
;यह छोटी जलधारा जल-प्रलय बन सकती है जब बहुत अधिक वर्षा होती है .  [self]
;Snappy sayings, little stories, easy lessons they can take home and turn over in their minds.(coca)
;रोचक कहावतें,छोटी कहानियाँ,सरल पाठ [की पुस्तकें] जो वे घर लेजा सकते हैं  और अपने मन में दोहरा सकते हैं . (manual)
;Added by Meena(13.10.09)
;It struggled to force its body through that little hole .
(defrule little4
(declare (salience 4720))
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 story|hole|stream|inside|pig)                ;added by 14anu-ban-08 (25-02-2015)  ;added 'stream' by 14anu-ban-08 (25-03-2015)     ;added inside by 14anu-ban-08 (13-04-2015)	;pig is added by 14anu-ban-02(13-03-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp    little4   "  ?id "  CotA )" crlf))
)



(defrule little5
(declare (salience 4600))
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp 	little5   "  ?id "  WodA )" crlf))
)

;Remove by 14anu-ban-08 (25-03-2015) ,unnecessary rule, it will fire from fire rule.
;$$$ Modified by 14anu07 on 25/06/2014
;@@@ Added by Nandini (11-12-13)
;I felt his essay needed a little pruning.(via mail)
;muJe lagawA hE ki usake nibanXa ko Wode kAta-CAzta kI AvaSyakawA hE.
;(defrule little6
;(declare (salience 4700))
;(id-root ?id little)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 pruning|water|milk|need) ;added 'need' by 14anu07
;(viSeRya-viSeRaNa  ?id1 ?id)
;(viSeRya-det_viSeRaNa  ?id1 ?) ;commented by 14anu07
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id WodA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp 	little6   "  ?id "  WodA )" crlf))
;)

;$$$ Modified by 14anu-ban-02(13-03-2016)
;[COUNTER STATEMENT]Once there were three little pigs, Sonu, Monu and Gonu.[sd_verified]
;[COUNTER STATEMENT]eka bAra wIna Cote suara, sonU, monU Ora gonu We.[sd_verified]
;$$$ Modified by 14anu-ban-01 on 05-08-2014
;58520:The only other members of the household were " his mild little wife " and her pet dog .[Karan Singla]
;इस परिवार के दूसरे सदस्य थे - उसकी विनम्र छोटी - सी  पत्नी और उनका पालतू कुत्ता .[Karan Singla]
;@@@ Added by Nandini (11-12-13)
;There was little snow on the lower reaches of the ski run. (via mail)
;skI ke pattI ke nicale BAgoMpara baraPa nahiM ke barAbar WI.
;Re-Modified by Shirisha Manju --- Suggested by Chaitanya Sir (19-06-14)
;Removed (viSeRya-on_saMbanXI  ?id1 ?) relation and (id-cat_coarse ?id adjective)
;added (not (viSeRya-det_viSeRaNa  ?id1 ?))
;The burgeoning metropolis like Bangalore has little time to spare a thought for its dead.
;बङ्गलोर जैसा मुकुलित होता हुआ महानगर के पास उसके मृत के लिए विचार का नहीं के बराबर समय है -- added by 14anu19
(defrule little7
(declare (salience 4710))
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(not (viSeRya-saMKyA_viSeRaNa  ?id1 ?))	;Added by 14anu-ban-02(13-03-2016)
(not (viSeRya-det_viSeRaNa  ?id1 ?))
(not (id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) );added by 14anu-ban-01 on 5-08-14.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nahIM_ke_barAbara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp 	little7   "  ?id "  nahIM_ke_barAbara )" crlf))
)

;$$$Modified by 14anu-ban-08 (25-03-2015)       ;added constraint, Run on parser 2
;@@@ Added by Nandini (18-12-13)
;Mongla rejoined them a little later.
;mongla  WodI xera bAxa  unako milA.
(defrule little8
(declare (salience 6001))    
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 later)
(id-root ?id2 rejoin)      ;added by 14anu-ban-08 (25-03-2015)
(viSeRya-viSeRaNa  ?id1 ?id)
(kriyA-aXikaraNavAcI  ?id2 ?id1)   ;added 'id2' by 14anu-ban-08 (25-03-2015)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp 	little8   "  ?id "  WodA )" crlf))
)

;@@@ Added by Nandini (21-1-14)
;My words has little effect on her.[via mail]
;mere SabxoM kA usapara WodA praBAva hE.
(defrule little9
(declare (salience 5010))
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 effect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp    little9   "  ?id "  WodA )" crlf))
)

;$$$Modified by 14anu-ban-08 (25-03-2015)      ;constraint added
;@@@ Added by 14anu11
;We have seen in the last chapter how his intellect came to grips with the society of bis time and how it strove to bring a little more sweetness and light into this world of ours .
;पिछले अध्याय में हमने देखा कि कैसे उसकी बुढद्धि ने समकालीन समाज की नब्ज पकडऋ ली थी और कैसे उसने हमारी दुनिया में अधिक मिठास भरी और प्रकाशमय बनाने की अनथक कोशिश की .
(defrule little10
(declare (salience 6000))
(id-root ?id little)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(viSeRya-det_viSeRaNa  ?id ?id2)
(id-root ?id1 more)    ;added by 14anu-ban-08 (25-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  little.clp    little10  "  ?id "  WodA )" crlf))
)


;"little","Adj","1.WodA"
;Give me a little salt.
;It's little away from use.
;--"2.CotA"
;A nice little girl.
;When I was a little child my mother used to carry me in her lap.
;--"3.mahawvahIna"
;He has a little problem with his speech.
;"littoral","N","1.wata"
;
;
