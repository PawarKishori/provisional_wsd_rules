;@@@ Added by 14anu-ban-07,(02-02-2015)
;You'll have to unscrew the handles to paint the door.(oald)
;आपको दरवाजा रँगने के लिए हैन्डल का पेच खोलना पडेंगे . (self)
(defrule unscrew0
(id-root ?id unscrew)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peca_Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unscrew.clp 	unscrew0   "  ?id "  peca_Kola )" crlf))
)

;@@@ Added by 14anu-ban-07,(02-02-2015)
;I can't unscrew the lid of this jar. (oald)
;मैं इस मर्तबान का ढक्कन नहीं खोल सकता हूँ . (self)
;I unscrew the cap again.(coca)
;मैं फिर से ढक्कन खोलता हूँ . (self)
(defrule unscrew1
(declare (salience 1000))
(id-root ?id unscrew)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 lid|cap)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unscrew.clp 	unscrew1   "  ?id "  Kola )" crlf))
)
