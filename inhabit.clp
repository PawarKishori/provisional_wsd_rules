;@@@ Added by 14anu-ban-06 (01-12-2014)
;He reflected on what a strange world Stephen and Veronica inhabited.(OALD)
;उसने विचार किया कि स्टीवन और वरॉनिक कितने विचित्र विश्व में  निवास करते हैं . (manual)
(defrule inhabit0
(declare (salience 0))
(id-root ?id inhabit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nivAsa_kara))
(assert (kriyA_id-subject_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inhabit.clp    inhabit0   "  ?id "  nivAsa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  inhabit.clp      inhabit0   "  ?id " meM )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (01-12-2014)
;Rajrappa/Chinmastika temple is inhabited at the confluence place of river Damodar and river Bheda (river Bhairvi) .(total tourism corpus)
;रजरप्पा/छिन्नमस्तिका मंदिर दामोदर नदी और भेड़ा नदी (भैरवी नदी) के संगम स्थल पर बसा हुआ है .(total tourism corpus)
;Giridih is a very much ancient and beautiful town which is inhabited in lap of comely mountains and hills , and all along been attracting the tourists especially tourists from West Bengal towards itself .(total tourism corpus)
;गिरिडीह एक बेहद प्राचीन एवं सुन्दर शहर जो मनोरम पहाड़ एवं पहाड़ियों के गोद में बसा हुआ है , तथा शुरू से ही भ्रमणकारियों विशेषकर पश्चिम बंगाल से पर्यटकों को अपनी ओर आकर्षित करता रहा है .(total tourism corpus)
(defrule inhabit1
(declare (salience 2000))
(id-root ?id inhabit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-in_saMbanXI  ?id ?)(kriyA-at_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id basA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inhabit.clp    inhabit1   "  ?id "  basA_ho )" crlf))
)

