
;"flaked","Adj","1.tukadZoM_meM"
;Flaked cherries were used in this dish.
(defrule flake0
(declare (salience 5000))
(id-root ?id flake)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id flaked )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id tukadZoM_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  flake.clp  	flake0   "  ?id "  tukadZoM_meM )" crlf))
)


;@@@ Added by 14anu-ban-05 on (10-03-2015)
;Cook until the fish flakes easily with a fork.[OALD]
;तब तक  पकाओ जब तक मछली खाने के काँटे के साथ आसानी से टुकडे-टुकडे नही हो जाती है . [manual]

(defrule flake3
(declare (salience 4801))
(id-root ?id flake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tukade-tukade_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flake.clp 	flake3   "  ?id "  tukade-tukade_ho_jA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-03-2015)
;Patches of skin are starting to flake off.[cald]
;त्वचा के भागों का  छूटना शुरु हो रहा हैं .		[manual] 

(defrule flake4
(declare (salience 4802))
(id-root ?id flake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flake.clp  flake4  "  ?id "  "  ?id1 "  CUta  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-03-2015)
;I got home and flaked out on the sofa.[cald]
;मैं घर पहुँचा और सोफे पर ढह गया. [manual]

(defrule flake5
(declare (salience 4802))
(id-root ?id flake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Daha_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flake.clp  flake5  "  ?id "  "  ?id1 "  Daha_jA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-03-2015)
;I'm tired of relying on other people - they keep flaking out on me.[cald]
;मैं अन्य लोगों पर विश्वास कर के थक चुका हूँ- वे  मेरे साथ कुछ भी उल्टा-सीधा करते रहते हैं.[manual]  ;suggested by Chaitanya Sir
(defrule flake6
(declare (salience 4802))
(id-root ?id flake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 out)
(kriyA-on_saMbanXI  ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kuCa_BI_ultA-sIXA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flake.clp  flake6  "  ?id "  "  ?id1 " kuCa_BI_ultA-sIXA_kara)" crlf))
) 


;@@@ Added by 14anu-ban-05 on (10-03-2015)
;The real pleasure of being one with the snow is in the rural zone where mountains , fields , trees , houses , stones , grass , say on everything snow spreads itself as if flakes of Pinji cotton has been decorated systematically .[tourism]
;barPa se ekAkAra hone kA asalI majA wo Kule grAmINa aMcala meM hE jahAz pahAdZa , Kewa , vqkRa , Gara , pawWara , GAsa kahie hara cIja para barPa yUz biCa jAwI hE mAno piMjI huI rUI ke PAhe karIne se sajA xie hoM .[tourism]

(defrule flake7
(declare (salience 4901))
(id-root ?id flake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 cotton)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PAhe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flake.clp 	flake7   "  ?id "  PAhe )" crlf))
)

;------------------------ Default Rules ----------------------

;"flake","N","1.sUkRma_tukadZA"
;Snow flakes were falling in the valley.
(defrule flake1
(declare (salience 4900))
(id-root ?id flake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUkRma_tukadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flake.clp 	flake1   "  ?id "  sUkRma_tukadZA )" crlf))
)

;"flake","V","1.parawa_karanA"
;He used his knife to flake off the stick.
(defrule flake2
(declare (salience 4800))
(id-root ?id flake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flake.clp 	flake2   "  ?id "  parawa_kara )" crlf))
)

