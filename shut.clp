;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu04 on 3-July-2014
;Example changed by 14anu-ban-01 on (30-12-2014)
;I wish someone would shut off that car alarm.[oald]
;मैं चाहता हूँ  कि  कोई व्यक्ति  उस कार के अलार्म को बन्द कर दे [self]
(defrule shut_tmp3
(declare (salience 5100))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-upasarga ?id ?id1)
(id-root ?id1 off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banxa_kara_xe ));changed meaning from "baMxa_kara" to "banxa_kara_xe" by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut_tmp3  "  ?id "  " ?id1 "   banxa_kara_xe )" crlf));changed meaning from "baMxa_kara" to "banxa_kara_xe" by 14anu-ban-01 on (30-12-2014)
)

;@@@ Added by 14anu04 on 3-July-2014
;She kicked her under the table to shut her up. (oxford)
;उसने उसको चुप करने के लिए मेज के नीचे से उसको लात मारी . 
;उसने उसको चुप करने के लिए मेज के नीचे से उसको ठोकर मारी . [Translation improved by 14anu-ban-01 on (30-12-2014)]
(defrule shut_tmp2
(declare (salience 5100))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-upasarga ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cupa_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut_tmp2  "  ?id "  " ?id1 "   cupa_kara )" crlf))
)

;@@@ Added by 14anu04 on 3-July-2014
;Sunglasses shut out 99% of the sun's harmful rays. (oxford)
;धूपचश्मा सूरज की हानिकारक किरणों का 99 % रोकते हैं. 
;धूप के चश्मे सूरज की हानिकारक किरणों को 99 % तक रोकते हैं. [Translation improved by 14anu-ban-01 on (30-12-2014)]
(defrule shut_tmp
(declare (salience 5100))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-upasarga ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 roka ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut_tmp  "  ?id "  " ?id1 "   roka )" crlf))
)

;@@@ Added by jagriti(21.1.2014)
;They shut him from their circle.
;उन्होंने अपनी मंडली से उसको निकाल दिया .  
(defrule shut0
(declare (salience 5000))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-from_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shut.clp 	shut0   "  ?id "  nikAla_xe )" crlf))
)
;@@@ Added by jagriti(21.1.2014)
;I can shut down easily in wrestling.
;मैं उसे कुश्ती करने में आसानी से हरा सकता हूँ .
(defrule shut1
(declare (salience 4900))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-upasarga ?id ?id1)
(id-root ?id1 down)
(kriyA-in_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 harA_xe ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut1  "  ?id "  " ?id1 "   harA_xe )" crlf))
)
;@@@ Added by jagriti(21.1.2014)
;The factory has been shut down through lack of work.
;फैक्टरी को कार्य की कमी के कारण बन्द कर दिया गया है .
(defrule shut2
(declare (salience 4800))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-upasarga ?id ?id1)
(id-root ?id1 down)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banxa_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut2  "  ?id "  " ?id1 "   banxa_kara )" crlf))
)

(defrule shut3
(declare (salience 4700))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shut.clp 	shut3   "  ?id "  banxa_kara )" crlf))
)

;....Default rule....
(defrule shut4
(declare (salience 100))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shut.clp 	shut4   "  ?id "  banxa_ho )" crlf))
)

;"shut","V","1.banxa karanA"
;When you go out shut the door.
;
;

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu20 on 13/06/2014
;Shut up your mouth and listen. [sentence improved by 14anu-ban-01 on (30-12-2014)]
;अपना मुँह बद कीजिए और सुनिए .  [Translation improved by 14anu-ban-01 on (30-12-2014)]
(defrule shut5
(declare (salience 6400))
(id-root ?id shut)
(id-word ?id1 up);modified =(+ ?id 1) as ?id1 by 14anu-ban-01 on (30-12-2014)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(kriyA-upasarga  ?id ?id1);modified =(+ ?id 1) as ?id1 by 14anu-ban-01 on (30-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banxa_kara));changed "baxa_kara" to "banxa_kara" by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut5  "  ?id "  " ?id1 "  banxa_kara  )" crlf));changed "baxa_kara" to "banxa_kara" by 14anu-ban-01 on (30-12-2014)
)

;@@@ Added by 14anu20 on 13/06/2014
;You shut up.
;आप चुप रहिए .  [Translation improved by 14anu-ban-01 on (30-12-2014)]
(defrule shut6
(declare (salience 6000))
(id-root ?id shut)
(id-word ?id1 up);modified =(+ ?id 1) as ?id1 by 14anu-ban-01 on (30-12-2014)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1);modified =(+ ?id 1) as ?id1 by 14anu-ban-01 on (30-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cupa_raha));modified by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut6  "  ?id "  " ?id1 "  cupa_raha  )" crlf));modified by 14anu-ban-01 on (30-12-2014)
)


;@@@ Added by 14anu-ban-01 on (30-12-2014)
;A valve immediately shuts off the gas when the lid is closed.  (oxford) 
;जब ढक्कन बंद किया जाता है तब वाल्व तत्काल ही गैस की आपूर्ति बन्द कर देता है.[self]
(defrule shut7
(declare (salience 5100))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-root ?id2 gas|water)
(kriyA-object ?id ?id2)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ApUrwi_banxa_kara_xe ))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  shut.clp	shut7  "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut7  "  ?id "  " ?id1 "   ApUrwi_banxa_kara_xe )" crlf))
)


;@@@ Added by 14anu-ban-01 on (30-12-2014)
;The engines shut off automatically in an emergency.[oald]
; इंजन आपातकालीन स्थिति में स्वतः ही बन्द हो जाते हैं.[self]
(defrule shut8
(declare (salience 5100))
(id-root ?id shut)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-root ?id2 engine|machine)
(kriyA-subject ?id ?id2)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banxa_ho_jA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " shut.clp	shut8  "  ?id "  " ?id1 "   banxa_ho_jA )" crlf))
)

