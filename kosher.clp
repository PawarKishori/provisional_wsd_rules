;@@@Added by 14anu-ban-07,(11-04-2015)
;Their business activities aren't quite kosher.(cambridge)
;उनके उद्योग क्रिया कलाप बिल्कुल विधिसम्मत नहीं हैं . (manual)
(defrule kosher1
(declare (salience 1000))
(id-word ?id kosher)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viXisammawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kosher.clp 	kosher1   "  ?id "  viXisammawa )" crlf))
)

;@@@Added by 14anu-ban-07,(11-04-2015)
;He also thanked fans who made him kosher food.(coca)(parser no. 20)
;उसने प्रशंसको का धन्यवाद दिया जिन्होंने उसके लिए यहूदी ढङ्ग से पका आहार बनाया था. (manual)
(defrule kosher2
(declare (salience 1100))
(id-word ?id kosher)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 food|meat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yahUxI_DaMga_se_pakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kosher.clp 	kosher2   "  ?id "  ahUxI_DaMga_se_pakA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-07,(11-04-2015)
;A kosher restaurant.(cambridge)
;यहूदी ढङ्ग का भोजनालय .  (manual)
(defrule kosher0
(id-word ?id kosher)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yahUxI_DaMga_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kosher.clp   kosher0   "  ?id "  yahUxI_DaMga_kA )" crlf))
)

