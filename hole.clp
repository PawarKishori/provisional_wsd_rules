

;@@@ Added by Prachi Rathore[13-2-14]
;We believe the gang are holed up in the mountains. [oald]
;हम मानते हैं दल पर्वतों में छुप गये हैं . 
(defrule hole2
(declare (salience 5000))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Cupa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hole.clp 	hole2 "  ?id "  " ?id1 " Cupa_jA)" crlf))
)

;@@@ Added by 14anu-ban-06 (24-01-2015)
;The peep holes of these forts adorned with fine carving and engraving attract towards itself .(parallel corpus)
;इन  हवेलियों  की  बारीक  कारीगरी  व  नक्काशी  युक्त  झरोखे  अपनी  ओर  आकर्षित  करते  हैं  ।(parallel corpus)
(defrule hole3
(declare (salience 5000))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id2)
(id-root ?id2 peep)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 fort|palace)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 JaroKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hole.clp 	hole3 "  ?id "  " ?id2 " JaroKA)" crlf))
)

;@@@ Added by 14anu-ban-06 (02-02-2015)
;I am not going to bring up my child in this hole.(OALD)
;मैं इस गन्दी जगह में मेरे बच्चे का पालन पोषण करने नहीं जा रहा हूँ . (manual)
(defrule hole4
(declare (salience 5100))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id1 ?id)
(id-root ?id1 bring)
(kriyA-object ?id1 ?id2)
(id-root ?id2 child|children|son|daughter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaMxI_jagaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hole.clp 	hole4   "  ?id "  gaMxI_jagaha )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-02-2015)
;I don't believe what she says—her story is full of holes.(OALD)
;मैं विश्वास नहीं करता हूँ जो  वह कहती है  — उसकी कहानी गलतियों से भरी हुई है .(manual)
(defrule hole5
(declare (salience 5100))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id2 ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id2)
(id-root ?id1 story|history)
(id-root ?id2 full)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galawI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hole.clp 	hole5   "  ?id "  galawI )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-02-2015)
;After his wife left, there was a gaping hole in his life.(OALD)
;उसकी पत्नी गुजरने के बाद, उसके जीवन में एक खालीपन था . (manual)
(defrule hole6
(declare (salience 5100))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 gape)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 KAlIpana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hole.clp 	hole6 "  ?id "  " ?id1 " KAlIpana)" crlf))
)

;@@@ Added by 14anu-ban-06 (02-02-2015)
;He was found not guilty because of holes in the prosecution case.(OALD)
;वह अभियोग पक्ष के मुकदमे में गलतियों की वजह से अपराधी नहीं पाया गया था . (manual)
(defrule hole7
(declare (salience 5100))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?id1)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2)
(id-root ?id2 prosecution)
(id-root ?id1 case)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galawI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hole.clp 	hole7   "  ?id "  galawI )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-02-2015)
;The roads were still full of holes. (COCA)
;सडकें गड्ढों से अभी भी भरी हुईं थीं . 
(defrule hole8
(declare (salience 5100))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id2 ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id2)
(id-root ?id1 road)
(id-root ?id2 full)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gadDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hole.clp 	hole8   "  ?id "  gadDA )" crlf))
)

;-------------------- Default Rules ----------------------

(defrule hole0
(declare (salience 5000))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cexa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hole.clp 	hole0   "  ?id "  Cexa )" crlf))
)

;"hole","N","1.Cexa"
;usane'Hole' ko simeMta se Bara xiyA
;
(defrule hole1
(declare (salience 4900))
(id-root ?id hole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cexa_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hole.clp 	hole1   "  ?id "  Cexa_banA )" crlf))
)

;"hole","V","1.Cexa_banAnA"
;sImA vAsiyoM ke makAna kI xIvAroM meM golAbArI se 'hole'(Cexa bana gaye)kiye. 
;

