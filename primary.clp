
(defrule primary0
(declare (salience 5000))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Axya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  primary.clp 	primary0   "  ?id "  Axya )" crlf))
)

;"primary","Adj","1.Axya"
;The doctor cured the disease in its primary stage only.
;--"2.mUla"
;The primary aim of N.S.S. is to produce social volunteers.
;--"3.prAWamika"
;One must possess primary education for high school study.
;
(defrule primary1
(declare (salience 4900))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_prakAra_kA_nirvAcana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  primary.clp 	primary1   "  ?id "  eka_prakAra_kA_nirvAcana )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthai 23-1-2014
;The primary requisite of the University at the time was accommodation for post-graduate classes and a suitable library. [Gyannidhi]
;उस समय विश्वविद्यालय की मूल जरूरत स्नातकोत्तर कक्षाओं के लिए आवास और सही पुस्तकालय था .
(defrule primary2
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 requisite|necessity|requirements|need|cause|unit|source)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  primary.clp 	primary2   "  ?id "  mUla )" crlf))
)


;$$$ Modified by 14anu-ban-09 on (13-11-2014)
;For example, soybeans are the primary ingredient in many dairy product substitutes (e.g., soy milk,margarine, soy ice cream, soy yogurt, soy cheese, and soy cream cheese) and meat alternatives (e.g. veggie burgers). [Agriculture]
;उदाहरण के लिए, सोयाबीन अनेक दैनिक उत्पाद के विकल्प की मुख्य सामग्री है (उदाहरण, सोया दूध, सोया का दही, कृत्रिम मक्खन, सोया पनीर, और सोया मलाई पनीर) और माँस के विकल्प (उदाहरण, वेजी बर्गर). [Self]
;@@@ Added by Sonam Gupta MTech IT Banasthai 24-1-2014
;Her primary responsibility is to train new employees. [Cambridge]
;उसका मुख्य उत्तरदायित्व नये कर्मचारियों को प्रशिक्षण देना है .
(defrule primary3
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 responsibility|job|concern|charge|task|duty|aim|importance|goal|objective|ingredient) ;added 'ingredient' by 14anu-ban-09 on (13-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  primary.clp 	primary3   "  ?id "  muKya )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthai 24-1-2014
;The disease is still in its primary stage. [OALD]
;बीमारी अब भी प्रारम्भिक चरण में है .
(defrule primary4
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 stage|training)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAramBika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  primary.clp 	primary4   "  ?id "  prAramBika )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthai 24-1-2014
;Primary teacher. [OALD]
;प्राईमरी शिक्षक .
(defrule primary5
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 teacher|education|school)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praImarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  primary.clp 	primary5   "  ?id "  praImarI )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (09-09-2014)
;Theirs is a culture at risk of extinction – and the primary reason is the colossal 20th-century grab of the waters of the Colorado River. [AnusaarakaGoogle_17_Jul_2014]
;apanI saMskqawi vilupwa hone ke Kaware me hE - Ora isakA prAWamika kAraRa 20 vIM saxI meM kolorAdo naxI kA pAnI BArI mAwrA me hadapa liyA WA. [Own Manual]  
;@@@ Added by Sonam Gupta MTech IT Banasthai 24-1-2014
;A primary care physician.  [MW]
;एक प्राथमिक देखभाल चिकित्सक .
(defrule primary6
(declare (salience 5200))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 care|treatment|physician|reason) ;added 'reason' by 14anu-ban-09
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAWamika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  primary.clp 	primary6   "  ?id "  prAWamika )" crlf))
)

;"primary","N","1.eka_prakAra_kA_nirvAcana"
;He won by a great margin in primary elections.
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_primary2
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 requisite|necessity|requirements|need|cause|unit|source)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   sub_samA_primary2   "   ?id " mUla )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_primary2
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 requisite|necessity|requirements|need|cause|unit|source)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   obj_samA_primary2   "   ?id " mUla )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_primary3
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 responsibility|job|concern|charge|task|duty|aim|importance|goal|objective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   sub_samA_primary3   "   ?id " muKya )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_primary3
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 responsibility|job|concern|charge|task|duty|aim|importance|goal|objective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   obj_samA_primary3   "   ?id " muKya )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_primary4
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 stage|training)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAramBika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   sub_samA_primary4   "   ?id " prAramBika )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_primary4
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 stage|training)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAramBika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   obj_samA_primary4   "   ?id " prAramBika )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_primary5
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 teacher|education|school)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praImarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   sub_samA_primary5   "   ?id " praImarI )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_primary5
(declare (salience 5500))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 teacher|education|school)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praImarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   obj_samA_primary5   "   ?id " praImarI )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_primary6
(declare (salience 5200))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 care|treatment|physician)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAWamika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   sub_samA_primary6   "   ?id " prAWamika )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_primary6
(declare (salience 5200))
(id-root ?id primary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 care|treatment|physician)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAWamika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " primary.clp   obj_samA_primary6   "   ?id " prAWamika )" crlf))
)
