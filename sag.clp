;########################################################################

;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################`

;@@@ Added by Jagrati
;Moreover, if the mirror 'dish' is very large it tends to sag under its own weight.[gyanidhi corpus]
;इसके अलावा यदि दर्पण बहुत बड़ा होगा तो यह संभावना रहती है कि वह अपने ही वजन से किसी ओर झुक जाए।
(defrule sag0
(declare (salience 5000))
(id-root ?id sag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-under_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Juka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sag.clp  	sag0   "  ?id "  Juka )" crlf))
)

;@@@ Added by 14anu-ban-11 on (11-02-2015)
;Your skin starts to sag as you get older.(oald)
;आपकी त्वचा लटकना शुरु करती है जब आप वृद्ध हो जाते है . (self)
(defrule sag3
(declare (salience 4901))
(id-root ?id sag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 skin)	
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sag.clp   sag3  "  ?id "  lataka )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-02-2015)
;A sag in machinery orders.[oald]
;उपकरणों के ऑर्डर में कमी/गिरावट . [self]
(defrule sag4
(declare (salience 4900))
(id-root ?id sag)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 order|recession|popularity|consumer|sale) 
(viSeRya-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamI/girAvata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sag.clp   sag4 "  ?id "  kamI/girAvata )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-02-2015)
;This time Darla gazed at the round sagging face.[coca] ;parse no. 547
;इस बार डॉर्ला ने गोल शिथिल चेहरे को गौर से देखा. [self]
(defrule sag5
(declare (salience 4900))
(id-word ?id sagging)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 face) 
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiWila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sag.clp   sag5 "  ?id "  SiWila )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-02-2015)
;The sullen looks, the pain,the scars on older women's sagging breasts and aging men's sunken chests, are haunting.[coca] :parse no. 831
;वे उदास निगाहें, दर्द,बूढ़ी स्त्रियों के ढीली-ढाली छातियों एवं बूढ़े होते हुए पुरुषों की  धँसी हुई/सिकुडी हुई छातियों पर पड़े हुए दाग/निशान  बार बार याद आते हैं [self]
(defrule sag6
(declare (salience 4900))
(id-word ?id sagging)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 breast|chest) 
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DIlA_DAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sag.clp   sag6 "  ?id "  DIlA_DAlA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-02-2015)
;Old secretaries with their sagging flesh didn't interest him.[coca] :parse no. 7
;लटकते हुए मांस वाली वृद्ध सेक्रेटि्रयों में उसे कोई दिलचस्पी/रुचि  नहीं थी . [self]
(defrule sag7
(declare (salience 4900))
(id-word ?id sagging)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 flesh|skin) 
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id latakawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sag.clp   sag7 "  ?id "  latakawA_huA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-02-2015)
;A sagging roof.[cald]  :parse no. 6
;एक झुकी हुई/झूलती हुई छत. [self]
(defrule sag8
(declare (salience 0))
(id-word ?id sagging)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JukA_huA/JulawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sag.clp   sag8 "  ?id "  JukA_huA/JulawA_huA )" crlf))
)
;-------------------------------------- Default rules -------------------------------
;@@@ Added by Jagrati
;Their share of the vote sagged badly at the last election. [iit-bombay]
; अंतिम चुनाव में अपने हिस्से के मतदान  बुरी तरह से कम हो गये.
(defrule sag1
(declare (salience 4900))
(id-root ?id sag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sag.clp  	sag1   "  ?id "  kama_ho )" crlf))
)

;@@@ Added by Jagrati
(defrule sag2
(declare (salience 4800))
(id-root ?id sag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Jola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sag.clp  	sag2   "  ?id "  Jola )" crlf))
)
