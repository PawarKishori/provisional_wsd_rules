;$$$ Modified by 14anu-ban-06 (06-12-2014)
;Added by sheetal(3-01-10).
;Research is a highly creative process , in which new ideas are always needed .(parallel corpus)
;शोध एक अत्यंत सृजनात्मक प्रक्रिया है , जिसमें सदैव नए विचारों की जरूरत होती है .(parallel corpus)
(defrule highly0
(declare (salience 0));salience reduced from '5000' to '0' by 14anu-ban-06 (06-12-2014)
(id-root ?id highly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb);added by 14anu-ban-06 (06-12-2014)
;(viSeRaNa-viSeRaka  ?viSe ?id);commented by 14anu-ban-06 (06-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyanwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  highly.clp      highly0   "  ?id "  awyanwa )" crlf))
)
;Political interference in defence matters is highly undesirable . 


;@@@ Added by 14anu-ban-06 (06-12-2014)
;She speaks highly of you.(OALD)
;वह आप के बारे में अच्छा बोलती है .(manual)
;His teachers think very highly of him.(OALD)
;उसके शिक्षक उसके बारे में बहुत अच्छा सोचते हैं .(manual)
(defrule highly1
(declare (salience 5100))
(id-root ?id highly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  highly.clp      highly1   "  ?id "  acCA )" crlf))
)

;@@@ Added by 14anu-ban-06 (06-12-2014)
;Also called the manchester of the east , today , ahmedabad is one of the most highly industrialized cities of india .(parallel corpus)
;मेनचेस्टर ओःफ द ईस्ट् भी कहा जाने वाला , अहमदाबाद आज , भारत के बडे औद्योगिक शहरों में से एक है .(parallel corpus)
;India has become a highly industrialized nation.(COCA)
;भारत एक बडा औद्योगिक राष्ट्र बन गया है . (manual)
(defrule highly2
(declare (salience 5100))
(id-root ?id highly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRaNa-viSeRaka ?id1 ?id)
(viSeRya-viSeRaNa ?id2 ?id1)
(id-root ?id2 city|country|nation|state)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  highly.clp      highly2   "  ?id "  badZA )" crlf))
)
