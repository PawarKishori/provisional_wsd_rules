;@@@ Added by 14anu-ban-11 on (13-03-2015)
;Politics is the only thing that separates us.(oald)
;राजनीति एकमात्र चीज है जो हमें अलग कर देती है . (self)
(defrule separate2
(declare (salience 4901))
(id-root ?id separate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 us)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  separate.clp    separate2   "  ?id "  alaga_kara_xe)" crlf))
)

;$$$ Modified by 14anu-ban-01 on (07-11-2014)
;These charged particles get separated by updrafts in the clouds and gravity.[NCERT corpus]
;ये आवेशित कण बादलों के ऊर्ध्ववाह एवं गुरुत्व के कारण पृथक हो जाते हैं[NCERT corpus]
(defrule separate0
(declare (salience 5000))
(id-root ?id separate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga/pqWaka));'pqWaka' added by 14anu-ban-01 on (07-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  separate.clp 	separate0   "  ?id "  alaga/pqWaka )" crlf)) ;'pqWaka' added by 14anu-ban-01 on (07-11-2014)
)

;"separate","Adj","1.alaga/pqWaka"
;The children sleep in seperate beds.
;--"2.Binna"
;That is a seperate issue && irrelevant to our discussion.
;
(defrule separate1
(declare (salience 4900))
(id-root ?id separate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  separate.clp 	separate1   "  ?id "  alaga_ho )" crlf))
)

;"separate","V","1.alaga_ho"
