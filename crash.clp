;@@@ Added by 14anu13 on 20-06-14
;No one should not have crash diets regularly.
;किसी को प्राय:हल्का भोजन नहीं करना चाहिए |
(defrule crash2
(declare (salience 5000))
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1  ?id)
(id-root ?id1 diet|food)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash2   "  ?id "  halkA )" crlf))
)

;@@@ Added by 14anu13 on 20-06-14
;A girl was killed yesterday in a car crash .
;कल एक लड़की कार दुर्घटना में मारी गयी |
(defrule crash3
(declare (salience 5000))
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-involving_saMbanXI  ?id  ?id1)(kriyA-in_saMbanXI ?id2  ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurGatanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash3   "  ?id "  xurGatanA )" crlf))
)


;@@@ Added by 14anu13 on 20-06-14
;She heard the crash of shattering glass as the vehicles collided.
;जब वाहन टकराए उसने काँच के टूटने की कड़कड़ाहट सुनी |
(defrule crash4
(declare (salience 5000))
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 shatter)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadZakadZAhata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash4   "  ?id "  kadZakadZAhata )" crlf))
)


;$$$ Modified  by 14anu-ban-03 (06-04-2015)
;@@@ Added by 14anu13 on 20-06-14
;The 1987 stock market crashed.
;1987 में  स्टाक बाजार में गिरावट आई | 	
(defrule crash5
(declare (salience 5000))
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 market)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id girAvata_A))  ;meaning changed from 'girAvata' to 'girAvata_A' by 14anu-ban-03 (06-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash5   "  ?id "  girAvata_A )" crlf))
)


;@@@ Added by 14anu13 on 20-06-14
;My laptop crashed yesterday. 
;मेरा लैपटाप कल खराब हो गया |
(defrule crash6
(declare (salience 5000))
(id-root ?id crash)
(id-root ?id1 laptop|mobile|computer|device)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id  ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash6   "  ?id "  KarAba_ho )" crlf))
)

;@@@ Added by 14anu13 on 20-06-14
; The car crashed into the lamp post.
; कार लैंप पोस्ट से टकराई | 
(defrule crash7
(declare (salience 5000))
(id-root ?id crash)
(id-root ?id1 car|bus|truck|bike)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash7   "  ?id "  takarA )" crlf))
)

;@@@ Added by 14anu-ban-03 (05-02-2015)
;The branch crashed down on my car. [same clp file]
;yaha SAKa merI gAdZI para gira padI. [same clp file]
(defrule crash8
(declare (salience 5000))
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 down)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 gira_padA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " crash.clp  crash8  "  ?id "  " ?id1 "  gira_padA  )" crlf))
)

;@@@ Added by 14anu-ban-03 (05-02-2015)
;My son's friends crashed our house last weekend. [same clp file]
;मेरे बेटे के मित्र पिछले सप्ताहान्त हमारे घर अनिमन्त्रित आए . [Manual]
(defrule crash9
(declare (salience 5000))
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(viSeRya-viSeRaNa ?id ?id2)
(id-root ?id2 house)             ;more constraint can be added
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id animaMwriwa_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash9   "  ?id " animaMwriwa_A )" crlf))
)


;@@@ Added by 14anu-ban-03 (06-04-2015)
;The financial crash blackened the image of bankers.[cald]
;आर्थिक गिरावट ने बैंक का सञ्चालकों की छवि खराब की . [self]
(defrule crash10
(declare (salience 5000))  
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 financial)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id girAvata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash10   "  ?id "  girAvata )" crlf))
)



;------------------ Default rules --------------------
(defrule crash0
(declare (salience 00))  ;decreased salience by 14anu-ban-03 (15-12-2014)
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XamAkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash0   "  ?id "  XamAkA )" crlf))
)

;"crash","N","1.XamAkA"
;The crash occurred during a thunderstorm && the system has been down ever
;since
;
(defrule crash1
(declare (salience 4900))
(id-root ?id crash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Daha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crash.clp 	crash1   "  ?id "  Daha )" crlf))
)

;"crash","VI","1.DahanA"
;The branch crashed down on my car
;--"2.takarAnA"
;The car crashed through the glass door
;The plane crashed into the fields
;The terorists crashed the car into the gate of the palace
;--"3.animaMwriwa_AnA"
;My son's friends crashed our house last weekend
;Let's crash the party!
;--"4.kAma_karanA_banxa_kara_xenA"
;My computer crashed last night
;
;LEVEL 
;Headword : crash
;
;Examples --
;
;1.The branch crashed down on my car.
;yaha SAKa merI gAdZI para gira padI.
;2.The car crashed through the glass door.
;gAdZI kAzca ke xaravAje se BidZa kara nikala gaI.
;3.The plane crashed into the hills.
;vAyuyAna pahAdZo meM takarA kara gira gayA.
;4.My computer crashed last night
;kala rAwa ko merA kampyUtara bETa gayA.
;
;uparaliKiwa vAkyoM meM "crash" ke jo Binna lagawe arWa A raheM hEM, unameM vAswava meM
;kuCa samAnawA hE, jiwake kAraNa ina saBI arWoM ko hama saMbaMXiwa mAna sakawe hEM. 
;
;"crash" Sabxa kA prayoga jisa saMxarBa meM ho rahA hE, vaha hE, kisI vaswu ke takarA kara,
; tUtane Ora girane ke saMxarBa meM, Ora uparaliKiwa vAkya 1-3 meM BI "crash" kA
; prayoga isI saMxarBa meM ho rahA hE.
;
;kisI vaswu ke takarA kara tUtakara girane se "XadZAm-BadZAm" kI Xvani AwI hE. wo hama aba
;uparaliKiwa vAkya 1-3 meM "crash" Sabxa kA arWagrahaNa hama Xvani ke isa BAva se kara sakawe
;hEM.
;
;vAkya 4. meM "crash" kA jo arWa A rahA hE, use "crash" ke uparaliKiwa arWa se 
;saMbaMXiwa karanA kaTina lagawA hE, kyoM ki kompyUtara takarA kara tUtakara gira nahIM sakawA hE. 
;kinwu yaxi isa vAkya ke saMxarBa meM hama isa arWa kA lAkRaNika viswAra kareM wo isa 
;saMxarBa meM BI hama "crash" ke uparaliKiwa arWa kA arWagrahaNa kara sakawe hEM.
;
;wo aba hama "crash" ke lie sUwra isa prakARa xe sakawe hEM.
;
;anwarnihiwa sUwra ;
;
;takarAkara-tUtakara-giranA - takarAkara-tUtakara-girane kI Xvani - XadZAm-BadZAm
;  
;
;sUwra : XadZAm-BadZAma
;
;"crash","V","XadZAm-BadZAm"
