;@@@ Added by 14anu-ban-11 on (02-03-2015)
;She designed to go far in the world of business.(oald)
;उद्योग के क्षेत्र  में सफ़ल होने  के लिए उसने  योजना बनाई .  (self)
(defrule world0
(declare (salience 10))
(id-root ?id world)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 business)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  world.clp 	world0   "  ?id "  kRewra)" crlf))
)


;@@@ Added by 14anu-ban-11 on (02-03-2015)
;When his wife died, his entire world was turned upside down.  (oald)
;जब उसकी पत्नी मरी,उसकी सम्पूर्ण जिंदगी अस्त व्यस्त हो गयी थी . (self)
(defrule world2
(declare (salience 30))
(id-root ?id world)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 entire)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZinxagI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  world.clp 	world2   "  ?id "  jZinxagI)" crlf))
)


;@@@ Added by 14anu-ban-11 on (02-03-2015)
; A map of the world.(oald)
;विश्व का मानचित्र .  (self)
(defrule world1
(declare (salience 00))
(id-root ?id world)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  world.clp 	world1   "  ?id "  viSva)" crlf))
)
