;$$$Modified by Anita --24.5.2014
;One advice often made is, "Study hard and you will do well in life."  [SOCIOLOGY TEXT BOOK CLASS XI]
; अक्सर एक सलाह दी जाती है  "अत्यधिक अध्ययन करो और आप जीवन में अच्छा करेंगे"।
(defrule well0
(declare (salience 5000))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?kri ?id) ; Add relation
(id-cat_coarse ?id adverb) ; [change the category]
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well0   "  ?id "  acCA )" crlf))
)

;default_sense && category=adjective	niroga	0
;"well","Adj","1.niroga"
;We are all well here.
;--"2.sanwoRajanaka"
;One should have a sound mind && well disciplined body.
;
(defrule well1
(declare (salience 4900))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCI_waraha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well1   "  ?id "  acCI_waraha )" crlf))
)


; Find a comfortable place , and encourage him - he really wants to do well .
;एक आरामदायक जगह ढूंढ निकालें और उसे उत्साहित करें - वह वास्तव में अच्छा करना चाहता है .
;@@@added by avni(14anu11)
(defrule well5
(declare (salience 5000))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well5   "  ?id "  acCA )" crlf))
)

;"well","Adv","1.acCI_waraha"
;The students behaved well.
;
(defrule well2
(declare (salience 4800))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id interjection)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id are_vAha!))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp     well2   "  ?id "  are_vAha!)" crlf))
)

;"well","Interj","1.are_vAha!"
;Well, what a thing to say.
;
(defrule well3
(declare (salience 4700))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuzA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well3 "  ?id "  kuzA )" crlf))
)

;"well","N","1.kuzA"
;There is a well near the tree.
;

;@@@ Added by 14anu-ban-11 on (17-10-2014)
;Consider a molecule well inside a liquid.(Ncert) 
;द्रव के भीतर एक अणु लीजिए.(Ncert)
(defrule well4
(declare (salience 5100))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?kri ?id)
(id-root ?kri consider)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNa_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well4   "  ?id "  pUrNad
_rUNa_se )" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-03-2015)
;The film was well good. (cambridge)
;सिनेमा बहुत अच्छी थी . (self)
(defrule well05
(declare (salience 5101))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka  ?id1 ?id)
(id-root ?id1 good)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well05   "  ?id "  bahuwa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-03-2015)
;Tears were welling up in her eyes. (oald)
;आँसू उसकी आँखों मसे बह रहे थे . (self)
(defrule well6
(declare (salience 5102))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well6  "  ?id "  baha)" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-03-2015)
;The sky darkened as thick smoke billowed from the blazing oil well. (cambridge)
;आसमान काला हो गया जब  घना धुआँ धधकता हुए तेल स्रोत से पूरी तरह से बाहर निकल गया.(self)
(defrule well7
(declare (salience 5103))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(kriyA-from_saMbanXI  ?id1 ?id2)
(id-root ?id1 billow)
(id-root ?id2 oil)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id srowa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well7   "  ?id "  srowa)" crlf))
)


;$$$ Modified by Bhagyashri Kulkarni (9-11-2016)
;He is not well. (rapidex)
;वह स्वस्थ नहीं है .
;@@@ Added by 14anu-ban-11 on (27-03-2015)
;Mother and baby are doing well.(oald)
;माँ और शिशु स्वस्थ हैं . (self)
(defrule well8
(declare (salience 5104))
(id-root ?id well)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 do|be)	;Added 'be' by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svasWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  well.clp 	well8   "  ?id "  svasWa)" crlf))
)


