;@@@Added by 14anu-ban-02(22-04-2015)
;He led a reckless and abandoned life and died young.[mw]	;run the sentence on parser no. 5
;उसने अचेत और परित्यक्त जीवन व्यतीत किया और जल्द ही  मर गया .[self] 
(defrule reckless1
(declare (salience 100)) 
(id-root ?id reckless) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 life)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id acewa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  reckless.clp  reckless1  "  ?id "  acewa )" crlf)) 
) 

;@@@ Added by 14anu-ban-04  (23-04-2015)
;The company showed a reckless disregard for the safety of the environment.              [oald]
;कम्पनी ने पर्यावरण की सुरक्षा के लिए लापरवाही दिखाई .                                           [self]
(defrule reckless2
(declare (salience 40))               
(id-root ?id reckless)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 disregard)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reckless.clp 	reckless2   "  ?id "  - )" crlf))
)

;--------------------------default_rules------------------------------------------------

;@@@Added by 14anu-ban-02(22-04-2015)
;Sentence: She was a good rider, but reckless.[oald]
;Translation: वह - अच्छी सवार , परन्तु लापरवाह थी . [self]
(defrule reckless0 
(declare (salience 0)) 
(id-root ?id reckless) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id lAparavAha)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  reckless.clp  reckless0  "  ?id "  lAparavAha )" crlf)) 
) 
