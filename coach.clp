;@@@ Added by 14anu-ban-03 (07-03-2015)
;Her father coached her for the Olympics. [oald]
;उसके पिता ने ओलम्पिक के लिए उसको प्रशिक्षण दिया . [manual]
(defrule coach2
(declare (salience 4900))
(id-root ?id coach)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praSikRaNa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coach.clp 	coach2   "  ?id "  praSikRaNa_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (07-03-2015)
;They believed the witnesses had been coached on what to say. [oald]
;उन्होंने माना कि गवाहो को तैयार किया गया था कि क्या कहना हैं. [manual]
(defrule coach3
(declare (salience 4900))
(id-root ?id coach)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 witness)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coach.clp 	coach3   "  ?id "  wEyAra_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (07-03-2015)
;They went to Italy on a coach tour. [oald]
;वे एक आरामदेह बस मे ईटली की यात्रा पर  गये . [manual]
(defrule coach4
(declare (salience 5000))  
(id-root ?id coach)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 tour)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAmaxeha_basa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coach.clp 	coach4   "  ?id "  ArAmaxeha_basa )" crlf))
)

;------------------------Default Rules--------------------

(defrule coach0
(declare (salience 00))  ;salience reduced  by 14anu-ban-03 (07-03-2015)
(id-root ?id coach)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praSikRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coach.clp 	coach0   "  ?id "  praSikRaka )" crlf))
)

;"coach","V","1.SikRA_xenA"
;Ramakanth Achrekar coached Sachin Tendulkar.
(defrule coach1
(declare (salience 00))  ;salience reduced  by 14anu-ban-03 (07-03-2015)
(id-root ?id coach)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikRA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coach.clp 	coach1   "  ?id "  SikRA_xe )" crlf))
)

