;##############################################################################
;#  Copyright (C) 2013-2014 Pramila (pramila3005 at gmail dot com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;He is difficult boss.
;वह एक टेढा मालिक है.
;vaha eka teDA mAlika hE.

(defrule difficult0
(declare (salience 5000))
(id-root ?id difficult)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id teDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  difficult.clp 	difficult0   "  ?id " teDA  )" crlf))
)


;It is very difficult to calculate.
;यह गणना के लिए बहुत मुश्किल है.
;yaha gaNanA ke lie bahuwa muSkila hE.
(defrule difficult1
(declare (salience 5000))
(id-root ?id difficult)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?id1)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muSkila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  difficult.clp 	difficult1   "  ?id " muSkila  )" crlf))
)

;@@@ Added by 14anu-ban-04 (05-02-2015)
;This attractive property of magnets made it difficult for them to move around.                [NCERT-CORPUS]
;चुम्बकों के इस आकर्षित करने वाले गुण ने उनका घूमना-फिरना दूभर बना दिया था.                                         [NCERT-CORPUS]
(defrule difficult5
(declare (salience 4010))
(id-root ?id difficult)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 move)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuBara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  difficult.clp 	difficult5  "  ?id "  xuBara )" crlf))
)



;----------------------------------- Default rules -----------------------------------------
;$$$ Modified by 14anu-ban-04 on 05-02-2015)
;added example sentence by 14anu-ban-04 on 05-02-2015)
;Environmental pollution is a difficult problem in front of the country.          [hinkhoj]     
;पर्यावरण प्रदुषण देश के समक्ष एक कठिन समस्या है.                                               [hinkhoj]    
(defrule difficult2
(declare (salience 4000))
(id-root ?id difficult)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTina/muSkila))               ; added 'muSkila' by 14anu-ban-04 on 05-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  difficult.clp 	difficult2   "  ?id "  kaTina/muSkila )" crlf))
)



(defrule difficult3
(declare (salience 3000))
(id-root ?id difficult)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cidacidA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  difficult.clp 	difficult3   "  ?id " cidacidA  )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_difficult0
(declare (salience 5000))
(id-root ?id difficult)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id teDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " difficult.clp   sub_samA_difficult0   "   ?id " teDA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_difficult0
(declare (salience 5000))
(id-root ?id difficult)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id teDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " difficult.clp   obj_samA_difficult0   "   ?id " teDA )" crlf))
)
