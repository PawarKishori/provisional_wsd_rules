;$$$Modified by 14anu-ban-08 (09-01-2015)     ;meaning changes from 'BatakI_huyI' to 'BatakI_ho'
;@@@ Added by 14anu03 on 18-june-14
;His hope was misplaced,So was his head. 
;उसकी आशा भटकी हुयी गयी थी,वैसे ही उसका दीमाग था .
(defrule misplace100
(declare (salience 5500))
(id-root ?id misplace)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-word ?id1 hope)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BatakI_ho))            ;changed meaning from 'BatakI_huyI' to 'BatakI_ho' by 14anu-ban-08
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  misplace.clp      misplace100   "  ?id " BatakI_ho)" crlf))                                                 ;changed meaning from 'BatakI_huyI' to 'BatakI_ho' by 14anu-ban-08
)

(defrule misplace0
(declare (salience 5000))
(id-root ?id misplace)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id misplaced )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id KoI_huI_cIja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  misplace.clp  	misplace0   "  ?id "  KoI_huI_cIja )" crlf))
)

;"misplaced","Adj","1.KoI huI cIja"
;He found the misplaced car keys.
;
(defrule misplace1
(declare (salience 4900))
(id-root ?id misplace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galawa_jagaha_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  misplace.clp 	misplace1   "  ?id "  galawa_jagaha_raKa )" crlf))
)

;"misplace","V","1.galawa jagaha raKanA"
;Don't misplace any important document.
;
;
