;@@@ Added by 14anu-ban-06 (25-02-2015)
;This is like some horrible nightmare that I can't wake up from. (COCA)
;यह कुछ भयानक दु:स्वप्न की तरह है जिनसे कि मैं से जाग नहीं सकता हूँ . (manual)
(defrule horrible0
(declare (salience 0))
(id-root ?id horrible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BayAnaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  horrible.clp   horrible0   "  ?id "  BayAnaka )" crlf))
)


;@@@ Added by 14anu-ban-06 (25-02-2015)
;We've been through so much horrible weather. (COCA)
;हम इतने ज्यादा खराब मौसम में  रह चुके हैं . (manual)
(defrule horrible1
(declare (salience 2000))
(id-root ?id horrible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 weather)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  horrible.clp   horrible1   "  ?id "  KarAba )" crlf))
)

