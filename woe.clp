;@@@ Added by 14anu-ban-11 on (15-04-2015)
;A tale of woe. (oald)
;सन्ताप का एक किस्सा . (self)
(defrule woe1
(declare (salience 10))
(id-root ?id woe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 tale)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanwApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  woe.clp      woe1   "  ?id "  sanwApa)" crlf))
)

;---------------------------------- Default Rules -------------------------------------------

;@@@ Added by 14anu-ban-11 on (15-04-2015)
;Thanks for listening to my woes.(oald)
;मेरी समस्याओं को सुनने के लिए धन्यवाद .(self)
(defrule woe0
(declare (salience 00))
(id-root ?id woe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samasyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  woe.clp      woe0   "  ?id "  samasyA)" crlf))
)

