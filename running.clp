
;@@@ Added by Anita--24.5.2014
;I can hear the sound of running water. [oxford learner's dictionary]
;मैं बहते हुए पानी की आवाज़ सुन सकता हूँ ।
(defrule running1
(declare (salience 2000))
(id-word ?id running)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 sound)
(viSeRya-of_saMbanXI  ?id1 ?id)
;(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bahawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  running.clp       running1  "  ?id "  bahawA_huA )" crlf))
)
;@@@ Added by Anita--24.5.2014
;He is won the Championship for the fifth year running. [cambridge dictionary]
;unhoMne lagawAra wIsare varRa se vijayasmAraka jIwA. [Anusaaraka out-put ]
;उसने लगातार पाँच वर्ष विजय श्रृंखला जीती ।
(defrule running2
(declare (salience 5000))
(id-word ?id running)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-word ?id1 month|year|day|week)
(id-word ?id2 first|second|third|fourth|fifth|sixth|seventh|eighth|ninth|tenth)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lagawAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  running.clp     running2    "  ?id "  lagawAra)" crlf))
)

;##########################default-rule#################################

;@@@ Added by Anita--24.5.2014
;Paul is directly responsible for the efficient running of the office. [By mail]
;पॉल कार्यालय के कुशल संचालन के लिए सीधे तौर पर जिम्मेदार है।
(defrule running_default-rule
;(declare (salience 1000))
(id-root ?id running)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 office)
;(viSeRya-of_saMbanXI  ?id ?sam)
;(viSeRya-for_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMcAlana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  running.clp       running_default-rule   "  ?id "  saMcAlana )" crlf))
)