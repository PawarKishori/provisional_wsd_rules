
;@@@ Added by 14anu-ban-05 on (11-02-2015)
;The rapid promotion of the director's son has itself fuelled resentment within the company. [CALD]
;निर्देशक के बेटे का तेजी से पदोन्नति देने में कंपनी के भीतर ही असंतोष बढ़ गया है. 	[manual]
(defrule fuel2
(declare (salience 4901))
(id-root ?id fuel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 resentment|speculation|flame)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fuel.clp 	fuel2   "  ?id "  baDZa_jA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (11-02-2015)
;Reports in today's newspapers have added fuel to the controversy.[CALD]
;आज के समाचार पत्रों में रिपोर्ट विवाद भड़का रहा है.				[manual]

(defrule fuel3
(declare (salience 5010))
(id-root ?id fuel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) add)
(id-root =(+ ?id 1) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (+ ?id 1) BadZakA_rahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fuel.clp  fuel3  "  ?id "  " (- ?id 1) " " (+ ?id 1)"  BadZakA_rahA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-03-2015)
;Ethnic tensions helped fuel the flames of civil war. [OALD]
;जातीय तनाव गृहयुद्ध की ज्वाला को भड़काने मे मदद की. 	[MANUAL]
(defrule fuel4
(declare (salience 4902))
(id-root ?id fuel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(kriyA-subject  ?id ?id2)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BadZakA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fuel.clp 	fuel4   "  ?id "  BadZakA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  fuel.clp 	fuel4   "  ?id " ko )" crlf))
)

;----------------------Default Rules ---------------

;"fuel","N","1.IMXana"
;Fuel is used for cooking.
(defrule fuel0
(declare (salience 5000))
(id-root ?id fuel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id IMXana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fuel.clp 	fuel0   "  ?id "  IMXana )" crlf))
)

;"fuel","V","1.IMXana_dAlanA"
;We must fuel [up] our car before a long trip.
(defrule fuel1
(declare (salience 4900))
(id-root ?id fuel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id IMXana_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fuel.clp 	fuel1   "  ?id "  IMXana_dAla )" crlf))
)

