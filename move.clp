;$$$Modified by 14anu-ban-08 (10-01-2015)         ;modified meaning from 'sWAnAMwariwa_kiyA' to 'sWAnAMwariwa_kara'
;@@@ Added by 14anu03 on 19-june-2014
;In 1969 Susan moved from Ithaca to California where she met her husband-to-be, who, ironically, also came from upstate New York.
;1969 में सुसं ने इथाका कैलिफोर्निया को स्थानान्तरित किया जहाँ वह उसके husband-to-be को, मिली व्यङ्ग्यपूर्वक, राज्य के वह भाग जो मुख्य शहर से दूर हो नेव् यौर्क से भी कौन आया.
(defrule move100
(declare (salience 5500))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 from)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sWAnAMwariwa_kara))      ;modified meaning from 'sWAnAMwariwa_kiyA' to 'sWAnAMwariwa_kara' by 14anu-ban-08 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  move.clp     move100   "  ?id "  " ?id1 "  sWAnAMwariwa_kara )" crlf))              ;modified meaning from 'sWAnAMwariwa_kiyA' to 'sWAnAMwariwa_kara' by 14anu-ban-08 
)

;Remove this rule by 14anu-ban-08 (24-02-2015) beacuse moving is adjective, so this rule is made in moving.clp file instead of move.clp
;Modified by Meena(30.9.10)
;"moving","Adj","1.hqxaya sparSI"
;The teacher gave a moving talk during her fare well.
;I find some of Brahms's music deeply moving .
;She told me a very moving story . 
;(defrule move0
;(declare (salience 5000))
;(id-root ?id move)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id moving )
;(or(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)(viSeRya-viSeRaNa   ?id1 ?id))
;(id-cat_coarse ?id1 noun);Added by Manju to exclude examples like :  Moving beyond the intellect, I therefore soar high in the vast expanse of the Gita on the twin wings of faith and experimentation. Suggested by Chaitanya Sir (16-08-13)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id hqxaya_sparSI))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  move.clp  	move0   "  ?id "  hqxaya_sparSI )" crlf))
;)

;$$$ Modified by Roja(26-05-14).Suggested by Chaitanya Sir. Counter Ex: Even when we are sleeping, air moves into and out of our lungs and blood flows in arteries and veins.  
;Modified by Nandini(2-12-13)
;This house is so compact that we are moving in for other.
(defrule move1
;(declare (salience 4900))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(or(kriyA-upasarga ?id ?id1)(kriyA-in_saMbanXI  ?id ?))
(kriyA-subject ?id ?sub) ; added by  Roja(26-05-14)
(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))) ; Added this condition by Roja(26-05-14).
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 makAna_meM_Akara_rahane_laga))
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Gara_baxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move1  "  ?id "  " ?id1 "  makAna_meM_Akara_rahane_laga  )" crlf))
)

;@@@ Added by Nandini (2-12-13)
;The police moved in on the terrorists.
;pulIsa ne AwafkavAxiyoM ko hatAyA.
(defrule move1-a
;(declare (salience 4950))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga  ?id ?id1)
(kriyA-subject  ?id ?id2)
(id-word ?id2 police)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hatA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move1-a  "  ?id "  " ?id1 "  hatA )" crlf))
)

(defrule move2
;(declare (salience 4800))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move2  "  ?id "  " ?id1 "  cala  )" crlf))
)

;English and Hindi traslation added by Nandini(2-12-13)
;The bus driver asked them to move along.[oxford advanced learner's dictionary]
(defrule move3
;(declare (salience 4600))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 along)
(kriyA-upasarga ?id ?id1)
(kriyA-vAkyakarma  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_baDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move3  "  ?id "  " ?id1 "  Age_baDZa  )" crlf))
)


(defrule move4
(declare (salience 4400))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sWAnAMwara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move4  "  ?id "  " ?id1 "  sWAnAMwara_kara  )" crlf))
)



(defrule move5
(declare (salience 4200))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 praswAva_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move5  "  ?id "  " ?id1 "  praswAva_raKa  )" crlf))
)



;(defrule move10
;;(declare (salience 4000))
;(id-root ?id move)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 in)
;(kriyA-upasarga ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 me_A))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move10  "  ?id "  " ?id1 "  me_A  )" crlf))
;)



(defrule move6
(declare (salience 3800))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 yAwrA_SurU_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move6  "  ?id "  " ?id1 "  yAwrA_SurU_kara  )" crlf))
)



(defrule move7
(declare (salience 3600))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viRaya_parivarwana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move7  "  ?id "  " ?id1 "  viRaya_parivarwana_kara  )" crlf))
)


;Modified by Meena(30.9.10)
;Find out everything you need to know about moving out . 
;Her landlord has given her a week to move out .
(defrule move8
;(declare (salience 3400))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id  move|moving)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 purAnA_Gara_CodZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move8   "  ?id "  " ?id1 "  purAnA_Gara_CodZa_xe  )" crlf))
)


;Modified by Meena(1.10.10)
;If you move up a bit, Tess can sit next to me.
;There's room for another one if you move up a bit.
(defrule move9
;(declare (salience 3200))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over|up) ;along word remove by Nandini(2-11-13)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kisaka))
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sWana_baxal))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move9  "  ?id "  " ?id1 "  Kisaka  )" crlf))
)





(defrule move10
;(declare (salience 2600))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 machine)
(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move10   "  ?id "  cala )" crlf))
)



;Added by Meena(30.9.10)
;Galelio was the first scientist to claim that the earth moves around the sun .
(defrule move11
;(declare (salience 2500))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) around)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp      move11   "  ?id "  GUma )" crlf))
)

;@@@ Added by Nandini(16-12-13)
;Next time I will keep my mouth shut, Rajvir promised, as they moved on.[via-mail]
(defrule move7a
(declare (salience 3700))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-vAkyakarma  ?id2 ?id3)
(id-root ?id3 shut)
(kriyA-samakAlika_kriyA  ?id3 ?id)
;(kriyA-vAkya_viBakwi  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_baDza_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move7a  "  ?id "  " ?id1 "  Age_baDza_jA  )" crlf))
)



;"move","V","1.calanA/hilanA"
;Please move a while further.
;
(defrule move12
(declare (salience -1))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move12   "  ?id "  cala )" crlf))
)

;@@@ Added by 14anu20 on 20/06/2014
;I move from place to place.
;मैं जगह-जगह घूमता हूँ . 
(defrule move012
(declare (salience 5600))                ;Salience increased from 100 to 5600 by 14anu-ban-08 (10-01-2015)
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ?id ?id1)
(id-root ?id1 place|city|town|country|continent|sites)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move012   "  ?id "  GUma )" crlf))
)

;$$$Modified 14anu-ban-08 (30-03-2015)  ;constraint added
;@@@ Added by Nandini (2-11-13)
;There is one star, however, which does not seem to move at all.
(defrule move13
(declare (salience -1))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?id2 ?id)   ;added 'id2' by 14anu-ban-08 (30-03-2015)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 at)         ;added by 14anu-ban-08 (30-03-2015)
(id-root ?id2 seem)      ;added by 14anu-ban-08 (30-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move13   "  ?id "  calawA_huA )" crlf))
)

;@@@ Added by Nandini (4-1-14)
;They moved to the fermenting room, where the leaves were spread over troughs. [via mail]
;ve ParmentiMga kakRa meM gaye jahAz pawwiyAz  PElAI huI WIM.
(defrule move14
(declare (salience 3560))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(or (id-root ?id1 room)(id-cat_coarse ?id1 PropN))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move14   "  ?id "  jA )" crlf))
)

;default_sense && category=verb	calanA/hila	0

;@@@ Added by 14anu21 on 18.06.2014 
;Time is moving on.
;समय विषय परिवर्तन कर रहा है .(Translation before adding rule)
;rule move7 was getting fired
;समय बीत रहा है.
(defrule move015
(declare (salience 3650))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-subject ?id ?idsub)
(id-word ?idsub time)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bIwa_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move015  "  ?id "  " ?id1 "  bIwa_raha  )" crlf))
)

;@@@ Added by 14anu21 on 18.06.2014 
;Let's move the meeting to wednesday.
;चलिये हम बैठक बुधवार को रख देते हैं . 
(defrule move016
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?idobj)
(id-root ?idobj meeting)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move016   "  ?id "  raKa )" crlf))
)

;@@@ Added by 14anu21  on 18.06.2014 
;I move that a vote be taken on this. 
;मैं इस पर मत लेने का प्रस्ताव रखता  हूँ.
; मैं चलता हूँ कि मत इसपर ले.(Translation before modification) 
(defrule move017
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) that)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAva_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move017   "  ?id "  praswAva_raKa )" crlf))
)

;$$$Modified by 14anu-ban-08 (30-03-2015)        ;constraint added
;@@@ Added by 14anu21 on 18.06.2014
;We were deeply moved by her plight.
;हम उसकी गम्भीर स्थिति से बहुत अदिक चले गये थे .
; उसकी गम्भीर स्थिति से हमारा ह्र्दय द्रवित हो गया.
(defrule move018
(declare (salience 5000))     ;salience added by 14anu-ban-08 (30-03-2015)
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(kriyA-by_saMbanXI  ?id ?id2)
(id-root ?id1 deeply)    ;added by 14anu-ban-08 (30-03-2015)
(id-root ?id2 plight)    ;added by 14anu-ban-08 (30-03-2015)
=>  
(retract ?mng)
(assert (id-wsd_root_mng ?id hrxaya_xraviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move018  "  ?id "  hrxaya_xraviwa )" crlf))
)

;$$$Modified by 14anu-ban-08 (30-03-2015)       ;constraint added
;@@@ Added by 14anu26   [24-06-14]
;At the end of the speech he seemed to be moving into the realms of fantasy. 
;भाषण के अन्त में वह कोरी कल्पना के राज्यों के अन्दर जाता हुआ  प्रतीत हुआ.
(defrule move15
(declare (salience 5300))    ;salience increased by 14anu-ban-08 (30-03-2015)
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 realm)           ;added by 14anu-ban-08 (30-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move15   "  ?id "  jA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (21-10-2014)
;A block moving on a smooth horizontal table is not acted upon by a horizontal force (since there is no friction), but may undergo a large displacement. 
;kisI cikanI kREwija meja para gawimAna piNda para koI kREwija bala kArya nahIM karawA hE, (kyofki GarRaNa nahIM hE) paranwu piNda kA visWApana kAPI aXika ho sakawA hE. [NCERT CORPUS]
;@@@ Added By 14anu-ban-08 (04-08-2014)
;To describe the position of an object moving in a plane, we need to choose a convenient point, say O as origin.    [NCERT]
;kisI samawala meaM gawimAna vaswu kI siWawi vyakw karane ke liye hama suviXAnusAra kiSI binxu 0 kO mUla binxu ke rUpa meaM cunawe hEM.
(defrule move16
(declare (salience 5400))
?mng <-(meaning_to_be_decided ?id)
(id-word ?id moving)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 object|train|block) ;added 'block' by 14anu-ban-09 on (21-10-2014)
;(id-cat_coarse ?id verb) ;commented by 14anu-ban-09 on (21-10-2014)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gawimAna))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  move.clp 	move16   "  ?id "  gawimAna )" crlf))
)

;@@@ Added By 14anu-ban-08 (19-08-2014)
;We also experience the impact of forces on us, like when a moving object hits us or we are in a merry-go - round.     [NCERT]
;hama apane Upara baloM ke safGAwa, jEse kisI gawimAna vaswu ke hamase takarAwe samaya aWavA "mErI go rAuNda JUle" meM gawi karawe samaya,anuBava karawe hEM .
(defrule move17
(declare (salience 5300))
?mng <-(meaning_to_be_decided ?id)
(id-word ?id moving)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 object|charge)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gawimAna))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  move.clp 	move17   "  ?id "  gawimAna )" crlf))
)


;@@@ Added By 14anu-ban-08 (29-08-2014)
;But it does not tell us in what direction an object is moving.    [NCERT]
;परन्तु औसत चाल से यह पता नहीं चल पाता कि वस्तु किस दिशा में गतिमान है .
(defrule move18
(declare (salience 5200))
?mng <-(meaning_to_be_decided ?id)
(id-word ?id moving)
(kriyA-subject ?id ?id1)
(id-root ?id1 object|particle|charge|plane)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gawimAna))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  move.clp 	move18   "  ?id "  gawimAna )" crlf))
)

;@@@ Added By 14anu-ban-08 (29-08-2014)
;To describe the position of an object moving in a plane, we need to choose a convenient point, say O as origin.    [NCERT]
;किसी समतल में गतिमान वस्तु की स्थिति व्यक्त करने के लिए हम सुविधानुसार किसी बिन्दु O को मूल बिन्दु के रूप में चुनते हैं .
(defrule move19
(declare (salience 5260))
?mng <-(meaning_to_be_decided ?id)
(id-word ?id moving)
(kriyA-in_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gawimAna))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  move.clp 	move19  "  ?id "  gawimAna )" crlf))
)

;@@@ Added by 14anu-ban-05 on 05-08-2014
;But the rest of the body continues to move forward due to inertia.  
;paranwu SarIra kA SeRa BAga, jadawva ke kAraNa, Age kI ora calawA rahawA hE.
(defrule move020
(declare (salience 5000))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 forward)
(kriyA-upasarga  ?id ?id1)
(kriyA-subject  ?id ?id2)
(id-root ?id2 rest)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_kI_ora_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move020  "  ?id "  " ?id1 "  Age_kI_ora_cala )" crlf))
)


;@@@ Added By 14anu-ban-08 (07-11-2014)
;They have electric charges that are comparatively free to move inside the material.    [NCERT]
;उनमें ऐसे वैद्युत आवेश (इलेक्ट्रॉन) होते हैं जो पदार्थ के भीतर गति के लिए अपेक्षाकृत स्वतन्त्र होते हैं.    [NCERT]
(defrule move20
(declare (salience 5269))
?mng <-(meaning_to_be_decided ?id)
(id-word ?id move)
(kriyA-inside_saMbanXI ?id ?id1)
(id-root ?id1 material)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gawi_kara))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  move.clp 	move20  "  ?id "  gawi_kara )" crlf))
)

;@@@Added by 14anu-ban-08 (05-02-2015)
;This attractive property of magnets made it difficult for them to move around.  [NCERT]
;चुम्बकों के इस आकर्षित करने वाले गुण ने उनका घूमना-फिरना दूभर बना दिया था.   [NCERT]
(defrule move21
(declare (salience 5300))
(id-word ?id move)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 around)
(kriyA-anaBihiwa_subject ?id ?id2)
(id-root ?id2 them)
=>
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 GUma-Pira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  move.clp     move21   "  ?id "  " ?id1 "  GUma-Pira )" crlf))
)

;@@@Added by 14anu-ban-08 (26-02-2015)
;I didn't want to move abroad but Bill talked me into it. (oald)
;मैं विदेश  नहीं  जाना चाहता था परन्तु बिल ने मुझे मनाया .  (manual)
(defrule move022
(declare (salience 3560))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 abroad)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move022   "  ?id "  jA )" crlf))
)


;@@@ Added by 14anu-ban-08 (28-02-2015)
;If the object moves from P to P′, the vector PP′ (with tail at P and tip at P′) is called the displacement vector corresponding to motion from point P (at time t) to point P′ (at time t′). [NCERT]
;यदि वस्तु P से चलकर P′ पर पहुञ्च जाती है तो सदिश PP′ (जिसकी पुच्छ P पर तथा शीर्ष P′ पर है) बिंदु P (समय t) से P′ (समय t′) तक गति के सङ्गत विस्थापन सदिश कहलाता है.  [NCERT]
;यदि वस्तु P से P′ पर पहुँच जाती है तो सदिश PP′ (जिसकी पुच्छ P पर तथा शीर्ष P′ पर है) बिंदु P (समय t) से P′ (समय t′) तक गति के सङ्गत विस्थापन सदिश कहलाता है.  [self]
(defrule move22
(declare (salience 5501))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 object)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca_jA))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move22  "  ?id "  pahuzca_jA )" crlf))
)

;@@@Added by 14anu-ban-08 (26-02-2015)
;They move in a semi-circular path in one of the dees and arrive in the gap between the dees in a time interval T / 2. [NCERT]
;ये किसी एक 'डी' में अर्धवृत्ताकर पथ पर गमन करते हुए T/2 समय अन्तराल में डीज के बीच के रिक्त स्थान में आते हैं .  [NCERT]
(defrule move023
(declare (salience 4901))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(conjunction-components ?id2 ?id ?id3)
(id-word ?id1 path)
(id-word ?id2 and)
(id-word ?id3 arrive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gamana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move023  "  ?id "  gamana_kara )" crlf))
)

;@@@ Added By 14anu-ban-08 (03-03-2015)
;When the external force is removed, the body moves, gaining kinetic energy and losing an equal amount of potential energy.  [NCERT]
;जब बाह्य बल हटा लिया जाता है तो वस्तु गति करने लगती है और कुछ गतिज ऊर्जा अर्जित कर लेती है, तथा उस वस्तु की उतनी ही स्थितिज ऊर्जा कम हो जाती है.  [NCERT]
(defrule move23
(declare (salience 5260))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 body)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gawi_karane_laga))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move23  "  ?id "  gawi_karane_laga )" crlf))
)

;@@@Added by 14anu-ban-08 (26-02-2015)
;A dot appears like the tip of an arrow pointed at you, a cross is like the feathered tail of an arrow moving away from you.  [NCERT]
;कोई डाट (बिंदु) आपकी ओर सङ्केत करते तीर की नोंक जैसा प्रतीत होता है तथा क्रॉस किसी तीर की पङ्खयुक्त पूँछ जैसा प्रतीत होता है.  [NCERT]
;कोई डाट (बिंदु) आपकी ओर सङ्केत करते तीर की नोंक जैसा प्रतीत होता है तथा क्रॉस आपसे दूर जाता हुआ तीर की पङ्खयुक्त पूँछ जैसा प्रतीत होता है.  [self]
(defrule move024
(declare (salience 4901))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 arrow)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xUra_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " move.clp	move024  "  ?id "  " ?id1 "  xUra_jA  )" crlf))
)

;@@@ Added By 14anu-ban-08 (03-03-2015)  ;Run on parser 148
;What's the date of your move?  [oald]
;आपके जाने की तिथि क्या हैं.  [self]
(defrule move24
(declare (salience 5260))
(id-root ?id move)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 date)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnA))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move24  "  ?id "  jAnA )" crlf))
)

;Commented by 14anu-ban-08 (04-03-2015) because it fire from move12
;@@@ Added By 14anu-ban-08 (03-03-2015)
;It's your move.  [oald]
;अब आपकी बारी हैं. [self]
;(defrule move25
;(declare (salience 5260))
;(id-root ?id move)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-RaRTI_viSeRaNa ?id ?id1)
;(id-root ?id1 your)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id bArI))  
;(if ?*debug_flag* then 
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  move.clp 	move25  "  ?id "  bArI )" crlf))
;)

;@@@ Added By 14anu-ban-08 (04-03-2015)
;I can't move my fingers.  [oald]
;मैं अपनी उंगलीयाँ नहीं हिला सकता.  [self]
(defrule move26
(declare (salience 5260))
(id-root ?id move)  
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 finger)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilA))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move26  "  ?id "  hilA )" crlf))
)

;@@@ Added By 14anu-ban-08 (04-03-2015)
;Things are not moving as fast as we hoped. [oald]
;चीजें उतनी तेजी से आगे नहीं बढ़ रही जैसा हमें आशा हैं. [self]
(defrule move27
(declare (salience 5260))
(id-root ?id move)  
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 thing)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZa))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move27  "  ?id "   baDZa )" crlf))
)

;@@@ Added By 14anu-ban-08 (04-03-2015)
;The government has not moved on this issue.  [oald]
;सरकार इस समस्या में बदलाव नहीं ला रही हैं.  [self]
(defrule move28
(declare (salience 5260))
(id-root ?id move)  
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI ?id ?id1)
(id-root ?id1 issue)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxalAva_lA_raha))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move28  "  ?id "   baxalAva_lA_raha )" crlf))
)

;@@@ Added By 14anu-ban-08 (30-03-2015)
;The magnitude of magnetic field B is 1 SI unit, when the force acting on a unit charge (1 C), moving perpendicular to B with a speed 1m / s, is one newton.  [NCERT]
;चुम्बकीय क्षेत्र B का परिमाण 1 SI मात्रक होता है, जबकि किसी एकाङ्क आवेश (1 C), जो कि B के लम्बवत 1m/s वेग v से गतिमान है, पर लगा बल 1 न्यूटन हो.  [NCERT]
;चुम्बकीय क्षेत्र B का परिमाण 1 SI मात्रक होता है, जबकि किसी एकाङ्क आवेश (1 C), जो कि 1 न्यूटन के बल B के लम्बवत 1m/s वेग v से गतिमान होता है.  [self]
(defrule move29
(declare (salience 5290))
(Domain physics)
(id-word ?id moving)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gawimAna_ho))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  move.clp 	move29   "  ?id "  gawimAna_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  move.clp 	move29   "  ?id "  physics )" crlf))
)

;@@@ Added By 14anu-ban-08 (30-03-2015)
;The police moved quickly to dispel the rumours.  [oald]
;पुलिस ने अफवाह दूर करने के लिए शीघ्र कार्यवाही करी. [self]
(defrule move30
(declare (salience 5260))
(id-root ?id move)  
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 police|inspector|uncle|aunt)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryavAhI_kara))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  move.clp 	move30 "  ?id "   kAryavAhI_kara )" crlf))
)
