;@@@ Added by 14anu-ban-11 on (12-03-2015)
;Keep a weather eye on your competitors. (oald)
;अपने प्रतिस्पर्ध्दी पर सतर्क दृष्टि रखिए . (self)
(defrule weather3
(declare (salience 5001))
(id-root ?id weather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 eye)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id sawarka_xqRti))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " weather.clp 	weather3  "  ?id1 "  " ?id "  sawarka_xqRti  )" crlf))
)


;@@@ Added by 14anu-ban-11 on (12-03-2015)
;She weathered the crisis well.(hinkhoj)
;उसने अच्छी तरह से सङ्कट पार किया .(self) 
(defrule weather4
(declare (salience 4901))
(id-root ?id weather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 well)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weather.clp 	weather4  "  ?id "   pAra_kara)" crlf))
)


;@@@ Added by 14anu-ban-11 on (12-03-2015)
;The company just managed to weather the recession.(oald)
;कम्पनी आर्थिक मन्दी से बचने मे सफल हुई . (self)
(defrule weather5
(declare (salience 4902))
(id-root ?id weather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 recession)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_baca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weather.clp 	weather5  "  ?id "  se_baca)" crlf))
)

;------------------------ Default Rules ----------------------

;"weather","N","1.mOsama"
;Let's go out.The weather is pleasant now.
(defrule weather0
(declare (salience 5000))
(id-root ?id weather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mOsama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weather.clp 	weather0   "  ?id "  mOsama )" crlf))
)


;"weather","VTI","1.KulI_havA_ke_praBAva_ko_JelanA"
;You buy a suite that weathers well.
(defrule weather1
(declare (salience 4900))
(id-root ?id weather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KulI_havA_ke_praBAva_ko_Jela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weather.clp 	weather1   "  ?id "  KulI_havA_ke_praBAva_ko_Jela )" crlf))
)

;"weather","VTI","1.KulI_havA_ke_praBAva_ko_JelanA"
;You buy a suite that weathers well.
;--"2.pAra_karanA"
;She weathered the crisis well.
;
