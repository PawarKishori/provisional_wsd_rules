
(defrule frequent0
(declare (salience 5000))
(id-root ?id frequent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAra_bAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  frequent.clp 	frequent0   "  ?id "  bAra_bAra )" crlf))
)

;Refer sabdasutra
;The frequent sleeping of the boys in the class ..
;default_sense && category=adjective	aksara_AnevAlA	0
;"frequent","Adj","1.aksara_AnevAlA"
;There are no frequent bus service from here to Koti.
;
(defrule frequent1
(declare (salience 4900))
(id-root ?id frequent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAra_bAra_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  frequent.clp 	frequent1   "  ?id "  bAra_bAra_jA )" crlf))
)

;"frequent","VT","1.bAra_bAra_jAnA"
;He frequents to her place every now && then.
;

;$$$ Modified by 14anu-ban-05 on (10-01-2015)
;@@@ Added by 14anu11
; The Al Qaida man had even surveyed the hotels and guest houses frequented by American and Israeli tourists, but Ayoob ' s arrest scuttled the plot .
;कायदा का यह शस अमेरिकी और इज्राएली पर्यटकों के टिकने वाले होटलों , गेस्ट हाउसों को देख - स्रवेकष्ण्  भी चुका था लेकिन अयूब की गिरतारी से सारी योजना धरी रह गई .
;ऐलकायदा का यह शक्स अमेरिकी और इज्राएली पर्यटकों के टिकने वाले होटलों , गेस्ट हाउसों का स्रवेकष्ण्  भी करचुका था लेकिन अयूब की  गिरफ्तारी से सारी योजना धरी रह गई . ;translated by 14anu-ban-05 on (10-01-2015)
(defrule frequent2
(declare (salience 5000))
(id-root ?id frequent)
(id-root =(+ ?id 1) by)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(kriyA-by_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tikane_vAlA )) ;changed meaning from 'ke_tikane_vAle' to 'tikane_vAlA' by 14anu-ban-05 on (10-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  frequent.clp 	frequent2   "  ?id " tikane_vAlA )" crlf))		;changed meaning from 'ke_tikane_vAle' to 'tikane_vAlA' by 14anu-ban-05 on (10-01-2015)
)
