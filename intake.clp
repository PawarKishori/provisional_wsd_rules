;@@@ Added by 14anu-ban-06  (08-11-2014)
;Intake of huge quantities of vegetarian food in excess of one's need is also considered as an aggressive food .(parallel corpus)
;आवश्यकता से अधिक तला भुना शाकाहार ग्रहण करना भी राजसिक माना गया है ।(parallel corpus)
(defrule intake0
(declare (salience 0))
(id-root ?id intake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id grahana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intake.clp 	intake0   "  ?id "  grahana )" crlf))
)

;@@@ Added by 14anu-ban-06  (08-11-2014)
;Intake in universities is down by 10%.(OALD)
;विश्वविद्यालयो में भर्ती 10 प्रतिशत से कम हो गयी हैं|(manual)
(defrule intake1
(declare (salience 2000))
(id-root ?id intake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 university|college|school|organisation|institute)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intake.clp 	intake1   "  ?id "  BarwI )" crlf))
)

;@@@ Added by 14anu-ban-06  (08-11-2014)
;In otis flow one should avoid the intake of curd , banana , guava and sour fruits .(parallel corpus)
;कान बहने में दही , केला , अमरूद और खट्‍टे फलों का सेवन करने से परहेज रखना चाहिए ।(parallel corpus)
;The intake of wine, per capita, in France is higher than anywhere else.(COCA)
;फ्रांस में प्रतिव्यक्ति, वाइन का सेवन  अन्य किसी जगह  की तुलना में ज्यादा है .(manual) 
;Hene had a steady intake of smoke on the job as the manager of a banquet hall. (COCA)
;हेने ने एक भोज हॉल के प्रबन्धक के रूप में काम पर धुएँ का एक अनवरत सेवन किया. (manual)
(defrule intake2
(declare (salience 2200))
(id-root ?id intake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 wine|smoke|fruit|vegetable)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sevana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intake.clp 	intake2   "  ?id "  sevana )" crlf))
)

;@@@ Added by 14anu-ban-06  (11-02-2015)
;The college has increased its intake of students by 50 percent this year. (cambridge)
;इस वर्ष कालेज ने 50 प्रतिशत के द्वारा विद्यार्थियों की भर्ती में वृद्धि की है . (manual)
(defrule intake3
(declare (salience 2300))
(id-root ?id intake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id2 ?id)
(kriyA-subject ?id2 ?id1)
(id-root ?id1 university|college|school|organisation|institute)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intake.clp 	intake3   "  ?id "  BarwI )" crlf))
)
