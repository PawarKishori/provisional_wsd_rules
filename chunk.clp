;@@@ Added by 14anu-ban-03 (19-02-2015)
;I've already written a chunk of the article. [cald]
;मैं  काफी मात्रा में लेख पहले से ही लिख चुका हूँ . [manual]
(defrule chunk1
(declare (salience 100))
(id-root ?id chunk)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1) 
(id-root ?id1 article|text)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI_mAwrA_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chunk.clp 	chunk1   "  ?id "  kAPI_mAwrA_meM )" crlf))
)

;@@@ Added by 14anu-ban-03 (20-02-2015)
;A substantial chunk of our profits. [cald]
;हमारे लाभ का एक बड़ा हिस्सा . [manual]
(defrule chunk2
(declare (salience 200))
(id-root ?id chunk)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1) 
(id-root ?id1 profit)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hissA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chunk.clp 	chunk2   "  ?id "  hissA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (19-02-2015)
;Cut it in chunks. [hinkhoj]
;इसको टुकडों में  काटिए . [manual]
(defrule chunk0
(declare (salience 00))
(id-root ?id chunk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tukadA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chunk.clp 	chunk0   "  ?id "  tukadA )" crlf))
)



