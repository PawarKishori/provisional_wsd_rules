
(defrule she0
(declare (salience 5000))
(id-root ?id she)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  she.clp 	she0   "  ?id "  swrI )" crlf))
)

;"she","N","1.swrI"
;oh! what a Wonderful cat is it he or she?
;
(defrule she1
(declare (salience 4900))
(id-root ?id she)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  she.clp 	she1   "  ?id "  vaha )" crlf))
)


;@@@ Added by 14anu-ban-01 on (27-10-2014)
;If the object were a human being, he or she will not feel his weight since there is no upward force on him.[NCERT corpus]
;यदि उस पिंड के रूप में कोई स्त्री अथवा पुरुष है, तो वह इस स्थिति में अपने भार का अनुभव नहीं करेगी/करेगा, क्योंकि उस पर ऊपर की दिशा में कोई बल नहीं लग रहा है.[NCERT corpus]
(defrule she2
(declare (salience 4900))
(id-root ?id she)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ?con ?id1 ?id)
(id-root ?con or)
(id-root ?id1 he)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?con ?id1 vaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " she.clp 	she2  "  ?id "  "  ?con "  " ?id1 "  vaha  )" crlf)
)
)

;"she","Pron","1.vaha{swrI}"
;I stroked the cat && she rubbed againast my leg.
;India - she is my motherland.
;
