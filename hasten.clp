;@@@ Added by 14anu-ban-06 (23-03-2015)
;She saw his frown and hastened to explain. (OALD)
;उसने उसकी त्यौरी को देखा और समझाने की जल्दी की . (manual)
(defrule hasten0
(declare (salience 0))
(id-root ?id hasten)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalxI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hasten.clp 	hasten0   "  ?id "  jalxI_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (23-03-2015)
;We hastened back to Rome. (OALD)
;हम रोम को जल्दी वापिस आ गए . (manual)
(defrule hasten1
(declare (salience 2000))
(id-root ?id hasten)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?id1)	
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalxI_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hasten.clp 	hasten1   "  ?id "  jalxI_A )" crlf))
)
