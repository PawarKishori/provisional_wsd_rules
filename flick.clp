
(defrule flick0
(declare (salience 5000))
(id-root ?id flick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-through_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAgajZa_palata));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flick.clp flick0 " ?id "  kAgajZa_palata )" crlf)) 
)

(defrule flick1
(declare (salience 4900))
(id-root ?id flick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-through_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAgajZa_palata));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flick.clp flick1 " ?id "  kAgajZa_palata )" crlf)) 
)

(defrule flick2
(declare (salience 4800))
(id-root ?id flick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-through_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAgajZa_palata));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flick.clp flick2 " ?id "  kAgajZa_palata )" crlf)) 
)

(defrule flick3
(declare (salience 4700))
(id-root ?id flick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAgajZa_palata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flick.clp	flick3  "  ?id "  " ?id1 "  kAgajZa_palata  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (26-03-2015)
;Flicking through the channels, I came across an old war movie.[OALD]
;चैनलों को बदलते हुये,मैं एक पुराने युद्ध फिल्म के पार आया.   [MANUAL]
(defrule flick5
(declare (salience 5001))
(id-root ?id flick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) through)
(kriyA-through_saMbanXI  ?id ?id1)
(id-root ?id1 channel)		;more constraints can be added
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) baxala))
(assert  (id-wsd_viBakwi   ?id1  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flick.clp  flick5  "  ?id "  " (+ ?id 1) "  baxala )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  flick.clp  flick5   "  ?id1 " ko )" crlf)
))


;@@@ Added by 14anu-ban-05 on (26-03-2015)
;She flicked the dust off her collar. [OALD]
;उसने अपनी गरदनी पर से धूल   झटके से ह‍टाया. [MANUAL]
(defrule flick6
(declare (salience 5002))
(id-root ?id flick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 dust)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jatake_se_hatA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flick.clp 	flick6   "  ?id "  Jatake_se_hatA )" crlf))
)

;------------------------ Default Rules ---------------------

;default_sense && category=verb	Jatake se mAra	0
;"flick","V","1.Jatake se mAranA"
;He flicked the whip at the horse to drive him faster.
(defrule flick4
(declare (salience 4600))
(id-root ?id flick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jatake_se_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flick.clp 	flick4   "  ?id "  Jatake_se_mAra )" crlf))
)

;default_sense && category=verb	Jatake se mAra	0
;"flick","V","1.Jatake se mAranA"
;He flicked the whip at the horse to drive him faster.
;--"2.JatakA xenA"
;He flicked the ball with a twist of wrist to send it to the boundary.
;He flicked the towel in his face.
;--"3.hilAnA"
;The dog flicked its tail to drive away the flies.
;
;
