
;------------------------DEFAULT RULE-----------------------------------------------

;@@@ Added by 14anu-ban-09 on 28-8-14
;Lumps of chalk crushed to a fine white powder. [OALD]
;cAka ke Delo ko cUra-cUra kara bArIka saPexa cUrA banA xiyA. [Own Manual]

(defrule powder0
(id-root ?id powder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cUrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  powder.clp 	powder0   "  ?id "  cUrA )" crlf))
)

;@@@ Added by 14anu-ban-09 on 27-8-14
;You have to powder the antibiotic tablet and mix it with food. [WM dictionary]
;tuma eMtIbAyotika tebaleta ko pIsa kara, use KAne ke sAWa milA xo. [Own Manual]

(defrule powder1
(declare (salience 2000))
(id-root ?id powder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  powder.clp 	powder1   "  ?id "  pIsa )" crlf))
)

;@@@ Added by 14anu-ban-09 on 27-8-14
;Next we lightly sprinkle some lycopodium powder on the surface of water in a large trough and we put one drop of this solution in the water.  [NCERT CORPUS]
;इसके बाद एक बडे नान्द में पानी लेकर, उसके ऊपर लायकोपोडियम पाउडर छिडक कर, लाइकोपोडियम पाउडर की एक पतली फिल्म जल के पृष्ठ के ऊपर बनाते हैं ; फिर ओलीक अम्ल के पहले बनाए गए घोल की एक बून्द इसके ऊपर रखते हैं.

(defrule powder2
(declare (salience 3000))
(id-root ?id powder)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAudara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  powder.clp 	powder2   "  ?id "  pAudara )" crlf))
)

;@@@ Added by 14anu-ban-09 on 27-8-14
;She powdered her face and put on her lipstick. [OALD]
;usane apane cahare para pAudara lagAyA Ora apanI lipastika lagAE. [Own Manual]

(defrule powder3
(declare (salience 3500))
(id-root ?id powder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 face) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAudara_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  powder.clp 	powder3   "  ?id "  pAudara_lagA )" crlf))
)
