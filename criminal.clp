;@@@Added by 14anu-ban-02(29-02-2016)
;This is a criminal waste of resources.[oald]
;यह संसाधनों की अनुचित बरबादी है . [self]
(defrule criminal2
(declare (salience 100))
(id-root ?id criminal)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 waste)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  criminal.clp 	criminal2   "  ?id "  anuciwa)" crlf))
)
;-----------Default_rules---------------
;$$$Modified by 14anu-ban-02(29-02-2016)
(defrule criminal0
(declare (salience 0))	;salience reduced to o from 5000 by 14anu-ban-02(29-02-2016)
(id-root ?id criminal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
;(id-root =(+ ?id 1) ~charge);added by sheetal	;commented by 14anu-ban-02(29-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AparAXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  criminal.clp 	criminal0   "  ?id "  AparAXika )" crlf))
)

;"criminal","Adj","1.AparAXika"
;No one can stop his criminal activities.
;It is criminal to waste energy.
;--"2.POjaxArI_kA"
;$$$Modified by 14anu-ban-02(29-02-2016)
;He takes up only criminal cases.
;Criminal charges will be brought against the driver.
(defrule criminal1
(declare (salience 4900))	;salience reduced to o from 4900 by 14anu-ban-02(29-02-2016)
(id-root ?id criminal)
?mng <-(meaning_to_be_decided ?id)
;(or (id-cat_coarse ?id noun)(viSeRya-viSeRaNa ?id1 ?id));"viSeRya-viSeRaNa"added by sheetal	;commented by 14anu-ban-02(29-02-2016)
(id-cat_coarse ?id noun)	;Added by 14anu-ban-02(29-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aparAXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  criminal.clp 	criminal1   "  ?id "  aparAXI )" crlf))
)

;"criminal","N","1.aparAXI"
;The locality is full of criminal.
;
