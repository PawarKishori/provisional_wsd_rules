
;-----------------------------------DEFAULT RULE-----------------------------------------------
;@@@ Added by 14anu-ban-09 on 26-8-14
;We find them optically and through radar. [COCA]
;hama unahe xqRtigawa rUpa se va radAra ke mAXyama se DuMDawe hE. [Own Manual] (NOTE-I'm not very sure about it but I'm forced to do so.)

(defrule optically0
(id-root ?id optically)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xqRtigawa_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  optically.clp 	optically0   "  ?id "  xqRti_rUpa_se )" crlf))
)

;---------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on 26-8-14
;Even though they're actually optically clear polyurethane. [Parallel Corpus]
;जबकि असलियत में ये पारदर्शी पॉलीयूरीथेन हैं .

(defrule optically1
(declare (salience 2000)) 
(id-root ?id optically)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAraxarSI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  optically.clp 	optically1   "  ?id "  pAraxarSI )" crlf))
)

