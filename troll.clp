
(defrule troll0
(declare (salience 5000))
(id-root ?id troll)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vewAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  troll.clp 	troll0   "  ?id "  vewAla )" crlf))
)

;"troll","N","1.vewAla"

;"troll","VTI","1.maCalI_PazsAnA"
;The fishermen were in the high sea trolling.
(defrule troll1
(declare (salience 4900))
(id-root ?id troll)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maCalI_PazsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  troll.clp 	troll1   "  ?id "  maCalI_PazsA )" crlf))
)

;@@@ Added by 14anu23 16/06/2014
;Troll a carol.
;आनन्द भरा गीत  बारी-बारी से गाना.
(defrule troll2
(declare (salience 4900))
(id-root ?id troll)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArI-bArI_se_gIwa_gA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  troll.clp 	troll2   "  ?id "  bArI-bArI_se_gIwa_gA )" crlf))
)

;$$$ Modified by 14anu-ban-07 (09-12-2014)
;@@@ Added by 14anu23 16/06/2014
;He trolled all around the city .
;उसने शहर के चारों ओर  घुमा . 
(defrule troll3
(declare (salience 4900))
(id-root ?id troll)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-in_saMbanXI  ?id ?id1) ; commented by 14anu-ban-07 (09-12-2014)
(kriyA-around_saMbanXI  ?id ?id1) ;added by 14anu-ban-07 (09-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  troll.clp 	troll3   "  ?id "  GumA )" crlf))
)

;@@@ Added by 14anu-ban-07 (17-03-2015)
;They are trolling the internet for new customers.(cambridge)
;वे नये ग्राहकों के लिए इन्टेर्नेट् खोज रहे हैं . (manual)
(defrule troll4
(declare (salience 5000))
(id-root ?id troll)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 internet)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Koja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  troll.clp 	troll4   "  ?id "  Koja )" crlf))
)


;@@@ Added by 14anu-ban-07 (17-03-2015)
;Both candidates have been trolling for votes.(oald)
;दोनों उम्मीदवार मत के लिए प्रयत्न कर रहे हैं . (manual)
(defrule troll5
(declare (salience 5200))
(id-root ?id troll)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 vote)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayawna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  troll.clp 	troll5   "  ?id "  prayawna_kara )" crlf))
)

