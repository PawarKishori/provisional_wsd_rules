;@@@ Added by 14anu-ban-06 (09-03-2015)
;Her account of the accident jibes with mine. (cambridge)[parser no. 32]
;उसका दुर्घटना का विवरण मेरेवाले से मेल खाता है . (manual)
(defrule jibe2
(declare (salience 2000))
(id-root ?id jibe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 with)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 se_mela_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " jibe.clp	jibe2  "  ?id "  " ?id1 "  se_mela_KA  )" crlf))
)

;---------------------- Default Rules -------------------

;@@@ Added by 14anu-ban-06 (09-03-2015)
;He made several cheap jibes at his opponent during the interview.(OALD)
;उसने साक्षात्कार के दौरान अपने प्रतिपक्षी पर कई घटिया ताना मारे . (manual)
(defrule jibe0
(declare (salience 0))
(id-root ?id jibe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jibe.clp 	jibe0   "  ?id "  wAnA )" crlf))
)

;@@@ Added by 14anu-ban-06 (09-03-2015)
;He jibed repeatedly at the errors they had made. (OALD)
;उसने त्रुटियों पर बार-बार ताना मारा जिन्हें उन्होंने किया था . (manual)
(defrule jibe1
(declare (salience 0))
(id-root ?id jibe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAnA_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jibe.clp 	jibe1   "  ?id "  wAnA_mAra )" crlf))
)

