;@@@ Added by 14anu-ban-06 (20-02-2015)
;Incredibly, no one was hurt in the accident.(cambridge)
;अविश्वसनीय रूप से, कोई भी  दुर्घटना में आहत नहीं हुआ था . (manual)
(defrule incredibly0
(declare (salience 0))
(id-root ?id incredibly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aviSvasanIya_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incredibly.clp 	incredibly0   "  ?id "  aviSvasanIya_rUpa_se )" crlf))
)

;@@@ Added by 14anu-ban-06 (20-02-2015)
;He was incredibly rich.(cambridge)
;वह बहुत ही अमीर था . (manual)
;He was incredibly angry.(cambridge)
;वह बहुत ही क्रोधित था . (manual)
;He was incredibly quick.(cambridge)
;वह बहुत ही तेज था . (manual)
;An incredibly loud bang followed the flash.(cambridge) 
;एक बहुत ही ऊँचे स्वर वाली तेज आवाज ने फ़्लैश का अनुसरण किया . (manual)
(defrule incredibly1
(declare (salience 10))
(id-root ?id incredibly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka ?id1 ?id)
(id-root ?id1 rich|angry|loud|quick)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_hI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incredibly.clp 	incredibly1   "  ?id "  bahuwa_hI )" crlf))
)
