

;@@@ Added by 14anu-ban-09 on (24-03-2015)
;Power is not something that is possessed such as blue eyes or red hair but manifests itself in terms of relations with others. [bnc Corpus]
;शक्ति कोई ऐसी वस्तु नहीं है जिसको रखा जा सकता है जैसेकि नीली आँखे या लाल बाल , लेकिन दूसरों के साथ संबंध के संद्रभ में स्वतः ही प्रकट हो जाता है. 	[bnc_manual]
;शक्ति कुछ ऐसी चीज़ नहीं है जिससे युक्त हो सकते है जैसे कि नीली आँखे या लाल बाल , लेकिन दूसरों के साथ संबंध के संद्रभ में स्वतः ही प्रकट हो जाती है.  [Self]
(defrule possess1
(declare (salience 1000))
(id-root ?id possess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI ?id ?id1)
(id-root ?id1 eye|hair) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yukwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  possess.clp 	possess1   "  ?id "  yukwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (24-03-2015)
;He was charged with possessing a shotgun without a licence.	[oald]
;उस पर बिना स्वच्छन्दता की बन्दूक पास रखने का दोष लगा/आरोप लगाया गया था .	[Manual] 
(defrule possess2
(declare (salience 1000))
(id-root ?id possess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 shotgun|gun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  possess.clp 	possess2   "  ?id "  pAsa_raKa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (24-03-2015)
;The gallery possesses a number of the artist's early works. [oald]
;चित्रशाला में बहुत से कलाकार की प्रारम्भिक रचना रखी हैं . 	                           [Manual]
(defrule possess3
(declare (salience 1000))
(id-root ?id possess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 gallery) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(assert (kriyA_id-subject_viBakwi ?id meM)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  possess.clp 	possess3   "  ?id "  raKa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  possess.clp    possess3  "  ?id " meM  )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (24-03-2015)
;Belgium was the first European country to possess a fully fledged rail network. [oald]
;बेल्जियम पहला युरोपीय देश था जिसके पास एक पूर्ण रूप से मजबूत रेल नेटवर्क हैं.  [Manual]
(defrule possess4
(declare (salience 1000))
(id-root ?id possess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(saMjFA-to_kqxanwa  ?id2 ?id)
(id-root ?id2 country|city|state|district) 
(kriyA-object ?id ?id1)
(id-root ?id1 network|bridge) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  possess.clp 	possess4   "  ?id "  pAsa )" crlf))
)


;---------------------------DEFAULT RULE-----------------------------------------
;@@@ Added by 14anu-ban-09 on (24-03-2015)
;NOTE:-Example sentence need to be added.
(defrule possess0
(declare (salience 000))
(id-root ?id possess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XAraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  possess.clp 	possess0   "  ?id "  XAraNa_kara )" crlf))
)

;--------------------------------------------------------------------------------

