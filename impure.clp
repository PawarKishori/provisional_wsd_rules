;@@@ Added by 14anu-ban-06 (07-02-2015)
;She was accused of having impure thoughts about her male students.(cambridge)
;उस पर नर विद्यार्थियों के बारे में अनैतिक विचार होने का आरोप लगा था . (manual)
(defrule impure1
(declare (salience 2000))
(id-root ?id impure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 thought|motive|behaviour)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anEwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impure.clp 	impure1   "  ?id "  anEwika )" crlf))
)

;----------------------- Default Rules -----------------------
;@@@ Added by 14anu-ban-06 (07-02-2015)
;Cold foods and salads may contain impure water. (COCA)
;ठण्डे आहार और सलाद में अशुद्ध पानी हो सकता है . (manual)
(defrule impure0
(declare (salience 0))
(id-root ?id impure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aSuxXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impure.clp   impure0   "  ?id "  aSuxXa )" crlf))
)


