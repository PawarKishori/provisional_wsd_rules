
;@@@ Added by 14anu-ban-11 on (18-04-2015)
;He came up with a clever wheeze. (oald)
;वह चतुर चाल के साथ आया . (self)
(defrule wheeze0
(declare (salience 10))
(id-root ?id wheeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 clever)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wheeze.clp 	wheeze0   "  ?id "  cAla)" crlf))
)


;@@@ Added by 14anu-ban-11 on (18-04-2015)
;I could hear the old man behind me wheezing.(cald)
;मैंने मेरे पीछे जोर जोर से सांस लेते हुए वृद्ध आदमी को सुन.  (self)
(defrule wheeze2
(declare (salience 20))
(id-root ?id wheeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-behind_saMbanXI  ?id1 ?id)
(id-root ?id1 hear)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_jora_se_sAMsa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wheeze.clp 	wheeze2   "  ?id "  jora_jora_se_sAMsa_le)" crlf))
)

;-------------------------------------Default rules----------------------------------------------------

;@@@ Added by 14anu-ban-11 on (18-04-2015)
;The cough, wheeze, and shortness of breath are things that go with smoking, not with age. (oald)
;खाँसी, साँस की घरघराहट, और साँस की कमी जो धूम्रपान के साथ अाती है  उम्र के साथ नहीं. (self)
(defrule wheeze1
(declare (salience 00))
(id-root ?id wheeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAzsa_kI_GaraGarAhata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wheeze.clp 	wheeze1   "  ?id "  sAzsa_kI_GaraGarAhata)" crlf))
)

;-------------------------------------------------------------------------------------------------------

