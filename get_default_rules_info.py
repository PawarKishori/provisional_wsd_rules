#Programme to print a warning message if more than one default rule exists for a same category
#Note: default rule count in a file is stored in a variable count. If needed this count can be used.
#Written by Roja (24-06-16)
#RUN: python get_default_rules_info.py filename.clp 
##################################################################################################
 
import sys

fr = open(sys.argv[1], 'r')
f = fr.readlines()

#fw = open("def_rule_count" , 'w')

count = 0
flag  = 0
def_rule = 0 

cat_lst = []
cat = ''
rule_name_lst = []
rule_cat_dic = {} 

for i in range(0, len(f)):
	if f[i].startswith('(defrule'):
		flag = 1 
		rule_name = f[i].strip().split()[1]
	elif flag == 1:
		if f[i].startswith('?mng') or f[i].startswith('(declare') or f[i].startswith('\n') or f[i].startswith(';'):
			def_rule = 1
		elif f[i].startswith('(id-root') or f[i].startswith('(id-cat'): #To handle (id-root 1 also) type 
			lst = f[i].strip().split()
			if '?' not in lst[1]: # =(+ ?id 1)
				flag = 0
			if lst[1] != '?id': #?id1 
				flag = 0
			if lst[0].startswith('(id-cat'): #To check whether there are multiple default rules with same category
				cat = lst[2].strip(')')
		elif f[i].strip().startswith('=>'):
			if flag == 1  and def_rule == 1:
				count += 1
				cat_lst.append(cat)
				rule_name_lst.append(rule_name)
				if cat not in rule_cat_dic :
					rule_cat_dic[cat] = rule_name
				else:
					rule_cat_dic[cat] = rule_cat_dic[cat] + ' , ' + rule_name
			else:
				flag = 0
				def_rule = 0
		else:
			flag = 0
			def_rule = 0 
			cat = ''
			
#fw.write(str(count))


#To display multiple default rules with same category
if len(cat_lst) >= 2:
	cat_lst.sort()
	for i in range(0, len(cat_lst)-1):
		if cat_lst[i+1] == cat_lst[i]:
			print 'Default Rule repeated for category: "' + cat_lst[i] + '" with rule names ' + rule_cat_dic[cat_lst[i]]	
