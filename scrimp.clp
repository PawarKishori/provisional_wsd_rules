;@@@ Added by 14anu-ban-01 on (25-03-2015)
;This motherboard doesn't scrimp on features.[coca]
;यह मदरबोर्ड विशेषताओं में कमी नहीं करता है .[self]
; It doesn't pay to scrimp on oil quality.[coca]
;तेल गुणवत्ता में कमी करना लाभदायी नहीं होता है . [self]
(defrule scrimp1
(declare (salience 0))
(id-root ?id scrimp)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrimp.clp 	scrimp1   "  ?id "  kamI_kara )" crlf))
)

;----------------------------------Default rules -----------------------------------------------
;@@@ Added by 14anu-ban-01 on (25-03-2015)
;Choose well, and don't scrimp.[coca]
;अच्छा चुनिए,मितव्ययी मत होइए/कञ्जूसी मत करिए . [self]
(defrule scrimp0
(declare (salience 0))
(id-root ?id scrimp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id miwavyayI_ho/kaFjUsI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrimp.clp   scrimp0   "  ?id "  miwavyayI_ho/kaFjUsI_kara )" crlf))
)


