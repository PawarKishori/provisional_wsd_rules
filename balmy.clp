;@@@Added by 14anu-ban-02(16-03-2015)
;A completely balmy but harmless old man who talked intently to plants and believed they answered back.[mw]
;एक पूरी तरह से पागल परन्तु हानि रहित बूढ़ा आदमी जिसने उत्सुकतापूर्वक पौधों से बातचीत की और माना कि उन्होनें वापिस उत्तर दिया . [self]
(defrule balmy1
(declare (salience 100)) 
(id-root ?id balmy) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 man)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pAgala)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  balmy.clp  balmy1  "  ?id "  pAgala )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(16-03-2015)
;Sentence: A balmy summer evening.[oald]
;Translation: एक सुहावनी  ग्रीष्म सन्ध्या .[self]
(defrule balmy0 
(declare (salience 0)) 
(id-root ?id balmy) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id suhAvanA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  balmy.clp  balmy0  "  ?id "  suhAvanA )" crlf)) 
) 
