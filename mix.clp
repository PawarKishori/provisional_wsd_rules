
(defrule mix0
(declare (salience 5000))
(id-root ?id mix)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id mixed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id miSra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mix.clp  	mix0   "  ?id "  miSra )" crlf))
)

;"mixed","Adj","1.miSra/milA huA"
;She ordered mixed vegetables.
;
;
(defrule mix1
(declare (salience 4900))
(id-root ?id mix)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Brama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " mix.clp	mix1  "  ?id "  " ?id1 "  Brama  )" crlf))
)

;These two sisters are almost identical && i often use to mix them up.
;
(defrule mix2
(declare (salience 4800))
(id-root ?id mix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id miSraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mix.clp 	mix2   "  ?id "  miSraNa )" crlf))
)

;"mix","N","1.miSraNa/Gola"
;Wait till the residue settles down in the mix.
;
(defrule mix3
(declare (salience 4700))
(id-root ?id mix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id miSraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mix.clp 	mix3   "  ?id "  miSraNa_kara )" crlf))
)

;"mix","V","1.miSraNa_karanA"
;She mixed the sounds to create the effect.
;--"2.milAnA"
;Mix the coupons properly.
;--"3.GulanA"
;The colors mix well.
;

;$$$Modified by 14anu-ban-08 (09-01-2015)     ;changed meaning from 'mila-jula' to 'mila_jula'
;@@@ Added by 14anu13 on 28-06-14
;Much , however , as these classes differ from each other , they live together in the same towns and villages , mixed together in the same houses and lodgings .
;यद्यपि इन वर्गों में परस्पर भेद हैं , फिर भी वे उन्हीं नगरों और गांवों में एक साथ रहते हैं और एक ही प्रकार के मकानों और स्थानों में एक - दूसरे से मिल- जुल के रहते हैं .
(defrule mix04
(declare (salience 4700))
(id-root ?id mix)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1   ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mila_jula))        ;changed meaning from 'mila-jula' to 'mila_jula' by 14anu-ban-08
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mix.clp 	mix04   "  ?id "  mila_jula )" crlf))                                        ;changed meaning from 'mila-jula' to 'mila_jula' by 14anu-ban-08
)

;@@@ Added By 14anu-ban-08 on (22-09-2014)
;Turbulence promotes mixing and increases the rates of transfer of mass, momentum and energy.      [NCERT]
;विक्षोभ मिश्रण प्रोत्साहित करता है तथा संहति, संवेग तथा ऊर्जा के स्थानांतर की दर में वृद्धि कर देता है.
(defrule mix4
(declare (salience 4902))
(id-root ?id mix)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 turbulence)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id miSraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mix.clp 	mix4   "  ?id "  miSraNa )" crlf))
)

;@@@ Added By 14anu-ban-08 on (22-09-2014)
;This occurs both because the water has a greater specific heat and because mixing currents disperse the absorbed heat throughout the great volume of water.    [NCERT]
;ऐसा दो कारणों से होता है - पहला जल की विशिष्ट ऊष्मा धारिता उच्च है तथा दूसरा मिश्रित धाराएँ अवशोषित ऊष्मा को विशाल आयतन के जल के सब भागों में विसारित कर देती हैं.
(defrule mix5
(declare (salience 4903))
(id-root ?id mix)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 current)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id miSriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mix.clp 	mix5   "  ?id "  miSriwa )" crlf))
)
