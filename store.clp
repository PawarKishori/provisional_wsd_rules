;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@Added by 14anu04 on 21-June-2014
;Example changed by 14anu-ban-01
;animals store up food for the winter. [self: with reference to oald]
;जानवर  शीत काल के लिए भोजन एकत्रित करते हैं.[self]
(defrule store_tmp
(declare (salience 2000))
(id-root ?id store)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 up)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ekawriwa_kara/ikatTA_kara))       ;added "ekawriwa_kara" and spelling corrected  by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " store.clp 	store_tmp  "  ?id "  " ?id1 "  ekawriwa_kara/ikatTA_kara )" crlf))       ;added "ekawriwa_kara" and spelling corrected  by 14anu-ban-01on (12-01-2015)
)

;Added by Meena(23.11.09)
;I went to the store, got a gallon of milk and returned the eggs.
(defrule store0
(declare (salience 5000))
(id-root ?id store)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xukAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  store.clp     store0   "  ?id "  xukAna )" crlf))
)

;@@@ Added by jagriti(24.1.2014)
;A store of energy. 
;ऊर्जा का संचय . 
;Good store of food is required in every house.
;आहार का अच्छा संचय प्रत्येक घर में जरूरी है . 
(defrule store1
(declare (salience 4900))
(id-root ?id store)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 energy|food|knowledge|chocolate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMcaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  store.clp     store1   "  ?id "  saMcaya )" crlf))
)

;$$$ Modified by jagriti(24.1.2014)....
;Changed default meaning from 'saMciwa_mAla' to 'xukAna'
;Buyers from stores are given the opportunity to go through fashion collections item by item and place orders.
;Salience reduced by Meena(23.11.09)
(defrule store2
(declare (salience 100))
(id-root ?id store)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xukAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  store.clp 	store2   "  ?id "  xukAna )" crlf))
)

;"store","N","1.saMciwa_mAla"
;Good store of food is required in every house.
;--"2.BaMdZAra"
;Benetlon has a good store of clothes.
;--"3.xukAna"
;There are several cosmetic stores in a city market.

;@@@ Added by jagriti(24.1.2014)
;A mind well stored with facts .
;मन अच्छी तरह तथ्यों से भरा हुआ है.
(defrule store3
(declare (salience 4800))
(id-root ?id store)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  store.clp 	store3   "  ?id "  BarA_ho )" crlf))
)

;@@@ Added by 14anu-ban-01 on (12-01-2015)
;Example removed from store_tmp and added here.
;You are storing up problems for yourself. 
;आप खुद के लिए समस्याएँ बढ़ा रहे हैं . [Translation improved by 14anu-ban-01]
(defrule store5
(declare (salience 2500))
(id-root ?id store)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 up)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id2 problem|trouble)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " store.clp 	store5  "  ?id "  " ?id1 "  baDZA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (31-03-2015)
;Torrential rain swept away a car from an Oregon grocery store parking lot. [partial sentence from COCA]
;मूसलाधार वर्षा ऒरगन किराने की दुकान के पार्किंग स्थल से गाडी बहा ले गई . [self]
(defrule store6
(declare (salience 1000))
(id-root ?id store)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) grocery)
(id-cat_coarse ?id noun)
=>
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) kirAne_kI_xukAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " store.clp 	store6  "  ?id "  " (- ?id 1)"  kirAne_kI_xukAna)" crlf))
)


;----------- Default rules -------------------

(defrule store4
(declare (salience 100))
(id-root ?id store)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMciwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  store.clp 	store4  "  ?id "  saMciwa_kara )" crlf))
)

;"store","V","1.saMciwa_karanA"
;Store the vegetables in an airtight container.
;
