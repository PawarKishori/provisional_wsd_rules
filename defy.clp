;@@@ Added by 14anu-ban-04 (09-03-2015)
;A forest fire raging in southern California is defying all attempts to control it.          [cald]
;दक्षिणी कैलिफोर्निया में  एक जंगल में लगी आग  तेज़ी से फैल  रह रही है  इसे क़ाबू में करने के लिए सभी प्रयास निष्फल हो रहे है .           [self]
(defrule defy1
(declare (salience 20))
(id-root ?id defy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 attempt)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niRPala_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defy.clp 	defy1   "  ?id "  niRPala_ho )" crlf))
)

;@@@ Added by 14anu-ban-04 (09-03-2015)
; I defy you to prove your accusations.                    [cald]
;तुम्हारे आरोपों को सिद्ध करने के लिए मैं तुम्हें  चुनौती देता हूँ  .                   [self]
(defrule defy2
(declare (salience 20))
(id-root ?id defy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cunOwI_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defy.clp 	defy2  "  ?id "  cunOwI_xe )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (09-03-2015)
;It is rare to see children openly defying their teachers.                       [cald]
;यह देखना दुर्लभ है  कि  बच्चे अपने शिक्षकों का खुलेआम से विरोध कर रहे हैं .                               [self]
(defrule defy0
(declare (salience 10))
(id-root ?id defy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  defy.clp 	defy0   "  ?id "  viroXa_kara )" crlf))
)

