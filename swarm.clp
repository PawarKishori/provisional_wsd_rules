;$$$ Modified by 14anu-ban-01 on (01-04-2015):corrected meaning
;Gnats came in swarms to torment them.[oald]
;काटनेवाली मक्खियाँ उनको सताने के लिए झुण्ड में आईं . 
;The swarm would be swept away if it came into the river.[COCA]--parse no.2
;यदि जमघट नदी में आया तो वह तबाह हो/मिट जाएगा  . [self]
(defrule swarm0
(declare (salience 5000))
(id-root ?id swarm)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JuNda))	;changed "BinaBinAnA" to "JuNda" by 14anu-ban-01 on (01-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swarm.clp 	swarm0   "  ?id "  JuNda )" crlf))	;changed "JuNda" to "jamaGata" by 14anu-ban-01
)

(defrule swarm1
(declare (salience 4900))
(id-root ?id swarm)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JuMda_meM_cakkara_kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swarm.clp 	swarm1   "  ?id "  JuMda_meM_cakkara_kAta )" crlf))
)

;"swarm","V","1.JuMda_meM_cakkara_kAtanA"
;The bees are swarming around the bee-hive.
;This market is swarming with people.
;
;

;@@@ Added by 14anu-ban-01 on (01-04-2015)
;After the game, thousands of fans swarmed onto the pitch.[cald]
;खेल के बाद, हजारों प्रशंसक पिच [खेल के लिए स्थान] पर एकत्रित हुए . [self]
(defrule swarm2
(declare (salience 4900))
(id-root ?id swarm)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 pitch|track|road|ground)
(kriyA-onto_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekawriwa_ho/ikatTA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swarm.clp 	swarm2   "  ?id "  ekawriwa_ho/ikatTA_ho )" crlf))
)

;@@@ Added by 14anu-ban-01 on (01-04-2015)
;The capital city is swarming with police.[oald]
;राजधानी पुलिस से भर रही है . [self]
(defrule swarm3
(declare (salience 4900))
(id-root ?id swarm)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  swarm.clp 	swarm3   "  ?id " Bara)" crlf))
)


