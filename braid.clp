;@@@Added by 14anu-ban-02(13-04-2015)
;The curtains were trimmed with silk braid. [oald]	;run the sentence on parser no. 3
;पर्दे रेश्मी फीते से सजाये गये थे . [self]
(defrule braid2
(declare (salience 100))
(id-root ?id braid)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 silk|gold)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PIwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  braid.clp 	braid2   "  ?id "  PIwA )" crlf))
)

;---------------------- Default Rules----------------------------

;"braid","N","1.veNI"
;She decorated her long braids with flowers.
(defrule braid0
(declare (salience 0))	;salience reduced to 0 from 5000 by 14anu-ban-02(13-04-2015)
(id-root ?id braid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id veNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  braid.clp 	braid0   "  ?id "  veNI )" crlf))
)

;"braid","VT","1.batanA/gUMWanA"
;Braid a collar
;Braid hair
(defrule braid1
(declare (salience 0))	;;salience reduced to 0 from 4900 by 14anu-ban-02(13-04-2015)
(id-root ?id braid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  braid.clp 	braid1   "  ?id "  bata )" crlf))
)

