;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@Added by 14anu13  on 29-06-14
;In evening time they are said to hover around rivers , lakes and deserted gardens and forests and so the people shun these places after dark .
;प्राय : ये भूत तथा चुडैल श्मशानों , नदी - तटों निर्जन वन - वाटिकाओं में  मँडराते है , अत : लोग रात में ऐसे स्थानों से दूर रहते हैं |
(defrule shun1
(declare (salience 5000));salience increased from 4000 to 5000 by 14anu-ban-01 on (30-12-2014)
(id-root ?id shun)
;(id-root ?id1 places)commented by 14anu-ban-01 on (30-12-2014)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 place|area|road);added by 14anu-ban-01 on (30-12-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_raha));changed "se_xUra_raha" to "xUra_raha" by 14anu-ban-01 on (30-12-2014)
(assert (kriyA_id-object_viBakwi ?id se));added by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  shun.clp 	shun1  "  ?id " se )" crlf);added by 14anu-ban-01 on (30-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shun.clp 	shun1   "  ?id "  xUra_raha )" crlf));changed "se_xUra_raha" to "xUra_raha" by 14anu-ban-01 on (30-12-2014)
)

;@@@ Added by 14anu-ban-11 on (11-04-2015)
;After the trial he was shunned by friends and family alike.(oald)
;मुकदमे के बाद उसको समान रूप से मित्रों और परिवार के द्वारा अलग कर दिया गया था . (self)
(defrule shun3
(declare (salience 4001))
(id-root ?id shun)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 friend)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shun.clp 	shun3  "  ?id "  alaga_kara_xe)" crlf))
)


;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@Added by 14anu13  on 29-06-14
;He was expelled from his native country.   [sentence  from http://www.thefreedictionary.com/shun]
;वह अपने मूल देश से निकाल दिया गया था|
;Above given example must be removed because it is irrelevant.
;Who says spiritual travelers must shun earthly comforts.[COCA]--> Run this sentence on parser no. 5(because 'subject_viBakwi' is not applicable at parser no.0 because it does not recognize 'spiritual travelers' as subject of 'shun')
;कौन कहता है कि आध्यात्मिक/धार्मिक पथिकों को सांसारिक सुख-साधन त्याग देने चाहिये.[self]
(defrule shun0
(declare (salience 4000))
(id-root ?id shun)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wyAga_xe));changed "nikAla_xe" to "wyAga_xe" by 14anu-ban-01 on (30-12-2014)
(assert (kriyA_id-subject_viBakwi ?id ko));added by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  shun.clp 	shun0  "  ?id " ko )" crlf);added by 14anu-ban-01 on (30-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shun.clp 	shun0   "  ?id "  wyAga_xe )" crlf));changed "nikAla_xe" to "wyAga_xe" by 14anu-ban-01 on (30-12-2014)
)


