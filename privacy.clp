;@@@ Added by 14anu-ban-01 on (22-01-2016)
;A section at the back of the warehouse was partitioned off to provide some privacy. [oald]
;गोदाम के पीछे वाले भाग को  एकान्तता के लिए अलग कर दिया गया था .[self]
(defrule privacy0
(declare (salience 0))
(id-root ?id privacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekAnwawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  privacy.clp      privacy0   "  ?id "  ekAnwawA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (22-01-2016)
;She complained that the photographs were an invasion of her privacy. [oald]
;उसने शिकायत की कि फोटो उसकी व्यक्तिगतता में हस्तक्षेप/दखलंदाजी थे .[self]
(defrule privacy1
(declare (salience 1000))
(id-root ?id privacy)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyakwigawawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  privacy.clp      privacy1   "  ?id "  vyakwigawawA)" crlf))
)
