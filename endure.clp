;@@@ Added by 14anu-ban-04 (12-02-2015)
;He had to endure the racist taunts of the crowd.                      [oald]
;उसको भीड़ के जातिवादी ताने सहन करने पड़े .                                        [self]
(defrule endure1
(declare (salience 20))
(id-root ?id endure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  endure.clp 	endure1  "  ?id "  sahana_kara)" crlf))
)


;@@@ Added by 14anu-ban-04 (12-02-2015)
;The torn flag has endured as a symbol of freedom.             [oald]
;विदीर्ण झण्डा स्वतन्त्रता के प्रतीक के जैसा रहा है .                              [self]
(defrule endure0
(declare (salience 10))
(id-root ?id endure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  endure.clp 	endure0  "  ?id "  raha)" crlf))
)
