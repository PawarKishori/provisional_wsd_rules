
;"shagged","Adj","1.WakA huA"
;Shagged boy fell asleep without removing his shoes.
(defrule shag0
(declare (salience 5000))
(id-root ?id shag)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id shagged )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id WakA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  shag.clp  	shag0   "  ?id "  WakA_huA )" crlf))
)



;"shag","N","1.kAtA huA wambAkU"
;Shag is filled in cigerette.
(defrule shag1
(declare (salience 4900))
(id-root ?id shag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAtA_huA_wambAkU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shag.clp 	shag1   "  ?id "  kAtA_huA_wambAkU )" crlf))
)

;
;


;$$$ Modified by 14anu-ban-01 on (29-12-2014): run this sentence on Parser no.3 because 'shag' is noun and parser is taking it as verb.
;@@@ Added by 14anu23 19/06/2014
;Shag before marriage is considered a taboo in India.
;शादी से पहले सेक्स  भारत में  वर्जित माना जाता है.
;शादी से पहले सेक्स करना भारत में  वर्जित माना जाता है/शादी से पहले सेक्स भारत में  वर्जित_ कर्म माना जाता है.[Translation modified  14anu-ban-01 on (29-12-2014)]
(defrule shag3
(declare (salience 5000))
(id-root ?id shag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(kriyA-subject  ?id1 ?id)commented by 14anu-ban-01 on (29-12-2014)
(viSeRya-before_saMbanXI  ?id ?);added by 14anu-ban-01 on (29-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id seksa/seksa_karanA));added "seksa_karanA" by 14anu-ban-01 on (29-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shag.clp 	shag3   "  ?id "  seksa/seksa_karanA )" crlf));added "seksa_karanA" by 14anu-ban-01 on (29-12-2014)
)


;$$$ Modified by 14anu-ban-01 on (29-12-2014)
;@@@ Added by 14anu23 19/06/2014
;Example changed by 14anu-ban-01:He has never shagged before.[COCA]
;उसने पहले कभी सेक्स नहीं किया/उसने पहले कभी यौन क्रिया नहीं की [self]
(defrule shag4
(declare (salience 5000));salience increased from 4900 to 5000 by 14anu-ban-01 on (29-12-2014)
(id-root ?id shag)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)));added by 14anu-ban-01 on (29-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id seksa_kara/yOna_kriyA_kara));added "seksa" by 14anu-ban-01 on (29-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shag.clp 	shag4   "  ?id "  seksa_kara/yOna_kriyA_kara )" crlf))
)
