;Commented by 14anu-ban-06 (09-09-2014) - heavy0 and heavy1 are same rules
;(defrule heavy0
;(declare (salience 5000))
;(id-root ?id heavy)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat ?id adjective|adjective_comparative|adjective_superlative)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id BArI))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heavy.clp 	heavy0   "  ?id "  BArI )" crlf))
;)


;@@@ Added by 14anu-ban-06 (09-09-2014)
;After 1857 the heavy hand of the British fell more on the Muslims than on the Hindus .(parallel corpus)
;सन 1857 के बाद अंग्रेजों का वार हिंदुओं की बनिस़्बत मुसलमानों पर गहरा रहा .
(defrule heavy2
(declare (salience 5100))
(id-root ?id heavy)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 hand)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " heavy.clp	 heavy2  "  ?id "  " ?id1  " vAra  )" crlf))
)   
;@@@ Added by 14anu-ban-06 (07-10-2014)
;The path was ankle-deep in weeds, and to either side the overgrown rhododendrons, heavy with flowers, crowded in over the mosses and ferns of a mild damp climate . (wiki)
;पथ घास फूस से टखनो तक भरा था, और कोई सी भी तरफ आच्छादित बुरुंश के फूल,फूलो से भरा, एक सुहावनी नम जलवायु काई और फर्न से परिपूर्ण थी.(manual)
;Her drive me out of town, to where the orange trees were heavy with blossom. (COCA)
;वह मुझे नगर के बाहर ले गया, जहाँ नारङ्गी के पेड कोंपल से भरे थे.(manual)
(defrule heavy3
(declare (salience 5150))
(id-root ?id heavy)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-with_saMbanXI ?id ?id1)
(id-root ?id1 flower|blossom)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarA));changed 'affecting_ids' to 'wsd_root' by 14anu-ban-06 (02-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heavy.clp 	heavy3   "  ?id "  BarA )" crlf))
)
 

;@@@ Added by 14anu-ban-06 (13-12-2014)
;She'd had a heavy day. (OALD)
;उसका एक व्यस्त दिन था . (manual)
;I have a pretty heavy schedule.(COCA)
;मेरा कार्यक्रम काफी व्यस्त है .(manual) 
(defrule heavy4
(declare (salience 5150))
(id-root ?id heavy)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 day|date|schedule)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyaswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heavy.clp 	heavy4   "  ?id "  vyaswa )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-02-2015)
;Meera had a heavy fall.(heavy.clp)
;मीरा जोर से गिर गयी.(manual)
(defrule heavy5
(declare (salience 5250))
(id-root ?id heavy)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 fall)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heavy.clp 	heavy5   "  ?id "  jora_se )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-02-2015)
;The road was lined on both sides with trees heavy with apples.(heavy.clp)
;सड़क के दोनो ओर सेबों से लदे पेड़ों की कतारें थीं.(manual)
(defrule heavy6
(declare (salience 5250))
(id-root ?id heavy)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-with_saMbanXI ?id ?id1)
(id-root ?id1 apple|jackfruit|fruit|mango|orange)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laxe_hue))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heavy.clp 	heavy6   "  ?id "  laxe_hue )" crlf))
)


;------------------------- Default Rules -------------------
(defrule heavy1
(declare (salience 4900))
(id-root ?id heavy)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  heavy.clp 	heavy1   "  ?id "  BArI )" crlf))
)

;"heavy","Adj","1.BArI"
;eka hamAla sO kilo"heavy"borA pITa para uTA lewA hE.


;
;
;LEVEL 
;Headword : heavy
;
;Examples --
;
;--"1.BArI"
;The suitcase is very heavy.
;sUtakesa bahuwa BArI hE.
;He does the heavy work in the lab.
;vaha lEba meM BArI kAma karawA hE.
;Kanti is heavy with child (pregnant)
;kAnwI (ke pAzva) bacce ke kAraNa BArI hE.
;We had heavy rain yesterday.
;kala hamAre yahAz BArI varRA huI.
;--"2.bahuwa_[BArI]"
;The city had heavy casualties due to the earth quake.
;BUkampa ke kAraNa Sahara meM bahuwa mOweM huIM (BArI nuksAna) huA.
;This road has heavy traffic in the evenings.
;SAma ko isa sadZaka para bahuwa trEPika howA hE. 
;He is a heavy drinker.
;vaha bahuwa BArI piyakkadZa hE.
;--"3.laxe"
;The road was lined on both sides with trees heavy with apples.
;sadZaka ke xono ora seboM se laxe pedZoM kI kawAreM WIM.<--pedZa seboM ke BAra meM xabe hEM
;--"4.vyaswa"
;She has a heavy date tomorrow.
;kala usakA bahuwa vyaswa xina hE. <---kAma kA BAra honA
;--"5.BArI_karanevAlA"
;He had a heavy meal yesterday.
;kala hama logoM ne (peta) BArI karanevAlA KAnA KAyA.
;The book made a heavy reading.
;kiwAba paDZane meM (sira) BArI karanevAlI hE.
;--"6.jZora_se"
;Meera had a heavy fall.
;mIrA jora se gira gayI. 
;--"7.saKwa"
;Meeta is rather heavy with her children.
;mIwA apane baccoM ke sAWa kAPI saKwa hE. 
;
;ina uxAharaNoM se eka pramuKa arWa jo uBara kara AwA hE vaha 'BArI' kA hE. xeKanA yaha hE ki anya arWa isase kEse judZe hEM. uxAharaNa 1 se 5 waka wo spaRtawaH 'BArI' kA arWa liye huse hEM. praSna 6 Ora 7 kA hE. uxAharaNa 6 meM  'girane' ke liye 'BArI' kA prayoga hE.samBavawaH yahAz saMkewa girane ke 'BArI pariNAma' para hE. EsA giranA jisakA pariNAma 'BArI cota' ho. isIprakAra se uxAharaNa 7 meM SAyaxa saMkewa 'anuSAsana ke BAra' kI ora hE.
;isa waraha se 'heavy' Sabxa ke alaga alaga arWoM ke paraspara sambanXa ko nimna wAra se jodZA jA sakawA hE --
;
;anwarnihiwa sUwra ;
;
; 
;                              BArI -BArI karanevAlA
;                               |
;                               BAravAlA
;                 |-------------|--------------|
;               (vaswu kA BAra)                (kAma_kA)
;                 |aXikaraNa                      |aXikaraNa
;                laxA                          vyaswa
;                              
;
; sUwra : BArI` 
