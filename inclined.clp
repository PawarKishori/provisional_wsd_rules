;$$$ Modified by 14anu-ban-06   (26-08-14) copied from incline.clp to inclined.clp and reduced salience to 0
;She was inclined to trust him.(OALD) 
;vaha usa para BarosA karane ko pravqwwa WI.
;I'm inclined to agree with you. (OALD)
;mEM Apake sAWa sahamawi le lie pravqwwa hUz.
(defrule inclined0
(declare (salience 0))
(id-root ?id inclined)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pravqwwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  inclined.clp  	inclined0   "  ?id "  pravqwwa )" crlf))
)

;@@@ Added by 14anu-ban-06  (26-08-14)
;Her speeches are usually more inclined towards people welfare , equality and truthfulness .(Parallel corpus)
;usake BARaNa ka JukAva loga kI xeKaBAla ora, samAnawA Ora saccAI kI ora hEM.
(defrule inclined1
(declare (salience 2000))
(id-root ?id inclined)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-towards_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JukAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  inclined.clp  	inclined1   "  ?id "  JukAva )" crlf))
)
