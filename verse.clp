
(defrule verse0
(declare (salience 5000))
(id-root ?id verse)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id versed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id nipuNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  verse.clp  	verse0   "  ?id "  nipuNa )" crlf))
)

;"versed","Adj","1.nipuNa"
;Kalidasa was versed in similies
;
(defrule verse1
(declare (salience 4900))
(id-root ?id verse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaviwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  verse.clp 	verse1   "  ?id "  kaviwA )" crlf))
)

;"verse","N","1.kaviwA"
;Milton's verses are well known
;
;

;@@@Added by 14anu13 on 01-07-14
;For they do not like all the verses of a long poem to belong to one and the same metre .
;इसका कारण यह है कि वे नहीं चाहते कि एक लंबे काव्य के सभी पद एक ही छंद में हों .
(defrule verse2
(declare (salience 5000))
(id-root ?id verse)
(id-root ?id1 poem)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI 	?id 	 ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  verse.clp 	verse2   "  ?id "  paxa )" crlf))
)
