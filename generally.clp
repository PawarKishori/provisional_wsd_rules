
;@@@ Added by 14anu-ban-05 on (14-11-2014)
;Meningitis caused by a virus is generally less serious . 
;वायरस से कारण बना गर्दन तोड बुखार आम तौर पर कम गम्भीर है . 
;Women generally earn less than men.[OALD]
;mahilAez AmawOra para puruRoM kI wulanA meM kama kamAwI hEM. [MANUAL]
(defrule generally0
(declare (salience 1000))
(id-root ?id generally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id AmawOra_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  generally.clp  	generally0   "  ?id "  AmawOra_para )" crlf))
)

;@@@ Added by 14anu-ban-05 on (14-11-2014)
;It turns out, however, that the law in the same form applies to a rigid body or, even more generally, to a system of particles.[NCERT]
;waWApi isa niyama ko isI rUpa meM xqDa piMdoM aWavA, yahAz waka ki vyApaka rUpa meM kaNoM ke nikAya para BI lAgU kiyA jAwA hE.[NCERT]
;Currents are not always steady and hence more generally, we define the current as follows.[NCERT]
;vixyuwa XArAez saxEva aparivarwI nahIM howIM, isalie aXika vyApaka rUpa meM hama vixyuwa XArA ko nimna prakAra se pariBARiwa karawe hEM.[NCERT]
(defrule generally1
(declare (salience 5000))
(Domain physics)
(id-root ?id generally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root =(- ?id 1) more)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id vyApaka_rUpa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  generally.clp 	generally1   "  ?id "  vyApaka_rUpa_meM )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  generally.clp 	generally1   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (14-11-2014)
;A rigid body generally means a hard solid object having a definite shape and size.[NCERT]
;sAXAraNawayA xqDa piMda kA arWa howA hE eka EsA kaTora Tosa paxArWa jisakI koI niSciwa Akqwi waWA AkAra ho.[NCERT]
;Generally a large margin of safety (of about a factor of ten in the load) is provided.[NCERT]
;sAXAraNawayA, surakRA ke lie BAra meM eka badA mArjina (lagaBaga xasa ke guNaka kA) xiyA jAwA hE.[NCERT]
(defrule generally2
(declare (salience 5000))
(Domain physics)
(id-root ?id generally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 mean|provide)	;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id sAXAraNawayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  generally.clp 	generally2   "  ?id "  sAXAraNawayA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  generally.clp 	generally2   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (14-11-2014)
;The term field in physics generally refers to a quantity that is defined at every point in space and may vary from point to point.[NCERT]
;BOwikI meM kRewra Sabxa kA upayoga vyApaka rUpa se usa rASi ko nirxiRta karane ke lie kiyA jAwA hE, jo xiksWAna ke prawyeka biMxu para pAriBARiwa kI jA sake waWA eka biMxu se xUsare biMxu para parivarwiwa howI ho.[NCERT]
(defrule generally3
(declare (salience 5000))
(Domain physics)
(id-root ?id generally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 refer)	;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id vyApaka_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  generally.clp 	generally3   "  ?id "  vyApaka_rUpa_se )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  generally.clp 	generally3   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (14-11-2014)
;These materials, generally called conductors, develop electric currents in them when an electric field is applied.[NCERT]
;ina paxArWoM jinheM sAmAnyawaH cAlaka kahawe hEM, meM vixyuwa kRewra anuprayukwa karane para vixyuwa XArA uwpanna ho jAwI hE.[NCERT]
(defrule generally4
(declare (salience 5000))
(Domain physics)
(id-root ?id generally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 call)	;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id sAmAnyawaH))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  generally.clp 	generally4   "  ?id "  sAmAnyawaH )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  generally.clp 	generally4   "  ?id "  physics )" crlf))
)



