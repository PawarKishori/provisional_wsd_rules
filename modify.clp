;-----------------Default Rule---------------------------
;@@@ Added by 14anu-ban-08 (02-12-2014)
;We would have to modify the radio before it could be used.   [NCERT]
;रेडियो को इस्तेमाल करने से पहले हमें इसे  सुधारना होगा.    [NCERT]
(defrule modify0
(declare (salience 0))
(id-root ?id modify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suXAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  modify.clp    modify0   "  ?id "  suXAra)" crlf))
)


;@@@Added by 14anu-ban-08 (02-12-2014)
;The principle of conservation of energy will have to be modified in this case.    [NCERT]
;इस स्थिति में, ऊर्जा-सरङ्क्षण नियम में किंचित परिवर्तन करना पडेगा.    [NCERT]
(defrule modify1
(declare (salience 10))
(id-root ?id modify)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id2 ?id1)
(kriyA-kriyArWa_kriyA ?id2 ?id)
(id-root ?id1 principle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  modify.clp    modify1   "  ?id " parivarwana_kara)" crlf))
)
