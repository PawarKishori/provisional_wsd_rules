;$$$Modified by 14anu-ban-08 (07-04-2015)      ;removed '-' from nOsEnika
(defrule naval0
(declare (salience 5000))
(id-root ?id naval)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOsEnika))   ;remove '-' from nOsEnika by 14anu-ban-08 (07-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  naval.clp 	naval0   "  ?id "  nOsEnika )" crlf))  ;remove '-' from nOsEnika by 14anu-ban-08 (07-04-2015)
)

;$$$Modified by 14anu-ban-08 (07-04-2015)     ;commented printout, added relation
;The breakneck production of naval vessels during World War II.[mw]
;विश्व युद्ध II के दौरान नौसैनिक जहाजो का अन्धाधुन्ध निर्माण.[self]
(defrule naval1
(declare (salience 5001))      ;salience increased by 14anu-ban-08 (07-04-2015)
(id-root ?id naval)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)         ;added by 14anu-ban-08 (07-04-2015)
(id-root ?id1 vessel)              ;added by 14anu-ban-08 (07-04-2015)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id sAmuxrika))       ;commented by 14anu-ban-08 (07-04-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nOsEnika_jahAja))  ;added by 14anu-ban-08 (07-04-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  naval.clp 	naval1   "  ?id "  sAmuxrika )" crlf))      ;commented by 14anu-ban-08 (07-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " naval.clp	naval1  "  ?id "  " ?id1 "  nOsEnika_jahAja )" crlf))   ;added by 14anu-ban-08 (07-04-2015)
)

;"naval","Adj","1.sAmuxrika"
;The naval force of that country is very huge.
;
;
