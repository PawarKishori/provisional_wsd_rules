
(defrule follow0
(declare (salience 5000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id following )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aglA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  follow.clp  	follow0   "  ?id "  aglA )" crlf))
)

(defrule follow1
(declare (salience 4900))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id following )
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id hone_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  follow.clp  	follow1   "  ?id "  hone_para )" crlf))
)

;"following","Prep","1.hone_para"
;There was a major uprising following his arrest.
;
;
(defrule follow2
(declare (salience 4800))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Ora_aXika_jAnakArI_prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " follow.clp	follow2  "  ?id "  " ?id1 "  Ora_aXika_jAnakArI_prApwa_kara  )" crlf))
)

;I heard the news about him,and decided to follow it up.
;mEne usake bAre meM KZabara sunI wo Ora aXika jAnakArI prApwa karane kA niScaya kiyA
(defrule follow3
(declare (salience 4700))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-through_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwa_waka));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp follow3 " ?id "  aMwa_waka )" crlf)) 
)

(defrule follow4
(declare (salience 4600))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aMwa_waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " follow.clp	follow4  "  ?id "  " ?id1 "  aMwa_waka  )" crlf))
)

(defrule follow5
(declare (salience 4500))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI_le));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp follow5 " ?id "  jAnakArI_le )" crlf)) 
)

(defrule follow6
(declare (salience 4400))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI_le));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp follow6 " ?id "  jAnakArI_le )" crlf)) 
)

(defrule follow7
(declare (salience 4300))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI_le));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp follow7 " ?id "  jAnakArI_le )" crlf)) 
)

(defrule follow8
(declare (salience 4200))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAnakArI_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " follow.clp	follow8  "  ?id "  " ?id1 "  jAnakArI_le  )" crlf))
)

(defrule follow9
(declare (salience 4100))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 i)
(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anusaraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  follow.clp 	follow9   "  ?id "  anusaraNa_kara )" crlf))
)

;inf_clause_-_- && category=verb	ke_bAxa_ho	3.44468249360189
(defrule follow10
(declare (salience 4000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anusaraNa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  follow.clp 	follow10   "  ?id "  anusaraNa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  follow.clp    follow10   "  ?id "  kA )" crlf)
)
)

;@@@ Added by 14anu-ban-05 Prajna Jha on (20-08-2014)
;(v) If necessary, follow the same procedure for another choice of the system.[NCERT]
;(@v) yaxi AvaSyaka ho, wo saMyojana se kisI anya nikAya ke lie BI yahI viXi apanAie.
(defrule follow12
(declare (salience 5000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 procedure)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp follow12 " ?id "  apanA)" crlf)) 
) 

;@@@ Added by 14anu-ban-05 Prajna Jha on (20-08-2014)
;The base units for length, mass and time in these systems were as follows.[NCERT] 
;ina praNAliyoM meM lambAI, xravyamAna evaM samaya ke mUla mAwraka kramaSaH isa prakAra hEM.
(defrule follow13
(declare (salience 5000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(- ?id 1) as)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1)  isa_prakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " follow.clp  follow13  "  ?id " " - ?id 1 "   isa_prakAra)" crlf))
)  

;@@@ Added by 14anu-ban-05 Prajna Jha on (20-08-2014)
;This is the procedure that we followed earlier in analyzing forces on bodies and solving problems without explicitly outlining and justifying the procedure.[NCERT]
;yahI kAryaviXi hamane piNdoM para lage baloM ke viSleRaNa Ora unase judI samasyA ke hala ke lie apanAI WI; hAlAfki,isake lie koI spaRta kAraNa nahIM bawAyA gayA WA.
(defrule follow14
(declare (salience 5000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 we|I|he|she|you)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp follow14 " ?id "  apanA)" crlf)) 
) 

;@@@ Added by 14anu-ban-05 Prajna Jha on (20-08-2014)
;Multiplying both sides by m/2, we have where the last step follows from Newton's Second Law. 
;xonoM pakRoM ko @m/2 se guNA karane para jahAz AKirI caraNa nyUtana ke xviwIya niyamAnusAra hE.
(defrule follow15
(declare (salience 5000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ?id ?id1)	
(id-root ?id1 Law|law|rule)
;(test (> ?id1 ?id))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 niyamAnusAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " follow.clp  follow15  "  ?id " " ?id1 "  niyamAnusAra)" crlf))
) 

;@@@ Added by 14anu-ban-05 Prajna Jha on (20-08-2014)
;The theorem may be stated as follows: The moment of inertia of a body about any axis is equal to the sum of the moment of inertia of the body about a parallel axis passing through its center of mass and the product of its mass and the square of the distance between the two parallel axes. 
;prameya kA kaWana isa prakAra hE: kisI piNda kA, kisI akRa ke pariwaH jadawva AGUrNa, usa yoga ke barAbara hE jo piNda ke xravyamAna kenxra se gujarane vAlI sAmAnAnwara akRa ke pariwaH lie gae jadawva AGUrNa Ora piNda ke xravyamAna waWA xonoM akRoM ke bIca kI xUrI ke varga ke guNanaPala ko jodane se prApwa howA hE.
(defrule follow16
(declare (salience 5000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viSeRaNa  ?id1 ?id)
(id-root ?id1 state|describe)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isa_prakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp 	follow16 " ?id "  isa_prakAra)" crlf)) 
) 


;@@@ Added by 14anu-ban-05 on (15-11-2014)
;One can argue this as follows.[NCERT]
;ise nimna prakAra se sixXa kiyA jA sakawA hE .[NCERT]
(defrule follow17
(declare (salience 5000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_viSeRaNa  ?id1 ?id)
(id-root ?id1 argue)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimna_prakAra_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp 	follow17 " ?id "  nimna_prakAra_se)" crlf)) 
)

;@@@ Added by 14anu20 dated 14/06/2014
;I have to follow through this project.
;मुझे यह परियोजना जारी रखना है . 
(defrule follow18
(declare (salience 1500))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) through)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  jArI_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " follow.clp	follow18  "  ?id "  " (+ ?id 1) "  jaArI_raKa  )" crlf))
)

;@@@ Added by 14anu20 on 23/06/2014.
;A silence followed.
;खामोसी  छाया . 
(defrule follow23_1
(declare (salience 1500))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 silence)
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id    ))
(assert (id-wsd_root_mng ?id CA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  follow.clp      follow23_1   "  ?id "     )" crlf)

(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  follow.clp  	follow23_1   "  ?id "  CA )" crlf))
)
 
;@@@ Added by 14anu-ban-05 on (03-12-2014)
;Plant pathogens follow similar patterns of response to weather and season.[Agriculture]
;पौधों के रोगाणु मौसम और ऋतु के प्रति समान अनुक्रियाओं के पैटर्नों का पालन करते हैं [MANUAL]
(defrule follow19
(declare (salience 5000))
(id-root ?id follow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 pattern|norm|order)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAlana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " follow.clp 	follow19 " ?id "  pAlana_kara)" crlf)) 
)


; Refer Sabdasutra
;default_sense && category=verb	ke_bAx_ho	0
;default_sense && category=verb	anusaraNa karanA/anukaraNa kara	0
;"follow","V","1.anusaraNa karanA/anukaraNa karanA"
;One should follow the precedents set by the peers.
;She follows the same way of life as her parents.
;--"2.pICe_jAnA"
;To reach the city you have to follow markers on this road. 
;Before entering the city, the road follows along the sea.
;--"3.pICA karanA"
;He is so infatuated that he follows her everywhere.
;--"4.AjFA mAnanA"
;In the office you should follow what the boss says.
;--"5.anugamana karanA"
;She is following the medical profession.
;--"6.samaJanA"
;You should try to follow the instructions of the teacher.
;He tried his best to follow the instructions, but failed. 
;Do try to follow how I am playing the instrument.
;--"7.XyAna se sunanA"
;He sat following every word of the leader's speech.
;--"8.varNana karanA"
;The story of this film follows the life of the saint.
;--"9.Gatiwa honA"
;A dispute between us is likely to follow due to the false propaganda created by him.
;--"10.arWa nikalanA"
;If rains are good it does not automatically follow that the crop will be better than previous years.
;
;
;LEVEL 
;
;
;Headword : follow
;  
;Examples --
; 
;`follow' Sabxa ke viviXa prayoga-- 
;
;"follow","V","1.anusaraNa karanA/anukaraNa karanA"
;             (anukaraNa meM kriyA kA anusaraNa)
;One should follow the precedents set by the peers.
;She follows the same way of life as her parents.
;--"2.AjFA mAnanA"
;             (kisI ke nirxeSa Axi kA AcaraNa xvArA anusaraNa)
;In the office you should follow what the boss says.
;--"3.samaJanA"
;             (boxXavya kA buxXi va vicAroM Axi xvArA anusaraNa)
;You should try to follow the instructions of the teacher.
;He tried his best to follow the instructions, but failed.
;Do try to follow how I am playing the instrument.
;--"4.XyAna se sunanA"
;          (SabxoM va vyAKyAna Axi kA SravaNinxriya evaM ekAgrawA xvArA anusaraNa)
;He sat following every word of the leader's speech.
;--"5.."varNana karanA"
;         (varNanIya viRaya kA SabxoM Axi xvArA anusaraNa)
;The story of this film follows the life of the saint.
;--"6.Gatiwa honA"
;          (kriyAoM kA anusaraNarUpa AvaSyaka pariNAma)
;A dispute between us is likely to follow due to the false propaganda created by him.
;--"7."arWa nikalanA"
;          (anusariwawA kA viSleRaNarUpa pariNAma)
;If rains are good it does not automatically follow that the crop will be better t
;----------------------------------------------------------------------------
;
;sUwra : arWa_nikalAnA`[<anusaraNa_karanA`]
;-------------------------
;
;    `follow' Sabxa ke uparinirxiRta viBinna prayogoM meM anusaraNa karane kA sAmAnya
;arWa prawIwa howA hE . ye saBI anusaraNa karane kI kriyA ke arWa-viswAra ke rUpa meM 
;viSleRiwa kiye jA sakawe hEM . tippaNiyoM se yaha spaRta hE . vivaraNa nimna hE--  
;
;-- anukaraNa karanA . anukaraNa kisI kI kriyA kA anusaraNa hE .
;
;-- AjFA mAnanA . kisI kI bAwoM yA nirxeSa Axi kA anusaraNa AcaraNa Axi ke 
;xvArA kiyA jAnA AjFA mAnanA socA jAwA hE .  
;
;-- samaJanA . samaJane se pahale samaJa meM AnevAlI bAwoM kA hamAre vicAroM(buxXi Axi) 
;xvArA anusaraNa kiyA jAwA hE . 
;
;-- XyAna se sunanA . isa kriyA meM ekAgrawA va SravaNenxriya xvArA SabxoM
;(BARaNoM Axi) kA anusaraNa kiyA jAwA hE . 
;
;-- varNana karanA . isa kriyA meM varNanIya viRaya kA SabxoM Axi xvArA anusaraNa kiyA 
;jAwA hE . 
;
;-- Gatiwa honA . kAryoM Axi ke pariNAmarUpa ko Gatiwa hone ke rUpa meM xeKA jA 
;sakawA hE . yaha kAryoM Axi ke anusaraNarUpa pariNAmavAlI AvaSyaka kriyA hE . 
;
;-- arWa nikalanA . anusariwawA ke viSleRaNa se arWa kA nikalanA howA hE .  
;
;


;;@@@Added by 14anu20 dated 14/06/2014
;;He follows me.
;;वह मेरा पीछा करता है.
;(defrule follow1
;(declare (salience 500))
;(id-root ?id follow)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id pICA_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  follow.clp  	follow1   "  ?id "  pICA_kara )" crlf))
;)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 24.06.2014 email-id:sahni.gourav0123@gmail.com
;He is following me.
;वह मुझे पीछा कर रहा है . 
(defrule follow11
(declare (salience 4200))
(id-word ?id following)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pICA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  follow.clp 	follow11   "  ?id "  pICA_kara )" crlf)
)
)

