
;@@@ Added by 14anu04 on 14-June-2014.
;He was trained under experts. 
;वह विशेषग्यों के तहत सिखाया गया था.
(defrule expert_tmp
(declare (salience 5100))
(id-root ?id expert)         ;experts is removed by 14anu-ban-04 (17-12-2014)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 under)
(test (=(- ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viSeRajFoM_ke_wahawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  expert.clp     expert_tmp   "  ?id "  " ?id1 "  viSeRajFoM_ke_wahawa  )" crlf))
)

(defrule expert0
(declare (salience 5000))
(id-root ?id expert)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nipuNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expert.clp 	expert0   "  ?id "  nipuNa )" crlf))
)

;"expert","Adj","1.nipuNa"
;My mother is an expert cook.
;
(defrule expert1
(declare (salience 4900))
(id-root ?id expert)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nipuNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expert.clp 	expert1   "  ?id "  nipuNa )" crlf))
)

;"expert","N","1.nipuNa"
;My mother is an expert in cooking.
;

;$$$ Modified by 14anu19 (26-06-2014)
;Added by sheetal(23-03-10)
;He is apparently an expert on dogs .
;वह ऊपरी तौर से कुत्तों का एक विशेषज्ञ है . 
(defrule expert01
(declare (salience 4950))
(id-root ?id expert)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) on|in)     ;condition is added by 14anu19 (26-06-2014)
;(id-root =(+ ?id 2) dog)   ;commented by 14anu19
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSeRajFa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  expert.clp    expert01   "  ?id "  viSeRajFa )" crlf))
)
