
;$$$ Modified by 14anu-ban-06 (24-02-2015)
;@@@ Added by Prachi Rathore[15-3-14]
;A weak trembling sound issued from his lips.[oald][parser problem]
;नाजुक कम्पन ध्वनि  उसके ओंठ से निकली .  
(defrule issue1
(declare (salience 5500))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-from_saMbanXI  ?id ?);commented by 14anu-ban-06 (24-02-2015)
(id-word ?id1 from)	;added by 14anu-ban-06 (24-02-2015)
(kriyA-upasarga ?id ?id1);added by 14anu-ban-06 (24-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue1   "  ?id "  nikala )" crlf))
)


;$$$ Modified by Bhagyashri Kulkarni (29-10-2016)
;The king is died without  a male issue. ('issue11' sentence)
;राजा एक  नर सन्तान के बिना मर गया है.
;@@@ Added by Prachi Rathore[15-3-14]
;He died without issue.[oald]
;वह बिना संतान के मर गया
(defrule issue3
(declare (salience 5500))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-without_saMbanXI ?id1 ?id)(viSeRya-RaRTI_viSeRaNa ?id ?id1)) ;added '(viSeRya-RaRTI_viSeRaNa ?id ?id1)' by Bhagyashri
(id-root ?id1 her|die) ;added by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMwAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue3   "  ?id "  saMwAna )" crlf))
)

;$$$ Modified by 14anu-ban-06 (25-02-2015)
;He issues a report every year that's closely read by many on Wall Street. (COCA)
;वह एक रिपोर्ट को प्रत्येक वर्ष प्रकाशित करता है जो वॉल् स्ट्रीट पर बहुतों के द्वारा ध्यानपूर्वक पढ़ी जाती है . (manual)
;@@@ Added by Prachi Rathore[15-3-14]
;  We issue a monthly newsletter.[oald]
;हम माहवार एक प्रकार के समाचार पत्र को प्रकाशित करते हैं . 
(defrule issue4
(declare (salience 5500))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 newsletter|magazine|report);added 'report' by 14anu-ban-06 (25-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakASiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue4   "  ?id "  prakASiwa_kara )" crlf))
)


;@@@ Added by Prachi Rathore[15-3-14]
;Issue of the water from the top.[shiksharthi-kosh]
;  सर्वोच्च स्थान से पानी का निकास . 
(defrule issue6
(declare (salience 5000))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 water|document|stamp)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue6   "  ?id "  nikAsa)" crlf))
)

;@@@ Added by Prachi Rathore[15-3-14]
;  You're just avoiding the issue. [oald]
;आप सम्स्या को टाल रहे हैं . 
(defrule issue7
(declare (salience 4500))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samasyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue7   "  ?id "  samasyA)" crlf))
)

;@@@ Added by Prachi Rathore[15-3-14]
;the issue of blankets to the refugees.[oald]
;शरणार्थियों को कम्बलों का वितरण . 
(defrule issue8
(declare (salience 5000))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 blanket)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viwaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue8   "  ?id "  viwaraNa)" crlf))
)

;@@@ Added by Prachi Rathore[15-3-14]
;the issue of a joint statement by the French and German foreign ministers.[oald]
;फ्रेन्च और जर्मन विदेशी मन्त्री के द्वारा एक संयुक्त बयान  का निर्णय . 
(defrule issue9
(declare (salience 5000))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 statement)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirNaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue9   "  ?id "  nirNaya)" crlf))
)

;$$$ Modified by Shruti Singh M.Tech(CS) Banasthali (31-08-2016)
;The article appeared in issue 25. 
;वस्तु अङ्क 25 में प्रकाशित हुई . 
;$$$ Modified by 14anu-ban-06 (11-12-2014)
;@@@ Added by 14anu01 on 23-06-2014
;The December issue of the magazine.
;पत्रिका के दिसंबर अंक.
(defrule issue10
(declare (salience 4500))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (viSeRya-saMKyA_viSeRaNa ?id ?id1)(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)) ;added by Shruti Singh 31-08-2016
;(id-word =(- ?id 1) january|february|march|april|may|june|july|august|september|october|november|december|summer|winter|autumn|spring);modified from 'id-root' to 'id-word' by 14anu-ban-06 (11-12-2014) ;commented by Shruti Singh 31-08-2016
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue10   "  ?id "  aMka)" crlf))
)

;commented by Bhagyashri to merge it in 'issue3'
;$$$ Modified by 14anu-ban-06 (15-12-2014)
;@@@ Added  by 14anu01 on 23-06-2014
;The king is died without  a male issue.
;राजा एक  नर सन्तान के बिना मर गया है.
;(defrule issue11
;(declare (salience 5600));salience increased to avoid clash with 'issue3' by 14anu-ban-06 (15-12-2014)
;(id-root ?id issue)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(id-root =(- ?id 1) male|female)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id saMwAna));meaning changed from 'sanawAna' to 'saMwAna' by 14anu-ban-06 (15-12-2014)
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue11   "  ?id "  saMwAna)" crlf))
;)

;$$$Modified by by Bhagyashri Kulkarni (17.09.2016)
;@@@ Added by 14anu-ban-06 (24-02-2015)
;A terrible scream issued from the room.(cambridge)[parser no.- 476]
;एक भयानक चीख कमरे से आयी . (manual)
(defrule issue12
(declare (salience 5500))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 from)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-subject ?id ?id2)
(id-root ?id2 scream)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id A)) ;meaning changed from AyA to A by Bhagyashri Kulkarni (17.09.2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue12   "  ?id "  A )" crlf))
)


;@@@ Added by shruti singh M.Tech(CS) Banasthali (31-08-2016)
;I bought a set of the new stamps on the date of issue. 
;मैंने जारी होने की तारीख पर एक समूह नये टिकटों का खरीदा .
(defrule issue13
(declare (salience 2000))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id )
(id-root ?id1 date)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jArI_honA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue13   "  ?id "  jArI_honA )" crlf))
)


;----------------- Default rules ------------------------


;@@@ Added by Prachi Rathore[15-3-14]
;  
(defrule issue2
(declare (salience 500))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jArI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue2   "  ?id "  jArI_kara )" crlf))
)

;@@@ Added by Prachi Rathore[15-3-14]
;  
(defrule issue5
(declare (salience 500))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue5   "  ?id "  viRaya )" crlf))
)

;$$$ Modified by Prachi Rathore[15-3-14]
;salience reduced from 5000 to 200
;Modified by Meena(24.6.10)
;The labour was with the Union leader on this issue . 
(defrule issue0
(declare (salience 200))
(id-root ?id issue)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  issue.clp    issue0   "  ?id "  viRaya )" crlf))
)

