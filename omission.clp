;@@@ Added by <14anu13>    24-06-2014
;There were a number of errors and omissions in the article.  [sentence from http://www.oxfordlearnersdictionaries.com/definition/english/omission]
;लेख में बहुत गलतियाँ और चूकें थीं . 
(defrule omission0
(declare (salience 4000))
(id-root ?id omission)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   cUka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  omission.clp 	omission0    "  ?id "   cUka )" crlf))
)




;@@@ Added by <14anu13>    24-06-2014
;Besides , the scientific books of the Hindus are composed in various favourite metres , by which they intend , considering that the books soon become corrupted by additions and omissions , to preserve them exactly as they are , in order to facilitate their being learned by heart , because they consider as canonical only , that which is known by heart , not that which exists in writing .
;साथ ही , हिन्दुओं की शास्त्र - संबंधी पुस्तकों की रचना विभिन्न छंदों में की गई है जिसका अभिप्राय यह रहा होगा कि छंदोबद्ध रचना कंठस्थ करने में आसानी होती है , अन्यथा पुस्तकों में लोग कहीं प्रक्षेप और कहीं विलोपन करके उसे विकृत कर देते हैं . साथ ही , वे उसी को प्रामाणिक मानते हैं जो कंठस्थ हो सके , जो कुछ लिखित रूप में विद्यमान है उसका उनकी दृष्टि में उतना महत्व नहीं |
(defrule omission1
(declare (salience 5000))
(id-root ?id omission)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI 	?id 	 ?)(viSeRya-RaRTI_viSeRaNa 	?id 	 ?)(and(id-root ?id1 shorten)(kriyA-by_saMbanXI 	?id1 	 ?id)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   vilopa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  omission.clp 	omission1    "  ?id "   vilopa )" crlf))
)

