
;@@@ Added by 14anu-ban-11 on (17-03-2015)
;She was carrying a wisp of wild grass in her hand.(hinkhoj)
;वह अपने हाथ में जङ्गली घास का गट्ठर उठा कर ले जा रही थी . (self)
(defrule wisp0
(declare (salience 10))
(id-root ?id wisp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id1 grass)
(id-root ?id2 wild)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 GAsa_kA_gatTara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir*  " wisp.clp   wisp0  "  ?id "  " ?id1 "  " ?id2 "  GAsa_kA_gatTara )" crlf))
)

;@@@ Added by 14anu-ban-11 on (17-03-2015)
;Wisps of hair.(hinkhoj)
;केश की लटें . (self)
(defrule wisp1
(declare (salience 00))
(id-root ?id wisp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wisp.clp 	wisp1   "  ?id "  lata )" crlf))
)

