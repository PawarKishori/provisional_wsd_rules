;@@@Added by 14anu-ban-08 (09-03-2015)
;At our first meeting I was nervous.  [oald]
;हमारी पहली मुलाकात के समय मैं घबराया हुआ था.  [self]
(defrule meeting1
(declare (salience 10))
(id-root ?id meeting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 first)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mulAkAwa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  meeting.clp  	meeting1   "  ?id "  mulAkAwa  )" crlf))
)

;@@@Added by 14anu-ban-08 (09-03-2015)
;He was banned from the meeting.  [oald]
;उस पर  मीटिंग के लिये प्रतिबन्ध लगाया गया था .  [self]
(defrule meeting2
(declare (salience 12))
(id-root ?id meeting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-from_saMbanXI ?id1 ?id)
(id-root ?id1 ban)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mItiMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  meeting.clp  	meeting2   "  ?id "  mItiMga )" crlf))
)
;commented by 14anu-ban-02(27-02-2016)
;@@@Added by 14anu-ban-08 (11-03-2015)
;Let's move the meeting to wednesday.  [from move.clp]
;चलिये हम बैठक बुधवार को रख देते हैं .  [self]
;(defrule meeting3
;(declare (salience 112))
;(id-root ?id meeting)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(kriyA-object ?id1 ?id)
;(id-root ?id1 move)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id bETaka))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  meeting.clp  	meeting3   "  ?id "  bETaka )" crlf))
;)

;------------------------ Default Rules ----------------------
;$$$Modified by 14anu-ban-02(27-02-2016)
;Let's move the meeting to wednesday.  [from meeting3]
;चलिये हम बैठक बुधवार को रख देते हैं .  [self]
;@@@Added by 14anu-ban-08 (09-03-2015)
;The meeting was postponed.  [oald]
;सम्मलेन स्थगित कर दी गई.  [self]
(defrule meeting0
(declare (salience 0))
(id-root ?id meeting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bETaka))	;meaning changed from 'sammelana' to 'bETaka' by 14anu-ban-02(27-02-2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  meeting.clp   meeting0   "  ?id " bETaka )" crlf))
)


