;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu13 on 17-06-14
;This water supply is unsafe.
;यह जलापूर्ति असुरक्षित है |
(defrule supply2
(declare (salience 5100))         ;salience increased from 5000 to 5100 by 14anu-ban-01
(id-root ?id supply)
(id-root ?id1 water)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id jalApUrwi)) ;commented by 14anu-ban-01 on (12-01-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jalApUrwi)) ;added by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " supply.clp 	supply2   "  ?id "  " ?id1 "  jalApUrwi )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-12-2014)
;The electric mains supply in our homes and offices is a voltage that varies like a sine function with time.[NCERT corpus]
;हमारे घरों एवं दफ्तरों में पाया जाने वाला मुख्य विद्युत प्रदाय (electric mains supply) एक ऐसी ही वोल्टता का स्रोत है जो समय के साथ ज्या फलन (sine function) की भाँति परिवर्तित होता है. [NCERT corpus]
(defrule supply3
(declare (salience 100))
(id-root ?id supply)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 military|medical|electrical)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  supply.clp 	supply3   "  ?id "  praxAya )" crlf))
)

;@@@ Added by 14anu-ban-01 on (12-01-2015)
;The supply of blood to the heart should be good.[example taken from supply0]
;hqadaya meM rakwa kI ApUrwI acCI honI cAhie.     [taken from supply0]
;हृदय में रक्त की आपूर्ति अच्छी होनी चाहिए.        [self]
(defrule supply4
(declare (salience 5100))
(id-root ?id supply)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 blood|oxygen|gas|water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ApUrwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  supply.clp 	supply4   "  ?id "  ApUrwi )" crlf))
)


;@@@ Added by 14anu-ban-01 on (17-02-2015)
; The prototypes of the International standard kilogram supplied by the International Bureau of Weights and Measures (BIPM) are available in many other laboratories of different countries.[NCERT corpus]
;अन्तर्राष्ट्रीय माप-तोल ब्यूरो द्वारा दिए गए अन्तर्राष्ट्रीय मानक किलोग्राम के आदिप्ररूप विभिन्न देशों की बहुत सी प्रयोगशालाओं में उपलब्ध हैं.[NCERT corpus]
(defrule supply5
(declare (salience 4900))
(id-root ?id supply)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 PropN)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  supply.clp 	supply5   "  ?id "  xe )" crlf))
)


;--------------------- Default rules ---------------------

;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;$$$ Modified by 14anu13  on 17-06-14 (changed hindi meaning from "सप्लाई" to "पूर्ति" )
;a transport plane is carrying food and medical supplies for refugees.
;मालवाहक जहाज  शऱणागतो के लिए भोजन व दवा पूर्ति कर रहा है |
;मालवाहक जहाज  शऱणागतों के लिए भोजन व दवा सामग्री ले जा रहा है.[Translation improved by 14anu-ban-01 ]
;$$$ Modified by 14anu-ban-02 (26-07-2014)
;Meaning changed from "saplAI" to "ApUrwi"
;------------Removed the following example from here and added in supply4 by 14anu-ban-01 ------
;The supply of blood to the heart should be good.
;hqadaya meM rakwa kI ApUrwI acCI honI cAhie.
(defrule supply0
(declare (salience 5000))
(id-root ?id supply)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmagrI))           ;changed "pUrwi" to "sAmagrI" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  supply.clp 	supply0   "  ?id "  sAmagrI )" crlf))                                           ;changed "pUrwi" to "sAmagrI" by 14anu-ban-01 on (12-01-2015)
)

;"supply","N","1.saplAI"
;The supply of blood to the heart should be good.
;

(defrule supply1
(declare (salience 4900))
(id-root ?id supply)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saplAI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  supply.clp 	supply1   "  ?id "  saplAI_kara )" crlf))
)

;"supply","V","1.saplAI_karanA"
;The baker supplied us bread.
;
