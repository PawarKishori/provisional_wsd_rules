;@@@ Added by 14anu-ban-01 on (07-03-2015)
;The new party subsumed several small left-wing parties. [oald]
;नये दल ने कई छोटे वामपन्थी दल सम्मिलित किए  .[self] 
(defrule subsume0
(declare (salience 0))
(id-root ?id subsume)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammiliwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subsume.clp 	subsume0   "  ?id " sammiliwa_kara )" crlf))
)



;@@@ Added by 14anu-ban-01 on (07-03-2015)
;Some of these rights have been subsumed under the fundamental rights.[self: with respect to cald]
;इनमें से कुछ अधिकार मूलभूत अधिकारों के अन्तर्गत माने गये हैं . [self]
(defrule subsume1
(declare (salience 100))
(id-root ?id subsume)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 rule|principle|right)
(kriyA-under_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id2 under)
(pada_info (group_head_id ?id1) (preposition  ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 anwargawa_mAna))
(assert (id-wsd_viBakwi ?id1 ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   "?*prov_dir* "  subsume.clp 	subsume1  "  ?id1 " ke )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " subsume.clp 	subsume1    "  ?id "  "?id2 "  anwargawa_mAna )" crlf))
)



