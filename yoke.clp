
(defrule yoke0
(declare (salience 5000))
(id-root ?id yoke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gulAmI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  yoke.clp 	yoke0   "  ?id "  gulAmI )" crlf))
)

;"yoke","N","1.gulAmI/parAXInawA/xAsawA"
;Gandhiji threw off the yoke of slavery through non-violence.  
;--"1.juA"
;The farmer ploughed the field with four yoke of oxen.
;
(defrule yoke1
(declare (salience 4900))
(id-root ?id yoke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  yoke.clp 	yoke1   "  ?id "  nAWa )" crlf))
)

;"yoke","VTI","1.nAWanA"
;The farmer yoked oxen to a plough.
;

;@@@ Added by 14anu06(Vivek Agarwal) on 21/6/2014********
;Partners who were yoked together for life.
;साझीदार जो जीवन भर के लिय एक साथ मिल गये थे . 
(defrule yoke2
(declare (salience 5000))
(id-root ?id yoke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 together)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id milanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  yoke.clp 	yoke2   "  ?id "  milanA )" crlf))
)


;@@@ Added by 14anu-ban-11 on (28-03-2015)
;The farmer ploughed the field with four yoke of oxen.(hinkhoj)
;किसान ने चार बैलो की गाडी से खेत जोता . (self)
(defrule yoke3
(declare (salience 5001))
(id-root ?id yoke)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 ox)
(id-root ?id2 of)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id2 ?id1 bEla_kI_gAdZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " yoke.clp  yoke3  "  ?id "  "?id2 "    " ?id1 "   bEla_kI_gAdZI)" crlf))
)




