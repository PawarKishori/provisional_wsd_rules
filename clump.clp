;@@@ Added by 14anu-ban-03 (05-03-2015)
;Galaxies tend to clump together in clusters. [oald]
;निहारिकाओ की गुच्छों मे इकट्ठा होने की  प्रवृत्ति होती हैं . [manual] ;suggested by chaitanya sir.
(defrule clump2
(declare (salience 5000))
(id-root ?id clump)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 cluster)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikatTA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clump.clp 	clump2   "  ?id "  ikatTA_ho )" crlf))
)


;---------------------Default Rules-----------------

;"clump","N","1.gucCA"
;A small clump of rose plants is there in our garden.
(defrule clump0
(declare (salience 5000))
(id-root ?id clump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gucCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clump.clp 	clump0   "  ?id "  gucCA )" crlf))
)

;$$$ Modified by 14anu-ban-03 (05-03-2015)
;The soldiers clumped their feet while marching.[same clp]
;सैनिक कूच करते हुए अपने पाँवों से थप थप करके चले . [manual] 
;"clump","V","1.XamAke_ke_sAWa_calanA"
;The soldiers clumped their feet while marching.
(defrule clump1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (05-03-2015)
(id-root ?id clump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Wapa_Wapa_karake_cala))  ;meaning changed from 'XamAke_ke_sAWa_calanA' to 'Wapa_Wapa_karake_cala' by 14anu-ban-03 (05-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clump.clp 	clump1   "  ?id "  Wapa_Wapa_karake_cala )" crlf))
)

;
