;----------------------------------DEFAULT RULE-------------------------------------------------

;@@@ Added by 14anu-ban-09 on 08-09-2014
;We could see the stars through an opening in the roof. [OALD]
;hama Cawa kI KulI jagaha se wAro ko xeKa sakawe hE. [Own Manual]

(defrule opening0
(id-root ?id opening)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KulI_jagaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opening.clp      opening0  "  ?id "  KulI_jagaha )" crlf))
)

;-------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on 08-09-2014
;Ghana has delayed the opening of universities and colleges by at least two weeks to put in place measures to screen students arriving from Ebola-hit countries. [Anusaaraka_Bing_14th_Aug_2014_Group2]
;GAnA ne robolA-hiwa xeSo se Ae bacco ko jAzcane ke mapaxaMda ko apanAne ke lie viSvavixyAlaya Ora kolejoM kA uxGAtana kama se kama xo sapwAha ke lie vilambiwa kara xiyA. [Own Manual]

(defrule opening1
(declare (salience 5000))
(id-root ?id opening)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 university)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxGAtana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opening.clp      opening1  "  ?id "  uxGAtana )" crlf))
)


;@@@ Added by 14anu-ban-09 on 08-09-2014
;The movie has an exciting opening. [OALD]
;calaciwra kA prAraMBika BAga uwwejanApUrna rahA. [Own Manual]

(defrule opening2
(declare (salience 5500))
(id-root ?id opening)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id2 ?id)
(kriyA-subject  ?id2 ?id1)
(id-root ?id1 movie)
(id-root ?id2 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAMraBika_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  opening.clp      opening2  "  ?id "  prAMraBika_BAga )" crlf))
)


