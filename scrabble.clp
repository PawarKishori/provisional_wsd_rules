;########################################################################
;#  Copyright (C) 2014-2015 14anu26 (noopur.nigam92@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################

;@@@ Addded by 14anu26  [24-06-14]
;Lizard was scrabbling over the wall.[rajpal]
;छिपकली दीवार पर रेंग रही थी. 
(defrule scrabble2
(declare (salience 4800))
(id-root ?id scrabble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-over_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id reMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrabble.clp         scrabble2   "  ?id " reMga )" crlf))
)
                    

;@@@ Addded by 14anu26  [24-06-14]
;we were playing scrabble.
;हम स्क्रैबल खेल रहे थे.
(defrule scrabble0
(id-root ?id scrabble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id skrEbala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrabble.clp 	scrabble0   "  ?id "  skrEbala )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (27-12-2014)
;@@@ Addded by 14anu26  [24-06-14]
;She scrabbled at the grassy slope.
;उसने घासदार ढाल में खरोंचा .
;वह घासदार ढाल पर संतुलन बनाने के लिये आस पास पकड़ते हुऐ चली.[Translation improved by 14anu-ban-01 on (27-12-2014)]
(defrule scrabble1
(id-root ?id scrabble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 slope);added by 14anu-ban-01 on (27-12-2014)
(or(kriyA-at_saMbanXI ?id ?id1)(kriyA-up_saMbanXI ?id ?id1)(kriyA-on_saMbanXI ?id ?id1));added by 14anu-ban-01 on (27-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMwulana_banAne_ke_liye_Asa_pAsa_pakadZawe_hue_cala ));changed "KaroMca" to "saMwulana_banAne_ke_liye_Asa_pAsa_pakadZawe_hue_cala" by 14anu-ban-01 on (27-12-2014).
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrabble.clp 	scrabble1   "  ?id " saMwulana_banAne_ke_liye_Asa_pAsa_pakadZawe_hue_cala )" crlf));
;changed "KaroMca" to "saMwulana_banAne_ke_liye_Asa_pAsa_pakadZawe_hue_cala" by 14anu-ban-01 on (27-12-2014).
)
