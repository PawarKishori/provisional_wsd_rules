;@@@ Added by 14anu-ban-05 Prajna Jha on 31.07.2014
;Let us first guess the answer based on our common experience. 
;sabase pahale hama apane sAmAnya anuBavoM ke AXAra para isa praSna ke uwwara kA anumAna lagAez.
(defrule guess1
(declare (salience 5000))
(id-root ?id guess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 us)
(kriyA-object  ?id ?id2)
(id-root ?id2 answer|outcome|event)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guess.clp   guess1   "  ?id "  anumAna_lagA )" crlf))
)


(defrule guess0
(declare (salience 4800))
(id-root ?id guess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anxAja_lagA))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guess.clp    guess0   "  ?id "  anxAja_lagA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  guess.clp    guess0   "  ?id " kA )" crlf)
)
)
;@@@ Added by 14anu21 on 19.06.2014
;He made a guess.
;उसने राय बनाई .  .(Translation before adding rule)
;उसने अनुमान लगाया.
(defrule guess2
(declare (salience 5000))
(id-root ?id guess)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?idverb ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guess.clp    guess2   "  ?id "  anumAna )" crlf)
)
)

