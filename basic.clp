;$$$ Modified by 14anu-ban-02(12.08.14)
;Meaning changed from buniyAxI to mUla
(defrule basic0
(declare (salience 100))	;Salience decrease to 0 from 5000 by 14anu-ban-02(17-02-2016)
(id-root ?id basic)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)	;Added by 14anu-ban-02(17-02-2016)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  basic.clp 	basic0   "  ?id "  mUla )" crlf))
)

;"basic","Adj","1.buniyAxI"
;First you should have the basic knowledge of computer.
;

;
;@@@ Added by 14anu-ban-02(1.08.14)
;Measurement of any physical quantity involves comparison with a certain basic, arbitrarily chosen, internationally accepted reference standard called unit.
;kisI BOwika rASi kA mApana, eka niSciwa, AXAraBUwa, yAxqcCikaA rUpa se cune gae mAnyawAprApwa, sanxarBa - mAnaka se isa rASi kI wulanA karanA hE ; yaha sanxarBa - mAnaka mAwraka kahalAwA hE.
(defrule basic2
(declare (salience 2000))
(id-root ?id basic)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AXAraBUwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  basic.clp 	basic2   "  ?id "  AXAraBUwa )" crlf))
)
;-----------Default_rule---------------------
(defrule basic1
(declare (salience 1000));(salience decreased from 4900 to 1000 by 14anu-ban-02)
(id-root ?id basic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlawawwva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  basic.clp 	basic1   "  ?id "  mUlawawwva )" crlf))
)

;"basic","N","1.mUlawawwva"
;His basic problem is with the spoken language .

;@@@Added by 14anu-ban-02(17-02-2016)
;Default Rule suggested by Chaitanya Sir.
;The interplay of theory and observation (or experiment) is basic to the progress of science.[ncert 11_01]
;सिद्धान्त तथा प्रेक्षण (अथवा प्रयोग) का पारस्परिक प्रभाव विज्ञान की प्रगति का मूल आधार है.[ncert 11_01]
(defrule basic3
(declare (salience 0))
(id-root ?id basic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla_AXAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  basic.clp 	basic3   "  ?id "  mUla_AXAra )" crlf))
)
