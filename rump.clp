;@@@ Added by 14anu-ban-10 on (24-03-2015)
;He slapped the horse on the rump.[oald]
;उसने घोडे को पिछला हिस्से पर थप्पड़ मारा. [manual]
(defrule rump1
(declare (salience 300))
(id-root ?id rump)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA_hissa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rump.clp 	rump1   "  ?id "  piCalA_hissa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (24-03-2015)
;The election reduced the party to a rump.[oald]
;चुनाव ने  पार्टी  को अवशिष्ट मे  बदल दिया.[manual] 
(defrule rump2
(declare (salience 400))
(id-root ?id rump)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id)
(id-root ?id1 reduce)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaSiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rump.clp 	rump2   "  ?id "  avaSiRta )" crlf))
)

;@@@ Added by 14anu-ban-10 on (24-03-2015)
;It is a piece of good quality meat cut from the rump of a cow.[oald]
;यह एक  उत्तम  गाय के  माँस का टुकडे  जो गाय की पूंछ से कटा हुआ है .[manual] 
(defrule rump3
(declare (salience 500))
(id-root ?id rump)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  pUMCa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rump.clp 	rump3   "  ?id "   pUMCa )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (24-03-2015)
;Get up off your rump and do some work!.[oald]
;आपके पुट्ठे पर से उठिए और कुछ काम किजये . [manual]
(defrule rump0
(declare (salience 200))
(id-root ?id rump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id putTe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rump.clp     rump0   "  ?id " putTe)" crlf))
)

