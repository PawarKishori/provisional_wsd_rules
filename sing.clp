

;Added by Meena(27.8.09)
;She sang me a song.
(defrule sing1
(declare (salience 4900))
(id-root ?id sing)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object_2  ?id ?id2)
(kriyA-object_1  ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gA))
(assert (kriyA_id-object1_viBakwi ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sing.clp 	sing1   "  ?id "  gA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " sing.clp       sing1   "  ?id " ke_liye )" crlf)
)

;"sing","V","1.gAnA gAnA"
;He sings beautifully.
;
;


;@@@ Added by 14anu01 on 30-06-2014
;Film sings have supplanted folk music in the lives of common people
;सिनेमा गाने सामान्य लोगों की जीवनियों में वर्ग विशेष सङ्गीत स्थान या पद ग्रहण कर चुके हैं 
(defrule sing2
(declare (salience 5500))
(id-root ?id sing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sing.clp 	sing2   "  ?id "  gAnA )" crlf))
)
