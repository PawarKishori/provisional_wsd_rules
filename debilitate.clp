;@@@ Added by 14anu-ban-04 (05-03-2015)
;The troops were severely debilitated by hunger and disease.                  [oald]
;भूख और बीमारी से दल  गम्भीर रूप से  दुर्बल हो गये थे .                                          [self] 
(defrule debilitate1
(declare (salience 20))
(id-root ?id debilitate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-by_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xurbala_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  debilitate.clp 	debilitate1   "  ?id "  xurbala_ho )" crlf))
)


;@@@ Added by 14anu-ban-04 (05-03-2015)
;Prolonged strike action debilitated the industry.                     [oald]
;दीर्घकालीन  हड़ताल  क्रिया ने उद्योग को खराब कर दिया .                                [self]
(defrule debilitate2
(declare (salience 20))
(id-root ?id debilitate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id KarAba_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  debilitate.clp     debilitate2   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  debilitate.clp 	debilitate2   "  ?id "  KarAba_kara_xe )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (05-03-2015)
;Chemotherapy exhausted and debilitated him.                          [cald]
;कीमोथेरपी ने उसको क्षीण और  कमजोर कर दिया .                                    [self]
(defrule debilitate0
(declare (salience 10))
(id-root ?id debilitate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id kamajoZra_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  debilitate.clp     debilitate0  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  debilitate.clp 	debilitate0  "  ?id "  kamajoZra_kara_xe )" crlf))
)
