;@@@ Added by 14anu03 on 17-june-14
;It looks as if this play will be a flop.
;यह लगता है यदि यह खेल असफल होगा .
(defrule flop100
(declare (salience 5500))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asaPala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flop.clp      flop100   "  ?id "  asaPala)" crlf))
)

(defrule flop0
(declare (salience 5000))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-back_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gira));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flop.clp flop0 " ?id "  gira )" crlf)) 
)

(defrule flop1
(declare (salience 4900))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-back_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gira));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flop.clp flop1 " ?id "  gira )" crlf)) 
)

(defrule flop2
(declare (salience 4800))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-back_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gira));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " flop.clp flop2 " ?id "  gira )" crlf)) 
)

(defrule flop3
(declare (salience 4700))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 gira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flop.clp	flop3  "  ?id "  " ?id1 "  gira  )" crlf))
)

;@@@ Added by 14anu11
;Film director John Huston was once asked why Fat City , his acclaimed 1972 movie about a struggling boxer , had flopped .
;फिल्म निर्देशक जॉन ह्यूस्टन से एक बार पूछा गया कि एक संघर्षरत मुकंकेबाज के बारे में 1972 की उनकी भचर्चित फिल्म फैट सीटी क्यों पिट गई थी ? 
(defrule flop6
(declare (salience 5000))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pita_gaI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flop.clp 	flop6   "  ?id " pita_gaI )" crlf))
)

;@@@ Added by 14anu-ban-05 on (04-03-2015)
;Hugh's hair keeps flopping over his eyes.[CALD]
;ह्यूग के बाल उसकी आँखों पर गिरते रहते है.		[manual]

(defrule flop7
(declare (salience 5001))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id para))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 gira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flop.clp	flop7  "  ?id "  " ?id1 "  gira  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  flop.clp	flop7  "  ?id " para )" crlf))
)

;@@@ Added by 14anu-ban-05 on (04-03-2015)
;The surprise party was a flop. [word reference forum]
;आश्चर्य -पार्टी  असफल हो गया था.	[manual]

(defrule flop8
(declare (salience 5501))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asaPala_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flop.clp 	flop8   "  ?id " asaPala_ho_jA)" crlf))
)

;@@@ Added by 14anu-ban-05 on (05-03-2015)
;When she gets home from school, she's so tired all she can do is flop down in front of the television.[CALD]
;जब वह स्कूल से घर आती है,वह इतनी थकी हुई होती है कि  वह केवल टीवी के सामने थक कर बैठ जाना यही वह कर सकती है . 	[manual]

(defrule flop9
(declare (salience 5001))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Waka_kara_bETa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flop.clp	flop9  "  ?id "  " ?id1 "  Waka_kara_bETa_jA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (05-03-2015)
;He flopped the newspaper on my desk.[word reference forum]
;उन्होंने मेरी मेज पर अखबार पटक दी.		[manual]

(defrule flop10
(declare (salience 5502))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 newspaper|document)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pataka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flop.clp 	flop10   "  ?id " pataka_xe)" crlf))
)

;-------------------- Default rules -------------------
(defrule flop4
(declare (salience 4600))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PadZaPadZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flop.clp 	flop4   "  ?id "  PadZaPadZA )" crlf))
)

(defrule flop5
(declare (salience 4500))
(id-root ?id flop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PadZaPadZAne_kI_AvAjZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flop.clp 	flop5   "  ?id "  PadZaPadZAne_kI_AvAjZa )" crlf))
)

;"flop","N","1.PadZaPadZAne_kI_AvAjZa"
;The rope hanging outside is creating a flop against the wall.
;--"2.pUrNawaH asaPala"
;He is a flop in whatever he does.
;
;
