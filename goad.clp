
;@@@ Added by 14anu-ban-05 on (17-03-2015)
;He finally goaded her into answering his question.	[OALD]
;उसने अन्ततः अपने प्रश्नों के उत्तर देने के लिये उसको  उकसाया. 		[MANUAL]

(defrule goad2
(declare (salience 4901))
(id-root ?id goad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ukasA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  goad.clp 	goad2   "  ?id "  ukasA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (17-03-2015)
;The boxers were goaded on by the shrieking crowd. [OALD]
;मुक्केबाज चीखती हुई भीड के द्वारा  प्रेरित किए गये थे . [MANUAL]

(defrule goad3
(declare (salience 4903))
(id-root ?id goad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 on)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 preriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " goad.clp  goad3  "  ?id "  " ?id1 "  preriwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (17-03-2015)
;He refused to be goaded by their insults. [CALD]
;उसने उनके अपमानों के द्वारा उकसावे में आने से मना किया .   [manual]

(defrule goad4
(declare (salience 4901))
(id-root ?id goad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ukasAvA_meM_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  goad.clp 	goad4   "  ?id "  ukasAvA_meM_A )" crlf))
)

;@@@ Added by 14anu-ban-05 on (17-03-2015)
;A group of children were goading another child in the school playground.	[CALD]
;बच्चों के समूह विद्यालय क्रीडास्थल में एक और बच्चे को उकसा रहे थे . 				[manual]

(defrule goad5
(declare (salience 4902))
(id-root ?id goad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p " human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ukasA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  goad.clp 	goad5   "  ?id "  ukasA)" crlf))
)
;------------------------ Default Rules ----------------------
(defrule goad0
(declare (salience 5000))
(id-root ?id goad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMkuSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  goad.clp 	goad0   "  ?id "  aMkuSa )" crlf))
)

;"goad","VT","1.preriwa_karanA"
;Vinoth keeps goading me to work hard.
(defrule goad1
(declare (salience 4900))
(id-root ?id goad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id preriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  goad.clp 	goad1   "  ?id "  preriwa_kara )" crlf))
)

;

