
;---------------------------DEFAULT RULE-------------------------------------

;@@@ Added by 14anu-ban-09 on (19-03-2015)
;NOTE:-Example sentence need to be added.

(defrule prowl0
(declare (salience 000))
(id-root ?id prowl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KojawA_Pira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prowl.clp 	prowl0   "  ?id "  SAhI )" crlf))
)

;------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (19-03-2015)
;Two cats were prowling together. 	[Report set. 8]
;दो बिल्लियाँ एक साथ शिकार खोजते फिर रहीं थीं . 	[Anusaaraka]

(defrule prowl1
(declare (salience 1000))
(id-root ?id prowl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 tiger|animal|bird|cat|dog|wolf|cheetah|hyena|lion|crane)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikAra_Kojawe_Pira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prowl.clp 	prowl1   "  ?id "  SikAra_Kojawe_Pira )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-03-2015)
;NOTE:- Parser problem. Run on parser no. 5.
;A man was seen prowling around outside the factory just before the fire started. 	[oald]
;आदमी आग लगने से पूर्व फैक्टरी के बाहर  इधर-उधर घुमता फिरता देखा गया था. 				[Manual]

(defrule prowl2
(declare (salience 1000))
(id-root ?id prowl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 around)
(viSeRya-outside_saMbanXI  ?id1 ?id2)
(id-root ?id1 factory|area|school|college)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumawA_Pira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prowl.clp 	prowl2  "  ?id "  GumawA_Pira )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-03-2015)
;He prowled the empty rooms of the house at night.	[oald]
;वह रात में घर के खाली कमरे में घुमता था . 			[Manual]
;Her husband was prowling restlessly around the room.	[oald]
;उसका पति कमरे के चारों ओर बेचैनी से घुम रहा था .			[Anusaaraka] 

(defrule prowl3
(declare (salience 1000))
(id-root ?id prowl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object ?id ?id1)(kriyA-around_saMbanXI ?id ?id1))
(id-root ?id1 room|street|corridor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Guma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prowl.clp 	prowl3   "  ?id "  Guma )" crlf))
)




