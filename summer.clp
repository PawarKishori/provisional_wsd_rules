;@@@ Added by 14anu-ban-01 on (05-11-2014).
;Today is the hottest day of the summer.[self]
;आज ग्रीष्म का सबसे गरम दिन है.[self]
(defrule summer0
(declare (salience 0))
(id-root ?id summer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id grIRma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  summer.clp 	summer0   "  ?id "  grIRma)" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-11-2014).
;We know from experience that a glass of ice-cold water left on a table on a hot summer day eventually warms up whereas a cup of hot tea on the same table cools down.[NCERT corpus]
;अपने अनुभवों से हम यह जानते हैं कि किसी तप्त गर्मी के दिन एक मेज पर रखा बर्फ के शीतल जल से भरा गिलास अन्ततोगत्वा गर्म हो जाता है जबकि तप्त चाय से भरा प्याला उसी मेज पर ठण्डा हो जाता है.[NCERT corpus]
(defrule summer1
(declare (salience 100))
(id-root ?id summer)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 day|vacation|holiday)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garmI))
(assert (id-wsd_viBakwi ?id1 kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  summer.clp 	summer1   "  ?id "  garmI)" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  summer.clp 	summer1  "  ?id " kA)" crlf)
)
