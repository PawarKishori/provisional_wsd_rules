;@@@ Added by 14anu-ban-03 (25-03-2015)
;The findings of this research do not comport with accepted theory. [cald]
;इस शोध के निष्कर्ष स्वीकृत सिद्धांत के समान नहीं होते हैं . [manual]
(defrule comport0
(declare (salience 00))     
(id-root ?id comport)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comport.clp 	comport0   "  ?id "  samAna_ho )" crlf))
)


;@@@ Added by 14anu-ban-03 (25-03-2015)
;She comported herself with great dignity at her husband's funeral. [cald]
;उसने अपने पति के क्रिया कर्म में बडे बड़प्पन से स्वयं को सम्भाला .  [manual]
(defrule comport1
(declare (salience 10))     
(id-root ?id comport)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samBAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " comport.clp 	comport1   "  ?id "  samBAla )" crlf))
)
