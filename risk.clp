;@@@ Added by 14anu-ban-10 on (06-02-2015)
;Never take the risk of glissading if you cannot see the complete slope .[tourism corpus]
;यदि आप पूरी ढलान को देख नहीं पाते तो ग्लिसाडिंग करने का जोखिम कभी न उठाएँ ।[tourism corpus]
;There is no risk in descending in soft snow .[tourism corpus]
;नरम बर्फ़ में अवरोहण करने में किसी तरह का जोखिम नहीं होता ।[tourism corpus]
;Unless they are very high places , as the slopes of these places are extremely difficult and the risk of falling down is also very high .[tourism corpus]
;बहुत ऊँचे स्थानों की बात दूसरी है क्योंकि इन स्थानों पर ढलानें बहुत कठिन रहती हैं और नीचे गिरने का जोखिम भी बहुत अधिक होता है ।[tourism corpus]
(defrule risk2
(declare (salience 5100))
(id-root ?id risk)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ?id ? )(viSeRya-in_saMbanXI  ?id ? ))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joKima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  risk.clp 	risk2   "  ?id "  joKima)" crlf))
)

;@@@ Added by 14anu-ban-10 on (06-02-2015)
;Nevertheless the mountaineers should not take any kind of risk while descending .[tourism corpus]
;इसके बावजूद भी अवरोहण में पर्वतारोहियों को किसी प्रकार का जोखिम नहीं उठाना चाहिए ।[tourism corpus]
;May be compared to some other games mountaineering has little more risks  .[tourism corpus]
;शायद कुछ खेलों की अपेक्षा पर्वतारोहण में कुछ अधिक जोखिम हों ।[tourism corpus]
(defrule risk3
(declare (salience 5200))
(id-root ?id risk)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ? ?id )(viSeRya-viSeRaNa  ?id ? ))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joKima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  risk.clp 	risk3   "  ?id "  joKima)" crlf))
)

;@@@ Added by 14anu-ban-10 on (06-02-2015)
;There is no doubt in it that like other games risks are to be taken even during mountaineering  .[tourism corpus]
;इसमें कोई संदेह नहीं है कि अन्य खेलों की भाँति पर्वतारोहण के समय भी जोखिम उठाने पड़ते हैं ।[tourism corpus]
(defrule risk4
(declare (salience 5300))
(id-root ?id risk)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 take)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joKima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  risk.clp 	risk4   "  ?id "  joKima)" crlf))
)

;-------------------- Default Rules -----------------

;"risk","N","1.KZawarA"
;There is a risk of going out in the curfew.    
(defrule risk0
(declare (salience 5000))
(id-root ?id risk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KZawarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  risk.clp 	risk0   "  ?id "  KZawarA )" crlf))
)

;"risk","VT","1.joKima_uTAnA"
;Don't take any risk of her life,consult some good doctors.
(defrule risk1
(declare (salience 4900))
(id-root ?id risk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joKima_uTA))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  risk.clp 	risk1   "  ?id "  joKima_uTA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  risk.clp      risk1   "  ?id " kI )" crlf)
)
)

