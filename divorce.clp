
;Added by Meena(25.6.10)
;Why is there such a divorce between the arts and the sciences in this country's schools ?
(defrule divorce00
(declare (salience 5000))
(id-root ?id divorce)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-between_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pArWakya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divorce.clp   divorce00   "  ?id " pArWakya )" crlf))
)






;Added by Meena(24.6.10)
;How can you divorce the issues of environmental protection and overpopulation?
(defrule divorce1
(declare (salience 4900))
(id-root ?id divorce)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 issue)
(kriyA-object ?id ?id1)
(viSeRya-of_saMbanXI ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divorce.clp   divorce1   "  ?id "  alaga_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  divorce.clp      divorce1   "  ?id " ko )" crlf)
)
)





;Added by Meena(25.6.10)
;She is divorcing her husband.
(defrule divorce2
(declare (salience 4800))
(id-root ?id divorce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walAka_xe))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divorce.clp   divorce2   "  ?id "  walAka_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  divorce.clp      divorce2   "  ?id " ko )" crlf)
)
)

;$$$ Modified by 14anu-ban-04 (13-12-2014)   ;meaning changed from 'alaga_kara_sakawe' to 'alaga_kara'
;@@@ Added by 14anu11
;You cannot divorce science from social and political happenings and from the eccnomic structure of the world .
;आप विज्ञान को दुनिया की सामाजिक और राजनैतिक घटनाओं और उसके आर्थिक ढांचे से अलग नहीं कर सकते .
(defrule divorce4
(declare (salience 5000))
(id-root ?id divorce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str)                                  ;added by 14anu-ban-04 (13-12-2014)  
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))  ;added by 14anu-ban-04  (13-12-2014)     
(kriyA-subject  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divorce.clp   divorce4   "  ?id "  alaga_kara )" crlf)
)
)

;---------------------------- Default rules ------------------------
(defrule divorce0
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id divorce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walAka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divorce.clp 	divorce0   "  ?id "  walAka )" crlf))
)

;"divorce","N","1.walAka/vivAha_vicCexa"
;They decided to have a divorce.
;--"2.pqWakIkaraNa"
;


;Salience reduced by Meena(24.6.10)
;Meaning modified by Sheetal(21.6.10)
;When Mrs . Chitnis discovered that her husband was an adulterer she divorced him . 
(defrule divorce3
;(declare (salience 4900))
(declare (salience 0))
(id-root ?id divorce)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walAka_xe_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  divorce.clp 	divorce3   "  ?id "  walAka_xe_xe )" crlf))
)

;"divorce","VT","1.walAka_xenA/vivAha_vicCexa_karanA"
;The couple divorced after only 6 months
;--"2.pqWaka_karanA"

