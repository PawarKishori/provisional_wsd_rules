
;@@@Added by 14anu-ban-02(12-02-2015)
;You did a beautiful thing in helping those poor children.[cambridge]
;तुमने गरीब बच्चों कि सहायता करके अच्छा कार्य किया.[manual]
(defrule beautiful1 
(declare (salience 100)) 
(id-root ?id beautiful) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 thing) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id acCA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  beautiful.clp  beautiful1  "  ?id "  acCA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(12-02-2015)
;A beautiful piece of music.[cambridge]
;एक मधुर लय .[manual]
(defrule beautiful2 
(declare (salience 100)) 
(id-root ?id beautiful) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 music) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id maXura)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  beautiful.clp  beautiful2  "  ?id "  maXura )" crlf)) 
) 

;----------------------------- Default Rules -------------------

;@@@Added by 14anu-ban-02(12-02-2015)
;Sentence: A beautiful woman.[cambridge]
;Translation:   एक सुन्दर स्त्री . [anusaaraka]
(defrule beautiful0
(declare (salience 0))
(id-root ?id beautiful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sunxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  beautiful.clp  beautiful0  "  ?id "  sunxara )" crlf))
)

