

;@@@ Added by 14anu-ban-02(26-08-2014)
;The theorem may be stated as follows: The moment of inertia of a body about any axis is equal to the sum of the moment of inertia of the body about a parallel axis passing through its center of mass and the product of its mass and the square of the distance between the two parallel axes.
;प्रमेय का कथन इस प्रकार है: किसी पिण्ड का, किसी अक्ष के परितः जडत्व आघूर्ण, उस योग के बराबर है जो पिण्ड के द्रव्यमान केन्द्र से गुजरने वाली सामानान्तर अक्ष के परितः लिए गए जडत्व आघूर्ण और पिण्ड के द्रव्यमान तथा दोनों अक्षों के बीच की दूरी के वर्ग के गुणनफल को जोडने से प्राप्त होता है.

(defrule axis0
(declare (salience 0))
(id-root ?id axis)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id akRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  axis.clp 	axis0   "  ?id "  akRa )" crlf))
)


;@@@ Added by 14anu-ban-02(26-08-2014)
;Mars takes longer to revolve on its axis than the Earth.[oald]
;मंगल ग्रह अपनी धुरी पर घूमने में प्रथ्वी से अधिक समय लेता है.[manual]
(defrule axis1
(declare (salience 10))
(id-root ?id axis)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id1 ?id)
(id-word ?id1 revolve)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XurI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  axis.clp 	axis1   "  ?id " XuRI )" crlf))
)
