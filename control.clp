
;@@@ Added by 14anu13 on 19-06-14
;This emphasises the importance of controlling body weight from any early age .
;यह शुरुआत से ही शारीरिक वजन नियंत्रण पर जोर देता है | 
(defrule control03
(declare (salience 5000))
(id-root ?id control)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa 	?  ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyaMwraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  control.clp 	control03   "  ?id "  niyaMwraNa )" crlf))
)

;@@@ Added by 14anu-ban-03 (14-10-2014)
;Science is a systematic attempt to understand natural phenomena in as much detail and depth as possible, and use the knowledge so gained to predict, modify and control phenomena. [ncert]
;विज्ञान प्राकृतिक परिघटनाओं को यथासम्भव विस्तृत एवं गहनता से समझने के लिए किए जाने वाला सुव्यवस्थित प्रयास है, जिसमें इस प्रकार अर्जित ज्ञान का उपयोग परिघटनाओं के भविष्य कथन, संशोधन, एवं नियन्त्रण के लिए किया जाता है.[ncert]
(defrule control3
(declare (salience 4900))
(id-root ?id control)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kqxanwa_karma ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyaMwraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  control.clp 	control3   "  ?id "  niyaMwraNa_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (16-01-2015)
;Polaroids can be used to control the intensity, in sunglasses, windowpanes, etc.. [ncert]
;poleroYidoM ko XUpa ke caSmoM, KidakI ke SISoM Axi meM wIvrawA niyanwriwa karane meM upayoga kiyA jA sakawA hE. [ncert]
(defrule control4
(declare (salience 4900))
(id-root ?id control)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyanwriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  control.clp 	control4   "  ?id "  niyanwriwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (10-03-2015)
;A forest fire raging in southern California is defying all attempts to control it. (cald)
;दक्षिणी कैलिफोर्निया में  एक जंगल में लगी आग  तेज़ी से फैल  रह रही है  इसे क़ाबू में करने के लिए सभी प्रयास निष्फल हो रहे है . [manual]
(defrule control5
(declare (salience 4900))
(id-root ?id control)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id2 fire)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAbU_meM_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  control.clp 	control5  "  ?id "  kAbU_meM_kara )" crlf))
)


;---------------- Default Rules ---------------

(defrule control0
(declare (salience 5000))
(id-root ?id control)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id controlled )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id niyanwriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  control.clp  	control0   "  ?id "  niyanwriwa )" crlf))
)

;"controlled","Adj","1.niyanwriwa"
;He remained calm && controlled during judicial trial.
;
(defrule control1
(declare (salience 4900))
(id-root ?id control)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyaMwraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  control.clp 	control1   "  ?id "  niyaMwraNa )" crlf))
)

(defrule control2
(declare (salience 4800))
(id-root ?id control)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vaSa_meM_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  control.clp 	control2   "  ?id "  vaSa_meM_raKa )" crlf))
)

;"control","VT","1.vaSa_meM_raKanA"
;Control the budget
;Are you controlling for the temperature?
;Control an account
;Do you control these data?
;
;



