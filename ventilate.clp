;@@@Added by 14anu-ban-07,(27-03-2015)
;The bathroom is ventilated by means of an extractor fan.(oald)
;स्नानघर निष्कर्षक पंखा के द्वारा हवा किया गया है . (manual)
(defrule ventilate0
(id-root ?id ventilate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id havA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ventilate.clp 	ventilate0   "  ?id "  havA_kara )" crlf))
)

;@@@Added by 14anu-ban-07,(27-03-2015)
;She used the meeting to ventilate all her grievances. (cambridge)
;उसने उसके सभी मनोमालिन्य प्रकट करने के लिए बैठक का उपयोग किया . (manual)
(defrule ventilate1
(declare (salience 1000))
(id-root ?id ventilate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 grievance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakata_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ventilate.clp 	ventilate1   "  ?id "  prakata_kara )" crlf))
)
