;@@@ Added by Anita-30.6.2014
;The family now resides in southern France. [cambridge dictionary]
;परिवार अब दक्षिणी फ्रांस में रहता है ।
;He returned to Britain in 1939, having resided abroad for many years. [oxford learner's dictionary]
;वह कई वर्षों तक विदेश में रहन के बाद 1939 में ब्रिटेन में लौट आया ।
;Their passports do not give them the right to reside in Britain. [oxford learner's dictionary]
;उनके पासपोर्ट उन्हें ब्रिटेन में रहने का अधिकार नहीं देते ।
(defrule reside00
(declare (salience 100))
(id-root ?id reside)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-in_saMbanXI  ?id ?)(kriyA-for_saMbanXI  ?id ?sam))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reside.clp 	reside00   "  ?id "  raha )" crlf))
)

;@@@ Added by Anita-30.6.2014 
;The power to sack employees resides in the Board of Directors. [cambridge dictionary]
;कर्मचारियों को बर्खास्त करने की शक्ति निदेशक मंडल में होती है ।
(defrule reside0
(declare (salience 500))
(id-root ?id reside)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 power)
(kriyA-subject  ?id ?id1)
(kriyA-in_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reside.clp 	reside0   "  ?id "  ho )" crlf))
)

;@@@ Added by Anita-1.7.2014 
;The ultimate authority resides with the board of directors. [oxford learner's dictionary]
;अंतिम प्राधिकारी निदेशक मंडल के पास होता है. ।
(defrule reside1
(declare (salience 1000))
(id-root ?id reside)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reside.clp 	reside1   "  ?id "  ho )" crlf))
)


;@@@ Added by 14anu-ban-02 (30.07.14)
;This is mainly the residing place of resident and migratory words .[tourism corpora]
;यह  मुख्य  रूप  से  स्थाई  और  प्रव्राजक  पक्षियों  का  वासस्थल  है  । [tourism corpora]
;The location of the national parks , forest reserves , tiger reserves and other protected areas in our country is in different views , residing places and climate areas .[tourism corpora]  
;हमारे  देश  के  राष्ट्रीय  उद्यानों  ,  वनारण्यों  ,  बाघ  रिजर्वों  व  अन्य  सुरक्षित  क्षेत्रों  की  अवस्थिति  अलग  -  अलग  प्रदृश्यों  ,  वास  -  स्थलों  और  जलवायु  क्षेत्रों  में  है  ।.[tourism corpora]
;Middle Button Island National Park is the residing place of different types of birds like sea eagle , pigeon , cheti etc  .[tourism corpora]
;समुद्री  चील  ,  कबूतर  ,  चेती  आदि  विभिन्न  प्रकार  के  पक्षियों  का  मिडिल  बटन  द्वीप  राष्ट्रीय  उद्यान  वास-स्थल  है । [tourism corpora]
(defrule reside2
(declare (salience 4000))
(id-root ?id reside)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) place)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAsasWala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reside.clp 	reside2   "  ?id "  vAsasWala )" crlf))
)

;@@@ Added by 14anu-ban-02 (30.07.14)
;Only some special type of fauna able to bear the cold can reside in Hemis High National Park  .[tourism corpora]
;हेमिस  हाड़  राष्ट्रीय  उद्यान  में  शीत  को  सहन  करने  वाले  कुछ  विशेष  प्रकार  के  पशु  -  पक्षी  ही  रह   सकते  हैं  ।[tourism corpora]
;Two rare animals like drudhlomi rabbit and pigmy boar  reside in the Manas National Park  .[tourism corpora]
;मानस  राष्ट्रीय  उद्यान  में  दृढ़लोमी  खरगोश  और  बौना  सुअर  दो  दुर्लभ  पशु  रहते  हैं  ।[tourism corpora]
(defrule reside3
(id-root ?id reside)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reside.clp 	reside3   "  ?id "  raha )" crlf))
)
