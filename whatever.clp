;@@@ Added by Anita--30-7-2014
;We'll go whatever the weather . [cambridge dictionary]
;मौसम जैसा भी हो हम जाएंगे ।
(defrule whatever0
(declare (salience 5000))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jEsA_BI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever0   "  ?id "  jEsA_BI_ho )" crlf))
)

;@@@ Added by Anita--30-7-2014
;Whatever happens , you know that I'll stand by you. [cambridge dictionary]
;आपको पता है कि कुछ भी हो , मैं आपके साथ हूँ ।
(defrule whatever1
(declare (salience 4900))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?kri ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever1   "  ?id "  kuCa_BI )" crlf))
)

;@@@ Added by Anita--30-7-2014
;Whatever the outcome of the war, there will be no winners. [cambridge dictionary]
;युद्ध का जो भी परिणाम हो,कोई भी विजेता नहीं होगा ।
;Whatever the reason, more Britons are emigrating to Australia today than at any time since the 1950s.
;कारण जो भी हो , 1950 के दशक के बाद से अन्य किसी भी समय की तुलना में बिटेन के अधिक लोग आज ऑस्ट्रेलिया के लिए देशान्तरगमन कर  रहे हैं ।
(defrule whatever2
(declare (salience 4800))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo_BI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever2   "  ?id "  jo_BI_ho )" crlf))
)


;@@@ Added by Anita--30-7-2014
;Whatever else may be said of him, Mr Meese is not scared of a fight. [cambridge dictionary]
;उनके बारे में जो कुछ भी कहा जाए, श्री मीस को लड़ाई का डर नहीं है ।
(defrule whatever3
(declare (salience 5100))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?kri ?id)
(kriyA-of_saMbanXI  ?kri ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo_kuCa_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever3   "  ?id "  jo_kuCa_BI )" crlf))
)

;@@@ Added by Anita--30-7-2014
;Whatever is he doing with that rod ? [cambridge dictionary]
;वह डंडे के साथ क्या कर रहा है ? 
;Whatever is that yellow thing on your plate? [cambridge dictionary]
;आपकी प्लेट में वह पीली चीज क्या है?
(defrule whatever4
(declare (salience 5200))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
(praSnAwmaka_vAkya)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever4   "  ?id "  kyA )" crlf))
)

;@@@ Added by Anita--31-7-2014
;Whatever does she see in him - he's revolting ! [cambridge dictionary]
;उसने उस में जो कुछ भी देखा है - वह विद्रोही है ।
(defrule whatever5
(declare (salience 5300))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 see)
(and(kriyA-subject  ?k ?id)(kriyA-vAkyakarma  ?k ?id1))
;(id-cat_coarse  ?id  determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo_kuCa_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever5   "  ?id "  jo_kuCa_BI )" crlf))
)

;@@@ Added by Anita--31-7-2014
;Whatever made him buy that jacket? [cambridge dictionary]
;उसने वह जैकेट क्यों खरीदी ?
(defrule whatever6
(declare (salience 5400))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 buy|laugh)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyoM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever6   "  ?id "  kyoM )" crlf))
)


;@@@ Added by Anita--01-08-2014
;Take whatever action is needed. [oxford learner's dictionary]
;जो भी कार्यवाही की जरूरत हो , कीजिए ।
;Do whatever you like. [oxford learner's dictionary]
;आप जो भी चाहें कीजिए ।
;You have our support, whatever you decide. [oxford learner's dictionary]
;आप जो भी निश्चय लें आपको हमारा समर्थन है  ।
(defrule whatever8
(declare (salience 5600))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 take|decide|do)
(kriyA-object  ?kri ?id)
(kriyA-vAkyakarma  ?id1 ?kri)
;(id-cat_coarse  ?id  determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever8   "  ?id "  jo_BI )" crlf))
)

;##########################################default-rule################################
;@@@ Added by Anita--01-08-2014
;I will give you whatever money you require . [Advanced Learner's English-Hindi Dictionary-Dr. Hardev Bahri}
; आपको जितने भी पैसे की आवश्यकता है मैंआपको दूँगा ।
(defrule whatever_default-rule
(declare (salience 0))
(id-root ?id whatever)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jiwanA_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whatever.clp 	whatever_default-rule   "  ?id "  jiwanA_BI )" crlf))
)
