;@@@ Added by 14anu-ban-10 on (24-03-2015)
;I don’t know what is going on—it’s a rum business.[oald]
;मैं नहीं जानता कि  क्या चाल रहा है - यह एक अजीब काम  है . [manual]
(defrule rum1
(declare (salience 400))
(id-root ?id rum)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ajIba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rum.clp 	rum1   "  ?id "  ajIba )" crlf))
)

;@@@ Added by 14anu-ban-10 on (24-03-2015)
;He’s a rum fellow.[oald]
;वह एक भद्दा व्यक्ति है.[manual]
(defrule rum2
(declare (salience 500))
(id-root ?id rum)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 fellow)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  BaxxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rum.clp 	rum2   "  ?id "   BaxxA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (24-03-2015)
;A rum and Coke, please.[oald]
;एक शराब  और  कोक  ,कृपया.[manual]
(defrule rum0
(declare (salience 200))
(id-root ?id rum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rum.clp      rum0   "  ?id "  SarAba )" crlf))
)

