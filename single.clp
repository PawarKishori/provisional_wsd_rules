
(defrule single0
(declare (salience 5000))
(id-root ?id single)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) a )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_hI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  single.clp 	single0   "  ?id "  eka_hI )" crlf))
)

(defrule single1
(declare (salience 4900))
(id-root ?id single)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  single.clp 	single1   "  ?id "  eka )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (03-01-2015):changed 'eka' to 'eka_hI'
;$$$ Modified by jagriti(3.4.2014)..eka_hI instead of ekamAwra
;At a single Halloween party one can see many weird faces.[news-dev]
;इस तरह एक ही हैलोवीन पार्टी में आपको ढेरों अजीबोगरीब चेहरे दिख जाएंगे।
;$$$ Modified by 14anu07 on 28/06/2014
(defrule single2
(declare (salience 4800))
(id-root ?id single)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_hI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  single.clp 	single2   "  ?id "  eka_hI )" crlf))
)

;Single3 and single4 merged by  14anu-ban-01 on (03-01-2015)
;@@@ Added by 14anu20 neha priti MNNIT ALLD. and 14anu7
;I am still single.
;मैं अभी भी अविवाहित हूँ .
(defrule single3
(declare (salience 5500))
(id-root ?id single)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avivAhiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  single.clp 	single3   "  ?id "  avivAhiwa )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (03-01-2015)
;Example added by 14anu-ban-01 on (03-01-2015)
;I think that there's not a single soul that didn't love her.[COCA]
;मेरे ख्याल से ऐसा एक भी  व्यक्ति नहीं होगा जो उसे प्यार ना करता हो.[self]
;@@@Added by 14anu07 on 28/06/2014
(defrule single5
(declare (salience 4900))
(id-root ?id single)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(niReXawmaka_vAkya)		 ;added by 14anu-ban-01 on (03-01-2015)
;(kriyA-kriyA_niReXaka ?id1 ?id2);commented by 14anu-ban-01 on (03-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  single.clp 	single5   "  ?id "  eka_BI )" crlf))
)

;Combined with single3
;@@@Added by 14anu7 on 28/06/2014
;(defrule single4
;(declare (salience 5300))
;(id-root ?id single)
;?mng <-(meaning_to_be_decided ?id)
;(subject-subject_samAnAXikaraNa ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id avivAhiwa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  single.clp 	single4   "  ?id "  avivAhiwa )" crlf))
;)

;Commented by 14anu-ban-01 on (03-01-2015) as 'single-minded' already exists in dictionary.
;@@@ Added by 14anu11
;Sometimes he is the single - minded revolutionary going like the arrow to his goal and shaking up millions in the process .
;कभी वे तीर की तरह अपने मकसद की ओर जाने वाले एकनिष्ठ क्रांतिकारी दिखाई देते हैं और इस प्रक्रिया में लाखों लोगों को झकझोरते चलते हैं .
;(defrule single6
;(declare (salience 5000))
;(id-root ?id single)
;(id-word =(+ ?id 1) -)
;(id-word ?id1 minded)
;(id-word =(+ ?id 1) minded)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ekaniRTa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng    " ?*prov_dir* "  single.clp 	single6   "  ?id "  "?id1"  ekaniRTa )" crlf))
;)



;default_sense && category=adjective	eka/eka_hI/ekamAwra	0
;"single","Adj","1.eka/eka hI/ekamAwra"
;Please give me a single piece of paper.
;
;
