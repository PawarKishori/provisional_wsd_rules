
(defrule whirl0
(declare (salience 5000))
(id-root ?id whirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whirl.clp 	whirl0   "  ?id "  cakkara )" crlf))
)

;"whirl","N","1.cakkara"
;The dancers went round in a whirl of color.
;--"2.GatanAoM_kA_wAzwA{jalxI_jalxI_hone_vAlI}"
;They had an endless whirl of activities.


;$$$ Modified by sudhir wsd participant [16-07-2014]
;Tom whirled her across the dance floor.   ;[Oald]
;toma ne usako nqtya ParSa ke saBI ora GUmAyA.   ;[manual]
;meaning changed from "cakkara_KA" to "GUmA"
(defrule whirl1
(declare (salience 100))
(id-root ?id whirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUmA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whirl.clp 	whirl1   "  ?id "  GUmA )" crlf))
)

;"whirl","VTI","1.cakkara_KAnA"
;The merry go round whirled suddenly.
;

;$$$ Modified by sudhir wsd participant [16-07-2014]
;modified "kriyA-into_saMbanXI" as "kriyA-subject" 
;removed air|wind and added paper|leaf
;@@@ Added by Pramila(Banasthali University) on 12-03-2014
;A paper whirled into the air.   ;shiksharthi
;एक कागज हवा में उड़ गया.
(defrule whirl2
(declare (salience 5500))
(id-root ?id whirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 paper|leaf)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id udZa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whirl.clp 	whirl2   "  ?id "  udZa_jA )" crlf))
)

;$$$ Modified by sudhir wsd participant [16-07-2014]
;modified "kriyA-subject" as "kriyA-object"
;removed wind|air and added cap|paper|leaf
;@@@ Added by Pramila(Banasthali University) on 12-03-2014
;The wind whirled my cap.   ;shiksharthi
;हवा मेरी टोपी उड़ा ले गई.
(defrule whirl3
(declare (salience 4900))
(id-root ?id whirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 cap|paper|leaf)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id udzA_le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  whirl.clp 	whirl3   "  ?id "  udzA_le_jA )" crlf))
)


;@@@ Added by 14anu-ban-11 (28-03-2015)
;They had an endless whirl of activities.(hinkhoj)
;उनके अनन्त घटनाओं का ताँता  . (self)
(defrule whirl4
(declare (salience 5001))
(id-root ?id whirl)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 activity)
;(id-root ?id2 of)        ;Commented by 14anu-ban-11 on (10-04-2015)
(pada_info (group_head_id ?id1)(preposition ?id2))      ; Added by 14anu-ban-11 on (10-04-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id2 ?id1 GatanAoM_kA_wAzwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " whirl.clp  whirl4  "  ?id "  "?id2 "   " ?id1 "   GatanAoM_kA_wAzwA)" crlf))
)

