
(defrule pencil0
(declare (salience 5000))
(id-root ?id pencil)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peMsila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pencil.clp 	pencil0   "  ?id "  peMsila )" crlf))
)

;"pencil","N","1.peMsila"
;Have you sharpened your pencil.
;
;$$$ Modified by 14anu09 [26-06-14]
;To pencil is a tough job.
;चित्रित करना एक मुश्किल काम है . 
(defrule pencil1
(declare (salience 4900))
(id-root ?id pencil)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 conversation|talk|discussion);###I want you to pencil the conversation.
(not(kriyA-object ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pencil.clp 	pencil1   "  ?id "  ciwriwa_kara )" crlf))
)

;"pencil","V","1.ciwriwa_karanA"
;She penciled a beautiful room.
;

;$$$ Modified by 14anu-ban-09 on (26-11-2014)
;@@@ Added by 14anu09[26-06-14]
;pencil in the talk.
;बात-चीत को लेखांकित करो.
(defrule pencil2
(declare (salience 5000))
(id-root ?id pencil)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) in)
;(id-word ?id1 in) ;commented by 14anu-ban-09 on (26-11-2014)
(id-cat_coarse ?id verb) ;uncommented by 14anu-ban-09 on (26-11-2014)
=>
(retract ?mng) 
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) leKAMkiwa_kara)) ;changed '?id1' to '(+ ?id 1)' by 14anu-ban-09 on (26-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pencil.clp	pencil2  "  ?id "  " (+ ?id 1) "  leKAMkiwa_kara  )" crlf)) ;changed '?id1' to '(+ ?id 1)' by 14anu-ban-09 on (26-11-2014)
)

;@@@ Added by 14anu09 [26-06-14]
;His mustache was pencil thin.
;उसकी नाक बहुत पतली है . 
(defrule pencil3
(declare (salience 4900))
(id-root ?id pencil)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka ?id1 ?id)
(id-word ?id1 thin)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pencil.clp 	pencil3   "  ?id "  batuwa )" crlf))
)


;@@@ Added by 14anu09 [26-06-14] 
;I want you to pencil the conversation.
;मैं चाहता हूँ कि आप वार्तालाप लेखाङ्कित करें . 
(defrule pencil4
(declare (salience 5000))
(id-root ?id pencil)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 conversation|talk|discussion)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id leKAMkiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pencil.clp 	pencil4   "  ?id "  leKAMkiwa_kara )" crlf))
)

