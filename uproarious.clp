;@@@Added by 14anu-ban-07,(07-02-2015)
;An uproarious party.(oald)
;एक कोलाहलपूर्ण पार्टी . (self)
(defrule uproarious0
(id-root ?id uproarious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kolAhalapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  uproarious.clp 	uproarious0   "  ?id "  kolAhalapUrNa )" crlf))
)

;@@@Added by 14anu-ban-07,(07-02-2015)
;An uproarious story.(oald)
;एक हास्यकर कहानी .(self)
(defrule uproarious1
(declare (salience 1000))
(id-root ?id uproarious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 story|act|play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAsyakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  uproarious.clp 	uproarious1   "  ?id "  hAsyakara )" crlf))
)

