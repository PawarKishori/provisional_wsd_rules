;@@@ Added by 14anu-ban-02 (13-11-2014)
;Sentence: They tell us to have a bath . [karan singla]
;Translation:वो हमे स्नान करने को कहते है . [karan singla]
(defrule bath0 
(declare (salience 0)) 
(id-root ?id bath) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id snAna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bath.clp  bath0  "  ?id "  snAna )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (13-11-2014)
;Sentence: They succeeded in making a textile fiber of spun soy protein fibers, hardened or tanned in a formaldehyde bath, which was given the name Azlon.[agriculture]
;Translation: वे एक कपड़े का फाइबर बनाने में सफल रहे  जो काता सोया प्रोटीन फाइबर का था,जिसे formaldehyde घोल   से  कड़ा और धूप से तप्त बनाया था , जिसका नाम  आज़लों  दिया गया  था.[manual]
(defrule bath1
(declare (salience 100)) 
(id-root ?id bath) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id Gola)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bath.clp  bath1  "  ?id "  Gola )" crlf)) 
) 
