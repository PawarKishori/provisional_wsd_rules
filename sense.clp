
(defrule sense0
;(declare (salience 5000)); commented by 14anu-ban-01 .
(id-root ?id sense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMvexanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sense.clp 	sense0   "  ?id "  saMvexanA )" crlf))
)

(defrule sense1
(declare (salience 4900))
(id-root ?id sense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sense.clp 	sense1   "  ?id "  anuBava_kara )" crlf))
)
;@@@ Added by jagriti(12.12.2013)
;In these ashrams one has a sense of homecoming.[gyanidhi-corpus]
;इन आश्रमों में एक अपने पन का अनुभव होता हैं।
(defrule sense2
(declare (salience 5500))
(id-root ?id sense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 homecoming|hot|cold)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sense.clp 	sense2   "  ?id "  anuBava )" crlf))
)

;"sense","V","1.anuBava karanA"
;Sensing his importance, Rakesh started buttering him.
;--"2.mehasUsa karanA"
;An apparatus that senses the presence of Toxic gases.
;
;


;@@@ Added by 14anu24
;In a broad sense , agriculture includes cultivation of the soil and growing and harvesting crops and breeding and raising livestock and dairying and forestry .
;एक व्यापक अर्थ  में कृषि में भूमि की जुताई , फसलों की रुपाई और कटाई , पशु -निकास , परिरक्षण , और स्वच्छता अभियांत्रिकी में से प्रत्येक सफल खेती के लिए महत्वपूर्ण है .
(defrule sense3
(declare (salience 5800))
(id-root ?id sense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 broad)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sense.clp    sense3   "  ?id "  arWa )" crlf))
)

;@@@ Added by 14anu26     [30-06-14]
;It was in a sense , Pakistan's truth commission . 
;वह एक एक तरह से से, पाकिस्तान का सच आयोग था . 
(defrule sense4
(declare (salience 5000))  ;salience reduced to 5000 from 6000 by 14anu-ban-01 on (03-02-2015) because this was interfering in rule sense3
(id-root ?id sense)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  waraha ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sense.clp 	sense4   "  ?id "  waraha )" crlf))
)

;@@@ Added by 14anu-ban-01 on (07-10-2014)
;A better sense of how to evaluate information sources.[wikipedia-wiki]
;ज्ञान सूत्रों का मूल्यांकन कैसे किया जाये,इसकी  बेहतर समझ.[self]
;A better sense of evaluation of information sources.[self]
;ज्ञान सूत्रों का मूल्यांकन करने की बेहतर समझ.[self]
(defrule sense5
(declare (salience 3000))
(id-root ?id sense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 evaluate|evaluation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sense.clp 	sense5   "  ?id "  samaJa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (21-10-2014)
;However, this temperature sense is somewhat unreliable and its range is too limited to be useful for scientific purposes.[NCERT corpus]
;परन्तु यह ताप बोध कुछ-कुछ अविश्वसनीय होता है तथा इसका परिसर इतना सीमित है कि किसी वैज्ञानिक कार्यों के लिए इसका कोई उपयोग नहीं किया जा सकता.[NCERT corpus]
(defrule sense6
(declare (salience 1000))
(id-root ?id sense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 temperature|pressure|volume)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id boXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sense.clp    sense6   "  ?id "  boXa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (16-03-2015)
;Dogs have a very good sense of smell.[cald]
;कुत्तों में गन्ध की बहुत अच्छी पहचान होती है .  [self] 
(defrule sense7
(declare (salience 1000))
(id-root ?id sense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 smell)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAna ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sense.clp 	sense7   "  ?id "  pahacAna )" crlf))
)


