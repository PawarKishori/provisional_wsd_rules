;@@@ Added by 14anu-ban-02(26-09-2014)
;Sentence: In this way while listening to the broadly said things on knowledge , power and duties from Krishna , Arjun once again did acoustic for the war .[karan singla]
;Translation: इस प्रकार श्रीकृष्ण के ज्ञानयोग भक्तियोग एवं कर्मयोग के बारे में विस्तार से कहने पर अर्जुन ने फिर से रथारूढ़ हो युद्ध के लिये शंखध्वनि की ।[karan singla]
(defrule broadly1
(declare (salience 100)) 
(id-root ?id broadly) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id1 ?id)
(id-root ?id1 listen) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id viswAra_se)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  broadly.clp  broadly1  "  ?id "  viswAra_se )" crlf)) 
) 

;@@@ Added by 14anu-ban-02(26-09-2014)
;In general, the errors in measurement can be broadly classified as (a) systematic errors and (b) random errors.[ncert] 
;सामान्यतः, मापन में आई त्रुटियों को मोटे तौर पर निम्नलिखित दो श्रेणियों में वर्गीकृत किया जा सकता है (a) क्रमबद्ध त्रुटियाँ एवं (b) यादृच्छिक त्रुटियाँ.[ncert]
(defrule broadly0
(declare(salience -1))
(id-root ?id broadly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mote_wOra_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  broadly.clp 	broadly0   "  ?id "  mote_wOra_para )" crlf))
)
