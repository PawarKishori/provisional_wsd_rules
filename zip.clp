;$$$ Modified by 14anu-ban-03 (30-07-2014)
; Meaning changed from jZipa to cena
;The zip of her purse is stuck.
;usake batue ki cena ataka gaI hE.
(defrule zip0
(declare (salience 5000))
(id-root ?id zip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cena))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  zip.clp 	zip0   "  ?id "  cena )" crlf))
)

;"zip","N","1.jZipa{parsa/pawalUna_Axi_baMxa_karane_kelie_jaMjIra_jEsA_upakaraNa}"
;--"2.sanasanI_AvAjZa"
;The zip of the jet frightened the child.
;
(defrule zip1
(declare (salience 4900))
(id-root ?id zip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZipa_KolanA_yA_baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  zip.clp 	zip1   "  ?id "  jZipa_KolanA_yA_baMxa_kara )" crlf))
)

;"zip","V","1.jZipa_KolanA_yA_baMxa_karanA"
;She zipped open her bag.
;--"2.bahuwa_wejZI_se_jAnA"
;The jet zipped across the sky.
;


;@@@ Added by 14anu-ban-11 on (28-03-2015)
;The jet zipped across the sky.(hinkhoj)
;जेट विमान आसमान में बहुत तेजी से गया . (self)
(defrule zip2
(declare (salience 4900))
(id-root ?id zip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(kriyA-across_saMbanXI  ?id ?id2)
(id-root ?id1 jet)
(id-root ?id2 sky)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_wejZI_se_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  zip.clp 	zip2   "  ?id "  bahuwa_wejZI_se_jA)" crlf))
)



;@@@ Added by 14anu-ban-11 on (28-03-2015)
;The zip of the jet frightened the child.(hinkhoj)
;जेट विमान की सनसनी आवाज ने बच्चे को डराया . (self)
(defrule zip3
(declare (salience 5001))
(id-root ?id zip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 jet)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanasanI_AvAjZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  zip.clp 	zip3   "  ?id "  sanasanI_AvAjZa)" crlf))
)




