
;@@@ Added by 14anu-ban-10 on (17-02-2015)
;Rasp off the rough edges of the table top.[hinkhoj]
;तालिका के शीर्ष के किसी न किसी किनारों से रगड़कर साफ करना ।[manual]
(defrule rasp3
(declare (salience 5200))
(id-root ?id rasp)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ragadakara_sAPa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rasp.clp 	rasp3   "  ?id "  ragadakara_sAPa_kara )" crlf))
)

;-------------------------- Default Rules --------------------
;"rasp","N","1.kirakirAne_kI_AvAja"
;A rasp of the saw on the log can be heard from the distance. 
(defrule rasp0
(declare (salience 5000))
(id-root ?id rasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kirakirAne_kI_AvAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rasp.clp 	rasp0   "  ?id "  kirakirAne_kI_AvAja )" crlf))
)

;"rasp","V","1.karkaSa_AvAja"
;Don't make a noise.the teacher rasped. 
(defrule rasp1
(declare (salience 4900))
(id-root ?id rasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karkaSa_AvAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rasp.clp 	rasp1   "  ?id "  karkaSa_AvAja )" crlf))
)



;"rasp","V","1.karkaSa_AvAja"
;Don't make a noise.the teacher rasped.  
;--"2.ragadZanA"
;The glacier rasps the valley floor.  
;"rasp","N","1.kirakirAne_kI_AvAja"
;A rasp of the saw on the log can be heard from the distance.  
;--"2.kAta_CAzta_kara_rewI_se_cikanA_karanA"
;Rasp off the rough edges of the table top.  
