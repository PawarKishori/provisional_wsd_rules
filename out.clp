
(defrule out0
(declare (salience 5000))
(id-root ?id out)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id outing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  out.clp  	out0   "  ?id "  sEra )" crlf))
)

(defrule out1
(declare (salience 4900))
(id-root ?id out)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) of)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out1   "  ?id "  - )" crlf))
)

;"outing","N","1.sEra"
;The family outing was a pleasure.
;--"2.BAga lenA"
;The horse finished second on his last outing.
;
(defrule out2
(declare (salience 4800))
(id-root ?id out)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out2   "  ?id "  bAhara )" crlf))
)

;"out","Adv","1.bAhara"
;They went out last night.
;Ram went to see his friend but he was out.
;The truth is out finally.
;--"2.KZawama/aMwa"
;All the lights were out in the streets.
;--"3.behoSa"
;He was out for more than an hour before the nurses could bring him round.
;
(defrule out3
(declare (salience 4700))
(id-root ?id out)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAhara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out3   "  ?id "  ke_bAhara )" crlf))
)

;"out","Prep","1.ke_bAhara"
;He walked out of the building with a man in blue coat.
;
(defrule out4
(declare (salience 4600))
(id-root ?id out)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhara_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out4   "  ?id "  bAhara_nikAla )" crlf))
)

;"out","VT","1.bAhara_nikAlanA"
;This actor was outed last week.
;
(defrule out5
(declare (salience 4500))
(id-root ?id out)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out5   "  ?id "  bAhara )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 21.06.14
;He was out for a duck.
;वह शून्य पर अाउट हुअा.
(defrule out06
(declare (salience 5000))
(id-root ?id out)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id2 ?id1)
(kriyA-kriyA_viSeRaNa  ?id2 ?id)
(id-cat_coarse ?id2 verb)
(id-root ?id1 duck)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Auta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out06   "  ?id "  Auta )" crlf))
)

;Commented by 14anu-ban-09 on (15-12-2014)
;NOTE-As this case is already handling in out8.
;@@@ Added by 14anu06 on 13/6/2014***********
;The fire has gone out.
; आग बुझ गयी है.
;(defrule out6
;(declare (salience 5500))
;(id-root ?id out)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-upasarga ?k ?id)
;(kriyA-subject  ?k ?id1)
;(id-root ?id1 fire|bitter|hate)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id buJA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out6   "  ?id "  buJA )" crlf))
;)

;Commented by 14anu-ban-09 on (15-12-2014)
;@@@ Added by 14anu06 on 13/6/2014***********
;NOTE- Is "out surface" a compound?
;The out surface of the ship was blue.
;जहाज की बाहरी सतह नीली थी . 
;(defrule out7
;(declare (salience 4900))
;(id-root ?id out)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaNa ? ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id bAharI))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out7   "  ?id "  bAharI )" crlf))
;)

;@@@ Added by 14anu01
;My labour has panned out
;मेरा श्रम सफल हुआ है . 
(defrule out8
(declare (salience 5500))
(id-word ?id out)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id particle)
(id-cat_coarse =(- ?id 1) verb)
(kriyA-upasarga  =(- ?id 1) ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  out.clp  	out8   "  ?id "  - )" crlf))
)

;Commented by 14anu-ban-09 on (15-12-2014)
;NOTE-As this case is already handling in out8.
;@@@ Added by 14anu24
;If you have a complaint about utilities ( gas , water , electricity , or telephones ) , try first to sort out the problem with the company .
;अगर आपको यूटिलीटीज ( गैस , पानी , बिजली या टेलिफिन ) के बारे में कोई शिकायत है , तो पहले उपयुक्त कंपनी से समस्या सुलझाने की कोशिश कीजिए .
;(defrule out9
;(declare (salience 5000))
;(id-root ?id out)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(- ?id 1) sort)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id -))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  out.clp 	out9   "  ?id "  - )" crlf))
;)

;@@@ Added by 14anu-ban-09 on (20-02-2015)
;She plucked out a grey hair.	[oald]
;उसने एक भूरा बाल खिच कर निकाला.	[self]
(defrule out9
(declare (salience 5500))
(id-word ?id out)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(id-cat_coarse ?id preposition)
(kriyA-out_saMbanXI  ? ?id1)
(id-root ?id1 hair|eyebrow|thorn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  out.clp  	out9   "  ?id "  nikAlA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (27-02-2015)
;There was a substantial drop in the number of people out of work last month.	[oald]
;पिछले महीने बेरोजगार लोगों की संख्या  में  एक अच्छी गिरावट आई  थी .				[self]

(defrule out10
(declare (salience 4900))
(id-root ?id out)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) of)
(id-word =(+ ?id 2) work)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) berojagAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " out.clp  out10  "  ?id "  " (+ ?id 1) " " (+ ?id 2) " berojagAra  )" crlf))
)
