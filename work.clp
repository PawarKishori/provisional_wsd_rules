;@@@ Added by 14anu03 on 25-june-2014
;The machine is now in working order.
;मशीन अब काम करने की स्थिती में है . 
(defrule work100
(declare (salience 5500))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 order)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAma_karane_kI_sWiwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  work.clp     work100   "  ?id "  " ?id1 "  kAma_karane_kI_sWiwI )" crlf))
)

(defrule work0
(declare (salience 5000))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 hard)
(kriyA-hard_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadZI_mehanawa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " work.clp work0 " ?id "  kadZI_mehanawa_kara )" crlf)) 
)

(defrule work1
(declare (salience 4900))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 hard)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kadZI_mehanawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " work.clp	work1  "  ?id "  " ?id1 "  kadZI_mehanawa_kara  )" crlf))
)

;added by human being
(defrule work2
(declare (salience 4800))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 hard)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariSrama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  work.clp 	work2   "  ?id "  pariSrama_kara )" crlf))
)

; He was working hard.
(defrule work3
(declare (salience 4700))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  work.clp 	work3   "  ?id "  kAma_kara )" crlf))
)

;$$$ Modified by 14anu-ban-11 on (10-12-2014)
; Yes , but not until she has had chance to work it out or read to the end of the sentence 
;जी हाऋ , परंतु तब तक नहीं जब तक कि उसे यत्न करके समाधान निकालने का अवसर न मिल जाए अथवा वह उस पंइ &gt; को आखिर तक न पढऋ ले .
;जी हाँ, परंतु तब तक नहीं जब तक कि उसे यत्न करके समाधान निकालने का अवसर न मिल जाए अथवा वह उस वाक्य को आखिर तक न पढे ले . --- added by 14anu-ban-11
;@@@ Added by avni(14anu11)
(defrule work9
(declare (salience 5010))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) it)
(id-root =(+ ?id 2) out) ; Modified  =(+ ?id 1) as  =(+ ?id 2) by 14anu-ban-11 on (10-12-2014)
;(id-root ?id1 it)                   ; Commented by 14anu-ban-11 on (10-12-2014)
;(id-root ?id2 out)               ; Commented by 14anu-ban-11 on (10-12-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) samAXAna))   ; corrected meaning 'smADAn' as 'samAXAna' by 14anu-ban-11 on (10-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  work.clp 	work9   "  ?id " "(+ ?id 1)" "(+ ?id 2)" samAXAna )" crlf)) 
)


(defrule work4
(declare (salience 4600))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kArya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  work.clp 	work4   "  ?id "  kArya )" crlf))
)

;default_sense && category=verb	kAma_kara	0
;"work","VT","1.kAma_karanA"
;The labourer worked in the quarry.
;
;

;Commented by 14anu-ban-11 on (10-12-2014)
;Note:- Rule is already exits of gym.
;$$$ Modified by 14anu22
;(defrule work5
;(declare (salience 4600))
;(id-root ?id work)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id gym)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kAma_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  work.clp    work5   "  ?id "  kAma_kara )" crlf))
;)
; I have started working  ----  Added by manju



;@@@ Added by Neha Arya from Gurukul on 11-07-2014 on WSD workshop.
;I have never been able to work her out.
;mE use kaBI nahIM samaJa pAyA hUz.
(defrule work6
(declare (salience 5100))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?object)
(id-root ?object ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " work.clp	work6  "  ?id "  " ?id1 "  samaJa  )" crlf))
)

;$$$ Modified by 14anu-ban-11 on (10-12-2014)
;@@@ added by 14anu22
;I was working out in the gym
;मैं जिम मे व्यायाम कर रहा था.
(defrule work8
(declare (salience 4600))
(id-root ?id work)
(id-word ?id1 out)
(kriyA-upasarga  ?id ?id1)
(kriyA-in_saMbanXI  ?id ?id2);Added by 14anu-ban-11 on (10-12-2014)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) ; Modified category noun as verb by 14anu-ban-11 on (10-12-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) vyAyAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  work.clp work8 " ?id " " (+ ?id 1) "  vyAyAma_kara)" crlf)))

;@@@ added by 14anu22
;We have to work this out.
;हमें ये हल करना है.
(defrule work7
(declare (salience 5000))
(id-root ?id work)
(id-word ?id1 out)
(kriyA-upasarga  ?id ?id1)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) hala_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  work.clp work7 " ?id " " (+ ?id 1) "  hala_kara)" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-04-2015)
;Police work is mainly routine.(oald)
;पुलिस नौकरी प्रमुख रूप से  नित्य है . (self)
(defrule work25
(declare (salience 4601))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 police)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nOkarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  work.clp 	work25   "  ?id "  nOkarI)" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-04-2015)
;Can you work it so that we get free tickets? (oald)
;क्या आप यह प्रयास करेगे जिससे कि हम मुक्त टिकट प्राप्त कर सके? (self)
(defrule work26
(declare (salience 4701))
(id-root ?id work)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkya_viSeRaNa  ?id ?id1)
(kriyA-object  ?id1 ?id2)
(id-root ?id1 get)
(id-root ?id2 ticket)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayAsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  work.clp 	work26   "  ?id "  prayAsa_kara)" crlf))
)


