;@@@ Added by 14anu-ban-02(17-02-2015)
;The arrangement of iron filings around the wire.[]ncert 12_04]
;लौह चूर्ण कणों का तार के चारों ओर अभिविन्यास. [ncert 12_04]
(defrule arrangement1 
(declare (salience 100)) 
(id-root ?id arrangement) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)
(viSeRya-around_saMbanXI  ?id ?id2)	;need more sentences to restrict the rule.
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id aBivinyAsa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arrangement.clp  arrangement1  "  ?id "  aBivinyAsa )" crlf)) 
) 

;@@@ Added by 14anu-ban-02(17-02-2015)
;She has an arrangement with the bank to repay the loan.[cambridge]
;उसका ऋण चुकाने के लिए बैंक के साथ समझौता है . [self]
(defrule arrangement2 
(declare (salience 100)) 
(id-root ?id arrangement) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-with_saMbanXI  ?id ?id1)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id samaJOwA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arrangement.clp  arrangement2  "  ?id "  samaJOwA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(16-04-2016)
;So the King had to make arrangements for her departure.[sd_verified]
;isalie rAjA ko usakI ravAnagI ke lie prabanXa karane pade.[sd_verified]
(defrule arrangement3 
(declare (salience 100)) 
(id-root ?id arrangement) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 departure)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id prabanXa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arrangement.clp  arrangement3  "  ?id "  prabanXa )" crlf)) 
) 

;-------------------------- Default Rules ---------------
;Added by 14anu-ban-02(17-02-2015)
;Sentence:Such an arrangement will act like magnetic bottle or magnetic container.[ncert 12_04]
;Translation: इस प्रकार की व्यवस्था एक चुम्बकीय बोतल अथवा चुम्बकीय पात्र की भाँति कार्य करेगी.[ncert]
(defrule arrangement0
(declare (salience 0))
(id-root ?id arrangement)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavasWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  arrangement.clp  arrangement0  "  ?id "  vyavasWA )" crlf))
)

