;$$$Modified by 14anu-ban-02(23-03-2015)       ;sabase is added in the meaning. ;for same sentence.
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 14/03/2014
;Painting is best done in daylight.[oald]
;चित्रकारी  सबसे अच्छे तरीके से दिन के उजाले में की जाती है
(defrule best4
(declare (salience 5000))
;(id-root ?id best)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  =(+ ?id 1) ?id)
;(kriyA-in_saMbanXI  =(+ ?id 1) ?) ;uncomment if any conflict is found
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sabase_acCe_warIke_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  best.clp 	best4   "  ?id "  sabase_acCe_warIke_se )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 14/03/2014
;We're the best of friends.[oald]
;हम सबसे अच्छे दोस्त हैं
(defrule best5
(declare (salience 5000))
(id-root ?id best)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-word ?id1 friends)
;(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sabase_acCe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " best.clp	best5  "  ?id "  " ?id1 "  sabase_acCe )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 14/03/2014
;He was determined not to be bested by his old rival.[oald]
;वह उसके पुराने प्रतिद्वन्दी से परास्त नहीं होने का दृढ निश्चय कर चुका था 
(defrule best6
(declare (salience 5000))
(id-root ?id best)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?kri ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parAswA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  best.clp 	best6   "  ?id " parAswA_ho  )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 15/03/2014
;He was determined not to be bested by his old rival.[oald]
;वह उसके पुराने प्रतिद्वन्दी से परास्त नहीं होने का दृढ निश्चय कर चुका था 
(defrule best7
(declare (salience 0))
(id-root ?id best)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parAswA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  best.clp 	best7   "  ?id " parAswA_kara  )" crlf))
)


;$$$Modified by 14anu-ban-02(23-03-2015)
;Well-drained soil suits the plant best.[oald] 
;सूखी मिट्टी पौधे को सबसे ज्यादा  फबती है . [self]
;Added by Aditya and Hardik(6.7.13),IIT(BHU)
;This is the color I like best.
(defrule best3
(declare (salience 5000))
(id-root ?id best)
?mng <-(meaning_to_be_decided ?id)
;(id-word = (- ?id 1) like)     ;commented by 14anu-ban-02(23-03-2015)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)        ;added by 14anu-ban-02(23-03-2015)
(id-root ?id1 like|suit)        ;added by 14anu-ban-02(23-03-2015)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sabase_jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  best.clp     best3   "  ?id "  sabase_jyAxA )" crlf))
)

;@@@Added by 14anu-ban-02(23-03-2015)
;Those were the best years of my life.[oald]
;वे मेरे जीवन के सबसे अच्छे वर्ष थे . [self]
(defrule best8
(declare (salience 1000))
(id-word ?id best)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 friend|year|way)

=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sabase_acCe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  best.clp     best8   "  ?id "  sabase_acCe )" crlf))
)

;@@@Added by 14anu-ban-02(23-03-2015)
;He works best in the mornings.[oald]
;वह सुबह सबसे अच्छी तरह से काम करता है . [self]
(defrule best9
(declare (salience 1000))
(id-word ?id best)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)        ;need example to restrict the rule.
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sabase_acCI_waraha_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  best.clp     best9   "  ?id "  sabase_acCI_waraha_se )" crlf))
)


;--------------------------------- Default rules ----------------------------------

;$$$Modified by 14anu-ban-02(23-03-2015)     ;root meaning changed to word meaning
;"best","Adj","1.sarvowwama"
;I don't know the best film of the year.[best0]
;मैं वर्ष की सबसे सर्वोत्तम फिल्म  नहीं जानता हूँ . [self]
(defrule best0
(declare (salience 0));salience reduced by Garima Singh
(id-word ?id best)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sarvowwama))
(assert (id-eng-src  ?id  best Word_mng)) ;As Mng is decided on word
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  best.clp 	best0   "  ?id "  sarvowwama )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-eng_src   " ?*prov_dir* "  best.clp     best0   "  ?id "     best   Word_mng )" crlf)
)


;"best","Adv","1.sabase_acCA"
;I work best in the morning.
(defrule best1
(declare (salience 0));salience reduced by Gartima Singh
(id-root ?id best)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sabase_acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  best.clp 	best1   "  ?id "  sabase_acCA )" crlf))
)

;"best","N","1.sarvowwama"
;I tried my level best to win the match.
(defrule best2
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id best)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvowwama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  best.clp 	best2   "  ?id "  sarvowwama )" crlf))
)

