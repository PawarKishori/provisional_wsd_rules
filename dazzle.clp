;@@@ Added by 14anu-ban-04 (11-02-2015)
;I was dazzled by the sunlight.                       [cald]
;मैं धूप से चौधिया गया था .                                     [self]
(defrule dazzle2
(declare (salience 5010))
(id-root ?id dazzle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(id-tam_type ?id passive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOXiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dazzle.clp 	dazzle2  "  ?id "  cOXiyA )" crlf))
)


;@@@ Added by 14anu-ban-04 (11-02-2015)
;I was dazzled by his charm and good looks.            [cald]
;मैं उसके आकर्षक और अच्छे  रूप से चकित हो गया था .                  [self]
(defrule dazzle3
(declare (salience 5020))
(id-root ?id dazzle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 charm|look|simile|warmth)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dazzle.clp 	dazzle3  "  ?id "  cakiwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-04 (11-02-2015)
;She knows a lot of famous people and tried to dazzle me with their names.               [oald]
;वह कई मशहूर लोगों को  जानती है और उनके नामों से मुझे चकित करने की  कोशिश करती है .                       [self] 
(defrule dazzle4
(declare (salience 5010))
(id-root ?id dazzle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dazzle.clp 	dazzle4   "  ?id "  cakiwa_kara )" crlf))
)

;--------------------------------------------------------------DEFAULT RULES ------------------------------------------------------

(defrule dazzle0
(declare (salience 5000))
(id-root ?id dazzle)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakAcOMXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dazzle.clp 	dazzle0   "  ?id "  cakAcOMXa_kara )" crlf))
)

(defrule dazzle1
(declare (salience 4900))
(id-root ?id dazzle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakAcOMXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dazzle.clp 	dazzle1   "  ?id "  cakAcOMXa_kara )" crlf))
)

;"dazzle","VT","1.cakAcOMXa_karanA[karanA]"
;She was dazzled by the bright headlights
;



