;written by Sukhada
;;Example:-We seek your blessings.
(defrule seek1
(declare (salience 5000))
(id-root ?id seek)
?mng <-(meaning_to_be_decided ?id)
(id-word ?tcs1 blessings)
(kriyA-object ?id ?tcs1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAmanA_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seek.clp seek1 " ?id " kAmanA_kara)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  seek.clp seek1 " ?id " kA )" crlf)
)
)
 
;Are you actively seeking jobs?
(defrule seek2
;(declare (salience 0))
(id-root ?id seek)
?mng <-(meaning_to_be_decided ?id)
(id-word ?tcs1 ?noun)
(kriyA-object ?id ?tcs1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id walASa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seek.clp seek2 " ?id "  walASa_kara)" crlf));inconsistency in the mng in assert & print statement has been corrected by Sukhada (15.3.10))
)


;$$$ Modified by 14anu-ban-01 on (27-12-2014)
;@@@ Added by 14anu24
;It may be desirable to seek further advice .
;शायद अधिक सलाह लेना अच्छा होगा .
;Before going to arbitration or to court you may wish to seek an impartial opinion on the merits of your case .
;आरबिट्रेशन करवाने या अदालत जाने से पहले आप संभवतः अपने मामले में किसी की निष्पक्ष सलाह लेना चाहें .
(defrule seek3
(declare (salience 5500))
(id-root ?id seek)
?mng <-(meaning_to_be_decided ?id)
(id-word ?tcs1 ?noun)
(kriyA-object ?id ?tcs1)
(id-word ?tcs1 advice|opinion);added opinion word
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le));changed "lenA" to "le" by 14anu-ban-01 on (27-12-2014)
(assert (make_verbal_noun ?id));added by 14anu-ban-01 on (27-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  seek.clp seek3    "  ?id " )" crlf);added by 14anu-ban-01 on (27-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seek.clp seek3 " ?id "  le)" crlf));changed "lenA" to "le" by 14anu-ban-01 on (27-12-2014)
)

;$$$ Modified by 14anu-ban-01 on (15-01-2015)
;@@@Added by 14anu18 (17-06-14)
;This book seeks to give the reader greater insight into communication with his fellow humans.
;यह पुस्तक सूचना के अन्दर पाठक को अधिक अन्तर्दृष्टि देने के लिए कोशिश करती है . 
;यह पुस्तक पाठक को अपने  मित्रों से व्यवहार करने की और अधिक अन्तर्दृष्टि देने का प्रयास करती है[Translation improved by 14anu-ban-01 on (16-01-2015)]
(defrule seek4
(declare (salience 3000))
(id-root ?id seek)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(id-word =(+ ?id 1) to) 			;commented by 14anu-ban-01 on (15-01-2015)
(kriyA-kriyArWa_kriyA ?id ?id1)			;added '?id1' by 14anu-ban-01 on (15-01-2015)
(to-infinitive  =(- ?id1 1) ?id1)		;added by 14anu-ban-01 on (15-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayAsa_kara/koSiSa_kara));"prayAsa_kara" added by 14anu-ban-01 on (15-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  seek.clp seek4 " ?id "  prayAsa_kara/koSiSa_kara)" crlf));"prayAsa_kara" added by 14anu-ban-01 on (15-01-2015)
)

