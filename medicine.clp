
(defrule medicine0
(declare (salience 5000))
(id-root ?id medicine)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) this)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ORaXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  medicine.clp 	medicine0   "  ?id "  ORaXI )" crlf))
)

;@@@Added by 14anu-ban-08 (09-03-2015)
;She's practicing medicine in Philadelphia.         [oald]
;वह फिलडेल्फ़ीअ में  चिकित्सा पद्धति की डाक्टरी कर रही है .         [self]
(defrule medicine2
(declare (salience 4900))
(id-root ?id medicine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 practice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cikiwsA_paxXawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  medicine.clp 	medicine2   "  ?id "  cikiwsA_paxXawi )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule medicine1
(declare (salience 4900))
(id-root ?id medicine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ORaXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  medicine.clp 	medicine1   "  ?id "  ORaXI )" crlf))
)

;default_sense && category=noun	cikiwsASAswra	0
; Wrong default in wasp, hence commented
;default_sense && category=noun	ORaXI	0
;"medicine","N","1.ORaXI"
;The medicine is beyond my means.
;
;
