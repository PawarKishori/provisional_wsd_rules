;@@@ Added by 14anu-ban-06 (27-02-2015)
;Floodlights illuminated the stadium. (OALD)
;तेज रोशनी वाली लाइट ने स्टेडियम् को प्रकाशित किया .(manual) 
(defrule illuminate0
(declare (salience 0))
(id-root ?id illuminate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakASiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  illuminate.clp   illuminate0   "  ?id "  prakASiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-06 (27-02-2015)
;This text illuminates the philosopher's early thinking. (OALD)
;यह विषय वाक्य दार्शनिक की प्रारम्भिक सोच को स्पष्ट करता है . (manual) 
(defrule illuminate1
(declare (salience 2000))
(id-root ?id illuminate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 text|article)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id spaRta_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  illuminate.clp   illuminate1   "  ?id "  spaRta_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  illuminate.clp       illuminate1   "  ?id " ko )" crlf))
)
