;@@@Added by 14anu-ban-02(26-03-2015)
;Sentence: A blanket ban on tobacco advertising.[oald]	;run the sentence on parser no. 11
;Translation: तम्बाकू विज्ञापनों पर सम्पूर्ण प्रतिबन्ध . [self]
(defrule blanket1 
(declare (salience 100)) 
(id-root ?id blanket) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ban)
 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sampUrNa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blanket.clp  blanket1  "  ?id "   sampUrNa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(26-03-2015)
;Sentence:A blanket of snow.[oald]	
;Translation:बरफ की एक परत .  [self]
(defrule blanket3 
(declare (salience 100)) 
(id-root ?id blanket) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id parawa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blanket.clp  blanket3  "  ?id "  parawa )" crlf)) 
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(26-03-2015)
;Sentence: A blanket refusal.[oald]	;run the sentence on parser no. 6
;Translation: एक सामूहिक इन्कार.[self]
(defrule blanket0 
(declare (salience 0)) 
(id-root ?id blanket) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sAmUhika)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blanket.clp  blanket0  "  ?id "  sAmUhika )" crlf)) 
) 

;@@@Added by 14anu-ban-02(26-03-2015)
;Sentence:The baby was wrapped in a blanket.[oald]	
;Translation:शिशु कम्बल में लपेटा गया था . [self]
(defrule blanket2 
(declare (salience 0)) 
(id-root ?id blanket) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kambala)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blanket.clp  blanket2  "  ?id "  kambala )" crlf)) 
) 
