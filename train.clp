
(defrule train0
(declare (salience 5000))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id training )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id praSikRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  train.clp  	train0   "  ?id "  praSikRaNa )" crlf))
)

;"training","N","1.praSikRaNa"
;Anand got good training in chess.
;
(defrule train1
(declare (salience 4900))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id trained )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id praSikRiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  train.clp  	train1   "  ?id "  praSikRiwa )" crlf))
)

(defrule train2
(declare (salience 4800))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id trained )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id praSikRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  train.clp  	train2   "  ?id "  praSikRiwa_kara )" crlf))
)

(defrule train3
(declare (salience 4700))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id relagAdI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  train.clp 	train3   "  ?id "  relagAdI )" crlf))
)

;@@@Added by 14anu18
;The train of events leading to Percy's death.
;पर्सी की मृत्यु के परिणामस्वरूप घटनाओं का क्रम . 
(defrule train3_1
(declare (salience 4700))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id krama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  train.clp 	train3_1  "  ?id "  rkrama )" crlf))
)

;"train","N","1.relagAdI"
;The boy saw the train coming.
;--"2.kawAra"
;We saw the baggage train.
;--"3.parikara"
;The actress was followed by a train of admirers.
;--"4.silasilA"
;His call obstructed my train of thoughts.
;--"5.puCallA"
;The girls are holding the train of the bride's gown.
;
(defrule train4
(declare (salience 4600))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikRA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  train.clp 	train4   "  ?id "  SikRA_xe )" crlf))
)

;"train","VT","1.SikRA_xenA"
;We have to train our children properly.
;--"2.praSikRiwa_karanA"
;They train their students well.
;--"3.aByAsa_karanA"
;He trained the dog to look after the house.
;--"4.se_[kisI_para]niSAnA_bAzXanA"
;He trained his camera on the picturesque view.
;--"5.anuvarXana_karanA"
;I trained a creeper over the fence.
;


;@@@Added by 14anu-ban-07,(11-03-2015)
;He carefully trained his gun on the suspect.(oald)
;उसने सावधानी से निशाने पर लगाया सन्दिग्ध पर उसकी बन्दूक को . (manual) 
(defrule train5
(declare (salience 4900))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI ?id ?id1)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id niSAne_para_lagA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  train.clp  	train5   "  ?id "  niSAne_para_lagA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  train.clp      train5   "  ?id " ko )" crlf)
)
)

;@@@Added by 14anu-ban-07,(11-03-2015)
;I trained a creeper over the fence.(same file)(parser no. 36)
;मैंने बाढ पर लता उगाई . (manual)
(defrule train6
(declare (salience 4900))
(id-word ?id trained)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 creeper)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ugA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  train.clp 	train6   "  ?id "  ugA )" crlf))
)

;@@@Added by 14anu-ban-07,(11-03-2015)
;His call obstructed my train of thoughts. (same file)
;उसके बुलावे ने  मेरे विचारों के  सिलसिले को अवरुद्ध किया .  (manual)
(defrule train7
(declare (salience 4800))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 thought)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id silasilA))
(assert (id-wsd_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  train.clp 	train7  "  ?id "  silasilA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  train.clp train7  "  ?id " ko)" crlf))
)


;@@@Added by 14anu-ban-07,(11-03-2015)
;The girls are holding the train of the bride's gown.(same file)
;लडकियाँ ने नववधू के चोगे का पुछल्ला भाग पकड रखा हैं . (manual)
(defrule train8
(declare (salience 5000))
(id-root ?id train)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 gown)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id puCallA_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  train.clp 	train8  "  ?id "  puCallA_BAga )" crlf)
))
