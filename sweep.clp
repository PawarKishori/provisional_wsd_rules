
(defrule sweep0
(declare (salience 5000))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id sweeping )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mahawwvapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sweep.clp  	sweep0   "  ?id "  mahawwvapUrNa )" crlf))
)

;"sweeping","Adj","1.mahawwvapUrNa"
;Teacher has sweeping power on children.
;
(defrule sweep1
(declare (salience 4900))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id sweeping )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saPAI_karanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sweep.clp  	sweep1   "  ?id "  saPAI_karanA )" crlf))
)
;"sweeping","N","1.saPAI_karanA"
;Sweeping the home is the best work for a lady.

;@@@ Added by jagriti(3.3.2014)
;Sweep up the dust.[rajpal]
;धूल बटोर लीजिए . 
(defrule sweep2
(declare (salience 4700))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 up)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 batora_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sweep.clp	sweep2  "  ?id "  " ?id1 "  batora_le )" crlf))
)
;@@@ Added by jagriti(3.3.2014)
;To sweep everything into one's net.[rajpal]
;सब कुछ अपने पास समेट लेना . 
(defrule sweep3
(declare (salience 4600))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sameta_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep3   "  ?id "  sameta_le )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (31-03-2015)
;Torrential rain swept away a car from an Oregon grocery store parking lot. [partial sentence from COCA]
;मूसलाधार वर्षा ऒरगन किराने की दुकान के  पार्किंग स्थल से गाडी बहा ले गई . [self]
;@@@ Added by jagriti(3.3.2014)
;The river swept away all the huts .[rajpal]
;नदी सारी झोपड़ियां बहा ले गयी.
(defrule sweep4
(declare (salience 4500))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 away)
(kriyA-upasarga ?id ?id1)
(kriyA-subject ?id ?id2)
(id-root ?id2 river|flood|rain|floodwater)	;added "rain|floodwater" by 14anu-ban-01  on (31-03-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahA_le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sweep.clp	sweep4  "  ?id "  " ?id1 "  bahA_le_jA )" crlf))
)
;@@@ Added by jagriti(3.3.2014)
;The government swept away several diseases.[rajpal]
;सरकार ने कई बीमारियाँ का उन्मूलन कर दिया . 
(defrule sweep5
(declare (salience 4400))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 away)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id2 disease)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 unmUlana_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sweep.clp	sweep5  "  ?id "  " ?id1 "  unmUlana_kara_xe )" crlf))
)
;@@@ Added by jagriti(3.3.2014)
;Rumours swept through the town.[oald] 
;अफवाहें नगर में से तेजी से फैल गयीं
;A storm swept across the plain.[rajpal]
;आँधी समतल भूमी के उस पार तेजी से फैल गई . 
(defrule sweep6
(declare (salience 4300))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-through_saMbanXI ?id ?id1)(kriyA-across_saMbanXI ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_PEla_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep6   "  ?id "  wejI_se_PEla_jA)" crlf))
)
;@@@ Added by jagriti(3.3.2014)
;The train swept the bridge soon.[rajpal]
;रेलगाडी शीघ्र ही पुल को तेजी से पार कर गयी . 
(defrule sweep7
(declare (salience 4200))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 train)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_para_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep7   "  ?id "  wejI_se_para_kara)" crlf))
)
;@@@ Added by jagriti(3.3.2014)
;He swept over the papers lying on the table.[rajpal]
;उसने मेज पर पडे हुए कागजों पर नजर दौडायी . 
(defrule sweep8
(declare (salience 4100))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-over_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id najara_xOdZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep8   "  ?id "  najara_xOdZA)" crlf))
)
;@@@ Added by jagriti(3.3.2014)
;He indicated the door with a sweep of his arm.[oald]
;उसने अपनी बाहु के घुमाव से दरवाजा दर्शाया . 
;They rowed the boat with sweep of their oars.[rajpal]
;उन्होंने नाव को पतवारों के घुमाव से खेया . 
(defrule sweep9
(declare (salience 4000))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 arm|oar|eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep9   "  ?id "  GumAva )" crlf))
)
;@@@ Added by jagriti(3.3.2014)
;Her book covers the long sweep of the country's history.[oald]
;उसकी किताब देश के इतिहास के लंबी अवधि को शामिल करती है.
(defrule sweep10
(declare (salience 3900))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 long)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep10   "  ?id "  avaXi )" crlf))
)
;@@@ Added by jagriti(3.3.2014)
;The rescue helicopter made another sweep over the bay.[oald]
;बचाव हेलीकाप्टर ने खाड़ी के ऊपर एक और चक्कर लगाया. 
(defrule sweep11
(declare (salience 3800))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 make)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep11   "  ?id "  cakkara )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (31-03-2015):changed conditions because "uda_jA" is more related to 'storm' as compared to 'tent'
;@@@ Added by 14anu-ban-11 on (13-12-2014)
;Their tent was swept away in the storm. (oald)
;उनका तम्बू आँधी में उड गया था.(manual)
(defrule sweep16
(declare (salience 300))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-subject  ?id ?id1)	;commented by 14anu-ban-01  on (31-03-2015)
(kriyA-upasarga  ?id ?id2)
;(id-root ?id1 tent)		;commented by 14anu-ban-01  on (31-03-2015)
(id-root ?id2 away)
(id-root ?id1 storm)		;added by 14anu-ban-01  on (31-03-2015)
(kriyA-in_saMbanXI ?id ?id1)	;added by 14anu-ban-01  on (31-03-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 uda_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep16   "  ?id " "?id2 "  uda_jA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (24-09-2014)
;2 Law of areas:The line that joins any planet to the sun sweeps equal areas in equal intervals of time.[ncert corpus]
;2 क्षेत्रफलों का नियम: सूर्य से किसी ग्रह को मिलाने वाली रेखा समान समय अन्तरालों में समान क्षेत्रफल प्रसर्प करती है.[ncert corpus]

(defrule sweep14
(declare (salience 3000))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 area)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasarpa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep14  "  ?id "  prasarpa_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-09-2014)
;From Kepler's second law, equal areas are swept in equal times.  [ncert corpus]
;केप्लर के दूसरे नियम के अनुसार, समान समय अन्तरालों में समान क्षेत्रफल प्रसर्प होते हैं.[ncert corpus]
(defrule sweep15
(declare (salience 3000))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 area)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasarpa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep15  "  ?id "  prasarpa_ho )" crlf))
)

;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_sweep10
(declare (salience 3900))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 long)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " sweep.clp   sub_samA_sweep10   "   ?id " avaXi )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_sweep10
(declare (salience 3900))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 long)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " sweep.clp   obj_samA_sweep10   "   ?id " avaXi )" crlf))
)


;@@@ Added by 14anu-ban-11 on (07-03-2015)
;A large wave swept away half the sandcastle. (cald)
;विशाल तरङ्ग ने रेत से बना हुआ आधा किला नष्ट कर दिया . (manual)
(defrule sweep17
(declare (salience 150))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(kriyA-upasarga  ?id ?id2)
(id-word ?id1 sandcastle)
(id-root ?id2 away)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 naRta_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep17   "  ?id " "?id2 "  naRta_kara_xe)" crlf))
)


;@@@ Added by 14anu-ban-11 on (07-03-2015)
;Strong winds regularly sweep the islands. (oald)
;सशक्त हवाएँ नियमित रूप से द्वीप पर तेजी से चलती हैं . (manual)
(defrule sweep18
(declare (salience 160))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 wind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep18  "  ?id "  wejI_se_cala)" crlf))
)

;$$$ Modified by 14anu-ban-01 on (31-03-2015):corrected meaning
;@@@ Added by 14anu-ban-11 on (07-03-2015)
;Without another word she swept out of the room.  (oald)
;एक और शब्द के बिना वह कमरे से बाहर चली गई . (manual)
(defrule sweep19
(declare (salience 170))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-without_saMbanXI  ?id ?id1)
(id-root ?id1 word)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calA_jA))	;changed "calI_jA" to "calA_jA" by 14anu-ban-01  on (31-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep19  "  ?id "  calA_jA)" crlf))	;changed "calI_jA" to "calI_jA" by 14anu-ban-01
)


;$$$ Modified by 14anu-ban-01 on (31-03-2015):corrected meaning
;@@@ Added by 14anu-ban-11 on (07-03-2015)
;His eyes swept around the room.  (oald)
;उसने आँखो को कमरे के चारों ओर घुमाया .(manual)
;उसकी आँखें कमरे में यहाँ-वहाँ घूमीं.[Translation improved by 14anu-ban-01  on (31-03-2015)]
(defrule sweep20
(declare (salience 180))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI  ?id ?id1)
(id-root ?id1 room)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUma))	;changed "GumAyA" to "GUma" by 14anu-ban-01  on (31-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep20  "  ?id "  GUma)" crlf))	;changed "GumAyA" to "GUma" by 14anu-ban-01
)


;@@@ Added by 14anu-ban-01 on (31-03-2015)
;The swarm would be swept away if it came into the river.[COCA]--parse no.2
;यदि जमघट नदी में आया तो वह तबाह हो/मिट जाएगा  . [self]
(defrule sweep21
(declare (salience 150))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object  ?id ?))
(id-root ?id1 away)
(kriyA-upasarga  ?id ?id1)
(kriyA-subject ?id ?id2)
(id-root ?id2 ?str)			
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wabAha_ho/mita))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep21   "  ?id " "?id1 "  wabAha_ho/mita_jA)" crlf))
)


;@@@ Added by 14anu-ban-01 on (31-03-2015)
;They swept away their old assurances without a thought.[COCA]
;उन्होंने विचार के बिना उनके पुराने आश्वासन नष्ट कर दे/मिटा दिए . [self]
(defrule sweep22
(declare (salience 150))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 away)
(kriyA-upasarga  ?id ?id1)
(kriyA-subject ?id ?)
(kriyA-object  ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 naRta_kara_xe/mitA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep22   "  ?id " "?id1 "  naRta_kara_xe/mitA_xe)" crlf))
)


;@@@ Added by 14anu-ban-01 on (31-03-2015)
;After being swept away from her parents, Little Noodle journeys across the grocery store to find her way home. [COCA]
;उसके माँ बाप से दूर हो जाने के बाद, छोटी नूडल अपने घर का रास्ता ढू़ँढने के लिए किराने की दुकान में  यहाँ से वहाँ घूमती  है . [self]
(defrule sweep23
(declare (salience 1000))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) away)
(id-root ?id1 parent|family|friend|home)
(kriyA-from_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) xUra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep23   "  ?id " "(+ ?id 1) "  xUra_ho)" crlf))
)


;@@@ Added by 14anu-ban-01 on (31-03-2015)
;Most of the houses were swept away by the flood.[self: with respect to COCA]
;ज्यादातर घर बाढ के द्वारा बहाकर ले जाए गये थे .[self]
(defrule sweep24
(declare (salience 1000))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 away)
(kriyA-upasarga  ?id ?id1)
(id-root ?id2 rain|flood|river|floodwater|water)
(kriyA-by_saMbanXI  ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep24   "  ?id " "?id1 "  bahA_le)" crlf))
)



;@@@ Added by 14anu-ban-01 on (31-03-2015)
;A rush of hot wind swept away the smoke of the President's cigar.[COCA]
;एक गर्म हवा के तीव्र प्रवाह ने प्रेसिडेन्ट के  चुरूट का धुँआ धूमिल कर दिया.[self]
(defrule sweep25
(declare (salience 1000))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 away)
(kriyA-upasarga  ?id ?id1)
(kriyA-subject ?id ?)
(id-root ?id2 smoke)
(kriyA-object ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XUmila_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep25   "  ?id " "?id1 "  XUmila_kara_xe)" crlf))
)


;@@@ Added by 14anu-ban-01 on (31-03-2015)
;He swept away the pencil shavings from the floor's wooden planks.[self: with respect to COCA]
;उसने फर्श के लकडी के तख्तों से धूल और पेंसिल कतरनें झाड दीं . [self]
(defrule sweep26
(declare (salience 1000))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 away)
(kriyA-upasarga  ?id ?id1)
(kriyA-subject ?id ?)
(id-root ?id2 dust|dirt|pencil|shaving)
(kriyA-object ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 JAda_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep26   "  ?id " "?id1 "  JAda_xe)" crlf))
)


;@@@ Added by 14anu-ban-01 on (31-03-2015)
;The boat floated out into the current and we were swept away into darkness.[COCA]
;नाव धारा में तैर आई और हम अन्धकार में बह गये . [self]
(defrule sweep27
(declare (salience 4600))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 away)
(kriyA-upasarga  ?id ?id1)
(not(kriyA-object  ?id ?))
(kriyA-into_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  sweep.clp 	sweep27   "  ?id " "?id1 "  baha)" crlf))
)


;------------------------ Default Rules ----------------------

;"sweep","N","1.PElAva"
(defrule sweep12
(declare (salience 100))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep12   "  ?id "  PElAva )" crlf))
)


;"sweep","V","1.saPZAI_karanA"
;I use to sweep my cycle everyday.
(defrule sweep13
(declare (salience 100))
(id-root ?id sweep)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saPZAI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sweep.clp 	sweep13   "  ?id "  saPZAI_kara )" crlf))
)


