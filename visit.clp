
; Added by human being
;Modified by Meena(5.12.09)
(defrule visit0
(declare (salience 4000))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 us|them|me|him|her|you)
(kriyA-object ?id ?id1)   
;(kriyA-object ?id1 ?id)  ;commented by Meena(5.12.09)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp 	visit0   "  ?id "  mila )" crlf))
)


;$$$ modified the meaning by 14anu07 0n 03/07/2014
;They are looking forward to Musharraf ' s visit.
;वे मुशर्रफ के आने की प्रतीक्षा कर रहे हैं . 
(defrule visit1
(declare (salience 4400))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  AnA));Changed the eaning from  "mulAkAwa to AnA"
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp 	visit1   "  ?id "   AnA)" crlf))
)




;added by Meena(5.12.09)
;My uncle's mother's cousin is visiting us .
(defrule visit2
(declare (salience 4300))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKane_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp     visit2   "  ?id "  xeKane_A )" crlf))
)


;Salience reduced by Meena(5.12.09)
(defrule visit3
(declare (salience 0))
;(declare (salience 4300))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKane_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp 	visit3   "  ?id "  xeKane_jA )" crlf))
)

;"visit","VT","1.xeKane_jAnA[AnA]"
;Tourists visit Taj Mahal in Agra
;

;$$$ Modified by Shirisha Manju 26-05-2016
;@@@ Added by 14anu-ban-09 on (23-07-2014)
;One gets Moksha with a visit to the Saptapuris!
;sapwapuriyoM ke xarSana se milawa hE mokRa.
(defrule visit4
(declare (salience 5000))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (viSeRya-with_saMbanXI ?id1 ?id)(viSeRya-to_saMbanXI ?id1 ?id)) ;modified "viSeRyA" as "viSeRya" by Shirisha Manju 26-05-2016
;(id-root ?id1 noun) ;commented by Shirisha Manju 26-05-2016
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp 	visit4   "  ?id "  xarSana )" crlf))
)


;@@@ Added by 14anu-ban-07, 28-07-2014
;देखने  का  समय   :
;The visiting hours of Palace is fixed from 8.30 to 11.30 in the morning and 1.00 to 3.30 in the afternoon. (parallel corpus)
;पैलेस  देखने  का  समय  प्रातः  8.30  से  11.30  व  दोपहर  1.00  से  3.30  बजे  नियत  है  ।
(defrule visit04
(declare (salience 4500))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) hour)
(viSeRya-of_saMbanXI =(+ ?id 1) ?id2)
(id-root ?id2 Palace|palace|Museum|museum)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) xeKane_kA_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  visit.clp  visit04   "  ?id "    " (+ ?id 1)" xeKane_kA_samaya)" crlf))
)

;@@@ Added by 14anu-ban-07, 05-08-2014
;मिलने का समय 

(defrule visit05
(declare (salience 4400))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) hour)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1)  milane_kA_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  visit.clp  visit05   "  ?id "    " (+ ?id 1)"  milane_kA_samaya)" crlf))
)



;LEVEL 
;Headword : visit
;
;Examples --
;
;"visit","V","1.xeKane_jAnA[AnA]"
;Children like to visit zoo.
;bacce cidiyAGara xeKanA pasanxa karawe hEM
;--"2.mulAkAwa_karanA"
;He visited his friend on sunday.
;ravivAra ko vaha apane xoswa se mulAkAwa karane gayA.
;--"3.milane_jAnA(cikiwsArWa)" <-- mulAkAwa_karane_jAnA"
;He visited the doctor yesterday
;vaha dAktara kala dOYktara ke pAsa milane gayA(cikiwsArWa).
;
;"visit","N","mulAkAwa"
;I paid a visit to my teacher  after a long time.
; kaI xinoM ke bAxa mere SikRaka se merI mulAkAwa huI.
;
;vyAKyA : uparyukwa aMkiwa Sabxa "visit" kA kriyA va saMjFA vAkyoM meM nimna  
;nABikIya BAva prakata howA hE "mulAkAwa_[karanA]".isakA sUwra nimna hE
; 
;sUwra : mulAkAwa{vyakwi_yA_sWAna_se}

;$$$Modified by 14anu-ban-07,(09-02-2015)
;meaning changed from mulAkAwa to yAwrA in print stmt by 14anu-ban-07,(09-02-2015)
;@@@Added by 14anu19
(defrule visit6
(declare (salience 4500))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id ?sam)
(id-root ?sam  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAwrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp     visit6   "  ?id "  yAwrA )" crlf))
)
;$$$Modified by 14anu-ban-07,(09-02-2015)
;meaning changed from mulAkAwa to xOrA in print stmt by 14anu-ban-07,(09-02-2015)
;@@@Added by 14anu19(21-06-2014)
;I made a visit to school.
;मैंने विद्यालय का दौरा किया . 
(defrule visit5
(declare (salience 4500))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-to_saMbanXI ?id1 ?sam)
(id-root ?sam  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOrA))
(assert (id-H_vib_mng ?sam kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp     visit5   "  ?id "  xOrA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  visit.clp     visit5  "  ?id1 "  kA )" crlf)
)

;@@@Added by 14anu-ban-07,(04-02-2015)
;The natural beauty of Anchuthengu is worth visit .(tourism corpus)
;अंचुतेंगु  की  प्राकृतिक  सुषमा  दर्शनीय  है  ।(tourism corpus)
(defrule visit7
(declare (salience 5100))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 worth)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xarSanIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " visit.clp	visit7  "  ?id "  " ?id1 " xarSanIya )" crlf))
)

;@@@Added by 14anu-ban-07,(06-02-2015)
;The 15 bow-shaped domes of the jama masjid surprise the visiting tourists .(tourism corpus)
;जामा  मस्जिद  के  15  धनुषाकार  गुंबद  आने  वाले  सैलानियों  को  हैरान  कर  देते  हैं  ।(tourism corpus)
;जामा  मस्जिद  के  15  धनुषाकार  गुंबद  देखने आने वाले पर्यटकों  को  हैरान  कर  देते  हैं  ।(manual)
(defrule visit8
(declare (salience 5200))
(id-word ?id visiting)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 tourist)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKane_Ane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp 	visit8   "  ?id "  xeKane_Ane_vAlA )" crlf))
)


;@@@Added by 14anu-ban-07,(06-02-2015)
;The sins of the fathers are visited upon the children (= children are blamed or suffer for what their parents have done)(oald)(parser no. 7)
;माता पिताओं के गुनाहों की  सजा  मिलती हैं बच्चों  को.(manual) 
(defrule visit9
(declare (salience 5300))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 upon|on)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?sam)
(id-root ?sam ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sajA_mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " visit.clp visit9  "  ?id "  " ?id1 "  sajA_mila  )" crlf)
)
)


;@@@Added by 14anu-ban-07,(06-02-2015)
;He left in 1983, horrified by the devastation that warfare and famine had visited on his homeland.(cambridge)(parser no. 52)
;विध्वंस के द्वारा भयभीत होकर जो युद्ध और अकाल ने उसके स्वदेश को क्षति  पहुँचाई थी ,वह  1983 में छोड़ कर चला गया  था .(manual)
(defrule visit10
(declare (salience 5400))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 upon|on)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kRawi_pahuzcA))
(assert (id-wsd_viBakwi ?id2 ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " visit.clp visit10  "  ?id "  " ?id1 "  kRawi_pahuzcA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  visit.clp      visit10   "  ?id2 " ko)" crlf)
)
)


;@@@Added by 14anu-ban-07,(06-02-2015)
;I was hoping to visit with Katie while I was in town.(cambridge)(parser no. 84)
;मैं  कॆटी के साथ समय बिताने की आशा कर रहा था जब तक  मैं नगर में हू़ँ .
(defrule visit11
(declare (salience 5500))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 with)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaya_biwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp 	visit11   "  ?id "  samaya_biwA )" crlf)
)
)

;@@@Added by 14anu-ban-07,(09-02-2015)
;I paid a visit to my teacher  after a long time.(same file)
;kaI samaya ke bAxa mere SikRaka se merI mulAkAwa huI.(manual)
(defrule visit12
(declare (salience 4600))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-to_saMbanXI ?id1 ?sam)
(id-root ?sam  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mulAkAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp     visit12   "  ?id "  mulAkAwa )" crlf))
)

;@@@Added by 14anu-ban-07,(09-02-2015)
;Xi said that since his visit to India in September last year, relations between the two countries have entered into a new stage. [by mail]
;Xi ने कहा कि  उनके पिछले वर्ष के सितम्बर में  भारत के दौरे के बाद से ,दोनों  देशों के बीच सम्बन्ध एक नये स्तर पर पहुँचे है.( manual)
(defrule visit13
(declare (salience 4700))
(id-root ?id visit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-since_saMbanXI  ? ?id)
;(id-root ?sam  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  visit.clp     visit13   "  ?id "  xOrA )" crlf)
)
)  

