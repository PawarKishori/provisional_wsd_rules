; Added by human being
(defrule million0
(declare (salience 5000))
(id-root ?id million)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id millions)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id xasa_lAKa))
(assert (id-wsd_number ?id s))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  million.clp  	million0   "  ?id "  xasa_lAKa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number  " ?*prov_dir* "  million.clp    million0   "  ?id " s )" crlf))
)

;$$$Modified by 14anu-ban-08 (04-03-2015)      ;relation added, constraint commented,changed 'id-root'
;The government has disbursed millions of dollars in foreign aid.  [merriam-webster]
;सरकार ने विदेशी सहायता में लाखों डौलर व्यय किए हैं .    [self]
;There were millions of people there.  [oald]
;वहाँ लाखों लोग थे.  [self]
(defrule million1
(declare (salience 5051))   ;salience by 14anu-ban-08 (08-04-2015)
(id-root ?id million)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) of)         ;commented by 14anu-ban-08 (04-03-2015) 
(viSeRya-of_saMbanXI ?id ?id1)    ;added 'id1' by 14anu-ban-08 (08-04-2015)
(id-root ?id1 dollar)            ;added by 14anu-ban-08 (08-04-2015)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lAKoM))   ;changed 'id-root ' to 'id-word' by 14anu-ban-08 (04-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  million.clp 	million1   "  ?id "  lAKoM )" crlf))   ;changed 'id-root ' to 'id-word' by 14anu-ban-08 (04-03-2015)
)

; It is made up of millions of cells.
(defrule million2
(declare (salience 4800))
(id-root ?id million)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xasa_lAKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  million.clp 	million2   "  ?id "  xasa_lAKa )" crlf))
)

;"million","N","1.xasa lAKa"
;We would be happier with a million laughs than with a million rupees.
;
;

;$$$Modified by 14anu-ban-08 (12-01-2015)      ;changed meaning from 'lAKoM_logoM' to 'lAKoM_loga'    
;@@@ Added by 14anu11
;Sometimes he is the single - minded revolutionary going like the arrow to his goal and shaking up millions in the process . 
;कभी वे तीर की तरह अपने मकसद की ओर जाने वाले एकनिष्ठ क्रांतिकारी दिखाई देते हैं और इस प्रक्रिया में लाखों लोगों को झकझोरते चलते हैं 
(defrule million3
(declare (salience 5050))
(id-root ?id million)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id millions) ;commented by 14anu-ban-09 on (12-01-2015)
(kriyA-object  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAKoM_loga))    ;changed meaning from 'lAKoM_logoM' to 'lAKoM_loga' by 14anu-ban-08 (12-01-2015)   ;changed 'id-word' to 'id-root' by 14anu-ban-08 (09-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  million.clp  	million3   "  ?id "  lAKoM_loga )" crlf))  ;changed 'id-word' to 'id-root' by 14anu-ban-08 (09-03-2015)
)

;@@@Added by 14anu-ban-08 (04-03-2015)
;There are short term variations taking place over centuries and long term variations taking place over a period of a million years.  [NCERT]
;इनमें अल्पकालिक परिवर्तन भी शामिल हैं जो शताब्दियों में नजर आने लगते हैं और दीर्घकालीन परिवर्तन भी जो लाखों वर्षों के दीर्घकाल में दृष्टिगत होते हैं. [NCERT]
;इनमें अल्पकालिक परिवर्तन भी शामिल हैं जो शताब्दियों में होते हैं और दीर्घकालीन परिवर्तन भी जो लाखों वर्षों के दीर्घकाल में दृष्टिगत होते हैं. [self]
(defrule million4
(declare (salience 4800))
(id-root ?id million)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-saMKyA_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lAKoM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  million.clp 	million4   "  ?id "  lAKoM )" crlf))
)

;@@@Added by 14anu-ban-08 (10-03-2015)
;There's a deficit of $3 million in the total needed to complete the project.   [oald]
;परियोजना सम्पूर्ण करने में 3 मिलियन डॉलर की कमी की आवश्यकता थी.  [self]
(defrule million5
(declare (salience 5051))
(id-root ?id million)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 total)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id miliyana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  million.clp 	million5   "  ?id "  miliyana )" crlf))
)

;@@@Added by 14anu-ban-08 (31-03-2015)
;They paid $2 million to the world champion to endorse their new aftershave.  [cald]
;उन्होंने अपने नये आफ्टरशेव लोशन का विज्ञापन करने के लिए विश्व विजेता को  2 मिलियन $ रुपये दिए .   [self]
(defrule million6
(declare (salience 5050))
(id-root ?id million)
?mng <-(meaning_to_be_decided ?id)
(saMKyA-saMKyA ?id ?)
(id-cat_coarse ?id number)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id miliyana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  million.clp 	million6   "  ?id "  miliyana )" crlf))
)

