;@@@ Added by 14anu-ban-03 (10-04-2015)
;The shed comes in conjecture that you assemble yourself. [oald]
;शेड  अनुभाग में आते है कि जो कि आपको खुद एकत्रित करने होते हैं . [manual]
(defrule conjecture2
(declare (salience 5000))
(id-root ?id conjecture)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjecture.clp 	conjecture2   "  ?id "  anuBAga )" crlf))
)


;@@@ Added by 14anu-ban-03 (10-04-2015)
;Popular conjecture created a world of demons.  [hinkhoj]
;लोकप्रिय कल्पना ने राक्षसों का एक विश्व बनाया . [manual]
(defrule conjecture3
(declare (salience 5000))
(id-root ?id conjecture)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalpanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjecture.clp 	conjecture3   "  ?id "  kalpanA )" crlf))
)


;@@@ Added by 14anu-ban-03 (10-04-2015)
;We can only conjecture about what was in the killer's mind. [cald]
;हम केवल अनुमान लगा सकते हैं कि हत्यारे के मन में क्या था . [manual] 
(defrule conjecture4
(declare (salience 5000))           
(id-root ?id conjecture)
?mng <-(meaning_to_be_decided ?id)
(kriyA-about_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjecture.clp 	conjecture4   "  ?id "  anumAna_lagA )" crlf))
)

;--------------------------------------------Default rules------------------------------------------------

(defrule conjecture0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (10-04-2015)
(id-root ?id conjecture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjecture.clp 	conjecture0   "  ?id "  anumAna )" crlf))
)

;"conjecture","N","1.anumAna"
;Sometimes weather conjectures become true.	
;
(defrule conjecture1
(declare (salience 00))           ;salience reduced by 14anu-ban-03 (10-04-2015)
(id-root ?id conjecture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjecture.clp 	conjecture1   "  ?id "  anumAna_kara )" crlf))
)

;"conjecture","V","1.anumAna_karanA"
;Most of the weather predictions are conjectured.
;---------------------------------------------------------------------------------------------------------------------

