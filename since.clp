;commented and merged in 'since5' by Bhagyashri Kulkarni (20.09.2016)
;What have you been doing since morning?(rapidex)
;Modified by Meena(18.02.10)
;(defrule since0
;(declare (salience 5000))
;(id-root ?id since)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) yesterday|tomorrow|today|morning|January) 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id se))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp 	since0   "  ?id "  se )" crlf))
;)



(defrule since1
;(declare (salience 4700))
(declare (salience 0))
(id-root ?id since)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waba_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp 	since1   "  ?id "  waba_se )" crlf))
)

;"since","Adv","1.waba_se"


;$$$ Modified by 14anu-ban-01 on (05-02-2015) --- added 'come' in the list
;I have been very busy since I came back from holiday.[self]
;जब से मैं अवकाश से वापिस आया हूँ मैं बहुत व्यस्त रहा हूँ      [self]
;Added by Meena(28.10.10)
;I heard from him twice since he went away.
(defrule since2
(declare (salience 4700))
(id-root ?id since)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viBakwi   ?id1  ?id)
;(kriyA-subject ?id2 ?id1)
(id-root ?id1 go|leave|play|meet|come);Added play and meet in the list by Aditya and Hardik,IIT(BHU) batch 2012-2017.		;added 'come' in the list by 14anu-ban-01 on (05-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaba_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp   since2   "  ?id "  jaba_se )" crlf))
)

;Removed 2 examples by 14anu-ban-01 on (27-10-2014) from here and added them in since7.
;Modified by Meena(28.10.10)
;It makes sense that the charge approaches zero, since the balloon is losing its charge.
;Modified by Meena(18.02.10)
(defrule since3
(declare (salience 4550));salience reduced from 4600 to 4550 by 14anu-ban-01 on (27-10-2014)
(id-root ?id since)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viBakwi   ?id1  ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyoMki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp   since3   "  ?id "  kyoMki )" crlf))
)




;Added by Meena(26.10.10)
;It makes sense that the charge approaches zero, since the balloon is losing its charge.
;(defrule since4
;(declare (salience 10))
;;(declare (salience 4600))
;(id-root ?id since)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id conjunction)   
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kyoMki))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp   since3   "  ?id "  kyoMki )" crlf))
;)

;"since","Conj","1.cUzki"
(defrule since4
;(declare (salience 0))
;(declare (salience 4500))
(id-root ?id since)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaba_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp 	since4   "  ?id "  jaba_se )" crlf))
)

;"since","Prep","1.jaba[waba]_se"


;$$$ Modified by Bhagyashri Kulkarni (20-09-2016)
;He is working on a new project since January.
;वह जनवरी से एक नयी परियोजना पर काम कर रहा है . 
;What have you been doing since morning? 
;आप सुबह से क्या करते रहे हैं? 
;Since 1957, advances in technology have enabled many countries including India to launch artificial earth satellites for practical use in fields like telecommunication, geophysics and meteorology.
;वर्ष 1957 से विज्ञान तथा प्रौद्योगिकी में उन्नति के फलस्वरूप भारत सहित कई देश दूर सञ्चार, भू भौतिकी, मौसम विज्ञान के क्षेत्र में व्यावहारिक उपयोगों के लिए मानव-निर्मित भू उपग्रहों को कक्षाओं में प्रमोचित करने योग्य बन गए हैं.
;$$$ Modified by 14anu-ban-01 on (05-02-2015)
;The directional property of magnets was also known since ancient times.[NCERT corpus]
;चुम्बकों का दैशिक गुण भी प्राचीन काल से ज्ञात था[NCERT corpus]
;Added by sheetal(5-10-09)
(defrule since5
(declare (salience 4550))
(id-root ?id since)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-since_saMbanXI  ? ?id1)(kriyA-since_saMbanXI ? ?id1)) ;added 'viSeRya-since_saMbanXI' by Bhagyashri Kulkarni
(or(id-cat_coarse =(+ ?id 1) number)(id-root ?id1 day|time|yesterday|tomorrow|today|morning|January))	;added 'times' by 14anu-ban-01 on (05-02-2015) ;added (id-cat_coarse =(+ ?id 1) number) and 'yesterday|tomorrow|today|morning|january' by Bhagyashri Kulkarni
;(link_name-lnode-rnode Jp ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp     since5   "  ?id "  se )" crlf))
)
;Added kriyA-since_saMbanXI relation instead of link-name "jp"

;Commented by 14anu-ban-01 on (05-02-2015) because "since" as conjunction will not have meaning "waba_se"
;@@@ Added by 14anu09[21-06-14]
;A decade has passed since...
;एक दशक गुजर चुका है तब से ...
;(defrule since06
;(declare (salience 4700))
;(id-root ?id since)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-vAkya_viBakwi   ?id1  ?id)
;(id-cat_coarse ?id conjunction) 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id waba_se))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp   since06   "  ?id "  waba_se )" crlf))
;)


;commented and merged in 'since5' by Bhagyashri Kulkarni (20.09.2016)
;@@@ Added by 14anu-ban-01 on (28-10-2014)
;Since, 1957, advances in technology have enabled many countries including India to launch artificial earth satellites for practical use in fields like telecommunication, geophysics and meteorology.[NCERT corpus]
;वर्ष 1957 के पश्चात् विज्ञान तथा प्रौद्योगिकी में उन्नति के फलस्वरूप भारत सहित कई देश दूर सञ्चार, भू भौतिकी, मौसम विज्ञान के क्षेत्र में व्यावहारिक उपयोगों के लिए मानव-निर्मित भू उपग्रहों को कक्षाओं में प्रमोचित करने योग्य बन गए हैं.[NCERT corpus]
;(defrule since6
;(declare (salience 4550))
;(id-root ?id since)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse =(+ ?id 1) number)
;(id-right_punctuation  =(+ ?id 1) PUNCT-Semicolon|PUNCT-Comma|PUNCT-Dot)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id se))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp 	since6   "  ?id "  se )" crlf))
;)

;@@@ Added by 14anu-ban-01 on (28-10-2014)
;Since its time period is around 100 minutes it crosses any altitude many times a day.[NCERT corpus]
;चूँकि इन उपग्रहों का आवर्तकाल लगभग 100 मिनट होता है, अतः ये किसी भी अक्षांश से दिन में कई बार गुजरते हैं.[NCERT corpus]
;Added following 2 examples from since3
;Since native speakers of English do not produce a variable mishmash of words of the sort in 4 , there must be another sort of rules according to which sentences are composed . 
;Since I know English, he spoke to me.
;Added following  example from since9 on (05-02-2015)
;Since a large number of electrons absorb energy, the energy absorbed per electron per unit time turns out to be small. (Ncert)
;चूँकि एक बडी सङ्ख्या में इलेक्ट्रॉन ऊर्जा अवशोषित करते हैं, अतः प्रति इलेक्ट्रॉन प्रति इकाई समय में अवशोषित ऊर्जा बहुत कम होगी.(Ncert)
(defrule since7
(declare (salience 4550))
(id-root ?id since)
?mng <-(meaning_to_be_decided ?id)
(id-word 1 since)
(not(kriyA-since_saMbanXI  ? ?))	;added by 14anu-ban-01 on (05-02-2015) ### Since the time he left his family, Ender demonstrated a great amount of courage.[here, since='jaba_se"]
(not (id-cat_coarse =(+ ?id 1) number))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cUzki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  since.clp 	since7  "  ?id "  cUzki )" crlf))
)

;@@@ Added by 14anu-ban-01 on (13-11-2014)
;However, terms similar to "soy milk" have been in use since 82 CE, and there is evidence of tofu consumption that dates to 220.[agriculture corpus]
;हालांकि,82 CE से  "सोया दूध" के जैसे  शब्द उपयोग किए गये है/उपयोग  में  लाए गये है  और जो   220 से  टोफू(tofu) खपत का साक्ष्य है .  [manual]
(defrule since8
(declare (salience 4500))
(id-root ?id since)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-since_saMbanXI ?id1 ?id2)
(id-root ?id1 be)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp     since8   "  ?id "  se )" crlf))
)

;Commented by 14anu-ban-01 on (05-02-2015) because required meaning is coming from since7.Merged with since7
;@@@ Added by 14anu-ban-11 on (01-12-2014)
;Since a large number of electrons absorb energy, the energy absorbed per electron per unit time turns out to be small. (Ncert)
;चूँकि एक बडी सङ्ख्या में इलेक्ट्रॉन ऊर्जा अवशोषित करते हैं, अतः प्रति इलेक्ट्रॉन प्रति इकाई समय में अवशोषित ऊर्जा बहुत कम होगी.(Ncert)
;(defrule since9
;(declare (salience 4800))
;(id-root ?id since)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-vAkya_viBakwi  ?id1 ?id)
;(id-root ?id1 absorb)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id cUzki))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  since.clp 	since9  "  ?id "  cUzki )" crlf))
;)

;@@@ Added by 14anu-ban-01 on (07-02-2015)
;Xi said that since his visit to India in September last year, relations between the two countries have entered into a new stage. [Mailed by Soma Mam]
;Xi ने कहा कि  उनके पिछले वर्ष के सितम्बर में  भारत के दौरे के बाद से ,दोनों  देशों के बीच सम्बन्ध एक नये स्तर पर पहुँचे है.[self: 14anu-ban-01 and 14anu-ban-07]
(defrule since10
(declare (salience 4500))
(id-root ?id since)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 visit|stay)
(kriyA-since_saMbanXI ? ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAxa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  since.clp     since10   "  ?id "  ke_bAxa_se)" crlf))
)
