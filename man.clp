(defrule man0
(declare (salience 500)); reduced sailence by 14anu20 on 23/06/2014
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxamI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  man.clp 	man0   "  ?id "  AxamI )" crlf))
)

;"man","N","1.AxamI"
;--"2.mAnava"
;All men are born free.
;--"3.nara"
;He is a self made man.
;--"4.sEnika"
;All officers && men in the Army are  brave.
;--"5.nOkara"
;My man will drive you home.
;
(defrule man1
(declare (salience 4900))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxamI_kAma_para_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  man.clp 	man1   "  ?id "  AxamI_kAma_para_ho )" crlf))
)

;"man","V","1.AxamI_kAma_para_honA[lagAnA]"
;The railway crossing at that point was not manned.
;

;@@@ Added by 14anu20 on 23/06/2014.
;He was a trade union man.
;वह एक कर्मचारी सङ्गठन मे कार्यकर्ता था . 
(defrule man2
(declare (salience 5900))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) union|establishment|company)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id me_kAryakarwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  man.clp 	man2   "  ?id "  me_kAryakarwA )" crlf))
)

;@@@ Added by 14anu20 on 23/06/2014
;He was more of a man than you.
;वह आपकी अपेक्षा अधिक साहसी 
(defrule man3
(declare (salience 900))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-word ?id1 more)
(id-word =(- ?id 1) a)
(id-word =(- ?id 2) of)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2)  sAhasI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " man.clp  man3  "  ?id "  " (- ?id 1) "  " (- ?id 2) "   sAhasI  )" crlf))
)

;@@@ Added by 14anu20 
(defrule man4
(declare (salience 900))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) one)
(kriyA-as_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2)  eka_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " man.clp  man4  "  ?id "  " (- ?id 1) "  " (- ?id 2) "   eka_sAWa  )" crlf))
)


;@@@ Added by 14anu20 on 24/06/2014.
;Damage is done by man to the environment .
;हानि पर्यावरण में मानव के द्वारा की गयी है . 
(defrule man5
(declare (salience 4500))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) by)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  man.clp 	man5   "  ?id "  mAnava )" crlf))
)


;@@@ Added by 14anu20 on 24/06/2014
;He is a man of public affair.
;वह सार्वजनिक कार्य में शामिल है . 
(defrule man6
(declare (salience 4500))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 affair)
(id-root =(+ ?id 1) of)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1)  meM_SAmila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " man.clp  man6  "  ?id "  " (+ ?id 1) "   meM_SAmila  )" crlf))
)

;@@@ Added by 14anu20 on 24/06/2014
;He is a man of means.
;
(defrule man7
(declare (salience 4500))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 2) means)
(id-root =(+ ?id 1) of)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2)  XanI_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " man.clp  man7  "  ?id "  " (+ ?id 1) "  " (+ ?id 2) "   XanI_vyakwi  )" crlf))
)

;@@@ Added by 14anu20 on 24/06/2014.
;He is a man of letter.
;वह विद्वान व्यक्ति है . 
(defrule man8
(declare (salience 4500))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 2) letter)
(id-root =(+ ?id 1) of)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2)  vixvAna_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " man.clp  man8  "  ?id "  " (+ ?id 1) "  " (+ ?id 2) "   vixvAna_vyakwi  )" crlf))
)

;@@@ Added by 14anu20 on 27/06/2014.
;The two of them lived as man and wife.
;उनमें से दो पति और पत्नी की तरह रहते थे 
(defrule man9
(declare (salience 4500))
(id-root ?id man)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 2) wife)
(kriyA-as_saMbanXI  ? =(+ ?id 2))
(id-root =(+ ?id 1) and)
(kriyA-as_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  man.clp 	man9   "  ?id "  pawi )" crlf))
)


