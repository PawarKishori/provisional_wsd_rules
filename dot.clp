;@@@ Added by 14anu-ban-04 (31-01-2015)
;The taxi showed up on the dot.             [olad]
;टैक्सी सही समय पर  पहुँची.                          [self]
(defrule dot2
(declare (salience 5010))
(id-root ?id dot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahI_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dot.clp 	dot2   "  ?id "    sahI_samaya )" crlf))
)

;@@@ Added by 14anu-ban-04 (31-01-2015)
;Please tell him I’ll call him on the dot of twelve.                  [oald]
;कृपया उसको बताइए कि  ठीक बारह  बजे मैं उसे  मिलने बुलाऊँगा.                           [self]
(defrule dot3
(declare (salience 5020))
(id-root ?id dot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI ?kri ?id)
(pada_info (group_head_id ?id)(preposition ?id2))
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id1 number)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 TIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dot.clp   dot3  "  ?id "  " ?id2 "   TIka )" crlf))
)

;@@@ Added by 14anu-ban-04 (31-01-2015)
;The plane landed at two o'clock on the dot.                    [cald]
;विमान  ठीक  दो बजे उतरा .                                            [self]
(defrule dot4
(declare (salience 5020))
(id-root ?id dot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI ?kri ?id)
(pada_info (group_head_id ?id)(preposition ?id2))
(kriyA-at_saMbanXI ?kri ?id1)
(id-cat_coarse ?id1 number)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 TIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dot.clp   dot4  "  ?id "  " ?id2 "   TIka )" crlf))
)

;---------------------- Default Rules -------------------

(defrule dot0
(declare (salience 5000))
(id-root ?id dot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dot.clp 	dot0   "  ?id "  binxu )" crlf))
)

;"dot","N","1.binxu"
;Dots are important in a graphor geometry
;Join up the dots to complete the drawing 
;A dot is used as a full stop at the end of a sentance
;The island was just a dot in the occan
;
(defrule dot1
(declare (salience 4900))
(id-root ?id dot)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binxu_se_cihniwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dot.clp 	dot1   "  ?id "  binxu_se_cihniwa_kara )" crlf))
)

;"dot","V","1.binxu_se_cihniwa_karanA"
;The students wre asked to dot the cities in the map.
;--"2.jagaha-jagaha_PElAnA"
;The sky was dotted with stars on the dot - le; ij 
;He's very punctual always arrive on the dot
;
