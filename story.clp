;Example added by 14anu-ban-01 on (12-01-2015) from story2
;But as each of these stories progressed , the stereotype returned .
;लेकिन इन कहानियों का आगे बढेना था कि फिर सास का वही पुराना रूप सामने आने लगा .
(defrule story0
(declare (salience 5000))
(id-root ?id story)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  story.clp 	story0   "  ?id "  kahAnI )" crlf))
)

;Commented by 14anu-ban-01 on (12-01-2015) as required meaning is coming from default rule story0
;But as each of these stories progressed , the stereotype returned .
;लेकिन इन कहानियों का आगे बढेना था कि फिर सास का वही पुराना रूप सामने आने लगा .
;@@@ Added by 14anu11
;(defrule story2
;(declare (salience 5000))
;(id-root ?id story)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-det_viSeRaNa  ?id ?id2)
;(viSeRya-of_saMbanXI  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kahAniyoM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  story.clp 	story2   "  ?id "  kahAniyoM )" crlf))
;)

(defrule story1
(declare (salience 4900))
(id-root ?id story)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 make )
(viSeRya-up_saMbanXI ?id1 ?id) ;Replaced viSeRya-up_viSeRaNa as viSeRya-up_saMbanXI programatically by Roja 09-11-13
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  story.clp 	story1   "  ?id "  kahAnI )" crlf))
)

;"story","N","1.kahAnI"
;My father used to tell me bed-time stories.
;--"2.maMjZila"
;The building has ten stories.
;
;
