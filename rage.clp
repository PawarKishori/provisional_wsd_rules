
;"raging","Adj","1.bahuwa_jora_kA"
;She has a raging headache.  
(defrule rage0
(declare (salience 5000))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id raging )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bahuwa_jora_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  rage.clp  	rage0   "  ?id "  bahuwa_jora_kA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (24-02-2015)
;She fell into a rage and refused to answer.[hinkhoj]
;उसने रोष व्यक्त करा और उत्तर देने के लिए मना कर दिया . [manual]
(defrule rage3
(declare (salience 5100))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roRa_vyakwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage3   "  ?id "  roRa )" crlf))
)
;@@@ Added by 14anu-ban-10 on (24-02-2015)
;The storms rage continued.[hinkhoj]
;प्रकृति का उग्र रूप आँधी जारी रही.[manual]
(defrule rage4
(declare (salience 5200))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakQwi_kA_ugra_rUpa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage4   "  ?id "   prakQwi_kA_ugra_rUpa )" crlf))
)
;@@@ Added by 14anu-ban-10 on (24-02-2015)
;His son has rage for collecting stamps.[hinkhoj]
;उसके बेटे की प्रबल इच्छा है टिकटों को इकट्ठा  करने की  . [manual]
(defrule rage5
(declare (salience 5300))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prabala_icCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage5   "  ?id "   prabala_icCA )" crlf))
)
;@@@ Added by 14anu-ban-10 on (24-02-2015)
;An epidemic is raging throughout the country.[hinkhoj]
;महामारी देश भर में  जोर पकड़ रही है . [manual]
(defrule rage6
(declare (salience 5400))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1 )
(id-root ?id1 epidemic)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_pakadaZ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage6   "  ?id "   jora_pakadaZ)" crlf))
)
;@@@ Added by 14anu-ban-10 on (24-02-2015)
;She was trembling with rage.[hinkhoj]
;वह  क्रोध से काँप रही थी . [manual]
(defrule rage7
(declare (salience 5500))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?  ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kroX))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage7   "  ?id "  kroX)" crlf))
)

;$$$ Modified by 14anu-ban-06 (24-04-2015)
;@@@ Added by 14anu-ban-10 on (24-02-2015)
;That's unfair!’ she raged. [oald]
;वह अन्यायी है!उसने  ज़ोर से चिलाया. [manual]
;वह अनुचित है! 'वह जोर से चिल्लाई . (manual);added by 14anu-ban-06 (24-04-2015)
(defrule rage8
(declare (salience 5600))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se_cillA));meaning changed from 'jora_se_cilAyA' to 'jora_se_cillA' by 14anu-ban-06 (24-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage8   "  ?id "  jora_se_cillA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (24-02-2015)
;Forest fires were raging out of control.[oald]
;जंगल कि आग तेज़ी से फैल कर बेक़ाबू  हो रही थी.[manual] 
(defrule rage9
(declare (salience 5700))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1 )
(id-root ?id1 fire)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_PEla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage9   "  ?id "  wejI_se_PEla)" crlf))
)

;------------------------ Default Rules ----------------------

(defrule rage1
(declare (salience 4900))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage1   "  ?id "  roRa )" crlf))
)

;"rage","VI","1.JuMJalAnA"
;He raged against me for disobeying him.
(defrule rage2
(declare (salience 4800))
(id-root ?id rage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JuMJalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rage.clp 	rage2   "  ?id "  JuMJalA )" crlf))
)

;"rage","VI","1.JuMJalAnA"
;He raged against me for disobeying him.
;--"2.jora_pakadZanA"    
;An epidemic is raging throughout the country.
;

