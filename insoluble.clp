;$$$ Modified by 14anu-ban-06 (11-11-2014)
;The issue is insoluble.(insoluble.clp)
;समस्या असमाधेय  हैं|(manual)
;Rights comparisons resemble religious quarrels in their regrettable tendency toward insoluble emotional conflict.(COCA)
;राइट्स मुक़ाबला उनके अफसोस की प्रवृत्ति में  असमाधेय भावनात्मक संघर्ष की ओर  धार्मिक झगड़े जैसे सदृश होता हैं.(manual)
(defrule insoluble0
(declare (salience 0));salience reduced to 0 (default rule)
(id-root ?id insoluble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asamAXeya))  ;meaning changed from 'jisakA_samAXAna_na_hosake' to  'asamAXeya' ;by 14anu-ban-06 (11-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insoluble.clp 	insoluble0   "  ?id "  asamAXeya )" crlf))
)

;$$$ Modified by 14anu-ban-06 (11-11-2014)
;Cholesterol is insoluble in water.(insoluble.clp)
;कोलेस्टेरोल पानी में अघुलनशील होता हैं|(manual)
;A chemical compound that is insoluble in pure water.(COCA)
;एक रासायनिक पदार्थ जो शुद्ध पानी में अघुलनशील है . (manual)
(defrule insoluble1
(declare (salience 4900))
(id-root ?id insoluble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-in_saMbanXI ?id ?);added by 14anu-ban-06 (11-11-2014) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aGulanaSIla));meaning changed from 'na_GulanevAlA' to 'aGulanaSIla'  ;by 14anu-ban-06 (11-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insoluble.clp 	insoluble1   "  ?id "  aGulanaSIla )" crlf))
)

;"insoluble","Adj","1.na_GulanevAlA"
;Cholesterol is insoluble in water.
;--"2.asamAXAnIya"
;The issue is insoluble.
;
;

;@@@ Added by 14anu-ban-06 (11-11-2014)
;Insoluble collagen increases with age leading to rigidity and general body stiffness .(parallel corpus)
;अघुलनशील कोलेजन की मात्रा आयु के साथ बढ़ती जाती है जिसके परिणामस्वरूप शरीर कठोर हो जाता है और उसमें कड़ापन आ जाता है .(parallel corpus)
;Insoluble fiber produces less gas .(parallel corpus)
;अघुलनशील फाइबर कम गैस उत्पन्न करता है ।(parallel corpus)
;On the other hand insoluble fiber passes through the intestines without any change and produces less gas .(parallel corpus)
;दूसरी तरफ अघुलनशील फाइबर आँतों से बिना किसी परिवर्तन के गुजरता है तथा कम गैस उत्पन्न करता है ।(parallel corpus)
;The insoluble carbohydrates in soybeans consist of the complex polysaccharides cellulose, hemicellulose, and pectin. (agriculture)
;सोयाबीनों में अघुलनशील कार्बोहाइड्रेट जटिल पॉलीसैकराइड्ज सेलूलोज, हेमीसेलूलोज, और पेक्टन से बने होते हैं . (manual)
(defrule insoluble2
(declare (salience 4900))
(id-root ?id insoluble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 collagen|fiber|carbohydrate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aGulanaSIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insoluble.clp 	insoluble2   "  ?id "  aGulanaSIla )" crlf))
)
