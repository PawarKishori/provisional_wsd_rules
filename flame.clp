
;@@@ Added by 14anu-ban-05 on (25-03-2015)
;Flames leaped from the burning house. [OALD]
;जलते हुये घर से लपटें निकलीं.		[manual]
(defrule flame3
(declare (salience 4901))
(id-root ?id flame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 leap)          ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lapata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flame.clp 	flame3   "  ?id "  lapata )" crlf))
)


;@@@ Added by 14anu-ban-05 on (25-03-2015)
;She felt a flame of anger flicker and grow. [OALD] 
;उसने क्रोध के आवेग को झलकता और बढ़ता हुआ  महसूस किया. [manual]   ;run on parser 3 of multiparse
(defrule flame4
(declare (salience 4902))
(id-root ?id flame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 anger|passion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Avega))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flame.clp 	flame4   "  ?id "  Avega )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-03-2015)
;The helicopter burst into flames.[OALD]
;हेलीकाप्टर आग की लपटों में फट गया. [manual]
(defrule flame5
(declare (salience 4903))
(id-root ?id flame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-into_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Aga_kI_lapata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flame.clp 	flame5   "  ?id "  Aga_kI_lapata )" crlf))
)

;----------------------------------------default rules--------------------------------------------

;"flaming","Adj","1.Aga_sI"
;The curry was flaming hot.
(defrule flame0
(declare (salience 5000))
(id-root ?id flame)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id flaming )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Aga_sI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  flame.clp  	flame0   "  ?id "  Aga_sI )" crlf))
)

;$$$ Modified by Shirisha Manju on 23-02-2015 Suggested by Chaitanya Sir
;"flame","N","1.lapata"
;The whole building was in flames before the fire brigade arrived on the scene.
(defrule flame1
(declare (salience 4900))
(id-root ?id flame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jvAlA)); changed meaning 'lapata' as 'jvAlA' by Shirisha Manju on 23-02-2015
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flame.clp 	flame1   "  ?id "  jvAlA )" crlf))
)

;"flame","V","1.Aga_kI_jvAlA"
;The flames of the fire were seen from a long distance.
(defrule flame2
(declare (salience 4800))
(id-root ?id flame)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Aga_kI_jvAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flame.clp 	flame2   "  ?id "  Aga_kI_jvAlA )" crlf))
)
;---------------------------------------------------------------------------



;"flame","V","1.Aga_kI_jvAlA"
;The flames of the fire were seen from a long distance.
;--"2.Aga_kA_raMga"
;Due to fire the whole area was glowing in flame colour.
;--"3.kroXiwa_honA"
;His hatered flamed anew on strong criticism from his collegues.
;
;"flame","N","1.lapata"
;The whole building was in flames before the fire brigade arrived on the scene.
;--"2.BadZakIlA_raMga"
;All the flowering bushes were in full bloom turning the whole area in scarlet flames.
;--"3.wIvra_cewana"
;His love letter kindled the flame of passion in        her.
;--"4.premI"
;At the party he met with his old flame.
;

