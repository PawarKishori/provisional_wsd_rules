
;@@@ Added by 14anu-ban-06 (8-08-14)
;Wires of incense sticks are hanging from it ' s roof . (tourism corpus)
;इसकी  छत  से  अगरबत्ती  के  तार  लटके  हुए  हैं  ।
;It is believed that with the smoke of incense sticks the prayers of devotees also reach the heavenly spirits .(tourism corpus)
;ऐसी  मान्यता  है  कि  अगरबत्ती  के  धुएँ  के  साथ  भक्तों  की  प्रार्थना  भी  स्वर्गीय  आत्माओं  तक  पहुँच  जाती  है  । 
;By the smoke of the burning scented incense sticks in  the temple snakes are seen in unconscious state .(tourism corpus)
;मंदिर  में  जलने  वाली  सुगंधित  अगरबत्तियों  के  धुएँ  से  साँप  मूर्छित  अवस्था  में  नजर  आते  हैं  ।
(defrule incense2
(declare (salience 5100))
(id-root ?id incense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) stick)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) agarabawI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  incense.clp  incense2   "  ?id "    " =(+ ?id 1) " agarabawI)" crlf))
)

;--------------------- Default rules ----------------
(defrule incense0
(declare (salience 5000))
(id-root ?id incense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sugaMXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incense.clp 	incense0   "  ?id "  sugaMXa )" crlf))
)

;"incense","N","1.sugaMXa"
;I like the incence of the sandal.
;
(defrule incense1
(declare (salience 4900))
(id-root ?id incense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Aga_babUlA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incense.clp 	incense1   "  ?id "  Aga_babUlA_kara )" crlf))
)

;"incense","V","1.Aga_babUlA_karanA"
;--"2.nArAjZa_karanA/gussA_xilAnA"
;She was incensed by his abusive words.
;
