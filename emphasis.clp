;@@@ Added by 14anu-ban-04 (28-03-2015)
;Our company puts the emphasis on quality.               [oald]
;हमारी कम्पनी गुणवत्ता को महत्व देती हैं.                              [self]
(defrule emphasis1       
(declare (salience 20))
(id-root ?id emphasis)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 put|lay|place) 
(viSeRya-on_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  emphasis.clp 	emphasis1   "  ?id "  mahawva)" crlf))
)

;------------------------------------------------------DEFAULT RULE -----------------------------------------------------------------------


;@@@ Added by 14anu-ban-04 (28-03-2015)
;She placed the emphasis on the word 'soon'.                       [cald]
;उसने 'शीघ्र' शब्द पर  जोर दिया.                                           [self]
;I can assure you,’ she added with emphasis, ‘the figures are correct.’             [oald]
;'उसने जोर से  कहा ' मैं आपको आश्वासन दे सकता हूँ, कि  आँकड़े सही हैं .                                [self]
(defrule emphasis0       
(declare (salience 10))
(id-root ?id emphasis)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  emphasis.clp 	emphasis0  "  ?id "  jora)" crlf))
)


