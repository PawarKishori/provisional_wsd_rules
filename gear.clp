
(defrule gear0
(declare (salience 5000))
(id-root ?id gear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyArI_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " gear.clp gear0 " ?id "  wEyArI_kara )" crlf)) 
)

(defrule gear1
(declare (salience 4900))
(id-root ?id gear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wEyArI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gear.clp	gear1  "  ?id "  " ?id1 "  wEyArI_kara  )" crlf))
)

(defrule gear2
(declare (salience 4800))
(id-root ?id gear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id giyara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gear.clp 	gear2   "  ?id "  giyara )" crlf))
)

;default_sense && category=noun	sAmAna	0
;"gear","N","1.sAmAna"
;I carry all my sports gear in a kitbag.
;--"2.gawi"
;The Congress party seems to be moving into top gear.
;He drives in second gear.
;
;

;@@@ Added by 14anu-ban-05 on (13-03-2015)
;The course had been geared towards the specific needs of its members. [OALD]
;पाठ्यक्रम को उसके सदस्यों की विशिष्ट आवश्यकताओं के लिये तैयार किया गया था।	[manual]

(defrule gear3
(declare (salience 5001))
(id-root ?id gear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-towards_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gear.clp 	gear3   "  ?id "  wEyAra_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (13-03-2015)
;Our training programmes are geared specifically to the needs of older workers.[OALD]
;हमारे प्रशिक्षण कार्यक्रम पुराने कार्यकर्ताओं की जरूरतों के लिए विशेष रूप से तैयार किया जा रहा है.		[MANUAL]

(defrule gear4
(declare (salience 5001))
(id-root ?id gear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(kriyA-to_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gear.clp 	gear4   "  ?id "  wEyAra_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (13-03-2015)
;The Congress party seems to be moving into top gear. [from gear.clp]
;काङ्ग्रेस पार्टी तेज गति से आगे बढती हुई प्रतीत होती है .		[manual]

(defrule gear5
(declare (salience 5002))
(id-root ?id gear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1) top)
(kriyA-into_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) weja_gawi))
(assert  (id-wsd_viBakwi   ?id  se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gear.clp  gear5  "  ?id "  " (- ?id 1) "  weja_gawi  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  gear.clp  gear5  "  ?id " se )" crlf))
)


