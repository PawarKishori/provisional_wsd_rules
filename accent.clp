;@@@Added by 14anu-ban-02(04-03-2015)
;In all our products the accent is on quality.[oald]
;हमारे सभी उत्पादों में विशेष ध्यान गुणवत्ता पर है . [self]
(defrule accent2
(declare (salience 100))
(id-root ?id accent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id)
(kriyA-on_saMbanXI  ?id1 ?id2)
(id-root ?id2 quality)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSeRa_XyAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accent.clp 	accent2   "  ?id "  viSeRa_XyAna )" crlf))
)

;@@@Added by 14anu-ban-02(04-03-2015)
;He tried to disguise his accent.[oald]
;उसने अपना घाव छिपाने का प्रयास किया . [self]
(defrule accent3
(declare (salience 100))
(id-root ?id accent)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(kriyA-kriyArWa_kriyA  ?id2 ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accent.clp 	accent3   "  ?id "  GAva )" crlf))
)

;------------------------ Default Rules ----------------------

;"accent","N","1.svara_kA_caDZAva_uwAra"
;I the word `remain',the accent is on the second syllable.
(defrule accent0
(declare (salience 0))	;salience reduced from 5000 to 0 by 14anu-ban-02(04-03-2015)
(id-root ?id accent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svara_kA_caDZAva_uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accent.clp 	accent0   "  ?id "  svara_kA_caDZAva_uwAra )" crlf))
)

;"accent","V","1.svara_uccAraNa_karanA"
;His English was slow && heavily accented.
(defrule accent1
(declare (salience 4900))
(id-root ?id accent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svara_uccAraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accent.clp 	accent1   "  ?id "  svara_uccAraNa_kara )" crlf))
)

