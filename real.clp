;$$$ Modified by 14anu-ban-10 on (04-08-2014) 
;Even if there is no real adventure people do not lose any chance to enjoy the adventure through mobile , computer and video game .[tourism corpus]
;चाहे वास्तविक हो या वर्चुअल , एडवेंचर के मौकों की कोई कमी भी नहीं है ।[tourism corpus]
;It is evident from this definition of a rigid body that no real body is truly rigid, since real bodies deform under the influence of forces.
;xqDa piNda kI isa pariBARA se yaha spaRta hE ki koI BI vAswavika piNda pUrI waraha xqDa nahIM howA, kyofki saBI vyAvahArika piNda baloM ke praBAva se vikqwa ho jAwe hEM.
;@@@ Added by Anita --20.2.2014
;A lot of these chemicals pose very real threats to our health. [By mail]
;बहुत सारे ये रसायनिक द्रव्य हमारे स्वास्थ्य के लिए वास्तविक खतरे उत्पन्न करते हैं ।
(defrule real2
(declare (salience 5300))
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 threat|body|adventure) ; added ' body' by 14anu-ban-10 on (04-08-2014) ;added adventure by 14anu-ban-10 on (05-02-2015)
;(kriyA-object  ? ?id1) ; commented by 14anu-ban-10 on (04-08-2014)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAswavika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real2   "  ?id "  vAswavika )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (15-12-2014) 
;@@@ Added by Anita--20.2.2014
;The excursion trip was a real disaster. --[old clp sentence] [using 6 no. parser]
;भ्रमण यात्रा पूर्णतः तबाही थी । 
(defrule real3
(declare (salience 5150))
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id) ;added ?id1' by 14anu-ban-10 on (15-12-2014)
(id-root ?id1 disaster) ;added by 14anu-ban-10 on (15-12-2014)
(id-cat_coarse ?id adjective) ;modified category adverb as adjective by 14anu-ban-10 on (15-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNawaH))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real3   "  ?id "  pUrNawaH )" crlf))
)
;$$$ Modified by 14anu-ban-10 on (15-12-2014)
;@@@ Added by Anita--28.2.2014
;She is a real class performer. [Gyan-Nidhi]
;वह विशिष्ट श्रेणी की  कलाकार है । 
(defrule real4
(declare (salience 5250))
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)  ;added ?id1' by 14anu-ban-10 on (15-12-2014)
(id-root ?id1 performer) ;added by 14anu-ban-10 on (15-12-2014)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real4   "  ?id "  viSiRta )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (11-08-2014)
;It was a real sod of a job.[oald]
;यह काम की एक  असली दिक्कत थी . [manual]
;The poor masses are left searching for blessing and redemption quite far from the 'real Sangam' at the shores of Ganga or Yamuna .
;The real thing is that the contraceptive pills should be taken at the regular time only then they become effective .
;The real beauty of Haflong place with blue hills , greenery and orange and full of pineapple gardens comes to sight in the months of October to February .
;Especially for adopted children it has already been proved that there is much closeness in their weight and the weight of their real parents in comparison to the weight of their adopted parents .
;@@@ Added by Anita--11.3.2014
;The Commission recommended the creation of a real teaching University in Calcutta. [gyanidhi ;sentence]
;आयोग ने कलकत्ता में एक असली अध्यापन विश्वविद्यालय बनाने की सलाह दी।
(defrule real5
(declare (salience 5900)) ;salience increased
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 university|college|school|Sangam|thing|beauty|parent|sod); added 'Sangam|thing|beauty|parent' by 14anu-ban-10 on (11-08-2014);sod added by 14anu-ban-10 on (25-02-2015)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real5   "  ?id "  asalI )" crlf))
)

;@@@ Added by Anita--17-06-2014
;It is evident from this definition of a rigid body that no real body is truly rigid since real bodies deform under the influence ;of forces. [ncert.]
;दृढ पिण्ड की इस परिभाषा से यह स्पष्ट है कि कोई भी वास्तविक पिण्ड पूरी तरह दृढ नहीं होता, क्योङ्कि सभी व्यावहारिक पिण्ड बलों के प्रभाव से विकृत हो जाते हैं.
;In good many situations in real life the size of objects can be neglected and they can be considered as pointlike objects ;without much error. [ncert] --Add sentence --25.6.2014
;वास्तविक जीवन में बहुत - सी स्थितियों में वस्तुओं के आमाप (साइज) की उपेक्षा की जा सकती है और बिना अधिक त्रुटि के उन्हें एक बिन्दु - वस्तु माना जा सकता है ।
(defrule real06
(declare (salience 5500))
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 body|life) ; Add 'life' word by Anita-25.6.2014
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAswavika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real06   "  ?id "  vAswavika )" crlf))
)


;$$$ Modified by 14anu-ban-10 on (15-12-2014)
;@@@ Added by 14anu11
;For , a devotee ' s real love of God is tested not in public , but in privacy , where he is left alone with God .
;भक्त का ईश्वर के प्रति  सच्चा प्रेम समूह में नहीं परखा जाता . एकांत में ही उसकी परख है जब भक्त भगवान के पास अकेला होता है .
(defrule real6
(declare (salience 6250))
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 love) ;added by 14anu-ban-10 on (15-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saccA)) ;meaning changed from scacA to saccA by 14anu-ban-10 on (15-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real6   "  ?id "  saccA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (07-02-2015)
;Whether real or virtual , there is no dearth of opportunities for adventure either .[tourism corpus]
;वास्तविक एडवेंचर यदि न हो तो भी मोबाइल , कंप्यूटर और वीडियो गेम के माध्यम से एडवेंचर का लुत्फ़ उठाने से लोग नहीं चूकते ।[tourism corpus]
(defrule real7
(declare (salience 6300))
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viBakwi  ?id ? )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAswavika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real7   "  ?id "  vAswavika )" crlf))
)

;####################################default-rule################################
;"real","Adj","1.sawya"
;Are these facts real?       [old clp. sentence]
;क्या ये तथ्य सत्य हैं ?
(defrule real0
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sawya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real0   "  ?id "  sawya )" crlf))
)

;"real","Adv","1.yaWArWa_meM"
;He had a real good laugh after a long time.
(defrule real1
(id-root ?id real)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaWArWa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  real.clp 	real1   "  ?id "  yaWArWa_meM )" crlf))
)




;"real","Adj","1.sawya"
;Are these facts real? 
;--"2.vAswavika"
;He is the real manager of the institution.
;--"3.pUrNawaH"
;The excursion trip was a real disaster 
;--"4.AmaxanI/sampawwi"
;


;Are these facts real?
;hEM ye waWya asalI?
;He is the real manager of the institution.
;vaha hE asalI/vAswava_meM prabaMXaka saMsWA_kA
;The excursion trip was a real disaster.
;paryatana yAwrA WI vAswava_meM Gora_vipawwi
;He had a real good laugh after a long time.
;eka laMbe kAla ke bAxa usake WI asalI acCI hazsI.
;
;
;sUtra: asalI^vAswava_meM

