;@@@ Added by  14anu-ban-01 on (13-04-2015)
;Her skull was crammed with too many thoughts.[oald]
;उसका मस्तिष्क बहुत अधिक विचारों से भर गया था . [self]
(defrule skull1
(declare (salience 1000))
(id-root ?id skull)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-karma  ?id1 ?id)
(id-root ?id2 thought|idea)
(kriyA-with_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maswiRka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  skull.clp 	skull1  "  ?id "  maswiRka)" crlf))
)


;@@@ Added by  14anu-ban-01 on (13-04-2015)
;When will he get it into his thick skull that I never want to see him again![oald]
;वह अपने मोटे दिमाग के अन्दर इस [बात] को कब डालेगा कि मैं  उससे फिर कभी नहीं मिलना चाहता ! [self]
(defrule skull2
(declare (salience 1000))
(id-root ?id skull)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 thick)
(viSeRya-viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ximAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  skull.clp 	skull2  "  ?id "  ximAga)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by  14anu-ban-01 on (13-04-2015)
;A fractured skull.[oald]
;एक टूटी हुई खोपड़ी. [self]
(defrule skull0
(declare (salience 0))
(id-root ?id skull)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KopadI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  skull.clp    skull0   "  ?id "  KopadI )" crlf))
)

