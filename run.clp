
(defrule run0
(declare (salience 5000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id running )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lagAwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  run.clp  	run0   "  ?id "  lagAwAra )" crlf))
)

;"running","Adj","1.lagAwAra/eka_ke_bAxa_eka"
;Martina hingis won the world cup championship Three times running.
;
(defrule run1
(declare (salience 4900))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 after)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pICA_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run1  "  ?id "  " ?id1 "  pICA_kara  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  run.clp       run1   "  ?id "  kA)" crlf)
)

(defrule run2
(declare (salience 4800))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 after)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pICA_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run2  "  ?id "  " ?id1 "  pICA_kara  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  run.clp       run2   "  ?id " kA )" crlf)
)

;$$$ Modified by 14anu-ban-01 on (14-04-2016): restricted the rule to exclude 'running <against> the wind'
;The last part of the course was hard because I was running against the wind.	[sd_verified]
;दौडने_के_लिये_बने मार्ग का आखिरी भाग कठिन था क्योँकि मैं हवा के विरुद्ध दौड रहा था.	[sd_verified]
(defrule run3
(declare (salience 2600)) ; Salience reduced from 4700 to 2600 by Roja 08-11-13. Ex: The last part of the course was hard because I was running against the wind. (Note: this rule need to be improved)
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 against)	;commented by 14anu-ban-01
(kriyA-against_saMbanXI ?id ?id1) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. ;changed "?" to "?id1"
(id-root ?id1 ?root&~wind)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run3 " ?id "  viroXa_kara )" crlf)) 
)

(defrule run4
(declare (salience 4600))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 against)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run4  "  ?id "  " ?id1 "  viroXa_kara  )" crlf))
)

(defrule run5
(declare (salience 4500))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-around_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iXara_uXara_BAga));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run5 " ?id "  iXara_uXara_BAga )" crlf)) 
)

(defrule run6
(declare (salience 4400))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 iXara_uXara_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run6  "  ?id "  " ?id1 "  iXara_uXara_BAga  )" crlf))
)

(defrule run7
(declare (salience 4300))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-away_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAga_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run7 " ?id "  BAga_jA )" crlf)) 
)

;$$$ Modified by Shirisha Manju --Suggested by Sukhada (28-06-14) - Replaced meaning 'BAga_jA' with ' BAga'
;He is running away to ensure he is not caught.
;वह पकडा नही जाने के लिये दुर भाग राहा है
(defrule run8
(declare (salience 4200))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BAga)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run8  "  ?id "  " ?id1 "  BAga  )" crlf))
)

(defrule run9
(declare (salience 4100))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-down_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gira_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run9 " ?id "  gira_jA )" crlf)) 
)

(defrule run10
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 gira_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run10  "  ?id "  " ?id1 "  gira_jA  )" crlf))
)

(defrule run11
(declare (salience 3900))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-into_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takarA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run11 " ?id "  takarA )" crlf)) 
)

(defrule run12
(declare (salience 3800))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 takarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run12  "  ?id "  " ?id1 "  takarA  )" crlf))
)

(defrule run13
(declare (salience 3700))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kawma_ho));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run13 " ?id "  Kawma_ho )" crlf)) 
)

(defrule run14
(declare (salience 3600))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kawma_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run14  "  ?id "  " ?id1 "  Kawma_ho  )" crlf))
)

(defrule run15
(declare (salience 3500))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-over_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_Upara_se_nikala_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run15 " ?id "  ke_Upara_se_nikala_jA )" crlf)) 
)

(defrule run16
(declare (salience 3400))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ke_Upara_se_nikala_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run16  "  ?id "  " ?id1 "  ke_Upara_se_nikala_jA  )" crlf))
)

(defrule run17
(declare (salience 3300))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-through_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIGrawA_se_paDa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run17 " ?id "  SIGrawA_se_paDa )" crlf)) 
)

(defrule run18
(declare (salience 3200))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SIGrawA_se_paDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run18  "  ?id "  " ?id1 "  SIGrawA_se_paDa  )" crlf))
)

(defrule run19
(declare (salience 3100))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp 	run19   "  ?id "  xOda )" crlf))
)

(defrule run20
(declare (salience 3000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 after)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pICA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run20  "  ?id "  " ?id1 "  pICA_kara  )" crlf))
)

(defrule run21
(declare (salience 2900))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 water)
(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp 	run21   "  ?id "  baha )" crlf))
)

;$$$ Modified by Shruti Singh M.Tech(CS) Banasthali University (30-08-2016)
;Run the fan.
;पन्खा चलाइए . 
;$$$ Modified by 14anu-ban-01 on (20-04-2015):merged rule no. 22,029,31,35
;He runs his own TV production company.
;वह अपनी खुद की दूरदर्शन उत्पादन कम्पनी चलाता है .[run029]
;A woman runs this house and she maintained taste and credibility. [tourism corpus]
;swrI yaha Gara calAtI hE Ora usane svAxa Ora viSvasanIyawA kAyama raKI hEM.[run31]
;Some trading standards departments run consumer advice centres near main shopping areas.
;ट्रेडिंग स्टैंडर्डजऋ के कुछ विभाग मुख्य बाजऋआर के पास उपभोक्ता सलाह  केंद्र चलाते हैं .[run35]
;Could you run a hot bath for me?[run.clp]	;added by 14anu-ban-01 on (23-04-2015)
;क्या तुम मेरे लिये गरम पानी चला सकते हो? [self]
;$$$ Modified by 14anu07
(defrule run22
(declare (salience 4000))	;salience increased to 4000 from 2800
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 company|business|shop|house|arrow|bow|system|party|centre|bath|fan)	;list of all 4 rules combined by 14anu-ban-01 	;added "bath" by 14anu-ban-01 on (23-04-2015)
;(id-word ?id1 system|party);Added "party" to the list by 14anu07		;commented by 14anu-ban-01 
;Added "fan" by Shruti
(or(kriyA-object ?id ?id1) (viSeRya-det_viSeRaNa  ?id ?id1)(viSeRya-viSeRaNa  ?id ?id1)); (viSeRya-det_viSeRaNa|viSeRya-viSeRaNa) relations are added by Shruti
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp 	run22   "  ?id "  calA )" crlf))
)

;$$$ Modified by Anita--3.4.2014
;We walk, run and ride a bicycle. 
;hama calawe hEM, xOdawe hEM Ora sAikila savArI karawe hEM. [Anusaaraka output] [using 3rd parse]
;;हम चलते हैं, दौड़ते हैं और साईकिल चलाते हैं ।
(defrule run23
(declare (salience 2700))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ? ?id ?) ;Add relation by Anita-3.4.2014
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp 	run23   "  ?id "  xOda )" crlf))
)


(defrule run25
(declare (salience 3101))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUmanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run25   "  ?id "  GUmanA )" crlf))
)


;$$$ Modified by  14anu-ban-01 on (20-04-2015) 
;Indian cricket team won the game by 20 runs.[run.clp]
;भारतीय क्रिकेट टीम बीस रन से जीत गयी.[run.clp]
;@@@ Added by 14anu09 and kokila eflu [19-06-14]
;He scored 10 runs.
;usane 10 xOdeM banAI.
;उसने १० दौडें बनाईं.
;उसने १० रन बनाए.[Translated by 14anu-ban-01]
(defrule run027
(declare (salience 4000))
(id-root ?id run)
(viSeRya-saMKyA_viSeRaNa ?id ?)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rana ));as in cricket "runs";changed "xOda" to "rana" by  14anu-ban-01 on (20-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run027   "  ?id "  rana )" crlf))					;changed "xOda" to "rana" by  14anu-ban-01 on (20-04-2015)
)


;$$$ Modified by 14anu-ban-10 on (08-12-2014)
;@@@ Added by 14anu09 and kokila eflu[19-06-14]
;Run Anusaaraka.
;calA Anusaaraka.
;अनुसारका चलाओ.
(defrule run026
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) PropN)
(samAsa_viSeRya-samAsa_viSeRaNa =(+ ?id 1) ?id)
;(not(id-root =(+ ?id 1 )  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
(id-cat_coarse ?id adjective) ;modified 'noun' as 'adjective' by 14anu-ban-10 on (08-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run026   "  ?id "  calA )" crlf))
)

;@@@ Added by 14anu09 and kokila elfu
;They gave him a run for money.
;उन्होंने उसको कठिन चुनौती दी . 
(defrule run028
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI ?id ?id1)
(id-root ?id1 money|gold)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kaTina_cunOwI))
;(assert (viSeRya-RaRTI_viSeRaNa_id ?id  -)) something has to be done about "run for his money"
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run028  "  ?id "  " ?id1 "  kaTina_cunOwI  )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-viSeRya-RaRTI_viSeRaNa_id   " ?*prov_dir* "  run.clp    run28   "  ?id " - )" crlf)
)


;@@@ Added by Anita-9.12.2013
; We ran down to the beach and plunged into the sea.
;हम समुद्र तट की ओर  गए और समुद्र में कूद गए ।
(defrule run26
(declare (salience 4001))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 down)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run26   "  ?id " " ?id1 " jA )" crlf))
)
;@@@ Added by Anita-18.12.2013
;A high, earth embankment ran along the banks of the river to protect the adjacent areas from floods.
;बाढ़ से बचने के लिए नदी के आसपास के किनारे ऊंची मिट्टी के बनाए गए।     [For discussion ]
(defrule run27
(declare (salience 4100))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 along)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sAWa_sAWa_banA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run27   "  ?id " " ?id1 " sAWa_sAWa_banA_ho )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (22-01-2015)
;This stretch of ring railway is abutting to the ring road but one does not come to know when does the track running with road goes inside a deep gorge and in place of the noise of vehicles the chirping of birds begins .[tourism corpus]
;रिंग  रेलवे  का  यह  स्ट्रैच  रिंग  रोड  से  सटा  हुआ  है  लेकिन  सड़क  के  साथ  चलती  पटरी  कब  गहरी  खाई  में  चली  जाती  है  और  गाड़ियों  के  शोर  की  जगह  चिड़ियों  की  चहचहाहट  शुरू  हो  जाती  है  पता  ही  नहीं  चलता  ।[tourism corpus]
;@@@ Added by Anita--19.3.2014
;The report runs into five volumes and covers, practically the whole sphere of education.  [gyanidhi sentence]
;रिपोर्ट पांच खण्डों में चलती है और इसमें शिक्षा का लगभग सारा क्षेत्र शामिल है।
(defrule run28
(declare (salience 4200))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-into_saMbanXI  ?id ? )(viSeRya-kqxanwa_viSeRaNa ? ?id ));added (viSeRya-kqxanwa_viSeRaNa) by 14anu-ban-10 on (22-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run28   "  ?id " cala )" crlf))
)

;$$$ Modified by  14anu-ban-01 on (20-04-2015) 
;@@@ Added by 14anu26     [05-07-14]
;You will need experience to run a hotel.
;आपको होटल सञ्चालन करने के लिए अनुभव की जरूरत होगी .
;आपको होटल का सञ्चालन करने के लिए अनुभव की जरूरत होगी .[Translated by 14anu-ban-01]
(defrule run29
(declare (salience 4500))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 hotel|industry)	;removed 'company|business' by  14anu-ban-01
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saFcAlana_kara))	;changed "saMcAlana" to "saFcAlana" by 14anu-ban-01
(assert (kriyA_id-object_viBakwi ?id kA))	;added by 14anu-ban-01
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  run.clp       run29   "  ?id " kA )" crlf)				;added by 14anu-ban-01
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run29   "  ?id "  saFcAlana_kara )" crlf))			;changed "saMcAlana" to "saFcAlana" by 14anu-ban-01
)

;@@@ Added by Anita--19-03-2014
;A thread of self-pity runs through his autobiography. [By mail]
;आत्मदया का सूत्र उसकी आत्मकथा में समाया हुआ है ।
(defrule run029
(declare (salience 4300))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 autobiography)
(kriyA-through_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAyA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run029   "  ?id "  samAyA_ho )" crlf))
)

;

;Merged this rule in run22 by 14anu-ban-01 on (20-04-2015) 
;@@@ Added by 14anu05 GURLEEN BHAKNA on 30.06.14
;He runs his own TV production company.
;वह अपनी खुद की दूरदर्शन उत्पादन कम्पनी चलाता है .
;(defrule run029
;(declare (salience 4000))
;(id-root ?id run)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object ?id ?id1)
;(id-root ?id1 company|business|shop|house)	
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id calA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp 	run029   "  ?id "  calA )" crlf))
;)


;@@@ Added by 14anu-ban-06 Karanveer Kaur (24.7.14)
;The jeep had to be stopped but engine was kept running .(Parallel Corpus)
;जीप रोक देनी पड़ी लेकिन इंजन चालू रखा गया ।
;The engine was running.  (COCA)
;iMjana cAlU raKA WA.
;I remembered that my car engine was running.  (COCA)
;muJe yAxa hE ki merI gAdI kA iMjana cAlU raKA WA.
(defrule run30
(declare (salience 5100))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 engine)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAlU_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run30   "  ?id "  cAlU_raKa )" crlf))
)


;Merged this rule in run22 by 14anu-ban-01 on (20-04-2015) 
;@@@ Added by 14anu-ban-03 (25-7-2014)
;A woman runs this house and she maintained taste and credibility. [tourism corpus]
;swrI yaha Gara calAtI hE Ora usane svAxa Ora viSvasanIyawA kAyama raKI hEM.
;(defrule run31 
;(declare (salience 5000))
;(id-root ?id run)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object ?id ?id1)
;(id-root ?id1 house|arrow|bow )
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id cala))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run31   "  ?id "  cala )" crlf))
;)

;@@@ Added by 14anu-ban-06 (30-7-2014)
;In West Bengal after the prayers running for nine days on Dussehra there is a tradition of dispersing honourably the statues of Mother Durga in the ocean and the rivers.  [tourism corpus]
;पं. बंगाल में दशहरे पर नौ दिनों तक चलने वाली दुर्गा पूजा के पश्चात माँ दुर्गा की मूर्तियों को समुद्र तथा नदियों में आदर सहित विसर्जित करने की परम्परा है ।
(defrule run32
(declare (salience 5000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?id1)
(id-root ?id1 day|month|year)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run32   "  ?id "  cala )" crlf))
)

;@@@ Added by 14anu-ban-10 on (27-9-2014)
;The index i runs from 1 to n, where n is the total number of particles of the body. [ncert corpus]
;yahAz BI sUcakAfka @i kA mAna 1 se @n waka baxalawA hE, jahAz @n piNda ke kula kaNoM kI safKyA hE.
;यहाँ भी सूचकाङ्क i का मान 1 से n तक बदलता है, जहाँ n पिण्ड के कुल कणों की सङ्ख्या है.
(defrule run34
(declare (salience 5200))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI ?id ? )
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp 	run34   "  ?id "  baxala)" crlf))
)

;Merged this rule in run22 by 14anu-ban-01 on (20-04-2015) 
;@@@ Added by 14anu24
;Some trading standards departments run consumer advice centres near main shopping areas.
;ट्रेडिंग स्टैंडर्डजऋ के कुछ विभाग मुख्य बाजऋआर के पास उपभोक्ता सलाह  केंद्र चलाते हैं .
;(defrule run35
;(declare (salience 4500))
;(id-root ?id run)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?id1)
;(id-root ?id1 centre)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id calAwe))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run35   "  ?id "  calAwe )" crlf))
;)

;@@@ Added by 14anu21 on 02.07.2014
;He scored forty runs.[self]
;उसने  चालीस घूमना खरोंचे .(Transaltion before adding rule)
;उसने  चालीस रन बनाए . 
;He scored forty runs on ten balls.
;उसने दस गोले पर चालीस घूमना खरोंचे .(Transaltion before adding rule)
;उसने दस गेंद पर चालीस रन बनाए . 
;Added rules ball8,score9
(defrule run36
(declare (salience 3500))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?idverb ?id)
(id-root ?idverb make|score)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run36   "  ?id "  rana )" crlf))
)

;@@@ added by 14anu22
;His nose is running.
;उसकी नाक बह रही है.
(defrule noserun
(declare (salience 7000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 nose)
(or(kriyA-subject  ?id ?id1)(kriyA-object ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       noserun   "  ?id "  baha )" crlf))
)


;$$$ Modified by 14anu-ban-05 on (02-04-2015)
;Ours is a company that isn’t run by grey men in suits.		 [OALD]
;हमारा एक ऐसी कम्पनी है जो सूटों में अधेड़ आदमियों के द्वारा  सञ्चालित नहीं किया जाता है . 		[MANUAL]
;@@@Added by 14anu19 (25-06-2014)
;He joined a famous school which was run by a great scholar Udraka.
;उसने एक प्रसिद्ध विद्यालय में प्रवेश लिया जो बडे विद्वान के द्वारा सञ्चालित किया गया था
(defrule run_by
(declare (salience 4200))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
;(kriyA-karma  ?id ?id1)	; commented by 14anu-ban-05 on (02-04-2015)
(kriyA-by_saMbanXI ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saFcAliwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run_by   "  ?id "  saMcAliwa_kara )" crlf))
)

;$$$Modified by 14anu-ban-07,(30-04-2015)
;@@@ Added by 14anu-ban-10 on (19-03-2015)
;A delicious shiver of excitement ran through his body.[oald]
;उत्तेजना की एक सुखद लहर उसके शरीर में  उठी .[manual]
(defrule run037
(declare (salience 4300))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1) ;added by 14anu-ban-07,(30-04-2015)
(id-root ?id1 shiver);added by 14anu-ban-07,(30-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run037   "  ?id "  uTI )" crlf))
)


;@@@ Added by 14anu-ban-01 on (20-04-2015) 
;He was run ragged with all the travel.[learnersdictionary]
;वह [लम्बी] यात्रा से क्लान्त हो गया था . [self]
;वह [लम्बी] यात्रा से अत्यधिक थक  गया था . [self]
(defrule run37
(declare (salience 4000))	
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ragged)	
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 klAnwa_ho/awyaXika_Waka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  run.clp 	run37  "  ?id "  " ?id1 "   klAnwa_ho/awyaXika_Waka_jA  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (20-04-2015) 
;He was run ragged by all the travel.[learnersdictionary]
;वह [लम्बी] यात्रा से क्लान्त हो गया था . self]
(defrule run38
(declare (salience 4300))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?)	
(id-root ?id1 ragged)
(kriyA-object ?id ?id1)
(kriyA-by_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 klAnwa_ho/awyaXika_Waka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  run.clp 	run38  "  ?id "  " ?id1 "   klAnwa_ho/awyaXika_Waka_jA  )" crlf))
)

;@@@ Added by 14anu-ban-01 on (20-04-2015) 
;All this travel is running me ragged![learnersdictionary]
;यह यात्रा मुझे क्लान्त कर रही है! [self]	
;यह यात्रा मुझे थका रही है! [self]	
(defrule run39
(declare (salience 4400))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-root ?id2 ragged)
(viSeRya-viSeRaNa  ?id1 ?id2)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 WakA/klAnwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  run.clp 	run39  "  ?id "  " ?id1 "   WakA/klAnwa_kara  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (20-04-2015) 
;They have taken out their van for a run around the city for sight seeing.[run.clp]
;वे अपनी गाड़ी शहर के दर्शनीय स्थलों की सैर के लिये ले गये.[run.clp]
(defrule run40
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 city|town)
(viSeRya-around_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run40  "  ?id "  sEra)" crlf))
)


;@@@ Added by 14anu-ban-01 on (20-04-2015) 
;This ferry operates on the Albagh-Bombay run[run.clp]
;यह फेरी अलबाग-मुम्बई मार्ग पर चलती है [run.clp]
(defrule run41
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run41  "  ?id "  mArga)" crlf))
)

;@@@ Added by 14anu-ban-01 on (22-04-2015) 
;When the new currency measures were announced there was a run on the bank.[run.clp]
;जब नयी मुद्रा को चलाने की घोषणा की गयी तो बैंक में सार्वजनिक माँग बढ़ गयी[run.clp]
;जब नयी मुद्रा को चलाने की घोषणा की गयी तो बैंक में सार्वजनिक माँग में बढो़त्री हुई[self]
(defrule run42
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 bank)
(viSeRya-on_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sArvajanika_mAzga_meM_baDZowrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run42  "  ?id "  sArvajanika_mAzga_meM_baDZowrI)" crlf))
)


;@@@ Added by 14anu-ban-01 on (22-04-2015) 
;Last year we had enjoyed an exceptional run of good monsoon. [run.clp]
;पिछले वर्ष हमने बरसात के असाधारण  लम्बे दौर का आनन्द लिया[run.clp]
(defrule run43
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 monsoon)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambA_xOra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run43  "  ?id "  lambA_xOra )" crlf))
)


;@@@ Added by 14anu-ban-01 on (22-04-2015) 
;"Shatranj ke khiladi" play had a good run in the country.[run.clp]
;शतरंज के खिलाड़ी नाटक देश में लम्बे दौर तक चला[run.clp]
;"शतरंज के खिलाड़ी" नाटक का देश में लम्बे दौर तक प्रसारण चला[self]
(defrule run44
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id2 play|opera|persormance|show|theatrical|comedy)
(kriyA-subject  ?id1 ?id2)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambe_xOra_waka_prasAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run44  "  ?id "  lambe_xOra_waka_prasAraNa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (22-04-2015)
;The run of the cards favoured his luck.[run.clp]
;पत्तों के बँटने में भाग्य ने उसका साथ दिया.[self]
(defrule run45
(declare (salience 4000))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 card)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baztanA))
(assert (id-wsd_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  run.clp       run45  "  ?id " meM)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  run.clp       run45  "  ?id "  baztanA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (22-04-2015)
;She ran her eyes over the stage.[run.clp]
;उसने मञ्च पर अपनी आँखें फेरीं/दौड़ाई.[self] 
(defrule run46
(declare (salience 3600))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 eye)
(kriyA-object ?id ?id1)
(kriyA-over_saMbanXI ?id ?) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pera/xOdA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run46 " ?id "  Pera/xOdA )" crlf)) 
)

;@@@ Added by 14anu-ban-01 on (22-04-2015)
;The ferries don't run on sundays.[run.clp]
;बडी नावें रविवार को नहीं चलती हैं . [self] 
(defrule run47
(declare (salience 3600))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ferry|train|bus|tram|transport)
(kriyA-subject ?id ?id1)
(kriyA-on_saMbanXI ?id ?) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run47 " ?id "  cala )" crlf)) 
)

;@@@ Added by 14anu-ban-01 on (22-04-2015)
;The lease of her house has only a year to run.[run.clp]
;उसके घर के किराए/इज़ारे का केवल एक ही वर्ष बाकी है [self] 
(defrule run48
(declare (salience 4200))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 year|month|day|hour)
(kriyA-kAlavAcI  ? ?id1)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(to-infinitive  =(- ?id 1) ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) bAkI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run48  "  ?id "  " (- ?id 1) "  bAkI )" crlf)) 
)

;@@@ Added by 14anu-ban-01 on (22-04-2015)
;Can I run you to the airport?[run.clp]
;क्या मैं तुम्हे एयर पोर्ट तक ले जा/छोड़ सकता हूँ [self] 
(defrule run49
(declare (salience 3600))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(kriyA-object  ?id ?id1)
(kriyA-to_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le_jA/Coda)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run49 " ?id "  le_jA/Coda )" crlf)) 
)

;@@@ Added by 14anu-ban-01 on (23-04-2015)
;The G T road runs parallel to the Allahabad city.[run.clp]
;जी टी रोड इलाहाबाद से होकर जाती है [run.clp] 
(defrule run50
(declare (salience 3600))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 road|path|runway|track|pipe|nerve|canal)
(id-root ?id2 parallel)
(kriyA-subject  ?id ?id1)
(kriyA-object  ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 hokara_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " run.clp	run50  "  ?id "  " ?id2 "  hokara_jA )" crlf))  
)


;@@@ Added by 14anu-ban-01 on (23-04-2015)
;;He murdered his wife,so the story runs.[run.clp]
;उसने अपनी पत्नी का कत्ल कर दिया ,इस तरह कहानी आगे बढ़ती है [self] 
(defrule run51
(declare (salience 3600))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 story)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_baDZa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run51 " ?id "  Age_baDZa )" crlf)) 
)


;@@@ Added by 14anu-ban-01 on (23-04-2015)
;I'm afraid the colour ran when I washed your new skirt.[run.clp]
;मुझे डर है कि जब मैने तुम्हारी नयी स्कर्ट धोई तब उसका रंग फैल/निकल गया[self] 
(defrule run52
(declare (salience 3600))
(id-root ?id run)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 colour|ink|liquid)
(kriyA-subject  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEla_jA/nikala_jA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " run.clp run52 " ?id "  PEla_jA/nikala_jA )" crlf)) 
)


;LEVEL 
;
;
;Headword : run
;
;Examples --
;
;"run","N","1.xOdZa" 
;he goes for a run every morning.
;vaha rojZa savere xOdZane jAwA hE<--xOdZanA
;--"2.sEra"
;They have taken out their van for a run around the city for sight seeing.
;ve apanI gAdZI Sahara ke xarSanIya sWaloM kI sEra ke liye le gaye.<--GUmanA<--xeKanA
;--"3.mArga"
;This ferry operates on the Albagh-Bombay run
;yaha PerI alabAga-mumbaI mArga para calawI hE. <--calanA
;--"4.lambA_xOra"
;`Shatranj ke khiladi 'play had a good run in the country.
;SawaraMja ke KilAdZI nAtaka xeSa meM lambe xOra waka calA.<--lambe samaya wakacalA<--calanA
;Last year we had enjoyed an exceptional run of good monsoon.  
;piCale varRa hamane asAXAraNa barasAwa ke lambe xOra kA Ananxa liyA.<--lambe samaya waka bAriSa 
;calI<--calanA
;--"5.sArvajanika mAzga"
;When the new currency measures were announced there was a run on the bank.
;jaba nayI muxrA ko calAne kI GoRaNA kI gayI wo bEMka meM sArvajanika mAzga baDZa gayI.<--baDZanA<--calanA
;--"6.bAdZA"
;She has a very big 'chicken-run' at her back yard.                       
;usake piCavAdZe eka bahuwa badZA murgI kA bAdZA hE<--bAdZA
;--"7.eka viSeRa Kela"
;Generally people go to shimla for a special sport,'ski-run'.
;sAmAnyawa: loga SimalA viSeRa Kela 'skI-rana'ke liye jAwe hEM<--baraPa kI DZalAna para xo pattiyoM para xOdZanA.
;--"8.rana"
;Indian cricket team won the game by 20 runs.
;BArawIya kriketa tIma bIsa rana se jIwa gayI.<--xOdZanA
;--"9.baMtanA/bAMtanA"
;The run of the cards favoured his luck.<--baMtanA
;pawwoM ke baMtane meM BAgya ne usakI waraPaxArI kI.<--pawwoM kA batanA<--baDZanA<--calanA
;
;"run","VI","1.xOdZanA"
;She cannot run fast because her leg pains.
;vaha wejZa nahIM xOdZa sakawI kyoM ki usakA pEra xarxa karawA hE.<--xOdZanA
;He was the first person to run a mile in four minutes.
;vaha praWama vyakwi WA jo cAra minata meM eka mIla xOdZA.<--xOdZanA
;
;--"2.xOdZAnA"
;She ran her eyes over the stage.
;usane apanI AzKe steja para xOdZAIM.<--xOdZI<--xOdZanA
;
;--"3.weja calanA eka jagaha se xUsarI jagaha"
;The ferries don't run on sundays.
;PerIjZa iwavAra ko nahIM calawI.<--calanA 
;In Calcutta trams run on rails
;kalakawwA meM trAmas patarI para calawI hE.<--calanA
;The lease of her house has only a year to run.
;usake Gara ke patte kA kevala eka hI varRa bacA hE.<--calanA hE
;P.C.Sarkar's magic show ran for six months only.
;pIsI sarakAra kA jAxU kA Kela kevala Ca: mahine hI calA.<--calanA
;--"4.le_jAnA  
;Can I run you to the airport?
;kyA mEM wumhe eyara porta waka le jA sakawA hUz.<--le jAnA<--calanA
;--"5.jAnA
;The G T road runs parallel to the Allahabad city.
;jI tI roda ilAhAbAxa se hokara jAwI hE.<--jAnA<--nikalanA<--calanA
;
;He murdered his wife,so the story runs.
;usane apanI pawnI kA kawla kara xiyA isa waraha kahAnI baDZawI hE<--calawI hE
;--"6.cAlU_karanA
;Could you run a hot bath for me?
;kyA wuma mere liye garama pAnI calA xoge.<--calanA 
;--"7.PElanA"
;I'm afraid the colour ran when I washed your new skirt.
;muJe dara hE jaba mEne wumhArI nayI skarta XoI usakA raMga PEla gayA.<--calA gayA
;
;nota:- uparyukwa saBI vAkyoM para xqRti dAlane para yaha niRkarRa nikAlA jA sakawA hE
;    ki 'run'Sabxa ke samaswa saMjFA Ora kriyA ke vAkyoM se nABikIya arWa xOdZane yA
;rana,se prApwa ho rahA hE.
;
;sUwra : xOdZa`^bAdZA  ;[run]
;
;


