
;@@@ Added by 14anu-ban-11 on (11-03-2015)
;Don't wobble the table I'm trying to write.(oald)
;मेज मत हिलाना मैं लिखने का प्रयास कर रहा हूँ . (self)
(defrule wobble1
(declare (salience 20))
(id-root ?id wobble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 table)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wobble.clp 	wobble1   "  ?id " hilAnA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (11-03-2015)
;Her voice wobbled with emotion.(oald)
;उसकी आवाज जोश के साथ काँपी . (self)
(defrule wobble0
(declare (salience 00))
(id-root ?id wobble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAzpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wobble.clp 	wobble0   "  ?id "  kAzpa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (11-03-2015)
;The handlebars developed a wobble. (oald)
;साइकल के हैंडल ने लडखडाहट का पैदा की . (self)
(defrule wobble2
(declare (salience 10))
(id-root ?id wobble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ladZaKadZAhata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wobble.clp 	wobble2   "  ?id "  ladZaKadZAhata)" crlf))
)


