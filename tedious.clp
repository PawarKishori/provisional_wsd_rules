;@@@ Added by 14anu-ban-07,(27-09-2014)
;We had to listen to the tedious details of his operation.(oald)
;हमे उसके आपरेशन का थकाऊ विवरण सुनना पड़ा.[manual]
(defrule tedious0
(id-root ?id tedious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WakAU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tedious.clp 	tedious0   "  ?id "  WakAU )" crlf))
)


;@@@ Added by 14anu-ban-07,(27-09-2014)
;Although the graphical method of adding vectors helps us in visualising the vectors and the resultant vector, it is sometimes tedious and has limited accuracy.(ncert corpus)
;यद्यपि सदिशों को जोडने की ग्राफी विधि हमें सदिशों तथा उनके परिणामी सदिश को स्पष्ट रूप से समझने में सहायक होती है, परन्तु कभी - कभी यह विधि जटिल होती है और इसकी शुद्धता भी सीमित होती है . (ncert corpus)

(defrule tedious1
(declare (salience 5700))
(id-root ?id tedious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jatila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tedious.clp 	tedious1   "  ?id "  jatila )" crlf))
)
