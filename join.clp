;Commented by 14anu02 on 5.6.14 because there is no phrasal verb as 'join on'
;(defrule join0
;(declare (salience 5000))
;(id-root ?id join)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 on)
;(kriyA-upasarga ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samUha_meM_SAmila_ho))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " join.clp	join0  "  ?id "  " ?id1 "  samUha_meM_SAmila_ho  )" crlf))
;)

;They were going for a movie && i joined on.
;ve sinemA jA rahe We Ora mEM BI unake samUha meM SAmila ho gayA

;$$$ Modified by 14anu02 on 5.6.14 -- added example with translation
;He joined up in 1950. 
;वह 1950 में सुरक्षाबल का सदस्य हुआ .
(defrule join1
(declare (salience 5000))   ;Salience increased by 14anu02
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 surakRAbala_kA_saxasya_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " join.clp	join1  "  ?id "  " ?id1 "  surakRAbala_kA_saxasya_ho  )" crlf))
)

(defrule join2
(declare (salience 4800))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join2   "  ?id "  mila )" crlf))
)

;default_sense && category=verb	saMyukwa_kara	0
;"join","VT","1.saMyukwa_karanA"
;I have joined the two wires.
;--"2.milanA"
;Lets join together in our effort to bring peace in the society.
;--"3.SAmila_honA"
;I joined the Navy in 1998.
;
;

;Added by Prachi Rathore[22-11-13]
;I don't have time for a drink now, but I'll join you later. 
;मेरे पास अभी पेय या शरबत के लिए समय नहीं है, परन्तु मैं बाद में आपका साथ दूँगा .. 
(defrule join3
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join3   "  ?id "  sAWa_xe )" crlf))
)

;Added by Prachi Rathore[22-11-13]
; It's a great club, why don't you join? 
;यह क्लब बहुत अच्छा है ,तुम क्यों नहीं इसके सदस्य बन जाते .
(defrule join4
(declare (salience 4997))	;Salience reduced by 14anu02
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa ? ?id)  ;Wrong fact
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saxasya_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join4   "  ?id "  saxasya_bana )" crlf))
)

;$$$ Modified by 14anu-ban-06 (01-12-2014)
;$$$ Modified by 14anu02 on 5.6.14
;Added by Prachi Rathore[22-11-13]
; If you join the dots on the paper, you'll get a picture.
;अगर तुम पेपर पर के  दोनों बिंदुओं को जोड़ दो तो ,तुम्हें एक तस्बीर दिखेगी . 
;अगर तुम पेपर पर बिंदुओं को जोड़ दो तो तुम्हें एक तस्वीर दिखेगी . (added by 14anu02)
(defrule join5
(declare (salience 5100))    ;Salience reduced by 14anu02 to avoid clash with join3;salience increased from 4999 to 5100 by 14anu-ban-06 (01-12-2014)
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-on_saMbanXI  ?id ?)      ;Commented by 14anu02 
(kriyA-object ?id ?id1)          ;added '?id1' by 14anu-ban-06 (01-12-2014)
(id-root ?id1 dot)               ;added by 14anu-ban-06 (01-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join5   "  ?id "  joda )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join5   "  ?id "  ko )" crlf)
)

;$$$ Modified by Bhagyashri Kulkarni (16-09-2016)
;Join the great travel of the nation against the HIV and AIDS. (health)
;एचआईवी और एड्स के विरुद्ध राष्ट्र की बडी यात्रा में शामिल होइए . 
;$$$ Modified by 14anu-ban-06  (20-08-2014) added Sangha|Service to id-root
;With Kashyapa five hundred of his disciples joined the Sangha .(Parallel corpus)
;कश़्यप के साथ उनके पांच सौ शिष़्य भी बौद्ध धर्म  में शामिल हो गये .
;That's the reason , as a woman , I joined the Indian Police Service .
;यही कारण था की , महिला होने पर भी , मैं भारतीय पुलिस सेवा में शामिल हुई !
;$$$ Modified by 14anu02 on 5.6.14
;Added by Prachi Rathore[22-11-13]
;I felt so unfit after Christmas that I decided to join a gym.
;क्रिसमस के बाद मैंने जिम की सदस्यता लेने का निश्चय किया
;क्रिसमस के बाद मैंने इतना अस्वस्थ महसूस किया कि जिम में शामिल होने का निश्चय किया. (added by 14anu02)
(defrule join6
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 gym|club|team|queue|rank|Sangha|Service|travel) ;added 'club|team|queue|rank' to list by 14anu02 ;added 'Sangha|Service' by 14anu-ban-06);added 'travel'  by Bhagyashri Kulkarni (16-09-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAmila_ho))
(assert (kriyA_id-object_viBakwi ?id meM))    ;Added by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join6   "  ?id "  SAmila_ho )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp      join6   "  ?id " meM )" crlf)     ;Added by 14anu02 
)

;Added by Prachi Rathore[22-11-13]
;A long suspension bridge joins the two islands.
;एक लम्बा पुल दोनों द्वीपों को जोड़ता है.
(defrule join7
(declare (salience 4999))     ;Salience increased by 14anu02
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(and(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?id2))
(and(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))(not(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join7   "  ?id "  joda )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join7   "  ?id "  ko )" crlf)
)

;Commented by 14anu02 on 5.6.14 since it is included in join6
;@@@ Added by Prachi Rathore[22-11-13]
;If you've come to buy tickets for tonight's performance, please join the UK queue
;अगर तुम आज की रात का प्रदर्शन (नाटक ) देखने आए हो तो कतार के अंत में शामिल होना होगा .
;(defrule join8
;(declare (salience 5000))
;(id-root ?id join)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?id1)
;(id-root ?id1 rank|queue)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id SAmila_ho))
;(assert (kriyA_id-object_viBakwi ?id meM)) 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join8   "  ?id "  SAmila_ho )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join8   "  ?id "  meM )" crlf)
;)


;commented by 14anu02 on 5.6.14 since it is included in join6
;;@@@   ---Added by Prachi Rathore
; She was prodded into joining the team. [cambridge]
;उसे टीम में सम्मिलित होने के लिये उकसाया गया था .
;The possibility of joining the educational line came in 1887. [gyannidhi]
;(defrule join9
;(declare (salience 5000))
;(id-root ?id join)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(or(kriyA-into_saMbanXI  ? ?id)(viSeRya-of_saMbanXI  ? ?id))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id sammiliwa_ho))
;(assert (kriyA_id-object_viBakwi ?id meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join9   "  ?id "  sammiliwa_ho )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join9   "  ?id "  meM )" crlf))
;)

;$$$ Modified by 14anu02 on 5.6.14
;@@@ Added by Prachi Rathore 3-1-14
;She and her sister used to be joined at the head when they were kids. 
;वह और उसकी बहन जब बच्चे थे तो सिर से जुड़े हुए थे. -- added by 14anu02
(defrule join10
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id judZa))	;meaning changed from 'juda' to 'judZa' by 14anu02
(assert (kriyA_id-object_viBakwi ?id se))       ;Added by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join10   "  ?id "  judZa)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join10   "  ?id "  se )" crlf))             ;Added by 14anu02
)

;$$$ Modified by 14anu02 on 5.6.14
;@@@ Added by Prachi Rathore 10-1-14
;Tim has applied to join the police.[cambridge]
;टिम ने पुलिस में शामिल होने के लिए आवेदन किया है. --added by 14anu02
(defrule join11
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-kriyArWa_kriyA  ?id1 ?id)     ;Commented by 14anu02
;(id-root ?id1 apply)                 ;Commented by 14anu02
(id-root ?id1 police|military|army|navy)        ;Added by 14anu02 
(kriyA-object  ?id ?id1)                        ;Added by 14anu02 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarwI_ho))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join11   "  ?id "  BarwI_ho )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join11   "  ?id "  meM )" crlf)
)

;$$$ Modified by 14anu02 on 5.6.14 -- added office to the list
;$$$ Modified by 14anu-ban-06  (20-08-14)
;Although the same number of German Republicans and Democrats joined the organization by 1860.(COCA)
;yaxyapi usI safKyA meM jarmanI se ripablikanja Ora demakrEtsa ne 1860 meM safgaTana kA paxaBAra grahaNa kiyA.
;@@@ Added by Prachi Rathore 10-1-14
;She joined the company three months ago.[oald]
;उसने तीन महीने पहले कम्पनी में पदभार ग्रहण किया . 
(defrule join12
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 company|office|organisation|organization);added 'organisation|organization' by 14anu-ban-06
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxaBAra_grahaNa_kara))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join12   "  ?id "  paxaBAra_grahaNa_kara )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join12   "  ?id "  meM )" crlf)
)

;$$$ Modified by 14anu02 on 5.6.14 -- added 'institute|coaching' to the list
;$$$ Modified by 14anu-ban-06  (20-08-14) added school|college to id-root
;He joined Presidency College , the premier college of Calcutta University in 1913 .
;उऩ्होंने कलकत़्ता विश़्वविद्यालय के प्रतिष़्ठित प्रेजीडेंसी कालेज में प्रवेश लिया था .
;@@@ Added by Prachi Rathore 10-1-14
;I've joined an aerobics class.[oald]
;mEMne vyAyAma ki kakRA meM praveSa liyA.
;उसने तीन महीने पहले कम्पनी में पदभार ग्रहण किया . (wrong tranlation)
(defrule join13
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 class|institute|coaching|school|School|College|college) ;added 'school|School|College|college'  by 14anu-ban-06
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praveSa_le))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join13   "  ?id "  praveSa_le )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join13   "  ?id "  meM )" crlf)
)

;$$$ Modified by 14anu02 on 5.6.14
;@@@ Added by Prachi Rathore[10-1-14]
;Members of the public joined the search for the missing boy. [oald]
;जनता के सदस्य लापता लडके की तलाश में जुडे . added by 14anu02
;
(defrule join14
(declare (salience 4900)) ;salience reduced by Bhagyashri Kulkarni (16-09-2016) from 5000 to 4900
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)	;Commented by 14anu02
;(viSeRya-of_saMbanXI  ?id1 ?id)	;Commented by 14anu02
(kriyA-object ?id ?id1)	        ;Added by 14anu02
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id judZa))     ;Meaning changed juda_jA as judZa by 14anu02
(assert (kriyA_id-object_viBakwi ?id meM))      ;Added by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join14   "  ?id "  judZa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  join.clp     join14  "  ?id "  meM )" crlf))      ;Added by 14anu02
)


;@@@ Added by Prachi Rathore[10-1-14]
;I wish he would join in with the other children.[oald]
;मै चाहती हू वह अन्य बच्चों के साथ शामिल हो जाए.
(defrule join15
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 in)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SAmila_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " join.clp	join15  "  ?id "  " ?id1 "  SAmila_ho  )" crlf))
)

;$$$ Modified by 14anu02 on 5.6.14 -- changed meaning 'SAmila_ho' as ke_sAWa_milakara_kAma_kara' and added (+ ?id1 1) in assert
;@@@ Added by Prachi Rathore[10-1-14]
;We'll join up with the other groups later.[oald]
;हम बाद में दूसरे समूहो के साथ मिलकर काम करेंगे . -- added by 14anu02 
(defrule join16
(declare (salience 5500))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
(id-cat_coarse ?id verb)
;(kriyA-with_saMbanXI  ?id ?)	      ;Commented by 14anu02
(id-word =(+ ?id1 1) with)           ;Added by 14anu02 because this rule is for the phrasal verb 'join up with'
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 (+ ?id1 1) ke_sAWa_milakara_kAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " join.clp	join16  "  ?id "  " ?id1 " " (+ ?id1 1) " ke_sAWa_milakara_kAma_kara  )" crlf))
)

;Commented by 14anu02 on 5.6.14 because its included in join13
;@@@ Added by Prachi Rathore[21-1-14]
;She worked in a school for a while before joining the college.
;उसने कालेज से पहले कुछ समय तक विद्यालय में काम किया . 
;(defrule join17
;(declare (salience 5000))
;(id-root ?id join)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-before_saMbanXI  ? ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id -))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join17   "  ?id "  - )" crlf))
;)

;Commented by 14anu02 on 5.6.14 because its included in join1
;Example in this is wrong-wrong usage of 'join up'
;@@@ Added by Prachi Rathore[21-1-14]
; If you join up the dots on the paper, you'll get a picture.[cambridge]
;यदि आप कागज पर बिंदु जोड देते हैं, तो आप चित्र प्राप्त करेंगे . 
;(defrule join18
;(declare (salience 5500))
;(id-root ?id join)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-upasarga ?id ?id1)
;(id-word ?id1 up)
;(kriyA-object  ?id ?id2)
;(id-root ?id2 dot)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 joda_xe))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " join.clp	join18  "  ?id "  " ?id1 "  joda_xe )" crlf))
;)


;commented by 14anu-ban-06 to aoid clash with join6/11
;@@@ Added by 14anu01
;there are the big changes in the media industry that you hope to join. 
;सञ्चार माध्यम उद्योग में बडे बदलाव हैं कि आप शामिल होने के लिए आशा करते हैं . 
;(defrule join19
;(declare (salience 6000))
;(id-root ?id join)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(id-word =(- ?id 1) to)
;(id-root ?id1 function|programe|media_industry)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id SAmila_ho))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join19   "  ?id "  ;SAmila_ho )" crlf))
;)

;@@@ Added by 14anu-ban-06   (20-08-2014)
;To them , success would come only if millions of other Indians joined in their struggle .
;उनका मानना है कि सफलता तभी मिलेगी जब लखों अन्य भारतीय भी उनके संघर्ष में शामिल होंगे .
(defrule join20
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAmila_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join20   "  ?id "  SAmila_ho )" crlf))
)

;@@@ Added by 14anu-ban-06 (24-09-2014)
;We join O and P by a straight line.  (NCERT)
;hama @P ko @O se eka sarala reKA se joda xewe hEM .
(defrule join21
(declare (salience 5100))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join21   "  ?id "  joda_xe )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-10-2014)
;It is important to note that displacement vector is the straight line joining the initial and final positions and does not depend on the actual path undertaken by the object between the two positions.  (NCERT)
;yahAz yaha bAwa mahawvapUrNa hE ki 'visWApana saxiSa' ko eka sarala reKA se vyakwa karawe hEM jo vaswu kI anwima sWiwi ko usakI prAramBika sWiwi se jodawI hE waWA yaha usa vAswavika paWa para nirBara nahIM karawA jo vaswu xvArA biMxuoM ke maXya calA jAwA hE .(NCERT)
(defrule join22
(declare (salience 5150))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 line)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join22   "  ?id "  joda )" crlf))
)

;$$$ Modified by 14anu-ban-06 (01-12-2014)
;@@@ Added by 14anu11
;The first thing that Basava did was to declare , that anybody could come and join his religion , and no distinction of any kind would be made among them.
;बसव का पहला काम तो यही घोषणा थी कि कोई भी उसके धर्म में शरीक हो सकता है तथा उनमें कोई भी भेदभाव नहीं किया जायेगा .
(defrule join23
(declare (salience 5000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 anybody);added by 14anu-ban-06 (01-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SarIka));spelling corrected 'SrIk' to 'SarIka' by 14anu-ban-06 (01-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  join.clp 	join23   "  ?id "  SarIka )" crlf))
)

;$$$ Modified by 14anu-ban-06 (09-12-2014)
;@@@ added by 14anu22
;join in the parts.
;भागों को जोडो.
(defrule join24
(declare (salience 8000))
(id-root ?id join)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) in)
(kriyA-in_saMbanXI ?id ?id1);added by 14anu-ban-06 (09-12-2014) to avoid clash with 'join15'
(id-root ?id1 part);added by 14anu-ban-06 (09-12-2014) to avoid clash with 'join15'
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) joda));meaning changed from 'jodanA' to 'joda' by 14anu-ban-06 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  join.clp join24 " ?id " " (+ ?id 1) "  joda)" crlf)));meaning changed from 'jodanA' to 'joda' by 14anu-ban-06 (09-12-2014)

