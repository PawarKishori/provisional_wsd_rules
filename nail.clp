;$$$ Modified by  Manasa 12-02-2016
;@@@ Added by 14anu04 on 19-June-2014
;The police have failed to nail the killer.
;पुलीस हत्यारा पकडने में असफल रही . 
(defrule nail_tmp4
(declare (salience 5400))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
;(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
;the above condition is commented by Manasa 12-02-2016
;The police is trying to nail that theif.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nail.clp 	nail_tmp4   "  ?id "  pakada )" crlf))
)

;@@@ Added by 14anu04 on 19-June-2014
;We spent the night nailing up the signboards.
;हमने सूचना-पट्ट लगाते हुए रात बिताया. 
(defrule nail_tmp3
(declare (salience 5400))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
(id-root ?id1 up)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lagA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  nail.clp     nail_tmp3   "  ?id "  " ?id1 "  lagA  )" crlf))
)

;@@@ Added by 14anu04 on 19-June-2014
;We nailed up the door from the outside. 
;हमने बाहर से दरवाजा बन्द किया. 
(defrule nail_tmp2
(declare (salience 5600))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 up)
(id-root ?id2 door|window)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  nail.clp     nail_tmp2   "  ?id "  " ?id1 "  baMxa_kara  )" crlf))
)

;@@@ Added by 14anu04 on 19-June-2014
;He hit the nails by the hammer.
;उसने हथौडे से कीलें ठोकी. 
(defrule nail_tmp
(declare (salience 5500))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
(id-root ?id1 hammer|hit|force)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nail.clp 	nail_tmp   "  ?id "  kIla )" crlf))
)

;@@@ Added by 14anu05 and 14anu21 on 14.06.14
;He hammered the nail into the coffin.
;उसने ताबूत के अन्दर कील हथौडे से ठोका.
(defrule nail02
(declare (salience 5500))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
(or (kriyA-object ?id1 ?id)(kriyA-subject ?id1 ?id))
(id-root ?id1 hammer|push|pull|pince)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nail.clp 	nail02   "  ?id "  kIla )" crlf))
)


;@@@ Added by 14anu-ban-08 (11-10-2014)
;A bar magnet can attract an iron nail from a distance.     [NCERT]
;कोई छड चुम्बक लोहे की कीलों को दूर से ही, अपनी ओर आकर्षित कर लेता है.    [NCERT]
(defrule nail2
(declare (salience 5001))        ;salience increased by 14anu-ban-08 (18-02-2015)
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 iron)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nail.clp 	nail2   "  ?id "  kIla )" crlf))
)

;@@@Added by 14anu-ban-08 (05-02-2015)
;He wondered how the magnet could affect objects such as nails or pins placed away from it and not in any way connected to it by a spring or string.   [NCERT]
;उनको आश्चर्य होता था कि कैसे एक चुम्बक उन कीलों और पिनों को अपनी ओर खींच लेती थी, जो उससे दूर रखे थे और किसी स्प्रिंग या धागे द्वारा उससे जुडे भी नहीं थे.   [NCERT]
(defrule nail3
(declare (salience 5500))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-such_as_saMbanXI ?id1 ?id)
(id-root ?id1 object)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nail.clp 	nail3   "  ?id "  kIla )" crlf))
)

;@@@Added by 14anu-ban-08 (05-02-2015)
;Shepherds on this island complained that their wooden shoes which had nails, at times stayed struck to the ground.  [NCERT]
;इस द्वीप के गडरियों ने शिकायत की कि उनके लकडी के जूते जिनमें कीलें लगी हुई थीं, कई बार जमीन से चिपक जाते थे.  [NCERT]
(defrule nail4
(declare (salience 5501))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(kriyA-vAkyakarma ?id2 ?id1)
(id-root ?id2 shoe)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nail.clp 	nail4   "  ?id "  kIla )" crlf))
)

;--------------- Default Rules -------------------

(defrule nail0
(declare (salience 5000))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAKUna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nail.clp 	nail0   "  ?id "  nAKUna )" crlf))
)

(defrule nail1
(declare (salience 4900))
(id-root ?id nail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kIla_jadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  nail.clp 	nail1   "  ?id "  kIla_jadZa )" crlf))
)

;"nail","VT","1.kIla_jadZanA"
;He nailed the box properly.
;Nail the painting on the wall.
;
;
