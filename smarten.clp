;@@@ added by 14anu22
;You should smarten yourself up.
;तुम्हे अपने-आप को आकर्षक बनाना चाहिये.
(defrule smarten0
(declare (salience 4000))
(id-root ?id smarten)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AkarRaka_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " smarten.clp smarten0  "  ?id "  " ?id1 "  AkarRaka_banA)" crlf))
)
