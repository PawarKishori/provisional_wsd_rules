;@@@ Added by 14anu-ban-06 (17-02-2015)
;It's ironic that she became a teacher—she used to hate school.(OALD)
;यह विडम्बनात्मक है कि वह शिक्षक बन गई— वह विद्यालय नापसन्द किया करती थी . (manual)
(defrule ironic0
(declare (salience 0))
(id-word ?id ironic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vidambanAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ironic.clp 	ironic0   "  ?id "  vidambanAwmaka )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-02-2015)
;An ironic comment.(cambridge)
;व्यंग्यात्मक टिप्पणी .(manual)
;An ironic reply.(cambridge) 
;व्यंग्यात्मक जवाब . (manual)
(defrule ironic1
(declare (salience 2000))
(id-word ?id ironic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 comment|reply)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyaMgyAwmaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ironic.clp 	ironic1   "  ?id "  vyaMgyAwmaka )" crlf))
)
