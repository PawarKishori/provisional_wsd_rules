;@@@ Added by 14anu-ban-01 on (07-04-2015) 
;He tried to salve their wounds.[self: with reference to COCA]
;उसने उनके घावों की दवा करने का प्रयास किया . [self]
(defrule salve2
(declare (salience 5000))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 wound|bruise|cut)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xavA_kara))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  salve.clp 	salve2   "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve2   "  ?id "  xavA_kara )" crlf))
)


;@@@ Added by 14anu-ban-01 on (07-04-2015) 
;Oils are a great salve for dry skin.[coca]
;तेल सूखी त्वचा के लिए एक बडी औषधि होते हैं[self]
(defrule salve3
(declare (salience 5100))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 skin|throat|spot)
(viSeRya-for_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ORaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve3   "  ?id "  ORaXi )" crlf))
)


;@@@ Added by 14anu-ban-01 on (07-04-2015) 
;The rule has one of those names that is a salve for insomniacs.[self: with respect to coca]
;[उस] नियम में उन नामों में से एक नाम है जो अनिद्रापीडित के लिए इलाज है . [self]
(defrule salve4
(declare (salience 5100))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 insomniac|hangover)
(viSeRya-for_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ilAja/upacAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve4   "  ?id "  ilAja/upacAra )" crlf))
)

;@@@ Added by 14anu-ban-01 on (07-04-2015) 
;That help was a salve for her guilty conscience.[self: with respect to coca]
;वह सहायता उसकी दोषी अन्तरात्मा के लिए अपराध बोध से मुक्ति थी . [self]
(defrule salve5
(declare (salience 5100))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id2 guilty|remorseful)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id1 conscience)
(viSeRya-for_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aparAXa_boXa_se_mukwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve5   "  ?id "  aparAXa_boXa_se_mukwi)" crlf))
)


;@@@ Added by 14anu-ban-01 on (07-04-2015) 
;A medic smoothed a wet salve on the sunburned face, neck, and hands.[coca]
;डाक्टर ने धूप-ताम्र चेहरे पर, गरदन, और हाथों पर एक गीला लेप लगाया . [self]
(defrule salve6
(declare (salience 5100))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 wet|smooth|pasty)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lepa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve6   "  ?id "  lepa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (07-04-2015)
;This trend typically provides some emotional salve to the poor and the middle class. [self: with reference to COCA]
;विचारधारा/प्रचलन आम तौर पर गरीब और मध्यम वर्ग को कुछ भावनात्मक आराम देता है . [self]
(defrule salve7
(declare (salience 5100))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 emotional)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve7   "  ?id "  ArAma )" crlf))
)


;@@@ Added by 14anu-ban-01 on (07-04-2015)
;He decides one salve to the trauma is to go ahead with that idea. [self: with reference to COCA]
;वह फैसला लेता है कि मानसिक आघात का एक इलाज है उस विचार के साथ आगे जाना  . [self]
(defrule salve8
(declare (salience 5100))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 trauma)
(viSeRya-to_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ilAja/upacAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve8   "  ?id "  ilAja/upacAra )" crlf))
)


;@@@ Added by 14anu-ban-01 on (07-04-2015)
;Mentally, the soothing salve of massage eases tension . [COCA]
;मानसिक रूप से,मालिश का आरामदेह/शांतिदायक  उपचार  तनाव को कम कर देता है . [self]
(defrule salve9
(declare (salience 5100))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 massage)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upacAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve9   "  ?id "  upacAra )" crlf))
)

;------------------------ Default Rules ----------------------

;"salve","N","1.marahama"
;We use Betadine ( an antiseptic cream ) as a salve on wounds.
(defrule salve0
(declare (salience 5000))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id marahama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve0   "  ?id "  marahama )" crlf))
)

;"salve","V","1.SAnwa_karanA"
;The gift was his way of salving his conscience. 
(defrule salve1
(declare (salience 4900))
(id-root ?id salve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  salve.clp 	salve1   "  ?id "  SAnwa_kara )" crlf))
)

