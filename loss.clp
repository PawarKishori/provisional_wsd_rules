;@@@ Added by 14anu13 on 21-06-14
; I'm sorry for your loss in buisness.
;मैं व्यापार में तुम्हारे नुकसान के लिए दुखी हूँ |
(defrule loss04
(declare (salience 5800))
(id-root ?id loss)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI  ?id1  ?id)
(id-root ?id1 sorry|happy)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nukasAna ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loss.clp 	loss04   "  ?id "  nukasAna )" crlf))
)

;@@@ Added by 14anu13 on 21-06-14
;The loss of his wife was a great blow to him.
;उसकी पत्नी की मृत्यु उसके लिये बड़ा धक्का थी |
(defrule loss03
(declare (salience 5800))
(id-root ?id loss)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id  ?id1)
(or(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-root ?id1 family))    
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mqwyu ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loss.clp 	loss03   "  ?id "  mqwyu )" crlf))
)

;@@@ Added by 14anu13 on 21-06-14
;When she died I was filled with a sense of loss.
;जब वह मरी मैं क्षोभ में डूब गया |
(defrule loss02
(declare (salience 5800))
(id-root ?id loss)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id2   ?id)
(id-root ?id2 sense)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRoBa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loss.clp 	loss02   "  ?id "  kRoBa )" crlf))
)

;@@@ Added by 14anu13 on 21-06-14
;I am suffering with  the loss of blood in my body.
;मैं अपने  शरीर में खून की कमी से ग्रस्त हू़ँ |
;The closure of the factory will lead to a number of job losses.
;कारखाने का बंद होना नौकरियों में बडी़ कमी  लाएगा |. 
(defrule loss01
(declare (salience 4800))
(id-root ?id loss)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ?id ?id1)(and(id-root ?id2 job)(samAsa_viSeRya-samAsa_viSeRaNa  ?id  ?id2)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loss.clp 	loss01   "  ?id "  kamI )" crlf))
)


;@@@ Added by 14anu-ban-08 on (08-09-2014)
;We assume conservation of energy of the rolling body, i.e. there is no loss of energy due to friction etc.  [NCERT]
;हम मान लेते हैं कि लोटन करते पिण्ड की ऊर्जा संरक्षित है अर्थात्, घर्षण आदि के कारण ऊर्जा की कोई हानि नहीं होती.
(defrule loss1
(declare (salience 100))
(id-root ?id loss)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-word ?id1 energy|heat)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAni))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loss.clp 	loss1   "  ?id "  hAni )" crlf))
)

;@@@ Added by 14anu-ban-08 on (08-09-2014)
;The outer jacket acts as a heat shield and reduces the heat loss from the inner vessel.  [NCERT]
;बाहरी आवरण ऊष्मा कवच की भांति कार्य करता है तथा यह भीतरी बर्तन से ऊष्मा - हानि को कम कर देता है.
(defrule loss2
(declare (salience 110))
(id-root ?id loss)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-word ?id1 heat|conduction|convection)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAni))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loss.clp 	loss2   "  ?id "  hAni )" crlf))
)

;@@@ Added by 14anu-ban-08 on (08-09-2014)
;In the steady state, heat flowing into the element must equal the heat flowing out of it; otherwise there would be a net gain or loss of heat by the element and its temperature would not be steady.   [NCERT]
;स्थायी अवस्था में छड़ के किसी अवयव में प्रवेश करने वाली ऊष्मा उससे बाहर निष्कासित होने वाली ऊष्मा के बराबर होनी चाहिए, वरना अवयव द्वारा ऊष्मा की नेट लब्धि अथवा हानि होगी तथा इसका ताप स्थायी नहीं रहेगा.
(defrule loss3
(declare (salience 115))
(id-root ?id loss)
?mng <-(meaning_to_be_decided ?id)
(kriyA-of_saMbanXI ?id1 ?id)
(id-root ?id1 flow)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAni))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loss.clp 	loss3   "  ?id "  hAni )" crlf))
)
;---------------------------Default Rule-------------------------------------

;@@@ Added by 14anu-ban-08 on (08-09-2014) and 14anu13 
;added another meaning hAni by 14anu-ban-08 (16-01-2015)
;He faced a great loss in his buisness.
;उसने अपने व्यापार में बहुत हानि का सामना किया|
;Civic religion - absolute formula Rango 1992 - 93 , due to broken was , which was massive loss of life and material. [karan singla]
;नगर का धर्म - निरपेक्ष सूत्र १९९२ - ९३ के दंगों के कारण छिन्न - भिन्न हो गया जिसमें बड़े पैमाने पर जान व माल का नुकसान हुआ ।
(defrule loss0
(declare (salience 0))
(id-root ?id loss)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nukasAna/hAni))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loss.clp 	loss0   "  ?id "  nukasAna/hAni )" crlf))                        
)

