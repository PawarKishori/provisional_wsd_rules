;@@@Added by 14anu-ban-02(27-02-2015)
;Sentence: There was ample time to get to the airport.[oald]
;Translation: हवाई अड्डा पहुँचने में काफी समय था.[self]
(defrule ample0 
(declare (salience 0)) 
(id-root ?id ample) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kAPI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ample.clp  ample0  "  ?id "  kAPI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(27-02-2015)
;An ample bosom. [cald]
;बड़ी छाती . [manual]
(defrule ample1 
(declare (salience 100)) 
(id-root ?id ample) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 bosom|girth)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id badZA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  ample.clp  ample1  "  ?id " badZA )" crlf)) 
) 
