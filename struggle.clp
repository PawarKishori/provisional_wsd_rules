
(defrule struggle0
(declare (salience 5000))
(id-root ?id struggle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMGarRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  struggle.clp 	struggle0   "  ?id "  saMGarRa )" crlf))
)

;"struggle","N","1.saMGarRa"
;Launching of different product involves the struggle of different companies.
;--"2.prayawna"
;After a long struggle he cleared the exams.


;Added by Meena(13.10.09)
;It struggled to force its body through that little hole .
(defrule struggle1
(declare (salience 5000))
(id-root ?id struggle)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMGarRa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  struggle.clp  struggle1   "  ?id "  saMGarRa_kara )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;If they're struggling , remind them of other things they're good at - such as drawing or cycling or football or singing .[Source:http://hindi-english-word-alignment.googlecode.com/svn/trunk/data/training/acl2005englishV1.txt]
;यदि वे जूझ रहें है तो उन्हें उन अन्य चीज़ों का ध्यान कराएँ जिनमें वे अच्छे हैं - जैसे कि चित्रकला या साइकिल चलाना या फुटबॉल खेलना या गाना गाना .[self]
;@@@ Added by avni(14anu11)
;If they ' re struggling , remind them of other things they ' re good at - such as drawing
;यदि वे कडी मेहनत कर रहें है तो , उन्हें उन अन्य विषयों का ध्यान करावें जिनमें वे अच्छे हैं - जैसा कि अआंकना या साइकिल चलाना या फुटबॉल खेलना या गाना गाना .
(defrule struggle3
(declare (salience 5000))
(id-root ?id struggle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object ?id ?))	;added by 14anu-ban-01 on (12-01-2015)
(not(kriyA-for_saMbanXI ?id ?))	;added by 14anu-ban-01 on (12-01-2015)
(not(kriyA-to_saMbanXI ?id ?))	;added by 14anu-ban-01 on (12-01-2015)
;(subject-subject_samAnAXikaraNa  ?id1 ?id)commented by 14anu-ban-01  on (12-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jUJa));changed "mehnw_kr_rhe" to "jUJa" by 14anu-ban-01 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  struggle.clp 	struggle3   "  ?id "  jUJa )" crlf));changed "mehnw_kr_rhe" to "jUJa" by 14anu-ban-01 on (12-01-2015)
)



(defrule struggle2
(declare (salience 4900))
(id-root ?id struggle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMgrAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  struggle.clp 	struggle2   "  ?id "  saMgrAma_kara )" crlf))
)

;"struggle","V","1.saMgrAma_karanA"
;Although he had almost no chance of survival after the accident but he struggled out of it.
;--"2.prayawna_karanA"
;Though the wind was blowing in the opposite direction the bowlers struggled to keep the length.
;--"3.hAWa-pEra_mAranA"
;An guy struggled to get away from the police.
;
