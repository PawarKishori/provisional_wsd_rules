
(defrule reel0
(declare (salience 5000))
(id-root ?id reel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_viSeRa_prakAra_kA_nqwya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reel.clp 	reel0   "  ?id "  eka_viSeRa_prakAra_kA_nqwya )" crlf))
)

;"reel","N","1.eka_viSeRa_prakAra_kA_nqwya"
;There is a provision provided in our college to learn the famous reel dance.
;--"1.GiranI"
;Shopkeepers wrap the electrical wires on reels.
;--"2.sinemA_rIla"
;That film was of 1.reels.
;
(defrule reel1
(declare (salience 4900))
(id-root ?id reel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakkara_KAnA_hila_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reel.clp 	reel1   "  ?id "  cakkara_KAnA_hila_jA )" crlf))
)

;"reel","VT","1.cakkara_KAnA_hila_jAnA"
;The boxer reeled && fell. 
;--"2.GUmanA"
;Everything began to reel before her eyes.  
;

;@@@ Addedby 14anu24 03-07-2014
;The state is reeling under drought and he has appealed to the Centre for relief .
;राज्य सूखे की चपेट में है और वे केंद्र से मदद की गुहार कर रहे हैं .
(defrule reel2
(declare (salience 5000))
(id-root ?id reel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-under_saMbanXI  ?id ?id1)
(id-root ?id1 drought)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id capeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reel.clp     reel2   "  ?id "  capeta )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-04-2015)
;A stone hit his head and the street reeled before his eyes. (cambridge)
;पत्थर उसका सिर पर लगा और सडक उसकी आँखों के सामने घूमने लगी . (manual)
(defrule reel3
(declare (salience 5100))
(id-root ?id reel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-before_saMbanXI ?id ?id1)
(id-root ?id1 eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUmane_laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reel.clp     reel3   "  ?id "  GUmane_laga )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-04-2015)
;We were reeling from the news that we had won all that money. (cambridge)
;हम समाचार से हक्का बक्का रह गये थे कि हमने वह सभी पैसा जीता था . (manual)
(defrule reel4
(declare (salience 5200))
(id-root ?id reel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-with_saMbanXI ?id ?id1)(kriyA-from_saMbanXI ?id ?id1))
(id-root ?id1 news)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hakkA_bakkA_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reel.clp     reel4   "  ?id "  hakkA_bakkA_raha )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-04-2015)
;She immediately reeled off several names. (OALD)
;उसने तत्काल कई नाम धडल्ले से बोले .(manual) 
(defrule reel5
(declare (salience 5300))
(id-root ?id reel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XadZalle_se_bola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " reel.clp	reel5  "  ?id "  " ?id1 "  XadZalle_se_bola  )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-04-2015)
;I slowly reeled the fish in. (OALD)[parser no.-2]
;मैंने धीरे से मछली को रील पर लपेटा . (manual)
(defrule reel6
(declare (salience 5400))
(id-root ?id reel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 rIla_para_lapeta))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " reel.clp	reel6  "  ?id "  " ?id1 "  rIla_para_lapeta  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  reel.clp       reel6   "  ?id " ko )" crlf))
)

;@@@ Added by 14anu-ban-06 (25-04-2015)
;A new reel of film. (OALD)
;फिल्म की नयी रील . (manual)
(defrule reel7
(declare (salience 5100))
(id-root ?id reel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 film|movie|song)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reel.clp 	reel7   "  ?id "  rIla )" crlf))
)
