;@@@ Added by 14anu-ban-11 on (25-02-2015)
;It's a sin to waste taxpayers' money like that.(oald)
;यह अपराध है इस तरह कर-दाता का पैसा व्यर्थ करना  .(self) 
(defrule sin1
(declare (salience 10))
(id-root ?id sin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(saMjFA-to_kqxanwa  ?id ?id1)
(kriyA-object  ?id1 ?id2)
(id-root ?id1 waste)
(id-root ?id2 money)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aparAXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sin.clp 	sin1   "  ?id "  aparAXa)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (25-02-2015)
;The Bible says that stealing is a sin.(oald)
;बाइबिल कहती है कि चोरी करना पाप है . (self) 
(defrule sin0
(declare (salience 00))
(id-root ?id sin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sin.clp      sin0   "  ?id "  pApa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (25-02-2015)
;If anyone sinned, it was Adam. (coca)
;यदि कोई भी पाप करता है,तो यह ऐडम था . (self)
(defrule sin2
(declare (salience 20))
(id-root ?id sin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pApa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sin.clp 	sin2  "  ?id "  pApa_kara)" crlf))
)

