
(defrule giant0
(declare (salience 5000))
(id-root ?id giant)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_badA)) ;Removed 'Z' in the meaning (Roja 05-08-11)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  giant.clp 	giant0   "  ?id "  bahuwa_badA )" crlf))
)

;"giant","Adj","1.bahuwa_badZA"
;A giant T.V. screen was put up in pubs to show matches.
;
(defrule giant1
(declare (salience 4900))
(id-root ?id giant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xEwya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  giant.clp 	giant1   "  ?id "  xEwya )" crlf))
)

;"giant","N","1.xEwya"
;Yetis are the giants of Himalayas.
;

;@@@ Added by 14anu-ban-05 on (11-04-2015)
;Camus is considered to be one of the twentieth century’s literary giants. [OALD]
;कैमिस बीसवी शताब्दी के साहित्यिक दिग्गजों में से एक माने जाते है . [MANUAL]
(defrule giant2
(declare (salience 4901))
(id-root ?id giant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 literary)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiggaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  giant.clp 	giant2   "  ?id "  xiggaja )" crlf))
)


;@@@ Added by 14anu-ban-05 on (11-04-2015)
;He was a giant of a man, standing nearly seven feet tall. [OALD]
;वह आदमी का एक विशालकाय रूप था, जो कि  करीब करीब सात फूट लम्बा था.	[MANUAL]
(defrule giant3
(declare (salience 4903))
(id-root ?id giant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSAlakAya_rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  giant.clp 	giant3   "  ?id "  viSAlakAya_rUpa )" crlf))
)


;@@@ Added by 14anu-ban-05 on (11-04-2015)
;The Amazon is a giant among rivers. [OALD]
;अमेजोन नदियों में सबसे बडी है .		[MANUAL]
(defrule giant4
(declare (salience 4902))
(id-root ?id giant)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sabase_badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  giant.clp 	giant4   "  ?id "  sabase_badA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (11-04-2015)
;The company is now one of the giants of the computer industry. [oald]
;कंपनी अब कंप्यूटर उद्योग के दिग्गजों में से एक है.		[manual]
(defrule giant5
(declare (salience 4904))
(id-root ?id giant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 industry|business)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiggaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  giant.clp 	giant5   "  ?id "  xiggaja )" crlf))
)


