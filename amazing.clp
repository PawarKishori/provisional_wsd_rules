;@@@Added by14anu-ban-02(12-02-2015)
;Sentence:It's amazing how quickly people adapt.[oald]
;Translation: यह आश्चर्यजनक है कि लोग कितनी जल्दी अपने आप को  ढालते हैं . [manual]
(defrule amazing0 
(declare (salience 0)) 
(id-root ?id amazing) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id AScaryajanaka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  amazing.clp  amazing0  "  ?id "  AScaryajanaka )" crlf)) 
) 

;@@@Added by 14anu-ban-02(12-02-2015)
;An amazing city encircled with curved mountains, beautiful architecture of churches and houses and dense pine trees is visible after reaching Shillong.[tourism en_aj]
;शिलांग  पहुँचते  ही  घुमावदार  पहाड़ों  ,  खूबसूरत  गिरजाघरों  ,  मकानों  की  शिल्पकला  तथा  सघन  चीड़  वृक्षों  से  घिरा  एक  अद्भुत  शहर  दिखाई  देता  है  ।[tourism]
(defrule amazing1 
(declare (salience 100)) 
(id-root ?id amazing) 
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id) 	;need sentences for restriction.
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id axXaBuwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  amazing.clp  amazing1  "  ?id "  axXaBuwa )" crlf)) 
) 
