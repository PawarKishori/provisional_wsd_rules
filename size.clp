;@@@ Added by 14anu-ban-01 on (11-02-2015)
;The field was about ten acres in size.[cald]
;मैदान परिमाण में लगभग दस एकड़ था.[self]
(defrule size1
(declare (salience 1000))
(id-root ?id size)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 acre)
(viSeRya-in_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parimANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  size.clp 	size1  "  ?id "  parimANa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (11-02-2015)
;If d is the diameter of the planet and α the angular size of the planet (the angle subtended by d at the earth), we have α = d / D (2.2)The angle can be measured from the same location on the earth. [NCERT corpus]
;यदि d ग्रह का व्यास और α उसका कोणीय आमाप (d द्वारा पृथ्वी के किसी बिंदु पर अन्तरित कोण) हो, तो α = d/D (2.2) कोण को, पृथ्वी की उसी अवस्थिति से मापा जा सकता है[NCERT corpus]
(defrule size2
(declare (salience 1000))
(id-root ?id size)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 angular)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AmApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  size.clp 	size2 "  ?id "  AmApa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (11-02-2015)
;I was amazed at the size of their garden .[cald]
;मैं उनके बगीचे के विस्तार को देखकर हैरान था. [self]
(defrule size3
(declare (salience 1000))
(id-root ?id size)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 garden|earth|farm|house|city|state|quarter)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viswAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  size.clp 	size3 "  ?id "  viswAra )" crlf))
)


;@@@ Added by 14anu-ban-01 on (11-02-2015)
;The baby is a good size (= quite large).[cald]
;शिशु काफी बड़ा  है. [self]
(defrule size4
(declare (salience 1000))
(id-root ?id size)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 good|big|large|super)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  kAPI_badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " size.clp 	size4  "  ?id "  "?id1"  kAPI_badA  )" crlf))
)

;@@@ Added by 14anu-ban-01 on (11-02-2015)
;Do these shoes come (= are they made) in children's sizes?[cald]
;क्या ये जूते बच्चों के नाप के बनाए जाते हैं?[self]
(defrule size5
(declare (salience 1000))
(id-root ?id size)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 come)
(kriyA-subject  ?id1 ?)
(id-root ?id1 ?str)			
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(kriyA-in_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  size.clp 	size5  "  ?id "  nApa )" crlf))
)




;@@@ Added by 14anu-ban-01 on (11-02-2015)
;They complained about the size of their gas bill.[oald]
;उन्होंने उनके गैस के बिल की कुल राशि के बारे में शिकायत की . [self]
(defrule size6
(declare (salience 1000))
(id-root ?id size)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 bill|tax)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kula_rASi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  size.clp 	size6 "  ?id "  kula_rASi )" crlf))
)

;------------------------------ Default Rules -------------------

;@@@ Added by 14anu-ban-01 on (11-02-2015)
;Some kinds of trees grow to a huge size.[cald]
;कुछ तरह के पेड़ विशालकाय आकार तक बढ़ जाते हैं.[self]
(defrule size0
(declare (salience 0))
(id-root ?id size)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  size.clp     size0  "  ?id "  AkAra )" crlf))
)

