
;"icing","N","1.AisiMga"
;Icing makes cake attractive.
(defrule ice0
(declare (salience 5000))
(id-root ?id ice)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id icing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id AisiMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  ice.clp  	ice0   "  ?id "  AisiMga )" crlf))
)

;$$$ Modified by 14anu-ban-06 (20-02-2015)
;Art helps break the ice between students. (COCA)
;कला विद्यार्थियों के बीच रुखाई दूर करने में सहायता करती हैं . (manual)
;@@@ Added by 14anu06(Vivek Agarwal) on 23/6/2014******
;There was a lot of ice between them. 
;उनके बीच बहुत रुखाई थी.
(defrule ice2
(declare (salience 5100))
(id-root ?id ice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-between_saMbanXI  ?id ?id1)
(id-root ?id1 them|relationship|student)         ;added 'student' and modified 'id-word' as 'id-root' by 14anu-ban-06 (20-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ruKAI)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ice.clp 	ice2   "  ?id "  ruKAI )" crlf))
)


;@@@ Added by 14anu-ban-06 (19-02-2015)
;The lake has iced over.(cambridge)
;झील बर्फ से ढक गई है . (manual)
;Parts of the river were iced over during the cold spell.(OALD) 
;नदी के भाग ठण्डे मौसम के दौरान बर्फ से ढके गये थे . (manual)
(defrule ice3
(declare (salience 5300))
(id-root ?id ice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 barPa_se_Daka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " ice.clp	ice3  "  ?id "  " ?id1 "  barPa_se_Daka_jA  )" crlf))
)

;@@@ Added by 14anu-ban-06 (19-02-2015)
;The plane was delayed because the engine had iced up. (cambridge)
;प्लेन  देरी से पहुँचा था क्योंकि इंजन जम गया था . (manual)
(defrule ice4
(declare (salience 5300))
(id-root ?id ice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jama_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " ice.clp	ice4  "  ?id "  " ?id1 "  jama_jA  )" crlf))
)

;------------------------ Default Rules ----------------------

;"ice","N","1.baraPZa"
;We put ice in juice to chill it.
(defrule ice1
(declare (salience 4900))
(id-root ?id ice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baraPZa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ice.clp 	ice1   "  ?id "  baraPZa )" crlf))
)

