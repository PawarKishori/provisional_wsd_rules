;@@@ Added by 14anu-ban-04 (20-04-2015)
;An ugly power station disfigures the landscape.         [oald]
;एक घृणित विद्युत उत्पादन गृह प्राकृतिक दृश्य को विरूपित करता है .            [self]
(defrule disfigure1
(declare (salience 20))
(id-root ?id disfigure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id virUpiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disfigure.clp     disfigure1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disfigure.clp 	disfigure1   "  ?id "  virUpiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (20-04-2015)
;She was horribly disfigured by burns.             [cald]
;वह  बुरी तरह से जलने  से   कुरूप हो  गयी थी .                    [oald]
(defrule disfigure2
(declare (salience 50))
(id-root ?id disfigure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-by_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kurUpa_ho))        
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disfigure.clp 	disfigure2   "  ?id "  kurUpa_ho )" crlf))
)

;---------------------------------------- Default Rules --------------------------------------------

;@@@ Added by 14anu-ban-04 (20-04-2015)
;His face was disfigured by a scar.               [merriam-webster]
;उसका चेहरा दाग से खराब हो गया था .                           [self]
(defrule disfigure0
(declare (salience 10))
(id-root ?id disfigure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba_ho))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disfigure.clp 	disfigure0  "  ?id "  KarAba_ho )" crlf))
)
