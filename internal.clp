;@@@ Added by 14anu-ban-06  (21-08-14)
;The pillar of base is open on roof for internal lighting management .(Parallel corpus)
;इनके स्तम्भाकार आधार छत पर आंतरिक प्रकाश की व्यवस्था हेतु खुले हैं ।
;Relativity says the internal motions go very slow .(Parallel corpus)
;सापेक्षता का कहना है कि आंतरिक गति बहुत धीमी गति से चलते हैं .
(defrule internal0
(declare (salience 0))
(id-root ?id internal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Anwarika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  internal.clp 	internal0   "  ?id "  Anwarika )" crlf))
)

;@@@ Added by 14anu-ban-06  (21-08-14)
;Last but not least , the foreign trade affected the internal distribution of income adversely .(Parallel corpus)
;विदेशी व्यापार ने देश के भीतरी  वितरण को बुरी तरह प्रभावित किया .
;In the internal part of this palace sloppy path was built .(Parallel corpus)
;इस महल के भीतरी भाग में ढालू मार्ग बनाया गया था ।
;In the middle of the internal city near the Sonbhandar cave the cylindrical well shaped Maniar Math has archaeological importance .(Parallel corpus)
;भीतरी नगर के मध्य में सोनभण्डार गुफा के नजदीक बेलनाकार कुआँ जैसी आकृति मनियार मठ का पुरातात्विक महत्‍व है ।
(defrule internal1
(declare (salience 2000))
(id-root ?id internal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 distribution|part|city|wall)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BIwarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  internal.clp 	internal1   "  ?id "  BIwarI )" crlf))
)

;@@@ Added by 14anu-ban-06  (21-08-14)
;In this the consultations of I C C and other internal boards are also considered .(Parallel corpus)
;इसमें आई सी सी और अन्य घरेलू बोर्डों का परामर्श भी शामिल होता है ।
(defrule internal2
(declare (salience 2000))
(id-root ?id internal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 board)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GarelU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  internal.clp 	internal2   "  ?id "  GarelU )" crlf))
)
