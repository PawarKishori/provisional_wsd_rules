
(defrule surround0
(declare (salience 5000))
(id-root ?id surround)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id surrounding )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Asa-pAsa_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  surround.clp  	surround0   "  ?id "  Asa-pAsa_kA )" crlf))
)

;"surrounding","Adj","1.Asa-pAsa kA"
;From the roof of my house I can see the whole town && the surrounding towns && villages.
;
(defrule surround1
(declare (salience 4900))
(id-root ?id surround)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surround.clp 	surround1   "  ?id "  kinArA )" crlf))
)

;"surround","N","1.kinArA"


;$$$ Modified by 14anu-ban-01 on (17-02-2015)
;A fire place with a tiled surround.
;A ring of compass needles surrounds the wire.[NCERT corpus] : by 14anu-ban-01 on (17-02-2015)
;चुम्बकीय सुइयों की एक मुद्रिका तार को चारों ओर से घेरती है . [self:improvised]
;तार के चारों ओर चुम्बकीय सुइयों की एक मुद्रिका बनाई गई है[NCERT corpus]
(defrule surround2
(declare (salience 4800))
(id-root ?id surround)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAroM_ora_se_Gera))
(assert (kriyA_id-object_viBakwi ?id ko))	;added by 14anu-ban-01 on (17-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  surround.clp 	surround2   "  ?id " ko )" crlf)	;added by 14anu-ban-01 on (17-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surround.clp 	surround2   "  ?id "  cAroM_ora_se_Gera )" crlf))
)

;"surround","V","1.cAroM_ora_se_GeranA"
;Police had surrounded the criminal's house.

;@@@ Added by 14anu-ban-01 on (21-10-2014)
;We know that in a solid, each atom or molecule is surrounded by neighbouring atoms or molecules.[NCERT corpus]
;hama jAnawe hEM ki kisI Tosa meM prawyeka paramANu yA aNu pAsa vAle paramANuoM yA aNuoM se GirA howA hE.[NCERT corpus]
(defrule surround3
(declare (salience 4800))
(id-root ?id surround)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) be)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GirA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  surround.clp 	surround3  "  ?id "  GirA_ho )" crlf))
)
;
