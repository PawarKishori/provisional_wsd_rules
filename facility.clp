;@@@ Added by 14anu-ban-05 on (12-01-2015)
;The hotel has special facilities for welcoming disabled people.         [oald]
;होटल में अक्षम लोगों के स्वागत के लिए विशेष सुविधाएं हैं.      [manual]
(defrule facility0
(declare (salience 100))
(id-root ?id facility)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suviXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  facility.clp  	facility0   "  ?id "  suviXA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (12-01-2015)
;You will be provided full board facility.       [from board.clp]
;आपको पूरी खाने-पीने की व्यवस्था दी जाएँगी .                  [from board.clp]
(defrule facility1
(declare (salience 101))
(id-root ?id facility)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 board|lodge|stay)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavasWA))
(assert  (id-wsd_viBakwi   ?id1  kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  facility.clp  	facility1   "  ?id "  vyavasWA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  facility.clp  	facility1  "  ?id1 " kI )" crlf))
)
