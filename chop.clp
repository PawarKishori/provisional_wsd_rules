;They decided to chop the mango tree down because it was cracking the wall of the house.
;unhone Ama ke pedZa ko kAtane kA niScaya kiyA kyoMki vaha Gara kI xIvAra meM xarAra dAla rahA WA
(defrule chop0
(declare (salience 5000))
(id-root ?id chop)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " chop.clp	chop0  "  ?id "  " ?id1 "  kAta  )" crlf))
)

;$$$  Modified by Preeti(30-7-14)
;"chop","N","1.AGAwa"
;He cut the sapling in one chop.
;usane eka AGAwa meM bAlavqkRa kAtA.hindi translation added by preeti
(defrule chop1
;(declare (salience 4900));salience changed
(id-root ?id chop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AGAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chop.clp 	chop1   "  ?id "  AGAwa )" crlf))
)

;$$$  Modified by Preeti(30-7-14)
;"chop","VT","1.kAtanA{kulhAdZI_se}"
;He chopped the sapling in one stroke.
;usane eka Jatake meM bAlavqkRa kAtA.hindi translation added by preeti
(defrule chop2
;(declare (salience 4800));salience changed
(id-root ?id chop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chop.clp 	chop2   "  ?id "  kAta )" crlf))
)

;"chop","V","1.kAtanA{kulhAdZI_se}"
;(defrule chop3
;(declare (salience 4700))
;(id-root ?id chop)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kAta));removed same meaning as chop2
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chop.clp 	chop3   "  ?id "  kAta ;)" crlf))
;)




;@@@ Added by Preeti(30-7-14)
;Chop the carrots up into small pieces. 
;gAjara ko Cote tukadoM meM  kAtie.
(defrule chop3
(declare (salience 4800))
(id-root ?id chop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-word ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  chop.clp 	chop3   "  ?id "  " ?id1 " kAta )" crlf))
)

;@@@ Added by Preeti(30-7-14)
;I chopped up the vegetables for the soup. 
;mEMne Sorabe ke lie sabjI kAtIM.
(defrule chop4
(declare (salience 4800))
(id-root ?id chop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga   ?id ?id1)
(id-word ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  chop.clp 	chop4   "  ?id "  " ?id1 " kAta )" crlf))
)

;@@@ Added by Preeti(30-7-14)
;The country was chopped up into small administrative areas. 
;xeSa ko Cote praSAsanika kRewroM meM bAtA gayA WA.
(defrule chop5
(declare (salience 4850))
(id-root ?id chop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga   ?id ?id1)
(id-word ?id1 up)
(kriyA-into_saMbanXI  ?id ?id2)
(id-root ?id2 area)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAZta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  chop.clp 	chop5   "  ?id "  " ?id1 " bAZta )" crlf))
)


;@@@ Added by Preeti(30-7-14)
;Give the mushrooms a quick chop before adding them to the pan. [Oxford Advanced Learner's Dictionary]
;kukaramuwwA ko wave para dalane se pahale  usake tukade kAta leM.
(defrule chop6
(declare (salience 1000))
(id-root ?id chop)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tukadZe_kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chop.clp 	chop6   "  ?id "  tukadZe_kAta )" crlf))
)




