;@@@ Added by 14anu-ban-11 on (18-02-2015)
;Someone was screaming for help.(oald)
;कोई सहायता के लिए चिल्ला रहा था . (anusaaraka)
(defrule scream2
(declare (salience 5001))
(id-root ?id scream)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 help)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cillA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scream.clp 	scream2   "  ?id "  cillA )" crlf))
)


;@@@ Added by 14anu-ban-11 on (18-02-2015)
;It's a scream.(oald) 
;यह चुटकुला है .(anusaaraka) 
(defrule scream3
(declare (salience 5002))
(id-root ?id scream)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa  ?id ?id1)
(id-root ?id1 a)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cutakulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scream.clp 	scream3   "  ?id "  cutakulA )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (23-02-2015)
;@@@ Added by 14anu-ban-11 on (18-02-2015)
;The powerboat screamed out to sea.(oald) 
;रेस की नौका समुद्र में तेज आवाज करते हुए चली गयी . (self)
(defrule scream4
(declare (salience 5003))
(id-root ?id scream)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 sea)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja_AvAja_karawe_hue_calA_jA))	;changed calI to calA by 14anu-ban-01 on (23-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scream.clp 	scream4   "  ?id "  weja_AvAja_karawe_hue_calA_jA)" crlf))	;changed calI to calA by 14anu-ban-01 on (23-02-2015)
)

;@@@ Added by 14anu-ban-01 on (23-02-2015)
;These books scream out to be included in a list of favourites.[oald]
;ये पुस्तकें प्रिय किताबों की सूची में सम्मिलित होने के लिए चीख-चीख कर सबका ध्यान आकर्षित कर रहीं हैं . [self]
(defrule scream5
(declare (salience 5003))
(id-root ?id scream)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 out)
(id-root ?id2 book|novel)
(kriyA-subject  ?id ?id2)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cIKa-cIKa_kara_sabakA_XyAna_AkarRiwa_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " scream.clp 	scream5 "  ?id "  "?id1 "  cIKa-cIKa_kara_sabakA_XyAna_AkarRiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-02-2015)
;People ran for the exits, screaming out in terror.[oald]
;लोग आतङ्क में चीखते हुए निकास की तरफ दौड़े. [self]
(defrule scream6
(declare (salience 10))
(id-root ?id scream)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 out)
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cIKa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " scream.clp 	scream6 "  ?id "  "?id1 "  cIKa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-02-2015)
;The baby was screaming itself hoarse. [oald]
;शिशु कर्कश चीख/रो रहा था . [self]
(defrule scream7
(declare (salience 5000))
(id-root ?id scream)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 baby)		;The following condition can be added(uncommented) once 'baby' is added to both human.gdbm and animate.gdbm
;(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id2 itself|myself|yourself|himself|herself|ourself|themself|theirself)
(kriyA-object  ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 cIKa/ro)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " scream.clp 	scream7 "  ?id "  "?id2 "  cIKa/ro )" crlf))
)


;------------------------ Default Rules ----------------------

;"scream","N","1.cIKa"
;Screams of laughter are being heard from the theatre.
(defrule scream0
(declare (salience 5000))
(id-root ?id scream)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cIKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scream.clp 	scream0   "  ?id "  cIKa )" crlf))
)

;"scream","V","1.cIKanA"
;The fans screamed with delight when they saw him.
(defrule scream1
(declare (salience 0))	;salience reduced to 0 from 4900 by 14anu-ban-01 on (23-02-2015)
(id-root ?id scream)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cIKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scream.clp 	scream1   "  ?id "  cIKa )" crlf))
)

