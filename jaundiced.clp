;@@@ Added by 14anu-ban-06 (10-04-2015)
;A jaundiced patient. (OALD)
;पीलियाग्रस्त मरीज . (manual)
(defrule jaundiced1
(declare (salience 2000))
(id-word ?id jaundiced)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 patient)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIliyAgraswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jaundiced.clp 	jaundiced1   "  ?id "  pIliyAgraswa )" crlf))
)

;@@@ Added by 14anu-ban-06 (10-04-2015)
;He was jaundiced from liver disease. (cambridge)[parser no.- 3]
;वह पित्ताशय रोग के कारण पीलियाग्रस्त था . (manual)
(defrule jaundiced2
(declare (salience 2100))
(id-word ?id jaundiced)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-from_saMbanXI ?id ?id1)
(id-root ?id1 disease)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIliyAgraswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jaundiced.clp 	jaundiced2   "  ?id "  pIliyAgraswa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (10-04-2015)
;He seems to take a very jaundiced view of life.(cambridge)
;वह जीवन का एक अत्यन्त पक्षपातपूर्ण रूख लेता हुआ प्रतीत होता है . (manual)
(defrule jaundiced0
(declare (salience 0))
(id-word ?id jaundiced)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRapAwapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jaundiced.clp 	jaundiced0   "  ?id "  pakRapAwapUrNa )" crlf))
)


