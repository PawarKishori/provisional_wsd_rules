
;@@@ Added by 14anu-ban-07 on 26-7-14
;कुल:
;There are as such a total of 34 scenic sites in Grand Palace.(parallel corpus)
;वैसे  ग्रांड  पैलेस  में  कुल  34  दर्शनीय  स्थल  हैं  ।
(defrule total3
(declare (salience 5100))
(id-root ?id total)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  total.clp 	total3   "  ?id "  kula )" crlf))
)

;---------------- Default rules ----------------------------

(defrule total0
(declare (salience 5000))
(id-root ?id total)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sampUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  total.clp 	total0   "  ?id "  sampUrNa )" crlf))
)

;"total","Adj","1.sampUrNa"
;It is awe-inspiing to watch the total eclipse.
;
(defrule total1
(declare (salience 4900))
(id-root ?id total)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  total.clp 	total1   "  ?id "  joda )" crlf))
)

;"total","N","1.joda"
;India scored a total of 278 runs.
;
(defrule total2
(declare (salience 4800))
(id-root ?id total)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  total.clp 	total2   "  ?id "  joda_nikAla )" crlf))
)

;"total","V","1.joda_nikAlanA"
;You havn't done the total yet?.
;

;@@@ Added by 14anu-ban-07 (09-10-2014)
;The total linear momentum and the total angular momentum (both vectors) of an isolated system are also conserved quantities.(ncert corpus)
;किसी वियुक्त निकाय का कुल रैखिक संवेग, तथा कुल कोणीय संवेग (दोनों सदिश) दोनों भी संरक्षित राशियाँ हैं.(ncert corpus)
;However, since the atoms are merely rearranged but not destroyed, the total mass of the reactants is the same as the total mass of the products in a chemical reaction.(ncert corpus)
;तथापि, चूँकि परमाणु केवल पुनर्व्यवस्थित ही होते हैं, नष्ट नहीं होते, किसी रासायनिक अभिक्रिया में अभिकर्मकों का कुल द्रव्यमान, उत्पादों के कुल द्रव्यमान के बराबर होता है.(ncert corpus)
;If the total binding energy of the reacting molecules is less than the total binding energy of the product molecules, the difference appears as heat and the reaction is exothermic.(ncert corpus)
;यदि अभिकर्मक अणुओं की कुल बन्धन ऊर्जा उत्पादित अणुओं की कुल बन्धन ऊर्जा से कम होती है तो ऊर्जा का यह अन्तर ऊष्मा के रूप में प्रकट होता है और अभिक्रिया ऊष्माक्षेपी होती है.(ncert corpus)
(defrule total4
(declare (salience 5000))
(id-root ?id total)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 momentum|mass|energy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  total.clp 	total4   "  ?id "  kula )" crlf))
)
