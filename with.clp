;FILE MODIFIED BY SOMA (1.10.16)
;FILE MODIFIED BY MEENA (31.8.09)

;DEFAULT RULE (Soma - 1.10.16)
;Modified by Meena(31.8.09) ;added (subject-conjunction ?id1  ?id)
;$$$ Modified 14anu-ban-11 on (25.8.14)(Rule was not firing)
;The speed with which it moves is not much.
(defrule with0
;(declare (salience 100)) ;salience commented by 14anu-ban-11 on (15-10-2014)
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp    with0   "  ?id "  se )" crlf))
)

;Modified by Soma (26-9-16)
;Modified by Meena(28.8.09)
;Do not talk with a driver
;He eats lunch with her.
;If John was with Lisa last night, who went to the movie with Diane.
;He eats rice with soup. - NOT BY THIS RULE
;This is almost inevitable with ladies garments like a polyester saree.;modified by Rashmi Ranjan
(defrule with1
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id1 ?id2);modified by Rashmi Ranjan ;removed viSeRya-with-saMbanXI by Soma : Counterexample 'I know the boy with the long hair'
;(id-cat_coarse  =(+ ?id 1) PropN|noun|pronoun|determiner|vector);modified by Rashmi Ranjan ;removed by Soma
(or(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(word-wordid-nertype ? ?id2 PERSON)) ;Added by Soma ;  word-wordid-nertype Smita 8 PERSON)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp 	with1   "  ?id "  ke_sAWa )" crlf))
)

;Added by Meena(15.9.09)
;He has a horse with a white mark on its head.
;A boy with long hair saved the girl.
; This rule is wrongly fired for the sentence: 'He writes the letter with a black pen' because of the wrong PP attachment
;The rule is not fired for "He use big books and other books with large types" : reason - wrong parse
(defrule with2
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI ?id1 ?id2)
(id-cat_coarse ?id1 noun)
(id-root ?id1 ?str&:(and (not (numberp ?str))(not(gdbm_lookup_p "time.gdbm" ?str)))) ;Added by Soma
(id-root ?id1 ?str&:(and (not (numberp ?str))(not(gdbm_lookup_p "place.gdbm" ?str)))) ;Added by Soma
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp 	with2   "  ?id "  vAlA )" crlf))
)

;with4 and with5 for busy - in 5 busy with some animate entity
;Modified by Soma (26-9-16)
;Modified by Meena(9.10.10)
;I am busy with my studies . 
;Added by Meena(7.7.10)
;I think you will find that physics helps you with calculus while calculus deepens and enhances your experience of physics .
(defrule with4
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
;(or(kriyA-with_saMbanXI  ?id1 ?id2)(viSeRya-with_saMbanXI  ?id1 ?id2))
(viSeRya-with_saMbanXI  ?id1 ?id2)
(id-word ?id1 busy|involved|comfortable|wrong|available|stuck|possible|numb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp      with4   "  ?id "  meM )" crlf))
)


;Added by Soma (26-9-16)
;I am busy with my puppy.
;mEM apane kuwwe ke bacce ke sAWa byaswa hUM 
(defrule with5
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI  ?id1 ?id2)
(id-word ?id1 busy|happy|involved|comfortable|impatient|okay|conversation)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp      with5   "  ?id "  ke_sAWa )" crlf))
)

;Added by Soma (26-9-16)
;He is friendly with everybody.
(defrule with6
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI  ?id1 ?id2)
(id-word ?id1 consistent|ready|friendly|gentle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp      with6   "  ?id "  ke_sAWa )" crlf))
)

;@@@Added by Soma (2-19-16)
;This is true with old computer models.
;यह पुराने सङ्गणक नमूने सेके बारे में सत्य है 
(defrule with7
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI  ?id1 ?id2)
(id-word ?id1 true)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAre_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp      with7   "  ?id "  ke_bAre_meM )" crlf))
)

;@@@Added by Soma (2-19-16)
;This plan will become effective with all Issues.
;यह योजना सभी समस्याओं के लिये प्रभावशाली हो जाएगी।
(defrule with8
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id1 ?id2)
(id-word ?id1 effective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp      with8   "  ?id "  ke_liye )" crlf))
)

;Modified by Soma ;(26-9-16)
;Added by Meena;(15.9.09)
;He was about six feet tall, with no distinguishing marks.
(defrule with9
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-with_saMbanXI  ?id1 ?id2) commented out
(id-word =(+ ?id 1) no)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp       with9   "  ?id "  binA )" crlf))
)


;@@@ Added by Anita-1.7.2014 
;The ultimate authority resides with the board of directors. [oxford learner's dictionary]
;अंतिम प्राधिकार निदेशक मंडल के पास होता है ।
(defrule with10
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id1 ?id2); added by 14anu-ban-11 on (15-10-2014)
(id-root ?id1 reside); added by 14anu-ban-11 on (15-10-2014) ;Modified by Soma (26-9-16)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp 	with10   "  ?id "  ke_pAsa )" crlf))
)


;@@@ Added by Soma ;(1.10.2016)
;this was the only interview available with her.
;उसके पास केवल यह साक्षात्कार उपलब्ध था
;I have no time with me.
(defrule with11
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI  ?id1 ?id2); added by 14anu-ban-11 on (15-10-2014)
(id-root ?id1 available|safe|have); added by 14anu-ban-11 on (15-10-2014) ;Modified by Soma (26-9-16)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp     with11   "  ?id "  ke_pAsa )" crlf))
)


;@@@ Added by 14anu-ban-11 on ;(07-11-2014)
;Note:-rule is working properly on parser no 5. 
;We have so far considered direct current ;(dc) sources and circuits with dc sources.(NCERT)
;अब तक हमने दिष्टधारा ;(dc) स्रोतों एवं	 दिष्टधारा स्रोतों से युक्त परिपथों पर विचार किया है.(NCERT)
(defrule with12
(declare (salience 900))
(id-root ?id with)
?mng <-;(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI  ?id1 ?id2) 
(id-root ?id1 circuit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_yukwa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp 	with12   "  ?id "  se_yukwa )" crlf))
)

; $$$ modified by Soma ;(26-9-16) verbs are the conditions
;@@@ Added by 14anu-ban-11 on ;(08-11-2014)
;Biological controls-Natural biological processes and materials can provide control, with acceptable environmental impact, and often at lower cost.;(agri)
;जैविक नियंत्रण-प्रकृतिक जैविक प्रक्रिया और  पदार्थ  सन्तोषजनक  पर्यावरिक  प्रभाव के साथ अक्सर  कम लागत में नियंत्रण प्रदान कर सकते हैं ;(manual)
;Hallwachs and Lenard studied how this photo current varied with collector plate potential, and with frequency and intensity of incident light. ;(NCERT)
;हालवॉक्स तथा लीनार्ड ने सङ्ग्राहक पट्टिका के विभव, आपतित प्रकाश की आवृत्ति तथा तीव्रता के साथ प्रकाश धारा में परिवर्तन का अध्ययन किया.;(NCERT)
;If a similar experiment is repeated with a nylon thread or a rubber band, no transfer of charge will take place from the plastic rod to the pith ball.;(NCERT)
;यदि इसी प्रयोग को नॉयलोन के धागे अथवा रबर के छल्ले के साथ दोहराएँ तो प्लास्टिक-छड से सरकण्डे की गोली में कोई आवेश स्थानान्तरित नहीं होता.;(NCERT)
;Visitors flood out, fumbling with water bottles and MP3 players.;(coca)
;दर्शक पानी की बोतलें और मपी3 प्लेयर  के साथ हाथ से इधर-उधर पलटते हुए बाहर आते है.;(self)i
;He has changed with time.
;vaha samaya ke sAWa baxala gayA hE
(defrule with13
(declare (salience 1000))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id1 ?id2) 
;(id-root ?id2 band|frequency|impact|bottle); Added 'frequency' by 14anu-ban-11 on (22-11-2014)
;                 Added 'impact' by 14anu-ban-11 on ;(03-12-2014); Added '' by 14anu-ban-11 on (03-02-2015) ; commented out by soma
(id-root ?id1 provide|repeat|vary|change|think|live|agree|deal|is|was|disagree|open|take|associate|converse|compare|reduce)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAWa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp 	with13   "  ?id "  ke_sAWa )" crlf))
)

;@@@ Added by Soma ;(01-10-2016)
;He came face to face with his enemy
;vaha usake Sawru ke Amane sAmane Aye
(defrule with14
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-with_saMbanXI  ?id1 ?id2) commented out
(id-word =(- ?id 1) face)
(id-word =(- ?id 2) to)
(id-word =(- ?id 3) face)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp       with14   "  ?id "  kA )" crlf))
)

;@@@ Added by Soma ;(01-10-2016)
;Wealth is not necessarily synonymous with happiness.
;धन आवश्यक रूप से सुख का पर्यायवाची नहीं है।	
(defrule with15
(declare (salience 900))
(id-root ?id with)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-with_saMbanXI  ?id1 ?id2) commented out
(id-word =(- ?id 1) synonymous)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  with.clp       with15   "  ?id "  kA )" crlf))
)



















;diagnosed with
;equipped with
;coupled with

;
;Case 1: 
;        If    'with' relates a NOUN with another NOUN,
;        then   with  vAlA
;
;Case 2: 
;        If     'with' relates a NOUN with a VERB, 
;        then
;
;           Case a:
;                  if the NOUN is an INSTRUMENT
;                  then with  se
;
;           Case b:
;                  if the NOUN is a SAHA-KAARAKA
;                  then with  ke_sAWa
;
;
;Examples:
;Case 1:
;         a man with a blue shirt
;         a student with long curly hairs
;Case 2:
; Case a:
;         Ram ate with spoon.
;         He cut an apple with a knife.
; Case b:
;         Ram went with Hari.
;         She came with her brother.

