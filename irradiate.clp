;@@@ Added by 14anu-ban-06 (28-02-2015)
;The cells are irradiated so that they cannot reproduce. (cambridge)
;कोशिकाओ का रेडियोधर्मी किरणों से उपचार किया जाता हैं जिससे कि वे दोबारा नहीं बन सके. (manual)
(defrule irradiate0
(declare (salience 0))
(id-root ?id irradiate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rediyoXarmI_kiraNoM_se_upacAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irradiate.clp 	irradiate0   "  ?id "  rediyoXarmI_kiraNoM_se_upacAra_kara )" crlf))
)


;@@@ Added by 14anu-ban-06 (28-02-2015)
;Shelley's face irradiated with happiness.(COCA)
;शेली का चेहरा खुशी से चमक उठा . (manual)
(defrule irradiate1
(declare (salience 2000))
(id-root ?id irradiate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id2)
(kriyA-subject ?id ?id1)
(id-root ?id1 face)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id camaka_uTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  irradiate.clp 	irradiate1   "  ?id "  camaka_uTa )" crlf))
)
