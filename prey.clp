
(defrule prey0
(declare (salience 5000))
(id-root ?id prey)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 upon)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AkramaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " prey.clp	prey0  "  ?id "  " ?id1 "  AkramaNa_kara  )" crlf))
)

;Hawks prey upon smaller birds. 
;bAjZa Cote pakRiyoM para AkramaNa karawA hE
(defrule prey1
(declare (salience 900)) ;salience reduced from 4900 to 900 by 14anu-ban-06
(id-root ?id prey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prey.clp 	prey1   "  ?id "  SikAra )" crlf))
)

;"prey","N","1.SikAra"
;The little mouse fell prey to the claws of a stealthy cat.
;

;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;These young thieves prey on the elderly. [Canbridge]
;ये तरुण चोर बुजुर्ग सा पर लूटते हैं . 
(defrule prey2
(declare (salience 4800))
(id-root ?id prey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 thief|crook|robber|burgalar|bandit|shoplifter)
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prey.clp 	prey2   "  ?id "  lUta )" crlf))
)

;$$$ Modified by 14anu-ban-06 Karanveer Kaur (Banasthali Vidyapith) 23-7-14 meaning changed from 'SikAra_karana' to 'SikAra_kara'
;A cunning fox pounced to prey upon a little squirrel.
;eka cAlAka lomadI CotI gilaharI ke Upara SikAra karane ko JapatI.
(defrule prey3
(declare (salience 4700))
(id-root ?id prey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prey.clp 	prey3   "  ?id "  SikAra_kara )" crlf))
)

;"prey","V","1.SikAra_karana"
;A cunning fox pounced to prey upon a little squirrel.
;
;@@@ Added by 14anu-ban-06 Karanveer Kaur (Banasthali Vidyapith) (23-7-14)
;Elderly people are easy prey for dishonest salesmen.  (OALD)
;bujurga loga AsAnI se beImAna xukAnaxAro kA SikAra banawe hE.
;They are easy prey for militia.  (COCA)
;ve AsAnI se senA kA SikAra banawe hE. 
(defrule prey4
(declare (salience 4700))
(id-root ?id prey)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikAra_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prey.clp 	prey4   "  ?id "  SikAra_bana )" crlf))
)
