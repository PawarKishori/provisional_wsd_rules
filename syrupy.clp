;@@@ Added by 14anu-ban-11 on (05-02-2015)
;A syrupy romantic novel. (oald)
;एक भावुक कल्पित उपन्यास .(SELF)
(defrule syrupy2
(declare (salience 30))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 novel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAvuka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy2   "  ?id "  BAvuka )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-03-2015) 
;He barked a thick syrupy cough to try and clear his throat.[oald]--->parse no.2
;उसने अपना गला साफ करने के प्रयास में एक मोटा गाढा कफ़ खाँसा/खखारा . [self:more natural]
;उसने अपना कण्ठ साफ करने के प्रयास के लिए एक मोटा गाढा कफ़ खाँसा/खखारा . [self:more faithful]
(defrule syrupy3
(declare (salience 20))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)		
(id-root ?id1 thick)			
(viSeRya-viSeRaNa  ?id2 ?id1)
(viSeRya-viSeRaNa  ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gADZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy3   "  ?id "  gADZA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (26-03-2015) 
;Heat the liquid until it is thick and syrupy.[oald]
;द्रव को तब तक गरम कीजिए जब तक यह मोटा और गाढा न हो जाए . [self]
(defrule syrupy4
(declare (salience 20))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)			
(id-root ?id2 thick)		
(conjunction-components  ?id1 ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gADZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy4  "  ?id "  gADZA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-03-2015) 
;Caroline pushed a raft of syrupy emotions back where they belonged.[coca]
;केरलाइन ने भावुकतापूर्ण जज़्बातों/भावनाओं का बेडा वापस वहाँ दबा दिया जहाँ उसे होना चाहिये था . [self]
(defrule syrupy5
(declare (salience 30))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 emotion|feeling)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAvukawApUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy5   "  ?id "  BAvukawApUrNa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-03-2015) 
;The syrupy music had been expressly created.[self:with respect to coca]
;मीठा सङ्गीत जान-बूझकर/सोच-समझकर बनाया गया था [self]
(defrule syrupy6
(declare (salience 30))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 music)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mITA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy6   "  ?id "  mITA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-03-2015) 
;Alexis replied to her in a syrupy voice.[self:with respect to coca]
;अलेक्सिस ने  खुशामदी आवाज में जवाब दिया .  [self]
(defrule syrupy7
(declare (salience 30))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 voice|tone)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KuSAmaxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy7   "  ?id "  KuSAmaxI )" crlf))
)


;@@@ Added by 14anu-ban-01 on (26-03-2015)
;This syrupy singer can sing jazz.[self: with respect to coca]
;यह सुरीला गायक जैज़ गा सकता है . [self]
(defrule syrupy8
(declare (salience 30))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 singer|choralist|musician|vocalist)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id surIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy8   "  ?id "  surIlA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-03-2015)
;Antony grimaced at the syrupy dishes on the table in front of him.[self:with reference to coca]
;अण्टोनी ने उसके सामने मेज पर [रखे] अतिशय मीठे व्यंजनों  पर(को देखकर) मुँह बनाया . [self]
(defrule syrupy9
(declare (salience 30))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 dish|pancake|cuisine)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awiSaya_mITA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy9   "  ?id "  awiSaya_mITA )" crlf))
)

;------------------------ Default Rules ----------------------

;Example changed by 14anu-ban-01 on (26-03-2014)
;It might be syrupy sweet and make you fat and lethargic.[coca]
;यह चाशनी जैसा मीठा हो सकता है और आपको मोटा और सुस्त बना सकता है . [self]
;@@@ Added by 14anu-ban-11 on (05-02-2015)
(defrule syrupy1
(declare (salience 20))
(id-root ?id syrupy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cASanI_jEsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  syrupy.clp 	syrupy1   "  ?id " cASanI_jEsA)" crlf))
)

