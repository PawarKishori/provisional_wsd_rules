
(defrule because0
(declare (salience 5000))
(id-root ?id because)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) of)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  because.clp 	because0   "  ?id "  - )" crlf))
)

;@@@ Added by 14anu07 on 30/06/2014
(defrule because3
(declare (salience 5000))
(id-root ?id because)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id conjunction)
(id-root =(- ?id 1) is)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isalie_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  because.clp 	because3   "  ?id "  isalie_ki)" crlf))
)

;@@@ Added by 14anu07 on 30/06/2014
(defrule because4
(declare (salience 5000))
(id-root ?id because)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id conjunction)
(id-root =(- ?id 2) is)
(id-root =(- ?id 1) only|just)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isalie_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  because.clp 	because4   "  ?id "  isalie_ki)" crlf))
)

;------------------- Default rules ----------------------------

;$$$ Modified by 14anu02 on 2.7.14
;I can't go because I am ill.
;मैं नहीं जा सकता हूँ क्योंकि मैं अस्वस्थ हूँ . 
(defrule because1
(declare (salience 4900))
(id-root ?id because)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id conjunction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyoMki))		;meaning changed from 'kyozki' to 'kyoMki' by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  because.clp 	because1   "  ?id "  kyoMki )" crlf))
)

;"because","Conj","1.kyoMki"

;commented by 14anu-ban-02(15-01-2015)
;correct meaning is coming from because1
;$$$ Modified by 14anu02 on 2.7.14
;I couldn't see Elena's expression, because her head was turned.
;मैं एलेना के हाव-भाव को नहीं देख सका,क्योंकि उसका सिर मुड़ा हुआ था . 
;(defrule because2
;(declare (salience 4800))
;(id-root ?id because)
;?mng <-(meaning_to_be_decided ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kyoMki))		;meaning changed from 'kyozki' to 'kyoMki' by 14anu02
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  because.clp 	because2   "  ?id "  kyoMki )" ;crlf))
;)

