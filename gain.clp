
(defrule gain0
(declare (salience 5000))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pICA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gain.clp	gain0  "  ?id "  " ?id1 "  pICA_kara  )" crlf))
)

;You will have to speed up-they are gaining on us!
;wumheM apanI raPwAra baDZAnI hogI kyoMki ve hamArA pICA kara rahe hEM
(defrule gain1
(declare (salience 4900))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gain.clp 	gain1   "  ?id "  prApwi )" crlf))
)

;"gain","N","1.prApwi"
;No one works without any gain.
;--"2.lABa"
;His business is bringing him a lot of gains.
;

;$$$ Modified 'prApwa_ho' to 'prApwa_kara' by Roja(07-05-14). Suggested by Chaitanya Sir.
;I gained a lot of information in the two-day workshop.
;mEMne xo xina kI kAryaSAlA meM bahuwa sUcanA prApwa kI. (Translation Suggested by Sukhada)
(defrule gain2
(declare (salience 4800))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gain.clp 	gain2   "  ?id "  prApwa_kara )" crlf))
)

;@@@ Added by Roja (17-05-14). Suggested by Chaitanya Sir.
;"gain","V","1.prApwa_honA"
;There is nothing to be gained from delaying the decision. [OALD]
;nirNaya ko vilambiwa karane se kuCa prApwa nahIM honA_hE/hogA
;nirNaya meM vilamba karane se kuCa prApwa nahIM honA_hE/hogA
(defrule gain3
(declare (salience 4800))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not (kriyA-object ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gain.clp     gain3   "  ?id "  prApwa_ho )" crlf))
)

;--"2.jarUrawa_se_aXika_milanA"
;His business gained him profit.
;--"3.pahuMcanA"
;After six hours of cycling,he finally gained the finishing line.
;--"4.GadZI_Axi_kA_wejI_se_calanA"
;My wrist watch gains by five minutes.

;@@@ Added by 14anu-ban-05 on 6.09.2014
;Last year through this train RTDC gained 9 crores .[TOURISM]
;पिछले  साल  इस  रेल  से  आरटीडीसी  को  साढ़े  नौ  करोड़  रुपए  का  लाभ  हुआ 
(defrule gain4
(declare (salience 4800))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1 )
(id-root ?id1 crore|lakh|million|billion)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lABa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gain.clp     gain4   "  ?id "  lABa_ho )" crlf))
)

;@@@ Added by 14anu-ban-05 on 6.09.2014
;The villagers of the state have also been associated with this business , so that they can also get economic gain .
;राज्य के ग्रामीणों को भी इस व्यवसाय से जोड़ा गया है , ताकि उन्हें भी आर्थिक लाभ मिल सके 
(defrule gain5
(declare (salience 5000))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 economic)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lABa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gain.clp     gain5   "  ?id "  lABa )" crlf))
)

;@@@ Added by 14anu-ban-05 on 6.09.2014
;At present to gain from the free economy here foreign investors are investing money .[TOURISM]
;फिलहाल यहाँ की मुक्त अर्थव्यवस्था से लाभ उठाने के लिए विदेशी निवेशक धन लगा रहे हैं ।
(defrule gain6
(declare (salience 4800))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(to-infinitive  ?id1 ?id)
(id-root ?id1 to)
(kriyA-from_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lABa_uTAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gain.clp     gain6   "  ?id "  lABa_uTAnA )" crlf))
)

;@@@ Added by 14anu07 0n 01/07/2014
;The impression that Pakistan would have a role to play had been gaining ground.
;यह भावना बलवती होने लगी थी कि पाकिस्तान की भूमिका जरूरी है 
(defrule gain7
(declare (salience 5100))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) ground)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) balavawI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gain.clp    gain7  "  ?id "  " (+ ?id 1) "  balavawI_ho)" crlf))
)


;@@@ Added by 14anu-ban-05 on (20-02-2015)
;She looked back and saw that the car was still gaining on her.[OALD]
;वह पीछे मुड़ी और देखा कि गाड़ी अभी भी उसके निकट आ रही  है.	[manual]

(defrule gain8
(declare (salience 4801))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) on)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) nikata_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gain.clp    gain8  "  ?id "  " (+ ?id 1) "  nikata_A)" crlf))
)


;@@@ Added by 14anu-ban-05 on (20-02-2015)
;His books have gained in popularity in recent years.[OALD]
;हाल  के वर्षों  में  उसकी पुस्तकों की लोकप्रियता  में वृद्धि हुई  हैं . [MANUAL]

(defrule gain9
(declare (salience 4802))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vqxXi_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gain.clp     gain9   "  ?id "  vqxXi_ho)" crlf))
)


;@@@ Added by 14anu-ban-05 on (12-03-2015)
;When the external force is removed, the body moves, gaining kinetic energy and losing an equal amount of potential energy.[NCERT]
;jaba bAhya bala hatA liyA jAwA hE wo vaswu gawi karane lagawI hE Ora kuCa gawija UrjA arjiwa kara lewI hE, waWA usa vaswu kI uwanI hI sWiwija UrjA kama ho jAwI hE.[NCERT]

(defrule gain10
(declare (salience 4803))
(id-root ?id gain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat ?id gerund_or_present_participle)
(kriyA-object  ?id ?id1)
(id-root ?id1 energy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arjiwa_kara_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gain.clp     gain10   "  ?id " arjiwa_kara_le)" crlf))
)



