;--- Commented by Nandini(6-5-14)
;(defrule lead0
;(declare (salience 5000))
;(id-root ?id lead)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id leading )
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id muKya))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  lead.clp  	;lead0   "  ?id "  muKya )" crlf))
;)

;"leading","Adj","1.muKya"
;He is a leading political thinker of the times. 
;--"2.praWama sWAna meM"
;He is the leading lady in this episode.
;
;
(defrule lead1
(declare (salience 4900))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lagA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lead.clp	lead1  "  ?id "  " ?id1 "  lagA_ho  )" crlf))
)

;There is a bathroom leading off with the bedroom in this house.
;isa Gara meM snAnaGara SayanakakRa ke sAWa lagA huA hE
(defrule lead2
(declare (salience 4800))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 prAraMBa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lead.clp	lead2  "  ?id "  " ?id1 "  prAraMBa_kara  )" crlf))
)

;She led off the discussion with an annual report.
;usane bahasa kI SuruAwa vArRika riporta se kI
(defrule lead3
(declare (salience 4700))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lalacA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lead.clp	lead3  "  ?id "  " ?id1 "  lalacA  )" crlf))
)

;She led us on to believe that we would be paid for our work.
;usane hameM yaha kahakara PusalAyA ki hameM apane kAma ke lie mehanawAnA xiyA jAegA


;$$$--- Modified by Nandini(6-5-14)
;A path led up the hill.[OLAD]
(defrule lead4
(declare (salience 4600))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 para_jA));meaning change by Nandini
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lead.clp	lead4  "  ?id "  " ?id1 "  para_jA  )" crlf))
)

;
;Pencil has lead in it.
(defrule lead5
(declare (salience 4500))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse =(- ?id 1) preposition)
(id-word =(- ?id 1) ~to) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead5   "  ?id "  sIsA )" crlf))
)

;$$$ --- Modified by Nandini(5-5-14)
;Added by sheetal
;Lead and tin are malleable metals.
(defrule lead5-a
(declare (salience 4500))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriya-object ?id1 ?id)
;(id-root ?rt metal)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp      lead5-a   "  ?id "  sIsA )" crlf))
)

;$$$ ---- Modified by Nandini(5-5-14)
;Lead and tin are malleable metals.
;sIsA Ora tina AGAwa varXanIya yA kUtya XAwu hEM.
(defrule lead6
(declare (salience 4400))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(conjunction-components  ?id1 ?id ?id2)
(subject-subject_samAnAXikaraNa  ?id1 ?id3)
(id-root ?id3 metal)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead6   "  ?id "  sIsA )" crlf))
)

;"lead","N","1.newqwva/sIsA"
;--"2.newqwva"
;He has given the lead && others may follow it. 
;--"3.baDZawa"
;His lead is now more than 10,000. 
;--"4.muKya_BUmikA{nAtaka_iwyAxi_meM}"
;He has played the lead in a film on Sardar Patel. 
;--"5.surAga"
;The lead led to the arrest of the criminal. 
;--"6.lIda{wAra}"
;There is nothing wrong with the VCR. Something has gone wrong with the lead causing disturbance in the monitor.
;--"7.sIsA"
;Pencil has lead in it.
;
(defrule lead7
(declare (salience 0))	;Salience decrease to 0 from 4300by 14anu-ban-02(19-02-2016)
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArga_xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead7   "  ?id "  mArga_xiKA )" crlf))
)

;$$$ Same rule written by 14anu19 with meaning "biwAnA" (26-06-2014)
;Without proper self-discipline it was impossible to lead a virtuous life .
;उचित आत्मानुशासन के बिना एक नैतिक जीवन चलना असम्भव था . 
;@@@--- Added by Nandini(5-5-14)
;She leads a rich and varied life. [from mail]
;vaha eka sampanna Ora vaiviXyapurNa jIvana vyawiwa karawI hE.
(defrule lead8
(declare (salience 4300))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 life)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyawiwa_karai/biwAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead8   "  ?id "  vyawiwa_kara/biwAnA )" crlf))
)

;@@@--- Added by Nandini(5-5-14)
;The wire led to a speaker.[OLAD]
;wAra spikara ko judA.
(defrule lead9
(declare (salience 4300))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 wire)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id juda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead9   "  ?id "  juda )" crlf))
)

;$$$Modified by 14anu-ban-08 (10-03-2015)     ;uncommented constraint
;@@@--- Added by Nandini(5-5-14)
;The pipe leading from the top of the water tank is leaking. [OLAD]
;pAnI taMkI ke sarvocca sWAna se jAnevAlA pAipa chu rahA hE.
;पानी की टकी के सर्वोच्च स्थान से जाने वाला पाइप रिस रहा हैं.  [self]
(defrule lead10
(declare (salience 5002))
(id-root ?id lead)
(id-word ?id leading)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI  ?id ?id1)
(id-root ?id1 top)          ;uncommented by 14anu-ban-08 (10-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnevAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead10   "  ?id " jAnevAlA )" crlf))
)


;@@@--- Added by Nandini(5-5-14)
;The track led us through a wood.[OLAD]
(defrule lead11
(declare (salience 4350))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-through_saMbanXI  ?id ?id1)(kriyA-to_saMbanXI  ?id ?id1))
(id-root ?id1 wood|conclusion)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead11   "  ?id " le_jA )" crlf))
)

;@@@--- Added by Nandini(5-5-14)
;Eating too much sugar can lead to health problems.[OLAD]
;jyAxA cInI KAnA svAsWya samasyAoM kA kAraNa_bana sakawI hE.
(defrule lead12
(declare (salience 4350))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(viSeRya-viSeRaka  ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAraNa_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead12   "  ?id " kAraNa_bana )" crlf))
)

;@@@ Added by Nandini(5-5-14)
;Which door leads to the yard?[OLAD]
;kOna sA xaravAjA ANgaNa kI ora jAwA hE?
(defrule lead13
(declare (salience 4350))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 door)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead13   "  ?id " jA )" crlf))
)

;$$$Modified by 14anu-ban-08 (10-03-2015)    ;added constraint
;@@@--- Added by Nandini(7-5-14)
;The situation is far worse than we had been led to believe.[OLAD]
;hama mAnane ke liye bAXya kiye gaye We usase bhi hAlawa Ora KarAba hE. 
;स्थिति हमारी सोच के बाध्य से ज्यादा खराब थी.  [SELF]
(defrule lead14
(declare (salience 5002))   ;salience increased 5002 by 14anu-ban-08 (10-03-2015)
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?id ?id1)
(id-root ?id1 believe)    ;added by 14anu-ban-08 (10-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAXya))  ;changed meaning from 'bAXya_ho' to 'bAXya' by 14anu-ban-08 (10-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead14   "  ?id " bAXya )" crlf))   ;changed meaning from 'bAXya_ho' to 'bAXya' by 14anu-ban-08 (10-03-2015)
)

;@@@--- Added by Nandini(7-5-14)
;The department led the world in cancer research.[OLAD]
;viBAga kEMsara saMSoXana meM viSva meM Age hE.
;$$$ by (14anu09) Vasu Vardhan[7-6-14] changed the meaning from "Age_ho" to "kA_newqwva_kara" 
;विभाग ने कैंसर शोध में विश्व का नेतृत्व किया . ( better in examples like "led the battle", "led the people")   
(defrule lead15
(declare (salience 4350))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA_newqwva_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead15   "  ?id " kA_newqwva_kara )" crlf))
)

;$$$Modified by 14anu-ban-08 (10-03-2015)     ;added constraint
;@@@--- Added by Nandini(7-5-14)
;The champion is leading by 18 seconds. [OLAD]
;vijewA 18 sekanda se Age hE.
(defrule lead16
(declare (salience 5002))  ;salience increased to 5002 by 14anu-ban-08 (10-03-2015)
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 second|hour|minute)      ;added by 14anu-ban-08 (10-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_ho))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead16   "  ?id " Age_ho )" crlf))   
)

;@@@--- Added by Nandini(7-5-14)
;Who will lead the party in the next election? [OLAD]
;agale cunAva meM pArtI kA newqwva kOna karegA?
(defrule lead17
(declare (salience 4350))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(viSeRya-in_saMbanXI  ?id1 ?id2)
;(id-root ?id2 election)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id newqwva_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead17   "  ?id " newqwva_kara )" crlf))
)

;$$$Modified by 14anu-ban-08 (10-03-2015)   ;added constraint
;$$$modified by 14anu19(26-06-2014)
;@@@--- Added by Nandini(7-5-14)
;It's your turn to lead. [OLAD]
;cAla calane kI ApakI bArI hE.
(defrule lead18
(declare (salience 5002))     ;salience increased to 14anu-ban-08 (10-03-2015)
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id1 ?id)
(id-root ?id1 turn)         ;added by 14anu-ban-08 (10-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAla_calanA))    ;cAla_chlanA is replaced by cAla_calanA
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead18   "  ?id " cAla_calanA )" crlf))
)

;@@@--- Added by Nandini(7-5-14)
;He is easily led away by other's opinions. [hinKoja]
;vaha anya kI rAya se AsAnI se praBAviwa huA hE.
(defrule lead19
(declare (salience 4900))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 praBAviwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lead.clp	lead19  "  ?id "  " ?id1 "  praBAviwa_ho  )" crlf))
)

;@@@ Added By 14anu-ban-08 (10-10-2014)
;Two large lead spheres are brought close to the small ones but on opposite sides as shown.    [NCERT]
;सीसे के दो विशाल गोलों को चित्र में दर्शाए अनुसार छोटे गोलों के निकट परन्तु विपरीत दिशाओं में लाया जाता है.     [NCERT]
(defrule lead20
(declare (salience 5000))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead20   "  ?id " sIsA )" crlf))
)

;$$$ MOdified by 14anu-ban-02(19-02-2016)
;###[COUNTER STATEMENT]This led to the development of a radically new theory (Quantum Mechanics) to deal with atomic and molecular phenomena.[NCERT 11_01]
;###[COUNTER STATEMENT]इससे परमाण्वीय तथा आण्विक परिघटनाओं पर विचार करने के लिए मूलतः नए सिद्धान्त (क्वान्टम यान्त्रिकी) के विकास का मार्ग प्रशस्त हुआ.[NCERT 11_01]
;@@@ Added By 14anu-ban-08 (10-10-2014)
;Electric force manifests itself in atmosphere where the atoms are ionised and that leads to lightning.  [NCERT]
;वैद्युत बल स्वयं वातावरण, जहाँ परमाणु आयनीकृत होते हैं, में प्रकट होता है और इसी के कारण तडित दमकती है.         [NCERT]
(defrule lead21
(declare (salience 5000))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
;(id-root =(+ ?id 1) to)	;commented by 14anu-ban-02(19-02-2016)
(kriyA-to_saMbanXI  ?id ?id1)	;Added by 14anu-ban-02(19-02-2016)
(id-root ?id1 lightning)	;Added by 14anu-ban-02(19-02-2016)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  ke_kAraNa ))	;;commented by 14anu-ban-02(19-02-2016)
(assert (id-wsd_root_mng ?id ke_kAraNa))	;Added by 14anu-ban-02(19-02-2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead21   "  ?id " ke_kAraNa )" crlf))
)	;Added by 14anu-ban-02(19-02-2016)
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " lead.clp  lead21   "  ?id "  " ( + ?id 1) " ke_kAraNa )" crlf))
;)	;commented by 14anu-ban-02(19-02-2016)


;$$$ Modified by 14anu-ban-02(19-02-2016)
;This human endeavor led, in course of time, to modern science and technology.[ncert 11_01]
;कालान्तर में मानव के इन्हीं प्रयासों से आधुनिक विज्ञान तथा प्रौद्योगिकी का मार्ग प्रशस्त हुआ है.[ncert 11_01]
;@@@ Added by 14anu-ban-09 on (21-10-2014)
;This led to the development of a radically new theory (Quantum Mechanics) to deal with atomic and molecular phenomena.  [NCERT CORPUS]
;isase paramANvIya waWA ANvika pariGatanAoM para vicAra karane ke lie mUlawaH nae sixXAnwa (kvAntama yAnwrikI) ke vikAsa kA mArga praSaswa huA. [NCERT CORPUS]

(defrule lead22
(declare (salience 4350))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 development|science|technology)	;science,technology added bu 14anu-ban-02(19-02-2016)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArga_praSaswa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead22   "  ?id " mArga_praSaswa_ho )" crlf))
)

;@@@Added by 14anu-ban-08 (07-03-2015)
;He led them from poverty to richness.  [given by soma mam]
;वह उनको दरिद्रता से धनाढ्यता की ओर ले गया. [self]
(defrule lead23
(declare (salience 4500))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 them)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora_le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead23   "  ?id "  kI_ora_le_jA )" crlf))
)

;@@@Added by 14anu-ban-08 (07-03-2015)
;Eating a lot of sugar may lead to health problem.  [given by soma mam]
;बहुत अधिक मीठा खाना स्वास्थ्य की समस्या का कारण हो सकता हैं.  [self]
(defrule lead24
(declare (salience 4000))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA_kAraNa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead24   "  ?id "  kA_kAraNa_ho )" crlf))
)

;Commented by 14anu-ban-02(19-02-2016)
;repeated rule
;@@@Added by 14anu-ban-08 (07-03-2015)
;This led to the development of a radically new theory (Quantum Mechanics) to deal with atomic and molecular phenomena.  [NCERT]
;इससे परमाण्वीय तथा आण्विक परिघटनाओं पर विचार करने के लिए मूलतः नए सिद्धान्त (क्वान्टम यान्त्रिकी) के विकास का मार्ग प्रशस्त हुआ.  [NCERT]
;(defrule lead25
;(declare (salience 5001))
;(id-root ?id lead)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-to_saMbanXI ?id ?id1)
;(id-root ?id1 development)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id  mArga_praSaswa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead25   "  ?id "   mArga_praSaswa )" crlf))
;)

;@@@Added by 14anu-ban-08 (08-04-2015)
;The wire led to a speaker.  [oald]
;तार स्पीकर से लगा हुआ हैं.  [self]
(defrule lead26
(declare (salience 5002))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 wire)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  lagA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead26   "  ?id "   lagA_ho )" crlf))
)


;@@@Added by 14anu-ban-08 (08-04-2015)
;She took the lead in the second lap.  [oald]
;दूसरे चक्कर में वह प्रथम स्थान पर रही.  [self]
(defrule lead27
(declare (salience 5012))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(kriyA-in_saMbanXI ?id1 ?id2)
(id-root ?id1 take)
(id-root ?id2 lap)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  praWama_sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead27   "  ?id "   praWama_sWAna )" crlf))
)

;@@@Added by 14anu-ban-08 (08-04-2015)
;You go first, I'll take my lead from you.  [oald]
;तुम पहले जाओ मैं तुमसे उदाहरण लूँगी.  [self]
(defrule lead28
(declare (salience 5002))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 take)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  uxAharaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead28   "  ?id "   uxAharaNa )" crlf))
)

;@@@Added by 14anu-ban-08 (13-04-2015)
;Which door leads to the yard?  [oald]
;कौन सा दरवाजा आंगन की ओर गया हैं.  [self]
(defrule lead29
(declare (salience 5002))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 yard|door|road)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead29   "  ?id "  kI_ora_jA )" crlf))
)

;@@@Added by 14anu-ban-08 (13-04-2015)
;The champion is leading by 18 seconds.  [oald]
;विजेता 18 सेकड़ से आगे बढ़ा हैं.  [self]
(defrule lead30
(declare (salience 5002))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 champion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_baDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead30   "  ?id "  Age_baDZA )" crlf))
)

;@@@Added by 14anu-ban-08 (13-04-2015)
;The pipe leading from the top of the water tank.  [oald]
;पाइप पानी की टंकी को ऊपर से जोड़ता हैं.  [self]
(defrule lead31
(declare (salience 5002))
(id-root ?id lead)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(kriyA-from_saMbanXI ?id ?id2)
(id-root ?id1 pipe)
(id-root ?id2 top)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lead.clp 	lead31   "  ?id "  jodZa )" crlf))
)

;We lead the way in space technology.[OLAD]; parse problem
;
;"lead","V","1.mArga_xiKAnA"
;He led me to his room. 
;--"2.mArga_milanA"
;The trail of blood on the road led the police to the criminal. 
;--"3.praBAviwa_karanA[honA]"
;He is easily led away by other's opinions. 
;--"4.vyawIwa_karanA"
;Many people lead a miserable life due to poverty. 
;--"5.Age_honA"
;This candidate is leading by 10,000 votes. 
;--"6.cAla_Suru_karanA{wASa_Axi_meM}"
;He led a heart. 
;--"7.newqwva_karanA"
;The captain led his soldiers from the front.
;
;LEVEL 
;Headword : lead
;
;Examples --
;
;"lead","V","1.newqwva karanA"
;The captain led his soldiers to the front.
;kapwAna ne apane sipAhiyoM kA sImA kI ora newqwva kiyA.
;--"2.le_jAnA"
;He led me to his room.
;vaha muJe apane kamare waka le gayA.
;--"3.mArga xiKAnA"
;The trail of blood on the road led the police to the criminal.
;sadZaka para banI KUna kI lakIra ne pulisa ko aparAXI waka pahuzcane kA mArga xiKAyA.
;--"4.praBAviwa karanA[honA]"
;He is easily led away by other's opinions.
;vaha xUsaroM ke vicAroM se jalxI hI praBAviwa ho jAwA hE.
;--"5.vyawIwa karanA"
;Many people lead a miserable life due to poverty.
;garIbI ke kAraNa bahuwa se loga kaRtakara jIvana vyawIwa kara rahe hEM.
;--"6.Age honA"
;This candidate is leading by 10,000 votes.
;yaha prawyASI 10,000 votoM se Age hE.
;--"7.cAla_Suru_karanA{wASa_Axi_meM}"
;He led a heart.
;usane pAna se cAla Suru kI.
;
;"lead","N","1.newqwva"
;The government organizations should give a lead in anti-corruption drive.
;BraRtAcAra ke viruxXa ladZAI meM sarakArI saMsWAoM ko newqwva karanA cAhiye.
;--"2.prAraMBa"
;He has given the lead & others may follow it.
;newqwva vaha kara rahA hE bAkI loga usakA anusaraNa kareM.
;--"3.baDZawa"
;His lead is now more than 10,000.
;usakI baDZawa aba 10,000 se aXika hE.
;--"4.muKya BUmikA{nAtaka iwyAxi meM}"
;He has played the lead in a film on Sardar Patel.
;usane saraxAra patela para banI eka Pilma meM muKya BUmikA niBAI hE.
;--"5.surAga"
;The lead led to the arrest of the criminal.
;surAga kI vajaha se aparAXI pakadZA gayA.
;--"6.lIda{wAra}"
;
;There is nothing wrong with the VCR. Something has gone wrong with the lead caus
;ing disturbance in the monitor.
;
;vI.sI.Ara. meM kuCa BI KarAba nahIM hE. 'lIda' meM kuCa KarAba ho gayA hE jisake kAraNa mOYnItara meM gadZabadZI A rahI hE.
;--"7.sIsA"
;Pencil has lead in it.
;pEMsila meM sIsA howA hE.
;
;
;ukwa uxAharaNoM meM 'lead' kA pramuKa arWa 'newqwva_karanA' uBara kara AwA hE.
;'newqwva_karanA' yAni 'mArga_xiKAnA', 'mArga_xiKAne' meM svayaM 'Age_honA'. 
;mArga_xiKAne se mArga para le jAnevAlA arWa AwA hE. Ora lejAnevAlA Age howA hE awaH Age honA arWa judZa jAwA hE. newqwva karanA se Upara xiye uxAharaNoM meM Aye SeRa arWa
;kEse judZe hEM yaha nimna anwarnihiwa sUwra meM xeKiye -
;
;
;anwarnihiwa sUwra ; 
;
;                       
;                           newqwva_karanA (kriyA) --- newqwva (saMjFA)
;                              |              
;                              |
;                           mArga_xiKAnA
;                              |
;                              |
;                           SuruAwa_karanA ------------ prAraMBa                 
;                              |
;                              |
;                   ----------Age_honA <--------- mArga
;                   |          |           |            
;                   |          |           |           
;                   |    (karwA)pramuKa_BUmikA -------|    
;                   |              |    |      |----|---------|
;          (kiwanI xUrI se Age hE)  |    |           |         |
;                  baDZawa            |   vyawIwa_karanA  lIda(wAra)  surAga
;                                  |
;                                praBAviwa_karanA
;
;
;
;Upara xiye anwarnihiwa sUwra se yaha spaRta hE ki ukwa uxAharaNoM ke prAyaH saBI arWa bIjArWa 'newqwva_karanA' se judZe hEM, mAwra 'sIsA' alaga hE. awaH isakA sUwra hogA -
;
;sUwra : newqwva_karanA[>le_jAnA]/sIsA 
