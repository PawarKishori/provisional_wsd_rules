
(defrule enlarge0
(declare (salience 5000))
(id-root ?id enlarge)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 apavqxXi_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " enlarge.clp	enlarge0  "  ?id "  " ?id1 "  apavqxXi_kara  )" crlf))
)

;@@@ Added by 14anu-ban-04 (06-12-2014)
;Reading will enlarge your vocabulary.[oald]
;अध्ययन आपकी शब्दावली को बढाएगा . [manual]
(defrule enlarge2
(declare (salience 4910))
(id-root ?id enlarge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1) 
(id-root ?id1 vocabulary|area|holding)             ;added by 14anu-ban-04 (28-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDA)) 
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  enlarge.clp      enlarge2   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  enlarge.clp 	enlarge2   "  ?id "  baDA)" crlf))
)
;--------------------- Default rules ---------------------

;Would you like to enlarge on your original statement.
;kyA wuma apane vAswavika kaWana meM apavqxXi karanA cAhoge ?
(defrule enlarge1
(declare (salience 4900))
(id-root ?id enlarge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA_kara)) ; Modified meaning "baDZA" to "badA_kara" by Roja(11-04-12) ;Suggested by Sukhada.
					 ; Ex: The boys' bedrooms will be enlarged.
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  enlarge.clp 	enlarge1   "  ?id "  badA_kara )" crlf))
)

;"enlarge","VI","1.baDZAnA"
;That building has been considerably enlarged.
;
;
