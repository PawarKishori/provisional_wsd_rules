;@@@Added by 14anu-ban-02(17-03-2015)
;She gave a bashful smile as he complimented her on her work.[cald]
;उसने एक नम्र मुस्कराहट दी जब उसने उसके कार्य पर उसकी प्रशंसा की. [self]
(defrule bashful1 
(declare (salience 100)) 
(id-root ?id bashful) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 smile) 
=> 
(retract ?mng)  
(assert (id-wsd_root_mng ?id namra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bashful.clp  bashful1  "  ?id "  namra )" crlf)) 
) 


;--------------------default_rules-----------------------------------------

;@@@Added by 14anu-ban-02(17-03-2015)
;Sentence: When we asked her if she had a boyfriend, she came over all bashful and wouldn’t say a thing.[oald]
;Translation: जब हमने उससे  पूछा यदि उसका कोई प्रेमी था तो तो वह पूरी तरह से  लज्जाशील हुई और कुछ नहीं कह पायी . [self]
(defrule bashful0 
(declare (salience 0)) 
(id-root ?id bashful) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng)  
(assert (id-wsd_root_mng ?id lajjASIla)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bashful.clp  bashful0  "  ?id "  lajjASIla )" crlf)) 
) 
