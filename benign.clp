;@@@Added by 14anu-ban-02(21-03-2015)
;You would never have guessed his intentions from the benign expression on his face.[oald]
;उसके चेहरे पर सौम्य हाव-भाव से आप उसकी नीयत का अन्दाज कभी नहीं  लगा पायेंगे.[self] 
(defrule benign1 
(declare (salience 100)) 
(id-root ?id benign) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 expression) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sOmya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  benign.clp  benign1  "  ?id "  sOmya )" crlf)) 
) 

;@@@Added by 14anu-ban-02(21-03-2015)
;The industry's environmental impact is relatively benign, even positive. [oald]
;उद्योग का पर्यावरण सम्बन्धी प्रभाव तुलनात्मक रूप से अनुकूल, यहाँ तक कि सकारात्मक भी है . [self]
(defrule benign2 
(declare (salience 100)) 
(id-root ?id benign) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 impact)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id anukUla)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  benign.clp  benign2  "  ?id "  anukUla )" crlf)) 
) 

;@@@Added by 14anu-ban-02(21-03-2015)
;A benign tumour.[cald]
;एक मामूली गिल्टी .[self]
(defrule benign3 
(declare (salience 100)) 
(id-root ?id benign) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 tumour) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id mAmUlI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  benign.clp  benign3  "  ?id "  mAmUlI )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(21-03-2015)
;Sentence: A benign old lady.[cald]
;Translation: एक दयालु वृद्ध महिला . [self]
(defrule benign0 
(declare (salience 0)) 
(id-root ?id benign) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id xayAlu)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  benign.clp  benign0  "  ?id "  xayAlu )" crlf)) 
) 
