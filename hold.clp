(defrule hold0
(declare (salience 5000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id holding )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aXisaMpawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hold.clp  	hold0   "  ?id "  aXisaMpawwi )" crlf))
)

;$$$ Modified by 14anu-ban-06 (08-12-2014)
;@@@ Added by 14anu23 21/06/2014
;The drain should be wide enough to hold dung and urine , which can be removed at regular intervals .
;नाली इतनी चौडी होनी चाहिए कि उसमें और पेशाब समा सके . गोबर को निश्चित अन्तराल के बाद हटाते रहना चाहिए . 
;गोबर और पेशाब को समाने के लिए नाली काफी चौडी होनी चाहिए  जो नियमित अन्तराल में हटाया जा सकता है . ;added by 14anu-ban-06 (08-12-2014)
(defrule hold_tmp
(declare (salience 5500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(to-infinitive ?id1 ?id)
(kriyA-object ?id ?id2);added by 14anu-ban-06 (08-12-2014) to avoid clash with 'hold3/11/32'
(id-root ?id2 dung|urine);added by 14anu-ban-06 (08-12-2014) to avoid clash with 'hold3/11/32'
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id samA));meaning changed from 'raKa' to 'samA' by 14anu-ban-06 (08-12-2014)
(assert (kriyA_id-object_viBakwi ?id ko));added by 14anu-ban-06 (08-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  hold.clp  	hold_tmp   "  ?id "  samA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hold.clp       hold_tmp   "  ?id " ko )" crlf);added by 14anu-ban-06 (08-12-2014)
)
)

;"holding","N","1.aXisaMpawwi"
;nyAyAXISa ne'holding'(KewIbAdZI)kA AXA hissA usake BAI ko xene kA PEsalA xiyA.
;
;

;$$$ Modified by Prachi Rathore[03-12-13]
;Meaning changed from bawAne_se_iMkAra_kara_xe to roka_le  and added object_viBakwi 'ko'
(defrule hold1
(declare (salience 4900))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 roka_le))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold1  "  ?id "  " ?id1 "  roka_le  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hold.clp     hold1   "  ?id "  ko )" crlf))
)

;She wanted to tell all the things to her father but her mother held her back.
;vaha apane piwA ko saba kuCa bawA xenA cAhawI WI lekina usakI mAz ne bawAne se iMkAra kara xiyA
(defrule hold2
(declare (salience 4800))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bane_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold2  "  ?id "  " ?id1 "  bane_raha  )" crlf))
)

;He's unable to hold down in this job.
;vaha isa nOkarI meM bane rahane ke lie asamarWa hE
(defrule hold3
(declare (salience 4700))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 forth)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAPI_samaya_waka_lagAwAra_bolawe_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold3  "  ?id "  " ?id1 "  kAPI_samaya_waka_lagAwAra_bolawe_raha  )" crlf))
)

;I sat quietly in a corner while he held forth.
;mEM cupacApa kone meM bETA rahA Ora vaha kAPI samaya waka lagAwAra bolawA rahA
(defrule hold4
(declare (salience 4600))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ruka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold4  "  ?id "  " ?id1 "  ruka  )" crlf))
)

;Hold on! i am not ready.
;ruko! aBI mEM wEyAra nahIM hUz
(defrule hold5
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CipA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold5  "  ?id "  " ?id1 "  CipA  )" crlf))
)

;I don't know why he is holding out on us everything like this.
;mEM nahIM jAnawA ki vo hamase saba kuCa isa waraha kyoM CipA rahA hE
(defrule hold6
(declare (salience 4400))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 with)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samarWana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold6  "  ?id "  " ?id1 "  samarWana_kara  )" crlf))
)

;He doesn't hold with smoking.
;vaha XUmrapAna kA samarWana kaBI nahIM karawA
(defrule hold7
(declare (salience 4300))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id1  hand)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 upara_uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold7  "  ?id "  " ?id1 "  upara_uTA  )" crlf))
)

(defrule hold8
(declare (salience 4200))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sazBAla));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " hold.clp hold8 " ?id "  sazBAla )" crlf)) 
)

(defrule hold9
(declare (salience 4100))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sazBAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold9  "  ?id "  " ?id1 "  sazBAla  )" crlf))
)

;$$$ Modified by 14anu-ban-06 (26-11-2014)
;Always hold your head erect.  (head.clp)[parser no. 18]
;अपना सिर सदैव सीधा रखो. (manual)
;I hold the door open for her.(COCA)
;मैं दरवाजा उसके लिए खुला रखता हूँ .(manual)
;I'll hold the door open for you.(OALD) 
;मैं दरवाजा आपके लिए खुला रखूँगा .(manual) 
;Added by human beings
(defrule hold10
(declare (salience 4000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1);added by 14anu-ban-06 (26-11-2014)
(object-object_samAnAXikaraNa ?id1 ?id2);added by 14anu-ban-06 (26-11-2014)
(id-word ?id1 door|head);added by 14anu-ban-06 (26-11-2014);added 'head' by 14anu-ban-06 (31-01-2015)
(id-word ?id2 open|erect);added 'erect' by 14anu-ban-06 (31-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold10   "  ?id "  raKa )" crlf))
)

;@@@ Added by Prachi Rathore[03-12-13]
;In the case of the contracting cloud, the gravity was not sufficient to hold back the gaseous material, which therefore tended to fly away from the axis of rotation.
;सिकुड़ते हुए बादल के मामले में गुरुत्व इतना नहीं था कि गैसीय पदार्थ को रोक सकता।
(defrule hold11
(declare (salience 5000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ? ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 roka_sakane_meM))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold11  "  ?id "  " ?id1 "  roka_sakane_meM  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hold.clp     hold11   "  ?id "  ko )" crlf))
)

;@@@ Added by Prachi Rathore[03-12-13]
;I wanted to tell him the truth, but something held me back. [OALD]
;मैंने  उसको सच्चाई बताना  चाही, परन्तु किसी ने मुझे रोक लिया .
(defrule hold12
(declare (salience 5000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id ?id2)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(id-root ?id2 back)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 roka_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold12  "  ?id "  " ?id2 "  roka_le  )" crlf))
)

;@@@ Added by Prachi Rathore[03-12-13]
;I think he's holding something back.[OALD]
;मैं सोचता हूँ कि वह कुछ छिपा रहा है . 
(defrule hold13
(declare (salience 4900))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id ?id2)
(id-cat_coarse ?id verb)
(id-root ?id2 back)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 CipA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold13  "  ?id "  " ?id2 "  CipA )" crlf))
)

;$$$ Modified by 14anu-ban-06   (12-08-2014)  added 'meeting|volume' to the id-root
;It was further reported that Long held a meeting.(COCA)
;yaha Ora sUciwa kiyA gayA ki loMga ne bETaka raKI.
;@@@   ---Added by Prachi Rathore
; The King held a council at Nottingham from 14 to 19 October 1330.
;राजा ने अक्तूबर 1330 14 से 19 से नोट्टिंगम में परिषद रखा . 
(defrule hold14
(declare (salience 4900))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 council|meeting|volume)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold14   "  ?id "  raKa )" crlf))
)

;@@@   ---Added by Prachi Rathore
;Hold that pose, It will make a great photograph.[m-w]
; ;वह मुद्रा बनाये रखिए, यह एक बढिया फोटो बनाएगा . 
(defrule hold15
(declare (salience 4000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 pose)
(kriyA-vAkyakarma ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banAye_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold15   "  ?id "  banAye_raKa )" crlf))
)

;$$$ Modified by 14anu-ban-06 (05-09-2014)
;When you hold a pencil in front of you against some specific point on the background (a wall) and look at the pencil first through your left eye A (closing the right eye) and then look at the pencil through your right eye B (closing the left eye), you would notice that the position of the pencil seems to change with respect to the point on the wall.(NCERT)  ;added by 14anu-ban-06 (14-11-2014)
;जब आप किसी पेंसिल को अपने सामने पकडते हैं और पृष्ठभूमि (माना दीवार) के किसी विशिष्ट बिन्दु के सापेक्ष पेंसिल को पहले अपनी बायीं आँख A से (दायीं आँख बन्द रखते हुए) देखते हैं, और फिर दायीं आँख B से (बायीं आँख बन्द रखते हुए), तो आप पाते हैं, कि दीवार के उस बिन्दु के सापेक्ष पेंसिल की स्थिति परिवर्तित होती प्रतीत होती है.(NCERT)
;@@@ Added by Prachi Rathore[16-1-14]
;I held the mouse by its tail.[oald]
;मैंने चूहे को उसकी पूँछ से पकडा . 
(defrule hold16
(declare (salience 4000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 mouse|pencil);added by 14anu-ban-06
;added 'pencil' by 14anu-ban-06 (14-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakada))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold16   "  ?id "  pakada )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hold.clp    hold16   "  ?id "  ko )" crlf))
)

;@@@ Added by Prachi Rathore[16-1-14]
;He has held the post for three years.[cambridge]
;उसने तीन वर्षों से पद संभाल रखा है . 
(defrule hold17
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 post)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMBAla_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold17   "  ?id "  saMBAla_raKa )" crlf))
)

;@@@ Added by Prachi Rathore[16-1-14]
;The girl held her father's hand tightly.[oald]
;लडकी ने उसके पिता का हाथ कसकर पकडा . 
(defrule hold18
(declare (salience 3000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold18   "  ?id "  pakada )" crlf)
)
)

;$$$ Modified by 14anu-ban-06   (31-07-2014) added mass to root
;A weightlifter holding a 150 kg mass steadily on his shoulder for 30 s does no work on the load during this time.   (NCERT)
;koI BArowwolaka 150 @kg xravyamAna ke BAra ko 30 @s waka apane kanXe para lagAwAra uTAe hue KadA hE wo vaha koI kArya nahIM kara rahA hE .
;@@@ Added by Prachi Rathore[16-1-14]
;He was holding the baby in his arms.[oald]
;वह उसकी बाहों में शिशु को उठाये हुए है . 
(defrule hold19
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 baby|mass) ; added 'mass' by 14anu-ban-06   (31-07-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold19   "  ?id "  uTA)" crlf)
)
)

;@@@ Added by Prachi Rathore[20-1-14]
;These nuts and bolts hold the wheels on.
;ये नट और बोल्ट पहिये को रोकते हैं . 
(defrule hold20
(declare (salience 4600))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 on)
(kriyA-object  ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold20  "  ?id "  " ?id1 "  roka  )" crlf))
)

;$$$ Modified by Bhagyashri Kulkarni (15-09-2016)
;All meetings are held in this room.
;सारी बैठकें इस कमरे में आयोजित की जातीं हैं.
;$$$ Modified by 14anu-ban-06   (12-08-2014) added prayer|keertana to id-root
;Prayers and keertanas are held by the committee everyday in this temple .(tourism corpus)
;इस  मन्दिर  में  समिति  द्वारा  नित्य  पूजा  ,  अर्चना  ,  कीर्तन  भजन  आयोजित किए जाते   हैं. 
;@@@ Added by Prachi Rathore[21-2-14]
;Peace talks were held to try to heal the growing rift between the two sides.[cambridge]
;अमन वर्ता दोनों तरफ के बीच बढती हुई दरार मिटाने का प्रयास करने के लिये आयोजित की गयी थी . 
;A press conference is being held as we speak. [coca]
(defrule hold21
(declare (salience 4600))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 talk|conference|prayer|keertana|meeting) ; added 'prayer|keertana' by  14anu-ban-06   (12-08-2014) ;added 'meeting' by Bhagyashri Kulkarni (15.09.2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayojiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold21   "  ?id "  Ayojiwa_kara)" crlf)
)
)

;@@@ Added by Prachi Rathore[21-2-14]
;Pujols said during a news conference held at a nearby luxury hotel.[coca]
;पुजोलस् ने कहा एक पास वाले समृद्धि होटल में आयोजित एक समाचार सम्मेलन के दौरान
(defrule hold22
(declare (salience 4600))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(saMjFA-to_kqxanwa  ?id1 ?id)
(id-root ?id1 talk|conference)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayojiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold22   "  ?id "  Ayojiwa)" crlf)
)
)

;@@@ Added by Prachi Rathore[21-2-14]
;Can you hold your own against him ?   ;shiksharthi
;क्या तुम उसका डटकर सामना कर सकते हो ?
(defrule hold23
(declare (salience 4600))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 own)
(kriyA-against_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold23   "  ?id "  sAmanA_kara )" crlf))
)

;@@@ Added by Prachi Rathore[21-2-14]
;He still holds his own.   ;shiksharthi
;वह अभी भी अपने आपे में है.
(defrule hold24
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 own)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold24   "  ?id "  - )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hold.clp    hold24   "  ?id "  meM )" crlf))
)

;@@@ Added by Prachi Rathore[27-3-14]
;We could get a new computer now or hold off until prices are lower. [oald]
;हम अब एक नया सङ्गणक ले सकते हैं या  मूल्य कम होने तक रुक सकते है. 
(defrule hold25
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ruka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold25  "  ?id "  " ?id1 "  ruka  )" crlf))
)


;@@@ Added by Prachi Rathore[27-3-14]
 ; An accident is holding up traffic.[oald]
; एक दुर्घटना यातायात रोक रही है .
(defrule hold26
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
(id-root ?id2 accident|application)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold26  "  ?id "  " ?id1 "  roka  )" crlf))
)


;@@@ Added by Prachi Rathore[27-3-14]
;She's always holding up her children as models of good behaviour. [oald]
;वह हमेशा उसके बच्चों को सद्व्यवहार के नमूने (माडेल) की तरह   प्रस्तुत करती रहती है . 
(defrule hold27
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?)
(kriyA-object  ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 praswuwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold27  "  ?id "  " ?id1 "  praswuwa_kara )" crlf))
)


;@@@ Added by Prachi Rathore[27-3-14]
 ;Masked men held up a security van in South London yesterday.[oald]
;नकाबधारी आदमियों ने कल दक्षिण लन्दन में एक सुरक्षा वैन लुटी . 
(defrule hold28
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 van)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 luta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold28  "  ?id "  " ?id1 "  luta )" crlf))
)

;@@@ Added by 14anu20 on 19/06/2014
;I hold up him as an example.
;मैं एक उदाहरण की तरह उसको पेश करता हूँ . 
(defrule hold09
(declare (salience 6200))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id2)
(id-cat_coarse ?id2 noun|adjective)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 peSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold09  "  ?id "  " ?id1 "  peSa_kara  )" crlf))
)



;@@@ Added by 14anu18 (02-07-14)
;The king holds the maximum power.
;राजा के पास अधिकतम शक्ति होती है .
(defrule hold28_1
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 power|authority|share|property|title)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ke_pAsa))
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold28_1   "  ?id "  ho)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hold.clp 	hold28_1    "  ?id " ke_pAsa )" crlf))

)

;@@@ Added by 14anu26    [28-06-14]
;"मैं ममता के साथ औपचारिक बातचीत करूँगा," वह कहता है . 
;"I will hold formal talks with Mamata , " he says .
(defrule hold29
(declare (salience 4600))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 talk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold29   "  ?id "  kara)" crlf))
)

;@@@ Added by 14anu-ban-06 (28-07-2014)
;On the occasion of Dussehra a huge fair is also held in the field of Kullu.  (Parallel Corpus)
;दशहरे के अवसर पर कुल्लू के मैदान में एक भारी मेला भी लगता है ।
(defrule hold30
(declare (salience 5000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma  ?id ?id1)
(id-root ?id1 fair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold30   "  ?id "  laga )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-08-2014)
;When temperature is held constant, the pressure and volume of a quantity of gas are related as PV = constant.  (NCERT)
;jaba wApa ko niyawa raKA jAwA hE, wo kisI gEsa kI niSciwa mAwrA kA xAba waWA Ayawana @PV = niyawAMka ke rUpa meM saMbaMXiwa howe hEM.
;When the pressure is held constant, the volume of a quantity of the gas is related to the temperature as V/T = constant.  (NCERT)
;jaba xAba ko niyawa raKawe hEM, wo kisI niSciwa parimANa kI gEsa kA Ayawana usake wApa se isa prakAra saMbaMXiwa hE: @V/@T = niyawAMka.
;If the gas is held under constant pressure during the heat transfer, then it is called the molar specific heat capacity at constant pressure and is denoted by C p. (NCERT)
;yaxi URmA sWAnAMwaraNa ke samaya gEsa kA xAba niyawa raKA jAwA hE, wo ise niyawa xAba @C @p xvArA nirxiRta kiyA jAwA hE.
(defrule hold31
(declare (salience 5100))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma  ?id ?id1)
(id-root ?id1 temperature|pressure|gas)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold31   "  ?id "  raKa )" crlf))
)

;@@@ Added by 14anu-ban-06   (07-08-2014)
;Try to hold breath when there is lightening .  (Parallel Corpus)
;बिजली गिरने पर साँस रोकने का प्रयास करें ।
(defrule hold32
(declare (salience 5200))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 breath)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold32   "  ?id "  roka )" crlf))
)
;@@@ Added by 14anu-ban-06 (10-10-2014)
;However, it is a natural view that anyone would hold from common experience.(NCERT)
;waWApi, yaha eka svABAvika vicAra hE, jo koI BI vyakwi apane sAmAnya anuBavoM se raKa sakawA hE.(NCERT)
(defrule hold33
(declare (salience 4500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI ?id ?id1)
(id-root ?id1 experience)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold33   "  ?id "  raKa )" crlf))
)

;@@@ Added by 14anu-ban-06   (14-10-2014)
;The above expression holds when the medium is vacuum.(NCERT)
;उपरोक्त समीकरण तब लागू होता है जबकि माध्यम निर्वात होता है.(NCERT)
;Equation (4.4) holds for a straight rod.(NCERT)
;समीकरण (4.4) सीधी छड पर लागू होती है.(NCERT)
;The same principle holds good when we measure the weight of an object by a spring balance hung from a fixed point e.g. the ceiling.(NCERT)
;यही सिद्धान्त उस समय लागू होता है जब हम किसी स्थिर बिन्दु, जैसे छत से लटकी किसी कमानीदार तुला से किसी पिण्ड का भार मापते हैं.(NCERT)
(defrule hold34
(declare (salience 5250))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 principle|law|rule|equation|expression);added 'equation' by 14anu-ban-06 (24-11-2014)
         ;added 'expression' by 14anu-ban-06 (04-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAgU_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold34   "  ?id "  lAgU_ho )" crlf))
)

;@@@ Added by 14anu22.
;It has been put on hold.
;उस्से रोके रखा गया है.
(defrule hold35
(declare (salience 7000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 put)
(kriyA-on_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id roke_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold35  "  ?id1 "  " ?id "  roke_raKa)" crlf))
)

;$$$ Modified by 14anu-ban-06 (09-12-2014)
;@@@ Added by 14anu22.
;I hold you responsible for it.
;मैं इसके लिये तुम्हे जिम्मेदार ठहराता हू.
(defrule hold36
(declare (salience 7000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 responsible)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(kriyA-object ?id ?id2)
;(object-object_samAnAXikaraNa ?id1 ?id2);commented by 14anu-ban-06 (09-12-2014)
(object-object_samAnAXikaraNa ?id2 ?id1);added by 14anu-ban-06 (09-12-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jimmexAra_TaharA));modified '?id2' to '?id1' by 14anu-ban-06 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold36  "  ?id "  " ?id1 "  jimmexAra_TaharA)" crlf))
)

;$$$ Modified by 14anu-ban-06 (12-12-2014)
;@@@ Added by 14anu22.
;Hold on to me.
;मझे पकडे रखो.
(defrule hold37
(declare (salience 8500))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) on)
(id-word =(+ ?id 2) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) pakade_raKa));meaning changed from 'pakade_raha' to 'pakade_raKa' by 14anu-ban-06  (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold37 "  ?id "  " (+ ?id 1) " " (+ ?id 2) " pakade_raKa)" crlf))
)

;$$$ Modified by 14anu-ban-06 (09-12-2014)
;@@@ Added by 14anu22.
;It took hold on me.
;उसने मुझे आधीन कर लिया.
(defrule hold38
(declare (salience 8000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 take); modified 'id-word' as 'id-root' by 14anu-ban-06 (09-12-2014)
(kriyA-object  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id AXIna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hold.clp	hold38  "  ?id1 "  " ?id "  AXIna_kara)" crlf))
)

;@@@ Added by 14anu-ban-06 (27-01-2015)
;The court of Gayasuddin Khilji used to be held here .(parallel corpus)
;गयासुद्दीन  खिलजी  का  दरबार  यहाँ  लगता  था  ।(parallel corpus)
(defrule hold39
(declare (salience 5000))
(id-root ?id hold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 court|camp)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hold.clp 	hold39   "  ?id "  laga )" crlf))
)
