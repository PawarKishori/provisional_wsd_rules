;@@@ Added by 14anu-ban-06 (24-04-2015)
;The inhibition of growth.(OALD)
;विकास को रोकना . (manual)
(defrule inhibition1
(declare (salience 2000))
(id-root ?id inhibition)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rokanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inhibition.clp 	inhibition1   "  ?id "  rokanA )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (24-04-2015)
;She had no inhibitions about making her opinions known. (OALD)
;उसे उसकी राय विदित बनाने के बारे में कोई संकोच नहीं था . (manual)
(defrule inhibition0
(declare (salience 0))
(id-root ?id inhibition)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkoca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inhibition.clp 	inhibition0   "  ?id "  saMkoca )" crlf))
)
