;@@@ Added by 14anu-ban-06 (23-03-2015)
;She has been harried by the press all week.(OALD)
;वह पूरे सप्ताह प्रैस के द्वारा तङ्ग की गयी है . (manual)
(defrule harry0
(declare (salience 0))
(id-root ?id harry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wafga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harry.clp 	harry0   "  ?id "  wafga_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (23-03-2015)
;They harried the retreating army. (OALD)
;उन्होंने पीछे हटती हुई सेना पर बार-बार आक्रमण किया . (manual)
(defrule harry1
(declare (salience 2000))
(id-root ?id harry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 army)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAra-bAra_AkramaNa_kara))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harry.clp 	harry1   "  ?id "  bAra-bAra_AkramaNa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  harry.clp       harry1   "  ?id " para )" crlf))
)
