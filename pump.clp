
;$$$ Modified by 14anu-ban-09 on (12-01-2015)
;Changed meaning from 'uwsAhiwa' to 'uwsAhiwa_kiyA_jA'
;@@@ Added by 14anu04 on 24-June-2014
;He was pumped up to go for gold in these games. 
;वह इन खेलों में सोना लाने के लिए उत्साहित गया था .
;उसे इन खेलों में सोना लाने के लिए उत्साहित किया गया था. [self]     ;Translation modified by 14anu-ban-09 on (12-01-2015)

(defrule pump_tmp
(declare (salience 5050))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1) 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwsAhiwa_kiyA_jA)) ;changed meaning from 'uwsAhiwa' to 'uwsAhiwa_kiyA_jA' by 14anu-ban-09 on (12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pump.clp 	pump_tmp  "  ?id "  " ?id1 " uwsAhiwa_kiyA_jA)" crlf)) 
)


(defrule pump0
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pampa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump0   "  ?id "  pampa )" crlf))
)

;"pump","N","1.pampa/xamakala"
;The mechanic used pump to fill air in  my bicycle.
;
(defrule pump1
(declare (salience 4900))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump1   "  ?id "  Bara )" crlf))
)

;@@@ Added by 14anu-ban-09 on 8-9-14
;Interest rates were pumped up last week. [OALD]
;byAja xare AkirI sapwAha baDa gaI WI. [Own Manual]

(defrule pump2
(declare (salience 5250))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1) 
(kriyA-karma  ?id ?id2)
(id-root ?id2 rate)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pump.clp 	pump2  "  ?id "  " ?id1 " baDA)" crlf))
)


;@@@ Added by 14anu13 on 18-06-14
;When the heart contracts and fresh blood is pumped into the arteries , the pressure is at its peak and is called the systolic pressure .
;जब हृदय संकुचित होता है और ताजा रक्त धमनियों में भेजा जाता है , रक्त दाब अधिकतम होता है और इसे प्रकुंचक या सिस्टोलिक दाब कहते हैं .
(defrule pump3
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-into_saMbanXI  ?id ?id1)(kriyA-karma  ?id ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BejA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump3   "  ?id "  BejA )" crlf))
)

;"pump","V","1.BaranA"
;The doctor immediately pumped oxygen to the patient's heart to bring his beat to normal.
;--"2.pAnI_nikAlanA"
;He used a motor to pump water from the well.
;

;@@@ Added by 14anu-ban-09 on (31-01-2015)
;He used a motor to pump water from the well.		[Hinkhoj]
;उसने कुँए से पानी निकालने के लिए मोटर का प्रयोग किया.		[Manual]
(defrule pump4
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 water|oil)	;added 'oil' by 14anu-ban-09 on (17-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump4   "  ?id "  nikAla )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-04-2015)  ;NOTE-Parser problem. Run on parser no. 6.
;Blood was pumping out of his wound. [oald]
;खून उसके घाव से बाहर निकल रहा था . 	[Manual]
(defrule pump5
(declare (salience 4900))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 blood)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump5   "  ?id "  nikala )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-04-2015)
;He kept pumping my hand up and down.  [oald]
;वह मेरा हाथ ऊपर नीचे उठाता रहा . 	               [Manual]
(defrule pump6
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 hand)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump6   "  ?id "  uTA )" crlf))
)


;@@@ Added by 14anu-ban-09 on (16-04-2015)
;My heart was pumping with excitement.  [oald]
;मेरा हृदय उत्तेजना से धड़क रहा था .  	[Manual]
(defrule pump7
(declare (salience 4900))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 heart)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xadaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump7   "  ?id "  Xadaka )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-04-2015)
;I pumped the handle like crazy. 	[oald]
;मैंने पागलो की तरह हैन्डल दबाया . 	[Manual]
(defrule pump8
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 handle|brake)	;added 'brake' by 14anu-ban-09 on (17-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump8   "  ?id "  xabA )" crlf))
)


;@@@ Added by 14anu-ban-09 on (16-04-2015)
;See if you can pump him for more details.  [oald]
;देखिए यदि आप ससे अधिक विवरण उगलवा सकते हैं . 	                         [Manual]
;देखिए यदि आप उसे अधिक विवरण की जानकारी प्राप्त कर सकते हैं .	[Manual]	;modified translation by 14anu-ban-09 on (17-04-2015)
(defrule pump9
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 detail)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI_prApwa_kara))	;modified by 'ugalavA' to 'jAnakArI_prApwa_kara' by 14anu-ban-09 on (17-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump9   "  ?id " jAnakArI_prApwa_kara )" crlf))	;modified by 'ugalavA' to 'jAnakArI_prApwa_kara' by 14anu-ban-09 on (17-04-2015)
)

;@@@ Added by 14anu-ban-09 on (16-04-2015)
;A runner pumping her arms. 	[cald]
; दौड़ने वाली अपने हाथ हिला रही थी.	[Manual]
(defrule pump10
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-word ?id1 arms)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump10   "  ?id "  hilA )" crlf))
)


;@@@ Added by 14anu-ban-09 on (16-04-2015)
;My legs were pumping as I ran up the stairs.   [oald]
;मेरी टाँगें हिल रहीं थीं जैसे मैं ऊपर की मञ्जिल पर दौडी . 	[Manual]
(defrule pump11
(declare (salience 4900))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 leg)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump11   "  ?id "  hila )" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-04-2015)
;A bicyclist pumping the pedals. 	[freedictionary.com]
;साइकिल चलाने वाले पैडल मार रहे हैं . 		[Manual]
(defrule pump12
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 pedal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump12   "  ?id " mAra )" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-04-2015)
;We have to pump up the tire. [freedictionary.com-modified]
;हमें पहिये में हवा भरनी हैं .		[Manual]
(defrule pump13
(declare (salience 5050))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1) 
(kriyA-object ?id ?id2)
(id-root ?id2 tire)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 havA_Bara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pump.clp 	pump13  "  ?id "  " ?id1 " havA_Bara)" crlf)) 
)

;@@@ Added by 14anu-ban-09 on (17-04-2015)
;He pump a witness for secret information. [freedictionary.com-modified]
;वह गुप्त सूचना के लिए साक्षी जानकारी प्राप्त करता है . 		[Manual]
(defrule pump14
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 witness)
(viSeRya-for_saMbanXI  ?id1 ?id2)
(id-root ?id2 information)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnakArI_prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump14   "  ?id " jAnakArI_prApwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-04-2015)
;Athletes pumping up at the gym. [freedictionary.com]
;व्यायामी व्यायामशाला में कसरत कर रहे हैं .	[Manual] 
(defrule pump15
(declare (salience 5050))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1) 
(kriyA-at_saMbanXI  ?id ?id2)
(id-root ?id2 gym)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kasarawa_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pump.clp 	pump15  "  ?id "  " ?id1 " kasarawa_kara)" crlf)) 
)

;@@@ Added by 14anu-ban-09 on (17-04-2015)
;The company pumped its new product on its website. [freedictionary.com]
;कम्पनी ने उनके वेब-स्थल पर उनके नये उत्पाद का प्रचार किया .	[Manual]
(defrule pump16
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 product)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump16   "  ?id " pracAra_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (17-04-2015)
;A writer who pumped out a new novel every year. [freedictionary.com]
;लेखक जिसने प्रत्येक वर्ष नयी उपन्यास निकाली . 		   [Manual]
(defrule pump17
(declare (salience 5050))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1) 
(kriyA-object ?id ?id2)
(id-root ?id2 novel)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nikAla)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pump.clp 	pump17  "  ?id "  " ?id1 " nikAla)" crlf)) 
)

;@@@ Added by 14anu-ban-09 on (18-04-2015)
;He rapidly pumped a dozen shots into the bull's-eye.  [http://dictionary.reference.com]
;उसने शीघ्रता से - वृषभ की आँख में दर्जन गोलियाँ डाल दी . 	[Manual]
(defrule pump18
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 shot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump18   "  ?id " dAla_xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-04-2015)
;He pumped away at his homework all evening.	[dictionary.reference.com]
;वह पूरी सन्ध्या उसके गृहकार्य पर काम करता था .	[Manual]
(defrule pump19
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
(id-root ?id1 homework)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump19   "  ?id " kAma_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-04-2015)
;The store has decided to pump up its advertising. [freedictionary.com]
;दुकान ने उसका विज्ञापन की प्रणाली को तेज करने का फैसला किया है . [Manual]
(defrule pump20
(declare (salience 5050))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1) 
(kriyA-object ?id ?id2)
(id-root ?id2 advertising)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 weja_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pump.clp 	pump20  "  ?id "  " ?id1 " weja_kara)" crlf)) 
)

;@@@ Added by 14anu-ban-09 on (18-04-2015)
;He pumped my hand warmly. [mw]
;उसने उत्साह से मेरा हाथ दबाया .  [Manual]
(defrule pump21
(declare (salience 5001))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 warmly)
(kriyA-object  ?id ?id2)
(id-root ?id2 hand)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump21   "  ?id " xabA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-04-2015)
;He pumped money into the economy. [mw]
;उसने अर्थ प्रबन्धन में पैसा डाला . 	[Manual]
(defrule pump22
(declare (salience 5000))
(id-root ?id pump)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 money)
(kriyA-into_saMbanXI  ?id ?id2)
(id-root ?id2 economy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pump.clp 	pump22   "  ?id " dAla )" crlf))
)

