;@@@ Added by 14anu-ban-06 (27-03-2015)
;An icy pavement. (cambridge)[parser no- 3]
;बरफ से ढकी हुई पटरी . (manual)
(defrule icy1
(declare (salience 2000))
(id-root ?id icy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 road|pavement)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baraPa_se_DakA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  icy.clp 	icy1   "  ?id "  baraPa_se_DakA_huA )" crlf))
)

;@@@ Added by 14anu-ban-06 (27-03-2015)
;My feet were icy cold.(OALD)[parser no- 8]
;मेरे पाँव बरफ जैसे ठण्डे थे . (manual)
(defrule icy2
(declare (salience 2100))
(id-root ?id icy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 cold)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baraPa_jEsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  icy.clp 	icy2   "  ?id "  baraPa_jEsA )" crlf))
)

;@@@ Added by 14anu-ban-06 (27-03-2015)
;Her skin was icy to the touch.(cambridge)[parser no- 2]
;उसकी त्वचा स्पर्श के लिए बहुत ठण्डी थी . (manual)
(defrule icy3
(declare (salience 2200))
(id-root ?id icy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI ?id ?id1)
(id-root ?id1 touch)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_TaNdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  icy.clp 	icy3   "  ?id "  bahuwa_TaNdA )" crlf))
)

;@@@ Added by 14anu-ban-06 (27-03-2015)
;My eyes met his icy gaze.(OALD) [parser no- 7]
;मेरी आँखें उसकी ठंडी निगाहो से मिलीं .  (manual)
(defrule icy4
(declare (salience 2300))
(id-root ?id icy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 gaze|stare)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TaNdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  icy.clp     icy4   "  ?id "  TaNdA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (27-03-2015)
;The door opened and an icy blast of wind swept through the room. (OALD)[parser no- 2]
;दरवाजा खुला और पवन का बर्फीला झोंका कमरे में से तेजी से फैल गया . (manual)
(defrule icy0
(declare (salience 0))
(id-root ?id icy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id barPIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  icy.clp 	icy0   "  ?id "  barPIlA )" crlf))
)

