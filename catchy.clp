;@@@ Added by 14anu-ban-03 (09-02-2015)
;A catchy title for a movie. [hinkhoj]
;फिल्म के लिए एक आकर्षक नाम . [manual]
(defrule catchy0
(declare (salience 00))
(id-root ?id catchy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catchy.clp 	catchy0   "  ?id "  AkarRaka )" crlf))
)

;NOTE- there is a parser problem in this sentence it is working correct on parser no.-2
;@@@ Added by 14anu-ban-03 (09-02-2015)   
;A catchy question. [hinkhoj]
;एक पेचीदा प्रश्न . [manual]
(defrule catchy1
(declare (salience 500))
(id-root ?id catchy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 question)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pecIxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catchy.clp 	catchy1   "  ?id "  pecIxA )" crlf))
)
