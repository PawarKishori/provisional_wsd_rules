
(defrule vary0
(declare (salience 5000))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id varied )
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwiwa_ho))
;(assert (id-H_vib_mng ?id ed_en)) ;Suggested by Sukhada(20-05-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  vary.clp  	vary0   "  ?id "  parivarwiwa_ho )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  vary.clp       vary0   "  ?id " ed_en )" crlf))
))

(defrule vary1
(declare (salience 4900))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id varied )
(id-cat_coarse ?id verb )
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwiwa_kara))
;(assert (id-H_vib_mng ?id ed_en))
(assert (id-H_vib_mng ?id yA)) ;Suggested by Sukhada(20-05-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  vary.clp  	vary1   "  ?id "  parivarwiwa_kara )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  vary.clp       vary1   "  ?id " ed_en )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  vary.clp       vary1   "  ?id " yA )" crlf))
)

(defrule vary2
(declare (salience 4800))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id varied )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Binna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  vary.clp  	vary2   "  ?id "  Binna )" crlf))
)

(defrule vary3
(declare (salience 4700))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id varied )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id nAnArUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  vary.clp  	vary3   "  ?id "  nAnArUpa )" crlf))
)

;"varied","Adj","1.nAnArUpa"
;Courses for the students are many && varied
;
;
;$$$ Modified by 14anu-ban-07 (11-12-2014)
;But voltages and currents that vary with time are very common.(ncert)
;तथापि, समय के साथ परिवर्तित होने वाली धाराओं और वोल्टताओं का मिलना एक आम बात है.(ncert)
(defrule vary4
(declare (salience 4600))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwiwa_ho)) ;meaning changed from parivarwana_ho to parivarwiwa_ho by 14anu-ban-07 (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vary.clp 	vary4   "  ?id "  parivarwiwa_ho )" crlf))
)

(defrule vary5
(declare (salience 4500))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-object ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vary.clp 	vary5   "  ?id "  parivarwana_kara )" crlf))
)

;default_sense && category=verb	waraha_waraha_kA_ho	0
;"vary","VT","1.waraha_waraha_kA_honA"
;Fish vary in sizes && shapes.
;--"Binna_honA"
;Prices of fruits vary from place to place
;--"baxala_xenA"
;The artist wanted to vary the show
;
;

;@@@ Added by 14anu17
;That can be misused with varying effects .
;यही कारण है कि अलग प्रभाव के साथ दुरुपयोग किया जा सकता है.
(defrule vary6
(declare (salience 4600))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(id-root =(+ ?id 1)  effect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vary.clp 	vary6   "  ?id "  alaga )" crlf))
)

;@@@ Added by 14anu17 
;Varying temperature is responsible for poor health.
;घटता-बढता तापमान खराब स्वास्थ्य का कारण है .
(defrule vary7
(declare (salience 4600))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1)  temperature|pressure)
(id-cat_coarse ?id verb )

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatawA-baDZawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vary.clp     vary7   "  ?id "  GatawA-baDZawA )" crlf))
)

;@@@ Added by 14anu-ban-07,(10-10-2014) 
;When the orbit of a satellite becomes elliptic, both the KE and PE vary from point to point.(ncert corpus)
;जब किसी उपग्रह की कक्षा दीर्घवृत्तीय होती है तो उसकी K.E तथा P.E दोनों ही पथ के हर बिंदु पर भिन्न होती हैं.(ncert corpus)
(defrule vary8
(declare (salience 4600))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-from_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Binna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vary.clp     vary8   "  ?id "  Binna_ho )" crlf))
)

;@@@ Added by 14anu-ban-07,(05-03-2015)
;Her novels vary in length. (oald)
;उसकी उपन्यास कि लम्बाईयाँ   बदलती रहती हैं .(manual) 
(defrule vary9
(declare (salience 4700))
(id-root ?id vary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb )
(kriyA-in_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxalawI_raha)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vary.clp 	vary9   "  ?id "  baxalawI_raha )" crlf))
)


