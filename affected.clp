;@@@Added by 14anu-ban-02(06-02-2015)
;Sentence: I found her very affected.[cambridge]
;Translation: वह मुझे काफी बनावटी लगी.[self]
(defrule affected0 
(declare (salience 0)) 
(id-root ?id affected) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id banAvatI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  affected.clp  affected0  "  ?id "  banAvatI )" crlf)) 
) 

;$$$ Modified by Bhagyashri Kulkarni (29-10-2016)
;Here the H.I.V. infected persons are taken full care of and assisted, so that the member of the affected family can live a dignified and complete life. (health)
;यहाँ एच्.आई.वी. सङ्क्रमित व्यक्ति का पूरा ख्याल रखा जाता है और सहायता की जाती है, जिससे कि प्रभावित परिवार के सदस्य एक सम्मानित व सम्पूर्ण जीवन जी सकें . 
;@@@Added by 14anu-ban-02(06-02-2015)
;Their iron-tipped rods were similarly affected.[ncert 12_04]
;लोहे की टोपी चढी उनकी लाठी भी इसी प्रकार प्रभावित होती थी.[ncert]
(defrule affected1 
(declare (salience 100)) 
(id-root ?id affected) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) ;added by Bhagyashri
(or(subject-subject_samAnAXikaraNa  ?id1 ?id)(viSeRya-viSeRaNa ?id1 ?id)) ;added 'viSeRya-viSeRaNa' by Bhagyashri
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id praBAviwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  affected.clp  affected1  "  ?id "  praBAviwa )" crlf)) 
) 
