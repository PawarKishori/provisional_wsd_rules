;@@@ Added by 14anu-ban-04 (23-02-2015)
;Flames devoured the house.                  [oald]
;लपटों ने घर को नष्ट कर दिया .                       [self]    
(defrule devour1
(declare (salience 20))
(id-root ?id devour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 house|building)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id naRta_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  devour.clp     devour1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devour.clp 	devour1  "  ?id "  naRta_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-04 (23-02-2015)
;His dark eyes devoured her beauty.             [oald]
;उसकी काली आँखों ने उसकी सुन्दरता को चाव से देखा .           [self]
(defrule devour2
(declare (salience 40))
(id-root ?id devour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1 )
(id-root ?id1 beauty)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id cAva_se_xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  devour.clp     devour2   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devour.clp 	devour2 "  ?id "  cAva_se_xeKa )" crlf))
)

;@@@ Added by 14anu-ban-04 (23-02-2015)
;She loves to read - she devours one book after another.           [cald]
;वह पढ़ना पसन्द करती है- वह एक के बाद एक पुस्तक को चाव से पढ़ती है .                  [self]
(defrule devour3
(declare (salience 40))
(id-root ?id devour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1 )
(id-root ?id1 book|magazine|newspaper|article|textbook)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id cAva_se_paDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  devour.clp     devour3   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devour.clp 	devour3 "  ?id "  cAva_se_paDa )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (23-02-2015)
;He devoured half of his burger in one bite.           [olad]
;वह एक ही निवाले में अपने बर्गर  को आधा खा गया .             [self]
(defrule devour0
(declare (salience 10))
(id-root ?id devour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KA_jA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  devour.clp     devour0   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  devour.clp 	devour0  "  ?id "   KA_jA )" crlf))
)
