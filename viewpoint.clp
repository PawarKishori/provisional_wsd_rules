;##############################################################################
;#  Copyright (C) 2014-2015 Vivek (vivek17.agarwal@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by 14anu06(Vivek Agarwal) 
(defrule viewpoint0			;Default Rule(added after the original clp file was commited)
(declare (salience 2000))
(id-root ?id viewpoint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id najZariyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " viewpoint.clp viewpoint0 "  ?id " najZariyA  )" crlf))
)

;@@@ Added by 14anu06(Vivek Agarwal) on 14/6/2014************
;His viewpoint was questionable.
;उसका  नज़रिया सन्देहयुक्त था.
(defrule viewpoint1
(declare (salience 5000))
(id-root ?id viewpoint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-RaRTI_viSeRaNa  ?id ?id1)(viSeRya-of_saMbanXI  ?id ?id2))
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
;(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id najZariyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  viewpoint.clp   viewpoint1 "  ?id " najZariyA  )" crlf))
)
