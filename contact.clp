;@@@ Added by 14anu-ban-03 (18-04-2015)
;She blushed at the sudden contact of his hand against her arm. [oald]
;वह अपनी बांह पर उसके हाथ के अचानक स्पर्श से शरमाई . [manual]
(defrule contact2
(declare (salience 10))  
(id-root ?id contact)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sparSa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contact.clp 	contact2   "  ?id "  sparSa )" crlf))
)


;@@@ Added by 14anu-ban-03 (18-04-2015)
;We have good contacts with the local community. [oald]
;हमारे स्थानीय समाज के साथ अच्छे सम्बन्ध हैं . [manual]
(defrule contact3
(declare (salience 10))  
(id-root ?id contact)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 good)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMbaMXa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contact.clp 	contact3   "  ?id "  saMbaMXa )" crlf))
)


;@@@ Added by 14anu-ban-03 (18-04-2015)
;I usually wear contact lenses, but I sometimes wear glasses when my eyes are tired. [cald]
;मैं आम तौर पर कान्टैक्ट/संस्पर्श लेन्स पहनता हूँ, परन्तु मैं कभी कभी चश्मे पहन लेता हूँ जब मेरी आँखें थक जाती हैं . [manual]
(defrule contact4
(declare (salience 10))  
(id-root ?id contact)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 lens)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAntEkta/saMsparSa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contact.clp 	contact4   "  ?id "  kAntEkta/saMsparSa )" crlf))
)

;@@@ Added by 14anu-ban-03 (18-04-2015)
;The company was formed by contacting three smaller firms. [oald]
;कम्पनी तीन छोटी व्यापारिक कम्पनियों को मिला कर बनाई गयी थी . [manual]
(defrule contact5
(declare (salience 10))  
(id-root ?id contact)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id milA_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  contact.clp 	contact5  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contact.clp 	contact5   "  ?id "  milA_kara )" crlf))
)

;-----------------------------------------------------Default rules---------------------------------------------

;@@@ Added by 14anu-ban-03 (18-04-2015)
;I don't have much contact with my uncle. [oald]
;मेरे चाचा के साथ मेरा ज़्यादा सम्पर्क नहीं है . [manual]
(defrule contact0
(declare (salience 00))
(id-root ?id contact)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samparka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contact.clp  contact0   "  ?id "  samparka )" crlf))
)

;@@@ Added by 14anu-ban-03 (18-04-2015)
;I've been trying to contact you all day. [oald]
;मैं पूरे दिन आपको सम्पर्क करने का प्रयास करता रहा . [manual]
(defrule contact1
(declare (salience 00))
(id-root ?id contact)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samparka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contact.clp  contact1   "  ?id "  samparka_kara )" crlf))
)

;----------------------------------------------------------------------------------------------------------------------------


