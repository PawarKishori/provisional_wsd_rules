;$$$Modified by 14anu-ban-08 (09-01-2015)         ;add the constraint, comment the constraint
;@@@ Added by 14anu06(Vivek Agarwal) on 21/6/2014*********
;A leader in the mold of her predecessors. 
;उसके पूर्ववर्तियों के शैली में एक नेता.
(defrule mold0
(declare (salience 5000))
(id-root ?id mold)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id1 ?id)
(id-root ?id1 leader)                    ;added by 14anu-ban-08 (09-01-2015)
;(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))    ;commented this constraint by 14anu-ban-08 (09-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SElI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mold.clp 	mold0   "  ?id "  SElI )" crlf))
)
