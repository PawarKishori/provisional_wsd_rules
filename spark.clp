
;@@@ Added by 14anu-ban-11 on (04-03-2015)
;A spark from the machinery had set fire to some material.(oald)
;यण्ट्र-उप्करण की चिंगारी ने कुछ द्रव्यो मे आग लगा दी थी . (self)
(defrule spark0
(declare (salience 00))
(id-root ?id spark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciMgArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spark.clp 	spark0   "  ?id "  ciMgArI)" crlf))
)

;@@@ Added by 14anu-ban-11 on (04-03-2015)
;A spark of hope.(oald)
;आशा की किरण . (self)
(defrule spark1
(declare (salience 10))
(id-root ?id spark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 hope)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spark.clp 	spark1   "  ?id "  kiraNa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (04-03-2015)
;She had a certain spark—that something extra.(oald)
;उसमे विश्वसनीय उत्साह था-जो कुछ  अलग-सा था . (self)
(defrule spark2
(declare (salience 20))
(id-root ?id spark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 certain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spark.clp 	spark2   "  ?id "  uwsAha)" crlf))
)

;@@@ Added by 14anu-ban-11 on (04-03-2015)
;The organizers are hoping to spark some interest in young people.(oald)
;आयोजक युवा लोगों में कुछ रूचिओ को उत्साहित करने की उम्मीद कर रहे हैं . (self)
(defrule spark3
(declare (salience 30))
(id-root ?id spark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 organizer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsAhiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spark.clp 	spark3   "  ?id "  uwsAhiwa_kara)" crlf))
)


;@@@ Added by 14anu-ban-11 on (04-03-2015)
;When they get together in a meeting the sparks really fly.(cald)
;जब वे बैठक में एक साथ होते हैं तो विवाद वास्तव में उठता हैं . (self)
(defrule spark4
(declare (salience 40))
(id-root ?id spark)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 fly)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spark.clp 	spark4   "  ?id "  vivAxa)" crlf))
)

