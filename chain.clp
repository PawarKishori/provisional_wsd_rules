;$$$ Modified by 14anu-ban-03 (10-12-2014)
;$$$ Modified by 14anu24
;If the shop is part of a chain , write to the head office .
;अगर दुकान किसी श्रृंखला यानि चेन का हिस्सा है , तो हेड आफिस यानि प्रमुख कार्यालय को लिखिए .
(defrule chain01
(declare (salience 2000));salience changed
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-of_saMbanXI  ?id ?id1);condition added
(viSeRya-of_saMbanXI  ?id1 ?id)  ;added by 14anu-ban-03 (10-12-2014)
(id-root ?id1 part)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SrqMKalA));seplling corrected
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain01   "  ?id "  SrqMKalA )" crlf))
)


;$$$ Modified by 14anu-ban-03(16-08-2014)
;To stay world ' s largest hotel chain Best Western ' s resort is also here .  [tourism corpus]
;यहाँ  ठहरने  के  लिए  विश्व  की  सबसे  बड़ी  होटल  श्रृंखला  बेस्ट  वैस्टर्न  का  रिजॉर्ट  भी  है  ।
;$$$  Modified by Preeti(22-4-14) ; added 'viSeRya-of_saMbanXI'
;His arrival set off a surprising chain of events. [Cambridge Learner’s Dictionary]
;usake Agamana ne GatanAoM kI eka AScaryajanaka SrqMKalA AraMBa kI.
(defrule chain0
(declare (salience 1000));salience changed
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-of_saMbanXI  ?id ?);condition added
(or(samAsa_viSeRya-samAsa_viSeRaNa ?id ?) (viSeRya-of_saMbanXI  ?id ?)) ;added 'samAsa_viSeRya-samAsa_viSeRaNa' relation by 14anu-ban-03(16-8-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SrqMKalA));seplling corrected
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain0   "  ?id "  SrqMKalA )" crlf))
)

(defrule chain1
;(declare (salience 4900));salience changed
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sikadZI_bAzXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain1   "  ?id "  sikadZI_bAzXa )" crlf))
)

;@@@ Added by Preeti(22-4-14)
;The prisoners were chained together. [merriam-webster.com]
;banxiyoM ko eka sAWa jaMjIra_se bAzXA gayA WA.
(defrule chain2
(declare (salience 4900))
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id chained)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaMjZIra_se_bAzXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain2   "  ?id "  jaMjZIra_se_bAzXa )" crlf))
)

;@@@ Added by Preeti(22-4-14)
;I have been chained to my desk all week. [Oxford Advanced Learner's Dictionary]
;mEM pUre sapwAha apanI deska se bazXA rahA.
(defrule chain3
(declare (salience 4950))
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id chained)
(kriyA-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_bazXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain3   "  ?id "  se_bazXa )" crlf))
)
;$$$ Modified by 14anu-ban-03(20-08-2014)
;The scene of floating chain of diyas looks spectacular .[tourism corpus]
;गंगा  नदी  पर  तैरती  दीप माला  का  दृश्य  देखते  ही  बनता  है   ।
;@@@ Added by Preeti(22-4-14)
;She wore a heavy gold chain around her neck. [Oxford Advanced Learner's Dictionary]
;usane apane gale meM eka BArI sone_kI mAlA pahanI WI.
(defrule chain4
(declare (salience 1000))
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-object  ?id1 ?id) (viSeRya-viSeRaNa ?id ?id1)) ;added 'viSeRya-viSeRaNa' relation by 14anu-ban-03
(id-root ?id1 wear|float) ;added 'float' by 14anu-ban-03
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain4   "  ?id "  mAlA )" crlf))
)


;@@@ Added by Preeti(22-4-14)
;The prisoners were kept in chains. [merriam-webster.com]
;banxiyoM ko jaMjIroM meM raKA gayA WA.
(defrule chain5
;(declare (salience 1000))
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaMjIra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain5   "  ?id "  jaMjIra )" crlf))
)

;"chain","V","1.sikadZI_bAzXanA"
;The dog was chained to the post.
;
;@@@ Added by 14anu-ban-03(20-08-2014)
;The chain of development of tea farming in Munnar , ancient instruments which were used for transforming the dense forests into tea garden.[tourism corpus]
; मुन्नार  में  चाय  की  खेती  के  विकास  की  कड़ी,  आदिकालीन  यंत्र  जो  घने  जंगलों  को  चाय  के  बागान  में  बदलने  के  लिए  प्रयुक्त  किए  जाते  थे.
(defrule chain6
(declare (salience 1000))
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-word ?id1 development)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadI))  ;spelling corrected from 'kadi' to 'kadI' by 14anu-ban-03 (13-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain6   "  ?id "  kadI )" crlf))
)

;@@@ Added by 14anu-ban-03 (13-03-2015)
;As part of the centenary celebrations a chain of beacons was lit across the region.[cald]
;शतवर्षीय उत्सव के अन्तरगत आकाशदीप की लड़ी  क्षत्र में सर्वत्र प्रकाशित की गयी थी . [self]
(defrule chain7
(declare (salience 1001))
(id-root ?id chain)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 beacon)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ladI))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chain.clp 	chain7  "  ?id "  ladI )" crlf))
)



