;@@@ Added by 14anu-ban-06 (05-03-2015)
;Driving on icy roads can be pretty hairy. (OALD)
;बर्फीली सडकों पर ड्राइविंग काफी कठिन हो सकती है . (manual)
(defrule hairy1
(declare (salience 2000))
(id-root ?id hairy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 driving)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hairy.clp 	hairy1   "  ?id "  kaTina )" crlf))
)

;@@@ Added by 14anu-ban-06 (05-03-2015)
;You avoid several stretches of I-70 that can be hairy in stormy weather.(COCA)[parser no. 10]
;आप -इ70 के कई खिंचाव को टालते हैं जो तूफानी मौसम में मुश्किल हो सकता है . (manual)
(defrule hairy2
(declare (salience 2100))
(id-root ?id hairy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-in_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muSkila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hairy.clp 	hairy2   "  ?id "  muSkila )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (05-03-2015)
;The backs of his hands were very hairy. (COCA)
;उसके हाथों का पिछला भाग अत्यन्त बाल(manual) (manual)
(defrule hairy0
(declare (salience 0))
(id-root ?id hairy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAlaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hairy.clp    hairy0   "  ?id "  bAlaxAra )" crlf))
)

