;##############################################################################
;#  Copyright (C) 2013-2014 Prachi Rathore (prachirathore02 at gmail dot com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################




;$$$ Modified by 14anu-ban-06 (21-01-2015)
;The inflation of a balloon.
;गुब्बारे का फुलाव . (manual) ;added by 14anu-ban-06 (21-01-2015)
(defrule inflation2
(declare (salience 5000))
(id-root ?id inflation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1);added by 14anu-ban-06 (21-01-2015)
(id-root ?id1 balloon|tire)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PulAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflation.clp 	inflation2   "  ?id "  PulAva )" crlf))
)

;To control inflation.
(defrule inflation0
(declare (salience 4000))
(id-root ?id inflation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muxrA_sPIwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflation.clp 	inflation0   "  ?id "  muxrA_sPIwi )" crlf))
)
