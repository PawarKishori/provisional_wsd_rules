;########################################################################
;#  Copyright (C) 2014-2015 Gurleen Singh Bhakna (gurleensingh79@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################`

;@@@ Added by 14anu05 GURLEEN BHAKNA on 30.06.14
;He scored his first goal of the season on Saturday.
;उसने शनिवार को सीज़न का उसका प्रथम गोल किया . 
(defrule goal1
(declare (salience 4700))
(id-root ?id goal)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 score)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  goal.clp     goal1   "  ?id "  gola )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-04-2015)
;The England team had two goals disallowed.    [cald]
;इंग्लैंड दल के दो गोल रद्द कर दिए   थे .                 [self]
(defrule goal2
(declare (salience 2701))
(id-root ?id goal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  goal.clp     goal2   "  ?id "  gola )" crlf))
)


;-----default rule-----
;@@@ Added by 14anu05 GURLEEN BHAKNA on 30.06.14
;Their goal was to eradicate malaria.
;उनका लक्ष्य मलेरिया का उन्मूलन करना था .
(defrule goal0
(declare (salience 2700))
(id-root ?id goal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lakRya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  goal.clp     goal0   "  ?id "  lakRya )" crlf))
)
