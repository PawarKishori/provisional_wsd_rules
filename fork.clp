
;"forked","Adj","1.kAzte_jEsA"
;Snake has a forked tongue.
(defrule fork0
(declare (salience 5000))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id forked )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kAzte_jEsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fork.clp  	fork0   "  ?id "  kAzte_jEsA )" crlf))
)

;She forked out five thousand rupees to her landlord.
;usane makAna mAlika ko pAzca hajZAra rupaE axA kara xie
(defrule fork1
(declare (salience 4900))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 axA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fork.clp	fork1  "  ?id "  " ?id1 "  axA_kara  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (27-03-2015)
;Shortly before dusk they reached a fork and took the left-hand track. [OALD]
;सन्ध्याकाल से ठीक पहले वे एक दोराहे पर पहुँचे और बांई ओर का ट्रैक लिया . 	[manual]
(defrule fork4
(declare (salience 4501))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 reach)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xorAhA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fork.clp 	fork4   "  ?id "  xorAhA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (27-03-2015)
;A monkey sitting in the fork of the tree.[OALD]
;पेड के  द्विशाखित भाग में बैठा हुआ बन्दर . [MANUAL]
(defrule fork5
(declare (salience 4501))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 tree)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xviSAKiwa_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fork.clp 	fork5   "  ?id "  xviSAKiwa_BAga )" crlf))
)



;@@@ Added by 14anu-ban-05 on (27-03-2015)
;Bear left at the fork in the road. [OALD]
;भालू  ने सड़क को दोराहे पर छोड़ दिया है.	[MANUAL]
(defrule fork6
(declare (salience 4501))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-at_saMbanXI  ? ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xorAhA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fork.clp 	fork6   "  ?id "  xorAhA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (27-03-2015)
;The farmer forked the manure into the soil.[from fork.clp]
;किसान ने मिट्टी में खाद काँटे से निकाला	[manual]
(defrule fork7
(declare (salience 4901))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 manure|piece)			;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAzte_se_nikAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fork.clp 	fork7   "  ?id "  kAzte_se_nikAlA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (27-03-2015)
;This road forks just beyond the bridge.[from fork.clp]
;यह सडक पुल के ठीक बाद  विभाजित होती है . 	[manual]
(defrule fork8
(declare (salience 4902))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 road|path)			;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBAjiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fork.clp 	fork8   "  ?id "  viBAjiwa_ho )" crlf))
)


;@@@ Added by 14anu-ban-05 on (27-03-2015)
;Fork right after the bridge. [OALD]
;पुल के बाद दाईं ओर मुडे.    [manual]
(defrule fork9
(declare (salience 4902))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(id-root ?id1 right|left)			
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fork.clp 	fork9   "  ?id "  muda )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-05 on (27-03-2015)
;He put the knives and forks on the table.[OALD]
;उसने मेज पर चाकू और काँटे डाल दिया.		[MANUAL]
(defrule fork3
(declare (salience 4500))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAztA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fork.clp 	fork3   "  ?id "  kAztA )" crlf))
)


(defrule fork2
(declare (salience 4800))
(id-root ?id fork)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fork.clp 	fork2   "  ?id "  nikAla_xe )" crlf))
)

;default_sense && category=verb	viBAjiwa_ho	0
;"fork","VI","1.viBAjiwa_honA"
;This road forks just beyond the bridge.
;--"2.kAzte_se_nikAlanA/KoxanA_Axi"
;The farmer forked the manure into the soil.
;
;
