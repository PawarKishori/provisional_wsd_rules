;@@@ Added by 14anu-ban-11 on (20-01-2015)
;The rising wail of police sirens.(oald)
;पुलीस भोंपू का जोर से बजना .(manual)
(defrule wail2
(declare (salience 5100))
(id-root ?id wail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 siren)
=>
(retract ?mng)
(assert (make_verbal_noun ?id))
(assert (id-wsd_root_mng ?id baja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  wail.clp         wail2   "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wail.clp 	wail2   "  ?id "  baja )" crlf))
)

;----------------------- Default Rules --------------------------

(defrule wail0
(declare (salience 5000))
(id-root ?id wail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ronA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wail.clp 	wail0   "  ?id "  ronA )" crlf))
)

;"wail","N","1.ronA"
;The cry of the lady became feeble
;
(defrule wail1
(declare (salience 4900))
(id-root ?id wail)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vilApa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wail.clp 	wail1   "  ?id "  vilApa_kara )" crlf))
)

;"wail","VTI","1.vilApa_karanA"
;The child started wailing when his mother left him
;
