(defrule tender0
(declare (salience 5000))
(id-root ?id tender)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id komala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tender.clp 	tender0   "  ?id "  komala )" crlf))
)

;"tender","Adj","1.komala"
(defrule tender1
(declare (salience 4900))
(id-root ?id tender)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nivixA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tender.clp 	tender1   "  ?id "  nivixA )" crlf))
)

;"tender","N","1.nivixA"
;Quote the lowest tender.
;--"2.IMXana_gAdI"
;
(defrule tender2
(declare (salience 4800))
(id-root ?id tender)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tender.clp 	tender2   "  ?id "  peSa_kara )" crlf))
)

;"tender","VT","1.peSa_karanA{nivixA}"
;The Company tenderered its application for the construction of the school building.

;@@@ Added by 14anu05 GURLEEN BHAKNA on 03.07.14
;This meat is extremely tender.
;यह माँस अत्यधिक नरम है .
(defrule tender3
(declare (salience 5100))
(id-root ?id tender)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id narama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tender.clp 	tender3   "  ?id "  narama )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 03.07.14
;Local firms were invited to tender for the building contract.
;स्थानीय व्यापारिक कम्पनियाँ इमारत अनुबन्ध के लिए प्रस्ताव रखने के लिए आमन्त्रित की गयीं थीं .
(defrule tender4
(declare (salience 5100))
(id-root ?id tender)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAva_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tender.clp 	tender4   "  ?id "  praswAva_raKa )" crlf))
)

