;@@@ Added by 14anu-ban-03 (06-02-2015)
;I have kept all my jewelleries in the casket. [hinkhoj]
;मैंने शृङ्गारदान में अपने सभी गहने रख दिए है. [manual] 
(defrule casket0
(declare (salience 00))
(id-root ?id casket)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SqMgAraxAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  casket.clp 	casket0   "  ?id "  SqMgAraxAna)" crlf))
)


;@@@ Added by 14anu-ban-03 (06-02-2015)
;A procession of mourners slowly followed the casket. [oald]
;शोक मनाने वालों के जलूस ने धीरे से शवपेटिका का अनुसरण किया . [manual]
(defrule casket1
(declare (salience 500))
(id-root ?id casket)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 follow)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SavapetikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  casket.clp 	casket1   "  ?id "  SavapetikA)" crlf))
)



