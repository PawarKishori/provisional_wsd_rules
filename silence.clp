;$$$Modified by 14anu-ban-01 on (02-01-2015)
;@@@ Added by 14anu20 MNNIt alld.
;He silences him.
;vaha usko cupa karAwa hE.
(defrule silence0
(declare (salience 5000))
(id-root ?id silence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)));Added by 14anu-ban-01 on (02-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cupa_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silence.clp 	silence0   "  ?id "  cupa_kAra )" crlf))
)


;@@@ Added by 14anu20 MNNIT alld.
;He is in silence.
;vaha cupa cApa hE.

(defrule silence1
(declare (salience 2400))
(id-root ?id silence)
(id-word =(- ?id 1) in)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) cupa_cApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " silence.clp	silence1  "  ?id "  " (- ?id 1) "  cupa_cApa  )" crlf))
)

;$$$Modified by 14anu-ban-01 on (02-01-2015)
;@@@ Added by 14anu20 on 23/06/2014.
;A silence followed.
;खामोसी  छाया . 
;खामोशी छा गई.[Transltion improved by 14anu-ban-01 on (02-01-2015)]
(defrule silence2
(declare (salience 500))
(id-root ?id silence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 follow|provoke|spread|embark)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KAmoSI));corrected "KAmosI" to "KAmoSI"  by 14anu-ban-01 on (02-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silence.clp 	silence2   "  ?id "  KAmoSI )" crlf));corrected "KAmosI" to "KAmoSI"  by 14anu-ban-01 on (02-01-2015)
)

;@@@ Added by 14anu-ban-01 on (06-02-2015)
;I need absolute silence when I'm working.[oald]
;जब मैं काम कर रहा हूँ तब मुझे पूर्णतया शांति चाहिए.[self]
(defrule silence4
(declare (salience 100))
(id-root ?id silence)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 need|require|maintain|be|have)
(kriyA-object  ?id1 ?id)
(id-root ?id2 absolute|complete)
(viSeRya-viSeRaNa  ?id ?id2)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silence.clp 	silence4   "  ?id "  SAnwi )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-02-2015)
;They finished their meal in total silence.[oald]
;उन्होंने अपना भोजन चुपचाप समाप्त किया[self]
(defrule silence5
(declare (salience 100))
(id-root ?id silence)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 total|absolute|complete)
(viSeRya-viSeRaNa  ?id ?id1)
(kriyA-in_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id1 1) ?id1 cupacApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " silence.clp 	silence5  "  ?id "  "(- ?id1 1)"    " ?id1 "  cupacApa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (06-02-2015)
;A scream broke the silence of the night.[oald]
;एक चीख़ ने रात के सन्नाटे को भंग कर दिया.[self]
(defrule silence6
(declare (salience 100))
(id-root ?id silence)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 night)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sannAtA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silence.clp 	silence6   "  ?id "  sannAtA )" crlf))
)

;------------------------ Default Rules ----------------

;@@@ Added by 14anu-ban-01 on (06-02-2015)
;I am much exercised by her silence.[exercise.clp]
;मैं उसकी चुप्पी से चिंता में पड़ जाता हूँ[self]
(defrule silence3
(declare (salience 0))
(id-root ?id silence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cuppI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silence.clp 	silence3   "  ?id "  cuppI )" crlf))
)

