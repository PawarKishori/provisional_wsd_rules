
;@@@ Added by 14anu-ban-11 0n (4-11-2014)
;Such units obtained for the derived quantities are called derived units.(NCERT)
;इस प्रकार प्राप्त किए गए व्युत्पन्न राशियों के मात्रकों को व्युत्पन्न मात्रक कहते हैं.(NCERT)
(defrule quantity0
(declare (salience 100))
(id-root ?id quantity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rASi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quantity.clp  quantity0   "  ?id "  rASi)" crlf))
)


;@@@ Added by 14anu-ban-11 0n (4-11-2014)
;A plot of temperature versus heat energy for a quantity of water is shown in Fig. 11.12.(NCERT)
;चित्र 11.12 में जल के किसी परिमाण अधिक गम्भीर जलन देती है ; भौतिकी के लिए ताप तथा ऊष्मा ऊर्जा के बीच ग्राफ का आलेख दर्शाया गया है.(NCERT)
;चित्र 11.12 में  जल की किसी मात्रा के लिये ताप के विपरीत ऊष्मा ऊर्जा का ग्राफ दर्शाया गया है.(Self)
;What are the factors on which the quantity of heat required to raise the temperature of a substance depend?(NCERT)
;वे कौन से कारक हैं जिन पर किसी पदार्थ का ताप बढाने के लिए आवश्यक ऊष्मा की मात्रा निर्भर करती है ?(NCERT)
(defrule quantity1
(declare (salience 200))
(id-root ?id quantity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAwrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quantity.clp  quantity1   "  ?id "  mAwrA )" crlf))
)


