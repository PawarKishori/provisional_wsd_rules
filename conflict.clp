;$$$ Modified by 14anu-ban-03 (13-12-2014)
;@@@ Added by 14anu11
;हिरोशिमा इस संघर्ष का प्रतीक बन गया . . . .
;Hiroshima became a symbol of this conflict . . . .
(defrule conflict2
(declare (salience 6000))
(id-root ?id conflict)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-det_viSeRaNa  ?id ?id1)  ;commented by 14anu-ban-03 (13-12-2014)
;(viSeRya-viSeRaNa  ?id2 ?id)  ;commented by 14anu-ban-03 (13-12-2014)
(viSeRya-of_saMbanXI  ?id1 ?id) ;added by 14anu-ban-03 (13-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMGarRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conflict.clp 	conflict2   "  ?id "  saMGarRa )" crlf))
)


;@@@ Added by 14anu-ban-03 (07-04-2015)
;The results of the new research would seem to conflict with existing theories. [cald]   ;working on parser no.-5
;नये शोध के परिणाम वर्तमान सिद्धान्तों से मेल ना खाते हुए प्रतीत होंगे .  [manaul]
(defrule conflict3
(declare (salience 5000))
(id-root ?id conflict)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mela_nA_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conflict.clp 	conflict3   "  ?id "  mela_nA_KA )" crlf))
)


;@@@ Added by 14anu-ban-03 (07-04-2015)
;The conflict of Waterloo. [oald]
;वाटरलू की लडाई .  [manual]
(defrule conflict4
(declare (salience 5000))
(id-root ?id conflict)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ladAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conflict.clp 	conflict4   "  ?id "  ladAI )" crlf))
)


;@@@ Added by 14anu-ban-03 (07-04-2015)
;A country conflicting for independence. [oald]
;देश स्वाधीनता के लिए सङ्घर्ष कर रहा हैं. [manual]
(defrule conflict5
(declare (salience 5000))   
(id-root ?id conflict)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMGarRa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conflict.clp 	conflict5   "  ?id "  saMGarRa_kara )" crlf))
)


;---------------------- Default rules -----------------------
;"conflict","N","1.viroXa/saMGarRa"
;His conflict of interest made him ineligible for the post.
(defrule conflict0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (07-04-2015)
(id-root ?id conflict)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conflict.clp 	conflict0   "  ?id "  viroXa )" crlf))
)

;"conflict","V","1.viroXa_karanA"
;The two proposals conflict!
(defrule conflict1
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (07-04-2015)
(id-root ?id conflict)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conflict.clp 	conflict1   "  ?id "  viroXa_kara )" crlf))
)


