;@@@ Added by 14anu-ban-06 (08-09-2014)
;Only the Digboi refinery with a capacity of 425 , 000 tons was based on indigenous crude .(parallel corpus)
;4 , 25 , 000 टन की क्षमता की केवल डिगबोई तेल शोधक इकाई ही स़्वदेशी कच़्चे तेल पर आधारित थी .
(defrule indigenous0
(declare (salience 0))
(id-root ?id indigenous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svaxeSI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indigenous.clp 	indigenous0   "  ?id "  svaxeSI )" crlf))
)

;@@@ Added by 14anu-ban-06 (08-09-2014)
;It's indigenous to China and the Far East .(parallel corpus)
;ये चीन और पूर्वी विश्व का मूल  है ,
(defrule indigenous1
(declare (salience 2000))
(id-root ?id indigenous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indigenous.clp 	indigenous1  "  ?id "  mUla )" crlf))
)
