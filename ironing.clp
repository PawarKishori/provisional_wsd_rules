;@@@ Added by 14anu-ban-06 (17-02-2015)
;I must do some ironing tonight.(cambridge)[parser no.- 3]
;मुझे आज रात कुछ इस्त्री करनी चाहिए . (manual)
;I must do the ironing tonight. (cambrdige)[parser no.- 3]
;मुझे आज रात इस्त्री करनी चाहिए .(manual)
(defrule ironing0
(declare (salience 0))
(id-root ?id ironing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iswrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ironing.clp 	ironing0   "  ?id "  iswrI )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-02-2015)
;A basket full of ironing.(cambridge) [parser no.- 2]
;इस्त्री किये हुए कपडो से भरी एक टोकरी . (manual)
;A pile of ironing. (OALD) [parser no.- 2]
;इस्त्री किये हुए कपडो का ढेर . (manual)
(defrule ironing1
(declare (salience 2000))
(id-root ?id ironing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 basket|pile)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iswrI_kiye_hue_kapade))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ironing.clp 	ironing1   "  ?id "  iswrI_kiye_hue_kapade )" crlf))
)

