;@@@ Added by 14anu-ban-11 on (03-02-2015)
;I added zest in the cake.(hinkhoj)
;मैंने केक में सन्तरे का छिलका डाला .(self) 
(defrule zest1
(declare (salience 20))
(id-root ?id zest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-in_saMbanXI  ?id1 ?id2)
(id-root ?id2 cake)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMware_kA_CilakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  zest.clp  zest1   "  ?id "  saMware_kA_CilakA )" crlf))
)


;@@@ Added by 14anu-ban-11 on (03-02-2015)
;So we're going to add a little lemon zest.(coca) 
;इसलिए हम थोडे नींबू के छिलके डालने जा रहे हैं .(self) 
(defrule zest2
(declare (salience 30))
(id-root ?id zest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(viSeRya-viSeRaNa  ?id ?id2)
(id-root ?id1 lemon)
(id-root ?id2 little)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 Wode_nIMbU_ke_Cilake))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " zest.clp zest2  "  ?id "  " ?id1 " " ?id2 "  Wode_nIMbU_ke_Cilake )" crlf))
)

;--------------------------- Default Rules -----------------

;@@@ Added by 14anu-ban-11 on (03-02-2015)
;He joined the new school with zest.(hinkhoj)
;वह उत्साह के साथ नये विद्यालय से जुडा .(self) 
(defrule zest0
(declare (salience 10))
(id-root ?id zest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  zest.clp  zest0   "  ?id "  uwsAha )" crlf))
)

