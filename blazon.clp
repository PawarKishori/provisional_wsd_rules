;@@@ Added by 14anu26  [05-07-14]
;He blazoned the winner's name. 
;उसने जीतने वाले का नाम  घोषित किया.
(defrule blazon2
(declare (salience 5000))
(id-root ?id blazon)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 name)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GoRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blazon.clp 	blazon2   "  ?id " GoRiwa_kara )" crlf))
)

;$$$Moified by 14anu-ban-02(13-01-2015)
;meaning added 'rAjyacihna'
;His knowledge of medieval blazon was unrivalled.
;उसका मध्ययुगीन कुलचिह्न का  ज्ञान अद्वितीय था.
;@@@ Added by 14anu26  [20-06-14]
;His knowledge of medieval blazon was unrivalled.
;मध्ययुगीन कुलचिह्न का उसका ज्ञान अद्वितीय था.
(defrule blazon0
(declare (salience 5000))
(id-root ?id blazon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kulacihna/rAjyacihna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blazon.clp 	blazon0   "  ?id " kulacihna/rAjyacihna )" crlf))
)

;$$$Modified by 14anu-ban-02(13-01-2015)
;meanig changed from 'pramuKawA_se_praxarSiwa' to 'pramuKawA_se_praxarSiwa_ho'.
;They saw their company name blazoned all over the media.
;उन्होंने उनकी कम्पनी का नाम पूरे सञ्चार माध्यम में प्रमुखता से प्रदर्शित होते  देखा .[manual]
;@@@ Added by 14anu26  [20-06-14]
;They saw their company name blazoned all over the media.
;उन्होंने उनकी कम्पनी का नाम पूरे सञ्चार माध्यम में प्रमुखता से प्रदर्शित  देखा .
(defrule blazon1
(declare (salience 500)) 
(id-root ?id blazon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramuKawA_se_praxarSiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  blazon.clp 	blazon1   "  ?id " pramuKawA_se_praxarSiwa_ho )" crlf))
)
