;@@@ Added by 14anu-ban-04 (25-02-2015)
;The 800 metres is the fourth event of the afternoon.               [oald]
; दोपहर के बाद 800 मीटर  की चौथी प्रतियोगिता है .                                [self]
(defrule event1
(declare (salience 20))
(id-root ?id event)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id1 ?id2)
(subject-subject_samAnAXikaraNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiyogiwA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  event.clp 	event1   "  ?id "  prawiyogiwA)" crlf))
)

;@@@ Added by 14anu-ban-04 (25-02-2015)
;The women's 200 metre event will be followed by the men's 100 metres.                [cald]
;स्त्रियों की 200 मीटर प्रतियोगिता आदमियों के 100 मीटर के बाद की जाएगी .                                 [self]
(defrule event2
(declare (salience 20))
(id-root ?id event)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiyogiwA))              
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  event.clp 	event2   "  ?id "  prawiyogiwA)" crlf))
)


;@@@ Added by 14anu-ban-04 (25-02-2015)
;The police are trying to determine the series of events that led up to the murder.            [cald]
;पुलिस घटनाओं के क्रमों को निर्धारित करने का प्रयास कर रहीं हैं जिससे खूनी तक पहुँचा जा सके.                              [self]
(defrule event3
(declare (salience 20))
(id-root ?id event)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ? ?id1)
(viSeRya-jo_samAnAXikaraNa  ?id ?id1)
(kriyA-to_saMbanXI ? ?id2)
(id-root ?id2 murder)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatanA))              
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  event.clp 	event3   "  ?id "  GatanA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (25-02-2015)
;If we talk up the event, people will surely come.                 [cambridge]
;यदि हम कार्यक्रम सराहते हैं, तो लोग निश्चित रूप से आएँगे .                          [self]
(defrule event0
(declare (salience 10))
(id-root ?id event)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryakrama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  event.clp 	event0   "  ?id "  kAryakrama )" crlf))
)

