;@@@ Added by 14anu-ban-02(22.08.14)
;What's the abbreviation for ‘Saint’?[oald]
;seVnta kA safkRipwa kyA hE ?[manual]
(defrule abbreviation0
(declare (salience 0))
(id-root ?id abbreviation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkRipwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abbreviation.clp 	abbreviation0   "  ?id "  safkRipwa )" crlf))
)

;@@@ Added by 14anu-ban-02(22.08.14)
;The SI, with standard scheme of symbols, units and abbreviations, was developed and recommended by General Conference on Weights and Measures in 1971 for international usage in scientific, technical, industrial and commercial work. [ncert ch2]
;SI प्रतीकों, मात्रकों और उनके सङ्केताक्षरों की योजना 1971 में, मापतोल के महा सम्मेलन द्वारा विकसित कर, वैज्ञानिक, तकनीकी, औद्योगिक एवं व्यापारिक कार्यों में अन्तर्राष्ट्रीय स्तर पर उपयोग हेतु अनुमोदित की गई.[ncert ch2]

(defrule abbreviation1
(declare (salience 10))
(id-root ?id abbreviation)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ? ?id )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkewAkRara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abbreviation.clp 	abbreviation1   "  ?id "  safkewAkRara )" crlf))
)
