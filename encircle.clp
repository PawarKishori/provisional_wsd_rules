;@@@ Added by 14anu-ban-04 (03-02-2015)
;An amazing city encircled with curved mountains, beautiful architecture of churches and houses and dense pine trees is visible after   reaching Shillong.                     [tourism-corpus] 
;शिलांग  पहुँचते  ही  घुमावदार  पहाड़ों  ,  खूबसूरत  गिरजाघरों  ,  मकानों  की  शिल्पकला  तथा  सघन  चीड़  वृक्षों  से  घिरा  एक  अद्भुत  शहर  दिखाई  देता  है  ।                 [tourism-corpus]
(defrule encircle0
(declare (salience 10))
(id-root ?id encircle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GirA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encircle.clp 	encircle0   "  ?id "  GirA_ho)" crlf))
)

;@@@ Added by 14anu-ban-04 (03-02-2015)
;The enemy troop encircled the fort.                 [hinkhoj]
;शत्रु दल ने किला घेरा .                                    [self]
(defrule encircle1
(declare (salience 20))
(id-root ?id encircle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GerA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encircle.clp     encircle1   "  ?id "  GerA )" crlf))
)
