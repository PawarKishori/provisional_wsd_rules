

;Commented by 14anu-ban-01 on (30-12-2014) because sentence is not proper in itself and the correct meaning (without tam) is coming from the default rule share1  
;$$$ Modified by 14anu-ban-11 on (13-12-2014)
;Practice looking up words together and sharing the dictionary .
;एक साथ मिलकर ( सहभागी बनकर ) शब्दों को ढुंढने का और शब्द - कोष ( डिक्शनरीयों ) का प्रयोग करने का अभ्यास करें .
;@@@ Added by avni(14anu11)
;(defrule share5
;(declare (salience 5000))
;(id-root ?id share)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?id1)
;(id-root ?id1 dictionary)  ;Added by 14anu-ban-11 on (13-12-2014)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id shbAgI_bnkr))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  share.clp 	share5   "  ?id "  shbAgI_bnkr )" crlf))
;)



;Modified by Meena(20.5.11)
;At the end of the day, 251.2 million shares were traded. 
;Added by sheetal(25-02-10)
;Buying of shares was brisk on Wall Street today .
(defrule share2
(declare (salience 5000))
(id-root ?id share)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (kriyA-of_saMbanXI  ?kri ?id)(viSeRya-of_saMbanXI  ?kri ?id)(viSeRya-det_viSeRaNa  ?id  ?det)(viSeRya-saMKyA_viSeRaNa  ?id ?id1))
;(subject-subject_samAnAXikaraNa  ?kri ?sam)    ;commented by Meena(20.5.11) 
;(id-root ?sam brisk)                           ;commented by Meena(20.5.11) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Seyara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  share.clp     share2   "  ?id "  Seyara )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu06 Vivek Agarwal, BTech in CS, MNNIT Allahabad on 28 May 2014*************
;**********vivek17.agarwal@gmail.com************
;eg: The share of the primary sector has increased.
;     प्राथमिक क्षेत्र का हिस्सा बढ़ा है. 
(defrule share3
(declare (salience 5100))
(id-root ?id share)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id2) 
(id-word ?id2 sector|person|people|country|men)                  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hissA));spelling corrected from 'hisasA' to 'hissA' by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  share.clp     share3   "  ?id "  hissA )" crlf));spelling corrected by 14anu-ban-01 on (30-12-2014)
)

;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu11
;The person who took up a particular job should do it with sincerity and should share a part of his earnings with the jangomas and the poor .
;व्यक्ति जो भी काम अखितयार करे उसे ईमानदारी से करे तथा उसका एक हिस्सा जंगमों को और गरीबों को दे .
(defrule share4
(declare (salience 6000))
(id-root ?id share)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(kriyA-with_saMbanXI  ?id ?id2);changed "id1" to "id2" by 14anu-ban-01 on (30-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe));changed "ko_xe" to "xe" by 14anu-ban-01 on (30-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  share.clp 	share4   "  ?id "  xe )" crlf))
)


;@@@ Added by 14anu-ban-11 on (13-12-2014)
;We shared the pizza between the four of us.(oald)
;हमने  पिज्जा हम चार के बीच बाँटा. (manual)
(defrule share6
(declare (salience 5010))
(id-root ?id share)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 pizza);Added by 14anu-ban-11 on (13-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  share.clp 	share6   "  ?id "  bAzta )" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-02-2015)
;Share prices surged.(oald)
;शेयर मूल्य तेजी से चढे . [manual]
(defrule share7
(declare (salience 5001))
(id-root ?id share)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 price)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Seyara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  share.clp 	share7   "  ?id "  Seyara )" crlf))
)
;--------------------------- Default rules -----------------
(defrule share0
(declare (salience 4999));salience '5000' is changed with 4999 by sheetal.;Modified by sheetal(25-02-10)
(id-root ?id share)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hissA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  share.clp    share0   "  ?id "  hissA )" crlf))
)

;"share","N","1.hissA"
;Ram gave his share to his brother.
;
(defrule share1
(declare (salience 4900))
(id-root ?id share)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAJA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  share.clp 	share1   "  ?id "  sAJA_kara )" crlf))
)

;"share","V","1.sAJA_kara"
;--"2.sAJA_karanA"
;They shared the apartment.
;

