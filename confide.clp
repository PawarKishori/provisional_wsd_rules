;@@@ Added by 14anu-ban-03 (06-04-2015)
;‘It was a lie,’ he confided. [oald]
;'यह झूठ था,' उसने बताया . [manual]
(defrule confide1
(declare (salience 10))
(id-root ?id confide)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confide.clp 	confide1   "  ?id " bawA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (06-04-2015)
;I confide everything to my friend. [oald]
;मैं अपने मित्र को सभी गुप्त बात बतात[manual]anual]
(defrule confide0
(declare (salience 00))
(id-root ?id confide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gupwa_bAwa_bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confide.clp  confide0   "  ?id " gupwa_bAwa_bawA )" crlf))
)

