;@@@ Added by 14anu03 on 16-june-2014
;It looks as if this play will be a flop.
;यह लगता है यदि यह खेल असफल होगा .
(defrule look100
(declare (salience 5500))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 as)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lagawA_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  look.clp     look100   "  ?id "  " ?id1 "  lagawA_hE )" crlf))
)

(defrule look0
(declare (salience 5000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 after)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xeKaBAla_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look0  "  ?id "  " ?id1 "  xeKaBAla_kara  )" crlf))
)

;Do you think you could look after this baby while we're away?
;kyA wumheM lagawA hE ki jaba hama bAhara hoMge waba wuma isa SiSu kI xeKaBAla kara sakoge ?

;$$$Modified by 14anu-ban-08 (06-04-2015)  ;commented relation, added relation
;You should not look down on him just because he is illiterate.  [same clp file]
;तुम्हें उसका तिरस्कार नहीं करना चाहिए सिर्फ इसलिए कि वह अनपढ हैं.  [same clp file]
(defrule look1
(declare (salience 4900))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)       ;commented by 14anu-ban-08 (06-04-2015)
(kriyA-on_saMbanXI ?id ?id2)    ;added by 14anu-ban-08 (06-04-2015)   
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))   ;added by 14anu-ban-08 (06-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wiraskAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look1  "  ?id "  " ?id1 "  wiraskAra_kara  )" crlf))
)

;You should not look down on him just because he is illiterate.
;wumheM usakA wiraskAra nahIM karanA cAhie ,sirPa isalie ki vaha anapaDZa hE

;$$$Modified by 14anu-ban-08 (06-04-2015)       ;Run on parser 41
;Have you seen my spectacles? I've been looking for them since morning. [same clp file]
;क्या तुमने मेरे चशमें देखे है? मैं उन्हें सुबह से ढूँढ रहा हूँ.  [same clp file]
(defrule look2
(declare (salience 4800))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)          ;added 'id2' by 14anu-ban-08 (06-04-2015) 
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))  ;added by 14anu-ban-08 (06-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 DUzDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look2  "  ?id "  " ?id1 "  DUzDa  )" crlf))
)

;Have you seen my spectacles? I've been looking for them since morning. 
;kyA wumane mere caSmeM xeKe hEM ? mEM unheM subaha se DUzDZa rahA hUz
(defrule look3
(declare (salience 4700))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 forward)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 prawIkRA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look3  "  ?id "  " ?id1 "  prawIkRA_kara  )" crlf))
)

;PP_null_forward && transitivity=TR && category=verb	xilacaspI_ho	0
;I'm not looking forward to Diwali this year.
;isa sAla xivAlI ke lie merI koI xilacaspI nahIM hE
(defrule look4
(declare (salience 4600))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BeMta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look4  "  ?id "  " ?id1 "  BeMta_kara  )" crlf))
)

;I decided to look in on her when i was in her city.
;jaba mEM usake Sahara meM WA waba mEne usase milane kA niScaya kiyA
(defrule look5
(declare (salience 4500))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nirIkRaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look5  "  ?id "  " ?id1 "  nirIkRaNa_kara  )" crlf))
)

;$$$Modified by 14anu-ban-08 (06-04-2015)    ;Run on parser 70, added constraint
;Police is looking into the murder case which happened last night.  [same clp file]
;पुलीस उस कत्ल के मामले का निरीक्षण कर रही है जो पिछली रात को हुआ था.    [same clp file]
(defrule look6
(declare (salience 4600))     ;salience increased by 14anu-ban-08 (06-04-2015)
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)       ;added 'id2' by 14anu-ban-08 (06-04-2015)
(id-root ?id2 case)             ;added by 14anu-ban-08 (06-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nirIkRaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look6  "  ?id "  " ?id1 "  nirIkRaNa_kara  )" crlf))
)

;Police is looking into the murder case which happened last night.
;pulisa usa kZawla ke mAmale kA nirIkRaNa kara rahI hE jo piCalI rAwa huA WA


;$$$Modified by 14anu-ban-08 (06-04-2015)    ;Run on parser 13
;No, i won't participate-i'll just look on the game.  [same clp file]
;नहीं,मैं खेल में हिस्सा नहीं लूँगा केवल दर्शक बनूँगा.  [self]
(defrule look7
(declare (salience 4300))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)         ;added 'id2' by 14anu-ban-08 (06-04-2015)
(id-root ?id2 game)            ;added by 14anu-ban-08 (06-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xarSaka_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look7  "  ?id "  " ?id1 "  xarSaka_bana  )" crlf))
)

;No, i won't participate-i'll just look on the game.
;nahIM ,mEM hissA nahIM lUzgA-Kela meM kevala xarSaka banUzgA
(defrule look8
(declare (salience 4200))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sawarka_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look8  "  ?id "  " ?id1 "  sawarka_ho  )" crlf))
)

;Look out while crossing the road.
;sawarka raho !jaba BI sadZaka pAra karo
(defrule look9
(declare (salience 4100))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XyAna_se_xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look9  "  ?id "  " ?id1 "  XyAna_se_xeKa  )" crlf))
)

;$$$ Modified by 14anu04 on 20-June-2014
;He looked through me.
;उसने मुझे अनदेखा किया. 
;Please look through this report again && give me the feedback.     ;comment this example by 14anu-ban-08 (16-01-2015)
;kqpyA isa riporta ko xobArA XyAna se xeKo Ora muJe usakI prawipuRti xo
(defrule look10
(declare (salience 4000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI  ?id ?id2)        ;added by 14anu04
(id-root ?id1 through)
;(kriyA-upasarga ?id ?id1) ;commented by 14anu04
;(kriyA-object ?id ?) ;commented by 14anu04
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))       ;added by 14anu04
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 anaxeKA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look10  "  ?id "  " ?id1 "  anaxeKA_kara  )" crlf))
)

;$$$Modified by 14anu-ban-08 (11-02-2015)   ;relation added, constraint added
;$$$ Modified by 14anu04 on 20-June-2014
;He looked through the document. 
;उसने दस्तावेज विश्लेषण किया. 
(defrule look11
(declare (salience 3900))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id2))         ;added by 14anu-ban-08 (11-02-2015)
;(id-word ?id1 through)          ;commented by 14anu-ban-08 (11-02-2015)
;(kriyA-upasarga ?id ?id1)               ;commented by 14anu04
;(kriyA-object ?id ?);commented by 14anu04
(kriyA-through_saMbanXI ?id ?id1)    ;added by 14anu-ban-08 (11-02-2015)
(id-root ?id1 document)            ;added by 14anu-ban-08 (11-02-2015)
(id-root ?id2 through)             ;added by 14anu-ban-08 (11-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 viSleRaNa_kara)) ;changed meaning from 'XyAna_se_xeKa' to 'viSleRaNa_kara'    ;change 'id1' to 'id2' by 14anu-ban-08 (11-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look11  "  ?id "  " ?id2 "  viSleRaNa_kara  )" crlf))     ;change 'id1' to 'id2' by 14anu-ban-08 (11-02-2015)
)

;Please look through this report again && give me the feedback. 
;kqpyA isa riporta ko xobArA XyAna se xeKo Ora muJe usakI prawipuRti xo
(defrule look12
(declare (salience 3800))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Axara_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look12  "  ?id "  " ?id1 "  Axara_kara  )" crlf))
)

;We should always look up to our teachers.
;hameM hameMSAM apane aXyApakoM kA Axara karanA cAhie
(defrule look13
(declare (salience 3700))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 suXAra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look13  "  ?id "  " ?id1 "  suXAra_ho  )" crlf))
)

;This road seems to be looking up at last.
;isa sadZaka meM AKirakAra suXAra howA xiKAI xe rahA hE
(defrule look14
(declare (salience 3600))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 DUzDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look14  "  ?id "  " ?id1 "  DUzDa  )" crlf))
)

;I need her number,so u'll have to look it up.
;
(defrule look15
(declare (salience 3500))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 after)
(kriyA-after_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKaBAla_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look15 " ?id "  xeKaBAla_kara )" crlf)) 
)

(defrule look16
(declare (salience 3400))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 after)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xeKaBAla_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look16  "  ?id "  " ?id1 "  xeKaBAla_kara  )" crlf))
)

;$$$ Modified by Shirisha Manju (23-4-14) Suggested by Chaitanya Sir
;Removed 'kriyA-for_saMbanXI' relation and Modified wsd_root_mng as affecting_id-affected_ids-wsd_group_root_mng
;He is looking for a house.
;vaha Gara DUzDa rahA hE.
(defrule look17
(declare (salience 3300))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) for) ;added by Shirisha Manju (23-4-14)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) DUzDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look17  "  ?id "  " (+ ?id 1) "  DUzDa  )" crlf))
)

(defrule look18
(declare (salience 3200))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 DUzDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look18  "  ?id "  " ?id1 "  DUzDa  )" crlf))
)

(defrule look19
(declare (salience 3100))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-on_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ko_xeKa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look19 " ?id "  ko_xeKa )" crlf)) 
)

(defrule look20
(declare (salience 3000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ko_xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look20  "  ?id "  " ?id1 "  ko_xeKa  )" crlf))
)

(defrule look21
(declare (salience 2900))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 to)
(kriyA-to_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rakRA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look21 " ?id "  rakRA_kara )" crlf)) 
)

(defrule look22
(declare (salience 2800))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 to)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 rakRA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look22  "  ?id "  " ?id1 "  rakRA_kara  )" crlf))
)


(defrule look23
(declare (salience 2700))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DUzDa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look23 " ?id "  DUzDa )" crlf)) 
)

(defrule look24
(declare (salience 2600))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 DUzDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look24  "  ?id "  " ?id1 "  DUzDa  )" crlf))
)

;Rule removed/commented out by 14anu4 on 20-June-2014
;(defrule look25
;(declare (salience 2500))
;(id-root ?id look)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 upon)
;(kriyA-upasarga ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 KyAla_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look25  "  ?id "  " ?id1 "  KyAla_kara  )" crlf))
;)

(defrule look26
(declare (salience 2400))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nirIkRaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look26  "  ?id "  " ?id1 "  nirIkRaNa_kara  )" crlf))
)

(defrule look27
(declare (salience 2300))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 upon)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 KyAla_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look27  "  ?id "  " ?id1 "  KyAla_kara  )" crlf))
)

(defrule look28
(declare (salience 2200))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 take)
(kriyA-object ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xqRti))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look28   "  ?id "  xqRti )" crlf))
)

(defrule look29
(declare (salience 2100))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhara_xeKa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look29 " ?id "  bAhara_xeKa )" crlf)) 
)

(defrule look30
(declare (salience 2000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAhara_xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look30  "  ?id "  " ?id1 "  bAhara_xeKa  )" crlf))
)

;When somebody looks out
(defrule look31
(declare (salience 1900))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) like )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look31   "  ?id "  xiKa )" crlf))
)

(defrule look32
(declare (salience 1800))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-for_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DUzDa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look32 " ?id "  DUzDa )" crlf)) 
)

(defrule look33
(declare (salience 1700))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 DUzDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look33  "  ?id "  " ?id1 "  DUzDa  )" crlf))
)

;$$$Modified by 14anu-ban-08 (11-02-2015)   ;constraint added
;When you hold a pencil in front of you against some specific point on the background (a wall) and look at the pencil first through your left eye A (closing the right eye) and then look at the pencil through your right eye B (closing the left eye), you would notice that the position of the pencil seems to change with respect to the point on the wall.  [NCERT]
;जब आप किसी पेंसिल को अपने सामने पकडते हैं और पृष्ठभूमि (माना दीवार) के किसी विशिष्ट बिंदु के सापेक्ष पेंसिल को पहले अपनी बायीं आँख A से (दायीं आँख बन्द रखते हुए) देखते हैं, और फिर दायीं आँख B से (बायीं आँख बन्द रखते हुए), तो आप पाते हैं, कि दीवार के उस बिंदु के सापेक्ष पेंसिल की स्थिति परिवर्तित होती प्रतीत होती है.  [NCERT]
(defrule look34
(declare (salience 3901))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 at)          ;commented by 14anu-ban-08 (11-02-2015)
(kriyA-at_saMbanXI ?id ?id1) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program.       ;added 'id1' by 14anu-ban-08 (11-02-2015)
(id-root ?id1 pencil|clerk)    ;added by 14anu-ban-08 (11-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look34 " ?id "  xeKa )" crlf)) 
)

(defrule look35
(declare (salience 1500))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 at)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look35  "  ?id "  " ?id1 "  xeKa  )" crlf))
)

(defrule look36
(declare (salience 1400))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) at )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look36   "  ?id "  xeKa )" crlf))
)

;Below examples and translations Suggested by Aditi ji (27-04-14). Added by Roja.
;She is looking at the book.
;vaha kiwAba xeKa rahI hEM.
;She is looking at Meera.
;vaha Meera ko xeKa rahI hEM.
(defrule look37
(declare (salience 1300))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look37   "  ?id "  xeKa )" crlf))
)

;$$$ Modified by Shirisha Manju Suggested by Chaitanya Sir (12-12-13) -- Added +1 category as adjective fact
;That book looks interesting.
(defrule look38
(declare (salience 1400)) ;Salience increased by Roja(27-04-14). Counter Ex: The graveyard which was  enveloped in mist looked ghostly. Suggested by Aditi Mam.
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat_coarse =(+ ?id 1) adjective) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look38   "  ?id "  xiKa )" crlf))
)

;Look how blue it is.
;xeKie yaha kiwanA nIlA hE.
(defrule look39
(declare (salience 1100))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look39   "  ?id "  xeKa )" crlf))
)

; Hence the TR/INTR is commented.
(defrule look40
(declare (salience 1000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id najZara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look40   "  ?id "  najZara )" crlf))
)

(defrule look41
(declare (salience 900))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 me)
(viSeRya-at_saMbanXI ?id1 ?id) ;Replaced viSeRya-at_viSeRaNa as viSeRya-at_saMbanXI programatically by Roja 09-11-13
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look41   "  ?id "  xeKa )" crlf))
)

(defrule look42
(declare (salience 800))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 it)
(viSeRya-at_saMbanXI ?id1 ?id) ;Replaced viSeRya-at_viSeRaNa as viSeRya-at_saMbanXI programatically by Roja 09-11-13
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look42   "  ?id "  xeKa )" crlf))
)

(defrule look43
(declare (salience 700))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look43 " ?id "  xeKa )" crlf)) 
)

(defrule look44
(declare (salience 600))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look44  "  ?id "  " ?id1 "  xeKa  )" crlf))
)

(defrule look45
(declare (salience 500))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-around_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAroM_ora_xeKa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " look.clp look45 " ?id "  cAroM_ora_xeKa )" crlf)) 
)

(defrule look46
(declare (salience 400))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cAroM_ora_xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look46  "  ?id "  " ?id1 "  cAroM_ora_xeKa  )" crlf))
)

; He look around for help.
(defrule look47
(declare (salience 300))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 again)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look47   "  ?id "  xeKa )" crlf))
)

;Added by Nandini (3-11-13)
;She looked back now to that fatal day in December. [sentence from mail]
;xisambara ke  usa nirNAyaka xina ko aba usane piCe_mudakara xeKA.
(defrule look48
(declare (salience 2950))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 piCe_mudakara_xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look48  "  ?id "  " ?id1 "  piCe_mudakara_xeKa  )" crlf))
)

;@@@ Added by Nandini (2-1-14)
;To his surprise Minakshi looked more beautiful than ever before.[sentence from mail]
;use AScarya huA Minakshi pahale  kI apekRA Ora  aXika sunxara xiKa rahI WI.
(defrule look49
(declare (salience 2980))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(subject-subject_samAnAXikaraNa  ?id1 ?id2)
(id-word ?id2 beautiful)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look49   "  ?id "  xiKa )" crlf))
)

;@@@ Added by 14anu20 dated 16/06/2014
;You look very cool.
;आप अत्यन्त बढिया दिखते हैं .
(defrule look50
(declare (salience 1900))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id =(- ?id 1))
(subject-subject_samAnAXikaraNa  =(- ?id 1) ?id1)
(id-cat_coarse ?id1 adjective)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look50   "  ?id "  xiKa )" crlf))
)

;@@@ Added by 14anu07 0n 03/07/2014
;They are looking forward to Musharraf ' s visit.
;वे मुशर्रफ के आने की प्रतीक्षा कर रहे हैं . 
(defrule look51
(declare (salience 4000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) forward)
(id-root =(+ ?id 2) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) prawIkRA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp look51  "  ?id "  " (+ ?id 1) " " (+ ?id 2) "  prawIkRA_kara  )" crlf))
)


;default_sense && category=verb	najZara_A	0
;default_sense && category=noun	xqRti	0
;"look","N","1.xqRti"
;Have a look at this book.
;--"2.avalokana"
;A pleasant look.
;--"3.PESana"
;A new look hairdress.
;--"4.SElI"
;A new look hairstyle.
;--"5.walASa"
;I have had a good look at the shelf but I could not get the book that I needed.
;
;

;@@@Added by 14anu-ban-08 (05-02-2015)
;We have drawn an observer looking at 90° to the direction of the sun.   [NCERT]
;हमने सूर्य की दिशा के 90° पर देखते हुए एक प्रेक्षक को दर्शाया है.  [NCERT]
(defrule look52
(declare (salience 3000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 direction)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKawe_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look52   "  ?id "  xekawe_ho)" crlf))
)

;@@@Added by 14anu-ban-08 (05-02-2015)
;Therefore objects which are not resolved at far distance, can be resolved by looking at them through a telescope.  [NCERT]
;इसलिए जिन बिंबों (objects) का विभेदन बहुत अधिक दूरी पर नहीं किया जा सकता, उन्हें दूरदर्शी द्वारा देखकर विभेदित किया जा सकता है.  [NCERT]
(defrule look53
(declare (salience 4000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI ?id ?id1)
(id-root ?id1 telescope)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look53   "  ?id "  xeKakara)" crlf))
)

;@@@Added by 14anu-ban-08 (05-02-2015)
;This can be checked by looking at the reflected light through an analyser.  [NCERT]
;इसकी जाँच परावर्तित प्रकाश को किसी विश्लेषक में से होकर आने वाले प्रकाश को देखकर की जा सकती है.  [NCERT]
(defrule look54
(declare (salience 4000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI ?id ?id1)
(id-root ?id1 light)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look54   "  ?id "  xeKakara)" crlf))
)

;@@@Added by 14anu-ban-08 (11-02-2015)
;I called her but she looked straight through me.   [same clp file]   
;मैंने उसे बुलाया लेकिन उसनें मुझे अनदेखा कर दिया.   [same clp file]
(defrule look55
(declare (salience 4001))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id2))
(kriyA-through_saMbanXI ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?id2 through)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 anaxeKA_kara_xe)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look55  "  ?id "  " ?id2 "  anaxeKA_kara_xe  )" crlf))   
)


;@@@Added by 14anu-ban-08 (11-02-2015)
;I was dazzled by his charm and good looks.    [cambridge]
;मैं उसके आकर्षक और अच्छे  रूप से चकित हो गया था .        [self]
(defrule look56
(declare (salience 4000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 good)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look56   "  ?id "  rUpa)" crlf))
)

;@@@Added by 14anu-ban-08 (02-03-2015)
;All monetary transactions were looked after by her.  [hindkhoj]
;सभी पैसे से सम्बन्धित लेन-देन उसके द्वारा सँभाले गये थे .           [self]
(defrule look57
(declare (salience 4900))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) after )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) sazBAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " look.clp	look57  "  ?id "  " (+ ?id 1) "  sazBAla  )" crlf))
)

;@@@Added by 14anu-ban-08 (06-04-2015)    ;Run on parser 2
;Empty houses quickly take on a forlorn look.[OALD]
;खाली मकान  जल्द ही सूना दिखने लगता है. [self]
(defrule look58
(declare (salience 4000))
(id-root ?id look)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 forlorn)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  look.clp 	look58   "  ?id "  xiKanA)" crlf))
)
