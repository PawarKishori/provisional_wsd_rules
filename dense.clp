;@@@ added by Pramila(BU) on 12-12-2013
;How can she be so dense?       ;sentence of this file
;वह इतनी मंदबुद्धि कैसे हो सकती है ?
(defrule dense1
(declare (salience 5000))
(id-root ?id dense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manxabuxXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dense.clp 	dense1   "  ?id "  manxabuxXi )" crlf))
)


;@@@ Added by 14anu-ban-04 (20-02-2015)
;We know that condensed matter (solids and liquids) and dense gases at all temperatures emit electromagnetic radiation in which a continuous distribution of several wavelengths is present, though with different intensities.        [NCERT-CORPUS]
;हम जानते हैं कि सङ्घनित पदार्थ (ठोस तथा द्रव) तथा सघन गैसें सभी तापों पर वैद्युतचुम्बकीय विकिरण उत्सर्जित करते हैं जिसमें अनेक तरङ्गदैर्घ्यों का सन्तत वितरण विद्यमान होता है यद्यपि उनकी तीव्रताएँ भिन्न होती हैं.                        [NCERT-CORPUS]
(defrule dense2
(declare (salience 4010))
(id-root ?id dense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 gas|glass|air|nucleus|substance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saGana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dense.clp 	dense2   "  ?id "  saGana )" crlf))
)


;@@@ Added by 14anu-ban-04 (20-02-2015)
;There are some really dense people in our class.                     [cald]
;वास्तव में हमारी कक्षा  में कुछ   लोग  मूर्ख हैं .                                       [SELF]
(defrule dense3
(declare (salience 4010))
(id-root ?id dense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  mUrKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dense.clp 	dense3  "  ?id "   mUrKa )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule dense0
(declare (salience 4000))
(id-root ?id dense)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dense.clp 	dense0   "  ?id "  GanA )" crlf))
)


;"dense","Adj","1.GanA"
;The deforestation of the dense forests of the central India will lead to an ecological imbalance.
;--"2.manxabuxXi
;How can she be so dense? 
;
;
;****************rule was removed***************
;meaning - GanA  (it was duplicate rule)
