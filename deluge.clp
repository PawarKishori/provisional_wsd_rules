;@@@ Added by 14anu-ban-04 on (24-03-2015) 
;The town was deluged due to the heavy monsoon rains.               [hinkhoj]          
;नगर भारी मानसून की वर्षा के कारण जलमग्न  हो गया था/आप्लावित हो गया था  .                               [self]
(defrule deluge2
(declare (salience 110))
(id-root ?id deluge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-tam_type ?id passive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalamagna_ho/AplAviwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deluge.clp 	deluge2   "  ?id "  jalamagna_ho/AplAviwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-04 on (24-03-2015) 
;A deluge of letters.                [oald]
;पत्रों की भरमार .                        [self]
(defrule deluge3
(declare (salience 110))              
(id-root ?id deluge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaramAra))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deluge.clp 	deluge3  "  ?id "  BaramAra)" crlf))
)



;$$$ Modified by 14anu-ban-04 on (24-03-2015)        ----changed meaning from 'acAnaka' to 'jala_pralaya'
;When the snow melts, the mountain stream becomes a deluge.                    [oald]
;जब बरफ पिघलती है,तो पहाड़ी नदी जल-प्रलय बन जाती है .                                      [self]
;An unseasonable deluge had washed away a section of river bank.                 [oald]
;बेमौसम जल-प्रलय ने नदी के किनारे  का भाग मिटा दिया था .                                       [self]
(defrule deluge0
(declare (salience 100))              ;salience reduced from '5000' to '100' by 14anu-ban-04 on (24-03-2015)
(id-root ?id deluge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jala-pralaya))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deluge.clp 	deluge0   "  ?id "  jala-pralaya )" crlf))
)

(defrule deluge1
(declare (salience 100))           ;salience reduced from '4900' to '100' by 14anu-ban-04 on (24-03-2015)
(id-root ?id deluge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM_dUba_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deluge.clp 	deluge1   "  ?id "  meM_dUba_jA )" crlf))
)

;"deluge","VT","1.meM_dUba_jAnA"
;The town was deluged due to the heavy monsoon rains.
;
;
