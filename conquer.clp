
;@@@ Added by 14anu-ban-03 (14-04-2015)
;The world champion conquered yet another challenger last night. [oald]
;विश्व विजेता ने पिछली रात एक और दावेदार को हराया . [manual]  
(defrule conquer1
(declare (salience 10))
(id-root ?id conquer)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 challenger)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id harA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  conquer.clp 	conquer1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conquer.clp 	conquer1   "  ?id "  harA )" crlf))
)

;@@@ Added by 14anu-ban-03 (14-04-2015)
;Mount Everest was conquered (= successfully climbed) in 1953. [oald]
;1953 में माव्न्ट एवरस्ट पर  सफलतापूर्वक चढाई कि गई थी.  [manual]  
(defrule conquer2
(declare (salience 10))
(id-root ?id conquer)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 Everest)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id para))
(assert (id-wsd_root_mng ?id saPalawApUrvaka_caDAI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  conquer.clp  conquer2   "  ?id " para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conquer.clp 	conquer2  "  ?id "  saPalawApUrvaka_caDAI_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (14-04-2015)
;The band is now setting out to conquer the world. [oald]
;बैंड विश्व में लोकप्रिय होने के लिए निकल पडा है .  [manual]
(defrule conquer3
(declare (salience 10))
(id-root ?id conquer)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 band)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id meM))
(assert (id-wsd_root_mng ?id lokapriya_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  conquer.clp  conquer3   "  ?id " meM )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conquer.clp 	conquer3   "  ?id "  lokapriya_ho )" crlf))
)

;------------------------------------ Default Rules -----------------------------------------

;@@@ Added by 14anu-ban-03 (14-04-2015)
;The Normans conquered England in 1066. [oald]
;नोर्मन ने 1066 में इंग्लैंड जीता . [manual]
(defrule conquer0
(declare (salience 00))
(id-root ?id conquer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conquer.clp 	conquer0   "  ?id "  jIwa )" crlf))
)

