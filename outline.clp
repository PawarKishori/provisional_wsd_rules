;Added by sheetal(5-01-10).
(defrule outline2
(declare (salience 4950))
(id-root ?id outline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(id-word ?id1 marker)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golIkqwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  outline.clp   outline2   "  ?id "  golIkqwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (06-04-2015)
;The bold outline of a mountain against the sky. [oald]
;आसमान के सम्मुख पर्वत की मोटी पट्टी.		[Manual]

(defrule outline3
(declare (salience 5001))
(id-root ?id outline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 bold)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pattI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  outline.clp 	outline3   "  ?id "  pattI )" crlf))
)

;------------------------ Default Rules ----------------------

;"outline","N","1.rUpareKA"
;That is an outline map of Maharashtra.
(defrule outline0
(declare (salience 5000))
(id-root ?id outline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpareKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  outline.clp 	outline0   "  ?id "  rUpareKA )" crlf))
)

;"outline","V","1.rUpareKA_AreKiwa_karanA"
;You must outline the important sentences with a marker.
(defrule outline1
(declare (salience 4900))
(id-root ?id outline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpareKA_AreKiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  outline.clp 	outline1   "  ?id "  rUpareKA_AreKiwa_kara )" crlf))
)

