;@@@ Added by 14anu-ban-04 (30-03-2015)
;His arm was draped casually around her shoulders.              [oald]
;उसकी बाँह को उसके कन्धों के चारों ओर यूँ ही टिकाया गया था.                      [self]
(defrule drape1
(declare (salience 20))
(id-root ?id drape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject  ?id ?id1)(viSeRya-kqxanwa_viSeRaNa ?id1 ?id))
(id-root ?id1 arm|neck|leg|head)
(kriyA-around_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id tikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  drape.clp     drape1  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drape.clp    drape1  "  ?id "  tikA)" crlf))
)

;@@@ Added by 14anu-ban-04 (30-03-2015)
;She draped the scarf loosely around her shoulders.                    [cald]
;उसने अपने कन्धों के चारों ओर स्कार्फ को ढीला  ओढ़ा .                                   [self]
(defrule drape2
(declare (salience 30))
(id-root ?id drape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id2)
(kriyA-around_saMbanXI ?id ?id1)
(id-root ?id1 shoulder|neck)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id oDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  drape.clp     drape2  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drape.clp    drape2  "  ?id "  oDa)" crlf))
)


;@@@ Added by 14anu-ban-04 (30-03-2015)
;The coffins were all draped with the national flag.         [cald]	
;सभी ताबूतों को  राष्ट्र ध्वज से ढका गया था .                               [self]
(defrule drape3
(declare (salience 20))
(id-root ?id drape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id Daka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  drape.clp     drape3  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drape.clp    drape3  "  ?id "  Daka)" crlf))
)

;@@@ Added by 14anu-ban-04 (30-03-2015)
;Walls draped in ivy.                                [oald]
;दीवारों को आइवी लता से सजाया .                                  [self] 
(defrule drape4
(declare (salience 30))
(id-root ?id drape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 wall)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id sajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  drape.clp     drape4  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drape.clp    drape4  "  ?id "  sajA)" crlf))
)

;@@@ Added by 14anu-ban-04 (30-03-2015)
;He draped his coat over the back of the chair.                                  [oald]
;उसने कुर्सी के पीछे अपना कोट लटकाया .                                              [self]
(defrule drape5
(declare (salience 20))
(id-root ?id drape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id latakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drape.clp    drape5 "  ?id "  latakA)" crlf))
)

;@@@ Added by 14anu-ban-04 (30-03-2015)
;The body was draped in a blanket.                           [oald]
;शरीर को कम्बल में ढका गया था .                                    [self]
(defrule drape6
(declare (salience 20))
(id-root ?id drape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id Daka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  drape.clp     drape6  "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drape.clp    drape6  "  ?id "  Daka)" crlf))
)

;--------------------------------------------------DEFAULT RULE ----------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (30-03-2015)
;She had a shawl draped around her shoulders.                         [oald]  ;run this sentence on parse no. 2
;उसकी शाल उसके कन्धों के चारों ओर लटकी हुई थी .                                  [self]
(defrule drape0
(declare (salience 10))
(id-root ?id drape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drape.clp    drape0 "  ?id "  lataka)" crlf))
)
