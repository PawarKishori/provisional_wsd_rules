;@@@ Added by 14anu-ban-10 on (26-02-2015)
;Her servant maid arrives regularly at seven every morning.[hinkhoj]
;उसकी नौकरानी प्रत्येक सुबह नियमित रूप से सात बजे आती है .[manual] 
(defrule regularly0
(declare (salience 100))
(id-root ?id regularly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyamiwa_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regularly.clp 	regularly0   "  ?id "   niyamiwa_rUpa_se)" crlf))
)

;@@@ Added by 14anu-ban-10 on (26-02-2015)
;The plants were spaced regularly, about 50 cm apart.[oald]
;पौधे 50 सेन्टीमीटर दूरी पर आस पास  व्यवस्थित,बराबर अन्तर पर लगाये  गये थे . [manual]
(defrule regularly1
(declare (salience 200))
(id-root ?id regularly)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 change)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavasWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regularly.clp 	regularly1   "  ?id "   vyavasWiwa)" crlf))
)
