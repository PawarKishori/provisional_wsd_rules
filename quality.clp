
;@@@ Added by 14anu-ban-11 on (18-03-2015)
;The quality of wheat from the northern region is superior. [superior.clp]
;उत्तरी क्षेत्र के गेहूँ की  किस्म श्रेष्ठ है .  [self]
(defrule quality4
(declare (salience 10))
(id-root ?id quality)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 wheat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quality.clp 	quality4   "  ?id "  kisma)" crlf))
)


;@@@Added by 14anu17
;She has all the qualities of a good teacher.[oxford]
;उसके पास अच्छे शिक्षक के सभी गुण हैं . 
(defrule quality1
(declare (salience 1))
(id-root ?id quality)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quality.clp 	quality1   "  ?id "  guNa )" crlf))
)

;@@@Added by 14anu17
;We aim to provide quality products at reasonable prices.[oxford]
;हम उचित मूल्यों में उच्चय पदार्थ देने के लिए लक्षित करते हैं . 
(defrule quality2
(declare (salience 1))
(id-root ?id quality)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uccaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quality.clp 	quality2   "  ?id "  uccaya )" crlf))
)

;@@@Added by 14anu17
;To have leadership quality.[oxford]
;नेतृत्व गुण का होना . 
(defrule quality3
(declare (salience 1))
(id-root ?id quality)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1)  leadership|administrative|skill|supremacy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quality.clp 	quality3   "  ?id "  guNa )" crlf))
)

;@@@Added by 14anu17
;Goods of  high quality.[oxford]
;ऊँची गुणवत्ता का  सामान.  
(defrule quality0
(declare (salience 0))
(id-root ?id quality)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNavawwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quality.clp 	quality0   "  ?id "  guNavawwA )" crlf))
)
