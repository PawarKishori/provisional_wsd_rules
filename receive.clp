;$$$ Modiified by Bhagyashri on (13-06-2016)
;They received me warmly.(OLD)
;उन्होंने मेरा ्च्छेसे स्वागत किया. 
;This building is ready to receive its new occupants.(OLD)
;यह िमारत उसके नये निवासियों का स्वागत करने के लिये तैयार है.
(defrule receive1
(declare (salience 10))
(id-root ?id receive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1) 
(kriyA-subject ?id ?id2)
(id-original_word ?id2 ?word)
(id-original_word ?id1 ?word1)
(or (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(word-nertype  ?word1   PERSON))
(or (id-root ?id2 ?str1&:(and (not (numberp ?str1))(gdbm_lookup_p "animate.gdbm" ?str1)))(word-nertype	?word	PERSON))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAgawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receive.clp 	receive1   "?id "  svAgawa_kara )" crlf))
)

;$$$ Modiified by Bhagyashri on (13-06-2016)
;I was received by the hosts.
;merA mejabAna ke xvArA svAgawa kiyA gayA WA.
;The chief was received warmly.
;muKiyA kA uwsAha se svAgawa kiyA gayA WA.
(defrule receive2
(declare (salience 10))
(id-root ?id receive)
?mng <-(meaning_to_be_decided ?id)
(id-tam_type ?id passive)
(kriyA-subject ?id ?id2)
(id-root ?id2 ?str1&:(and (not (numberp ?str1))(gdbm_lookup_p "animate.gdbm" ?str1)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAgawa_kara))
(assert (kriyA_id-subject_viBakwi ?id  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receive.clp 	receive2   "?id "  svAgawa_kara )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  receive.clp      receive2   "  ?id " kA )" crlf)
)

;@@@ Added by Anita-2013
;He was received into the church. [cambridge.org/dictionary/learner-english]
;उसको चर्च में शामिल किया गया  ।
;Three young people were received into the church. (OLD)  added by Bhagyashri (11-06-16)
;तीन युवा लोग ईस्टर पर गिरजाघर में शामिल किए गये थे . 

(defrule receive3
(declare (salience 15))
(id-root ?id receive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAmila_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receive.clp 	receive3   "?id "  SAmila_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-12-2015)
;I returned home, and when I announced that I would been taken back into the service and should receive a salary, heavens, what a to-do there was!..." [Crime and Punishment]
;मैं घर लौट आया और जब मैंने सबको बताया कि मैं नौकरी पर फिर बहाल कर दिया गया हूँ और मुझे तनख्वाह मिला करेगी, तो कसम से, कैसा जश्न हुआ [Crime and Punishment]
;When did you receive my telegram?[hinkhoj]
;Apako merA wAra_kA samAcAra kaba milA? [manual]
(defrule receive4
(declare (salience 10))
(id-root ?id receive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 salary|money|instruction|service|support|telegram)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   mila))
(assert (kriyA_id-subject_viBakwi ?id  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receive.clp 	receive4   "?id "     mila)" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  receive.clp      receive4   "  ?id " ko )" crlf)
)


;#####################Default rule################################
;@@@ Added by Anita-2013
;Did you receive my letter? [cambridge.org/dictionary/learner-english]
;क्या आपने मेरा खत प्राप्त किया ?[verified-sentence]
;I received a phone call from your mother. [cambridge.org/dictionary/learner-english]
;तुम्हारी माँ से मुझे फ़ोन कॉल प्राप्त हुई थी ।
;Members of Parliament received a 4.2% pay increase this year. [cambridge.;org/dictionary/learner-english]
;पार्लियामेंट के सदस्यों को इस वर्ष ४.२ % की बढोत्तरी प्राप्त हुई ।
;She died after receiving a blow to the head. [cambridge.org/dictionary/learner-english]
;सिर पर चोट लगने के बाद वह मृत्यु को प्राप्त हुआ ।
;He received a phone call from the police this morning. [By mail]
;उसने आज सुबह पुलिस से एक फोन कॉल प्राप्त की ।
;Emergency cases will receive professional attention immediately.(OLD) added by Bhagyashri (11-06-16)
;आपातकालीन परिस्थितियाँ तत्काल व्यावसायिक ध्यान प्राप्त करेंगी .
;He received an award for bravery from the police service. (OLD) added by Bhagyashri (11-06-16)
;उसने पुलिस सेवा से साहस के लिए एक पुरस्कार प्राप्त किया . 
(defrule receive0
(id-root ?id receive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  receive.clp 	receive0   "?id "  prApwa_kara )" crlf))
)


