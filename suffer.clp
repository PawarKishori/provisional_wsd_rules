;$$$ Modified by 14anu02 on 16.06.14
;His daughter suffers from diabetes.
;उसकी बेटी मधुमेह से पीडित है. 
(defrule suffer0
(declare (salience 5000))
(id-root ?id suffer)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id suffering )	;commented by 14anu02
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) from)	;Added by 14anu02	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIdZiwa))	;changed from 'vexanA_saha' to 'se pIdZiwa' by 14anu02
;(assert (id-H_vib_mng ?id ing)) ;Commented by Sukhada(20-05-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  suffer.clp  	suffer0   "  ?id "  pIdZiwa )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*wsd_dir* "  suffer.clp     suffer0   "  ?id " ing )" crlf))
))

;@@@ Added by jagriti(11.1.2014)
;The company suffered a huge losses .
;कम्पनी ने एक भारी नुकसान उठाया . 
(defrule suffer1
(declare (salience 4900))
(id-root ?id suffer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 loss)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  suffer.clp  	suffer1   "  ?id "  uTA )" crlf)
))

(defrule suffer2
(declare (salience 4800))
(id-root ?id suffer)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id suffering )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vexanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  suffer.clp  	suffer2  "  ?id "  vexanA )" crlf))
)



;Modified by Meena(2.9.11)
;Fonda himself once again suffered the attribution of the word wooden. 
(defrule suffer3
(declare (salience 100))
(id-root ?id suffer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jela))
;(assert (id-wsd_root_mng ?id ko_Jela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suffer.clp 	suffer3   "  ?id "  Jela )" crlf))
)

;default_sense && category=verb	saha	0
;"suffer","V","1.sahanA"
;She is suffering from jaundice.
;
;

;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu02 on 16.06.14
;His relationship with Anne did suffer.
;अन्ने के साथ उसके सम्बन्ध बुरी तरह प्रभावित हुए. 
;एनी के साथ उसका सम्बन्ध बुरी तरह प्रभावित हुअा[Translation improved by 14anu-ban-01 ]
(defrule suffer4
(declare (salience 5000))
(id-root ?id suffer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id burI_waraha_praBAviwa_ho ));changed "burI_waraha_praBAviwa" to "burI_waraha_praBAviwa_ho" by 14anu-ban-01
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  suffer.clp  	suffer4  "  ?id "  burI_waraha_praBAviwa_ho )" crlf))                  ;changed "burI_waraha_praBAviwa" to "burI_waraha_praBAviwa_ho" by 14anu-ban-01 on (12-01-2015)
)

