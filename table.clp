;Added by Shirisha Manju Suggested by Chaitanya Sir (18-09-13)
;Other units retained for general use are given in Table 2.2.
;sAmAnya vyavahAra meM Ane vAle anya mAwraka sAraNI 2.2 meM xie gae hEM.
(defrule table_with_no
(declare (salience 5051))      ;salience increased from 5001 to 5051 by 14anu-ban-07 because of rule table3
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-saMKyA_viSeRaNa  ?id ?id1)(id-cat_coarse  =(+ ?id 1) number))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAraNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  table.clp     table_with_no   "  ?id "  sAraNI )" crlf))
)


(defrule table0
(declare (salience 5000))
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mejZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  table.clp 	table0   "  ?id "  mejZa )" crlf))
)

;"table","N","1.mejZa"
;--"2.meja"
;Don't spread things on the table .
;--"3.sUcI"
;You can see the table && check the prices.
;--"4.prawiyogiwA_meM_KilAdiyo_kA_sWAna"
;He will go to the top of the league table,if he wins.
;--"5.pahAdA"
;Learn the three times table.
;
(defrule table1
(declare (salience 4900))
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswAviwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  table.clp 	table1   "  ?id "  praswAviwa_kara )" crlf))
)

;"table","VT","1.praswAviwa_karanA"
;The minister will table the bill in the parliament on Monday.
;

;@@@ Added by Prachi Rathore[25-2-14]
;Do you know your six times table?[oald]
;क्या आप छः का पहाडा जानते हैं? 
(defrule table2
(declare (salience 5050))
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?id1)
(id-root ?id1 time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahAdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  table.clp 	table2   "  ?id "  pahAdA )" crlf))
)


;@@@ Added by Prachi Rathore[25-2-14]
;The rounded numbers are listed in the table.
;पूर्णांकित अंक  तालिका में सूचीबद्ध किए गये हैं .
(defrule table3
(declare (salience 5050))
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAlikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  table.clp 	table3   "  ?id "  wAlikA)" crlf))
)

;@@@ Added by 14anu12 on 28-6-2014
;The team has slid down the First Division table.
;टीम ने प्रथम दिविओन्सी टेबुल के नीचे फिसलना है . 
(defrule table4
(declare (salience 5050))
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tebula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  table.clp 	table4   "  ?id "  tebula)" crlf))
)


;@@@ Added by 14anu-ban-07,(19-08-2014)
;टेबल फैन :
;The rotation may be about an axis that is fixed (e.g. a ceiling fan) or moving (e.g. an oscillating table fan). (ncert corpus)
;घूर्णन किसी ऐसी अक्ष के परितः हो सकता है जो स्थिर हो (जैसे छत के पङ्खे में) या फिर एक ऐसी अक्ष के परितः जो स्वयं घूमती हो (जैसे इधर से उधर घूमते मेज के पङ्खे में).(ncert corpus)
;घूर्णन किसी ऐसी अक्ष के परितः हो सकता है जो स्थिर हो (जैसे छत के पङ्खे में) या फिर एक ऐसी अक्ष के परितः जो स्वयं घूमती हो (जैसे इधर से उधर घूमते टेबल फैन में).(manually)
(defrule table5
(declare (salience 5000))
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) fan)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) webala_PEna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " table.clp	 table5  "  ?id "  " =(+ ?id 1) " webala_PEna  )" crlf))
)

;@@@ Added by 14anu-ban-07 (27-11-2014)
;Obviously, these tables are not exhaustive.(ncert)
;स्पष्ट है कि ये सारणी विस्तृत नहीं हैं.(ncert)
(defrule table6
(declare (salience 5200))
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 exhaustive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAraNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  table.clp 	table6   "  ?id "  sAraNI )" crlf))
)

;@@@ Added by 14anu-ban-07 (27-11-2014)
;You will appreciate from this table the multi-cultural, international character of the scientific endeavor.(ncert)
;इस सारणी द्वारा आप वैज्ञानिक प्रयासों के बहु-सांस्कृतिक, अन्तर्राष्ट्रीय स्वरूप का मूल्याङ्कन करेँगे.(ncert)
(defrule table7
(declare (salience 5300))
(id-root ?id table)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id2)
(kriyA-from_saMbanXI  ?id1 ?id)
(id-root ?id2 character)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAraNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  table.clp 	table7   "  ?id "  sAraNI )" crlf))
)
