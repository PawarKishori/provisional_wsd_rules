;@@@ Added by 14anu05 on 21.06.14
;He was out for a duck.
;वह शून्य पर अाउट हुअा.
(defrule duck2
(declare (salience 5000))
(id-root ?id duck)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id2 ?id)
(kriyA-kriyA_viSeRaNa  ?id2 ?id1)
(id-cat_coarse ?id2 verb)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SUnya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  duck.clp 	duck2   "  ?id "  SUnya )" crlf))
)

;@@@ Added by 14anu-ban-04 (24-02-2015)
;She ducked her head and got into the car.                     [oald]
;उसने अपना सिर झुकाया और गाड़ी में चला गया .                              [self]
(defrule duck3
(declare (salience 4910))
(id-root ?id duck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JukA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  duck.clp 	duck3   "  ?id "  JukA )" crlf))
)

;@@@ Added by 14anu-ban-04 (24-02-2015)
;The government is ducking the issue.                    [oald]
;सरकार समस्या को टाल रही है .                                   [self]
;He ducked the first few blows then started to fight back.       [oald]    (run this sentence on parser no. 6)
;उसने पहले कुछ   प्रहारों  को टाला  तब वापिस लड़ाई करना  शुरु किया .                 [self]    
(defrule duck4
(declare (salience 4920))
(id-root ?id duck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 issue|blow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tAla))
(assert (kriyA_id-object_viBakwi ?id ko)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  duck.clp     duck4   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  duck.clp 	duck4   "  ?id "  tAla )" crlf))
)


;@@@ Added by 14anu-ban-04 (24-02-2015)
;The kids were ducking each other in the pool.               [oald]
;बच्चे ताल में एक दूसरे को धकेल रहे थे .                                   [self]
(defrule duck5
(declare (salience 4920))
(id-root ?id duck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI   ?id ?id1)
(id-root ?id1 pool|water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xakela))
(assert (kriyA_id-object_viBakwi ?id ko)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  duck.clp     duck5   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  duck.clp 	duck5   "  ?id "  Xakela)" crlf))
)

;@@@ Added by 14anu-ban-04 (24-02-2015)
;She ducked into the adjoining room as we came in.         [oald]
;वह सटे हुए कमरे में छिप गई जैसे हम अन्दर आए .                          [self]
(defrule duck6
(declare (salience 4920))
(id-root ?id duck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(kriyA-into_saMbanXI ?id ?id2)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cipa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  duck.clp 	duck6   "  ?id "  Cipa_jA)" crlf))
)

;@@@ Added by 14anu-ban-04 (24-02-2015)
;I ducked down behind the table.                    [oald]
;मैं मेज के पीछे छिपा.                                      [self]
(defrule duck7
(declare (salience 4920))
(id-root ?id duck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 down)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Cipa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " duck.clp	 duck7  "  ?id "  " ?id1 "  Cipa)" crlf))
)

;------------------------ Default Rules ----------------------

;"duck","N","1.bawaKa/2.SUnya{kriketa_meM}"
(defrule duck0
(declare (salience 5000))
(id-root ?id duck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawaKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  duck.clp 	duck0   "  ?id "  bawaKa )" crlf))
)

;"duck","VI","1.JukanA/2.dubakI_mAranA"
;Before he could duck, another stone struck him.
(defrule duck1
(declare (salience 4900))
(id-root ?id duck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Juka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  duck.clp 	duck1   "  ?id "  Juka )" crlf))
)

