;@@@Added by 14anu-ban-02(23-03-2015)
;She felt her husband constantly belittled her achievements.[oald]
;उसने महसूस किया कि उसके पति ने लगातार उसकी उपलब्धियों का अपमान किया . [self]
(defrule belittle1 
(declare (salience 100)) 
(id-root ?id belittle) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object  ?id ?id1)
(id-root ?id1 achievement)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id apamAna_kara)) 
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  belittle.clp  belittle1  "  ?id " kA )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  belittle.clp  belittle1  "  ?id "  apamAna_kara )" crlf)) 
) 


;@@@Added by 14anu-ban-02(24-03-2015)
;Stop belittling yourself - your work is highly valued. [cald]
;तुम खुद को छोटा समझना बन्द करो- तुम्हारा कार्य अत्यन्त कीमती है . [self]
(defrule belittle2 
(declare (salience 100)) 
(id-root ?id belittle) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject  ?id ?id1)
(id-root ?id1 stop)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id CotA_samaJa))
(assert (kriyA_id-object_viBakwi ?id ko)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  belittle.clp  belittle2  "  ?id " ko )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  belittle.clp  belittle2  "  ?id "  CotA_samaJa )" crlf)) 
) 


;---------------------------default_rules--------------------------------------------------


;@@@Added by 14anu-ban-02(23-03-2015)
;Sentence: By saying this, I do not mean to belittle the importance of his role.[oald]
;Translation:एसा कहने से,मेरा उसकी  भूमिका का महत्व कम करने का  तात्पर्य नहीं है. [self]
(defrule belittle0 
(declare (salience 0)) 
(id-root ?id belittle) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kama_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  belittle.clp  belittle0  "  ?id "  kama_kara )" crlf)) 
) 
