;@@@ Added by 14anu-ban-06 (28-02-2015)
;He's a good jumper.(OALD)
;वह एक अच्छा कूदने वाला है . (manual)
(defrule jumper0
(declare (salience 0))
(id-root ?id jumper)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kUxane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jumper.clp 	jumper0   "  ?id "  kUxane_vAlA )" crlf))
)

;@@@ Added by 14anu-ban-06 (28-02-2015)
;A red woolly jumper. (cambridge)
;एक लाल ऊनी स्वेटर . (manual)
(defrule jumper1
(declare (salience 2000))
(id-root ?id jumper)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-word ?id1 woolly)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svetara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jumper.clp 	jumper1   "  ?id "  svetara )" crlf))
)
