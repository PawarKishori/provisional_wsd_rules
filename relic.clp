;$$$ Modified by 14anu-ban-06 (25-04-2015)
;@@@ Added by 14anu-ban-10 on (21-03-2015)
;The pictures on the walls were relics from the days before her marriage. [oald][parser no.-43]
;दीवारों पर चित्र उसकी शादी से पहले दिनों के यादगार  थे. [manual]
(defrule relic3
(declare (salience 400))
(id-root ?id relic)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-karma  ?id ? );commented by 14anu-ban-06 (25-04-2015)
(viSeRya-from_saMbanXI ?id ?);added by 14anu-ban-06 (25-04-2015)
(id-cat_coarse ?id noun);changed category from 'verb' to 'noun' by 14anu-ban-06 (25-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  yAxagAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relic.clp     relic3   "  ?id "   yAxagAra)" crlf))
)

;@@@ Added by 14anu-ban-10 on (21-03-2015)
;The building stands as the last remaining relic of the town's cotton industry.[oald]
;इमारत नगर के कपास उद्योग की आखिरी रही हुई स्मारक चिह्न के जैसा खडी हुई है . [manual]
(defrule relic4
(declare (salience 500))
(id-root ?id relic)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  smAraka_cihna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relic.clp     relic4   "  ?id "  smAraka_cihna)" crlf))
)

;$$$ Modified by 14anu-ban-06 (25-04-2015)
;@@@ Added by 14anu-ban-10 on (03-03-2015)
;Holy relics.[oald] [parser no- 8]
;पवित्र निशानी हैं . [manual]
;पवित्र अवशेष.(manual);added by 14anu-ban-06 (25-04-2015)
(defrule relic2
(declare (salience 600));salience increasd from '300' by 14anu-ban-06 (25-04-2015)
(id-root ?id relic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun);changed 'verb' to noun' by 14anu-ban-06 (25-04-2015)
(viSeRya-viSeRaNa  ?id ?id1);added by 14anu-ban-06 (25-04-2015)
(id-root ?id1 holy);added by 14anu-ban-06 (25-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaSeRa));meaning changed from 'niSAnI' to 'avaSeRa' by 14anu-ban-06 (25-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relic.clp     relic2   "  ?id "  avaSeRa)" crlf))
)
                                          
;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (03-03-2015)
;After the death of someone the left money or property , that is called relic ![manual]
;वह धन या संपत्ति जो किसी के मरने के उपरांत बची हो, अवशेष कहलाते है ! [hinkhoj]
(defrule relic0
(declare (salience 100))
(id-root ?id relic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaSeRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relic.clp     relic0   "  ?id "  avaSeRa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (03-03-2015)
;Our transportation system is a relic of the past.[oald]
;हमारी परिवहन प्रणाली भूतकाल का  चिह्न है . [manual]
(defrule relic1
(declare (salience 200))
(id-root ?id relic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cinha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relic.clp     relic1   "  ?id "  cinha)" crlf))
)

