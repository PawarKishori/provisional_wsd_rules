
;@@@ Added by 14anu-ban-11 on (24-03-2015)
;Champagne glasses usually have long stems.(oald)
;शैंपेन काँच आम तौर पर लम्बी नली की होती है . (self)
(defrule stem2
(declare (salience 5001))
(id-root ?id stem)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 glass)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stem.clp 	stem2   "  ?id "  nalI)" crlf))
)


;@@@ Added by 14anu-ban-11 on (24-03-2015)
;Ignorance is the stem of racial discrimination.(oald)
;अज्ञानता नस्लीय भेदभाव की जड है . (self)
(defrule stem3
(declare (salience 5002))
(id-root ?id stem)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 discrimination)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stem.clp 	stem3   "  ?id "  jadZa)" crlf))
)


(defrule stem0
(declare (salience 5000))
(id-root ?id stem)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stem.clp 	stem0   "  ?id "  wanA )" crlf))
)

(defrule stem1
(declare (salience 4900))
(id-root ?id stem)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stem.clp 	stem1   "  ?id "  roka )" crlf))
)

;"stem","V","1.rokanA"
;Doctor bandaged the cut to stem the bleeding.
;--"2.jadZa"  
;Ignorance is the stem of racial discrimination.
;
;
