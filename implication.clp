;@@@ Added by 14anu-ban-06 (01-04-2015)
;He resigned after his implication in a sex scandal. (OALD)
;उसने एक लिंग अनैतिक आचरण में उसकी सहभागिता के बाद त्याग पत्र दिया .  (manual)
(defrule implication1
(declare (salience 2000))
(id-root ?id implication)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-after_saMbanXI ?id1 ?id)
(kriyA-in_saMbanXI ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahaBAgiwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implication.clp 	implication1   "  ?id "  sahaBAgiwA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (01-04-2015)
;From what she said, the implication was that they were splitting up. (cambridge)
;उसने जो कहा से विवक्षा थी कि वे अलग हो रहे थे . (manual)
(defrule implication0
(declare (salience 0))
(id-root ?id implication)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implication.clp 	implication0   "  ?id "  vivakRA )" crlf))
)
