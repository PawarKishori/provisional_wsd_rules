;@@@Added by 14anu-ban-02(20-04-2015)
;Spent ten years in the bushes.[mw]
;दस वर्ष जङ्गलों में  बिताइए . [self]
(defrule bush1
(declare (salience 100))
(id-root ?id bush)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jafgala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bush.clp 	bush1   "  ?id "  jafgala )" crlf))
)
;--------------------------default_rules---------------------------------
;@@@Added by 14anu-ban-02(20-04-2015)
;A rose bush.[oald]
;गुलाब की झाड़ी.[self]  
(defrule bush0
(declare (salience 0))
(id-root ?id bush)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JAdZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bush.clp 	bush0   "  ?id "  JAdZI )" crlf))
)
