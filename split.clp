;@@@ Added by 14anu-ban-01 on (24-09-2014)
;Thus the wire passes through the slab and the slab does not split.  [NCERT Corpus]
;इस प्रकार तार हिम शिला से पार हो जाता है तथा शिला विभक्त नहीं होती.[NCERT corpus]
(defrule split2
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 slab)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBakwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split2   "  ?id "  viBakwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-09-2014)
;The commander told the soldiers to split into three groups.[split.clp]
;सेनाध्यक्ष ने सैनिकों को तीन टुकडियों/समूहों  में विभजित होने का आदेश दिया./को कहा.[self]
(defrule split3
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 group)
(not(kriyA-object ?id ?))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBAjiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split3   "  ?id "  viBAjiwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-01 on (25-09-2014)
;She split the class into groups of four.[oald]
;उसने कक्षा को चार समूहों में विभाजित कर दिया.[self]
(defrule split4
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBAjiwa_kara))
(assert (id-wsd_viBakwi ?id1 ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp     split4 "  ?id1 " ko)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split4   "  ?id "  viBAjiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-09-2014)
;Slate splits easily into thin sheets.[oald]
;स्लेट आसानी से पतली चादरों में विभाजित हो जाता है.[self]
;Thus the wire passes through the slab and the slab does not split.  [NCERT Corpus]
;इस प्रकार तार हिम शिला से पार हो जाता है तथा शिला विभाजित नहीं होती.[NCERT corpus]
(defrule split5
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(not(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBAjiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split5  "  ?id "  viBAjiwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-09-2014)
;She split the prize money with her brother.[self:with reference to oald]
;उसने पुरस्कार राशि अपने भाई के साथ बाँट ली.[self]
;We live in the same house and split all the bills.[self:with reference to oald]
;हम एक ही घर में रहते हैं ओर सभी/सारे बिल बाँट लेते हैं.[self]
(defrule split6
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?)
(kriyA-object ?id ?id1)
(id-root ?id1 money|bill)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzta_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split6 "  ?id "  bAzta_le )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-09-2014)
;His time is split between the London and Paris offices.[oald]
;उसका समय लन्दन और पेरिस वाले दफ्तरों के बीच बँटा हुआ है .[self] 
(defrule split7
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 time)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baztA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split7  "  ?id "  baztA_ho )" crlf))
)

;@@@ Added by 14anu-ban-01 on (26-09-2014)
;The results split neatly into two groups. [oald]
; परिणाम सफाई से दो भागों में बँट गये[self]
;The committee split over government subsidies.[oald]
;समिति सरकारी अनुदान के बारे में/को लेकर  दो समूहों में बँट गयी.[self]
(defrule split8
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 result|committee)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bazta_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split8  "  ?id "  bazta_jA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (27-09-2014)
;Her dress had split again.[self:with reference to oald]
;उसकी पोशाक फिर से फट गयी .[self]
(defrule split9
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 dress|cloth|pant)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split9  "  ?id "  Pata_jA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (27-09-2014)
;The cushion split open and sent feathers everywhere.[oald]
;तकिया फट के खुल गया और चारों तरफ पङ्ख फैल गये.[self]
(defrule split10
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) open)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) Pata_ke_Kula_jA))	;removed '=' by 4anu-ban-01 on (07-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " split.clp split10	"  ?id "  "(+ ?id 1) "  Pata_ke_Kula_jA  )" crlf))						;removed '=' by 4anu-ban-01 on (07-03-2015)
)

;@@@ Added by 14anu-ban-01 on (27-09-2014)
;How did you split your lip?[oald]
;आपने अपना होंठ कैसे काट लिया?[self]
(defrule split11
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 lip|hand|finger)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split11  "  ?id "  kAta_le)" crlf))
)


;@@@ Added by 14anu-ban-01 on (27-09-2014)
;The singer split with his wife last month.[self:with reference to oald]
;गायक पिछले महीने अपनी पत्नी से अलग हो गया.[self]
(defrule split12
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split12 "  ?id "  alaga_ho_jA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (27-09-2014)
;She intends to split from the band at the end of the tour.[oald]
;वह यात्रा के अन्त में बैण्ड से अलग होना चाहती है.[self]
(defrule split13
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split13 "  ?id "  alaga_ho)" crlf))
)

;@@@ Added by 14anu-ban-01 on (04-10-2014)
;For a split second,I felt like I am in heaven.[self:with reference to COCA]
;एक पल के लिए मुझे लगा जैसे मैं स्वर्ग में हूँ.[self]
(defrule split14
(declare (salience 4900))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 second)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " split.clp	 split14  "  ?id "  " ?id1  " pala  )" crlf))
)

;@@@ Added by 14anu07
(defrule split15
(declare (salience 5000))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) alaga_ho))
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " split.clp	split15  "  ?id "  " (+ ?id 1) " alaga_ho)" crlf)
)

;$$$ Modified by 14anu-ban-01 on (15-01-2015)
;@@@ Added by 14anu07
(defrule split16
(declare (salience 0))	;salience reduced by 14anu-ban-01 from 5000 to 0 because it is a default rule
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) into)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bazta))	;corrected "baMta" to "bazta" by 14anu-ban-01 on (15-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split16   "  ?id "  bazta )" crlf))	;corrected "baMta" to "bazta" by 14anu-ban-01 on (15-01-2015)
)

;------------------------- Default rules ------------------------


;@@@ Added by 14anu-ban-01 on (07-03-2015)
;I stripped off bark to use for kindling before laying on the split lengths of cedar. [oald]--Parse no.3
;मैंने देवदार की काटी हुई लकड़ियों पर रखने से पहले आग जलाने में प्रयोग करने के लिए वृक्ष की छाल को छील लिया .[self] 
(defrule split00
(declare (salience 0))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAtA_huA/viBAjiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split00   "  ?id " kAtA_huA/viBAjiwa )" crlf))
)

(defrule split0
(declare (salience 0));reduced to 0 from 5000 by 14anu-ban-01 on (04-10-14)
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split0   "  ?id "  kAta )" crlf))
)

;"split","N","1.kAta"
;Sew up a split in a seam.
;
(defrule split1
(declare (salience 4500))
(id-root ?id split)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZAdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  split.clp 	split1   "  ?id "  PZAdZa )" crlf))
)

;"split","V","1.PZAdZanA/cIranA"
;Why did you split my book into two.
;--"2.viBAjiwa_ho"
;The commander told the soldiers to split into three groups.
;
