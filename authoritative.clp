;@@@Added by 14anu-ban-02(07-03-2015)
;She addressed the group with an authoritative voice.[mw]
;उसने सुदृढ़ आवाज के साथ समूह सम्बोधित किया . [self]
(defrule authoritative1 
(declare (salience 100)) 
(id-root ?id authoritative) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 voice|sound)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id suxqDZa))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  authoritative.clp  authoritative1  "  ?id "  suxqDZa )" crlf)) 
) 
;@@@Added by 14anu-ban-02(07-03-2015)
;The most authoritative book on the subject.[oald]
;विषय पर सबसे अधिक प्रमाणित पुस्तक . [self]
(defrule authoritative2 
(declare (salience 100)) 
(id-root ?id authoritative) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 book)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pramANiwa))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  authoritative.clp  authoritative2  "  ?id "  pramANiwa )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(07-03-2015)
;Sentence: She has an authoritative manner that at times is almost arrogant.[cambridge]
;Translation: उसका तरीका आधिकारिक  है जो लगभग कभी कभी  अहङ्कारी लगता है .  [self]
(defrule authoritative0
(declare (salience 0))
(id-root ?id authoritative)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AXikArika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  authoritative.clp  authoritative0  "  ?id "  AXikArika )" crlf))
)

