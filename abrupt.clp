;$$$Modified by 14anu-ban-02(22-04-2015)  ;meaning changed from 'Akasmika' to 'beaxaba' by 14anu-ban-02(22-04-2015)
;She was very abrupt with me in our meeting.[oald]
;वह हमारे सम्मेलन में मेरे साथ अत्यन्त बेअदब थी . [self]
(defrule abrupt0
(declare (salience 100))
(id-root ?id abrupt)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 pronoun|propN)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id beaxaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abrupt.clp 	abrupt0   "  ?id "  beaxaba )" crlf))
)

;@@@Added by 14anu-ban-02(22-04-2015)
;An abrupt manner.[oald]
;रूखा तरीका . [self]
(defrule abrupt2
(declare (salience 100))
(id-root ?id abrupt)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 manner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abrupt.clp 	abrupt2   "  ?id "  rUKA )" crlf))
)

;------------------------default_rules-------------------------------------------

(defrule abrupt1
(declare (salience 0))	;salience reduce to 0 from 4900 by 14anu-ban-02(22-04-2015)
(id-root ?id abrupt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akasmika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abrupt.clp 	abrupt1   "  ?id "  Akasmika )" crlf))
)

;"abrupt","Adj","1.Akasmika"
;His speech had an abrupt ending.
;--"2.rUKA"
;Everybody dislikes his abrupt manners.
;--"3.asambaxXa"
;He has an abrupt style of writing.
;
;
;LEVEL 
;
;
;"abrupt","Adj","1.Akasmika"
;His speech had an abrupt ending.
;usake BARaNa kA anwa akasmAw huA. 
;
;--"2.rUKA"-KuSka- vyavahAra meM Akasmika baxalAva
;Everybody dislikes his abrupt manners.
;saBI loga usake Akasmika vyavahAra ko nApasanxa karawe hEM.
;
;--"3.asambaxXa"-acAnaka eka avasWA se xUsarI meM jAnA-Akasmika parivarwana
;He has an abrupt style of writing.
;usakI eka asambaxXa liKAI kI SElI hE.
;
;  nota:- uparyukwa aMkiwa 'abrupt'Sabxa ke liye viSeRaNa ke wInoM vAkyoM kA arWa  
;        eka hI arWa 'akasmAwa'Sabxa se nikAlA jA sakawA hE awaH isakA sUwra
;        nimna prakAra liKa sakawe hEM
;
;         sUwra : Akasmika`
;       
;
