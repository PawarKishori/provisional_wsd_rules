;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 27/6/2014****
;He upgraded his software.
;उसने अपना सॉफ्टवेयर अपग्रेड किया. 
(defrule upgrade2
(declare (salience 5000))
(id-root ?id upgrade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 software|hardware|computer|system|mobile|android|windows|phone|pc|desktop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apagreda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  upgrade.clp 	upgrade2   "  ?id "  apagreda )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabad on 27/6/2014****
;A new software upgrade is available.
;एक नया सॉफ्टवेयर अपग्रेड उपलब्ध है .
;A software upgrade is available for this device.
;एक सॉफ्टवेयर अपग्रेड इस उपकरण के लिए उपलब्ध है. 
(defrule upgrade3
(declare (salience 5100)) ;salience increased from 5000 by 14anu-ban-07 (21-01-2015)
(id-root ?id upgrade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb|noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 software|hardware|computer|system|mobile|android|windows|phone|pc|desktop)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apagreda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  upgrade.clp 	upgrade3   "  ?id "  apagreda )" crlf))
)

;------------------------------- Default rules---------------------------
(defrule upgrade0
(declare (salience 5000))
(id-root ?id upgrade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caDZAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  upgrade.clp 	upgrade0   "  ?id "  caDZAI )" crlf))
)

;"upgrade","N","1.caDZAI"
;There has been a major upgrade in defence technology.
;
(defrule upgrade1
(declare (salience 4900))
(id-root ?id upgrade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id unnawi_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  upgrade.clp 	upgrade1   "  ?id "  unnawi_kara )" crlf))
)

;"upgrade","VT","1.unnawi_karanA"
;We should upgrade the defence technology.
;
