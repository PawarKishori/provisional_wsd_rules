;@@@ Added by 14anu13  on (18-06-14)
;He watched her suffering with clinical detachment.
;उसने भावावेग बिना उसकी पीडा देखी |
(defrule clinical0
(declare (salience -1)) 
(id-root ?id clinical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAvAvega_binA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clinical.clp 	clinical0   "  ?id "  BAvAvega_binA )" crlf))
)


;@@@ Added by 14anu13 (18-06-14)
;Everything in the nursery was white and clinical .
;पौधघर मे हर चीज सफेद व असज्जित थी |

(defrule clinical1
(id-root ?id clinical)
(id-root ?id1 white|black|red|unmaneged|wierd|green)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(conjunction-components ? ?id1 ?id )
;(id-root ?id1 white|black|red|unmaneged|wierd|green)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asajjiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clinical.clp 	clinical1   "  ?id "  asajjiwa )" crlf))
)

;@@@ Added by 14anu13 (18-06-14)
;He is persuing a clinical research.
;वह एक चिकित्सकीय शोध कर रहा है |
(defrule clinical2
(id-root ?id clinical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 research|training|trails)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cikiwsakIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clinical.clp 	clinical2   "  ?id "  cikiwsakIya )" crlf))
)
