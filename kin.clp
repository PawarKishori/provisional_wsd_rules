;########################################################################
;#  Copyright (C) 2014-2015 14anu26 (noopur.nigam92@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################
;@@@ Addded by 14anu26  [05-06-14]
;Extended family and kin are an important part of the social structure of the republic. 
;विस्तृत परिवार और रिश्तेदार गणतन्त्र की सामाजिक संरचना का महत्त्वपूर्ण भाग हैं .
(defrule kin0
(declare (salience 0))
(id-root ?id kin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id riSwexAra ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kin.clp 	kin0   "  ?id "  riSwexAra  )" crlf))
)

;@@@ Addded by 14anu26  [05-06-14]
;He was kin to the brothers. 
;वह भाइयों का सम्बन्धी था.
(defrule kin1
(declare (salience 100))
(id-root ?id kin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambanXI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kin.clp 	kin1   "  ?id "  sambanXI  )" crlf))
)

;@@@ Addded by 14anu26  [05-06-14]
;dolphins, whales, and their kin.
;डॉल्फिन, व्हेल और उनकी जाति .
(defrule kin2
(declare (salience 200))
(id-root ?id kin)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) their)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kin.clp 	kin2   "  ?id "  jAwi )" crlf))
)

