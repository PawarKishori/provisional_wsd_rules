;@@@ Added by 14anu-ban-06 (11-04-2015)
;I wrote a song which they’re thinking of using as a jingle.(OALD)
;मैंने एक ऐसा गाना लिखा जिसे वे छोटे मधुर गाने के जैसे उपयोग करने के बारे में सोच रहे हैं . (manual)
(defrule jingle2
(declare (salience 5100))
(id-root ?id jingle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-as_saMbanXI ?id1 ?id)
(id-root ?id1 use)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA_maXura_gAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jingle.clp 	jingle2   "  ?id "  CotA_maXura_gAnA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx
(defrule jingle0
(declare (salience 5000))
(id-root ?id jingle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JanaJanAhata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jingle.clp 	jingle0   "  ?id "  JanaJanAhata )" crlf))
)

;"jingle","V","1.KanaKanAnA"
;He jingled his bunch of keys.
(defrule jingle1
(declare (salience 4900))
(id-root ?id jingle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KanaKanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jingle.clp 	jingle1   "  ?id "  KanaKanA )" crlf))
)

