
(defrule fix0
(declare (salience 5000))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id fixed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWira_kI_huI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fix.clp  	fix0   "  ?id "  sWira_kI_huI )" crlf))
)

;"fixed","Adj","1.sWira_kI_huI"
;In this shop prices of all items are fixed.
;--"2.xqDa_kiyA_huA"
;He has fixed ideas about his son.
;--"3.sWApiwa_kiyA_huA"
;He has a fixed smile on his face.
;
;
(defrule fix1
(declare (salience 4900))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 waya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fix.clp	fix1  "  ?id "  " ?id1 "  waya_kara  )" crlf))
)

;Have you fixed on a date for your party?
;kyA wumane apanI pArtI kI wArIKZa waya kara lI hE?


(defrule fix3
(declare (salience 4800))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSciwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix3   "  ?id "  niSciwa_kara )" crlf))
)

;@@@ Added by 14anu09
;Fix a shelf to the wall.
;दीवार को शेल्फ स्थापित कीजिए . 
(defrule fix06
(declare (salience 4900))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(AjFArWaka_kriyA ?id)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWApiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix06   "  ?id "  sWApiwa_kara )" crlf))
)

;@@@ Added by 14anu09[18-6-14]
;He noted every detail so as to fix the scene in his mind.
;उसने जिससे उसके मन में दृश्य स्थापित करने के लिए प्रत्येक ब्यौरा ध्यान से देखा . 
(defrule fix07
(declare (salience 4900))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 scene)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWApiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix07   "  ?id "  sWApiwa_kara )" crlf))
)

;@@@ Added by 14anu09 and kokila eflu
;I will fix a meeting.
;मैं बैठक बुलाऊंगा. 
(defrule fix08
(declare (salience 4900))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-word ?id1 meeting)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bulA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix08   "  ?id "  bulA )" crlf))
)

;Added by Meena (27.8.09)
;Dick is important to fix the problem.
(defrule fix4
(declare (salience 4800))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 problem)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sulaJA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix4   "  ?id "  sulaJA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  fix.clp       fix4   "  ?id " ko )" crlf)
)
)

;@@@ Added by 14anu-ban-05 on (29-09-2014)
;The rotation may be about an axis that is fixed (e.g. a ceiling fan) or moving (e.g. an oscillating table fan). [NCERT]
;GUrNana kisI EsI akRa ke pariwaH ho sakawA hE jo sWira ho (jEse Cawa ke pafKe meM) yA Pira eka EsI akRa ke pariwaH jo svayaM GUmawI ho (jEse iXara se uXara GUmawe meja ke pafKe meM).
;The motion of a rigid body which is pivoted or fixed in some way is rotation.[NCERT] 
;eka Ese xqDa piNda kI gawi jo yA wo cUla para tikA ho yA kisI na kisI rUpa meM sWira ho, GUrNI gawi howI hE.
;To be tested
(defrule fix6
(declare (salience 5000))
(Domain physics)
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 that|which)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id sWira_ho))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix6   "  ?id "  sWira_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fix.clp       fix6   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (29-09-2014)
;The line along which the body is fixed is termed as its axis of rotation.[NCERT]
;vaha sarala reKA jisake anuxiSa isa xqDa piNda ko sWira banAyA gayA hE isakI GUrNana - akRa kahalAwI hE.
;Thus, in more general cases of rotation, such as the rotation of a top or a pedestal fan, one point and not one line, of the rigid body is fixed.[NCERT]
;GUrNana gawi ke aXika sArvika mAmaloM meM, jEse ki lattU yA pITikA - pafKe ke GUmane meM, xqDa piNda kA eka binxu sWira rahawA hE, na ki eka reKA.
;While the fan rotates and its axis moves sidewise, this point is fixed.[NCERT]
;jaba pafKA GUmawA hE Ora isakI akRa iXara se uXara xolana karawI hE waba BI yaha binxu sWira rahawA hE. 
;To be tested
(defrule fix7
(declare (salience 5000))
(Domain physics)
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 body|line|point|axis)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id sWira_raha))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix7   "  ?id "  sWira_raha )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fix.clp       fix7   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (29-09-2014)
;The most common way to constrain a rigid body so that it does not have translational motion is to fix it along a straight line.[NCERT]
;kisI xqDa piNda kI sWAnAnwaraNa gawi ko niruxXa karane kI sarva sAmAnya viXi yaha hE ki use eka sarala reKA ke anuxiSa sWira kara xiyA jAe.
;To be tested
(defrule fix8
(declare (salience 5000))
(Domain physics)
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 it)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id sWira_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix8   "  ?id "  sWira_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fix.clp       fix8   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (29-09-2014)
;You may notice that in rotation of a rigid body about a fixed axis, every particle of the body moves in a circle, which lies in a plane perpendicular to the axis and has its center on the axis.[NCERT] 
;Apa xeKa sakawe hEM ki eka xqDa piNda ke eka sWira akRa ke pariwaH GUrNana meM, piNda kA hara kaNa eka vqwwa meM GUmawA hE ; yaha vqwwa akRa ke lambavaw wala meM hE Ora inakA kenxra akRa para avasWiwa hE.
;In this case the axis is not fixed, though it always passes through the fixed point.[NCERT]
;isa mAmale meM akRa wo sWira nahIM hE para yaha hameSA eka sWira binxu se gujarawI hE.
;To be tested
(defrule fix9
(declare (salience 5000))
(Domain physics)
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 axis|point|line)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id sWira))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix9   "  ?id "  sWira )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fix.clp       fix9   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-11 on (21-10-2014)
;The line along which the body is fixed is termed as its axis of rotation.(Ncert)
;वह सरल रेखा जिसके अनुदिश इस दृढ पिंड को स्थिर बनाया गया है इसकी घूर्णन-अक्ष कहलाती है.(Ncert)
(defrule fix10
(declare (salience 5000))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1) 
(id-root ?id1 axis|point|line|body)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWira))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix10   "  ?id "  sWira )" crlf)
)
)

;@@@ Added by 14anu-ban-05 on (20-11-2014)
;Over time, the nutrients fixed by soil microbes and humus are released and available to crops.[agriculture]
;समय के साथ,पोषक तत्वों जो नियत किया है मिट्टी के  सूक्ष्म कीटाणुओं द्वारा और  मिट्टी से छोड़ा हुआ खाद फसलों के लिए उपलब्ध है[MANUAL]
(defrule fix11
(declare (salience 5000))
(Domain agriculture)
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 microbe)
=>
(retract ?mng)
(assert (id-domain_type  ?id agriculture))
(assert (id-wsd_root_mng ?id niyawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix11   "  ?id "  niyawa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fix.clp      fix11   "  ?id "  agriculture )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (27-01-2015)
;@@@ Added by 14anu13 on 23-06-14
;He noted every detail so as to fix the scene in his mind.
;उसने दृश्य अपने दिमाग में याद करने के लिये हर विस्तार पर ध्यान दिया |
(defrule fix12
(declare (salience 4901)) ; increased salience from 4800 to 4901 by 14anu-ban-05
(id-root ?id fix)
(id-root ?id1 scene)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(viSeRya-in_saMbanXI  ?id1 ?id2)	;added by 14anu-ban-05 on (27-01-2015)
(id-root ?id2 mind)			;more constraints can be added ;added by 14anu-ban-05 on (27-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix12   "  ?id "  yAxa_kara )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (27-01-2015)
;same example for rules fix08 and fix13
;I'll fix a ceremony.
;मैं एक समारोह आयोजित करूँगा.
;@@@ Added by 14anu13 on 23-06-14
;I'll fix a meeting.	
;मैं एक सभा आयोजित करूँगा |
(defrule fix13
(declare (salience 4800))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-word ?id1 meeting|ceremony|wedding|funeral|engagement)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ayojiwa_kara))	;modified from aAyojiwa_kara to Ayojiwa_kara by 14anu-ban-05 on (27-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix13   "  ?id "  Ayojiwa_kara )" crlf))
)			;modified from aAyojiwa_kara to Ayojiwa_kara by 14anu-ban-05 on (27-01-2015)

;@@@ Added by 14anu13 on 23-06-14
;Can I fix you a drink?
;क्या मै अापके लिये एक पेय बना सकता हू़ँ ?
(defrule fix14
(declare (salience 4800))
(id-root ?id fix)
(id-word ?id1 drink|supper|dinner|lunch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id  ?id2)
(object-object_samAnAXikaraNa  ?id2  ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix14   "  ?id "  banA )" crlf))
)

;@@@ Added by 14anu13 on 23-06-14
;Can I fix  a drink for you ?
;क्या मै अापके लिये एक पेय बना सकता हू़ँ ?
(defrule fix15
(declare (salience 4800))
(id-root ?id fix)
(id-word ?id1 drink|supper|dinner|lunch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id  ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix15   "  ?id "  banA )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (12-12-2014)
;added proper sentence for rule
;They couldn't fix my old computer, so I bought a new one.[cambridge dictionary]
;वे मेरे पुराने कंप्यूटर को ठीक नहीं कर सकता, इसलिए मैंने नया खरीदा है।[Manual]
;@@@ Added by 14anu13 on 23-06-14
;I'll fix a meeting.
;मैं एक सभा आयोजित करूँगा |
(defrule fix16
(declare (salience 4800))
(id-root ?id fix)
(id-word ?id1 hair|laptop|machine|mobile|equipment|computer) ;added computer by 14anu-ban-05 on (12-12-2014)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix16   "  ?id "  TIka_karA )" crlf))
)


;-------------------------- Default rules ----------------
;He then [varied] the charges in pairs, keeping the distance [fixed] for each pair.   [physics]
;तत्पश्चात उन्होंने प्रत्येक युगल के लिए  निश्चित की हुई दूरी रखकर युगलों में आवेशों में परिवर्तन किया .
(defrule fix2
(declare (salience 4700))	;decreased salience from 4800 to 4700 by 14anu-ban-05 on (12-12-2014)
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niSciwa_kara))  ;meaning modified by Pramila(Banasthali University) on 23-10-2013
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix2   "  ?id "  niSciwa_kara )" crlf))
)


(defrule fix5
(declare (salience 4500))
(id-root ?id fix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTinAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fix.clp 	fix5   "  ?id "  kaTinAI )" crlf))
)

;"fix","N","1.kaTinAI"
;By agreeing to his proposal, she has put herself in a great fix.
;--"2.beimAnI"
;The culprit got away because this case was a fix.
;--"3.naSIlI_xavAI"
;A person addicted to drugs needs frequent fixes to remain normal.
;--"4.sWApanA"
;Please fix the co-ordinates of our position by survey.
;
;
;LEVEL 
;Headword : fix
;
;Examples --
;
;"fix","V","1.lagAnA"
;He has come to fix a pole
;vaha KaMBA lagAne AyA hE.
;--"2.niSciwa_karanA"<--samaya se mItiMga ko jodZanA
;Please fix the time for our meeting
;kqpayA bETaka ke samaya ko niSciwa kara xIjiye.
;--"3.wEyAra_karanA"  <-- TIka karanA
;You fix up a meal for the guests tonight
;Aja rAwa awiWiyoM ke liye Bojana kA wEyAra kara xIjiye.
;--"4.TIka_karanA"  <--vApasa jodZanA
;Can you fix this machine?
;kyA wuma isa maSIna ko TIka kara sakawe ho?
;--"5.kisI_para xqRti jamAnA"<-- xqRti ko vyakwi se jodZa xenA
;Her eyes are fixed on the picture
;usakI xqRti ciwra para jamI hE
;
;"fix","N","ulaJana"
;He was in a fix.
;vaha ulaJana meM WA. <--asamaMjasa kI sWiwi meM PazsanA <--eka sWiwi se judZa jAnA
;
;vyAKyA - uparyukwa kriyA vAkyoM ke anusAra `jodZanA' niRkarRa nikalawA hE. kyoMki jodZane kA arWa yahAz kuCa PElA huA hE awaH isake sAWa '`' lagAnA uciwa hogA.
;'TIka karanA' (u.4.) BI yaxyapi 'jodZane' se sambanXiwa hE Pira BI AsAnI ke liye ise BIsUwra meM raKA jA sakawA hE. saMjFAprayoga 'ulaJana' BI eka xqRti se 'judZane' kA hI BAva aBIvyakwa kara rahA hE. awaH isakA sUwra hogA -
;
;sUwra : jodZanA`^TIka_karanA
;
