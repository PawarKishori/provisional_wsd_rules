
(defrule dye0
(declare (salience 5000))
(id-root ?id dye)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dye.clp 	dye0   "  ?id "  raMga )" crlf))
)

;"dye","N","1.raMga"
;They use vegetable dyes for coloring the garments.
;
(defrule dye1
(declare (salience 4900))
(id-root ?id dye)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dye.clp 	dye1   "  ?id "  raMga )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-09-2014)
;Until recently the lac dye was used for cloth dyeing in our country .    [karan-singla]
;हाल मेँ तक हमारे देश में कपडे  रङ्गने के लिए लाह का रङ्ग  उपयोग किया गया था . 
(defrule dye2
(declare (salience 5010))
(id-root ?id dye)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 cloth) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMgane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dye.clp 	dye2  "  ?id "  raMgane )" crlf))
)
;@@@ Added by 14anu-ban-04 (10-09-2014)
;Kalamkari , the art of dyeing with vegetable oils and colors , still flourishes and silk sarees woven in the state rank among the most beautiful in the world .  [karan-singla]
;कलंकारी , वनस्पति तेल तथा रङ्गों से रङ्गने  की कला , आज भी फल्फूल रही है और राज्य में बुनी गयीं रेशम की साडियों को संसार की अत्यंत सुन्दर साडियो में स्थान प्राप्त है .
(defrule dye3
(declare (salience 5020))
(id-root ?id dye)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 art)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMgane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dye.clp 	dye3  "  ?id "  raMgane )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-09-2014)
;The pre - war handicap of lack of facilities for bleaching and dyeing was totally removed and mercerising and calico printing were introduced on a wide scale .    [karan-singla]
;ब़्लीचिंग और रङ्गने  के लिए सुविधाओं की कमी की युद्ध पूर्व की कठिनाई अब बिल़्कुल समाप़्त कर दी गयी थी और मर्सराइजिंग तथा कैलिको छपाई का काम व़्यापक स़्तर पा शुरू किया गया .
(defrule dye4
(declare (salience 5030))
(id-root ?id dye)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMgane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dye.clp 	dye4  "  ?id "  raMgane )" crlf))
)

;"dye","V","1.raMganA"
;Clothes will be dyed to look attractive
;
