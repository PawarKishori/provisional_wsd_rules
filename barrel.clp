;@@@ Added by 14anu21 on 24.06.2014
;Rama found the barrel of a broken gun.
;राम को एक टूटी हुई बन्दूक का पीपा मिला.(Translation before adding rule)
;राम को एक टूटी हुई बन्दूक की नली मिली . 
;
(defrule barrel0
(declare (salience 100))
(id-root ?id barrel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(and(viSeRya-of_saMbanXI  ?id ?id1)(id-root ?id1 gun|pistol|riffle))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nalI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  barrel.clp    barrel0   "  ?id "  nalI )" crlf)
)
)
;@@@ Added by 14anu21 on 24.06.2014
;The barrel was aimed directly at me.
;पीपा मुझपर प्रत्यक्ष-रूप से लक्षित किया गया था .(Translation before adding rule)
;बनदूक की नली मुझपर प्रत्यक्ष-रूप से लक्षित किया गया था . 
(defrule barrel1
(declare (salience 100))
(id-root ?id barrel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(and(or(kriyA-subject  ?idverb ?id)(kriyA-object ?idverb ?id))(id-root ?idverb aim))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxUka_kI_nalI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  barrel.clp    barrel1   "  ?id "  banxUka_kI_nalI )" crlf)
)
)
;---------------------- Default rule -----------------
;@@@Added by 14anu-ban-02(12-01-2015)
;Oil prices fell to $9 a barrel.           (oald)
;तेल की कीमते ९ डॉलर प्रति पीपे तक गिरी हैं.             (manual)
(defrule barrel2
(declare (salience 0))
(id-root ?id barrel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIpA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  barrel.clp    barrel2   "  ?id "  pIpA )" crlf)
)
)
