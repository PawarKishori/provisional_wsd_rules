
;@@@ Added by 14anu-ban-09 on (22-11-2014)
;Some people must undergo a ritual purification after certain activities. [Free dictionary]
;कुछ लोगों को कुछ क्रिया कलाप के बाद विधि संबंधी स्वच्छीकरण अनुभव करना चाहिए . [Self]

(defrule purification0
(id-root ?id purification)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svacCIkaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  purification.clp 	purification0   "  ?id "  svacCIkaraNa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (22-11-2014)
;This has been achieved by purification and special preparation of materials such as quartz. [NCERT CORPUS]
;ise kvAtrja jEse paxArWoM ke SoXana waWA viSiRta viracana xvArA banAyA jAwA hE. [NCERT CORPUS]

(defrule purification1
(declare (salience 1000))
(id-root ?id purification)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 material)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SoXana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  purification.clp 	purification1   "  ?id "  SoXana )" crlf))
)

