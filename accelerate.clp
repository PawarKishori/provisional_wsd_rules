;@@@Added by 14anu-ban-02(04-02-2015)
;An object released near the surface of the Earth is accelerated downward under the influence of the force of gravity.[ncert]
;यदि धरती की सतह से थोडी ऊँचाई पर से कोई वस्तु छोड दी जाए तो वह पृथ्वी की ओर गुरुत्व बल के कारण त्वरित होगी.[ncert]
(defrule accelerate0
(declare (salience 100))
(id-root ?id  accelerate)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(kriyA-under_saMbanXI  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 force)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wvariwa_ho))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "   accelerate.clp       accelerate0   "  ?id "  physics )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   accelerate.clp 	 accelerate0   "  ?id "  wvariwa_ho)" crlf))
)

;;@@@Added by 14anu-ban-02(04-02-2015)
;;If the start is not too sudden, i.e. if the acceleration is moderate, the frictional force would be enough to accelerate our feet along ;with the bus.[ncert]
;;यदि बस की पिक-अप अति आकस्मिक नहीं है, अर्थात् त्वरण साधारण है तो घर्षण बल हमारे पैरों को बस के साथ त्वरित करने के लिए पर्याप्त होगा.[ncert]
;(defrule accelerate2
;(declare (salience 200))
;(id-root ?id  accelerate)
;?mng <-(meaning_to_be_decided ?id)
;(Domain physics)
;(to-infinitive  ? ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id wvariwa_karanA))
;(assert (id-domain_type  ?id physics))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "   accelerate.clp       accelerate2   "  ?id "  physics )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   accelerate.clp 	 accelerate2   "  ?id "  wvariwa_karanA)" crlf))
;)

;------------------------------------------------------Default_rules-------------------------------------------------------------------------
;@@@Added by 14anu-ban-02(04-02-2015)
;We shall see how particles can be accelerated to very high energies in a cyclotron.[NCERT 12_04]
;हम यह देखेंगे कि साइक्लोट्रॉन में किस प्रकार कणों को अति उच्च ऊर्जाओं तक त्वरित किया जा सकता है.[ncert]
(defrule accelerate1
(declare (salience 0))
(id-root ?id  accelerate)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wvariwa_kara))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "   accelerate.clp       accelerate1   "  ?id "  physics )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   accelerate.clp 	 accelerate1   "  ?id "  wvariwa_kara)" crlf))
)
