;@@@ Added by Anita-- 24.7.2014
;He pulled a thick wad of £10 notes out of his pocket. [oxford learner's dictionary]
;उसने अपनी जेब से १० पॉउन्ड की एक मोटी गड्डी निकाली ।
(defrule wad1
(declare (salience 5000))
(id-root ?id wad)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaddI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wad.clp 	wad1   "  ?id "  gaddI )" crlf))
)

;@@@ Added by Anita-- 24.7.2014
;They had a wad of money. [oxford learner's dictionary]
;उनके पास ढेर सारा पैसा है ।
(defrule wad2
(declare (salience 4800))
(id-root ?id wad)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?sam)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dera_sArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wad.clp 	wad2   "  ?id "  Dera_sArA )" crlf))
)

;@@@ Added by Anita-- 25.7.2014
;The nurse used a wad of cotton wool to stop the bleeding. [oxford learner's dictionary]
;परिचारिका ने खून को रोकने के लिए रुई के फाहे का उपयोग किया ।
(defrule wad3
(declare (salience 5100))
(id-root ?id wad)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 wool)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PAhA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wad.clp 	wad3   "  ?id "  PAhA )" crlf))
)

;############################default-rule#####################################
(defrule wad_noun_default
(declare (salience 0)) ;reduced salience by Anita--24.7.2014
(id-root ?id wad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaxxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wad.clp 	wad_noun_default   "  ?id "  gaxxI )" crlf))
)

;"wad","N","1.gaxxI"
;Sit on the wad
;
(defrule wad_verb
(declare (salience 4900))
(id-root ?id wad)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAta_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wad.clp 	wad_verb   "  ?id "  dAta_banA )" crlf))
)

;"wad","VT","1.dAta_banAnA"
;He waded his coat
;
