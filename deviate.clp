;@@@ Added by 14anu-ban-04 on (13-11-2014)
;In many games such as cricket, tennis, baseball, or golf, we notice that a spinning ball deviates from its parabolic trajectory as it moves through air.  [NCERT-CORPUS]
;कई खेल जैसे क्रिकेट, टेनिस, बेसबॉल या गोलक में हम देखते हैं कि वायु में जाती हुई बॉल अपने काट ऐयरोफॉयल जैसी प्रतीत होती है जिसके परितः परवलीय पथ से हट जाती है.[NCERT-CORPUS]
(defrule deviate0 
(declare (salience 10))
(id-root ?id deviate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deviate.clp 	deviate0   "  ?id "  hata_jA )" crlf))
)


 
;@@@ Added by 14anu-ban-04 (13-11-2014)
;The Mahabharata says that the king who is also the one who dispenses justice should not deviate from the path of truth.[karan-singla]
;महाभारत में कहा गया है कि एक राजा  जो न्याय भी देता/करता है  उसे  सच के पथ से नहीं भटकना चाहिए |  [manual]
(defrule deviate2
(declare (salience 30))
(id-root ?id deviate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI ?id ?id1)
(id-root ?id1 path) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deviate.clp 	deviate2  "  ?id "   Bataka)" crlf))
)

;@@@ Added by 14anu-ban-04 (13-11-2014)
;In 1965 & 1971 of india - pakistan war , this was used to deviate the air bombers .  [karan-singla]
;1965 एवं 1971 के भारत पाकिस्तान युद्ध के समय भी यही किया गया था जिससे कि वायु बमवर्षकों को भ्रमित किया जा सके ।       [karan-singla]
;1965 एवं 1971 के भारत पाकिस्तान युद्ध में वायु बमवर्षकों को भ्रमित करने के लिए यह उपयोग किया गया था |    [manual]
(defrule deviate3
(declare (salience 40))
(id-root ?id deviate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 bomber) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bramiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deviate.clp 	deviate3  "  ?id "  Bramiwa_kara)" crlf))
)


 
