;@@@ Added by 14anu-ban-01 on (19-01-2016)
;The sample was partitioned into three subsamples.	[COCA]
;नमूना तीन उप नमूनों में बाँटा गया था . 	[self]
(defrule partition0
(id-root ?id partition)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzta/viBAjiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  partition.clp 	partition0   "  ?id "  bAzta/viBAjiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (19-01-2016)
;The young man stepped into the dark entry, which was partitioned off from the tiny kitchen. 	[Crime and punishment]
;नौजवान ने अँधेरी ड्योढ़ी में कदम रखा, जिसे एक पर्दा लगा कर छोटी-सी रसोई से अलग कर दिया गया था	 [Crime and punishment]
(defrule partition1
(id-root ?id partition)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 off)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 alaga_kara_xe))
(if ?*debug_flag* then  
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  partition.clp 	partition1   "  ?id "  " ?id1 " alaga_kara_xe )" crlf))
)

;NOTE: This rule has to be generalized for Proper Nouns like~ "Ireland was partitioned in 1922.[COCA]"
;@@@ Added by 14anu-ban-01 on (19-01-2016)
;The country was partitioned a year after the elections.	[oald]
;चुनावों के एक वर्ष बाद देश/राष्ट्र को क्षेत्रों में बाँट/विभाजित_कर दिया गया था .  	[self]
(defrule partition2
(id-root ?id partition)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject  ?id ?id1)(kriyA-object  ?id ?id1))	;to partition a country[oald]
(id-root ?id1 country|state)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRewroM_meM_viBAjiwa_kara_xe/kRewroM_meM_bAzta_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  partition.clp 	partition2   "  ?id "  kRewroM_meM_viBAjiwa_kara_xe/kRewroM_meM_bAzta_xe )" crlf))
)

;I entered one large room partitioned into three sections. [COCA]	;added by 14anu-ban-01 on (19-01-2016)
;'partitioned' is a past participle here meaning 'बँटा हुअा' 
