;@@@ Added by 14anu-ban-04 (03-04-2015)
;He disarmed her immediately by apologizing profusely.             [oald]
;तत्काल  प्रचुरतापूर्वक क्षमा माँगने से   उसने उसको शान्त कर दिया.                      [self]
(defrule disarm1
(declare (salience 20))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disarm)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disarm.clp       disarm1  "  ?id "  SAnwa_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-04 (06-04-2015)
;Experts successfully managed to disarm the bomb.                   [cald]
;विशेषज्ञ  बम को निष्क्रिय करने में सफलतापूर्वक  सफल हुए .                            [self]
(defrule disarm2
(declare (salience 30))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disarm)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 bomb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id niRkriya_kara))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   disarm.clp     disarm2   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disarm.clp       disarm2 "  ?id "  niRkriya_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 (06-04-2015)
;With one movement, she disarmed the man and pinned him against the wall.              [cald]
;एक  चाल से, उसने आदमी को निरस्त्र कर दिया और दीवार से उसको बाँध दिया .                              [self]
(defrule disarm3
(declare (salience 30))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disarm)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(conjunction-components ? ?id ?id2)
(kriyA-object ?id2 ?id3)
(id-root ?id2 pin)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niraswra_kara_xe)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disarm.clp       disarm3  "  ?id "  niraswra_kara_xe )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (03-04-2015)
;Most of the rebels were captured and disarmed.             [oald]
;विद्रोही ज्यादातर पकड़े गये थे और उनके हथियार छीन लिए गये थे .               [self]
(defrule disarm0
(declare (salience 10))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disarm)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haWiyAra_CIna_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disarm.clp       disarm0  "  ?id "  haWiyAra_CIna_le )" crlf))
)

