;@@@  Added by 14anu-ban-04 (17-03-2015)
;The chapel was dedicated in 1880.                  [oald]
;छोटा गिरजा 1880 में खोला गया था .                           [self]
(defrule dedicate3
(declare (salience 4820))
(id-root ?id dedicate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dedicate.clp 	dedicate3  "  ?id "  Kola )" crlf))
)


;@@@  Added by 14anu-ban-04 (17-03-2015)
;A memorial stone was dedicated to those who were killed in the war.                 [oald]
;एक पत्थर का स्मारक   उनको अर्पण किया गया था जो युद्ध में मारे गए थे .                                      [self]
(defrule dedicate4
(declare (salience 4810))
(id-root ?id dedicate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?id1)
(id-tam_type ?id passive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id arpaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dedicate.clp 	dedicate4  "  ?id "  arpaNa_kara)" crlf))
)

;------------------------------------------------------- DEFAULT RULES --------------------------------------------------------------------
(defrule dedicate0
(declare (salience 5000))
(id-root ?id dedicate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id dedicated )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id samarpiwa_kiyA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  dedicate.clp  	dedicate0   "  ?id "  samarpiwa_kiyA_huA )" crlf))
)

(defrule dedicate1
(declare (salience 4900))
(id-root ?id dedicate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id dedicated )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id samarpiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  dedicate.clp  	dedicate1   "  ?id "  samarpiwa )" crlf))
)

;"dedicated","Adj","1.samarpiwa"
;He is a dedicated worker.
;
;

;$$$ Modified by 14anu-ban-04 (17-03-2015)           ; ---------changed meaning from 'samarpiwa_karanA' to  'samarpiwa_kara'
;He dedicated his life to helping the poors.                    [oald]
;उसने गरीबों की सहायता करने  के लिए अपना जीवन समर्पित किया .                   [self]  
(defrule dedicate2
(declare (salience 4800))
(id-root ?id dedicate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)                 ;changed category from 'noun' to 'verb'   by 14anu-ban-04 (17-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samarpiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dedicate.clp 	dedicate2   "  ?id "  samarpiwa_kara )" crlf))
)

;"dedicate","N","1.samarpiwa karanA"
;Gandhiji dedicated his life for his country.
;
;
