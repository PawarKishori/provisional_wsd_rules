;$$$ Modified by 14anu-ban-09 on 23-8-14 -- added Eng and manual sentences
;There are two books whose titles bear the prefix Agastya . [Parallel Corpus] ;Added by 14anu-ban-09
;ऐसी दो पुस़्तकें हैं जिनके नाम के साथ अगस़्त़्य उपसर्ग जुड़ा हुआ है .

(defrule prefix0
(declare (salience 0000)) ;Salience reduced by 14anu-ban-09
(id-root ?id prefix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upasarga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prefix.clp 	prefix0   "  ?id "  upasarga )" crlf))
)

;"prefix","N","1.upasarga"
;I left out the question in which we had to add prefixes.
;--"2.pUrva_prawyaya"
;In another two years I will finish my medical course && prefix Dr. will be added to my name.
;
;$$$ Modified by 14anu-ban-09 on 23-8-14
;Changed meaning "SurU_yA_prAraMBa_meM_lagA" to "prAraMBa_meM_lagA"
(defrule prefix1
(declare (salience 4900))
(id-root ?id prefix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAraMBa_meM_lagA)) ;Modified by 14anu-ban-09
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prefix.clp 	prefix1   "  ?id "  prAraMBa_meM_lagA )" crlf)) ;Modified by 14anu-ban-09
)

;@@@ Added by 14anu-ban-09 on 23-8-14
;According to Professor Shukdev Sinh , Premchand ji himself never used word Munshi as prefix to his name .
;प्रोफेसर शुकदेव सिंह के अनुसार प्रेमचंद जी ने अपने नाम के आगे मुंशी शब्द का प्रयोग स्वयं कभी नहीं किया । [Manual]
;प्रोफेसर शुकदेव सिंह के अनुसार प्रेमचंद जी ने अपने नाम के पहले मुंशी शब्द का प्रयोग स्वयं कभी नहीं किया । [Own Manual]
;Apart from this there was tradition of attaching word munshi as prefix to name, in Kayasthas.
;इसके अतिरिक्त कायस्थों के नाम के पहले सम्मान स्वरूप मुंशी शब्द लगाने की परम्परा रही है ।

(defrule prefix2
(declare (salience 5000))
(id-root ?id prefix)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-as_saMbanXI  ?id1 ?id)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-root ?id2 name)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahale)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prefix.clp 	prefix2   "  ?id "  pahale )" crlf)) 
)

;"prefix","V","1.SurU_yA_prAraMBa_meM_lagAnA"
;All S.T.D. codes are prefixed with `0'.
;
