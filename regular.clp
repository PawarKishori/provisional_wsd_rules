;@@@ Added by 14anu-ban-10 on (26-02-2015)
;Do you want baby size bicycle or regular size?[hinkhoj]
;क्या आप को  छोटे आकार कि साइकिल या  आमतौर पर पायी जाने वाली  आकार कि चाहये ? [manual]
(defrule regular2
(declare (salience 5100))
(id-root ?id regular)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 size)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AmawOra_para_pAyI_jAne_vAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regular.clp 	regular2   "  ?id "   AmawOra_para_pAyI_jAne_vAlI)" crlf))
)
;@@@ Added by 14anu-ban-10 on (26-02-2015)
;They belong to regular forces of India.[hinkhoj]
 ;वे भारत के  किसी राष्ट्र की स्थाई सेना से सम्बद्ध बलों के हैं . [manual]
(defrule regular3
(declare (salience 5200))
(id-root ?id regular)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 force)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI_rARtra_kI_sWAI_senA_se_sambaXxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regular.clp 	regular3   "  ?id "   kisI_rARtra_kI_sWAI_senA_se_sambaXxa)" crlf))
)
;@@@ Added by 14anu-ban-10 on (26-02-2015)
;A regular geometric pattern.[oald]
;एक  बराबर ज्यामितीय पैटर्न. [manual]
(defrule regular5
(declare (salience 5400))
(id-root ?id regular)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 pattern)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id barAbara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regular.clp 	regular5   "  ?id "  barAbara )" crlf))
)
;@@@ Added by 14anu-ban-10 on (26-02-2015)
;It's important to follow the regular procedure.[oald]
;यह  सामान्य  प्रक्रिया का पालन करना महत्त्वपूर्ण है  .[manual]
(defrule regular6
(declare (salience 5400))
(id-root ?id regular)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 procedure)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmAnya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regular.clp 	regular6   "  ?id "  sAmAnya )" crlf))
)
;@@@ Added by 14anu-ban-10 on (26-02-2015)
;She couldn't find any regular employment.[oald]
;वह कोई स्थायी रोजगार  ढूँढ नहीं पाई. [manual]
(defrule regular7
(declare (salience 5500))
(id-root ?id regular)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 employment)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAyI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regular.clp 	regular7   "  ?id "  sWAyI )" crlf))
)

;------------------------ Default Rules ----------------------
(defrule regular0
(declare (salience 5000))
(id-root ?id regular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyamiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regular.clp 	regular0   "  ?id "  niyamiwa )" crlf))
)

;"regular","N","1.niyamiwa_sEnika"
;He is one of the regulars here.
(defrule regular1
(declare (salience 4900))
(id-root ?id regular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyamiwa_sEnika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regular.clp 	regular1   "  ?id "  niyamiwa_sEnika )" crlf))
)

