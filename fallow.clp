
(defrule fallow0
(declare (salience 5000))
(id-root ?id fallow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa_lAla_yA_pIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fallow.clp 	fallow0   "  ?id "  kuCa_lAla_yA_pIlA )" crlf))
)

;"fallow","Adj","1.kuCa_lAla_yA_pIlA"
;There are lots of fallow farmland in Punjab.
;
(defrule fallow1
(declare (salience 4900))
(id-root ?id fallow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parawI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fallow.clp 	fallow1   "  ?id "  parawI )" crlf))
)

;"fallow","N","1.parawI{BUmi}"
;There are many fallows in our country.
;
(defrule fallow2
(declare (salience 4800))
(id-root ?id fallow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jowa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fallow.clp 	fallow2   "  ?id "  jowa )" crlf))
)

;"fallow","VT","1.jowanA"
;The farmer fallowed the farmland.
;
;@@@ Added by 14anu-ban-05 on (20-11-2014)
;Such management practices that succeed in retaining suitable soil cover in areas under fallow will ultimately reduce soil loss.[agriculture]
;इस तरह के प्रबंधन के तरीके जो कि बंजर अधीन क्षेत्रों में मृदा आवरण बनाए रखने में सफल होते हैं,अन्त में मृदा नुकसान को कम करेंगें.[manual]
(defrule fallow3
(declare (salience 5000))
(Domain agriculture)
(id-root ?id fallow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(- ?id 1) under)
=>
(retract ?mng)
(assert (id-domain_type  ?id agriculture))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) baMjara_aXIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fallow.clp  fallow3  "  ?id "  " - ?id 1 "  baMjara_aXIna)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fallow.clp      fallow3   "  ?id "  agriculture )" crlf))
)


