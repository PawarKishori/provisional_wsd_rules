
;@@@ Added by Prachi Rathore[25-2-14]
;--"3.paxacApa"
;We heard the tramp of the marching army.
(defrule tramp2
(declare (salience 5050))
(id-root ?id tramp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxacApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tramp.clp 	tramp2   "  ?id "  paxacApa )" crlf))
)

;@@@Added by 14anu-ban-07,(12-03-2015)
;Pilgrims tramp to Badrinath.(same file)
;तीर्थयात्री बद्रीनाथ के लिए पद यात्रा करते हैं . (manual)
(defrule tramp3
(declare (salience 5000))
(id-root ?id tramp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 pilgrim)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxa_yAwrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tramp.clp 	tramp3   "  ?id "  paxa_yAwrA_kara )" crlf))
)

;@@@Added by 14anu-ban-07,(12-03-2015)
; She's been tramping the streets looking for a job. (oald)
; वह नौकरी ढूँढते हुए सडकों पर भटकती रही  . (manual)
(defrule tramp4
(declare (salience 5100))
(id-root ?id tramp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 street)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bataka))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tramp.clp 	tramp4   "  ?id "  Bataka  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  tramp.clp      tramp4   "  ?id " para )" crlf)
)
)

;@@@Added by 14anu-ban-07,(12-03-2015)
;The girls went for a tramp through the countryside.(cambridge)
;लडकियाँ ग्रामीण क्षेत्र में से लम्बी सैर  के लिए गयी. (manual)
(defrule tramp5
(declare (salience 5100))
(id-root ?id tramp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-for_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lambI_sEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tramp.clp 	tramp5   "  ?id "  lambI_sEra )" crlf))
)

;------------------------ Default Rules ----------------------

;"tramp","N","1.AvArAgarxa"
;He is a tramp.
(defrule tramp0
(declare (salience 5000))
(id-root ?id tramp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvArAgarxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tramp.clp 	tramp0   "  ?id "  AvArAgarxa )" crlf))
)

;"tramp","VT","1.Xaba-Xaba_karawe_hue_calanA"
;The intruders came tramping through the enterance.
(defrule tramp1
(declare (salience 4900))
(id-root ?id tramp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xaba-Xaba_karawe_hue_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tramp.clp 	tramp1   "  ?id "  Xaba-Xaba_karawe_hue_cala )" crlf))
)

;"tramp","VT","1.Xaba-Xaba_karawe_hue_calanA"
;The intruders came tramping through the enterance.
;--"2.paxa_yAwrA_karanA"
;Pilgrims tramp to Badrinath.
;
;"tramp","N","1.AvArAgarxa"
;He is a tramp.
;--"2.paxa_yAwrA"
;During his trams in the forests he discovereds variou new species of plants.
;--"3.paxacApa"
;We heard the tramp of the marching army.
;


