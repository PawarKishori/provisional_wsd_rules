;@@@ Added by 14anu-ban-06 (16-01-2015)
;Under the influence of the electric field of the incident wave the electrons in the molecules acquire components of motion in both these directions.(NCERT)
;आपतित तरङ्ग के विद्युत क्षेत्र के प्रभाव में अणुओं में इलेक्ट्रॉन इन दोनों दिशाओं में गति ग्रहण कर लेते हैं.(NCERT)
;As Fig. 10.24 (a) shows, the incident sunlight is unpolarised.(NCERT)
;आपतित सूर्य का प्रकाश अध्रुवित है [चित्र 10.23(a) ].(NCERT)
;By putting a second polaroid, the intensity can be further controlled from 50 % to zero of the incident intensity by adjusting the angle between the pass-axes of two polaroids.(NCERT)
;दूसरा पोलेरॉइड रखकर तथा दोनों पोलेरॉइडों की पारित-अक्षों के बीच के कोण को समायोजित करके तीव्रता को आपतित तीव्रता के 50% से शून्य तक नियन्त्रित कर सकते हैं.(NCERT)
;The above discussion shows that the intensity coming out of a single polaroid is half of the incident intensity.(NCERT)
;उपरोक्त विवेचन दर्शाता है कि एक पोलेरॉइड से आने वाले प्रकाश की तीव्रता, आपतित तीव्रता की आधी है.(NCERT)
;The central part of the incident plane wave traverses the thickest portion of the lens and is delayed the most.(NCERT)
;आपतित समतल तरङ्ग का मध्य भाग लेंस के सबसे मोटे भाग से होकर जाता है तथा सर्वाधिक विलम्बित होता है.(NCERT)
(defrule incident1
(declare (salience 2000))
(id-root ?id incident)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 intensity|plane|sunlight|wave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Apawiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incident.clp    incident1   " ?id "  Apawiwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-01-2015)
;When an unpolarised beam of light is incident at the Brewster's angle on an interface of two media, only part of light with electric field vector perpendicular to the plane of incidence will be reflected. (NCERT)
;जब दो माध्यमों के अन्तरापृष्ठ पर एक अध्रुवित प्रकाश का किरण-पुञ्ज बूरस्टर कोण पर आपतित होता है, प्रकाश का केवल एक भाग,जिसका विद्युत क्षेत्र सदिश आपतन तल के लम्बवत है,परावर्तित होगा.(NCERT)
(defrule incident2
(declare (salience 2500))
(id-root ?id incident)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-at_saMbanXI ?id ?id1)
(id-root ?id1 angle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Apawiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incident.clp    incident2   " ?id "  Apawiwa )" crlf))
)

;----------------------- Default rules -----------------
;@@@ Added by 14anu-ban-06 (16-01-2015)
;Several incidents of attacks on the crew of ships anchoring off Nicobars were reported during this period .(parallel corpus)
;इस बीच निकोबार समुद्र तट के समीप समुद्र में लंगर डाले हुए जहाज के नाविकों पर अचानक आक्रमण की अनेक घटनाएं घटीं .(parallel corpus)
(defrule incident0
(declare (salience 0))
(id-root ?id incident)
?mng <- (meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incident.clp    incident0   " ?id "  GatanA )" crlf))
)


