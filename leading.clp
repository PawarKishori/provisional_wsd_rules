;@@@Added by 14anu-ban-08 (12-02-2015)   ;note- working on parser no.- 2
;He is a leading political thinker of the times.   [hinkhoj] 
;वह अपने समय का मुख्य राजनीतिक विचारक हैं.   [self]
(defrule leading0
(declare (salience 0))
(id-root ?id leading)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  leading.clp 	leading0   "  ?id "  muKya )" crlf))
)

;@@@Added by 14anu-ban-08 (12-02-2015)    ;note- working on parser no.- 2
;The name lodestone (or loadstone) given to a naturally occurring ore of iron-magnetite means leading stone.   [NCERT]
;प्राकृतिक रूप से पाए जाने वाले लोहे के एक अयस्क मैग्नेटाइट का एक नाम लोडस्टोन है, जिसका अर्थ है लीडिंग स्टोन अर्थात मार्गदर्शक पत्थर.  [NCERT]
(defrule leading1
(declare (salience 100))
(id-root ?id leading)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 stone)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArgaxarSaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  leading.clp 	leading1   "  ?id "  mArgaxarSaka )" crlf))
)
