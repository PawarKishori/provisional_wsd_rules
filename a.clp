;commented by 14anu-ban-02(09-01-2015)
;correct meaning is coming from a5.
;@@@ Added by 14anu03 on 23-june-2014
;It looks as if this play will be a flop.
;यह लगता है यदि यह खेल असफल होगा .
;(defrule a101
;(declare (salience 5500))
;(id-root ?id a)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 need)
;(test (=(+ ?id 1) ?id1))
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  a.clp      a101   "  ?id ;"  " ?id1 "  )" crlf))
;)


;$$$ Modified by Shirisha Manju (20-05-16) -- added heavy to the list
;Meera had a heavy fall.[heavy.clp]
;mIrA jora se gira gayI.[manual]
;$$$ Modified by 14anu03 on 14-june-2014
;He has a huge set of skills.[added by 14anu3 on 14-june-2014]
;Added by Amba
; A million attended.
(defrule a0
(declare (salience 3900))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) huge|large|million|distinction|first|second|third|single|few|lot|hurry|little|wash|heavy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp 	a0   "  ?id "  - )" crlf))
)

(defrule a1
(declare (salience 3800))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) even)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp 	a1   "  ?id "  eka )" crlf))
)

(defrule a2
(declare (salience 3700))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) called|what|many)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp 	a2   "  ?id "  - )" crlf))
)



;Added by Meena(19.5.10)
;Typically , a doctor will see about thirty patients a day .
;The factory typically produces 500 chairs a week.
(defrule a3
(declare (salience 1000))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 day|week|month|year|century|hour|minute|second)
(viSeRya-det_viSeRaNa ?id1 ?id)
(viSeRya-det_viSeRaNa =(- ?id 1) =(- ?id 2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp         a3   "  ?id "  prawi )" crlf))
)




;Added by Meena(25.5.10)
;I went to the store , got a gallon of milk , and returned the eggs .
(defrule a4
(declare (salience 1000))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 gallon|pint|meter|foot|centimeter|millimeter)
(or(viSeRya-of_saMbanXI  ?id1 ?id2)(viSeRya-viSeRaNa ?id1 ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp         a4   "  ?id "  - )" crlf))
)

;Added by Meena(aug'09)
;We need a president who understands us.
(defrule a6
(declare (salience 3300))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ?)
(viSeRya-det_viSeRaNa ?id1 ?id)       ;added by Meena(21.01.10)
(viSeRya-jo_samAnAXikaraNa  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_EsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp    a6   "  ?id "  eka_EsA )" crlf))
)

;Commented by Shirisha Manju .Suggested by Sriramji
;Added by Meena(2.3.10)
;Phil gave me a sweater which he bought in Paris .
;(defrule a7
;(declare (salience 3300))
;(id-root ?id a)
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 ?)
;(viSeRya-det_viSeRaNa ?id1 ?id)      
;(viSeRya-jo_samAnAXikaraNa  ?id1 ?id2)
;(id-root ?id1 sweater|shirt|book)   ;the list should be included in the database 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id vaha))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp    a7   "  ?id "  vaha )" crlf))
;)




;Added by Meena(17.9.09)
;Would you like a cup of tea?
(defrule a8
(declare (salience 3300))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) cup|glass|bowl|plate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp         a8   "  ?id "  eka )" crlf))
)

;Uncommented by Shirisha manju (20-05-16) -- added 'heavy' to the list in a0 rule 
;Commented by 14anu-ban-02(27-03-2015)  ;correct meaning is coming from a10.
;$$$Modified by 14anu-ban-02(11-02-2015)
;###[COUNTER STATEMENT]###She showed a natural aptitude for the work.[oald]
;###[COUNTER STATEMENT]###उसने कार्य के लिए प्राकृतिक रुझान दिखाया . [self]
;added 'fall' in the list.
;###[COUNTER STATEMENT]###Meera had a heavy fall.[heavy.clp]
;###[COUNTER STATEMENT]###mIrA jora se gira gayI.[manual]
 ;Added "right" in the list of "?id2" ;Ex.Go straight and take a right turn.(Meena 27.7.11)
;Added "welcome|music|curriculum" in the list (Meena 5.3.11)
;Modified by Meena(17.2.11) ;added "vacant" in the list for "id2":He had a vacant look/expression on his face.
;Added time in the "(not(id-root ?id1 ....... |offer|salary|time))";{Meena(8.02.10)
;I wondered for a long time why everyone liked her so much . 
;Added (not(id-root ?id1 ....... |offer|salary)(Meena (11.01.10))
;Added by Meena(13.10.09)
;One day a small opening appeared.
(defrule a9
(declare (salience 3400))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(or(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)(viSeRya-viSeRaNa ?id1 ?id2)(viSeRya-wulanAwmaka_viSeRaNa ?id1 ?id2)) ;added samAsa_viSeRya-samAsa_viSeRaNa by sukhada Ex:The chief guest gave a welcome address. This has been taken into account in a10(Meena 3.3.11)  
(not (id-root ?id1 sound|shame|success|effect|idea|manner|offer|salary|time|affair|fall|conductor)) ;added affair in the list(Meena 28.4.11)
(not(id-root ?id2 right|variable|vacant|welcome|music|curriculum|birthday|natural))  ;Added by (Meena 24.5.10)	;'natural' is added by 14anu-ban-02(28-02-2015)
(not(id-cat_coarse ?id1 PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp         a9   "  ?id "  eka )" crlf))
)
;Modified by Shirisha Manju (added viSeRya-wulanAwmaka_viSeRaNa )
;Ex: John is quite certainly a better choice .



;Modified by Meena(28.4.11)
;The debate was a pretty disappointing affair. 
;Added by Meena(3.3.11)
;When we want to hear a music programme on the radio, we have to tune the radio to the correct station.
;The chief guest gave a welcome address.  
(defrule a10
(declare (salience 3300))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id1 ?id)
(or(samAsa_viSeRya-samAsa_viSeRaNa  =(+ ?id 2) =(+ ?id 1))(viSeRya-viSeRaNa =(+ ?id 2) =(+ ?id 1)))
;(id-root  =(+ ?id 2) speech|address|programme|card)   
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp         a10   "  ?id "  - )" crlf))
)


;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)24-jan-2014
;There is a temple dedicated to Shiva here.
;यहाँ पर शिव को समर्पित एक मंदिर है
(defrule a11
(declare (salience 4000))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(kriyA-aBihiwa  ?kri ?id1)
(id-word ?kri is)
(kriyA-dummy_subject  ?kri ?id2)
(id-word ?id2 there)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp    a11   "  ?id "  eka )" crlf))
)
;
;@@@ Added by 14anu26     [30-06-14]
;It was in a sense , Pakistan's truth commission . 
;वह एक  तरह से , पाकिस्तान का सच आयोग था . 
(defrule a13
(declare (salience 1000))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 sense|way)
(viSeRya-det_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp         a13   "  ?id "  eka )" crlf))
)

;@@@ Added by 14anu01
;This construction must be completed within a week.
;यह निर्माण एक सप्ताह में पूरा करना होगा . 
(defrule a14
(declare (salience 5000))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) within)
(id-word =(+ ?id 1) day|night|minute|month|year|morning|week)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp 	a14   "  ?id "  eka )" crlf))
)

;commented by 14anu-ban-02(09-01-2015)
;correct meaning is coming from one21
;@@@ Added by 14anu01 on 21-06-2014
;sadly, it is a short one.
; दुर्भाग्यवश, यह छोटा है . 
;(defrule a15
;(declare (salience 5500))
;(id-root ?id a)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) short|long|mid|middle)
;(id-word =(+ ?id 2) one)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id -))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp 	a15   "  ?id "  - )" crlf))
;)

;@@@Added by 14anu-ban-02(24-02-2015)
;The motion of a current-carrying conductor in a magnetic field, the response of a circuit to an ac voltage (signal), the working of an antenna, the propagation of radio waves in the ionosphere, etc., are problems of electrodynamics.[ncert 11_01]
;किसी धारावाही चालक की चुम्बकीय क्षेत्र में गति, किसी विद्युत परिपथ की प्रत्यावर्ती वोल्टता (सिगनल) से अनुक्रिया, किसी ऐन्टेना की कार्यप्रणाली, आयन मण्डल में रेडियो तरङ्गों का सञ्चरण आदि वैद्युत गतिकी की समस्याएँ हैं.[ncert]
(defrule a16
(declare (salience 4000))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id1 conductor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp 	a16   "  ?id "  kisI )" crlf))
)

;@@@Added by 14anu-ban-02(04-03-2015)
;When an external force does work in taking a body from a point to another against a force like spring force or gravitational force, that work gets stored as potential energy of the body.[ncert 12_02]
;जब कोई बाह्य बल किसी वस्तु को एक बिंदु से दूसरे बिंदु तक, किसी अन्य बल; जैसे-स्प्रिंग बल, गुरुत्वीय बल आदि के विरुद्ध, ले जाता है, तो उस बाह्य बल द्वारा किया गया कार्य उस वस्तु में स्थितिज ऊर्जा के रूप में सञ्चित हो जाता है.[ncert]
(defrule a17
(declare (salience 4000))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(kriyA-object  ?id2 ?id1)
(id-word ?id2 taking)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp 	a17   "  ?id "  kisI )" crlf))
)

;@@@Added by 14anu-ban-02(26-03-2015)
;A blanket of snow.[oald]
;बरफ की एक परत . [self]
(defrule a18
(declare (salience 1100))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id1 ?str&:(and (not (numberp ?str))(not(gdbm_lookup_p "human.gdbm" ?str))))	;added by 14anu-ban-02(05-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp         a18   "  ?id "  eka )" crlf))
)

;@@@Added by 14anu-ban-02 (01-04-2015)
;It was just a bogus claim.[mw]
;यह सिर्फ एक झूठा दावा  था . [self]
;A blistering attack.[oald]	;run the sentence on parser no. 2
;एक गम्भीर हमला . [self]
(defrule a19
(declare (salience 3400))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id1 attack|claim|person)	;claim is added by 14anu-ban-02 on (06-04-2015)	;person is added by 14anu-ban-02(05-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp         a19   "  ?id "  eka )" crlf))
)

;========================== Default Rules ============================
(defrule a5
(declare (salience -1000))
(id-root ?id a)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  a.clp        a5   "  ?id "  - )" crlf))
)

