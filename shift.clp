
(defrule shift0
(declare (salience 5000))
(id-root ?id shift)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shift.clp 	shift0   "  ?id "  parivarwana )" crlf))
)

;"shift","N","1.parivarwana"
;There is a shift in the direction of the wind.
;--"2.pAlI"
;He is on the night shift at the factory.
;--"3.kampyUtara_yA_tAiparAitara_para_sWiwa_eka_kuMjI"
;The whole article had to be typed in small letters because the shift key was not working.
;
(defrule shift1
(declare (salience 4900))
(id-root ?id shift)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shift.clp 	shift1   "  ?id "  baxala )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-10-2014)
;Shift B parallel to itself until its tail Q coincides with that of A, i.e. Q coincides with O.[NCERT corpus]
;B को स्वयं के समान्तर खिसकाइये ताकि उसकी पुच्छ Q सदिश A की पुच्छ O के सम्पाती हो जाए.[NCERT corpus]
(defrule shift2
(declare (salience 5000))
(id-root 1 shift)
?mng <-(meaning_to_be_decided 1)
(id-word 1 shift)
(id-cat_coarse =(+ 1 1) PropN|symbol)
=>
(retract ?mng)
(assert (id-wsd_word_mng 1 KisakAiye))
(assert (id-wsd_viBakwi =(+ 1 1) ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  shift.clp 	shift2   "  =(+ 1 1) " ko)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  shift.clp 	shift2     1   KisakAiye )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-10-2014)
;I have shifted my television from drawing room to bedroom. [shift.clp]
;मैंने अपना दूरदर्शन बैठक से शयन कक्ष में स्थानान्तरित कर दिया है.[self]
(defrule shift3
(declare (salience 4900))
(id-root ?id shift)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-root ?obj television|table|bed|almirah|sofa);list can be long
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAnAnwariwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shift.clp 	shift3   "  ?id "  sWAnAnwariwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-10-2014)
;Don't try to shift the responsibility onto others.[shift.clp]
;अन्य पर उत्तरदायित्व डालने का प्रयास मत करो . [self]
(defrule shift4
(declare (salience 4900))
(id-root ?id shift)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-root ?obj responsibility|blame)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dZAla))
(assert (make_verbal_noun ?id))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shift.clp 	shift4   "  ?id "  dZAla )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  shift.clp 	shift4     "  ?id " )" crlf))
)


;"shift","V","1.baxalanA"
;The action of the novel shifts from Paris to London.
;--"2.xUsare_para_dZAlanA"
;Don't try to shift the responsibility onto others.
;--"3.sWAnAnwariwa_karanA"
;I have shifted my T.V. from drawing room to bedroom. 
;
