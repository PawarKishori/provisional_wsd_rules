;@@@ Added by 14anu-ban-11 on (02-04-2015)
;Note:- Working properly on parster no:- 10.
;It took hours to get the house straight.(oald)
;घर को सुव्यवस्थित करने मे यह घण्टे लेता है . (self)
(defrule straight3
(declare (salience 4901))
(id-root ?id straight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 house)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suvyavasWiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  straight.clp 	straight3   "  ?id "  suvyavasWiwa_kara)" crlf))
)


;@@@ Added by 14anu-ban-11 on (02-04-2015)
;Just be straight with her and tell her how you feel. (cald)
;सिर्फ उसके साथ ईमानदार रहो और उसको बताओ आप कैसे महसूस करते हैं . (self)
(defrule straight4
(declare (salience 401))
(id-root ?id straight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-with_saMbanXI  ?id ?id1)
(id-root ?id1 her|him|them)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ImAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  straight.clp 	straight4   "  ?id "  ImAnaxAra)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (02-04-2015)
;They had been working for 16 hours straight.(oald)
;वे लगातार 16 घण्टों से काम करते रहे थे . (self)
(defrule straight2
(declare (salience 400))
(id-root ?id straight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagAwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  straight.clp 	straight2   "  ?id "  lagAwAra)" crlf))
)

(defrule straight0
(declare (salience 5000))
(id-root ?id straight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  straight.clp 	straight0   "  ?id "  sIXA )" crlf))
)

;"straight","Adj","1.sIXA"
;She went straight from her school to her house.
(defrule straight1
(declare (salience 4900))
(id-root ?id straight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  straight.clp 	straight1   "  ?id "  sIXA )" crlf))
)

