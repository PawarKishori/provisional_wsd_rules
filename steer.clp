
;"steering","N","1.stIyariMga/cAlana-cakkA"
;The car's steering got wrecked in the accident.
(defrule steer0
(declare (salience 5000))
(id-root ?id steer)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id steering )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id stIyariMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  steer.clp  	steer0   "  ?id "  stIyariMga )" crlf))
)

;$$$ Modified by 14anu-ban-11 on (07-02-2015)   ;Note:- working properly on parser no. 5.
;One should not prefer steer meat.
;किसी को बछडे माँस पसन्द नहीं करना चाहिए . 
;"steer","N","1.baCadZA"
(defrule steer1
(declare (salience 4900))
(id-root ?id steer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)  ;Added by 14anu-ban-11 on (07-02-2015)
(id-root ?id1 meat)  ;Added by 14anu-ban-11 on (07-02-2015)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id baCadZA)) ;commented by 14anu-ban-11
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id baCadZe_kA_mAzsa))    ;Added by 14anu-ban-11 on (07-02-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  steer.clp 	steer1   "  ?id "  baCadZA )" crlf))            ;commented by 14anu-ban-11 on (07-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " steer.clp steer1  "  ?id1 "  " ?id "  baCadZe_kA_mAzsa)" crlf))   ;Added by 14anu-ban-11 on (07-02-2015)
)

;@@@ Added by 14anu-ban-11 on (07-02-2015)
;Our parents steer us at every step of life.(hinkhoj)
;हमारे माँ बाप जीवन के प्रत्येक कदम पर हमें मार्ग दिखाते हैं . (self)
(defrule steer3
(declare (salience 4801))
(id-root ?id steer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 parent|friend)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id mArga_xiKa)) ;commented by 14anu-ban-11 on (19-02-2015)
(assert (id-wsd_root_mng ?id mArga_xiKA)) ;Added by 14anu-ban-11 on (19-02-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  steer.clp 	steer3   "  ?id "  mArga_xiKa)" crlf));commented by 14anu-ban-11 on (19-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  steer.clp 	steer3   "  ?id "  mArga_xiKA)" crlf)) ;Added by 14anu-ban-11 on (19-02-2015)
)


;@@@ Added by 14anu-ban-11 on (07-02-2015)
;Do you know somebody working there who could give you a steer?(oald)
;क्या आप वहाँ पर काम करते हुए किसी को जानते हैं जो आपको सङ्केत दे सके? (self)
(defrule steer4
(declare (salience 4901))
(id-root ?id steer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object_2  ?id1 ?id)
(id-root ?id1 give)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkewa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  steer.clp 	steer4   "  ?id "  saMkewa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (07-02-2015)
;The ship steered into port.(oald) 
;जहाज बन्दरगाह में लाया गया .(anusaaraka)
(defrule steer5
(declare (salience 4802))
(id-root ?id steer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 port)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAyA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  steer.clp 	steer5   "  ?id "  lAyA_jA )" crlf))
)

;------------------------ Default Rules ----------------

;"steer","V","1.paricAlana"
;Captain Smith steered the Titanic into the Atlantic.
(defrule steer2
(declare (salience 4800))
(id-root ?id steer)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paricAlana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  steer.clp 	steer2   "  ?id "  paricAlana )" crlf))
)

;"steer","V","1.paricAlana"
;Captain Smith steered the Titanic into the Atlantic.
;--"2.mArga_xiKAnA"
;Our parents steer us at every step of life.
