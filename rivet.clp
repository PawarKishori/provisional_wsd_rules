
(defrule rivet0
(declare (salience 5000))
(id-root ?id rivet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ribeta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rivet.clp 	rivet0   "  ?id "  ribeta )" crlf))
)

;"rivet","N","1.ribeta"
;She has used rivet for tightening her bag.                
;
(defrule rivet1
(declare (salience 4900))
(id-root ?id rivet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KUztI_se_jadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rivet.clp 	rivet1   "  ?id "  KUztI_se_jadZa )" crlf))
)

;@@@Added by 14anu18 (21-06-2014)
;His speech riveted the attention of everyone.
;उसके भाषण ने प्रत्येक व्यक्ति का ध्यान आकर्षित किया . 
(defrule rivet2
(declare (salience 4900))
(id-root ?id rivet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-word ?id1 attention)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rivet.clp 	rivet2   "  ?id "  AkarRiwa_kara )" crlf))
)

;@@@Added by 14anu18 (21-06-2014)
;He was riveted to the newreels shown on television.
;वह दूरदर्शन पर दिखाए हुए समाचार-फिल्म को आकर्षित हुआ था.
(defrule rivet3
(declare (salience 4900))
(id-root ?id rivet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rivet.clp 	rivet3   "  ?id "  AkarRiwa_ho )" crlf))
)

;"rivet","VT","1.KUztI_se_jadZanA"
;The scissor handles are made of two pieces riveted together.
;--"2.AkqRta_karanA"
;He was completely riveted by her tale. 
;

;@@@Added by 14anu-ban-10 on (12-02-2015)
;My eyes were riveted on the figure lying in the road.[oald]
;मेरी आँखें सड़क में पड़ा आंकड़ा पर आकर्षित थी।[manual]
(defrule rivet4
(declare (salience 5100))
(id-root ?id rivet)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rivet.clp 	rivet4   "  ?id "  AkarRiwa)" crlf))
)

;$$$ Modified by 14anu-ban-06 (28-04-2015)
;@@@Added by 14anu-ban-10 on (12-02-2015)
;The steel plates were riveted together.[oald]
;स्टील प्लेट एक साथ  कीलक लगाये गये थे।[manual]
;इस्पात की चादरें एक साथ खूँटी से जडी गयी थी . (manual);added by 14anu-ban-06 (28-04-2015)
(defrule rivet5
(declare (salience 5200))
(id-root ?id rivet)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1 )
(id-root ?id1 together)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KUztI_se_jadZa));meaning changed from 'kIlaka_lagAyA' to 'KUztI_se_jadZa' by 14anu-ban-06 (28-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rivet.clp 	rivet5   "  ?id "  KUztI_se_jadZa)" crlf))
)
