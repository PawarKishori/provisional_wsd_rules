
(defrule guide0
(declare (salience 5000))
(id-root ?id guide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paWapraxarSaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guide.clp 	guide0   "  ?id "  paWapraxarSaka )" crlf))
)

;"guide","N","1.paWapraxarSaka"
;The teacher is your best guide.
;


;$$$ Modified by 14anu-ban-05 on (25-11-2014)
;added printout-statement for viBakwi 'kA'
;Modified by sheetal(19-10-09).
(defrule guide1
(declare (salience 4900))
(id-root ?id guide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArgaxarSana_kara))
(assert (kriyA_id-object_viBakwi ?id kA));Added by sheetal for sent." .
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  guide.clp     guide1   "  ?id "  mArgaxarSana_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  guide.clp       guide1   "  ?id " kA )" crlf))
)
;The role of a teacher is to guide a student towards knowledge . 

;default_sense && category=verb	aguvAI_kara	0
;"guide","VT","1.aguvAI_karanA"
;My brother guided me in my work.

;@@@ Added by 14anu-ban-05 on (21-11-2014)
;Thus the practice of calculating the fertilizer equivalent value of the nutrients in crop residue is a reasonable guide to estimating the partial value of crop residues.[agriculture]
;इस प्रकार फसल अवशिष्ट में पोषक तत्वों के उर्वरक तुल्य मूल्य की जाँच करने की कार्यप्रणाली है जो फसल अवशिष्टों की असामान्य मूल्य को मूल्यांकन करने के लिए एक उचित मार्गदर्शक  हैं .[manual]
(defrule guide2
(declare (salience 5000))
(id-root ?id guide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 reasonable)	;more constraints can be added
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uciwa_mArgaxarSaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " guide.clp  guide2  "  ?id "  " ?id1 "   uciwa_mArgaxarSaka )" crlf))
)


