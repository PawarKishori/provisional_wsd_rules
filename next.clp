;Modified by Meena(1.10.10)
;If you move up a bit , Tess can sit next to me .
;Added by Meena(24.11.09)
;He was sitting next to Mohan's brother.
(defrule next_to0
(declare (salience 5001))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-aXikaraNavAcI  =(- ?id 1) ?id1)(kriyA-next_to_saMbanXI   =(- ?id 1) ?id1)) ;kriyA-lupwa_prep_saMbanXI  is now changed to kriyA-aXikaraNavAcI (Modified by Roja 28-12-10 Suggested by Sukhada)
(id-word =(+ ?id 1) to )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng  =(+ ?id 1)  ?id  ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  next.clp       next_to0   "(+ ?id 1) " " ?id " ke_pAsa )" crlf)
))

(defrule next0
(declare (salience 5000))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to )
(id-cat_coarse =(+ ?id 2) ~verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next0   "  ?id "  - )" crlf))
)

;On very next day
(defrule next1
(declare (salience 4900))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) very)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next1   "  ?id "  agalA )" crlf))
)

(defrule next2
(declare (salience 4800))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id) ; Modified ?id + 1 as ?id1 by Manju(24-08-13) eg: For next 2 years there will be good rains.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next2   "  ?id "  agalA )" crlf))
)

(defrule next3
(declare (salience 4700))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(id-cat =(+ ?id 1) proper_noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next3   "  ?id "  agalA )" crlf))
)

(defrule next4
(declare (salience 4500))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) ~noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next4   "  ?id "  Age )" crlf))
)

;"next","Adj","1.agalA"
;What is the next word?
;The next worst disaster was the cyclone.
;

;@@@ Added by Nandini(5-12-13)
;The next is of helium, followed by carbon, oxygen, neon and so on. [from mail]
;अगला हिलियम ,उसके बाद कारबन , आक्सीजन, निओन वगैरह है .
(defrule next7
(declare (salience 4850))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ? ?id)
;(viSeRya-det_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next7   "  ?id "  agalA )" crlf))
)


;@@@ Added by Nandini(15-1-14)
;Nobody knows what will happen next.
;koI nahIM jAnawA hE ki Age kyA hogA.
(defrule next8
(declare (salience 5050))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(kriyA-aXikaraNavAcI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next8   "  ?id "  Age )" crlf))
)

;@@@ Added by 14anu-ban-08 on (08-09-2014)
;Next we lightly sprinkle some lycopodium powder on the surface of water in a large trough and we put one drop of this solution in the water.  [NCERT]
;इसके बाद एक बडे नान्द में पानी लेकर, उसके ऊपर लायकोपोडियम पाउडर छिडक कर, लाइकोपोडियम पाउडर की एक पतली फिल्म जल के पृष्ठ के ऊपर बनाते हैं ; फिर ओलीक अम्ल के पहले बनाए गए घोल की एक बून्द इसके ऊपर रखते हैं.
(defrule next9
(declare (salience 5000))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-word ?id1 sprinkle)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isake_bAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next9   "  ?id "  isake_bAxa )" crlf))
)

;@@@Added by 14anu-ban-08 (07-02-2015)
;What will she come out with next?   [example from come.clp]
;अब आगे वह क्या कहेगी?   [Self]
(defrule next10
(declare (salience 4508))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id1 ?id)
(id-root ?id1 come)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Age))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  next.clp 	next10   "  ?id "  Age )" crlf))
)

;----------------------- Default Rules ----------------
;"next","Adv","1.usake_bAxa"
;We visited the shrine next.
(defrule next6
(declare (salience 4400))
(id-root ?id next)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id usake_bAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  next.clp 	next6   "  ?id "  usake_bAxa )" crlf))
)

