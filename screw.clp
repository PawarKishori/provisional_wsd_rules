;#############################################################################
;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;######################################################################
;$$$ Modified by 14anu-ban-01 on 31-08-14.
;@@@ Added by jagriti(7.1.2014)
;This student screw with everyone in her dorm .
;यह विद्यार्थी उसके छात्रावास में सब के साथ संभोग करता है . 
(defrule screw0
(declare (salience 5000))
(id-root ?id screw);screw will always be singular in this sense:source-->oald.
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)));added by 14anu-ban-01 on 31-08-14.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMBoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  screw.clp 	screw0   "  ?id "  saMBoga_kara)" crlf))
)

;@@@ Added by jagriti(7.1.2014)
;A bracket was screwed in the wall. 
;दीवारगीर दीवार में  कस दिया गया था.
(defrule screw1
(declare (salience 4900))
(id-root ?id screw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-in_saMbanXI ?id ?)(kriyA-to_saMbanXI ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kasa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  screw.clp 	screw1   "  ?id "  kasa)" crlf))
)

;@@@ Added by 14anu-ban-01 on 31-08-14.
;We can not use a screw gauge or similar instruments.[NCERT corpus]
;इनके लिए हम पेञ्चमापी जैसे मापक - यन्त्रों का उपयोग नहीं कर सकते.[NCERT corpus]
(defrule screw4
(declare (salience 1000))
(id-root ?id screw)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 gauge)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 peFcamApI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " screw.clp	screw4  "  ?id "  " ?id1 "  peFcamApI  )" crlf))
)

;@@@ Added by 14anu-ban-01 on 31-08-14.
;This right handed screw rule is illustrated in Fig. 7.15a.  [NCERT corpus]
; दक्षिणावर्त पेञ्च का नियम चित्र 7.15a में दर्शाया गया है.[NCERT corpus]
(defrule screw5
(declare (salience 1000))
(id-root ?id screw)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) hand)
(id-root =(- ?id 2) right)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2) xakRiNAvarwa_peFca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " screw.clp	screw5  "  ?id "  "=(- ?id 1) "  "=(- ?id 2)" xakRiNAvarwa_peFca  )" crlf))
)

;@@@ Added by 14anu-ban-01 on 31-08-14.
;But the rotation of the right-handed screw in case of a×b is from a to b, whereas in case of b×a it is from b to a.[NCERT corpus]  
;लेकिन, a×b के लिए दक्षिणावर्त पेञ्च को a से b की ओर घुमाना होता है जबकि b × a के लिए b से a की ओर. [NCERT corpus]
;meaning in this rule is similar to screw5 but conditions are different.
(defrule screw6
(declare (salience 1000))
(id-root ?id screw)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 right-handed)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xakRiNAvarwa_peFca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " screw.clp	screw6  "  ?id "  "?id1" xakRiNAvarwa_peFca  )" crlf))
)

;@@@ Added by 14anu-ban-01 on 31-08-14.
;You may have observed that sometimes sealed bottles with metallic lids are so tightly screwed that one has to put the lid in hot water for sometime to open the lid. [NCERT corpus] 
;आपने यह देखा होगा कि कभी - कभी धातुओं केढक्कन वाली बंद बोतलों के चूड़ीदा - रसजयक्कनों को इतना कसकर बंद कर दिया जाता है किढक्कनों को खोलने के लिए उन्हें कुछ देर तक गर्म जल में डालना होता है.[NCERT corpus]
(defrule screw7
(declare (salience 1000))
(id-root ?id screw)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 bottle)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  screw.clp 	screw7   "  ?id "  baMxa_kara_xe)" crlf))
)


;....Default rule....
;$$$ Modified by 14anu-ban-01 on 31-08-14.
;@@@ Added by jagriti(7.1.2014)
(defrule screw2
(declare (salience 1))
(id-root ?id screw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(if ?*debug_flag* then
(assert (id-wsd_root_mng ?id peFca));corrected spelling of peFca from 'peMca' to 'peFca'
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  screw.clp 	screw2  "  ?id " peFca)" crlf))
)

;@@@ Added by jagriti(7.1.2014)
(defrule screw3
(declare (salience 1))
(id-root ?id screw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XoKA_xe ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  screw.clp 	screw3  "  ?id " XokA_xe)" crlf))
)


