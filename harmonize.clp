;@@@ Added by 14anu-ban-06 (23-03-2015)
;The new building does not harmonize with its surroundings.(OALD)
;नयी इमारत उसके आसपास के साथ मेल नहीं खाती है . (manual)
(defrule harmonize0
(declare (salience 0))
(id-root ?id harmonize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mela_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harmonize.clp 	harmonize0   "  ?id "  mela_KA )" crlf))
)

;@@@ Added by 14anu-ban-06 (23-03-2015)
;The plan is to harmonize safety standards across all the countries involved. (cambridge)
;योजना सम्मिलित हुए सभी देशों में सुरक्षा मानदण्ड एक समान करने की है . (manual)
(defrule harmonize1
(declare (salience 2000))
(id-root ?id harmonize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-across_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_samAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harmonize.clp 	harmonize1   "  ?id "  eka_samAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (23-03-2015)
; To harmonize with the lead singer. (OALD)
;प्रमुख गायक के साथ सुर ताल मिलाने को. (manual)
(defrule harmonize2
(declare (salience 2200))
(id-root ?id harmonize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(id-root ?id1 singer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sura_wAla_milA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harmonize.clp 	harmonize2   "  ?id "  sura_wAla_milA )" crlf))
)
