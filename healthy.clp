
(defrule healthy0
(declare (salience 5000))
(id-root ?id healthy)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svasWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  healthy.clp 	healthy0   "  ?id "  svasWa )" crlf))
)

;@@@ Added by 14anu21 on 16.06.2014
;I have healthy relations with my grandmother.
;मेरी दादी के साथ मेरे अच्छे सम्बन्ध हैं . 
(defrule healthy1
(declare (salience 5010))
(id-root ?id healthy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id )
(id-root ?id1 relation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  healthy.clp 	healthy1   "  ?id "  acCA )" crlf))
)

;"healthy","Adj","1.svasWa"
;'healthy'rahane ke liye prAwa:kAla vAyu sevana ke liye jAnA cAhiye.
;
;
