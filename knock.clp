(defrule knock0
(declare (salience 5000))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ArAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock0  "  ?id "  " ?id1 "  ArAma_kara  )" crlf))
)

;I spent most of my time knocking about the house yesterday.
;kala mEMne Gara meM apanA aXikawara samaya ArAma karane meM biwA xiyA


(defrule knock1
(declare (salience 4900))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 gira_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock1  "  ?id "  " ?id1 "  gira_jA  )" crlf))
)

;An old lady was knocked down as she crossed the road
;eka bUDZI Orawa sadZaka pAra karawe samaya gira gaI


(defrule knock2
(declare (salience 4800))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kImawa_meM_kamI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock2  "  ?id "  " ?id1 "  kImawa_meM_kamI_ho  )" crlf))
)

;It was an expensive ring when i bought it but now it's price has been knocked down.
;yaha azgUTI bahuwa mahaMgI WI jaba mEne ise KarIxA WA lekina aba isakI kImawa meM kamI A gaI hE



(defrule knock3
(declare (salience 4700))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 corI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock3  "  ?id "  " ?id1 "  corI_kara  )" crlf))
)

;He has knocked off that watch from the shop.
;usane vaha GadZI xukAna se curAI hE
;kImawa_meM_kamI_karanA	0
;The shopkeeper refused to knock off anymore.
;xukAnaxAra ne kImawa meM Ora aXika kamI karane se manA kara xiyA
;jalxI_se_pUrA_karanA	0
;She knocked off all the work before lunch.
;usane xopahara ke KAne se pahale apanA kAma jalxI se pUrA kara liyA
;mAra_xenA	0
;I have heard that the missing child has been knocked off.
;mEne sunA hE ki usa lApawA bacce ko mAra xiyA gayA hE



;$$$Modified by 14anu-ban-07,(07-04-2015)
;Don't take so much of sleeping pills,they'll knock you out.(samefile)
;iwanI sArI nIMxa kI goliyAz mawa lo ,ye wumhe acewa kara xeMgIM

(defrule knock5
(declare (salience 4500))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)  ;added ?id2 by 14anu-ban-07,(07-04-2015)
(id-cat_coarse ?id verb)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))) ;added by 14anu-ban-07,(07-04-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 acewa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock5  "  ?id "  " ?id1 "  acewa_kara_xe  )" crlf))
)


(defrule knock6
(declare (salience 4400))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 harA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock6  "  ?id "  " ?id1 "  harA_xe  )" crlf))
)

;I am very upset that they have knocked out our team.
;muJe bahuwa xuKa hE ki unhone hamArI tIma ko harA xiyA



(defrule knock7
(declare (salience 4300))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 takarA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock7  "  ?id "  " ?id1 "  takarA_jA  )" crlf))
)

;I got knocked over by a bus when i was going home.
;jaba mEM Gara jA rahA WA waba eka basa se takarA gayA



(defrule knock8
(declare (salience 3200))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 WakA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	  knock8  "  ?id "  " ?id1 "  WakA_xe  )" crlf))
)

;iwanA kAma mawa karo nahIM wo wuma apane Apa ko WakA xoge !


(defrule knock9
(declare (salience 3100))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 garBavawI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock9  "  ?id "  " ?id1 "  garBavawI_kara  )" crlf))
)

;Just tell me who has knocked you up?
;muJe yaha bawAo ki wumheM kisane garBavawI kiyA hE?

;$$$Modified by 14anu-ban-07,(06-04-2015)
;What time do you knock off work? (oald)
;आप कार्य किस समय पर समाप्त कर रहे हैं? (manual)
(defrule knock10
(declare (salience 4800))  ;salience increased from 2700 to 4800 14anu-ban-07,(06-04-2015)
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)	;added by 14anu-ban-07,(06-04-2015) 	
(id-root ?id2  work)  		;added by 14anu-ban-07,(06-04-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samApwa_kara)); meaning changed from samApwa_karo to samApwa_kara by 14anu-ban-07,(06-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	 knock10  "  ?id "  " ?id1 "  samApwa_kara  )" crlf))     ; meaning changed from samApwa_karo to samApwa_kara by 14anu-ban-07,(06-04-2015)
)



(defrule knock11
(declare (salience 2500))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nIce_girA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	  knock11  "  ?id "  " ?id1 "  nIce_girA  )" crlf))
)


;$$$ Modified by 14anu-ban-04 on 28-07-2014 --  Changed the relation from kriyA-subject to kriyA-object
;Added by Meena(9.02.10)
;I knocked my cup and saucer and spilled the coffee.
;meM mere pyAle Ora waSwarI se takarA gayA Ora koYPI CalakI.  ; added by 14anu-ban-04 on 28-07-2014
(defrule knock12
(declare (salience 2400))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1) ; Changed the relation from kriyA-subject to kriyA-object by 14anu-ban-04 on 28-07-2014
(id-root ?id1 cup|saucer)    ; added by 14anu-ban-04 on 28-07-2014
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takarA_jA))
(assert (kriyA_id-object_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knock.clp     knock12   "  ?id "  takarA_jA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  knock.clp      knock12   "  ?id " se )" crlf)
)
)


;@@@ Added by 14anu24
;Do not agree to have work done simply because someone happens to knock on your door with an offer .
;कोई काम करवाने के लिए इसलिए न तैयार हो जाईए कि कोई प्रस्ताव लेकर आपके दरवाजे पर दस्तक देता है.
(defrule knock14
(declare (salience 5000))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-word ?id1 door)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaswaka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knock.clp    knock14   "  ?id "  xaswaka_xe )" crlf))
)


;@@@Added by 14anu-ban-07,(08-04-2015)
;He got a nasty knock on the head.(oald)
;उसे सिर पर आघात लगा था. (manual)
(defrule knock30
(declare (salience 1000))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ?id1 ?id2)
(kriyA-object  ?id1 ?id)
(id-root ?id2 head)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AGAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knock.clp 	knock30   "  ?id "  AGAwa )" crlf))
)

;@@@Added by 14anu-ban-07,(04-04-2015)
;She knocked up a meal in ten minutes.(oald)
;उसने दस मिनटों में भोजन तैयार किया . (manual)
(defrule knock31
(declare (salience 3300))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-cat_coarse ?id verb)
(id-root ?id2 meal)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	  knock31  "  ?id "  " ?id1 "  wEyAra_kara  )" crlf))
)

;============== knock back ========

;@@@Added by 14anu-ban-07,(04-04-2015)
;That house must have knocked them back a bit.(oald)(parser problem)
;वह घर उनको थोडा सा महँगा पड़ा होगा . (manual)
(defrule knock15
(declare (salience 3500))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
(id-root ?id2 house|car)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mahazgA_padZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp          knock15  "  ?id "  " ?id1 "  mahazgA_padZa  )" crlf))
)

;@@@Added by 14anu-ban-07,(04-04-2015)
;She was knocking back the champagne at Maria's party.(cambridge)
;वह मरीअ की पार्टी में शैंपेन जल्दी से पी रही थी . (manual)
(defrule knock16
(declare (salience 3400))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-cat_coarse ?id verb)
(id-root ?id2 champagne|beer|alcohol)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jalxI_se_pI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp          knock16  "  ?id "  " ?id1 "  jalxI_se_pI  )" crlf))
)

;$$$Modified by 14anu-ban-07,(04-04-2015)
;@@@ Added by 14anu04 on 3-July-2014
;Hearing the news knocked me back. (oxford)(parser no. 3)
;समाचार सुनकर मैं अचम्भित हो गया. 
(defrule knock_tmp
(declare (salience 5000))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 back)
;(kriyA-kriyA_viSeRaNa  ?id ?id1) ;commented by 14anu-ban-07,(04-04-2015)
(kriyA-upasarga ?id ?id1)         ;uncommented by 14anu-ban-07,(04-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 acaMBiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp        knock_tmp  "  ?id "  " ?id1 "  acaMBiwa_ho  )" crlf))
)


;============= knock off ========

;@@@Added by 14anu-ban-07,(06-04-2015)
;He hired a hit-man to knock off a business rival.(cambridge)
;उसने एक उद्योग प्रतिद्वन्दी  को मारने के लिए पैसे लेकर हत्या करने वाले पेशेवर हत्यारा को काम पर रखा है. (manual)
(defrule knock18
(declare (salience 5000))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mAra))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock18  "  ?id "  " ?id1 "  mAra  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  knock.clp       knock18   "  ?id " ko )" crlf))
)

;@@@Added by 14anu-ban-07,(06-04-2015)
;The manager knocked $5 off because it was damaged.(cambridge)(parser problem)
;प्रबन्धक ने $5 कीमत में कमी किया क्योंकि वह (सामान) खराब हो गया था . (manual)
(defrule knock19
(declare (salience 5100))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-cat_coarse ?id2 number)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kImawa_meM_kamI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock19  "  ?id "  " ?id1 "  kImawa_meM_kamI_kara  )" crlf))
)



;@@@Added by 14anu-ban-07,(06-04-2015)
;He knocks off three novels a year.(oald)
;वह  तीन उपन्यास एक वर्ष में जल्दी से समाप्त करता है . (manual)
(defrule knock20
(declare (salience 5300))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id2 novel|book)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jalxI_se_samApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock20  "  ?id "  " ?id1 "  jalxI_se_samApwa_kara  )" crlf))
)

;I don't knock off until six.
;mEM Ca: baje waka apanA kAma karanA nahIM rokawA
(defrule knock4
(declare (salience 4600))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAma_karanA_roka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp        knock4  "  ?id "  " ?id1 "  kAma_karanA_roka_xe  )" crlf))
)


;@@@ Added by 14anu04 on 3-July-2014
;Let's knock off for lunch.  (oxford)
;हमें दोपहर के खाने के लिए रुकना चाहिये. 
(defrule knock_tmp2
(declare (salience 5000))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 off)
;(kriyA-kriyA_viSeRaNa  ?id ?id1)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ruka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp        knock_tmp2  "  ?id "  " ?id1 "  ruka  )" crlf))
)

;==================== knock out ==================== 

;@@@Added by 14anu-ban-07,(07-04-2015)
;He knocks out five books a year.(oald)
;वह एक  वर्ष में पाँच पुस्तकें जल्दी से खत्म करता है .(manual)
(defrule knock21
(declare (salience 4600))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id2 novel|book|draft)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jalxI_se_Kawma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock21  "  ?id "  " ?id1 "  jalxI_se_Kawma_kara  )" crlf))
)


;@@@Added by 14anu-ban-07,(07-04-2015)
;The surge in the power supply knocked out all the computers.(oald)
;विद्युत् आपूर्ति में आवेश ने सभी सङ्गणक नष्ट कर दिए . (manual)
(defrule knock22
(declare (salience 4700))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id2 computer|tank)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 naRta_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock22  "  ?id "  " ?id1 "  naRta_kara  )" crlf))
)

;@@@Added by 14anu-ban-07,(07-04-2015)
;Any creativity I had was soon knocked out of me at school. (cambridge(parser problem)
;जो मेरी  सर्जनात्मकता थी  वह विद्यालय में  शीघ्र ही गंवा दी थी . (manual)
(defrule knock23
(declare (salience 4900))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-karma ?id ?id2)
(id-root ?id2 creativity)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 gaMvA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock23  "  ?id "  " ?id1 "  gaMvA_xe  )" crlf))
)

;==================== knock around ==================== 

;@@@Added by 14anu-ban-07,(08-04-2015)
;He spent a few years knocking around Europe.(oald)(parser no. 4)
;उसने यूरोप घूमते  हुए कुछ वर्ष बिताए . (manual)
(defrule knock24
(declare (salience 4900))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 GUmawe_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock24  "  ?id "  " ?id1 "  GUmawe_ho  )" crlf))
)

;@@@Added by 14anu-ban-07,(08-04-2015)
;I used to knock around with him at school.(cambridge)
;मैं विद्यालय में उसके साथ काफी समय बिताया करता था . (manual)
(defrule knock26
(declare (salience 5000))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(kriyA-with_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAPZI_samaya_biwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock26  "  ?id "  " ?id1 "  kAPZI_samaya_biwA  )" crlf))
)

;@@@Added by 14anu-ban-07,(08-04-2015)
;It must be knocking around here somewhere.(oald)
;वह यहाँ कहीं पड़ा  होगा . (manual)
(defrule knock25
(declare (salience 4800))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 padZA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock25  "  ?id "  " ?id1 "  padZA_ho  )" crlf))
)


;==================== knock together ==================== 

;@@@Added by 14anu-ban-07,(08-04-2015)
;The house consists of two cottages knocked together.(cambridge)
; घर बना हुआ है  दो कुटियों की दीवार गिरा कर . (manual)
(defrule knock28
(declare (salience 5100))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 together)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(viSeRya-kqxanwa_viSeRaNa ?id2 ?id)
(id-root ?id2 cottage)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xIvAra_girA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock28  "  ?id "  " ?id1 "  xIvAra_girA_kara  )" crlf))
)

;@@@Added by 14anu-ban-07,(08-04-2015)
;I could knock together a quick lunch if you like.(cambridge)
;यदि आप चाहें तो  मैं दोपहर का खाना जल्दी से बना सकती हु़ँ .(manual) 
(defrule knock27
(declare (salience 5000))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 together)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jalxI_se_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " knock.clp	knock27  "  ?id "  " ?id1 "  jalxI_se_banA  )" crlf))

)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-07,(08-04-2015)
;There was a knock on the door.(oald)
;दरवाजे पर एक दसतक  थी . (manual)
(defrule knock29
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaswaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knock.clp 	knock29   "  ?id "  xaswaka )" crlf))
)

;Salience reduced by Meena(9.02.10)
(defrule knock13
(declare (salience 0))
(id-root ?id knock)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaswaka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  knock.clp    knock13   "  ?id "  xaswaka_xe )" crlf))
)


;default_sense && category=verb	xaswaka xe	0
;"knock","V","1.xaswaka xenA"
;Somebody is knocking on the door.
;--"2.Tokara lagAnA"
;He knocked the glass clear across the room.
;
;
