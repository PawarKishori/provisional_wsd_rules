
;@@@ Added by Anita--26.5.2014
;If you are still not satisfied and you have reservations about these names, there are other options that are available in the market. [By mail]
;यदि आप अभी भी संतुष्ट नहीं हैं और आपको इन नामों  के बारे में शंका है, तो अन्य विकल्प हैं जो कि बाजार में उपलब्ध हैं ।
;Workers and employees shared deep reservations about the wisdom of the government's plans for the industry. [cambridge dictionary]
;उद्योग के बारे में कर्मचारियों और कार्यकर्त्ताओं ने  सरकार की  योजनाओं की  गंभीरता के बारे काफी संशयों का आदान -प्रदान किया  ।
(defrule reservation0
(declare (salience 4900))
(id-root ?id reservation)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-about_saMbanXI  ?id ?sam)(viSeRya-viSeRaNa  ?id ?))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMSaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reservation.clp 	reservation0   "  ?id "  saMSaya )" crlf))
)

;@@@ Added by Anita--26.5.2014
;He accepted my advice without reservation. [cambridge dictionary]
;उन्होंने बिना किसी शर्त के मेरी सलाह स्वीकार कर ली।
(defrule reservation1
(declare (salience 4800))
(id-root ?id reservation)
?mng <-(meaning_to_be_decided ?id)
(kriyA-without_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sarwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reservation.clp 	reservation1   "  ?id "  Sarwa )" crlf))
)
;@@@ Added by Anita--26.5.2014
;The family lives on a Native American reservation. [cambridge dictionary]
;परिवार एक मूल अमेरिकी आरक्षण पर रहता है ।
(defrule reservation2
(declare (salience 4950))
(id-root ?id reservation)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?kri ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArakRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reservation.clp 	reservation2   "  ?id "  ArakRaNa)" crlf))
)
;@@@ Added by Anita--27.5.2014
;He's the chief warden of a big-game reservation.
;वह एक बड़े खेल की आरक्षित भूमि का मुख्य संरक्षक है ।
(defrule reservation3
(declare (salience 5000))
(id-root ?id reservation)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 warden)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArakRiwa_BUmi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reservation.clp 	reservation3   "  ?id "  ArakRiwa_BUmi)" crlf))
)

;################################default-rule#############################

;@@@ Added by Anita--26.5.2014
;I would like to make a table reservation for two people for nine o'clock. [cambridge dictionary]
;मैं नौ बजे दो लोगों के लिए एक मेंज़ का आरक्षण करना पसन्द करूँगा ।
;Please confirm your reservation in writing by Friday. [cambridge dictionary]
;कृपया शुक्रवार तक लेखन में अपने आरक्षण की पुष्टि कीजिए । 
(defrule reservation_default-rule
(declare (salience 0))
(id-root ?id reservation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArakRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reservation.clp 	reservation_default-rule   "  ?id "  ArakRaNa)" crlf))
)


