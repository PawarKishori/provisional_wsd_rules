
(defrule short0
(declare (salience 3000))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id shorts )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id CotI_pawalUna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  short.clp  	short0   "  ?id "  CotI_pawalUna )" crlf))
)

;"shorts","N","1.CotI pawalUna"
;One feel comfortable in shorts while playing.
;
(defrule short1
(declare (salience 100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short1   "  ?id "  alpa )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (21-10-2014)
;Note that hot and cold are relative terms, like tall and short.[NCERT corpus]
;ध्यान दीजिए, कि लम्बे और ठिगने की भान्ति तप्त तथा शीत भी आपेक्षिक पद हैं.[NCERT corpus]
(defrule short2
(declare (salience 4000))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TiganA/CotA));Meaning 'TiganA' added by 14anu-ban-01 on (19-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short2   "  ?id "  TiganA/CotA )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (23-03-2015)
;@@@ Added by jagriti(3.1.2014)
;Let us take a short rest and sleep a little and then we can continue on our way.
;आओ थोड़ी देर झपकी ले लें फिर आगे बढ़गें।
;The school has a short vacation in between the semesters.
;sEmestaroM ke bIca meM skUla meM WodZI  CuttiyAz rahawI hEM.
;He lives at a short distance from here.[rajpal]
;वह यहाँ से थोडी दूरी पर रहता है . 
(defrule short3
(declare (salience 4800))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 rest|vacation|distance|time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodZA))	;changed "WodZI" to "WodZA" by 14anu-ban-01 on (23-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short3   "  ?id "  WodZA )" crlf))	;changed "WodZI" to "WodZA" by 14anu-ban-01 on (23-03-2015)
)


;@@@ Added by jagriti(3.1.2014)
;A short behaviour .
;एक चिड़चिड़ा व्यवहार.	
(defrule short4
(declare (salience 4700))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 behaviour)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cidZacidZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short4   "  ?id " cidZacidZA )" crlf))
)



;$$$ Modified by 14anu-ban-01 on (23-03-2015) 
;@@@ Added by jagriti(3.1.2014)
;A short eyesight fellow .
;एक कमजोर दृष्टि वाला व्यक्ति [Translated by 14anu-ban-01 on (23-03-2015)]
(defrule short5
(declare (salience 4600))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(+ ?id 1) eyesight)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajZora))	;changed "nikata" to "kamajZora" by 14anu-ban-01 on (23-03-2015) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short5   "  ?id " kamajZora )" crlf))	;changed "nikata" to "kamajZora" by 14anu-ban-01 on (23-03-2015) 
)

;$$$ Modified by 14anu-ban-01 on (23-03-2015)
;@@@ Added by jagriti(3.1.2014)
;They were asked to submit a short report of the event.
;unase GatanA kI eka saMkRipwa reporta xene ko kahA gayA.
(defrule short6
(declare (salience 4500))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 report|essay|synopsis|presentation)	;removed 'story' from the list by 14anu-ban-01 on (23-03-2015) and added it in short14 because "short story=लघु कथा " is a better translation.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkRipwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short6   "  ?id " saMkRipwa )" crlf))
)
;@@@ Added by jagriti(3.1.2014)
;He is short of time.
;usake pAsa samaya kI kamI hE.
;He is short of ideas for improving the sales of the company.
;kampanI kI bikrI baDZAne ke liye nae vicAroM kI usake pAsa kamI hE.

(defrule short7
(declare (salience 4200))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short7   "  ?id " kamI )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (23-03-2015):modified meaning,removed"=",added pada fact
;@@@ Added by jagriti(3.1.2014)
;It took just short of fifteen minutes to reach the station.
;steSana waka pahuzcane meM 15 minata se kuCa kama lage.
(defrule short8
(declare (salience 4400))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(- ?id 1) just)
(viSeRya-of_saMbanXI  ?id ?id1)
(pada_info (group_head_id ?id1)(preposition ?id2)) 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng  (- ?id 1) ?id ?id2  se_kuCa_kama))	;changed "se_kama" to "se_kuCa_kama" ,removed "=" from "=(- ?id 1)" and added ?id2 by 14anu-ban-01 on (23-03-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short8   "  ?id " se_kama )" crlf)) commented by 14anu-ban-01 on (23-03-2015)	
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "   short.clp 	short8   " (- ?id 1) "  " ?id "  " ?id2 "  se_kuCa_kama )" crlf))	;added by 14anu-ban-01 on (23-03-2015)
)



;@@@ Added by jagriti(3.1.2014)
;The bus had a puncture just short of the bus stand.
;basa stEMda se WodZA pahale hI basa meM paMkcara ho gayA.
(defrule short9
(declare (salience 4300))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
(id-root ?id1 just)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng  ?id ?id1 WodZA_pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short9   "  ?id " WodZA_pahale )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (23-03-2015)
;@@@ Added by jagriti(7.3.2014)
;Short skirts are in again.
;छोटी स्कर्ट फिर से चलन में हैं . 
(defrule short10
(declare (salience 4200))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa  ?id1 ?id)(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id))
(id-root ?id1 skirt|turban)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA))	;changed "CotI" to "CotA" by 14anu-ban-01 on (23-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short10   "  ?id " CotA )" crlf))	;changed "CotI" to "CotA" by 14anu-ban-01 on (23-03-2015)
)

;$$$ Modified by 14anu-ban-01 on (23-03-2015)
;@@@ Added by jagriti(7.3.2014)
;Short memory.[rajpal]
;कमजोर स्मृति . 
(defrule short11
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 memory)	;removed 'intelligence' from the list  by 14anu-ban-01 as short12 is handling it.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short11   "  ?id " kamajora )" crlf))
)


;@@@ Added by jagriti(7.3.2014)
;Short intelligence.[rajpal]
;अल्प ज्ञान . 
(defrule short12
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 intelligence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short12   "  ?id " alpa )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (23-03-2015)
;The short rate was up to 9.02%. [COCA]
;अल्पकालिक दर 9.02 % तक थी . [self]
;@@@ Added by jagriti(7.3.2014)
;Short notice.[rajpal]
; अल्पकालिक सूचना .
(defrule short13
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 notice|rate)	;added 'rate' by 14anu-ban-01 on (23-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpakAlika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short13  "  ?id " alpakAlika )" crlf))
)



;$$$ Modified by 14anu-ban-01 on (23-03-2015)
;I like to read short stories and write short poems.[self]
;मैं लघु कहानियाँ पढना और लघु कविताएँ लिखना पसन्द करता हूँ . [self]
;@@@ Added by jagriti(7.3.2014)
;Short vowel.[rajpal]
;Short wave.[rajpal]
(defrule short14
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 vowel|wave|story|poem)	;added 'story|poem' by 14anu-ban-01 on (23-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laGu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  short.clp 	short14  "  ?id " laGu )" crlf))
)
;@@@ Added by jagriti(7.3.2014)
;To detect shorts in someone.[rajpal]
;किसी में कमजोरी का पता लगाना . 
(defrule short15
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id shorts)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kamajorI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  short.clp  	short15   "  ?id "  kamajorI )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-03-2015)
;That short spur of the hallway ended at a men's room.[self: with reference to coca]
;प्रवेश-कक्ष का वह अपर्याप्त छोटा_रास्ता आदमी के कमरे में समाप्त हुआ .  (self)
(defrule short16
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 spur)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aparyApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  short.clp  	short16   "  ?id "  aparyApwa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (01-04-2015)
;The hotel is only a short step from the beach. (oald(step))
;होटल समुद्रतट से सिर्फ कुछ कदम दूर है . (self)
(defrule short27
(declare (salience 4201))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 step)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kuCa_kaxama_xUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " short.clp  short27  "  ?id "  " ?id1 "  kuCa_kaxama_xUra)" crlf))
)

;@@@ Added by 14anu-ban-11 on (01-04-2015)
;He had short curly hair.(oald)
;उसके छोटे घुँघराले केश थे . (self)
(defrule short17
(declare (salience 4002))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 hair)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id CotA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  short.clp  	short17   "  ?id "  CotA)" crlf))
)

;@@@ Added by 14anu-ban-11 on (01-04-2015)
;I am pregnant she said that stopped him short.(oald)
;उसको अचानक रोका जब उसने कहा कि मैं गर्भवती हूँ    . (self)
(defrule short18
(declare (salience 4001))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa  ?id1 ?id)
(kriyA-object  ?id2 ?id1)
(id-root ?id1 him)
(id-root ?id2 stop)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id acAnaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  short.clp  	short18   "  ?id "  acAnaka)" crlf))
)


;@@@ Added by 14anu-ban-11 on (01-04-2015)
;I'm going to France for a short break. (oald)
;मैं कुछ समय के लिए फ्रांस जा रहा हूँ . (self)
(defrule short19
(declare (salience 4003))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 break)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id kuCa_samaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " short.clp  short19  "  ?id1 "  " ?id "  kuCa_samaya)" crlf))
)

;@@@ Added by 14anu-ban-11 on (01-04-2015)
;She only drinks shorts, never wine or beer. (cald)
;वह केवल  शॉर्ट्स पीती है,वाइन या बियर कभी नहीं   . (self)
(defrule short25
(declare (salience 4101))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 drink)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id SoYrtsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  short.clp  	short25   "  ?id "  SoYrtsa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (01-04-2015)
;She's always short of breath when she climbs the stairs.(cald)
;उसे हमेशा साँस की दिक्कत होती  है जब वह सीढियों पर चढती है . (self)
(defrule short26
(declare (salience 4201))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 breath)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id xikkawa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  short.clp  	short26   "  ?id "  xikkawa_ho)" crlf))
)



;"short","Adj","1.CotA"
;They had to walk only a short distance to reach the station.
;
;LEVEL 
;
;
;
;Headword : short
;
;Examples --
;
;1. "CotA"
;Both the girls had short black hair.
;xono ladZakiyoM ke kAle Cote bAla We.
;It was a short walk from there house to the station.
;unake Gara se steSana waka CotA-sA pExala kA rAswA WA.
;The days are shorter during the winter months.
;sarxiyoM ke mahInoM meM xina Cote howe hEM.
;
;2. "saMkRipwa"
;They were asked to submit a short report of the event.
;unase GatanA kI eka saMkRipwa reporta xene ko kahA gayA.
;
;3. "kamI"
;He is short of time.
;usake pAsa samaya kI kamI hE.
;He is short of ideas for improving the sales of the company.
;kampanI kI bikrI baDZAne ke liye nae vicAroM kI usake pAsa kamI hE.
;
;4. "se_kama"
;The packet should contain five envelops, but it is short of one.
;pEketa meM pAzca liPAPe hone cAhiez, lekina isameM eka kama hE.
;It took just short of fifteen minutes to reach the station.
;steSana waka pahuzcane meM 15 minata se kuCa kama lage.
;
;5. "WodZA_pahale"
;The bus had a puncture just short of the bus stand.
;basa stEMda se WodZA pahale hI basa meM paMkcara ho gayA.
;
;6. "se_WodZI_xUra_para"
;The stone fell just short of me.
;muJase WodZI_hI_xUra_para pawWara girA. 
;
;7. "WodZe_samaya_kA"
;The school has a short vacation in between the semesters.
;sEmestaroM ke bIca meM skUla meM WodZe samaya kI CuttiyAz rahawI hEM.
;
;8. "ruKAI_liye_hue"
;He was a bit short with his wife.              
;apanI pawnI ke prawi usakA vyavahAra kuCa ruKAI liye hue WA.
;
;9. "Cote" 
;He will be back in two short months.
;vaha (Cote-Cote) xo mahInoM meM vApasa A jAegA. <--WodZe_hI_samaya_meM
;Upara xiye uxAharaNoM se spaRta hE ki 'short' meM 'Cote-WodZe' yA xUsare SabxoM meM kaheM wo 'alpa' kA BAva hE.
;
;'alpa' vaswu, sWAna yA samaya kuCa BI ho sakawA hE.
;
;jo rUpa Ora AkAra meM 'alpa' ho, vaha 'CotA' hE.
;jise 'alpa' SabxoM meM kahA jAe, vaha 'saMkRipwa' hE. isameM vyavahAra kI ruKAI BI A jAwI hE - rUKe vyavahAra meM kama SabxoM meM bAwa karanA Ora Kula kara bAwa ne karanA SAmila howA hE. awaH aMgrejZI meM isa prasaMga meM BI 'short' kA prayoga howA hE.
;jo 'nirXAriwa' ke barAbara na ho vaha 'kama' yA 'pahale' hE. uxAharaNa 9 meM 'metaphorical'
;prayoga hE. yaha prayoga prAyaH 'samaya' ke saMxarBa meM howA hE waWA yahAz 'mahInA, xina, yA varRa Axi' vAswava meM kama nahIM howe para kama 'prawIwa' howe hEM.
;
;'short' Sabxa ke viBinna prayoga xeKa kara isakA mUla arWa 'alpa' liyA jA sakawA hE. jisase ki anya arWa nimna prakAra se sambanXiwa ho sakawe hEM
;
;anwarnihiwa sUwra ;
; 
;
;                    alpa
;                     |
;                ---------------------------------
;               |      |        |                |
;             vaswu     sWAna    kAla           SabxoM_meM{vyavahAra}
;              |           |--|    |             |
;       ------------------    |   alpakAlika     ruKAI_liye_hue
;       |       |        |    ----             
;      CotA   saMkRipwa  kama      |
;                               WodZA_pahale{sWAna}
;                               
;
;isaprakAra se 'short' Sabxa kisa prasaMga meM A rahA hE usake AXAra para isakA 'vaswu, sWAna, kAla' Axi kA arWa milawA hE. jEse 
;'short notice'  meM 'alpakAla kA notisa' hE. shortly xeKiye.
; 
;'short distance' 'alpa(sWAna kI) xUrI' hE.
;
;ina uxAharaNoM ke AXAra para isakA sUwra hogA -
; 
;sUwra : alpa`[>WodZA_pahale]
;
;sUwra meM 'alpa' ke sAWa '`' yaha bawAwA hE ki 'short' ke liye 'alpa' ke sAWa 'kuCa_anya arWa' BI judZe hEM. 
;
;
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_short3
(declare (salience 4800))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 rest|vacation|distance|time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   sub_samA_short3   "   ?id " WodZI )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_short3
(declare (salience 4800))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 rest|vacation|distance|time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   obj_samA_short3   "   ?id " WodZI )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_short4
(declare (salience 4700))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 behaviour)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cidZacidZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   sub_samA_short4   "   ?id " cidZacidZA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_short4
(declare (salience 4700))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 behaviour)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cidZacidZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   obj_samA_short4   "   ?id " cidZacidZA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_short6
(declare (salience 4500))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 report|essay|synopsis|presentation|story)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkRipwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   sub_samA_short6   "   ?id " saMkRipwa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_short6
(declare (salience 4500))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 report|essay|synopsis|presentation|story)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkRipwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   obj_samA_short6   "   ?id " saMkRipwa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_short11
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 intelligence|memory)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   sub_samA_short11   "   ?id " kamajora )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_short11
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 intelligence|memory)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   obj_samA_short11   "   ?id " kamajora )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_short12
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 intelligence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   sub_samA_short12   "   ?id " alpa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_short12
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 intelligence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   obj_samA_short12   "   ?id " alpa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_short13
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 notice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpakAlika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   sub_samA_short13   "   ?id " alpakAlika )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_short13
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 notice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpakAlika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   obj_samA_short13   "   ?id " alpakAlika )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_short14
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 vowel|wave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laGu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   sub_samA_short14   "   ?id " laGu )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_short14
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 vowel|wave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laGu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " short.clp   obj_samA_short14   "   ?id " laGu )" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-11-2014)
;They had achieved a lot in a short space of time.(oald)
;उन्होंने  कम समय की अवधि में बहुत हासिल किया था . (manual)
;It was found that certain metals like zinc, cadmium, magnesium, etc., responded only to ultraviolet light, having short wavelength, to cause electron emission from the surface.(Ncert) 
;यह पाया गया कि जिंक, कैडमियम, मैग्नीशियम जैसी कुछ धातुओं में यह प्रभाव केवल कम तरङ्गदैर्घ्य की पराबैंगनी तरङ्गों के लिए होता है.(Ncert)
(defrule short20
(declare (salience 5000))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 wavelength|space) ;Added 'space' by 14anu-ban-11 on (05-12-2014)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  short.clp  	short20   "  ?id "  kama )" crlf))
)

;@@@ Added by 14anu-ban-11 on (28-11-2014)
;In short, the wave picture is unable to explain the most basic features of photoelectric emission.(Ncert)
;सङ्क्षेप में, तरङ्ग-चित्रण के द्वारा प्रकाश-विद्युत उत्सर्जन के अत्यन्त मूल लक्षणों की व्याख्या नहीं हो सकती.(Ncert)
(defrule short21
(declare (salience 4100))
(id-root ?id short)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) in)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  (- ?id 1) safkRepa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng    " ?*prov_dir* "  short.clp 	short21 "  ?id " " (- ?id 1) "  safkRepa_meM )" crlf))
)
