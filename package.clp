
;@@@ Added by Sonam Gupta MTech IT Banasthali 21-1-2014
;A package wrapped in brown paper. [Cambridge]
;भूरे कागज में लपेटा हुआ बण्डल .
(defrule package2
(declare (salience 5500))
(id-root ?id package)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 wrap|cover|drape|enclose|enfold|wrapped)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baNdala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  package.clp 	package2   "  ?id "  baNdala )" crlf))
)

;In the past week , more than a hundred packages from across the country have been sent to the National Institute for Communicable
;पिछले हते देश भर से 100 से ज्यादा पैकेट नई दिल्ली स्थित राष्ट्रीय संक्रामक रोग संस्थान ( एनाऐसीडी ) में भेजे गए .
;@@@ Added by 14anu11
(defrule package3
(declare (salience 5050))
(id-root ?id package)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?id1)
(kriyA-subject  ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEketa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  package.clp 	package3   "  ?id "  pEketa )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (24-01-2015)
;@@@ Added by 14anu01 on 30-06-2014
;She has got package of rupees 1 lakh. 
;वह रुपए 1 लाख का पैकेज मिला है.
;उसे 1 लाख रुपए का पैकेज मिला है. 		[Self] ;Translation modified by 14anu-ban-09 on (21-01-2015)

(defrule package03
(declare (salience 5500))
(id-root ?id package)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(id-word =(+ ?id 1) of) 		;commented by 14anu-ban-09 on (24-01-2015)
;(id-cat_coarse ?id1 number)		;commented by 14anu-ban-09 on (24-01-2015)
(viSeRya-saMKyA_viSeRaNa  ?id1 ?)	;Added by 14anu-ban-09 on (24-01-2015)
(viSeRya-of_saMbanXI  ?id ?id1)		;Added by 14anu-ban-09 on (24-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEkeja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  package.clp 	package03   "  ?id "   pEkeja)" crlf))
)

;@@@ Added by 14anu01 on 30-06-2014
;A holiday package to Paris. 
;पेरिस के लिए एक छुट्टी पैकेज.
(defrule package4
(declare (salience 5500))
(id-root ?id package)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1) holiday|weekly|monthly|software|vacation|banking|data_based)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEkeja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  package.clp 	package4   "  ?id "   pEkeja)" crlf))
)

;$$$ Modified by 14anu-ban-09 on (21-01-2015)
;@@@ Added by 14anu01 on 30-06-2014
;A two-day package to Paris.
;पेरिस के लिए एक दो दिवसीय पैकेज
;पेरिस के लिए दो दिन का पैकेज. 		[Self]  ;Translation improved by 14anu-ban-09 on (21-01-2015)
(defrule package5
(declare (salience 5700))
(id-root ?id package)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(id-word =(- ?id 1) week|month|day)  		;commented by 14anu-ban-09 on (21-01-2015)
;(id-cat_coarse =(- ?id 2) number)		;commented by 14anu-ban-09 on (21-01-2015)
(viSeRya-to_saMbanXI  ?id ?id1)			;added by 14anu-ban-09 on (21-01-2015)
(id-cat_coarse ?id1 PropN)			;added by 14anu-ban-09 on (21-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEkeja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  package.clp 	package5   "  ?id "   pEkeja)" crlf))
)

;-------------------- Default rules ---------------
(defrule package0
(declare (salience 5000))
(id-root ?id package)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pulinxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  package.clp 	package0   "  ?id "  pulinxA )" crlf))
)

;"package","N","1.pulinxA"
(defrule package1
(declare (salience 4900))
(id-root ?id package)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaTarI_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  package.clp 	package1   "  ?id "  gaTarI_banA )" crlf))
)



;"package","V","1.gaTarI_banAnA"
;The items were packaged neatly.


