;@@@ Added by 14anu-ban-01 on (09-02-2015)
; The corpuscular model predicted that if the ray of light (on refraction) bends towards the normal then the speed of light would be greater in the second medium.[NCERT corpus]
;कणिका मॉडल ने प्रागुक्त किया कि यदि प्रकाश की किरण (अपवर्तन के समय) अभिलम्ब की ओर मुडती है, तब दूसरे माध्यम में प्रकाश की चाल अधिक होगी.[NCERT corpus]
(defrule speed2
(declare (salience 5000))
(id-root ?id speed)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 light|sound|motion|rotation|gravity|galaxy)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  speed.clp 	speed2   "  ?id "  cAla )" crlf))
)

;---------------------- Default Rules ----------------

(defrule speed0
(declare (salience 5000))
(id-root ?id speed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raPwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  speed.clp 	speed0   "  ?id "  raPwAra )" crlf))
)

;"speed","V","1.wejZI se calanA"
;Speeding fast at corners is very dangerous.
(defrule speed1
(declare (salience 4900))
(id-root ?id speed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejZI_se_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  speed.clp 	speed1   "  ?id "  wejZI_se_cala )" crlf))
)

