;@@@ Added by 14anu-ban-05 on (09-09-2015)
;On the other hand, it fostered commercialisation of Indian agriculture which adversely affected the comparative self - sufficiency of the village economies in India. [social science]
;अन्य पक्ष पर,इसने इंडियन कृषि वर्ग का  व्यावसायीकरण को बढ़ावा दिया -  जिसने भारत में  अपेक्षाकृत रूप से आत्मनिर्भर ग्रामीण अर्थ व्यवस्था को  प्रभावित किया.[manual]

(defrule foster0
(declare (salience 100))
(id-root ?id foster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZAvA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  foster.clp 	foster0   "  ?id "  baDZAvA_xe )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-09-2015)
;As images replace words, they will foster faster comprehension, enable easier communication, support stronger retention, and stimulate new ways of thinking.[COCA]
;जब प्रतिबिंब के बदले शब्द प्रयोग में लाते हैं, वे तेजी से समझने मे सहायता करेंगे, अधिक आसान सञ्चार समर्थ करेंगे, अधिक मजबूत प्रतिधारण  को सहारा देंगे, और सोच के नये मार्ग को प्रोत्साहित करेंगे . [MANUAL]
(defrule foster1
(declare (salience 101))
(id-root ?id foster)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ? )
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAyawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  foster.clp 	foster1   "  ?id "  sahAyawA_kara )" crlf))
)
