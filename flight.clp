
; @@@ added by 14anu23 on 27/6/14
;A comfortable flight.
;एक आरामदायक उडान . 
(defrule flight0
(declare (salience 5000))
(id-root ?id flight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id udZAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flight.clp 	flight0   "  ?id "  udZAna )" crlf))
)


; @@@ added by 14anu23 on 27/6/14
; We met on a flight from London to Paris.
; हम पेरिस लन्दन से विमान पर मिले . 
;हम पेरिस से लन्दन जानेवाली विमान में मिले 	; translation modified by 14anu-ban-05 on (13-12-2014)
(defrule flight1
(declare (salience 5000))
(id-root ?id flight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flight.clp 	flight1   "  ?id "  vimAna )" crlf))
)



; @@@ added by 14anu23 on 27/6/14
;Flight XY 4793 is now boarding at Gate 17. 
;विमान XY 4793 द्वार 17 में अब  सवार होना जा  रहा है . 
(defrule flight2
(declare (salience 5000))
(id-root ?id flight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flight.clp 	flight2   "  ?id "  vimAna )" crlf))
)



; @@@ added by 14anu23 on 27/6/14
;If we leave now, I can catch the earlier flight.
;यदि हम अब चले जाते हैं, तो मैं उतना अधिक पहले का विमान पकड सकता हूँ . 
(defrule flight3
(declare (salience 5000))
(id-root ?id flight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 early)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flight.clp 	flight3   "  ?id "  vimAna )" crlf))
)

; @@@ added by 14anu23 on 27/6/14
;A flight of fancy. 
;कल्पना की उडान . 
(defrule flight4
(declare (salience 5000))
(id-root ?id flight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (viSeRya-of_saMbanXI  ?id1 ?id)(viSeRya-RaRTI_viSeRaNa  ?id ?))
(id-root ?id1 fancy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalpanA_kI_udZAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flight.clp 	flight4   "  ?id "  kalpanA_kI_udZAna )" crlf))
)

;@@@ Added by 14anu-ban-05 on (13-12-2014)
;Aircraft designers make sure that none of the natural frequencies at which a wing can oscillate match the frequency of the engines in flight.[NCERT]
;vAyuyAna aBikalpanAkAra yaha suniSciwa karawe hEM ki udAna ke xOrAna iMjana kI Avqwwi kA pafKe ke xolana kI kisI BI samBAviwa prAkqwika Avqwwi se sumela na ho.[NCERT]
(defrule flight5
(declare (salience 5000))
(id-root ?id flight)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) in)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) udAna_ke_xOrAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " flight.clp  flight5  "  ?id "  " - ?id 1 "   udAna_ke_xOrAna)" crlf))
)



