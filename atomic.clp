;@@@ Added by14anu-ban-02(22-11-2014)
;It is routinely applied in the analysis of atomic, nuclear and elementary particle processes.[ncert]
;इस नियम का दिनचर्या-अनुप्रयोग परमाण्विक, नाभिकीय तथा मूल कण प्रक्रियाओं के विश्लेषणों में किया जाता है.[ncert]
(defrule atomic0
(declare (salience 0))
(id-root ?id  atomic)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paramANvika))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "   atomic.clp       atomic0   "  ?id "  physics )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   atomic.clp 	 atomic0   "  ?id "  paramANvika)" crlf))
)

;@@@ Added by 14anu-ban-02(22-11-2014)
;The cesium atomic clocks are very accurate.[ncert]
;सीजियम परमाणु घडियाँ अत्यन्त यथार्थ होती हैं.[ncert]
(defrule atomic1
(declare (salience 100))
(id-root ?id  atomic)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 clock)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paramANu))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "   atomic.clp       atomic1   "  ?id "  physics )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "   atomic.clp 	 atomic1   "  ?id "  paramANu)" crlf))
)
