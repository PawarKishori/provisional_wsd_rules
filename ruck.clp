;@@@ Added by 14anu-ban-10 on (03-03-2015)
;His brilliance raised him above the ruck.[hinkhoj]
;उसकी चमक ने  उभरा कर उसको शिकन से ऊपर किया ।[manual]
(defrule ruck2
(declare (salience 5100))
(id-root ?id ruck)
?mng <-(meaning_to_be_decided ?id)
(kriyA-above_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sikana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruck.clp 	ruck2   "  ?id "  Sikana )" crlf))
)

;@@@ Added by 14anu-ban-10 on (03-03-2015)
;When the police arrived, it soon turned into an ugly ruck.[oald]
;जब पुलिस आई, यह शीघ्र  भयंकर भीड़ में बदल गाया .  [manual]
(defrule ruck3
(declare (salience 5200))
(id-root ?id ruck)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BIdaZ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruck.clp 	ruck3   "  ?id "  BIdaZ )" crlf))
)

;@@@ Added by 14anu-ban-10 on (03-03-2015)
;To take part in a ruck.[oald]
;रक में भाग लेना.[manual]
(defrule ruck4
(declare (salience 5300))
(id-root ?id ruck)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruck.clp 	ruck4   "  ?id "  raka )" crlf))
)

;@@@ Added by 14anu-ban-10 on (03-03-2015)
;Your dress is rucked up at the back.[oald]
;आपके  वस्र का पिछला भाग सिकुड़ा हुआ  है .[manual] 
(defrule ruck5
(declare (salience 5400))
(id-root ?id ruck)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) up)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) sikudAZ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " ruck.clp  ruck5  "  ?id "  " (+ ?id 1) "   sikudAZ)" crlf))
)


;------------------------ Default Rules ----------------------

;"ruck","N","1.janasAXAraNa"
;She wants to come out of the ruck && establish herself as a leader. 
(defrule ruck0
(declare (salience 5000))
(id-root ?id ruck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id janasAXAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruck.clp 	ruck0   "  ?id "  janasAXAraNa )" crlf))
)

;"ruck","V","1.Sikana/silavata_padZanA"
;Your clothes got rucked up due to sleep. 
(defrule ruck1
(declare (salience 4900))
(id-root ?id ruck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sikana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ruck.clp 	ruck1   "  ?id "  Sikana )" crlf))
)

