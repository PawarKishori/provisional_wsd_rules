;COMMENTED BY 14anu-ban-04 on  (19-03-2015) because in this rule meaning is coming from 'encounter5'
;@@@ Added by 14anu-ban-04 (19-03-2015)
;Then the incoming α-particle could get very close to the positive charge without penetrating it, and such a close encounter would result in a large deflection.                                  [NCERT-CORPUS]
;तब अन्दर आता हुआ ऐल्फा-कण धन आवेश को भेदे बिना इसके अत्यन्त समीप आ सकता है तथा इस प्रकार के निकट समागम के परिणामस्वरूप अधिक विक्षेप होगा.        [NCERT-CORPUS]
;(defrule encounter7
;(declare (salience 5010))
;(id-root ?id encounter)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-viSeRaNa  ?id ?id1)
;(id-root ?id1 close)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id samAgama))  
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter7  "  ?id "  samAgama )" crlf))
;)

;@@@ Added by 14anu-ban-04 (20-03-2015)
;In their last encounter with Italy, England won by 3-2.      [cald]
;ईटली के साथ उनके आखिरी मुकाबले में, इंग्लैंड 3-2 से जीता .                 [self] 
(defrule encounter7
(declare (salience 5020))
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?kri ?id1)
(kriyA-in_saMbanXI ?kri ?id)
(id-root ?kri win)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukAbalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter7  "  ?id "  mukAbalA )" crlf))
)

;@@@ Added by 14anu-ban-04 (20-03-2015)
;I had an alarming encounter with a wild pig.                     [cald]
;मेरा एक जंगली सुअर के साथ भयप्रद सामना हुआ था .                              [self]                        
(defrule encounter8
(declare (salience 5010))
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-with_saMbanXI  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter8  "  ?id " sAmanA )" crlf))
)

;@@@ Added by 14anu-ban-04 (20-03-2015)
;This meeting will be the first encounter between the party leaders since the election.                     [cald]  
;चुनाव के बाद यह बैठक   पार्टी नेताओं के बीच  पहली  मुलाकात  रहेगी .                                                        [self]
(defrule encounter9
(declare (salience 5020))
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 meeting)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mulAkAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter9  "  ?id "  mulAkAwa )" crlf))
)

;@@@ Added by 14anu-ban-04 (20-03-2015)
;There was a violent encounter between fans of the opposing teams.                    [merriam-webster]
;विरोधी दलों के प्रशंसकों के बीच एक भयंकर सामना हुआ था .                                            [self]                        
(defrule encounter10
(declare (salience 5010))
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id2)
(viSeRya-between_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter10  "  ?id " sAmanA )" crlf))
)

(defrule encounter1
(declare (salience 4900))
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter1   "  ?id "  sAmanA_kara )" crlf))
)

;@@@ Added by Pramila(BU) on 28-01-2014
;I encountered him in the street.        ;shiksharthi
;मैं उससे गली में मिला.
;On their way home they encountered a woman selling flowers.         ;cald
;लौटते समय वे एक महिला फूल बेचने वाली महिला से मिले.
(defrule encounter3
(declare (salience 5000))
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or (kriyA-in_saMbanXI  ?id ?)(kriyA-on_saMbanXI  ?id ?))
(kriyA-object  ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter3   "  ?id "  mila )" crlf))
)

;@@@ Added by 14anu-ban-06 (20-10-2014)
;The following relation for rectilinear motion under constant acceleration a has been encountered in Chapter 3.(NCERT)
;अध्याय 3 में, नियत त्वरण a के अन्तर्गत सरल रेखीय गति के लिए निम्न भौतिक सम्बन्ध पढा हुआ है .(manual)
(defrule encounter4
(declare (salience 4900))
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-cat_coarse ?id verb)
(id-root ?id1 relation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paDA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter4   "  ?id "  paDA_ho )" crlf))
)

;$$$ Modified by 14anu-ban-04 on (20-03-2015)       --------changed meaning from 'sAmanA' to 'samAgama'
;Then the incoming α-particle could get very close to the positive charge without penetrating it, and such a close encounter would result in a large deflection.                                  [NCERT-CORPUS]
;तब अन्दर आता हुआ ऐल्फा-कण धन आवेश को भेदे बिना इसके अत्यन्त समीप आ सकता है तथा इस प्रकार के निकट समागम के परिणामस्वरूप अधिक विक्षेप होगा.        [NCERT-CORPUS]
;@@@ Added by 14anu23 on 26/6/14
;It was his first sexual encounter. 
;यह उसका प्रथम यौन सामना था . 
;यह उसका प्रथम यौन समागम था.                      ;modified translation by 14anu-ban-04 on (20-03-2015)
(defrule encounter5
(declare (salience 5000))                     
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
;(id-root ?id1 close|sexual|many|few)          ;commented by 14anu-ban-04 on (20-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAgama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter5   "  ?id " samAgama )" crlf))
)

;@@@ Added by 14anu23 on 26/6/14
;The two gangsters were later killed in a police encounter. 
;दो गुण्डे बाद में एक पुलीस मुठभेड में मारे गये थे . 
(defrule encounter6
(declare (salience 5010))                        ;salience increased by 14anu-ban-04 (17-12-2014)
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muTaBedZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter6   "  ?id "  muTaBedZa )" crlf))
)

;------------------------ Default Rules ----------------------

;"encounter","N","1.ladZAI"
;The terrorists were killed in an encounter with the police.
;I had a brief encounte
(defrule encounter0
(declare (salience 100))                   ;salience decreased from '5000' to '100' by 14anu-ban-04 on (20-03-2015)
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ladZAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter0   "  ?id "  ladZAI )" crlf))
)

;"encounter","VT","1.sAmanA_honA[karanA]"
;They encountered several implementaional problems in the beginning of the project. 
(defrule encounter2
(declare (salience 4800))
(id-root ?id encounter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  encounter.clp 	encounter2   "  ?id "  sAmanA_ho )" crlf))
)

