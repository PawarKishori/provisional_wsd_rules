
;$$$Modified by 14anu-ban-02(02-04-2015)
;Boisterous children.[cald]	;run the sentence on parser no. 6
;उधमी बच्चे . [self]
(defrule boisterous1
(declare (salience 100))
(id-root ?id boisterous)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)	;added by 14anu-ban-02(02-04-2015)
(id-root ?id1 child|crowd)	;added by 14anu-ban-02(02-04-2015)
(id-cat_coarse ?id adjective)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uXamI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boisterous.clp 	boisterous1   "  ?id "  uXamI )" crlf))
)

;"boisterous","Adj","1.uXamI"
;A boisterous crowd
;Boisterous practical jokes
;--"2.pralayakArI"
;Boisterous winds && waves
;
;

;@@@Added by 14anu-ban-02(02-04-2015)
;Boisterous winds & waves.[boisterous1]	;run the sentence on parser no. 25
;तेज हवाएँ और लहरें . [self]
(defrule boisterous2
(declare (salience 100))
(id-root ?id boisterous)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 wind|wave)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejaZ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boisterous.clp 	boisterous2   "  ?id "  wejaZ )" crlf))
)

;@@@Added by 14anu-ban-02(02-04-2015)
;The children and the dogs raced out of the house to give me a boisterous welcome.[oald]
;बच्चे और कुत्ते मुझे जोरदार स्वागत देना के लिये दौड़ते हुए घर के बाहर आये . [self]
(defrule boisterous3
(declare (salience 100))
(id-root ?id boisterous)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 welcome)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joZraxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boisterous.clp 	boisterous3   "  ?id "  joZraxAra )" crlf))
)

;@@@Added by 14anu-ban-02(02-04-2015)
;A boisterous game.[cald]
;धींगामस्ती से भरा खेल . [self]
(defrule boisterous4
(declare (salience 100))
(id-root ?id boisterous)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XIMgAmaswI_se_BarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boisterous.clp 	boisterous4   "  ?id "  XIMgAmaswI_se_BarA )" crlf))
)


;-----------------------default_rules-------------------------------------------
(defrule boisterous0
(declare (salience 0))	;salience reduce to 0 from 5000 by 14anu-ban-02(02-04-2015)
(id-root ?id boisterous)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kolAhalapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boisterous.clp 	boisterous0   "  ?id "  kolAhalapUrNa )" crlf))
)
