;@@@ Added by 14anu-ban-11 on (16-02-2015)
;For Gods sake Please go away. [hinkhoj]
;भगवान के लिए प्लीज बाहर  जाओ.(self)
(defrule sake0
(declare (salience 40))
(id-root ?id sake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1) god's|God's|God)
(id-word =(- ?id 2) for)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (- ?id 2) BagavAna_ke_liye)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sake.clp sake0  "  ?id "  " (- ?id 1) " " (- ?id 2) "  BagavAna_ke_liye)" crlf))
)

;--------------------- Default Rules ------------------

;@@@ Added by 14anu-ban-11 on (16-02-2015)
;For your own sake.  [cambridge]
;आपकी अपनी भलाई के लिए .(self) 
(defrule sake1
(declare (salience 00))
(id-root ?id sake)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id BalAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  sake.clp   sake1  "  ?id "  BalAI)" crlf))
)

