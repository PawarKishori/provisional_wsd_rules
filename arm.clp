
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 6-dec-2013
;Let us begin our exploration of the universe, armed with the tools provided by science.[cambridge]
;चलिये हम विज्ञान के द्वारा दिए हुए साधनों से लैस ,ब्रह्माण्ड की हमारी खोज आरम्भ करे. 
(defrule arm2
(declare (salience 5000))
(id-root ?id arm)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lEsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arm.clp 	arm2   "  ?id "  lEsa )" crlf))
)
;@@@ Added by Anita-4.6.2014
;His arms were wrapped around her waist. [oxford learner's dictionary]
;उसकी बाहें उसकी कमर के चारों ओर लपटी हुईं थीं । 
(defrule arm3
(declare (salience 5100))
(id-root ?id arm)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma  ?kri ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arm.clp 	arm3   "  ?id "  bAzha )" crlf))
)

;@@@ Added by14anu-ban-02 (24-11-2014)
;A flat vertical glass plate, below which a vessel of some liquid is kept, forms one arm of the balance.[ncert]
;एक चपटी क्षैतिज काँच की प्लेट जिसके नीचे किसी पात्र में द्रव भरा है, तुला की एक भुजा कार्य करती है.[ncert]
(defrule arm4
(declare (salience 3000))
(id-root ?id arm)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-word ?id1 balance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BujA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arm.clp 	arm4   "  ?id "  BujA )" crlf))
)

;@@@ Added by 14anu19 (13-06-2014)
;Indian mutineers were overpowered by the well armed , well organised British forces.
;भारतीय विद्रोही शास्त्रों से सुसज्तित ,सुसङ्गठित ब्रिटेन के बल से नियन्त्रण किए गये थे
(defrule arm5
(declare (salience 100));
(id-root ?id arm)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) well)
(viSeRya-viSeRaNa =(- ?id 1) ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) SAswroM_se_susajwiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng " ?*prov_dir* " arm.clp arm5 " ?id " " (- ?id 1) " SAswroM_se_susajwiwa )" crlf))
)

;****************DEFAULT RULES**************************

(defrule arm0
(declare (salience 0));salience reduced by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 6-dec-2013
(id-root ?id arm)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAhu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arm.clp 	arm0   "  ?id "  bAhu )" crlf))
)

(defrule arm1
(declare (salience 0));salience reduced by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 6-dec-2013
(id-root ?id arm)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Saswra_yukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arm.clp 	arm1   "  ?id "  Saswra_yukwa_kara )" crlf))
)

;"arm","VT","1.Saswra_yukwa_karanA"
;Two men armed with rifles entered && looted the bank
;
;LEVEL 
;Headword : arm
;



;Examples --
;
;N.
;     
;--"1.bAzha"
;Lata was holding the baby in her arms.  
;lawA bacce ko apanI goxa meM liye hue WI
;The baby rushed into his mother's arms.
;baccA wejZI se apanI mAz ki bAzhoM meM [Gusa] gayA 
;--"2.bagala" <--bAzha kA nicalA hissA
;Meena was carrying a book under her arm.
;mInA apanI bagala meM {xabAkara} eka kiwAba le jA rahI WI
;--"3.vaswrAxi_kI_bAzha"
;This dress has long arms.
;isa dresa kI bAheM bahuwa lambI hEM
;--"4. hawWA" <--kursI Axi kI bAzha 
;Put all the chairs with broken arms in the next room 
;tUte hawWoM vAlI saBI kursiyAz agale kamare meM raKa xo
;--"5.SAKA" <--vqkRa, naxI Axi kI bAzha
;This arm of the river goes towards the valley
;naxI kI yaha XArA GAtI kI ora jAwI hE
;
;V.
;
;--"6.SaswrAxi se lesa honA" <--bAzha_meM_honA
;The police was not appropriately armed for this task. 
;isa kAma ke liye pulisa sahI wOra se SaswroM se lEsa nahIM WI
;Mohan was fully armed for all the questions in the interview
;mohana intaravyU ke saBI praSnoM ke liye pUrI wOra se wEyAra WA
;
;anwarnihiwa sUwra ; 
;
;                         bAzha
;                          |
;  |-----------------------|------------------|
;  |                       |                  |
;jo hAWa meM XAraNa kiyA jAe  Akqwi            aMga{hAWa_ke_samAna_kAma_meM_AnevAlA}
;  |                       |                  |
; Saswra             kapadZe_kI_bAzha   -|-------|--------|
;                                     |       |        |
;                                 hawWA{kursI} SAKA{vqkRa} XArA {naxI}
;
;
;sUwra : bAzha`
