;Salience reduced by Meena(26.10.10)
(defrule if0
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id if)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id conjunction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaxi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  if.clp 	if0   "  ?id "  yaxi )" crlf))
)



;Added know in the list(Meena,(26.10.10))
;I don't know if he came.
(defrule if1
(declare (salience 4900))
(id-root ?id if)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) ask|decide|depend|discuss|know|tell|question|wonder)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kyA)); Modified ki_kyA as kyA by manju Ex: I do not know if he came.
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  if.clp 	if1   "  ?id "  kyA )" crlf))
)

;"if","Conj","1.yaxi"
;--"2.agara"
;If you have finished eating you may leave the table.
;


;$$$ Modified by 14anu-ban-06 (10-12-2014)
;@@@ Added by 14anu09 [25-06-14]
;If and when the gas prices go down... 
;यदि कभी तेल कीमतें कम होती हैं...
;preferred a wsd rule over an addition in the dictionary because it would not fire(some 'affecting id' type rules exist in when)
(defrule if2
(declare (salience 4900))
(id-root ?id if)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) and)
(id-word =(+ ?id 2) when)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2) yaxi_kaBI));meaning changed from 'yaXi_kaBI' to 'yaxi_kaBI' by 14anu-ban-06 (10-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " if.clp	if2  "  ?id "  " =(+ ?id 1) "	" =(+ ?id 2) " yaxi_kaBI )" crlf))
)

