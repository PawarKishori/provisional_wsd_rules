;@@@ Added by 14anu02 on 18.06.14
;The odds are that he is no longer alive.
;संभावना हैं कि वह अब नहीं जीवित है . 
(defrule odds0
(declare (salience 4900))
(id-root ?id odds)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samBAvanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  odds.clp 	odds0   "  ?id "  samBAvanA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (10-12-2014)
;Changed meaning 'bAXAe' to 'bAXA'.
;NOTE-There is a root problem with 'odds' in the below sentence. 
;@@@ Added by 14anu02 on 18.06.14
;She bravely fought against all odds.
;उसने बहादुरी से सब बाधाओ के विरुद्ध लडाई की . 
;उसने बहादुरी से सारी बाधाओं के विरुद्ध लडाई की . Translation improved by 14anu-ban-09 
(defrule odds1
(declare (salience 5000))
(id-root ?id odds)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-against_saMbanXI ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  odds.clp 	odds1   "  ?id "  bAXA)" crlf))
)
