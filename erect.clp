;@@@ Added by 14anu-ban-04 (23-01-2015)
;Erect a tent at the clean place.          [same clp file]
;स्वच्छ स्थान में तम्बू गाडो .                         [same clp file]
(defrule erect2
(declare (salience 4910))
(id-root ?id erect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 tent)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gAdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erect.clp 	erect2   "  ?id "  gAdZa )" crlf))
)

;@@@ Added by 14anu-ban-04 (23-01-2015)
;Police had to erect barriers to keep crowds back.               [oald]
;भीड़ को पीछे की तरफ रखने के लिए  पुलीस को अवरोध खडे़ करने पडे़.                  [self]
;Police erected barricades to keep the crowds from approaching the crime scene.        [merriam-webster]
;पुलिस ने भीड़ को अपराध दृश्य के निकट आने से रोकने  के लिए अस्थायी दीवारें खड़ी कीं .                            [self]
(defrule erect3
(declare (salience 4920))
(id-root ?id erect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 barrier|barricade)       ;added 'barricade' by  14anu-ban-04 on (26-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KadA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erect.clp 	erect3  "  ?id "  KadA_kara )" crlf))
)


;NOTE: [THERE IS A PARSER PROBLEM IN THE SENTENCE SO IT RUN ON PARSER NO 18.]
;@@@ Added by 14anu-ban-04 (31-01-2015)
;Always hold your head erect.            [same clp file]
;अपना सिर सदैव सीधा रखो.                      [same clp file]
;apanA sira saxEva sIXA raKo
(defrule erect4
(declare (salience 5010))
(id-root ?id erect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 head)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erect.clp 	erect4  "  ?id "  sIXA )" crlf))
)


;------------------------------------------------------------DEFAULT RULES ----------------------------------------------------------------
(defrule erect0
(declare (salience 5000))
(id-root ?id erect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXA_KadA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erect.clp 	erect0   "  ?id "  sIXA_KadA )" crlf))
)

;"erect","Adj","1.sIXA_KadA"
;Mr. A.K. Singh used to keep an erect posture at all times.
;
(defrule erect1
(declare (salience 4900))
(id-root ?id erect)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erect.clp 	erect1   "  ?id "  banA )" crlf))
)

;"erect","V","1.banAnA/KadA_karanA"
;Saleem  erected that huge mansion in a matter of a few months.
;
;LEVEL 
;Headword : erect
;
;Examples --
;
;"erect","Adj","1.sIXA/KadZA/UrXva"
;Always hold your head erect.
;apanA sira saxEva sIXA raKo
;
;"erect","VT","1.KadZA karanA" 
;He has erected a huge house near the bank.
;usane eka bahuwa badZA Gara bEMka ke pAsa KadZA karA hE
;Builder is going to erect a block of flats.
;nirmANakarwA PZlEtoM kA eka KaNda banAne jA rahA hE.
;Erect a tent at the clean place.
;svacCa sWAna para waMbU gAdZo.
;
;'erect' kA mUla arWa 'sIXA{Upara_kI_ora}' hE. awaH kriyA prayoga meM jaba koI
;cIjZa 'Upara kI ora sIXI banAI jAwI hE' wo usake liye 'erect' kA prayoga howA
;hE. isakA sUwra hogA -
;
;sUwra : sIXA_[KadZA_kara]
; 
