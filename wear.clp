
(defrule wear0
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Gata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wear.clp	wear0  "  ?id "  " ?id1 "  Gata_jA  )" crlf))
)


(defrule wear1
(declare (salience 4800))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XIre-XIre_lupwa_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wear.clp	wear1  "  ?id "  " ?id1 "  XIre-XIre_lupwa_ho_jA  )" crlf))
)



(defrule wear2
(declare (salience 4600))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XIre_XIre_bIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wear.clp	wear2  "  ?id "  " ?id1 "  XIre_XIre_bIwa  )" crlf))
)



(defrule wear3
(declare (salience 4400))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Gisa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wear.clp	wear3  "  ?id "  " ?id1 "  Gisa_jA  )" crlf))
)

;$$$ Modified by Pramila(BU) on 01-03-2014
;The sheets have worn thin.   ;oald
;चादरें पतली हो गई है.
;meaning changed from 'Gisa_jA' to 'ho'
(defrule wear4
(declare (salience 4200))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat_coarse =(+ ?id 1) adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear4   "  ?id "  ho )" crlf))
)


;@@@ Added by Pramila(BU) on 22-03-2014
;He was wearing worn shoes.   ;oald
;वह घिसे हुए जूते पहने हुए था.
(defrule wear5
(declare (salience 4200))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat_coarse =(+ ?id 1) adjective)
(kriyA-subject ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahanA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear5   "  ?id "  pahanA_ho )" crlf))
)



;$$$ Modified by 14anu-ban-11 on (09-02-2015)
;Added by Meena(3.02.10)
;In some countries wearing yashmak is compulsory for women.
;कुछ देशों में  स्त्रियों के लिए बुर्खा पहनना अनिवार्य है .(self)
(defrule wear6
(declare (salience 4100))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)  ; Added by 14anu-ban-11 on (09-02-2015)
(viSeRya-viSeRaNa ?id1 ?id) ; Added by 14anu-ban-11 on (09-02-2015)
(id-word ?id1 yashmak) 	; Added by 14anu-ban-11 on (09-02-2015)
;(kriyA-object ?id ?id1)	; commented by 14anu-ban-11 on (09-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear6   "  ?id "  pahana )" crlf))
)




;Salience reduced by Meena(3.02.10)
(defrule wear7
(declare (salience 0))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp 	wear7   "  ?id "  pahana )" crlf))
)

;default_sense && category=verb	pahana	0
;"wear","VTI","1.pahananA"
;Wear this jacket   .
;--"2.BAva_honA"
;His face wore a sad look.
;--"3.Gisa_jAnA"
;The tyres are worn due to constant use.
;--"4.jZyAxA_calanA"
;Fashion clothes often do not wear very long.
;
;

;@@@ Added by Pramila(BU) on 01-03-2014
;His face wore a sad look.   ;shiksharthi
;उसके चेहरे पर उदासी लग रही थी.
(defrule wear8
(declare (salience 4900))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 look|smile)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(assert (kriyA_id-subject_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp 	wear8   "  ?id "  laga )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  wear.clp 	wear8  "?id " para )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;He wears a smile  ;shiksharthi
;उसके चेहरे पर मुस्कुराहट रहती है
(defrule wear9
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root-category-suffix-number ?id ? verb s s)
(kriyA-object  ?id ?id1)
(id-root ?id1 look|smile)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(assert (kriyA_id-subject_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp 	wear9   "  ?id "  raha )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  wear.clp 	wear9  "?id " para )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;She wears her hair short.    ;shiksharthi
;वह अपने छोटे बाल रखती है.
(defrule wear10
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 hair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear10   "  ?id "  raKa )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;This material wears well.    ;shiksharthi
;यह माल बहुत चलता है.
(defrule wear11
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id2)
(id-root ?id2 well)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear11   "  ?id "  cala )" crlf))
)


;@@@ Added by Pramila(BU) on 01-03-2014
;This cloth will wear for years.    ;shiksharthi
;यह कपड़ा वर्षो चलेगा.
(defrule wear12
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id2)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear12   "  ?id "  cala )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;He is worn with hard work.    ;shiksharthi
;वह कठिन परिश्रम से थक गया है.
(defrule wear13
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id2)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear13   "  ?id "  Waka )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;His coat has worn away.    ;shiksharthi
;उसका कोट जीर्ण हो गया है.
(defrule wear14
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 away)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jIrNa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wear.clp	wear14  "  ?id "  " ?id1 "  jIrNa_ho  )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;I use this suit as common wear.   ;shiksharthi
;मैं इस सूट को आम पोशाक की तरह इस्तेमाल करता हूँ.
(defrule wear15
(declare (salience 0))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id poSAka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear15   "  ?id "  poSAka )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;The carpets are starting to wear.   ;oald
;कालीन घिसना शुरू हो रहे हैं.
(defrule wear16
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id2 ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gisa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear16   "  ?id "  Gisa )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;I've worn holes in all my socks.   ;oald
;मैंने अपने सभी मोजे में छेद कर दिए.
(defrule wear17
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(viSeRya-in_saMbanXI  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear17   "  ?id "  kara )" crlf))
)

;@@@ Added by Pramila(BU) on 01-03-2014
;His shoes were beginning to show signs of wear.   ;oald
;उसके जूतो ने घिसाव के लक्षण दिखाने शुरू कर दिए है.
;The machines have to be checked regularly for wear.   ;oald
;मशीनों की घिसाव के लिए नियमित रूप से जाँच की जानी चाहिए.
(defrule wear18
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI  ?id1 ?id)(kriyA-for_saMbanXI  ?id1 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GisAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear18   "  ?id "  GisAva )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 01.07.2014 email-id:sahni.gourav0123@gmail.com
;The carpets starts wearing.
;कालीन घिसना शुरु करता है .
(defrule wear19
(declare (salience 4000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kqxanwa_karma ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GisanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear19   "  ?id "  GisanA )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 01.07.2014 email-id:sahni.gourav0123@gmail.com
;The water had worn a channel in the rock.
;पानी ने चट्टान में नहर बनाई थी . 
(defrule wear20
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 channel|hole|path)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear20   "  ?id "  banAnA )" crlf))
)

;@@@Added by 14anu19(27-06-2014)
;He was worn out.
;वह थक गया था
(defrule wear21
(declare (salience 5000))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 out)
(kriyA-subject  ?id ?id2)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  wear.clp      wear21   "  ?id "  " ?id1 " Waka )" crlf))
)

;@@@ Added by 14anu-ban-11 on (09-02-2015)
;She wore a shawl about her shoulders.(mail)
;उसने उसके कन्धों चारो ओर शाल लपेटी .(self) 
(defrule wear22
(declare (salience 4200))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 shawl)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lapetI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear22   "  ?id "  lapetI )" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-03-2015)
;She was wearing a dress in a putrid shade of yellow.(cald)
;उसने पीले रङ्ग की एक भ्रष्ट छाया का लिबास पहना हुआ था . (self)
(defrule wear23
(declare (salience 5001))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 dress)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahanA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear23   "  ?id "  pahanA_ho )" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-03-2015)
;Cottons are good for summer wear.(hinkhoj)
;ग्रीष्म मे कपास पहनावे के लिए अच्छी हैं . 
(defrule wear24
(declare (salience 4200))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id1 ?id2)
(viSeRya-for_saMbanXI  ?id2 ?id)
(id-root ?id1 cotton)
(id-root ?id2 good)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahanAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear24   "  ?id "  pahanAva )" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-03-2015)
;I'd ask my boss for some time off but I don't think she'd wear it.(cambridge)
;मैं कुछ छुट्टी के लिए अपने बॉस से पूछूँगा परन्तु मैं नहीं सोचता हूँ कि वह यह अनुमति देगी . (self)
(defrule wear25
(declare (salience 4200))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 she|he)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumawi_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear25   "  ?id "  anumawi_xe )" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-04-2015)
;I don't have anything suitable to wear for the party.(cald)
;पार्टी मे पहनने के लिए  मेरे पास कुछ भी अच्छा नही है . (self)
(defrule wear26
(declare (salience 5001))
(id-root ?id wear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 party)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wear.clp      wear26   "  ?id "  pahana)" crlf))
)



