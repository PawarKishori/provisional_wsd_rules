
(defrule component0
(declare (salience 5000))
(id-root ?id component)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AMSika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  component.clp 	component0   "  ?id "  AMSika )" crlf))
)

;"component","Adj","1.AMSika"
;The spare component parts of cars were dumped by the mechanic.
(defrule component1
(declare (salience 00)) ; salience reduced by 14anu-ban-03
(id-root ?id component)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  component.clp 	component1   "  ?id "  aMSa )" crlf))
)

;"component","N","1.aMSa"
;The mechanic dumped the spare components of cars.
;
;$$$ Modified by 14anu-ban-06 (20-10-2014)
;The right side is a product of the displacement and the component of the force along the displacement.
;समीकरण का दायाँ पक्ष वस्तु पर आरोपित बल का विस्थापन के अनुदिश घटक और वस्तु के विस्थापन का गुणनफल है.(NCERT)
;@@@ Added by 14anu-ban-03(20-8-014)
;The component parallel to the surfaces in contact is called friction. [NCERT-CORPUS]
; पृष्ठों के समान्तर घटक को घर्षण बल कहते हैं.
;Friction, by definition, is the component of the contact force parallel to the surfaces in contact, which opposes impending or actual relative motion between the two surfaces. 
; परिभाषा के अनुसार, घर्षण बल सम्पर्क बल का सम्पर्क पृष्ठों के समान्तर घटक होता है, जो दो पृष्ठों के बीच समुपस्थित अथवा वास्तविक आपेक्ष गति का विरोध करता है.
(defrule component2
(declare (salience 4900))
(id-root ?id component)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI ?id ?id1) (samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id) (samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)) 
(id-word ?id1 parallel|perpendicular|torque|force);added force by 14anu-ban-06 (20-10-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  component.clp 	component2   "  ?id "  Gataka )" crlf))
)

;@@@ Added by 14anu-ban-03 (31-10-2014)
;It is much easier to add vectors by combining their respective components. [ncert]
;भिन्न - भिन्न सदिशों को उनके सङ्गत घटकों को मिलाकर जोडना अधिक आसान होता है. [ncert]
(defrule component3
(declare (salience 4900))
(id-root ?id component)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  component.clp 	component3   "  ?id "  Gataka )" crlf))
)

;@@@ Added by 14anu-ban-03 (28-11-2014)
;The radius of the circular component of motion is called the radius of the helix. [ncert]
;gawi ke vqwwIya avayava kI wrijyA ko kuNdalinI kI wrijyA kahawe hEM. [ncert]
(defrule component4
(declare (salience 4900))
(id-root ?id component)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1) 
(id-root ?id1 circular)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avayava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  component.clp     component4   "  ?id "  avayava )" crlf))
)

;@@@ Added by 14anu-ban-03 (02-12-2014)
;Many newer pesticides are derived from plants or naturally occurring substances , but the toxophore or active component may be altered to provide increased biological activity or stability. [agriculture]
;कई नए कीटनाशक पौधों या प्राकृतिक रूप से उत्पन्न पदार्थों से प्राप्त किए जाते हैं, लेकिन जैविक गतिविधि या स्थिरता मे वृद्धि प्रदान करने के लिए toxophore या सक्रिय घटक  परिवर्तित किया जा सकता है।  [manual]
(defrule component5
(declare (salience 4900))
(id-root ?id component)
?mng <-(meaning_to_be_decided ?id)
(Domain agriculture)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1) 
(id-root ?id1 active)
=>
(retract ?mng)
(assert (id-domain_type  ?id agriculture))
(assert (id-wsd_root_mng ?id Gataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  component.clp 	component5   "  ?id "  agriculture )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  component.clp    component5   "  ?id "  Gataka )" crlf))
)





