;@@@ Added by 14anu-ban-05 on (26-02-2015)
;The festival was a feast of colours.[from feast.clp]
;यह त्यौहार रङ्गों का उत्सव था . [manual]

(defrule feast2
(declare (salience 5001))
(id-root ?id feast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 colour)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feast.clp 	feast2   "  ?id "  uwsava )" crlf))
)

;@@@ Added by 14anu-ban-05 on (26-02-2015)
;Travelling in Kerala State is a feast to eyes. [from feast.clp]
;केरल राज्य में यात्रा आँखों को आनन्दित करता  है.            [manual]

(defrule feast3
(declare (salience 5002))
(id-root ?id feast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Ananxiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feast.clp 	feast3   "  ?id "  Ananxiwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (26-02-2015)
;The evening was a real feast for music lovers.[oald]
;यह सन्ध्या सङ्गीत प्रेमियों के लिए एक आनन्ददायक समारोह था . 	[manual]

(defrule feast4
(declare (salience 5002))
(id-root ?id feast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAroha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feast.clp 	feast4   "  ?id "  samAroha )" crlf))
)

;@@@ Added by 14anu-ban-05 on (26-02-2015)
;A book that is a veritable feast for the mind.[freedictionary]
;यह पुस्तक  मन के लिए  संतोषजनक है .		[manual] 

(defrule feast5
(declare (salience 5003))
(id-root ?id feast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(- ?id 1) veritable)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) saMwoRajanaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " feast.clp  feast5  "  ?id "  " (- ?id 1) "  saMwoRajanaka)" crlf))
)

;------------------------ Default Rules ----------------------

;"feast","N","1.Boja"
;The wedding feast was excellent.
(defrule feast0
(declare (salience 5000))
(id-root ?id feast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Boja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feast.clp 	feast0   "  ?id "  Boja )" crlf))
)

;"feast","V","1.Bojana_karanA"
;Guests had a sumptuousfeast at the wedding.
(defrule feast1
(declare (salience 4900))
(id-root ?id feast)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bojana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  feast.clp 	feast1   "  ?id "  Bojana_kara )" crlf))
)



;"feast","N","1.Boja"
;The wedding feast was excellent.
;--"2.saMwoRa"
;The festival was a feast of colours.
;--"3.wyEhAra"
;Feast of Christ is one of the bigest festivals in Goa.


;"feast","V","1.Bojana_karanA"
;Guests had a sumptuousfeast at the wedding.
;--"2.Annaxiwa_karana"
;Travelling in Kerala State is a feast to eyes.


