;@@@ Added by 14anu-ban-02(17-02-2015)
;Sentence: Chess can be an extremely absorbing game.[oald]
;Translation: 	चेस एक अत्यधिक दिलचस्प खेल हो सकता है . [anusaaraka]
(defrule absorbing0 
(declare (salience 0)) 
(id-root ?id absorbing) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id xilacaspa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  absorbing.clp  absorbing0  "  ?id "  xilacaspa )" crlf)) 
) 

;@@@ Added by 14anu-ban-02(17-02-2015)
;The opposite is true for energy absorbing (endothermic) reactions.[11_01]	run this sentence on parser no.108
;ऊष्मा अवशोषी अभिक्रियाओं में इसका विलोम सत्य है.[ncert]
(defrule absorbing1 
(declare (salience 100)) 
(id-root ?id absorbing) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 reaction|water|film|state) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id avaSoRiwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  absorbing.clp  absorbing1  "  ?id "  avaSoRiwa )" crlf)) 
) 
