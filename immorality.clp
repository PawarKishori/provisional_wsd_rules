;@@@ Added by 14anu-ban-06 (23-04-2015)
;Sexual immorality. (OALD)[parser no.-2]
;यौन व्यभिचार . (manual)
(defrule immorality1
(declare (salience 2000))
(id-root ?id immorality)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 sexual)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyaBicAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immorality.clp 	immorality1   "  ?id "  vyaBicAra )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (23-04-2015)
;The immorality of war.(OALD)
;युद्ध की अनैतिकता . (manual)
(defrule immorality0
(declare (salience 0))
(id-root ?id immorality)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anEwikawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immorality.clp 	immorality0   "  ?id "  anEwikawA )" crlf))
)
