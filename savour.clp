;@@@ Added by 14anu-ban-11 on (16-02-2015)
;I wanted to savour every moment.(oald)
;मैं प्रत्येक क्षण का आन्नद उठना चाहता  हूँ . (self)
(defrule savour2
(declare (salience 4800))
(id-word ?id savour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 moment)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id Annaxa_uTa)); Commented by 14anu-ban-11 on (19-02-2015)
(assert (id-wsd_root_mng ?id Ananaxa_uTa)) ;Added by 14anu-ban-11 on (19-02-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  savour.clp 	savour2   "  ?id "  Annaxa_uTa)" crlf)); Commented by 14anu-ban-11 on (19-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  savour.clp 	savour2   "  ?id "  Ananaxa_uTa)" crlf));Added by 14anu-ban-11 on (19-02-2015)
)

;-------------------- Default Rules ----------------

;"savour","N","1.svAxa"
;The delicious savour of gulabjamun attracts me.
(defrule savour0
(declare (salience 5000))
(id-root ?id savour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savour.clp 	savour0   "  ?id "  svAxa )" crlf))
)

;"savour","V","1.pasanxa_karanA"
;I like to savour the rewards of success.
(defrule savour1
(declare (salience 4900))
(id-root ?id savour)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pasanxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  savour.clp 	savour1   "  ?id "  pasanxa_kara )" crlf))
)

