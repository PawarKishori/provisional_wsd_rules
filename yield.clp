
(defrule yield0
(declare (salience 5000))
(id-root ?id yield)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id yielding )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id narma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  yield.clp  	yield0   "  ?id "  narma )" crlf))
)

;"yielding","Adj","1.narma"
;I practised polevault with an yielding bamboo stick.
;
;
(defrule yield1
(declare (salience 4900))
(id-root ?id yield)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pExAkara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " yield.clp yield1 " ?id "  pExAkara )" crlf)) 
)

(defrule yield2
(declare (salience 4800))
(id-root ?id yield)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pExAkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " yield.clp	yield2  "  ?id "  " ?id1 "  pExAkara  )" crlf))
)

(defrule yield3
(declare (salience 4700))
(id-root ?id yield)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  yield.clp 	yield3   "  ?id "  upaja )" crlf))
)

;"yield","N","1.upaja"
;The yield of kharif crops was not good this season.
;
(defrule yield4
(declare (salience 4600))
(id-root ?id yield)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXIna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  yield.clp 	yield4   "  ?id "  aXIna_ho )" crlf))
)

;"yield","VI","1.aXIna_honA"
;He yielded to his boss's pressure.
;
(defrule yield5
(declare (salience 4500))
(id-root ?id yield)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  yield.clp 	yield5   "  ?id "  xe )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (17-02-2016)
;As observations improve in detail and precision or experiments yield new results, theories must account for them, if necessary, by introducing modifications.	[NCERT corpus]
;जैसे-जैसे प्रेक्षणों के विस्तृत विवरण तथा परिशुद्धता में संशोधन होते जाते हैं, अथवा प्रयोगों द्वारा नए परिणाम प्राप्त होते जाते हैं, वैसे यदि आवश्यक हो तो उन संशोधनों को सन्निविष्ट करके सिद्धान्तों में उनका स्पष्टीकरण किया जाना चाहिए.	[NCERT corpus]
;@@@ Added by 14anu-ban-11 
;The triangle law is used to obtain the resultant of A and B and we see that the two methods yield the same result.(NCERT)
;चित्र 4.6(c) में सदिशों A व B का परिणामी निकालने के लिए त्रिभुज नियम का उपयोग दिखाया गया है ;दोनों चित्रों से स्पष्ट है कि दोनों विधियों से एक ही परिणाम निकलता है .
(defrule yield6
(declare (salience 5100))
(id-root ?id yield)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 result)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_ho))	;meaning changed from 'nikala' to 'prApwa_ho' by 14anu-ban-01 on (17-02-2016)
(assert (kriyA_id-subject_viBakwi ?id se)) 	;added by 14anu-ban-01 on (17-02-2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  yield.clp 	yield6   "  ?id " se )" crlf)	
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  yield.clp 	yield6   "  ?id "  prApwa_ho )" crlf))	
)

