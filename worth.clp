
;$$$ Modified by 14anu-ban-07 -- ;added viewing|seeing in id-word fact
;The statue of Buddha located in the Northern part of Grand Palace is worth viewing. (parallel corpus)
;ग्रांड  पैलेस  के  उत्तरी  भाग  में  स्थित  बुद्ध  की  प्रतिमा  देखने  लायक  है  ।
;@@@ Added by Pramila(BU) on 20-02-2014
;There is never anything worth watching on TV.
;कभी भी दूरदर्शन पर कुछ भी देखने लायक नहीं होता है .
(defrule worth0
(declare (salience 4900))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(+ ?id 1) watching|viewing|seeing)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) xeKane_lAyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " worth.clp	worth0  "  ?id "  "  (+ ?id 1)"  xeKane_lAyaka  )" crlf))
)

;$$$ Modified by 14anu07 0n 04/07/2014
;It is also worth asking whether a blind numbers approach is the correct - indeed , only - way to ensure a stable population .
;यह भी पूछने योग्य है कि क्या बच्चों की संया की सीमा तय करना ही जनसंया वृद्धि को रोकने का सही उपाय है .
(defrule worth1
(declare (salience 100))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  yogya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  worth.clp 	worth1   "  ?id "  yogya )" crlf))
)

;"worth","Adj","1.mUlya"
;It is worth buying the anklet for the price.
;

;$$$ Modified by Pramila(BU) on 22-03-2014
;This thing is of little worth.   ;shiksharthi
;यह वस्तु बहुत कम कीमत की है.
(defrule worth2
(declare (salience 100))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-of_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  worth.clp 	worth2   "  ?id "  mUlya )" crlf))
)


;@@@ Added by Pramila(BU) on 22-03-2014
;My watch is worth rupees 800. ;shiksharthi
;मेरी घड़ी ८०० रुपये मूल्य की है.
(defrule worth3
(declare (salience 100))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(saMKyA-saMKyA  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  worth.clp 	worth3   "  ?id "  mUlya )" crlf))
)
;"worth","N","1.mUlya"
;

;@@@ Added by Pramila(BU) on 22-03-2014
;Is the play worth seeing ?    ;shiksharthi
;क्या नाटक देखने लायक है ?
(defrule worth4
(declare (salience 100))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  worth.clp 	worth4   "  ?id "  lAyaka )" crlf))
)
;@@@ Added by Anita--28.5.2014
;Or is it respect or social recognition or individual satisfaction that decides the worth of a job?
;या फिर ऐसा है कि सामाजिक मान्यता वैयक्तिक संतोष ही नौकरी की कीमत तय करता है । 
(defrule worth5
(declare (salience 200))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?sam)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kImawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  worth.clp 	worth5   "  ?id "  kImawa )" crlf))
)
;@@@ Added by 14anu-ban-11
;It has been stated that a view of the Saptapuris in the Chaturmas is worth providing Moksha for.
;yaha bawAyA gayA hE ki chaturmas meM saptapuris kA xqRtikoNa ke lie mokRa xenA yogya hE.
(defrule worth6
(declare (salience 800))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma   ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yogya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  worth.clp 	worth6   "  ?id "  yogya )" crlf))
)

;$$$ Modified by 14anu-ban-11 on (27-01-2015)
;@@@ Added by 14anu24
;It is worth checking if there is one near you .
;आपके लिए अच्छा होगा कि आप पता लगाएऋ कि आपके पास ऐसा केंद्र है या नहीं .
(defrule worth7
(declare (salience 200))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
;(subject-subject_samAnAXikaraNa  ?id ?); Commented by 14anu-ban-11 (27-01-2015)
(subject-subject_samAnAXikaraNa  ? ?id); Added by 14anu-ban-11 (27-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA_hogA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  worth.clp 	worth7   "  ?id "  acCA_hogA )" crlf))
)


;@@@ Added by 14anu-ban-11 on (11-12-2014)
;You will need to consider how much these offers are worth to you when you are deciding whether you are getting a good deal.(deal.clp)
;यह तय करते समय कि क्या आपको सबसे उत्तम सौदा मिल रहा है या नहीं , आपको विचार करना होगा कि आपके लिए इस तरह के प्रस्तावों का क्या मूल्य है .(deal.clp)
(defrule worth8
(declare (salience 900))
(id-root ?id worth)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 offer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  worth.clp 	worth8   "  ?id "  mUlya )" crlf))
)



