;----------------------------------Default Rule------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on (16-08-2014).
;252:A complete set of these units, both the base units and derived units, is known as the system of units.  [NCERT corpus]
;मूल - मात्रकों और व्युत्पन्न मात्रकों के सम्पूर्ण समुच्चय को मात्रकों की प्रणाली (या पद्धति) कहते हैं.[NCERT corpus]
(defrule system0
(declare (salience 0))
(id-root ?id system)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id praNAlI))
(assert (id-domain_type  ?id physics))	;added 14anu-ban-01 on (11-02-2015) because i want "praNAlI" as default meaning in physics domain also.
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  system.clp  	system0   "  ?id "  physics )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  system.clp  	system0   "  ?id "  praNAlI )" crlf))
)
;--------------------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-01 on 16-08-14.
;26:For example, when Johannes Kepler (1571-1630) examined the extensive data on planetary motion collected by Tycho Brahe (1546-1601), the planetary circular orbits in heliocentric theory (sun at the center of the solar system) imagined by Nicolas Copernicus (1473 — 1543) had to be replaced by elliptical orbits to fit the data better. [NCERT corpus]
;उदाहरण के लिए, जब जोहान्नेस केप्लर (1571 - 1630) ने टाइको ब्राह (1546 - 1601) द्वारा ग्रह - गति से सम्बन्धित सङ्गृहीत किए गए विस्तृत आङ्कडों का परीक्षण किया, तो निकोलस कोपरनिकस (1473 - 1543) द्वारा कल्पित सूर्य केन्द्री सिद्धान्त (जिसके अनुसार सूर्य सौर - मंडल के केन्द्र पर स्थित है)की वृत्ताकार कक्षाओं को दीर्घवृत्तीय कक्षाओं द्वारा प्रतिस्थापित करना पडा, ताकि सङ्गृहीत आङ्कडों तथा दीर्घवृत्तीय कक्षाओं में अनुरूपता हो सके.[NCERT:changed 'parivAra' to 'maMdala']
(defrule system1
(declare (salience 1000))
(id-root ?id system)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 solar)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id maMdala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  system.clp  	system1  "  ?id "  maMdala )" crlf))
)

;$$$ Modified by Manasa (29-02-2016)
;Subsequently, the subjects of kinetic theory and statistical mechanics interpreted these quantities in terms of the properties of the molecular constituents of the bulk system.
;@@@ Added by 14anu-ban-01 on 20-08-14.
;We shall first see what the center of mass of a system of particles is and then discuss its significance.[NCERT corpus]
;पहले हम यह देखेङ्गे कि  कणों के निकाय का द्रव्यमान केन्द्र क्या है और फिर इसके महत्व पर प्रकाश डालेङ्गे. [NCERT corpus:added " कणों के निकाय का"]
(defrule system2
(declare (salience 5000))
(id-root ?id system)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 particle|constituents) ;added 'constituents' by Manasa.
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id nikAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  system.clp  	system2  "  ?id "  nikAya )" crlf))
)

;@@@ Added by 14anu-ban-01 on 20-08-14.
;For simplicity we shall start with a two particle system.[NCERT Corpus]
;सरलता की दृष्टि से हम दो कणों के निकाय से शुरुआत करेङ्गे.[NCERT Corpus]
(defrule system3
(declare (salience 1000))
(id-root ?id system)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 particle)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_viBakwi ?id1 ke))
(assert (id-wsd_word_mng ?id nikAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  system.clp     system3  "  ?id1 " ke)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  system.clp  	system3  "  ?id "  nikAya )" crlf))
)

