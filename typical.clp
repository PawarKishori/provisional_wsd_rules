
(defrule typical0
(declare (salience 5000))
(id-root ?id typical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  typical.clp 	typical0   "  ?id "  viSiRta )" crlf))
)

;$$$ Modified by 14anu-ban-07 (15-10-2014)
;It is typical of him to speak the truth.(manual translation required)
(defrule typical1
;(declare (salience 4900))     
(declare (salience 5000))	;salience increased from 4900 to 5000 by 14anu-ban-07 (15-10-2014)
(id-root ?id typical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-of_saMbanXI  ?id ?id1)		;Added by by 14anu-ban-07 (15-10-2014)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))  ;;Added by 14anu-ban-07 (15-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxarSa_rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  typical.clp 	typical1   "  ?id "  AxarSa_rUpa )" crlf))
)

;"typical","Adj","1.AxarSa_rUpa"
;It is typical of him to speak the truth.
;
;

;
;@@@ Added by 14anu-ban-07 (15-10-2014)
;Approximately all houses are made of typical rocks in these the use of dirt and stones has not been done .[tourism corpus]
;लगभग सभी घर ठेठ पत्थरों के बने हैं इनमें मिट्टी एवं ईटों का प्रयोग नहीं किया गया है । [tourism corpus]
;Their cycle is typically commercial therefore when they go from one country to the other on tourism then take more interest in business only .[tourism corpus]
;इनका दायरा ठेठ व्यापारिक होता है अतः पर्यटन पर जब यह एक देश से दूसरे देश जाते हैं तब व्यापार में ही अधिक रूचि लेते हैं ।[tourism corpus]
;It is remarkable that there was a very ancient palace here which was built in the old style with typical Katwa stones .[tourism corpus]
;उल्लेखनीय है कि यहाँ पर एक अत्यंत प्राचीन राजमहल था जो ठेठ कटवाँ पत्थरों से प्राचीन शैली में निर्मित था । [tourism corpus]
(defrule typical2
(declare (salience 5000))
(id-root ?id typical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 rock|stone|commercial)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TeTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  typical.clp 	typical2   "  ?id "  TeTa )" crlf))
)



