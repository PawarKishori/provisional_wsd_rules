;########################################################################
;#  Copyright (C) 05.07.2014 14anu21, 14anu17 
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################`
;
;@@@ Added by 14anu17 same rule added by 14anu21 on 05.07.2014 and 14anu01 with extra list food, life
;We had a simple meal of soup and bread. [oxford]
;हमने शोरबे और ब्रेड का एक सरल भोजन खाया . [Translation before adding rule]
;हमने शोरबे और ब्रेड का एक सादा भोजन खाया .
;He likes to eat simple food.[self]
;वह सरल आहार खाना पसन्द करता है . [Translation before adding rule]
;वह सादा आहार खाना पसन्द करता है . 
;He lives a simple life. [self]
;वह एक सरल जीवन रहता है . [Translation before adding rule]
;वह एक सादा जीवन रहता है .
;[Added rule live14] 
;वह एक सादा जीवन जीता है . 
(defrule simple0
(id-root ?id simple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 meal|food|life)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  simple.clp 	simple0   "  ?id "  sAxA )" crlf))
)

;@@@ Added by 14anu21 and 14anu01 on 05.07.2014
;She is a simple girl.[self]
;वह एक सरल लड़की है.[Translation before adding rule]
;वह एक साधारण लड़की है.
(defrule simple1
(id-root ?id simple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAXAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  simple.clp 	simple1   "  ?id "  sAXAraNa )" crlf))
)

;@@@ Added by 14anu17
;I'm a simple country girl.[oxford]
;मैं एक साधारण देशी लडकी हूँ .
(defrule simple2
(declare (salience 100))
(id-root ?id simple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective )
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse =(+ ?id 1) noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAXAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  simple.clp 	simple2   "  ?id "  sAXAraNa )" crlf))
)


;------------------------ Default rule -----------------------------

;@@@ Added by 14anu17
;Here are some eight simple guidelines.[oxford]
;यहाँ कुछ आठ सरल मुख्य बात् हैं . 
(defrule simple3
(declare (salience 0))
(id-root ?id simple)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  simple.clp 	simple3   "  ?id "  sarala )" crlf))
)
