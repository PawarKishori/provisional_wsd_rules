
(defrule check0
(declare (salience 5000))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bila_Barakara_jagaha_KAlI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check0 "  ?id "  " ?id1 "  bila_Barakara_jagaha_KAlI_kara)" crlf))
)


(defrule check1
(declare (salience 4900))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id checked )
(viSeRya-viSeRaNa =(+ ?id 1) ?id)
;(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jAzcA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  check.clp  check1   "  ?id "  jAzcA_huA )" crlf))
)

;$$$ Modified by 14anu-ban-03 (26-11-2014)
;Added by sheetal(3-01-10).
(defrule check200
(declare (salience 4850))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)  ;added by 14anu-ban-03 (26-11-2014)
(id-root ?id1 dacoit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  check.clp  check200  "  ?id "   roka )" crlf))
)
;The government took serious steps to check the dacoits . 

;$$$ MODIFIED by 14anu17 on 13-06-2014
;Check whether your footwear is a comfortable fit.
;जाँचिऐ की अपका जूता एक आराम से फिट है या नहीं.

(defrule check2
(declare (salience 00))         ;salience reduced by 14anu-ban-03 (28-02-2015)
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id checked )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAzca))	;changed 'word_mng' to 'root_mng' by 14anu17
;(assert (id-wsd_word_mng ?id bila_Barakara_jagaha_KAlI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  check.clp  check2  "  ?id "   jAzca )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  check.clp  check3   "  ?id "  bila_Barakara_jagaha_KAlI_kara )" crlf))
)

(defrule check3
(declare (salience 4700))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id checked )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cAraKAne_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  check.clp  check3  "  ?id "  cAraKAne_vAlA )" crlf))
)

;"checked","Adj","1.cAraKAne_vAlA"
;He was wearing a checked shirt.
;
;

;$$$ Modified by 14anu-ban-03 (28-02-2015)
(defrule check4
(declare (salience 4600))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pahuzcanA/haswAkRara_kara))  ;meaning changed from 'pahuzcanA_Ora_haswAkRara_kara' to 'pahuzcanA/haswAkRara_kara' by 14anu-ban-03 (28-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check4  "  ?id "  " ?id1 "  pahuzcanA/haswAkRara_kara  )" crlf))
)

;He checked into a cheap hotel near the station.
;vaha steSana ke pAsa eka saswe hotala meM rukA

;$$$ Modified by 14anu-ban-03 (28-02-2015)   ;running on parser 4
;He checked us in at the Taj Hotel.  [same clp]
;usane wAja hotala meM hamArA kamarA ArakRiwa kiyA.[same clp]
(defrule check5
(declare (salience 5000))  ;salience increased by 14anu-ban-03 (28-02-2015)
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)   ;commented by 14anu-ban-03 (28-02-2015)
(kriyA-at_saMbanXI ?id ?id2)  ;added by 14anu-ban-03 (28-02-2015)
(id-root ?id2 Hotel|hotel)  ;added by 14anu-ban-03 (28-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kamarA_ArakRiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check5  "  ?id "  " ?id1 "  kamarA_ArakRiwa_kara  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (28-02-2015)   ;running on parser 2
;We'll check in our luggage after having lunch. [same clp]
;hama Bojana ke bAxa apanA sAmAna le leMge. [same clp]
(defrule check6
(declare (salience 5000))  ;salience increased by 14anu-ban-03 (28-02-2015)
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)  ;added 'id2' by 14anu-ban-03 (28-02-2015)
(id-root ?id2 luggage)  ;added by 14anu-ban-03 (28-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 le_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check6  "  ?id "  " ?id1 "  le_le  )" crlf))
)


(defrule check7
(declare (salience 4300))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bila_cukAkara_hotala_CodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check7  "  ?id "  " ?id1 "  bila_cukAkara_hotala_CodZa  )" crlf))
)

;We checked out the Motel next morning.
;hamane agalI subaha hotala (bila cukAkara) CodZa xiyA
(defrule check8
(declare (salience 4200))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check8  "  ?id "  " ?id1 "  jAzca_kara  )" crlf))
)

;None of the information he gave me checked out.
;koI BI sUcanA usane muJe jAzca karake nahIM xI
(defrule check9
(declare (salience 4100))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check9  "  ?id "  " ?id1 "  jAzca_kara  )" crlf))
)

;Inspector checked up on the company && discovered that it had not been paying enough tax.
;iMspEktara ne kaMpanI kI jAzca kI Ora pAyA ki vaha pUrA tEksa nahIM Bara rahI hE

;$$$ Modified by 14anu-ban-03 (28-02-2015)   ;running on parser 2
;To check in a marriage certificate. [oald]
;एक विवाह प्रमाणपत्र में नाम दर्ज करना. [manual]
(defrule check10
(declare (salience 5000))   ;salience increased by 14anu-ban-03 (28-02-2015)
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)  ;added by 14anu-ban-03 (28-02-2015)
(id-root ?id2 certificate)  ;added by 14anu-ban-03 (28-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id meM))  ;added by 14anu-ban-03 (28-02-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nAma_xarja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  check.clp  check10  "  ?id " meM )" crlf)  ;added by 14anu-ban-03 (28-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check10  "  ?id "  " ?id1 "  nAma_xarja_kara  )" crlf))
)


(defrule check11
(declare (salience 3900))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nAma_xarja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check11  "  ?id "  " ?id1 "  nAma_xarja_kara  )" crlf))
)


(defrule check12
(declare (salience 3800))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sahI_kA_cihna_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check12  "  ?id "  " ?id1 "  sahI_kA_cihna_lagA  )" crlf))
)

(defrule check13
(declare (salience 3700))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check13  "  ?id "  " ?id1 "  jAzca_kara  )" crlf))
)


(defrule check14
(declare (salience 3600))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check14  "  ?id "  " ?id1 "  roka  )" crlf))
)


(defrule check15
(declare (salience 3500))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check15  "  ?id "  " ?id1 "  jAzca_kara  )" crlf))
)

(defrule check16
(declare (salience 3400))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parIkRaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  check.clp  check16   "  ?id "  parIkRaNa )" crlf))
)

(defrule check17
(declare (salience 3300))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sawya_kI_jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " check.clp  check17  "  ?id "  " ?id1 "  sawya_kI_jAzca_kara  )" crlf))
)


;$$$ Modified by 14anu-ban-03 (28-02-2015)
;Dams check the flow of river water. [hinkhoj]
;बाँध नदी के जल का बहाव अचानक रोकते हैं . [manual]
(defrule check18
(declare (salience 3800)) ;salience chnaged from 3200 to 3800
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)  ;added by 14anu-ban-03 (28-02-2015)
(id-root ?id1 flow)   ;added by 14anu-ban-03 (28-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acAnaka_roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  check.clp  check18   "  ?id "  acAnaka_roka )" crlf))
)

;@@@ Added by 14anu22
(defrule check20
(declare (salience 5100))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-through_saMbanXI  ?id ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  check.clp  check20 " ?id " " ?id1 " jAzca_kara)" crlf)))

;@@@ Added by 14anu22
(defrule check21
(declare (salience 5100))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAzca_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  check.clp  check21 " ?id " " ?id1 " jAzca_kara)" crlf)))


;default_sense && category=noun	jAzca	0
;"check","N","1.jAzca"
;The police had a check of the area where the murder took place.
;--"2.saMyama"
;One should keep one's temper in check.
;--"3.roka"
;Dams give checks to the flow of river water.
;--"4.mAwa{SawaraMja_ke_Kela_meM}"
;He was in check after three moves itself.
;--"5.cAraKAnA"
;He was wearing a shirt with checks.
;
;


;@@@ Added by 14anu-ban-03 (28-02-2015)
;Four soldiers were detailed to check the road for troops.[cald]
;चार सैनिकों को दलों के लिए सड़क को नियन्त्रित करने का आदेश दिया गया था . [manual]
(defrule check22
(declare (salience 5000))
(id-root ?id check)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 soldier)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyaMwriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  check.clp  check22   "  ?id "  niyaMwriwa_kara )" crlf))
)


