;##############################################################################
;#  Copyright (C) 2014-2015 Vivek (vivek17.agarwal@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by (14anu06) Vivek Agarwal on 24-06-2014
;Default Rule.....
(defrule boom0
(declare (salience 4700))
(id-root ?id boom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XamAkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  boom.clp 	boom0   "  ?id " XamAkA )" crlf))
)

;$$$ Modified by 14anu-ban-02(12-01-2015)
;I saw a field commander booming out orders.(manual)
;मैने फील्ड कमांडर को आदेश गरजते हुये देखा.(manual)
;@@@ Added by (14anu06) Vivek Agarwal on 24-06-2014
;A field commander booming out orders.
;आदेश गरजते हुय एक फील्ड कमांडर.
(defrule boom1
(declare (salience 5000))
(id-root ?id boom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)                  ;commented by 14anu-ban-02(12-01-2015)
;(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))          ;commented by 14anu-ban-02(12-01-2015)
(kriyA-upasarga  ?id ?id1)             ;added by 14anu-ban-02(12-01-2015)
(id-root ?id1 out)                     ;added by 14anu-ban-02(12-01-2015) 
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id garaja))                                      ;commented by 14anu-ban-02(12-01-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 garaja))     ;added by 14anu-ban-02(12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  boom.clp     boom1   "  ?id "  " ?id1 "  garaja )" crlf))         ;added by 14anu-ban-02(12-01-2015)
)

