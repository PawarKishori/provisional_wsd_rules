;$$$ Modified by 14anu-ban-09 on (26-08-2014) 
;Changed meanig 'xqRti-' to 'xqRti' and added example sentences 
;Even our optical system is not spared . [Parallel Corpus] 
;यहां तक कि हमारी दृष्टि प्रणाली इसके प्रभाव से अछूती नहीं रहती है .
;Optical illusion. [Parallel Corpus]
;दृष्टि भ्रम.

(defrule optical0
;(declare (salience 0000)) ;commented by 14anu-ban-09
(id-root ?id optical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xqRti))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  optical.clp 	optical0   "  ?id "  xqRti )" crlf))
)
;xqRti_saMbaMXI

;@@@ Added by 14anu-ban-09 on (26-08-2014)
;In the same way , Al Haitham was known as father optical science and Abu Musa Jabir as father of Chemistry. [Parallel corpus]
;इसी तरह से अल हैथाम को प्रकाशिकी विज्ञान का पिता और अबु मूसा जबीर को रसायन शास्त्र का पिता भी कहा जाता है ।

(defrule optical1
(declare (salience 4900))
(id-root ?id optical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 science)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAkASikI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  optical.clp 	optical1   "  ?id "   prakASikI)" crlf))
)

;@@@ Added by 14anu-ban-09 on (26-08-2014) 
;An optical microscope uses visible light to ' look ' at the system under investigation. [NCERT CORPUS]
;eka prakASIya sUkRmaxarSI xvArA kisI nikAya kI jAzca ke lie xqSya - prakASa kA upayoga kiyA jAwA hE.

(defrule optical2
(declare (salience 5000))
(id-root ?id optical)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 microscope|fiber|component) ;more constraints can be added
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakASIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  optical.clp 	optical2   "  ?id "  prakASIya )" crlf))
)

;@@@ Added by 14anu-ban-09 on (26-08-2014)
;We have already studied that the geometric center of a spherical mirror is called its pole while that of a spherical lens is called its optical center. [NCERT CORPUS] ;added by 14anu-ban-09 on (15-11-2014)
;hama pahale hI aXyayana kara cuke hEM ki golIya xarpaNa kA jyAmiwIya keMxra isakA Xruva kahalAwA hE, jabaki golIya leMsa ke jyAmiwIya keMxra ko prakASika keMxra kahawe hEM. [NCERT CORPUS]
;Every optical instrument gets dirty. [COCA]
;prawyeka prAkASika yaMwra gaMxe ho jAwe hE. [Own Manual]
;NOTE-Optical instrument.[Parallel Corpus] =>प्रकाशिक यंत्र. Meaning is taken from Parallel Corpus


(defrule optical3
(declare (salience 4800))
(id-root ?id optical)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 instrument|center|centre) ;added 'center|centre' by 14anu-ban-09 on (15-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAkASika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  optical.clp 	optical3   "  ?id "   prakASikI)" crlf))
)

;"optical","Adj","1.xqRti_saMbaMXI"
;Micro scopes && telescopes are optical insttruments.
;
;
