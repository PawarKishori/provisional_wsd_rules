;@@@ Added by 14anu-ban-10 on (15-11-2014)
;If you try to displace any ball from its equilibrium position, the spring system tries to restore the ball back to its original position. [ncert corpus] 
;yaxi Apa kisI geMxa ko apanI sAmya sWiwi se visWApiwa karane kA prayAsa kareMge wo spriMga waMwra usa geMxa ko vApasa apanI prAraMBika sWiwi meM lAne kA prayAsa karegA.[ncert corpus]
(defrule restore0
(declare (salience 100))
(id-root ?id restore)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vApasa_lA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  restore.clp 	restore0   "  ?id "  vApasa_lA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (15-11-2014)
;The restoring mechanism can be visualised by taking a model of spring-ball system shown in the Fig. 9.1.[ncert corpus]
;prawyAnayana kriyAviXi ko samaJane ke lie ciwra 9.1 meM xiKAe gae spriMga geMxa nikAya ke moYdala para vicAra kareM.[ncert corpus]
(defrule restore1
(declare (salience 200))
(id-root ?id restore)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawyAnayana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  restore.clp 	restore1   "  ?id "  prawyAnayana)" crlf))
)
