
(defrule service0
(declare (salience 5000))
(id-root ?id service)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sevA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  service.clp 	service0   "  ?id "  sevA )" crlf))
)

;"service","N","1.sevA"
;The person does the service in the office.
;--"2.viBAga"
;He is working in the Health service.
;--"3.sahAyawA"
;Her services to the state have been immense.
;--"4.vyavasWA"
;A good postal service is needed for every village.
;--"5.upAsanA/anuRtAna"
;He attended the morning service in the temple.
;--"6.saPZAI-XulAI"
;Take your car for service every 3,000 miles.
;--"7.seta/barawana"
;He bought a 30-piece dinner service.
;--"8.wAmIla"
;--"9.sarvisa{tenisa_meM}"
;Her service has improved. 
;
(defrule service1
(declare (salience 4000));changed the salience (the rule interfered with other rules) by 14anu09
(id-root ?id service)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saPZAI-XulAI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  service.clp 	service1   "  ?id "  saPZAI-XulAI_kara )" crlf))
)

;"service","V","1.saPZAI-XulAI_karanA"
;This machine has already been serviced.
;--"2.kI_ApUrwi_karanA"
;The power station is serviced by road transport.
;

;Commented this rule by 14anu-ban-01 on (29-12-2014) because the required meaning is coming from default rule service0
;@@@ Added by 14anu09[20-6-14]
;... in the service of the nation...
;...देश की सेवा में.....
;(defrule service2
;(declare (salience 4900));other senses are covered in higher saliences
;(id-root ?id service)
;(kriyA-object ?id ?)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id sevA_meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  service.clp 	service2   "  ?id "  sevA_meM 	 )" crlf))
;)

;$$$ Modified by 14anu-ban-01 on (29-12-2014)
;@@@ Added by 14anu09[20-6-14]
;I went for the servicing of my car.
;मैं मेरी गाडी की मरम्मत के लिए गया . 
;Poor servicing of engines affects all of us.[COCA]
;इंजन की घटिया मरम्मत हम सभी को परेशान/प्रभावित करती है. [self]
(defrule service3
(declare (salience 5100))
(id-root ?id service)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-word ?id1 car|bike|machine|engine|motor|train);added 'engine|motor|train' to list by 14anu-ban-01 on (29-12-2014)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id marammawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  service.clp 	service3   "  ?id "  marammawa 	 )" crlf))
)

;@@@ Added by 14anu-ban-01 on (29-12-2014)
;Like any other type of equipment it requires regular servicing.[oald]
;किसी भी अन्य उपकरण की तरह इसे भी नियमित मरम्मत की ज़रूरत होती है[self]
(defrule service4
(declare (salience 5100))
(id-root ?id service)
(viSeRya-viSeRaNa  ?id ?id1)
(id-word ?id1 regular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id marammawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  service.clp 	service4   "  ?id "  marammawa 	 )" crlf))
)
