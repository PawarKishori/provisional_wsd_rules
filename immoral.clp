;@@@ Added by 14anu-ban-06 (23-04-2015)
;An immoral person.(OALD)
;एक चरित्रहीन व्यक्ति . (manual)
(defrule immoral1
(declare (salience 2000))
(id-root ?id immoral)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cariwrahIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immoral.clp 	immoral1   "  ?id "  cariwrahIna )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (23-04-2015)
;Immoral earnings.(OALD)
;अनैतिक आचरण से कमाई .(manual)
(defrule immoral2
(declare (salience 2200))
(id-root ?id immoral)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 earnings)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anEwika_AcaraNa))
(assert  (id-wsd_viBakwi   ?id  se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immoral.clp 	immoral2   "  ?id "  anEwika_AcaraNa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  immoral.clp      immoral2   "  ?id " se )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (23-04-2015)
; An immoral act. (OALD)
;अनैतिक कार्य . (manual)
(defrule immoral0
(declare (salience 0))
(id-root ?id immoral)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anEwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immoral.clp 	immoral0   "  ?id "  anEwika )" crlf))
)
