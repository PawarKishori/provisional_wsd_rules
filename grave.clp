
;@@@ Added by 14anu-ban-05 on (19-02-2015)
;His health is grave. [COCA]
;उसका स्वास्थ्य चिंताजनक है . [manual]

(defrule grave3
(declare (salience 4901))
(id-root ?id grave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciMwAjanaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grave.clp 	grave3   "  ?id "   ciMwAjanaka)" crlf))
)

;------------------------ Default Rules ----------------------

;"grave","Adj","1.gaMBIra"
;He is in grave danger of being attacked by the terrorists.
(defrule grave0
(declare (salience 5000))
(id-root ?id grave)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaMBIra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grave.clp 	grave0   "  ?id "  gaMBIra )" crlf))
)

;"grave","N","1.kabra"
;The graves are the reminders of great men.
(defrule grave1
(declare (salience 4900))
(id-root ?id grave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kabra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grave.clp 	grave1   "  ?id "  kabra )" crlf))
)

;;@@@ Added by 14anu01
;Those final words have graven in my mind.
;
(defrule grave2
(declare (salience 4900))
(id-root ?id grave)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_hI_praBAva_dAlanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grave.clp 	grave2   "  ?id "   bahuwa_hI_praBAva_dAlanA)" crlf))
)

;LEVEL 
;Headword : grave
;
;
;Examples --
;
;"grave","Adj","1.gaMBIra"
;He is in grave danger of being attacked by the terrorists.
;
;"grave","N","1.kabra"
;The tombs have graves of the royal family.
;
;
;sUwra : gamBIra/kabra

