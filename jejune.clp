;@@@ Added by 14anu-ban-06 (11-04-2015)
;A jejune lecture by one of our professors. (cambridge)[parser no.-4]
;हमारे प्राध्यापक में से एक के द्वारा उबाऊ लेक्चर . (manual)
(defrule jejune1
(declare (salience 2000))
(id-root ?id jejune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 lecture)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ubAU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jejune.clp 	jejune1   "  ?id "  ubAU )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (11-04-2015)
;He made jejune generalizations about how all students were lazy and never did any work.(cambridge)
;उसने बचकाने व्यापकीकरण बनाए कि सभी विद्यार्थी कैसे आलसी थे और कुछ कार्य कभी नहीं करते . (manual)
(defrule jejune0
(declare (salience 0))
(id-root ?id jejune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacakAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jejune.clp 	jejune0   "  ?id "  bacakAnA )" crlf))
)
