;@@@ Added by 14anu-ban-04 (06-12-2014)
;With the use of a soft spring, we can increase the effective length of the parallel current and by using mercury, we can make the displacement of even a few mm observable very dramatically.         [NCERT-CORPUS]
;कोमल कमानी का उपयोग करके हम समान्तर विद्युत धाराओं की प्रभावी लम्बाई में वृद्धि कर सकते हैं तथा पारे (मरकरी) का उपयोग करके हम कुछ मिलीमीटर के विस्थापनों को भी प्रभावशालीढङ्ग से प्रेक्षणीय बना सकते हैं.                 [NCERT-CORPUS]
(defrule effective1
(declare (salience 20))
(id-root ?id effective)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 length)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAvI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  effective.clp 	effective1   "  ?id "  praBAvaI )" crlf))
)

;@@@ Added by 14anu-ban-04 (12-02-2015)
;The new laws will become effective next month.                     [cald]
;अगले महीने नये नियम  लागू  हो जाएँगे .                                          [self]
(defrule effective2
(declare (salience 30))
(id-root ?id effective)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 law|rule)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAgU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  effective.clp 	effective2   "  ?id "  lAgU )" crlf))
)

;----------------------- Default Rules ------------------

;@@@ Added by 14anu-ban-04 (06-12-2014)
;In Delhi a huge and effective Red Fort embellished with strong , walls, gates , minarets and arches was built in about eight years.[tourism corpus]
;दिल्ली  में  लगभग  आठ  वर्षों  में  मज़बूत  दीवारों  ,  द्वारों  ,  मीनारों  ,  तथा  प्राचीरों  से  सुसज्जित  एक  विशाल  और  प्रभावशाली  लाल  किले  का  निर्माण  किया  गया  था ।[tourism corpus]
(defrule effective0
(declare (salience 10))
(id-root ?id effective)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAvaSalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  effective.clp 	effective0   "  ?id "  praBAvaSalI )" crlf))
)
