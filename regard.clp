
(defrule regard0
(declare (salience 5000))
(id-root ?id regard)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id regarding )
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ke_bAre_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  regard.clp  	regard0   "  ?id "  ke_bAre_meM )" crlf))
)

;"regarding","Prep","1.ke_bAre_meM"
;He said so much regarding your request. 
;
(defrule regard1
(declare (salience 4900))
(id-root ?id regard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regard.clp 	regard1   "  ?id "  XyAna )" crlf))
)

(defrule regard2
(declare (salience 4800))
(id-root ?id regard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regard.clp 	regard2   "  ?id "  mAna )" crlf))
)

;"regard","VT","1.mAnanA"
;I regard his behaviour unmanly.
;--"2.Axara_karanA"
;He is very highly regarded as the best teacher in the school.
;--"3.XyAna_xenA"   
;He hardly regards my advice.
;--"4.xeKanA"
;He regarded me with some doubt.
;
;

;@@@ Added by 14anu-ban-10 on (19-11-2104)
;There can be some confusion regarding the trailing zero (s).[ncert corpus]
;anugAmI SUnya sArWaka afka hEM yA nahIM isa viRaya meM BrAnwi ho sakawI hE.[ncert corpus]
(defrule regard3
(declare (salience 5100))
(id-root ?id regard)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-regarding_saMbanXI ? ? )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isa_viRaya_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regard.clp 	regard3   "  ?id "  isa_viRaya_meM)" crlf))
)

;@@@ Added by 14anu20 on 27/06/2014.
; His advice was little regarded during his lifetime .
;उसकी सलाह उसके जीवन-काल के दौरान थोडी मान गया था 
(defrule regard4
(declare (salience 4900))
(id-root ?id regard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id regarded)
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnA_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regard.clp 	regard4   "  ?id "  mAnA_gayA)" crlf))
)

;@@@ Added by 14anu20 on 27.06.2014.
;This information is as regards the letter.
;यह सूचना पत्र के सम्ब्न्ध में है.
(defrule regard5
(declare (salience 4900))
(id-root ?id regard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(- ?id 1) as)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) ke_sambanXa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " regard.clp	regard5  "  ?id "  " (- ?id 1) "    ke_sambanXa_meM  )" crlf))
)

;$$$ Modified by 14anu-ban-06 (20-04-2015)
;@@@ Added by 14anu-ban-10 on (04-02-2015)
;Jharkhand can also be regarded as an effect of the progressive growth of industrial globalization .[tourism corpus]
;झारखंड  को  औद्योगिक  ग्लोबलाइजेशन  के  उत्तरोत्तर  विकास  का  भी  प्रभाव  माना  जा  सकता  है  ।[tourism corpus]
(defrule regard6
(declare (salience 5200))
(id-root ?id regard)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnA));meaning changed from   mAnA to mAnA_ja by 14anu-ban-10 on (12-02-2015);meaning changed from 'mAnA_ja' to 'mAnA' by 14anu-ban-06 (20-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  regard.clp 	regard6   "  ?id "  mAnA)" crlf))
);meaning changed from   mAnA to mAnA_ja by 14anu-ban-10 on (12-02-2015)
