;@@@ Added by 14anu-ban-06 (04-04-2015)
;All her life she had implicit faith in socialism. (cambridge)
;उसके जीवन भर में उसको समाजवाद पर पूर्ण विश्वास था . (manual)
(defrule implicit1
(declare (salience 2000))
(id-root ?id implicit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 trust|faith)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implicit.clp 	implicit1   "  ?id "  pUrNa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (04-04-2015)
;These assumptions are implicit in his writing. (OALD)
;ये धारणाएँ उसके लेखन में अन्तर्निहित हैं . (manual)
(defrule implicit0
(declare (salience 0))
(id-root ?id implicit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwarnihiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implicit.clp 	implicit0   "  ?id "  anwarnihiwa )" crlf))
)
