;@@@ Added by 14anu-ban-06 (18-02-2015)
;The discovery was incidental to their main research. (OALD)
;खोज उनके प्रमुख शोध से जुडी हुई थी .  (manual)
;The points you make are true, but they're incidental to the main problem. (cambridge)
;जिन्हें आपने तय किया हैं वह तथ्य सत्य हैं , परन्तु वे प्रमुख समस्या से जुडे हुए हैं . (manual)
(defrule incidental2
(declare (salience 2000))
(id-root ?id incidental)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI ?id ?id1)
(id-root ?id1 research|problem)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id judA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incidental.clp 	incidental2   "  ?id "  judA_huA )" crlf))
)

;---------------------- Default Rules -------------------
;@@@ Added by 14anu-ban-06 (18-02-2015)
;Try not to be distracted by incidental details.(cambridge)
;प्रासंगिक जानकारी के द्वारा ध्यान विचलित न करने का प्रयास कीजिए  . (manual)
(defrule incidental0
(declare (salience 0))
(id-root ?id incidental)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAsaMgika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incidental.clp 	incidental0   "  ?id "  prAsaMgika )" crlf))
)

;@@@ Added by 14anu-ban-06 (18-02-2015)
;You'll need money for incidentals such as tips and taxis.(OALD)
;आपको प्रासंगिक व्यय जैसे सुझावों और  व्यवस्था के लिए पैसे की आवश्यकता होंगी . (manual)
(defrule incidental1
(declare (salience 0))
(id-root ?id incidental)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAsaMgika_vyaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incidental.clp 	incidental1   "  ?id "  prAsaMgika_vyaya )" crlf))
)
