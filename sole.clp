;$$$ Modified by 14anu-ban-01 on (08-01-2015)
;@@@ Added by 14anu23 13/06/2014
;Buy a new sole for your shoes. 
; अपने जूतों के लिए एक नया सोल खरीदिए .
(defrule sole2
(declare (salience 5000))
(id-root ?id sole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(kriyA-object ?id ?id1) ;commented by 14anu-ban-01 on (08-01-2015)
(viSeRya-viSeRaNa ?id ?id1);added by 14anu-ban-01 on (08-01-2015)
(id-root ?id1 new|old|rubber|thin|thick|grippy|rigid|flexible|sticky|outer|sturdy|thick|heavy|rounded|flat);added by 14anu-ban-01 on (08-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sola ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sole.clp 	sole2   "  ?id "  sola )" crlf))
)

;@@@Added by 14anu-ban-01 on (08-01-2015)
;Sole is mainly eaten by people living in coastal areas.[sole.clp: changed 'wastal' to 'coastal']
;कुकुरजीभी मुख्य्तः तटीय क्षेत्रों में रहने वाले लोगों द्वारा खाई जाती है.[self]
(defrule sole3
(declare (salience 4900))
(id-root ?id sole)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject ?id1 ?id)(kriyA-object ?id1 ?id))
(id-root ?id1 eat|cut|cook)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kukurajIBI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sole.clp 	sole3   "  ?id "   kukurajIBI )" crlf))
)

;---------------- Default rules --------------------------
(defrule sole0
(declare (salience 5000))
(id-root ?id sole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekamAwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sole.clp 	sole0   "  ?id "  ekamAwra )" crlf))
)

;"sole","Adj","1.ekamAwra/akelA"
;His sole responsibility was of his parents.
;
(defrule sole1
(declare (salience 4900))
(id-root ?id sole)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEra_kI_walavA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sole.clp 	sole1   "  ?id "  pEra_kI_walavA )" crlf))
)



;"sole","N","1.pEra_kI_walavA"
;A shoe is not completed with out a sole.
;--"2.kukurajIBI"
;Sole is mainly eaten by people living in wastal areas.
;
