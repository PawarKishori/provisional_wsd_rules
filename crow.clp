;@@@ Added by 14anu-ban-03 (09-03-2015)
;The cock crows in the morning. [hinkhoj]
;मुर्गा सुबह कुकुरू कूं करता हैं . [manual]
(defrule crow2
(declare (salience 4900))
(id-root ?id crow)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 morning)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kukurU_kUM_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crow.clp 	crow2   "  ?id "  kukurU_kUM_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;"crow","N","1.kOA/kAka/kAga"
;A black bird with a harsh sound.
(defrule crow0
(declare (salience 00)) ;salience reduced by 14anu-ban-03 (09-03-2015)
(id-root ?id crow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kOA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crow.clp 	crow0   "  ?id "  kOA )" crlf))
)

;"crow","VI","1.kAzva_kAzva_karanA/kukurU_kUM_karanA"
;The cocks crowed all morning.
(defrule crow1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (09-03-2015)
(id-root ?id crow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAzva_kAzva_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  crow.clp 	crow1   "  ?id "  kAzva_kAzva_kara )" crlf))
)

;"crow","VI","1.kAzva_kAzva_karanA/kukurU_kUM_karanA"
;The cocks crowed all morning.
;--"2.SeKI_baGAranA/dIMga_hAkanA"
;Her success is not something to crow about.
;
