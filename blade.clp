;@@@Added by 14anu-ban-02 (01-04-2015)
;Not a blade of grass stirred.[oald]
;घास की एक पत्ती नहीं हिली . [self]
(defrule blade1 
(declare (salience 100)) 
(id-root ?id blade) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 grass)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pawwI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blade.clp  blade1  "  ?id "  pawwI )" crlf)) 
) 



;---------------------------default_rule -----------------------------


;@@@Added by 14anu-ban-02 (01-04-2015)
;Sentence: A packet of razor blades.[cald]
;Translation: रेज़र ब्लेडों की एक पोटली . [self]
(defrule blade0 
(declare (salience 0)) 
(id-root ?id blade) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id bleda)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blade.clp  blade0  "  ?id "  bleda )" crlf)) 
) 


