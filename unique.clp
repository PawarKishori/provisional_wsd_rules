;@@@ Added by 14anu03 on 21-june-14
;The theme of the party was somewhat unique.
;पार्टी का विषय कुछ अनोखा था . 
(defrule unique02
(declare (salience 5500))
(id-root ?id unique)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka ?id ?id1)
(id-word ?id1 somewhat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anoKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unique.clp 	unique02   "  ?id "  anoKA )" crlf))
)


;@@@ Added by Manasa-Gurukul-Arsha sodh sansthan 16-07-14
;The view of Jubliee Park is unique.(parallel corpus)
;judamI vana kI xqRtikoNa axvixIya hEM.
(defrule unique2
(declare (salience 5000))
(id-root ?id unique)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id axvixIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unique.clp 	unique2   "  ?id "  axvixIya )" crlf))
)

;$$$ Modified by 14anu-ban-03 (15-07-2014)
;Changed meaning from 'eka' to 'anoKA'
;The construction of these jain temples are unique in itself. (parallel corpa)
;ina jEna maMxirom ke nirmANa svayamm meM anoKe heM.
(defrule unique0
(id-root ?id unique)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anoKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unique.clp 	unique0   "  ?id "  anoKA )" crlf))
)

;(defrule unique1
;(declare (salience 4900))
;(id-root ?id unique)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id anupama))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  unique.clp 	unique1   "  ?id "  anupama )" crlf))
;)


;"unique","Adj","1.anupama/apUrva"
;  She speaks Hindi with a unique accent.
;
;
