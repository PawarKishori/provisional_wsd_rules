;@@@ Added by 14anu22
;she works during day.
;वह दिन के समय काम करती है.
(defrule day0
(declare (salience 5000))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  day.clp day0   "  ?id "  xina )" crlf))
)

;@@@ Added by 14anu22
;tommorrow is a day off.
;कल एक छुट्टी का दिन है.
(defrule day1
(declare (salience 5100))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off|out )
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CuttI_kA_xina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  day.clp day1 " ?id " " ?id1 "  CuttI_kA_xina)" crlf)))


;@@@ Added by 14anu22
;i went there day to day.
;मैं दिन प्रतिदिन वहा गया.
(defrule day3
(declare (salience 5100))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to|by )
(id-word =(+ ?id 2) day )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) xina_prawixina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " day.clp day3 " ?id " " (+ ?id 1) " " (+ ?id 2) " xina_prawixina)" crlf)))


;@@@ Added by 14anu22
;i worked there day long.
;मैंने वहां दिन भर काम किया.
(defrule day5
(declare (salience 5100))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) long )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) xina_Bara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  day.clp day5 " ?id " " (+ ?id 1) " xina_Bara)" crlf)))

;@@@ Added by 14anu22
;he likes to day dream.
;उसे खयाली पुलाव पकाना पसंद है.
(defrule day6
(declare (salience 5200))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 dream )
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 KayAlI_pulAva_pakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  day.clp day6 " ?id " " ?id1 " KayAlI_pulAva_pakA)" crlf)))

;@@@ Added by 14anu22
;We talked to each other at day break.
;हमने प्रात: काल में एक दूसरे से बात की.
(defrule day7
(declare (salience 5100))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 break )
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 prAwa:kAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " day.clp day7 " ?id " " ?id1 " prAwa:kAla)" crlf)))

;@@@ Added by 14anu22
;childminders provide day care.
;चाईल्ड माइंडर दिवा शिशु रक्शा प्रदान करते हैं.
(defrule day8
(declare (salience 5100))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 care)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xivA_SiSu_rakRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  day.clp day8 " ?id " " ?id1 " xivA_SiSu_rakRA)" crlf)))

;@@@ Added by 14anu22
;He was killed in the day light.
;वह दिन दहाडे मारा गया.
(defrule day9
(declare (salience 5100))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 light)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xina_xahAde))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  day.clp day9 " ?id " " ?id1 " xina_xahAde)" crlf)))

;@@@ Added by 14anu22
;He is a day boy.
;वह एक अनावासी छाट्र है.
(defrule day10
(declare (salience 5100))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 boy)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 anAvAsI_CAtra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  day.clp day10 " ?id " " ?id1 " anAvAsI_CAtra)" crlf)))

;@@@ Added by 14anu22
;I have heard about the day star.
;मैने शुक्र ग्रह के बारे मे सुना है.
(defrule day11
(declare (salience 5100))
(id-root ?id day)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 star)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Sukra_graha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  day.clp day11 " ?id " " ?id1 " Sukra_graha)" crlf)))


