;Added by sheetal
;The position we talked about is gone.
(defrule en_tam0
(declare (salience 5000))
(id-TAM ?id en)
(subject-subject_samAnAXikaraNa  ?sub ?id)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id en 0_cukA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  en_tam.clp        en_tam0  "  ?id "  0_cukA )" crlf))
)

;@@@ Added by Pramila(BU) on 17-02-2014
;Overcome by curiosity, the boy looked through the window.  ;oald
;जिज्ञासा के वशीभूत होकर , लड़के ने खिड़की से देखा.
(defrule en_tam1
(declare (salience 5000))
(id-TAM ?id en)
?mng <-(meaning_to_be_decided ?id)
(AjFArWaka_kriyA  ?id)
(kriyA-by_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id en 0_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  en_tam.clp        en_tam1  "  ?id "  0_kara )" crlf))
)


(defrule en_tam2
(declare (salience 0));Salience reduced to 0 from 4999 by 14anu-ban-02(02.09.14)
(id-TAM ?id en)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id en yA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  en_tam.clp        en_tam2  "  ?id "  yA_huA )" crlf))
)

;@@@ Added by 14anu-ban-02(2.09.14)
;The area SBAC bounded by the ellipse and the radius vectors SB and SC is larger than SBPC in (Fig. 8.1a).[ncert]
;दीर्घवृत्त तथा त्रिज्या सदिशों SB एवं SC द्वारा घेरा गया क्षेत्रफल SBPC की तुलना में अधिक है (चित्र 8.1a).[ncert]
(defrule en_tam3
(declare (salience 5000))
(id-TAM ?id en)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-E_tam-H_tam_mng ?id en yA_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_tam_mng  " ?*prov_dir* "  en_tam.clp        en_tam3  "  ?id "  yA_gayA )" crlf))
)

