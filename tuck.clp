
(defrule tuck0
(declare (salience 5000))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cunata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tuck.clp 	tuck0   "  ?id "  cunata )" crlf))
)

;"tuck","N","1.cunata"
;He made a tuck in the frock.
;--"2.miTAI"
; A school tuck shop is nearby.
;
(defrule tuck1
(declare (salience 4900))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sameta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tuck.clp 	tuck1   "  ?id "  sameta )" crlf))
)

;"tuck","V","1.sametanA"
;Tuck your skirt properly.
;--"2.cunata_dAlanA"
;The lady tucked her hair with a clip.
;--"3.surakRiwa_sWAna_para_raKanA"
;He tucked the watch in the bureau.
;--"4.ArAma_xenA"
;Tuck a blanket.
;

;@@@ Added by 14anu-ban-07 (18-03-2015) 
;There's plenty of food, so please tuck in.(cambridge)(parser no. 2)
;आहार बहुत सारा है, इसलिए कृपया  पेट भर भोजन खाए. (manual)
(defrule tuck2
(declare (salience 5000))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 peta_Bara_Bojana_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " tuck.clp	tuck2  "  ?id "  " ?id1 "  data_kara_KA  )" crlf))
)


;@@@ Added by 14anu-ban-07 (18-03-2015) 
;I tucked the children in and said goodnight.(oald)
;मैंने बच्चों को ओढा कर सुला दिया और गुड्नाइट कहा . (manual)
(defrule tuck3
(declare (salience 5100))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 oDZA_kara_sulA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " tuck.clp	tuck3  "  ?id "  " ?id1 "  oDZA_kara_sulA_xe  )" crlf))
)

;@@@ Added by 14anu-ban-07 (18-03-2015) 
;Judging by the way they tucked into their dinner, they must have been very hungry.(cambridge)(parser no. 78)
;जैसे उन्होंने  रात का  पेट भर भोजन खाया कह सकते हैं, वे अत्यन्त भूखे रहे होंगे . (manual)
(defrule tuck4
(declare (salience 5100))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 into)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 peta_Bara_Bojana_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " tuck.clp	tuck4  "  ?id "  " ?id1 "  data_kara_KA  )" crlf))
)

;@@@ Added by 14anu-ban-07 (18-03-2015)
;Grandma always kept a bit of money tucked away in case there was an emergency.(cambridge)
;ग्रैमॉ हमेशा  छिपा कर रखती है थोडा सा  पैसे  को  अगर कोई  एमर्जेंसी अाए  . (manual)
(defrule tuck5
(declare (salience 5200))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CipA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " tuck.clp	tuck5  "  ?id "  " ?id1 "  CipA_kara  )" crlf))
)

;@@@ Added by 14anu-ban-07 (18-03-2015)
;They needed to joke uncomfortably about her ability to tuck away plate after plate of food.(coca)
;वे उसकी  प्लेट के बाद प्लेट बहुत ज्यादा खाने की योग्यता के बारे में गंदे तरीके से मजाक कर रहे थे.(manual)
(defrule tuck6
(declare (salience 5300))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-after_saMbanXI  ?id ?id2)
(viSeRya-of_saMbanXI  ?id2 ?id3)
(id-root ?id3 food)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahuwa_jyAxA_KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " tuck.clp	tuck6  "  ?id "  " ?id1 "  bahuwa_jyAxA_KA  )" crlf))
)

;@@@ Added by 14anu-ban-07 (19-03-2015)
;Eventually I found the certificate tucked under a pile of old letters. (cambridge)
;अन्त में मुझे पुराने पत्रों के ढेर के नीचे सम्भाल कर रखा हुआ प्रमाणपत्र मिला . (manual)
(defrule tuck7
(declare (salience 5400))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-under_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMBAla_kara_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tuck.clp 	tuck7   "  ?id "  saMBAla_kara_raKa )" crlf))
)

;@@@ Added by 14anu-ban-07 (19-03-2015)
;Tuck your gloves in your pocket so that you don't lose them. (cambridge)
;आपकी जेब में आपके दस्ताने सम्भाल कर रखिए जिससे कि आप उनको ना खोए . (manual)
(defrule tuck8
(declare (salience 5500))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 pocket|bag)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMBAla_kara_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tuck.clp 	tuck8   "  ?id "  saMBAla_kara_raKa )" crlf))
)

;@@@ Added by 14anu-ban-07 (19-03-2015)
;She tucked a blanket around his legs. (oald)
;उसने उसकी टाँगों के चारों ओर कम्बल ओढाया .  (manual)
(defrule tuck9
(declare (salience 5600))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id oDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tuck.clp 	tuck9   "  ?id "  oDZA )" crlf))
)

;@@@ Added by 14anu-ban-07 (19-03-2015)
;Tucked down this alley are some beautiful old houses. (cambridge)
;इस पगडण्डी के नीचे की ओर  कुछ सुन्दर पुराने घर  मुश्किल से नजर आती हैं. (manual)
(defrule tuck10
(declare (salience 5700))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) down)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muSkila_se_najZara_A))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " tuck.clp	tuck10  "  ?id "  muSkila_se_najZara_A  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  tuck.clp      tuck10   "  ?id " kA )" crlf))
)

;@@@ Added by 14anu-ban-07 (19-03-2015)
;A group of tiny brick houses is tucked away behind the factory.(cambridge)
;बहुत छोटे ईंट के घरों का समूह फैक्टरी के पीछे मुश्किल से नजर आते है .(manual)
(defrule tuck11
(declare (salience 5300))
(id-root ?id tuck)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 away)
(kriyA-upasarga ?id ?id1)
(kriyA-behind_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 muSkila_se_najZara_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " tuck.clp	tuck11  "  ?id "  " ?id1 "  muSkila_se_najZara_A  )" crlf))
)


