;@@@ Added by 14anu-ban-05 on (29-01-2015)
;The aircraft was burning fiercely.	[OALD]
;वायुयान प्रचण्ड रूप से जल रही थी. 		[MANUAL]

(defrule fiercely0
(declare (salience 100))
(id-root ?id fiercely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pracaNda_rUpa_se))
;(assert  (id-wsd_viBakwi   ?id  se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fiercely.clp  	fiercely0   "  ?id "  pracaNda_rUpa_se )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  fiercely.clp  	fiercely0   "  ?id " se )" crlf)
)
)



;@@@ Added by 14anu-ban-05 on (29-01-2015)
;People like Rajshree Tondon fiercely opposed Khajuraho , it 's architecture , it 's philosophy but the philosophy of Khajuraho and its art kept leaving it 's stamp on Indian art .	[tourism]
;rAjaSrI taMdana jEse logoM ne KajurAho , usake vAswu , usake xarSana kA jamakara viroXa kiyA kinwu KajurAho kA xarSana Ora usakA Silpa BArawIya kalA para apanI CApa CodawA gayA .	[tourism]

(defrule fiercely1
(declare (salience 101))
(id-root ?id fiercely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 oppose)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jamakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fiercely.clp  	fiercely1   "  ?id "  jamakara )" crlf))
)

