;@@@ Added by 14anu-ban-04 (28-03-2015)
;They paid $2 million to the world champion to endorse their new aftershave.           [cald]
;उन्होंने अपने नये आफ्टरशेव लोशन का विज्ञापन करने के लिए विश्व विजेता को  2 मिलियन   रुपये दिए .                   [self]
(defrule endorse1      
(declare (salience 20))
(id-root ?id endorse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(kriyA-object ?id ?id2)
(id-root ?id1 pay)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA)) 
(assert (id-wsd_root_mng ?id vijFApana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  endorse.clp     endorse1  "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  endorse.clp 	endorse1  "  ?id "  vijFApana_kara)" crlf))
)


;@@@ Added by 14anu-ban-04 (28-03-2015)
;I wholeheartedly endorse his remarks.                  [oald]
;मैं पूरे दिल से उसकी टिप्पणियों का समर्थन करता हूँ .                      [self]
(defrule endorse0       
(declare (salience 10))
(id-root ?id endorse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA)) 
(assert (id-wsd_root_mng ?id samarWana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  endorse.clp     endorse0  "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  endorse.clp 	endorse0  "  ?id "  samarWana_kara)" crlf))
)
