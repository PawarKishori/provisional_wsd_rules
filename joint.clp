;$$$ Modified by 14anu-ban-06 (28-01-2015)
;The report was a joint effort. (OALD)
;रिपोर्ट एक संयुक्त प्रयास थी . (manual)
(defrule joint0
(declare (salience 5000))
(id-root ?id joint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMyukwa));meaning changed from 'sAJe_kA' to 'saMyukwa' by 14anu-ban-06 (28-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  joint.clp 	joint0   "  ?id "  saMyukwa )" crlf))
)

;"joint","Adj","1.sAJe_kA"
;The project is a joint venture between India && Russia.
;
(defrule joint1
(declare (salience 4900))
(id-root ?id joint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  joint.clp 	joint1   "  ?id "  jodZa )" crlf))
)

;"joint","N","1.jodZa{SarIra_kA}"
;She is suffering from joint pain.
;

;commented this rule to make this meaning as default meaning by 14anu-ban-06 (28-01-2015)
;@@@ Added by Prachi Rathore[15-3-14]
;the issue of a joint statement by the French and German foreign ministers.[oald]
;फ्रेन्च और जर्मन विदेशी मन्त्री के द्वारा एक संयुक्त बयान  का निर्णय . 
;(defrule joint2
;(declare (salience 5500))
;(id-root ?id joint)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)
;;(viSeRya-viSeRaNa  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id saMyukwa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  joint.clp 	joint2   "  ?id "  saMyukwa )" crlf))
;)


;@@@ Added by Prachi Rathore[15-3-14]
;Joint both the ends of the rope.[shiksharthi-kosh]
; रस्सी के दोनो सिरे जोड़ दीजिये . 
(defrule joint3
(declare (salience 500))
(id-root ?id joint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  joint.clp 	joint3   "  ?id "  joda_xe )" crlf))
)

;$$$ Modified by 14anu-ban-06 (28-01-2015)
;### [COUNTER EXAMPLE] ### On looking above Jatashankar many feel thrill and fear , the reason for it is the hanging rock fragment between two mountain joints .(parallel corpus)
;### [COUNTER EXAMPLE] ### जटाशंकर  के  ऊपर  देखने  पर  कइयों  को  रोमांच  और  भय  लगता  है  ,  इसकी  वजह  2  पर्वत  जोड़ों  के  बीच  लटका  शिलाखंड  है  ।(parallel corpus)
;@@@ Added by Prachi Rathore[15-3-14]
; A fast-food joint.
;एक फ़ास्ट फूड का अड्डा . 

(defrule joint4
(declare (salience 5500))
(id-root ?id joint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(or(viSeRya-viSeRaNa  ?id ?id1)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1));commented by 14anu-ban-06 (28-01-2015)
(viSeRya-viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id addA))
;(assert (id-H_vib_mng ?id1 kA));commented by 14anu-ban-06 (28-01-2015)
(assert  (id-wsd_viBakwi   ?id1  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  joint.clp 	joint4   "  ?id " addA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  joint.clp 	joint4   "  ?id1 "  kA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  joint.clp      joint4   "  ?id1 " kA )" crlf)
)
)

;$$$ Modified by 14anu-ban-06 (28-01-2015)
;### [COUNTER EXAMPLE] ### On looking above Jatashankar many feel thrill and fear , the reason for it is the hanging rock fragment between two mountain joints .(parallel corpus)
;### [COUNTER EXAMPLE] ### जटाशंकर  के  ऊपर  देखने  पर  कइयों  को  रोमांच  और  भय  लगता  है  ,  इसकी  वजह  2  पर्वत  जोड़ों  के  बीच  लटका  शिलाखंड  है  ।(parallel corpus)
;We had lunch at a hamburger joint and then went to see a movie. [cambridge]
;हमने एक कीमे के अड्डे पर दोपहर का खाना खाया और फिर चलचित्र  देखने के लिये गया . 
(defrule joint5
(declare (salience 5500))
(id-root ?id joint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 hamburger);added by 14anu-ban-06 (28-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id addA))
;(assert (id-H_vib_mng ?id1 kA));commented by 14anu-ban-06 (28-01-2015)
(assert  (id-wsd_viBakwi   ?id1  kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  joint.clp 	joint5   "  ?id " addA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  joint.clp 	joint5   "  ?id1 "  kA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  joint.clp      joint5   "  ?id1 " kA )" crlf)
)
)

