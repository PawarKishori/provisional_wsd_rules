
(defrule influence0
(declare (salience 5000))
(id-root ?id influence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  influence.clp 	influence0   "  ?id "  praBAva )" crlf))
)

;"influence","N","1.praBAva"
;Used her parents' influence to get the job
;Her wishes had a great influence on his thinking
;The influence of mechanical action
;She was the most important influence in my life
;
(defrule influence1
(declare (salience 4900))
(id-root ?id influence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAviwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  influence.clp 	influence1   "  ?id "  praBAviwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  influence.clp         influence1   "  ?id " ko )" crlf)
)
)

;"influence","VT","1.praBAviwa_karanA"

;@@@ Added by 14anu-ban-06 (24-11-2014)
;### [COUNTER EXAMPLE] ### The latter quantity can be influenced by external factors.(NCERT);added by 14anu-ban-06 (04-03-2015)
;### [COUNTER EXAMPLE] ### बाद वाली राशि बाह्य कारकों द्वारा प्रभावित की जा सकती है.(NCERT)
;The responses influenced by the clouds are part of a forever ongoing process .(parallel corpus)
;बादलों से प्रभावित होने वाले प्रत्युत्तर एक निरंतर चलने वाली प्रक्रिया है ।(parallel corpus)
;Synthesis of polynucleotides , even before'life'began , may have been influenced by the interaction of nucleotides with peptides .(parallel corpus)
;जीवन का आरंभ होने से भी पहले , पॉलीन्यूक्लिओटाइडों का निर्माण , न्यूक्लिओटाइडों और पेप्टाइडों की पारस्परिक क्रिया से प्रभावित हुआ होगा .
;(parallel corpus)
(defrule influence2
(declare (salience 5000))
(id-root ?id influence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 cloud|interaction);added by 14anu-ban-06 (04-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAviwa_ho))
(assert  (id-wsd_viBakwi   ?id1  se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  influence.clp 	influence2   "  ?id "  praBAviwa_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  influence.clp      influence2   "  ?id1 " se )" crlf)
)
)

