;$$$Modified by 14anu-ban-08 (09-04-2015)     ;commented relation, added relation
;I am sure that somebody is listening in our conversation.  [same clp file]
;मुझे यकीन है कि कोई हमारी बातों को चोरी छुपे सुन रहा हैं.  [same clp file]
(defrule listen0
(declare (salience 5000))
(id-root ?id listen)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 in)                 ;commented by 14anu-ban-08 (09-04-2015)
;(kriyA-upasarga ?id ?id1)           ;commented by 14anu-ban-08 (09-04-2015)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)           ;added by 14anu-ban-08 (09-04-2015)
(id-root ?id1 conversation)             ;added by 14anu-ban-08 (09-04-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 corI-Cupe_suna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " listen.clp	listen0  "  ?id "  " ?id1 "  corI-Cupe_suna  )" crlf))
)

;I am sure that somebody is listening in our conversation.
;muJe yakZIna hE ki koI hamArI bAwoM ko corI-Cipe suna rahA hE
(defrule listen1
(declare (salience 4900))
(id-root ?id listen)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  listen.clp 	listen1   "  ?id "  suna )" crlf))
)

;Remove by 14anu-ban-08 (09-04-2015) beacuse hindi meaning is inapproriate according to hindi sentence, and it fires from listen1  
;@@@--- Added by Nandini(1-5-14)
;None of this would have happened if you'd listened to me.[OLAD]
;yaha kuCa BI nahIM huA howA yaxi Apa muJe XyAna_se suneMge.
;(defrule listen2
;(declare (salience 4950))
;(id-root ?id listen)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-to_saMbanXI  ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id XyAna_se_suna))   
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  listen.clp 	listen2   "  ?id "  XyAna_se_suna )" crlf))      
;)

;$$$Modified by 14anu-ban-08 (09-04-2015)       ;changed meaning
;@@@--- Added by Nandini(1-5-14)
;Can you listen out for the doorbell? [OLAD]
;kyA Apa xvAraGaNtI ke AvAja_para_kAna_xe sakawe hEM?
;क्या आप द्वारघण्टी की आवाज पर ध्यान दे सकते हैं.  [self]
(defrule listen3
(declare (salience 5000))
(id-root ?id listen)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AvAja_para_XyAna_xe))   ;changed meaning from 'AvAja_para_kAna_xe' to 'AvAja_para_XyAna_xe' by 14anu-ban-08 (09-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " listen.clp	listen3  "  ?id "  " ?id1 "  AvAja_para_XyAna_xe  )" crlf))   ;changed meaning from 'AvAja_para_kAna_xe' to 'AvAja_para_XyAna_xe' by 14anu-ban-08 (09-04-2015)
)


;"listen","V","1.sunanA"
;Please speak a bit loudly. I am not able to listen anything.
;Why don' you listen to me?
;(kriyA-to_saMbanXI  10 12)
;
