;@@@ Added by 14anu09 [10-6-14]
;His inner circle of friends was small.
;उसके दोस्तों का अन्दरूनी दल छोटा था . 
(defrule inner2
(declare (salience 6000));increased salience from '5000' to avoid clsh with 'inner0' by 14anu-ban-06 (15-12-2014)
(id-root ?id inner)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-word ?id1 circle)
(viSeRya-RaRTI_viSeRaNa ?id1 ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id anxarUnI_xala))
;(assert (kriyA_id-object_viBakwi ?id  me))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " inner.clp    inner2        "  ?id1 "  " ?id "  anxarUnI_xala )" crlf ))
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  .clp    ;inner2   "  ?id " me )" crlf)
)

;@@@ Added by 14anu-ban-06 (21-02-2015)
;Sarah seemed to have a profound sense of inner peace. (cambridge)
;सरहा में अन्दरूनी शांति की एक गहरी संवेदना प्रतीत होती हैं . (manual)
(defrule inner3
(declare (salience 5100))
(id-root ?id inner)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 peace|feeling)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anxarUnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inner.clp 	inner3   "  ?id "  anxarUnI )" crlf))
)
;-------------------- Default rules ------------------------------

(defrule inner0
(declare (salience 5000))
(id-root ?id inner)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BIwarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inner.clp 	inner0   "  ?id "  BIwarI )" crlf))
)

(defrule inner1
(declare (salience 4900))
(id-root ?id inner)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BIwarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inner.clp 	inner1   "  ?id "  BIwarI )" crlf))
)

;"inner","Adj","1.BIwarI"
;They decorated the inner portion of their rooms.
;--"2.Awmika[BAvanA]"
;Mozart's music touches one's inner feelings.
;
;
