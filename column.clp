;##############################################################################
;#  Copyright (C) 2014-2015 Gurleen Bhakna (gurleensingh@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-03 (12-12-2014)
;@@@ Added by 14anu05 GURLEEN BHAKNA on 13.06.14
;The temple is supported by marble columns.
;मन्दिर सङ्गमरमर स्तम्भों से सहारा दिया गया है.
;मन्दिर को सङ्गमरमर स्तम्भों से सहारा दिया गया है. [translation given by 14anu-ban-03 (12-12-2014)]
(defrule column0
(declare (salience 3000))
(id-root ?id column)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 support|held|stay|stand)  ;commented by 14anu-ban-03 (12-12-2014)
;(kriyA-by_saMbanXI ?id1 ?id) ;commented by 14anu-ban-03 (12-12-2014)
;(id-cat_coarse ?id1 verb)  ;commented by 14anu-ban-03 (12-12-2014)
(id-cat_coarse ?id noun)  ;added by 14anu-ban-03 (12-12-2014)
=> 
(retract ?mng)
(assert (id-wsd_root_mng ?id swamBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  column.clp 	column0   "  ?id "  swamBa )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 13.06.14
;Put a mark in the appropriate column.
;उचित पंक्ति में निशान डालिए.
(defrule column1
(declare (salience 3000))
(id-root ?id column)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 put|read|stand)
(kriyA-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paMkwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  column.clp 	column1   "  ?id "  paMkwi )" crlf))
)
