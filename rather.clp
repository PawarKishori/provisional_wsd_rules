;@@@Added by 14anu24
;Much of the glucose stays in the bloodstream , rather than being metabolised or stored , and the body does not get all the energy that it should .
;इसके पलसस्वरूप अधिक ग्लूकोज कोशिकाओं द्वारा इस्तेमाल होने या संग्रहित होने की बजाय रक्त में ही रहता है तथा शरीर को उतनी उर्जा नहीं मिल पाती जितनी उसे मिलनी चाहिए .
(defrule rather_tmp
(declare (salience 5500))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse =(+ ?id 1) adverb)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bajAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp   rather_tmp   "  ?id "  bajAya )" crlf))
)

(defrule rather0
(declare (salience 5000))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather0   "  ?id "  kAPI )" crlf))
)

(defrule rather1
(declare (salience 4900))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather1   "  ?id "  kAPI )" crlf))
)

(defrule rather2
(declare (salience 4800))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather2   "  ?id "  kAPI )" crlf))
)

(defrule rather3
(declare (salience 4700))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(+ ?id 1) noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather3   "  ?id "  kAPI )" crlf))
)

(defrule rather4
(declare (salience 4600))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 than)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather4   "  ?id "  kAPI )" crlf))
)

(defrule rather5
(declare (salience 4500))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 too)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather5   "  ?id "  kAPI )" crlf))
)

;NOTE: following rule needs to be restricted : added by 14anu-ban-01 on (15-04-2016) but I didn't get the appropriate logic to do so.
;@@@ Added by 14anu-ban-10 on (07-02-2015)
;Even the lion , the king of jungle , considers it rather safe to parry away from a fully grown black rhinoceros .[tourism corpus]
;एक पूर्ण विकसित गैंडे से जंगल का राजा शेर भी  बल्कि सुरक्षित  बचकर निकलने में भलाई समझता है .[self]
(defrule rather7
(declare (salience 5600))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka  ? ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id balki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather7   "  ?id "  balki )" crlf))
)

;@@@ Added by 14anu-ban-01 on (15-04-2016)
;He commented that the two essays were rather similar.	[sd_verified]
;उसने टिप्पणी की कि दोनों निबन्ध काफी मात्रा में समान थे.	[Translated by Chaitanya Sir]
(defrule rather8
(declare (salience 5600))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka ?id1 ?id)
(kriyA-vAkyakarma  2 9)
(subject-subject_samAnAXikaraNa  ?id2 ?id1)
(kriyA-subject  ? ?id2)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAPI_mAwrA_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather8   "  ?id "  kAPI_mAwrA_meM )" crlf))
)


;------------------------- Default Rules -----------------

(defrule rather6
(declare (salience 4400))
(id-root ?id rather)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id balki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rather.clp 	rather6   "  ?id "  balki )" crlf))
)

;"rather","Adv","1.balki"
;He came very late last night or rather in the wee hours this morning.
;My bat is rather more expensive than his.   
;This science book is rather too easy for class seven.
;It's rather a shame that she missed the concert.   
;
;'rather' behaves like an adverb.
;(Should be added to the list of adverbs)
;----------------------------------------------------------------------
;suwra  balki^kuCa_haxa_waka
;rather  balki
;He came very late last night or rather in the wee hours this morning.
;However, in the following sentences 'rather  balki' is not good.
;jaba BI 'rather' kA prayoga howA hE, generally background meM kahIM
;'balki' hE.
;uxA:
;A: He resembles his mother.
;B: No, He looks rather like his father.
;
;My bat is rather more expensive than his.
;This science book is rather too easy for class seven.
;It's rather a shame that she missed the concert.
;I would rather go.
