;@@@ Added by 14anu-ban-03 (20-02-2015)
;A squat chunky man. [oald]
;एक गोल-मटोल तगड़ा आदमी . [anusaaraka]
(defrule chunky1
(declare (salience 100))
(id-root ?id chunky)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wagadA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chunky.clp 	chunky1   "  ?id "  wagadA )" crlf))
)

;@@@ Added by 14anu-ban-03 (20-02-2015)
;The chunky pieces of uncooked dough.[hinkhoj]
;कच्चे आटे के गठीले टुकडे. [manual]
(defrule chunky2
(declare (salience 200))
(id-root ?id chunky)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 piece)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaTIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chunky.clp 	chunky2   "  ?id "  gaTIlA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (20-02-2015)
;A chunky gold bracelet. [oald]
;एक मोटा सोने का कंगन .  [manual]
(defrule chunky0
(declare (salience 00))
(id-root ?id chunky)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id motA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chunky.clp   chunky0   "  ?id "  motA )" crlf))
)

