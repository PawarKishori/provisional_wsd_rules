
(defrule thin0
(declare (salience 5000))
(id-root ?id thin)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thin.clp 	thin0   "  ?id "  pawalA )" crlf))
)

;"thin","Adj","1.pawalA"
;The artist gave a  thin coat of colour to his painting.
;--"2.xubalA_pawalA"
;He is thin && emaciated.
;--"3.kRINa"
;We heard a thin feeble cry.
;--"4.virala"
;Air is thin at high altitudes.
;--"5.kama"
;She has a thin beard.
;
(defrule thin1
(declare (salience 4900))
(id-root ?id thin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawalA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thin.clp 	thin1   "  ?id "  pawalA_ho )" crlf))
)

;"thin","VI","1.pawalA_honA"
;He has thinned down a lot.
;
(defrule thin2
(declare (salience 4800))
(id-root ?id thin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawalA_yA_kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thin.clp 	thin2   "  ?id "  pawalA_yA_kama_kara )" crlf))
)

;"thin","VT","1.pawalA_yA_kama_karanA"
;The silver foil is thinned by thrashing constantly.
;

;@@@ Added by 14anu-ban-07,(04-03-2015)
;He gave a thin smile.(oald)
;उसने एक झूठी मुस्कराहट दी . (manual)
(defrule thin3
(declare (salience 5100))
(id-root ?id thin)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 smile)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JUTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thin.clp 	thin3   "  ?id "  JUTA )" crlf))
)

;@@@ Added by 14anu-ban-07,(04-03-2015)
;Their arguments all sound a little thin to me.(oald)
;उनकी बहस मुझे  थोडी कमजोर लगती हैं . (manual)
(defrule thin4
(declare (salience 5200))
(id-root ?id thin)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 argument)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajZora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  thin.clp 	thin4   "  ?id "  kamajZora )" crlf))
)

;@@@ Added by 14anu-ban-07,(04-03-2015)
;The traffic will thin out after the rush hour.(cambridge)
;यातायात भीड के समय के बाद कम होगा . (manual)
(defrule thin5
(declare (salience 5000))
(id-root ?id thin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " thin.clp	thin5  "  ?id "  " ?id1 "  kama_ho )" crlf))
)


;@@@ Added by 14anu-ban-07,(04-03-2015)
;He's thinned down a lot since I last saw him.(cambridge)
;मेरे पिछली बार उससे मिला के उपरान्त से वह बहुत पतला हो गया है . (manual)
(defrule thin6
(declare (salience 5000))
(id-root ?id thin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 down)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pawalA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " thin.clp	thin6  "  ?id "  " ?id1 "  pawalA_ho )" crlf))
)
