


;Added by Meena(2.3.10)
;Clinton announced on Tuesday a bold new proposal .
(defrule bold0
(declare (salience 5000))
(id-root ?id bold)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 proposal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bold.clp      bold0   "  ?id "  nirBIka )" crlf))
)



;Salience reduced by Meena((2.3.10)
(defrule bold1
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id bold)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nidara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bold.clp 	bold1   "  ?id "  nidara )" crlf))
)

(defrule bold2
(declare (salience 4900))
(id-root ?id bold)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabaMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bold.clp 	bold2   "  ?id "  xabaMga )" crlf))
)

;"bold","Adj","1.xabaMga"
;Bold settlers on some foreign shore
;--"2.spaRta"
;Bold handwriting
;
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_bold0
(declare (salience 5000))
(id-root ?id bold)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 proposal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bold.clp   sub_samA_bold0   "   ?id " nirBIka )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_bold0
(declare (salience 5000))
(id-root ?id bold)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 proposal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirBIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bold.clp   obj_samA_bold0   "   ?id " nirBIka )" crlf))
)
;@@@Added by 14anu-ban-02(04-04-2015)
;It was a bold move on their part to open a business in France.[oald]
;फ्रांस में उद्योग प्रारम्भ करना उनकी तरफ से  साहसिक प्रयास था . [self]
(defrule bold3
(declare (salience 5000))
(id-root ?id bold)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 move)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAhasika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bold.clp      bold3   "  ?id " sAhasika )" crlf))
)

;@@@Added by 14anu-ban-02(04-04-2015)
;The bold outline of a mountain against the sky.[oald]
;आसमान के सम्मुख पर्वत की मोटी पट्टी. [self]
(defrule bold4
(declare (salience 5000))
(id-root ?id bold)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 outline|type|stroke)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id motA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bold.clp      bold4   "  ?id " motA )" crlf))
)

;@@@Added by 14anu-ban-02(04-04-2015)
;A bold thinker.[mw]
;स्वछन्द विचारक .  [self]
(defrule bold5
(declare (salience 5000))
(id-root ?id bold)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 thinker)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svaCanxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bold.clp      bold5  "  ?id " svaCanxa )" crlf))
)


