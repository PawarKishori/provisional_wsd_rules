

;Added by Meena(24.4.10)
;He gave up his lucrative law practice for the sake of the country .
(defrule law_practice1
(declare (salience 4900))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 law)
(or (samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1) (viSeRya-viSeRaNa ?id ?id1));modified for OL by Sukhada(6-9-10)

=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vakAlawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " practice.clp  law_practice1  "  ?id "  " ?id1 "  vakAlawa  )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;A review of pay and working practices. [OALD]
;वेतन और सञ्चालन चलन का पुनरवलोकन . 
(defrule practice2
(declare (salience 4800))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-into_saMbanXI  ? ?id)(viSeRya-of_saMbanXI  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice2   "  ?id "  calana )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;We need a bit more practice before the concert. [Cambridge]
;हमें सङ्गीत गोष्ठी से पहले - थोडा सा अधिक अभ्यास की आवश्यकता है . 
(defrule practice3
(declare (salience 4700))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(and(viSeRya-viSeRaNa  ?id ?)(kriyA-object  ? ?id))(and(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?)(kriyA-object_1  ? ?id))(and(viSeRya-viSeRaNa  ?id ?)(viSeRya-det_viSeRaNa  ?id ?)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?)(kriyA-kAlavAcI  ? ?id)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aByAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice3   "  ?id "  aByAsa )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;Guidelines for good practice.[OALD]
;अच्छी कार्यप्रणाली के लिये दिशा निर्देश.
(defrule practice4
(declare (salience 4600))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word ?id1 guidelines|guideline)
(and(viSeRya-viSeRaNa  ?id ?)(viSeRya-for_saMbanXI  ?id1 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryapraNAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice4   "  ?id "  kAryapraNAlI )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
(defrule practice5
(declare (salience 4500))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(and(viSeRya-viSeRaNa  ?id ?)(viSeRya-det_viSeRaNa  ?id ?)(viSeRya-of_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paramparA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice5   "  ?id "  paramparA )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;The German practice of giving workers a say in how their company is run. [OALD]
(defrule practice6
(declare (salience 4400))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 pronoun)
(and(saMjFA-to_kqxanwa  ?id ?)(viSeRya-RaRTI_viSeRaNa  ?id ?id1)(subject-subject_samAnAXikaraNa  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Axawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice6   "  ?id "  Axawa )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;The practice of medicine. [OALD]
;चिकित्सकीय पेशा.
(defrule practice7
(declare (salience 4300))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(and(viSeRya-det_viSeRaNa  ?id ?)(viSeRya-of_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id cikiwsakIya_peSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice7   "  ?id "  cikiwsakIya_peSA )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
(defrule practice8
(declare (salience 4200))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 noun)
(id-word ?id1 solicitor|barrister|docter|lawyer|veterinary)
(and(kriyA-subject  ?id2 ?id1)(kriyA-in_saMbanXI  ?id2 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice8   "  ?id "  peSe )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
; Have you been practicing your lines for the play? [MW]
;क्या आप खेल के लिए आपकी लाइनों का अभ्यास कर रहे हैं? 
(defrule practice9
(declare (salience 4100))
(id-word ?id practicing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(and(kriyA-object  ?id ?)(kriyA-subject  ?id ?)(kriyA-for_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aByAsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice9   "  ?id "  aByAsa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-09-2014)
;In practice, it has a large number of useful applications and can help explain a wide variety of phenomena for low viscosity incompressible fluids. [NCERT CORPUS]
;वास्तव में इसके कई उपयोग हैं जो कम श्यानता तथा असंपीड्य तरलों की बहुत सी घटनाओं की व्याख्या कर सकते हैं.
(defrule practice10
(declare (salience 5000))
(id-root ?id practice) ;changed 'practicing' to 'practice' and 'id-word' to 'id-root' by 14anu-ban-09 (20-11-2014)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ? ?id)
(id-root =(- ?id 1) in) ;changed 'id-word' to 'id-root' by 14anu-ban-09 (20-11-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) vAswava_meM ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " practice.clp practice10  "  ?id "  " (- ?id 1) "  vAswava_meM  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-11-2014)
;Thus the practice of calculating the fertilizer equivalent value of the nutrients in crop residue is a reasonable guide to estimating the partial value of crop residues. [Agriculture] 
;इस प्रकार फसल अवशिष्ट में पोषक तत्वों के उर्वरक तुल्य मूल्य की जाँच करने की कार्यप्रणाली है जो फसल अवशिष्टों की असामान्य मूल्य को मूल्यांकन करने के लिए एक उचित मार्गदर्शक गाइड हैं. [Self] 
;Crop rotation is the practice of growing a series of dissimilar/different types of crops in the same area in sequential seasons. [Agriculture]
;सस्यावर्तन/फसल रोटेशन एक   भिन्न / विभिन्न प्रकार की फसलों की श्रृंखला  को एक ही क्षेत्र में अनुक्रमिक मौसम में उपजाने का कार्यप्रणाली है|[Manual]    
(defrule practice11
(declare (salience 4600))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 grow|calculate) ;added 'calculate' by 14anu-ban-09 on (20-11-2014)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryapraNAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice11   "  ?id "  kAryapraNAlI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-11-2014)
;Such management practices that succeed in retaining suitable soil cover in areas under fallow will ultimately reduce soil loss. [Agriculture]
;इस तरह के प्रबंधन की कार्यप्रणाली जो कि बंजर अधीन क्षेत्रों में मृदा आवरण बनाए रखने में सफल होते हैं,अन्त में मृदा नुकसान को कम करेंगें. [Self]
(defrule practice12
(declare (salience 4600))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 management)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryapraNAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice12   "  ?id "  kAryapraNAlI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-03-2015)
;The team is practicing for their big game on Friday.	[oald]
;दल शुक्रवार को उनके बडे खेल के लिए अभ्यास कर रहा है . 		[self]

(defrule practice13
(declare (salience 4100))
(id-word ?id practicing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 game|dance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aByAsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice13   "  ?id "  aByAsa_kara )" crlf))
)


;@@@ Added by 14anu-ban-09 on (09-03-2015)
;They practiced the dance until it was perfect.		[oald]
;उन्होंने नाच का अभ्यास किया जब तक वह उत्तम नहीं हो गया था .  		[self]

(defrule practice14
(declare (salience 4100))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 game|dance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aByAsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice14   "  ?id "  aByAsa_kara )" crlf))
)


;@@@ Added by 14anu-ban-09 on (09-03-2015)
;She's practicing medicine in Philadelphia. 		[oald]
;वह फिलडेल्फ़ीअ में  चिकित्सा पद्धति की डाक्टरी कर रही है . 		[self]

(defrule practice15
(declare (salience 4100))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 medicine)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAktarI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice15   "  ?id "  dAktarI_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (31-03-2015)
;It was time to put their suggestion into practice. [oald]
;यह उनके सुझाव को व्यवहार में लाने का समय था  . 		         [Manual]

(defrule practice16
(declare (salience 4801))
(id-root ?id practice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-into_saMbanXI  ?id1 ?id)
(id-root ?id1 put)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  practice.clp 	practice16   "  ?id "  vyavahAra )" crlf))
)

