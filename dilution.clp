;##############################################################################
;#  Copyright (C) 2014-2015 Vivek (vivek17.agarwal@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by 14anu06 
(defrule dilution1			;Default Rule(added after the original clp file was commited)
(declare (salience 4000))
(id-root ?id dilution)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jala_miSraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " dilution.clp dilution1 "  ?id " jala_miSraNa  )" crlf))
)

;$$$ Modified by 14anu-ban-04 (06-01-2015)
;@@@ Added by 14anu06 on 11/6/2014
;The dilution of government stakes in the scheme was unexpected.
;योजना में सरकारी खूँटों का कम करना अनपेक्षित था . 

(defrule dilution0
(declare (salience 5000))
(id-root ?id dilution)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 share|stock|stake)			;added by 14anu-ban-04 (06-01-2015)
;(id-word ?id1 share|stock|stocks|shares|stake|stakes)  ;commented by 14anu-ban-04 (06-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_karanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " dilution.clp dilution0 "  ?id " kama_karanA  )" crlf))
)
