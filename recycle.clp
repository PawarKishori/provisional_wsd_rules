;@@@ Added by 14anu-ban-04 (14-04-2015)
;The author recycles a familiar story in her latest novel.          [merriam-webster]
;लेखक अपने सबसे अधिक नवीनतम उपन्यास में परिचित कहानी की पुनरावृत्ति करता है .             [self]
(defrule recycle1
(declare (salience 30))
(id-root ?id recycle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 idea|method|joke|story)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI))
(assert (id-wsd_root_mng ?id punarAvqwwi_kara))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  recycle.clp     recycle1   "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " recycle.clp 	recycle1  "  ?id "  punarAvqwwi_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (14-04-2015)
;Don't forget to recycle the empties.             [cald]
;खाली डिब्बों का पुनर्चक्रण करना मत भूलिए .                    [self]
(defrule recycle2
(declare (salience 20))
(id-root ?id recycle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id punarcakraNa_kara))             
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  recycle.clp     recycle2   "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " recycle.clp 	recycle2  "  ?id " punarcakraNa_kara )" crlf))
)


;------------------------------------------ Default Rules --------------------------------------------

;@@@ Added by 14anu-ban-04 (14-04-2015)
;Metal, paper and glass can be recycled.                 [cald]
;धातु ,कागज और काँच का पुनर्चक्रण किया जा सकता है .                  [self]
(defrule recycle0
(declare (salience 10))
(id-root ?id recycle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id punarcakraNa_kara))        
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  recycle.clp     recycle0   "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " recycle.clp 	recycle0 "  ?id " punarcakraNa_kara )" crlf))
)

