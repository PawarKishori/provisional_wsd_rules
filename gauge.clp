;@@@ Added by (14anu06) Vivek Agarwal, MNNIT Allahabad on 28/6/2014*****
;This is a gauge of character.
;यह स्वभाव का मानदण्ड है . 
(defrule gauge2
(declare (salience 5000))
(id-root ?id gauge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnaxaMda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gauge.clp 	gauge2   "  ?id "  mAnaxaMda )" crlf))
)

;@@@ Added by 14anu-ban-10 (26-7-14)
;Diu is attached with metre gauge railway line .
;xIva mItara geja relave lAina ke sAwa judAz huA hE.
(defrule gauge3
(declare (salience 5000))
(id-root ?id gauge)
?mng <-(meaning_to_be_decided ?id)
(or(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)(viSeRya-viSeRaNa ?id ?id1))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id geja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gauge.clp 	gauge3  "  ?id " geja)" crlf))
)

;------------------------- Default rules --------------------------------
(defrule gauge0
(declare (salience 500)) ;salience reduced from 5000 to 500 by Vivek Agarwal
(id-root ?id gauge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gauge.clp 	gauge0   "  ?id "  mApa )" crlf))
)

;"gauge","N","1.mApa"
;A new broad guage line is being laid here.
;
(defrule gauge1
(declare (salience 4900))
(id-root ?id gauge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gauge.clp 	gauge1   "  ?id "  nApa )" crlf))
)

;"gauge","VT","1.nApanA"
;There is a new equipment to guage the speed of the bowler's delivery.
;
