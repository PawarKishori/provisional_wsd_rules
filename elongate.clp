;@@@ Added by 14anu-ban-04 (11-04-2015)
;New medical procedures have elongated the careers of many athletes.                 [merriam-webster] 
;नयी आयुर्विज्ञान सम्बन्धी कार्यविधि बहुत से व्यायामियों की जीविका को बढ़ा चुकीं हैं .                               [self]
(defrule elongate1
(declare (salience 20))
(id-root ?id elongate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id baDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  elongate.clp     elongate1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  elongate.clp  elongate1   "  ?id "  baDA)" crlf))
)

;--------------------------------------------------------DEFAULT RULE ----------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (11-04-2015)
;The cells elongate as they take in water.             [cald]
;कोशिकाएँ बढ़ती हैं जब वे पानी में डाली जाती हैं .                            [self]
(defrule elongate0
(declare (salience 10))
(id-root ?id elongate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  elongate.clp  elongate0   "  ?id "  baDa)" crlf))
)

