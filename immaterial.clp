;@@@ Added by 14anu-ban-06 (31-03-2015)
;The immaterial mind.  (OALD)
;अभौतिक मन .  (manual)
;The immaterial soul.  (OALD)
;अभौतिक आत्मा . (manual)
(defrule immaterial1
(declare (salience 2000))
(id-root ?id immaterial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 mind|soul)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBOwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immaterial.clp 	immaterial1   "  ?id "  aBOwika )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (31-03-2015)
;These facts are immaterial to the problem. (OALD)
;ये तथ्य समस्या के लिए महत्वहीन हैं . (manual)
(defrule immaterial0
(declare (salience 0))
(id-root ?id immaterial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawvahIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immaterial.clp 	immaterial0   "  ?id "  mahawvahIna )" crlf))
)

