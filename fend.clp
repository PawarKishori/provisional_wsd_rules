
;@@@Added by 14anu18 (24-06-14)
;She left her 14-year-old daughter to fend for herself.
;उसने अपनी14 वर्षीय बेटी को खुद के लिए प्रबंध करने के लिए  छोड़ दिया.
(defrule fend0
(declare (salience 5000))
(id-root ?id fend)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?)
(id-cat_coarse ?id verb)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prbaMXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fend.clp 	fend0   "  ?id "   prbaMXa_kara )" crlf))
)


;@@@Added by 14anu18 (24-06-14)
;He fended off the awkward questions. 
;उसने अजीब प्रश्न रोके . 
(defrule fend1
(declare (salience 5000))
(id-root ?id fend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fend.clp	fend1  "  ?id "  " ?id1 "  roka  )" crlf))
)

;Default rule.
;@@@Added by 14anu18 (24-06-14)
(defrule fend2
(declare (salience 500))
(id-root ?id fend)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fend.clp 	fend2   "  ?id "   bacAna )" crlf))
)

