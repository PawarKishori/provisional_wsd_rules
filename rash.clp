;@@@ Added by 14anu-ban-10 on (04-03-2015)
;A rash of bank robberies.[hinkhoj]
;बैंक डकैतियों का ददोरा . [manual]
(defrule rash2
(declare (salience 5100))
(id-root ?id rash)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaxorA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rash.clp 	rash2   "  ?id "  xaxorA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (04-03-2015)
;The recent rash of strikes in the railways caused great inconvienence.[hinkhoj]
;रेलवे में स्ट्राइक के हाल ही मे हुए  आकस्मिक हड़तालों का सिलसिला  बडे असुविधा का कारण बना . [manual]
(defrule rash3
(declare (salience 5200))
(id-root ?id rash)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 strike)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akasimaka_hadaZwAlo_kA_silasilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rash.clp 	rash3  "  ?id "  Akasimaka_hadaZwAlo_kA_silasilA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (04-03-2015)
;This is what happens when you make rash decisions.[oald]
;यह है  जो होता है जब आप  अपरिणामदर्शी निर्णय बनाते हैं . [manual]
(defrule rash4
(declare (salience 5300))
(id-root ?id rash)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apariNAmaxarSI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rash.clp 	rash4  "  ?id "  apariNAmaxarSI)" crlf))
)

;------------------------ Default Rules ----------------------

;"rash","Adj","1.awiSIGra"
;She shouldn't make rash promises.
(defrule rash0
(declare (salience 5000))
(id-root ?id rash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awiSIGra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rash.clp 	rash0   "  ?id "  awiSIGra )" crlf))
)


;$$$ Modified by 14anu-ban-04 (02-04-2015)      ;-------spelling corrected from 'PunsI' to 'PuMsI'
;A rash had erupted all over his chest.                          [oald]
;उसकी पूरी  छाती पर  फुंसी  निकल आई थी.                                  [self]
;"rash","N","1.PuMsI"
;Too much oily food brought her out in red itchy rashes.
(defrule rash1
(declare (salience 4900))                      ;salience decreased from '4900' to '10' by 14anu-ban-04 (02-04-2015)
(id-root ?id rash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PuMsI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rash.clp 	rash1   "  ?id "  PuMsI )" crlf))
)

;"rash","N","1.PuMsI"
;Too much oily food brought her out in red itchy rashes.
;--"2.Akasmika_hadZawAloM_kA_silasilA"
;The recent rash of strikes in the railways caused great inconvienence.  
;
