
;$$$ Modified by 14anu-ban-05 on (27-02-2015) (added "beaches" to word fact)
;The island has long golden beaches fringed by palm trees.    [cald]
;द्वीप  पर बड़े सुनहरे समुद्रतट के किनारे के निकट ताड़ के पेड़ लगे है .     [self]
;$$$ Modified by 14anu-ban-06 Karanveer Kaur (25-7-14) added moments to word fact)
;Unpolished , a conch shell picked up from the bank of Lake Manyara is still present with me in its original form and reminds me of those golden moments .
;बिना पॉलिश किया हुआ , लेक मनयारा के तट से लिया गया एक शंख अपने मूलरूप में आज भी मेरे पास विद्यमान है और उन सुनहरे क्षणों की याद दिलाता है ।
(defrule golden0
(declare (salience 5000))
(id-root ?id golden)
(id-word =(+ ?id 1)  opportunity|moments|beaches) ;added moments by 14anu-ban-06 (25-7-14)	;added beaches by 14anu-ban-05
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sunaharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  golden.clp    golden0   "  ?id "  sunaharA)" crlf))
)
;He wasted his golden opportunity to play in the national team . 
; Added by Shirisha Manju


(defrule golden1
(declare (salience 4900))
(id-root ?id golden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sone_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  golden.clp 	golden1   "  ?id "  sone_kA)" crlf))
)
; Modified by shirisha Manju (id-wsd_root_mng "sunaharA" as "sone_kA") ---- suggested by Sheetal


;"golden","Adj","1.sunaharA"
;I bought a golden bangle for my mother.
;--"2.kImawI"
;I missed a golden chance of meeting Bill Gates when he visited India.
;--"3.behawarIna/bahuwa_acCA"
;Raj's contribution to the Indian cinema is golden.
;
;
;@@@ Added by 14anu-ban-05 on (27-02-2015)
;Raj's contribution to the Indian cinema is golden.[from golden.clp]
;भारतीय सिनेमा के लिए राज  का योगदान बेहतरीन रहा  है.		[manual]
(defrule golden2
(declare (salience 4901))
(id-root ?id golden)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id behawarIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  golden.clp 	golden2   "  ?id "  behawarIna)" crlf))
)

;@@@ Added by 14anu-ban-05 on (27-02-2015)
;It was the golden age of cinema.[hinkhoj]
;यह सिनेमा का स्वर्ण युग था. [manual]
(defrule golden3
(declare (salience 4902))
(id-root ?id golden)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) age|era)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) svarNa_yuga ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " golden.clp  golden3  "  ?id "  " (+ ?id 1) "  svarNa_yuga   )" crlf))
)
