;Commented by 14anu-ban-01 on (09-03-2015) because this rule is completely similar to subtle0.
;(defrule subtle1
;(declare (salience 4900))
;(id-root ?id subtle)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat ?id adjective|adjective_comparative|adjective_superlative)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id sUkRma))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtle.clp 	subtle1   "  ?id "  sUkRma )" crlf))
;)


;@@@ Added by 14anu-ban-01 on (09-03-2015)
;The fragrance is a subtle blend of jasmine and sandalwood.[oald]
;सुगन्ध चमेली और चन्दन का एक हल्का मिश्रण है . [self]
(defrule subtle1
(declare (salience 6000))
(id-root ?id subtle)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 blend|combination|fusion|union|shade)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtle.clp 	subtle1   "  ?id "  halkA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (09-03-2015)
;She's been dropping subtle hints about what she'd like as a present.[oald]
;वह इस बारे में  क्षीण सङ्केत देती आ रही है कि वह उपहार के रूप में क्या [लेना] पसन्द करेगी .  [self]
(defrule subtle2
(declare (salience 6000))
(id-root ?id subtle)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 hint|clue|notion|sign)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRINa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtle.clp 	subtle2   "  ?id "  kRINa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (09-03-2015)
;I decided to try a more subtle approach.[oald]
;मैंने एक अधिक प्रवीण तरीके का इस्तेमाल करना का फैसला किया .  [self]
(defrule subtle3
(declare (salience 6000))
(id-root ?id subtle)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 approach)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pravINa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtle.clp 	subtle3   "  ?id "  pravINa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (09-03-2015)
;He made a subtle plan.[self: with respect to oald]
;उसने एक कुशल योजना बनाई .   [self]
;A subtle use of lighting in the play.[self: with respect to oald]--parse no.3
;खेल में प्रकाश व्यवस्था का एक कुशल उपयोग . [self]
(defrule subtle4
(declare (salience 6000))
(id-root ?id subtle)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 plan|use)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuSala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtle.clp 	subtle4   "  ?id "  kuSala )" crlf))
)

;@@@ Added by 14anu-ban-01 on (09-03-2015)
;The job required a subtle mind.[oald]
;[उस] कार्य/काम को एक कुशाग्र/तीव्र बुद्धि ही कर सकता था.    [self]
(defrule subtle5
(declare (salience 6000))
(id-root ?id subtle)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 mind|brain|head)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuSAgra/wIvra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtle.clp 	subtle5   "  ?id "  kuSAgra/wIvra )" crlf))
)


;@@@ Added by 14anu-ban-01 on (09-03-2015)
;He is good at making infinitely subtle distinctions.[self: with respect to oald]
;वह अति सूक्ष्म विभेदन करने में अच्छा है . [self]
(defrule subtle6
(declare (salience 6000))
(id-root ?id subtle)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 very|infinitely)
(viSeRya-viSeRaka ?id ?id1)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 awi_sUkRma))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " subtle.clp 	subtle6  "  ?id "  "?id1 "  awi_sUkRma )" crlf)							
)
)


;@@@ Added by 14anu-ban-01 on (09-03-2015)
;The play's message is perhaps too subtle to be understood by young children.[cald]
;नाटक का सन्देश शायद छोटे बच्चों के द्वारा समझे जाने के लिए ज्यादा ही संवेदी है.  [self]
(defrule subtle7
(declare (salience 6000))
(id-root ?id subtle)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 message|news|report)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMvexI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtle.clp 	subtle7   "  ?id "  saMvexI )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule subtle0
(declare (salience 5000))
(id-root ?id subtle)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUkRma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  subtle.clp 	subtle0   "  ?id "  sUkRma )" crlf))
)


;"subtle","Adj","1.sUkRma"
;Her mind is very subtle.
;
;
