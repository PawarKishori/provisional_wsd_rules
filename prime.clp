
(defrule prime0
(declare (salience 5000))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prime.clp 	prime0   "  ?id "  muKya )" crlf))
)

;"prime","Adj","1.muKya"
;The prime cause for this is not known.
;
(defrule prime1
(declare (salience 4900))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prime.clp 	prime1   "  ?id "  bahAra )" crlf))
)

;"prime","N","1.bahAra"
;He is in the prime of his life.
;
(defrule prime2
(declare (salience 4800))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prime.clp 	prime2   "  ?id "  wEyAra_kara )" crlf))
)

;"prime","V","1.wEyAra_karanA"
;The workers were primed by their leader for strike.
;The windows were all primed before actual painting.

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;Changed meaning from 'muKaya_kAraNa' to 'muKya_kAraNa'
;@@@ Added by 14anu21 on 18.06.2014 
;What are the prime factors behind the economic growth of the country? 
;देश के अर्थशास्त्रीय विकास के पीछे अभाज्य गुणक क्या हैं? (Translation before addition)
;देश की आर्थिक विकास के पीछे मुख्य कारण क्या हैं ?
(defrule prime_factor
(declare (salience 5000))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 factor)
(viSeRya-viSeRaNa  ?id1 ?id)
(not(and(viSeRya-of_saMbanXI  ?id1 ?id2)(or(id-root ?id2 number)(id-cat_coarse ?id2 number))))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 muKya_kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  prime.clp 	prime_factor   "  ?id " "  ?id1 " muKya_kAraNa )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (01-01-2015)
;@@@ Added by 14anu06 Vivek Agarwal, MNNIT Allahabad on 27/6/2014****
;Find all the numbers which are prime.
;उन सभी अङ्कों को पाइए जो अभाज्य हैं 
(defrule prime03
(declare (salience 5200))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
;(subject-subject_samAnAXikaraNa  ?id1 ?id) 	;commented by 14anu-ban-09 on (01-01-2015)
(subject-subject_samAnAXikaraNa  ?id2 ?id) 	;Added by 14anu-ban-09 on (01-01-2015)
(viSeRya-jo_samAnAXikaraNa  ?id1 ?id2)		;Added by 14anu-ban-09 on (01-01-2015)
(id-root ?id1 number|digit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aBAjya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prime.clp 	prime03   "  ?id "  aBAjya )" crlf))
)

;@@@ Added by 14anu06 Vivek Agarwal, MNNIT Allahabad on 27/6/2014****
;He was in the prime of his life.
;वह अपने जीवन के उत्कर्ष में था.
(defrule prime04
(declare (salience 5000))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 life)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwkarRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prime.clp 	prime04   "  ?id "  uwkarRa )" crlf))
)


;@@@ Added by 14anu-ban-09 on (05-08-2014)
;In both the cases, the overcoming of the problem of bending of beam under a load is of prime importance. [NCERT CORPUS]
;दोनों ही स्थितियों में, भार के अंतर्गत दण्ड के बंकन की समस्या से छुटकारा पाना बहुत ही महत्वपूर्ण होता है.

(defrule prime3
(declare (salience 5500))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 importance)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahuwa_mahawvapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " prime.clp  prime3  "  ?id "  " ?id1 "  bahuwa_mahawvapUrNa  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-03-2015)
;My prime concern is to protect my property. [oald]
;मेरा पहला सरोकार मेरी सम्पत्ति की रक्षा हैं .	     [Manual]  
(defrule prime4
(declare (salience 5500))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 concern)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prime.clp 	prime4   "  ?id "  pahalA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-03-2015)
;The building is a prime example of 1960s architecture.  [oald]
;इमारत 1960s वास्तुकला का एक उत्कृष्ट उदाहरण है .        	          [Manual]  
(defrule prime5
(declare (salience 5500))
(id-root ?id prime)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 example)
(viSeRya-of_saMbanXI ?id1 ?id2)
(id-root ?id2 architecture|building)	 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwkqRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prime.clp 	prime5   "  ?id "  uwkqRta )" crlf))
)
