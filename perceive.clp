;---------------------------------DEFAULT RULE------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (24-11-2014)
;I perceived a change in his behaviour. [OALD]
;मैंने उसके बर्ताव में बदलाव महसूस किया . [Anusaaraka] 

(defrule perceive0
(id-root ?id perceive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahasUsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perceive.clp 	perceive0   "  ?id "  mahasUsa_kara )" crlf))
)

;--------------------------------------------------------------------------------------------------------------------------


;@@@ Added by 14anu-ban-09 on (24-11-2014)
;The measurement of light as perceived by human eye is called photometry. [NCERT CORPUS]
;mAnava newra xvArA anuBava kie gae prakASa kI mApa prakASamiwi kahalAwI hE. [NCERT CORPUS]

(defrule perceive1
(declare (salience 1000))
(id-root ?id perceive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anuBava_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  perceive.clp 	perceive1   "  ?id "  anuBava_kara )" crlf))
)
