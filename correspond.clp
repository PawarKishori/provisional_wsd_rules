
;"corresponding","Adj","1.jo_samawulya_ho"
;Draw two corresponding lines.
(defrule correspond0
(declare (salience 5000))
(id-root ?id correspond)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id corresponding )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jo_samawulya_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  correspond.clp  	correspond0   "  ?id "  jo_samawulya_ho )" crlf))
)

;@@@ Added by 14anu-ban-03 (13-10-2014)
;The positions of maximum and minimum intensities can be calculated by using the analysis given in Section 10.4 where we had shown that for an arbitrary point P on the line GG ′ (Fig. 10.12 (b)) to correspond to a maximum.[ncert]
;अधिकतम तथा न्यूनतम तीव्रता की स्थितियों की गणना अनुच्छेद 10.4 में दिए गए विश्लेषण का उपयोग करके की जा सकती है, जहाँ पर हमने रेखा GG ′ [चित्र 10.12(b) ] पर एक यथेच्छ बिंदु P लिया जो अधिकतम तीव्रता के सङ्गत करता है. [ncert]
;This in translation corresponds to linear motion. [ncert]
;यह रेखीय गति में स्थानान्तरण को सङ्गत करता है . [manual]
(defrule correspond2
(declare (salience 4900))
(id-root ?id correspond)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 motion|maximum)  ;added 'maximum' by 14anu-ban-03 (29-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safgawa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  correspond.clp 	correspond2   "  ?id "  safgawa_kara )" crlf))
)

;@@@ Added by Manasa(24-8-2015)
;The American FBI corresponds to the British M15
;america kA FBI british m15 ke anukUl hez.
(defrule correspond3
(declare (salience 4000))
(id-root ?id correspond)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukUl_honA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  correspond.clp       correspond3   "  ?id "  anukUl_honA )" crlf))
)

;----------------------------- Default Rules --------------------

(defrule correspond1
(declare (salience 1000))
(id-root ?id correspond)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawrAcAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  correspond.clp 	correspond1   "  ?id "  pawrAcAra_kara )" crlf))
)



;default_sense && category=verb	anurupa_ho	0
;"correspond","V","1.anurupa_honA"
;Her motive for life corresponds with her father's.
;--"2.pawra_vyavahAra_honA"
;They have been corresponding each other for years.
;
;
