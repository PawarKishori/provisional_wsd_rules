;@@@ Added by 14anu21 on 23.06.2014
;These measures are aimed at closing the gap between rich and poor.
;ये उपाय अमीर और दीन के बीच दरार कम करने में लक्षित किए गये हैं . 
;ये उपाय अमीर और दीन के बीच अन्तर कम करने में लक्षित किए गये हैं . 
(defrule gap0
(declare (salience 4900))
(id-root ?id gap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-object  ?id1 ?id)(kriyA-subject  ?id1 ?id))
(id-root ?id1 close|reduce)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gap.clp 	gap0   "  ?id "  aMwara )" crlf))
)

;@@@ Added by 14anu21 on 23.06.2014
;They met again after a gap of twenty years. 
;वे बीस वर्षों की दरार के बाद फिर से मिले .(Translation after adding rule)
;वे बीस वर्षों के अन्तराल के बाद फिर से मिले . 
(defrule gap1
(declare (salience 4900))
(id-root ?id gap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 year|day|month|week|decade|century)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwarAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gap.clp 	gap1   "  ?id "  aMwarAla )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (11-02-2015)
;The gap between the electrodes is exaggerated for clarity.[NCERT]
;spaRtawA ke lie ilEktrodoM ke maXya xUrI baDAe gae hEM.   [MANUAL]

;@@@ Added by 14anu21 on 23.06.2014
;There is a gap of one metre between my bed and my table. 
;मेरे बिस्तर के बीच और मेरी मेज के बीच एक मीटर की एक दरार है . (Translation before adding rule)
;मेरे बिस्तर और मेरी मेज के बीच एक मीटर की दूरी है . 
(defrule gap2
(declare (salience 4880))
(id-root ?id gap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (viSeRya-of_saMbanXI  ?id ?id1)(viSeRya-between_saMbanXI  ?id ?id1))	;added (viSeRya-between_saMbanXI) by 14anu-ban-05 on (11-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gap.clp 	gap2   "  ?id "  xUrI )" crlf))
)

;@@@ Added by 14anu-ban-05 on (11-02-2015)
;The phase of the supply is adjusted so that when the positive ions arrive at the edge of D1, D2 is at a lower potential and the ions are accelerated across the gap. [NCERT]
;srowa kA kalA kA samAyojana isa prakAra kiyA jAwA hE ki jaba XanAyana @D1 ke Cora para pahuzcawA hE wo usa samaya @D2 nimna viBava para howA hE waWA Ayana isa rikwa sWAna meM wvariwa howe hEM.[NCERT]

(defrule gap3
(declare (salience 4901))
(id-root ?id gap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-across_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rikwa_sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gap.clp 	gap3   "  ?id "  rikwa_sWAna )" crlf))
)

;@@@ Added by 14anu-ban-05 on (11-02-2015)
;One end of a galvanometer is connected to the metallic strip midway between the two gaps.[NCERT]
;gElvenomItara kA eka sirA rikwiyoM ke maXya bIcobIca XAwvika pattikA se judA rahawA hE.  [NCERT]
;It is connected across one of the gaps.	[NCERT]
;ise xonoM meM se kisI eka rikwi meM saMyojiwa kara xewe hEM.[NCERT]

(defrule gap4
(declare (salience 4902))
(id-root ?id gap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (viSeRya-saMKyA_viSeRaNa  ?id ?id1)(viSeRya-of_saMbanXI  ?id2 ?id)(id-root ?id2 one)) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rikwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gap.clp 	gap4   "  ?id "  rikwi)" crlf))
)

;@@@ Added by 14anu-ban-05 on (11-02-2015)
;In this region (region C in Figure), no energy gap exists where the upper and lower energy states get mixed.[NCERT]
;isa kRewra meM (ciwra meM kRewra @C), koI UrjA anwarAla nahIM rahawA Ora UparI waWA nicalI UrjA avasWAez miSriwa ho jAwI hEM.[NCERT]

(defrule gap5
(declare (salience 4903))
(id-root ?id gap)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 energy)			;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwarAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gap.clp 	gap5   "  ?id "  anwarAla)" crlf))
)




