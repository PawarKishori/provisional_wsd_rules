;$$$ Modified by 14anu-ban-03 (11-04-2015)
;The taste of that chocolate conjured up memories of school days. [same clp]
;cAkaleta ke usa svAxa ne muJe skUla ke xina yAxa xilA xie.[same clp]
(defrule conjure0
(declare (salience 5000))
(id-root ?id conjure)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xilA_xe))  ;meaning changed from 'yAxa_A' to 'xilA_xe' by 14anu-ban-03 (11-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " conjure.clp	conjure0  "  ?id "  " ?id1 "  xilA_xe  )" crlf))
)

;@@@ Added by 14anu-ban-03 (11-04-2015)
;He could conjure coins from behind people's ears. [oald]
;उसने व्यक्तियों के कानों के पीछे से सिक्के छिपा लिए . [manual]
(defrule conjure2
(declare (salience 5000))  
(id-root ?id conjure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_behind_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CipA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjure.clp 	conjure2   "  ?id "  CipA_le )" crlf))
)

;@@@ Added by 14anu-ban-03 (11-04-2015)
;He conjured a delicious meal out of a few leftovers. [oald]
;उसने कुछ बचे खुचे मे से स्वादिष्ट भोजन बनाया . [manual]
(defrule conjure3
(declare (salience 5000))  
(id-root ?id conjure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 meal)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjure.clp 	conjure3   "  ?id "  banA )" crlf))
)


;@@@ Added by 14anu-ban-03 (11-04-2015)
;She conjured that no one be told of her decision until the next meeting.[oald]
;उसने विनती की कि कोई भी अगले सम्मेलन तक उसके निर्णय के बारे में नहीं बताएगा . [manual]
(defrule conjure4
(declare (salience 5000))  
(id-root ?id conjure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-vAkyakarma ?id ?id2)
(id-root ?id2 tell)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinawI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjure.clp 	conjure4   "  ?id "  vinawI_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (11-04-2015)
;They were conjured by her performance. [oald]
;वे उसके प्रदर्शन से मन्त्रमुग्ध हुए गये थे . [manual]
(defrule conjure5
(declare (salience 5000))  
(id-root ?id conjure)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manwramugXa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjure.clp 	conjure5   "  ?id "  manwramugXa_ho )" crlf))
)


;----------------------------------------Default rules------------------------------------------------------------

;"conjure","V","1.jAxU_karanA"
;The magician conjures before unfolding the trick.
(defrule conjure1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (11-04-2015)
(id-root ?id conjure)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAxU_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conjure.clp 	conjure1   "  ?id "  jAxU_kara )" crlf))
)

