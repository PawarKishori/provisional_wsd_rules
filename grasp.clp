
(defrule grasp0
(declare (salience 5000))
(id-root ?id grasp)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id grasping )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id loBI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  grasp.clp  	grasp0   "  ?id "  loBI )" crlf))
)

;"grasping","Adj","1.loBI"
;
(defrule grasp1
(declare (salience 4900))
(id-root ?id grasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grasp.clp 	grasp1   "  ?id "  pakadZa )" crlf))
)

;"grasp","N","1.pakadZa"
;He has good grasp over the English language.
;
(defrule grasp2
(declare (salience 4800))
(id-root ?id grasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grasp.clp 	grasp2   "  ?id "  pakadZa )" crlf))
)

;"grasp","VT","1.pakadZanA"
;The baby tightly grasped his mother's fingure.
;

;@@@ Added by 14anu-ban-05 on (17-04-2015)
;It’s difficult to grasp the sheer enormity of the tragedy.                [oald]
;दुःखद घटना की पूर्णतया हकीकत  को समझना  मुश्किल है .                                   [self]
(defrule grasp3
(declare (salience 4801))
(id-root ?id grasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 enormity)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grasp.clp 	grasp3   "  ?id "  samaJa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-04-2015)
;I grasped the opportunity to work abroad.	[OALD]
;मैंने विदेश में काम करने के मौके को बिना किसी हिचकिचाहट के ले लिया .[MANUAL]
(defrule grasp4
(declare (salience 4801))
(id-root ?id grasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 opportunity)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id binA_kisI_hicakicAhata_ke_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grasp.clp 	grasp4   "  ?id "  binA_kisI_hicakicAhata_ke_le )" crlf))
)


;@@@ Added by 14anu-ban-05 on (22-04-2015)
;She felt a firm grasp on her arm.       [OALD]
;उसने उसकी बाहु पर  मज़बूत  पकड़ महसूस की . 	[MANUAL]
(defrule grasp5
(declare (salience 5001))
(id-root ?id grasp)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) firm)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) majZabUwa_pakada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grasp.clp  grasp5  "  ?id "  " (- ?id 1) "  majZabUwa_pakada  )" crlf))
)


;@@@ Added by 14anu-ban-05 on (22-04-2015)
;To be successful in business one must have a firm grasp of human nature.	[COCA]
;उद्योग में सफल रहने के लिये, आपके पास मानवीय प्रकृति की अच्छी समझ होनी चाहिये.			[MANUAL]
(defrule grasp6
(declare (salience 5001))
(id-root ?id grasp)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) firm)
(viSeRya-of_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) acCI_samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grasp.clp  grasp6  "  ?id "  " (- ?id 1) "  acCI_samaJa  )" crlf))
)


;@@@ Added by 14anu-ban-05 on (22-04-2015)
;As the drugs took hold, her grasp of reality began to slip slowly away. [OALD]
;जैसे ही दवाओं ने असर किया,उसकी वास्तविकता की समझ धीरे-धीरे कम होने लगी.	[MANUAL]
(defrule grasp7
(declare (salience 4901))
(id-root ?id grasp)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ? )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grasp.clp 	grasp7   "  ?id "  samaJa )" crlf))
)

