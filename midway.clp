;@@@ Added by 14anu-ban-08 on (30-08-2014)
;The leaves have a unique smell midway between eucalyptus and mint.    [NCERT]
;yukaliptasa Ora takasAla ke bIca pawwiyoM kI anoKI ganXa hE.        [NCERT]
(defrule midway0
(declare (salience 0))
(id-root ?id midway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bIca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  midway.clp 	midway0  "  ?id "  bIca)" crlf))
)

;@@@ Added by 14anu-ban-08 on (30-08-2014)
;Thus, for two particles of equal mass the center of mass lies exactly midway between them.     [NCERT]
;इस प्रकार समान द्रव्यमान के दो कणों का द्रव्यमान केन्द्र ठीक उनके बीचोंबीच है.   [Self]
(defrule midway1
(declare (salience 1000))
(id-root ?id midway)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id1 ?id)
(id-word =(+ ?id 1) between)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) bIcoMbIca ))  
                                                  ;Spelling corrected from "bIcombIca" to "bIcoMbIca" by 14anu-ban-08 on (20-11-2014)
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " midway.clp  midway1  "  ?id "  " (+ ?id 1) "  bIcoMbIca  )" crlf))                                                
)
