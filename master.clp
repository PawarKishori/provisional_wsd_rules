;###FILE ADDED by 14anu18 (3-7-14)



;@@@ Added by 14anu18 (3-7-14)
;I am a master of disguise.	[OLD]
;मैं भेष का निपुण हूँ
(defrule master1
(declare (salience 400))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI ?id ?)(viSeRya-viSeRaNa ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nipuNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master1   "  ?id "  nipuNa )" crlf))
)


;@@@ Added by 14anu18 (3-7-14)
;The master cylinder was destroyed.	[OLD]
;मुख्य सिलेंडर नष्ट कर दिया गया . 
(defrule master2
(declare (salience 4000))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master2   "  ?id "  muKya )" crlf))
)



;@@@ Added by 14anu18 (3-7-14)
;He is the master of the house.	[OLD]
;वह गृह का मालिक है . 
(defrule master3
(declare (salience 4000))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(or(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAlika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master3  "  ?id "  mAlika )" crlf))
)



;$$$Modified by 14anu-ban-08 (23-01-2015)         ;added constraint
;@@@ Added by 14anu18 (3-7-14)
;I managed to master my fears.	[OLD]
;मैंअपने भय पर महारत हासिल करने में सफल हुआ.
(defrule master4
(declare (salience 5000))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)          ;added id1 by 14anu-ban-08 (23-01-2015)
(id-root ?id1 fear)           ;added by 14anu-ban-08 (23-01-2015)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id para))
(assert (id-wsd_root_mng ?id mahArawa_hAsila_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master4  "  ?id "  mahArawa_hasila_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  master.clp      master4   "  ?id " para )" crlf)
)
)

;@@@Added by 14anu-ban-08 (16-03-2015)
;Dance master Prabhudeva has given Divorce to his wife.   [Anusaaraka-Agama team obervation]
;नृत्य शिक्षक प्रभुदेवा ने अपनी पत्नी को तलाक दिया.  [self]
(defrule master5
(declare (salience 4001))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master5   "  ?id "  SikRaka )" crlf))
)

;@@@Added by 14anu-ban-08 (16-03-2015)
;She struggled hard to master her temper.  [oald]
;वह अपने गुस्से पर काबू करने के लिये कठोर संघर्ष करती थी.  [self]
(defrule master6
(declare (salience 4001))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 temper)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAbU_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master6   "  ?id "  kAbU_kara )" crlf))
)

;@@@Added by 14anu-ban-08 (16-03-2015)
;I went to my physics master to clear some doubts in maths.  [hindkhoj]
;मैं गणित के सन्देह पूछने के लिये भौतिक विज्ञान के शिक्षक के घर गया.  [self]
(defrule master7
(declare (salience 400))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 physics|chemistry|math|english|biology|computer|history|hindi|civics|ziology|sanskrit|botany|sociology)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master7   "  ?id "  SikRaka )" crlf))
)

;@@@Added by 14anu-ban-08 (16-03-2015)
;This painting is clearly the work of a master.  [cald]
;इस चित्रकारी में किसी माहिर ने काम किया हैं.  [self]
(defrule master8
(declare (salience 400))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(subject-subject_samAnAXikaraNa ?id2 ?id1)
(id-root ?id1 work)
(id-root ?id2 painting)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAhira))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master8   "  ?id "  mAhira )" crlf))
)

;-----------------------------Default Rule---------------------------------

;@@@Added by 14anu-ban-08 (16-03-2015)
;They lived in fear of their master.  [oald]
;वे अपने मालिक के डर से जी रहा था.  [self]
(defrule master0
(declare (salience 0))
(id-root ?id master)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAlika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  master.clp 	master0   "  ?id "  mAlika )" crlf))
)
