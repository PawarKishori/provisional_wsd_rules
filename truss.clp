;$$$ Modified by 14anu-ban-07 (08-12-2014)
;@@@ Added by 14anu23 16/06/2014
;Alfred had trussed the chicken. 
;अल्फ्रेड ने मुर्गी की पंख और पैर बाँध दी थी.
(defrule truss2
(declare (salience 4900))
(id-root ?id truss)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 chicken|bird)
(id-cat_coarse ?id verb)
(kriyA-object   ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paMKa_Ora_pEra_bAzXa_xe)) 
(assert  (id-wsd_viBakwi   ?id1  kA)) ;added viBakwi by 14anu-ban-07(08-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  truss.clp 	truss2   "  ?id "  paMKa_Ora_pEra_bAzXa_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  truss.clp      truss2   "  ?id1 " kA )" crlf);added viBakwi by 14anu-ban-07(08-12-2014)
)
)

;------------------------ Default rules ------------------------
(defrule truss0
(declare (salience 5000))
(id-root ?id truss)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  truss.clp 	truss0   "  ?id "  DAzcA )" crlf))
)

;"truss","N","1.DAzcA"
;The truss caved in,in the auditorium.
;--"2.harniyA_se_pIdiwa_vyakwi_ke_pahanane_kA_kamarabanXa"
;
(defrule truss1
(declare (salience 4900))
(id-root ?id truss)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  truss.clp 	truss1   "  ?id "  bAzXa )" crlf))
)

;"truss","VT","1.bAzXanA"
;The owner trussed the dog when the guests arrived .
;
