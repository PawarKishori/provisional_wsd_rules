
;@@@Added by 14anu-ban-08 (09-04-2015)     ;Run on parser 11
;It is permissible for a lawful user to disassemble a computer program.  [freedictionary.com]
;एक वैध प्रयोगकर्त्ता के लिए अनुज्ञेय है एक संगणक प्रोग्राम का अनुवाद करना  .   [self]
(defrule lawful1
(declare (salience 100))
(id-root ?id lawful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 user)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vEXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lawful.clp         lawful1  "  ?id "  vEXa )" crlf))
)


;--------------------------------- Default Rules ------------------------------------

;@@@Added by 14anu-ban-08 (09-04-2015)
;She is his lawful wife, and so is entitled to inherit the money.  [oald]
;वह उसकी जायज पत्नी है उसके संपत्ति में अधिकार पाने की.  [self]
(defrule lawful0
(declare (salience 0))
(id-root ?id lawful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAyaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  lawful.clp         lawful0  "  ?id "  jAyaja )" crlf))
)

