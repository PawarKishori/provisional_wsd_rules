;@@@ Added by 14anu-ban-03 (25-03-2015)
;Work on the office building will be complete at the end of the year. [oald]
;कार्यालय भवन का निर्माण कार्य वर्ष के अन्त में समाप्त हो जाएगा. [manual]
(defrule complete2
(declare (salience 5000))
(id-root ?id complete)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-at_saMbanXI ?id ?id1)
(id-root ?id1 end)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  complete.clp 	complete2   "  ?id "  samApwa )" crlf))
)


;@@@ Added by 14anu-ban-03 (25-03-2015)
;I only need one more card to complete the set. [oald]
;मुझे  समूह तैयार करने के लिए केवल एक  और कार्ड की आवश्यकता है . [manual]
(defrule complete3
(declare (salience 5000))
(id-root ?id complete)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 set)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  complete.clp 	complete3   "  ?id "  wEyAra_kara )" crlf))
)


;-------------------------------------------Default Rules-------------------------------

(defrule complete0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (25-03-2015)
(id-root ?id complete)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  complete.clp 	complete0   "  ?id "  pUrA )" crlf))
)

;"complete","Adj","1.pUrA"
;You should have a complete meal everyday.
;
(defrule complete1
(declare (salience 00))     ;salience reduced by 14anu-ban-03 (25-03-2015)
(id-root ?id complete)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  complete.clp 	complete1   "  ?id "  pUrA_kara )" crlf))
)

;"complete","V","1.pUrA_karanA"
;Did you complete your homework?
;-----------------------------------------------------------------------------

