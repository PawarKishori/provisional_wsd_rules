;##############################################################################
;#  Copyright (C) 2014 14anu21 , 14anu03 
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;$$$ Modified by 14anu-ban-04 (08-12-2014)
;###[COUNTER EXAMPLE]### He swung the camera around to face the opposite direction.           [oald]
;###[COUNTER EXAMPLE]### उसने विपरीत दिशा के सम्मुख होने के लिए कैमरा घुमाया.      [self]
;@@@ Added by 14anu21 on 18.06.2014
;All work was done by the students under the direction of Rama. 
;सारा काम राम के निर्देशन में छात्रों द्वारा किया गया.
(defrule direction0
(id-root ?id direction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(and(kriyA-under_saMbanXI  ?idverb ?id)(or(viSeRya-of_saMbanXI  ?id ?id1)(viSeRya-given_by_saMbanXI  ?id ?id1)))  ;added by 14anu-ban-04 (08-12-2014)
;(or(and(kriyA-under_saMbanXI  ?idverb ?id)(or(viSeRya-of_saMbanXI  ?id ?id1)(viSeRya-given_by_saMbanXI  ?id ?id1)))        ;commented by 14anu-ban-04 (08-12-2014)
;(kriyA-object  ?idverb ?id))                   ;commented by 14anu-ban-04 (08-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirxeSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  direction.clp     direction0   "  ?id "  nirxeSana )" crlf))
)

;NOTE [this rule is commented by 14anu-ban-04 on (12-12-2014) because the meaning is incorrect and there is no sentence related to this meaning]
;@@@ Added by 14anu21 on 20.06.2014
;He is the director of this institute.
;वह इस संस्था का निदेशक है . 
;(defrule direction1
;(id-root ?id direction)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id nixeSaka))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  direction.clp     direction1   "  ?id "  nixeSaka )" crlf))
;)

;
;@@@ Added by 14anu21 on 20.06.2014
;Can I know the direction of cinema hall?
;क्या मैं  चलचित्र हॉल का रास्ता जान सकता हूँ ? 
(defrule direction2
(declare (salience 100))
(id-root ?id direction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?idverb ?id)
(id-root ?idverb know|tell|guide)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAswA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  direction.clp     direction2   "  ?id "  rAswA )" crlf))
)


;There is some clever direction and the film is very well shot. 


;@@@ Added by 14anu03 on 03-july-14
;She set off in the opposite direction.
;वह विपरीत दिशा में रवाना हुई .
(defrule direction3
(declare (salience 0))
(id-root ?id direction)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1 )
(id-word ?id1 different|opposite|right|west|north|south|east)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " direction.clp  direction3  " ?id "   xiSA  )" crlf))
)

;@@@ Added by 14anu03 on 03-july-14
;Under his direction, the college has developed an international reputation.
;उसके निर्देशन के अंदर,कालेज ने अन्तर्राष्ट्रीय नाम हासिल किया . 
(defrule direction4
(declare (salience 1))
(id-root ?id direction)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa ?id ?id1 )
(id-word ?id1 his|her|their)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirxeSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " direction.clp  direction4  " ?id "   nirxeSana  )" crlf))
)


