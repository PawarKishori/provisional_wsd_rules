
;@@@ Added by 14anu-ban-09 on (24-03-2015)
;What's that putrid smell?	[cald] 
;यह कैसी बदबूदार गन्ध है? 	[Manual]
(defrule putrid1
(declare (salience 1000))
(id-root ?id putrid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id )
(id-root ?id1 smell|odour|body)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxabUxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  putrid.clp 	putrid1   "  ?id "  baxabUxAra )" crlf))
)

;@@@ Added by 14anu-ban-09 on (25-03-2015)
;She was wearing a dress in a putrid shade of yellow. 	[cald] 
;उसने पीला रङ्ग का एक भद्दा लिबास पहना हुआ था .  			[Manual]
(defrule putrid2
(declare (salience 1000))
(id-root ?id putrid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id )
(id-root ?id1 shade)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaxxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  putrid.clp 	putrid2   "  ?id "  BaxxA )" crlf))
)


;@@@ Added by 14anu-ban-09 on (25-03-2015)
;A good play ruined by putrid acting. 	[freedictionary.com] 
;एक अच्छा नाटक बुरे अभिनय से बर्बाद हो गया . 	`	[Manual]
(defrule putrid3
(declare (salience 1000))
(id-root ?id putrid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id )
(id-root ?id1 acting)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id burA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  putrid.clp 	putrid3   "  ?id "  burA )" crlf))
)

;------------------------DEFAUT RULE----------------------------------------------

;@@@ Added by 14anu-ban-09 on (24-03-2015)
;NOTE:-Example sentence need to be added.
(defrule putrid0
(declare (salience 000))
(id-root ?id putrid)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BraRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  putrid.clp 	putrid0   "  ?id "  BraRta )" crlf))
)

;------------------------------------------------------------------------------------
