
;##############################################################################
;#  Copyright (C) 2002-2005 Garima Singh (gsingh.nik@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;Commented by 14anu-ban-02 (10-10-2014)
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)24-jan-2014
;He fielded the ball expertly.
;उसने सुविज्ञतापूर्वक गेंद का क्षेत्ररक्षण किया . 
;(defrule ball2
;(declare (salience 4000))
;(id-root ?id ball)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object  ?kri ?id)
;(id-root ?kri field|bounce)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id geMxa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball2  "  ?id "  geMxa )" crlf))
;)

;$$$ Modified by 14anu-ban-02(12-01-2015)
;He took her to the ball party.
;वह बॉल पार्टी में उसको ले गया.
;@@@ Added by 14anu04 12-June-2014
;He took her to the ball party.
;वह बॉल पार्टी में उसको ले गया.
(defrule ball3
(declare (salience 4000))
(id-root ?id ball)
?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 at|to)                  ;commented by 14anu-ban-02(12-01-2015)
;(id-root =(+ ?id 1) party)            ;commented by 14anu-ban-02(12-01-2015)
;(test (= (- ?id 2) ?id1))             ;commented by 14anu-ban-02(12-01-2015)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)      ;added by 14anu-ban-02(12-01-2015)
(id-root ?id1 party|dance)                      ;added by 14anu-ban-02(12-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id boYla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball3  "  ?id "  boYla )" crlf))
)


;*************************DEFAULT RULES********************************

;$$$ Modified by 14anu-ban-02 (10-10-2014)
;Meaning changed from golA to geMxa
;He fielded the ball expertly.[rule:ball2]
;उसने सुविज्ञतापूर्वक गेंद का क्षेत्ररक्षण किया .[rule:ball2]
;Here the balls represent atoms and springs represent interatomic forces.[ncert]
;इसमें गेंदें परमाणुओं को तथा स्प्रिंग अन्तरा-परमाणविक बलों को निरूपित करती हैं.[ncert]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)24-jan-2014
(defrule ball0
(declare (salience 0))
(id-root ?id ball)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id geMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball0  "  ?id "  geMxa )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)24-jan-2014
(defrule ball1
(declare (salience 0))
(id-root ?id ball)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golAkAra_bana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball1  "  ?id "  golAkAra_bana )" crlf))
)
;@@@ Added by 14anu21 on 02.07.2014 
;He is playing with the ball. [self]
;वह गोला खेल रहा है . (Translation before adding rule)
;वह गेंद के साथ खेल रहा है.
(defrule ball5
(declare (salience 100))
(id-root ?id ball)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ?idverb ?id)
(id-root ?idverb play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id geMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball5  "  ?id "  geMxa )" crlf))
)
;@@@ Added by 14anu21 on 02.07.2014  
;The ball was lost while playing.[self]
;गोला खेलने उस अवधि में खोया गया था . 
;गेंद खेलते हुए खो गई.

(defrule ball4
(declare (salience 100))
(id-root ?id ball)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?idverb ?id)
(kriyA-while_saMbanXI  ?idverb ?idsam)
(id-root ?idsam play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id geMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball4  "  ?id "  geMxa )" crlf))
)



;@@@ Added by 14anu21 on 02.07.2014
;The ball touched the bails.[self]
;गोले ने जमानत स्पर्श की .(Translation before adding the rule;Added rule bail4)
;गेंद ने गुल्ली को छुआ.

;
(defrule ball6
(declare (salience 100))
(id-root ?id ball)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-subject  ?idverb ?id)(kriyA-object ?idverb ?id))
(or(kriyA-subject  ?idverb ?id1)(kriyA-object ?idverb ?id1))
(id-root ?id1 bails|stump|bat|leg|hand|shoulder|ground)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id geMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball6  "  ?id "  geMxa )" crlf))
)

;@@@ Added by 14anu21 on 02.07.2014
;He caught the ball.[self]  
;उसने गोला पकडा . (Translation before adding rule)
;उसने गेंद पकडी.

;
(defrule ball7
(declare (salience 100))
(id-root ?id ball)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(and(kriyA-subject  ?idverb ?id)(kriyA-object ?idverb ?id1))(and(kriyA-subject ?idverb ?id1)(kriyA-object ?idverb ?id)))
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id geMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball7  "  ?id "  geMxa )" crlf))
)

;@@@ Added by 14anu21 on 02.07.2014
;He scored forty runs on ten balls.[self]
;उसने दस गोले पर चालीस घूमना खरोंचे .(Translation before adding rules run29,score9,ball8)
;उसने दस गेंद पर चालीस रन बनाए . 
;Added rules run29,score9
;
(defrule ball8
(declare (salience 100))
(id-root ?id ball)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ?idverb ?id)
(kriyA-subject  ?idverb ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id geMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ball.clp 	ball8  "  ?id "  geMxa )" crlf))
)

