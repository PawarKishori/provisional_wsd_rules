;@@@ Added bY Krithika 10/3/2014
; I am very familiar with people of Ratlam. [old clp]
; mEM rawalAma ke logoM ke sAWa awyanwa pariciwa hUz.

(defrule familiar2
(declare (salience 5000))
(id-root ?id familiar)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-with_saMbanXI  ?id ?)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  familiar.clp 	familiar2   "  ?id "  pariciwa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (06-11-2014)
;The two familiar temperature scales are the Fahrenheit temperature scale and the Celsius temperature scale.[NCERT]
;jaba xAba waWA selsiyasa wApa mApakrama, xo supariciwa wApa mApakrama hEM.[NCERT]
(defrule familiar3
(declare (salience 1000))
(Domain physics)
(id-root ?id familiar)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 scale)		;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id supariciwa))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  familiar.clp		familiar3   "  ?id "  supariciwa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  familiar.clp       familiar3   "  ?id "  physics )" crlf))
)

;@@@ Added by (14anu11)avni
;परिचित किताब को पुनः पढने से पन्ने पर लिखे शब्दों पर और अधिक ध्यान दे पाने में मदद मिलेगी और वे नये कहानियों और शब्दों के पैटर्न्स ( नमूनों ) को पहचानना प्रारंभ कर देंगे .
;Knowing a familiar book will help them notice more about the words on the page and they will start to recognise the patterns in new
; words and stories .
(defrule familiar4
(declare (salience 5000))
(id-root ?id familiar)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  familiar.clp 	familiar4   "  ?id "  pariciwa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-12-2014)
;It is the most familiar form of electromagnetic waves.[NCERT]
;aha vExyuwacumbakIya warafgoM kA sarvAXika supariciwa rUpa hE.[NCERT]
(defrule familiar5
(declare (salience 5000))
(id-root ?id familiar)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(- ?id 1) most)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1)  sarvAXika_supariciwa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " familiar.clp  familiar5  "  ?id "  " - ?id 1 "  sarvAXika_supariciwa  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (28-08-2015)
;After studying this chapter, the learners will become familiar with the state of the Indian economy in 1947, the year of India's Independence  understand the factors that led to the underdevelopment and stagnation of the Indian economy.[social]
;इस अध्याय के बारे में अध्ययन करने के बाद, शिक्षार्थी 1947 में भारतीय अर्थव्यवस्था की अवस्था के साथ परिचित हो जाएँगे, भारत के इन्डिपेन्डन्स का वर्ष वह कारक जो भारतीय अर्थव्यवस्था के अपर्याप्त विकास के लिए और गतिहीनता का कारण  समझेंगे . [self]

(defrule familiar6
(declare (salience 5000))
(id-root ?id familiar)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 learner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pariciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  familiar.clp 	familiar6   "  ?id "  pariciwa )" crlf))
)


; ####################Default rules########################
; Removed salience by Krithika 10/2/2014
(defrule familiar0
(id-root ?id familiar)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnA_pahacAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  familiar.clp 	familiar0   "  ?id "  jAnA_pahacAnA )" crlf))
)

; Removed salience by Krithika 10/2/2014
(defrule familiar1
(id-root ?id familiar)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAnA-mAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  familiar.clp 	familiar1   "  ?id "  jAnA-mAnA )" crlf))
)

;"familiar","Adj","1.jAnA-mAnA"
;Amitabh Bacchan is a very a familiar figure.
;--"2.pariciwa"
;I am very familiar with people of Ratlam.

;########################Additional examples#########################
; This dark [matter] is most probably different from the stuff we are [familiar] with, e.g. atoms, neutrons, protons, etc.
; You are already familiar with some direct methods for the measurement of length.
