(defrule hit0
(declare (salience 5000))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AlocanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hit.clp	hit0  "  ?id "  " ?id1 "  AlocanA_kara  )" crlf))
)

;He always hit back at those who oppose him.
;vaha hameSAM unakI AlocanA karawA hE jo usakA viroXa karawe hEM
(defrule hit1
(declare (salience 4900))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mila_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hit.clp	hit1  "  ?id "  " ?id1 "  mila_jA  )" crlf))
)

;We've hit on the solution at last.
;AKirakAra hameM hala mila gayA hE
(defrule hit2
(declare (salience 4800))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AlocanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hit.clp	hit2  "  ?id "  " ?id1 "  AlocanA_kara  )" crlf))
)

;He always hit out at those who oppose him.
;vaha hameSAM unakI AlocanA karawA hE jo usakA viroXa karawe hEM

;$$$ Modified by 14anu-ban-06 (10-10-2014) added object|ground to id-root
;If the object is released from rest, the initial potential energy is completely converted into the kinetic energy of the object just before it hits the ground.(NCERT)
;yaxi piMda ko virAmAvasWA se mukwa kiyA jAwA hE, wo BUmi se takarAne se TIka pahale piMda kI sampUrNa sWiwija UrjA gawija UrjA meM parivarwiwa ho jAwI hE.(NCERT)
;In the falling object example, if you include the effect of air resistance during the fall and see the situation after the object hits the ground and stays there, the total mechanical energy is obviously not conserved.(NCERT)
;girawe piMda ke uxAharaNa meM yaxi Apa girawe piMda para lagane vAle vAyu ke prawiroXa ke praBAva ko BI sammiliwa kara leM Ora piMda ke BUmi para takarAne Ora vahAz Taharane kI sWiwiyoM ko xeKeM wo Apa yaha pAezge ki spaRta rUpa se, kula yAnwrika UrjA saMrakRiwa nahIM huI hE.(NCERT)
;@@@ ADDED BY PRACHI RATHORE
; If there were no friction, we would remain where we were, while the floor of the bus would simply slip forward under our feet and the back of the bus would hit us. [PHYSICS]
; यदि घर्षण न होता, तो हम वहीं रहते जहां पहले थे जबकि हमारे पैरों के नीचे बस का फर्श केवल आगे की दिशा में सरकता और बस का पीछे का भाग हमसे आकर टकराता.
(defrule hit5
(declare (salience 4710))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1))
(id-root ?id1 back|wall|floor|iceberg|bridge|wind|object|ground); added by object|ground 14anu-ban-06 (10-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takarA))
(assert (kriyA_id-object_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit5   "  ?id "  takarA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hit.clp   hit5   "  ?id " se )" crlf)
)
)

;@@@ ADDED BY PRACHI RATHORE
;The idea for the book hit me in the middle of the night. [CAMBRIDGE]
;पुस्तक के लिए विचार मुझे रात के मध्यस्थल में आया . 
(defrule hit6
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1))
(id-root ?id1 idea|solution|do|importance|smell)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa_AyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit6   "  ?id "  samaJa_AyA )" crlf)
)
)

;@@@ ADDED BY PRACHI RATHORE
;;I hit my head as I was getting into the car.[VEENA MAM TRANSLATION]
 ; जब मैं कार के अंदर घुस रहा था मेरा सिर टकरा गया .
(defrule hit7
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(or(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?id1))
;(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(viSeRya-RaRTI_viSeRaNa  ?id1 ?)
(kriyA-object  ?id ?id1)
(kriyA-subject  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takarA))
(assert (kriyA_id-subject_viBakwi ?id ne))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit7   "  ?id "  takarA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hit.clp   hit7   "  ?id " ne )" crlf)
)
)

;@@@ ADDED BY PRACHI RATHORE
;I couldn't remember where I'd seen him before, and then it suddenly hit me. [OALD]
; मैं याद नहीं कर सका कि मैंने  उसको पहले कहाँ देखा , और बाद में अचानक मुझे याद आया . 
(defrule hit8
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-vAkyakarma ?id ?)(kriyA-vAkyakarma ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit8   "  ?id "  AyA )" crlf)
)
)

;@@@ ADDED BY PRACHI RATHORE
;President hits town tomorrow. [OALD]
;अध्यक्ष कल नगर पहुँचेगा . 
(defrule hit9
(declare (salience 4695))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1))
;(id-root ?id1 euro|temperature|town|road|tornado)
(or(kriyA-kAlavAcI ?id ?)(kriyA-kqxanwa_karma ?id ?)(kriyA-anaBihiwa_subject ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit9   "  ?id "  pahuzca )" crlf)
)
)

;$$$ Modified by Shirisha Manju (4-09-2015)-- Suggested by Chaitanya Sir
;COUNTER EXAMPLE: That year, an earthquake hit Bihar and killed thousands.
;$$$ Modified by 14anu-ban-06 (03-02-2015)
;Production has been badly hit by the strike. (cambridge)
;उत्पादन स्ट्राइक के द्वारा बुरी तरह से प्रभावित हुआ है . (manual)
;@@@ ADDED BY PRACHI RATHORE
;Rural areas have been worst hit by the strike. [OALD]
;ग्रामीण क्षेत्र स्ट्राइक के द्वारा सर्वाधिक बुरी तरह प्रभावित किए गये हैं . 
;ग्रामीण क्षेत्र स्ट्राइक के द्वारा सर्वाधिक बुरी तरह प्रभावित  हुए हैं .(manual);added by 14anu-ban-06 (03-02-2015)
(defrule hit10
(declare (salience 4710))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 tax|area|death|increase|economy|layoff|earthquake|production);added 'production' by 14anu-ban-06 (03-02-2015)
(id-tam_type ?id passive) ;added by Shirisha Manju (4-09-15)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAviwa_ho));meaning changed from 'praBAviwa_kara' to 'praBAviwa_ho' by 14anu-ban-06 (03-02-2015) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit10   "  ?id "  praBAviwa_ho )" crlf)
)
)

;@@@ ADDED BY PRACHI RATHORE
;We've hit on the solution at last.[OALD]
;हम आखिरकार हल पर पहुँच चुके हैं . 
(defrule hit11
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI ?id ?id1)
(id-root ?id1 solution)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit11   "  ?id "  pahuzca )" crlf)
)
)

;@@@ ADDED BY PRACHI RATHORE
;He hit the nail squarely on the head with the hammer. [OALD]
;उसने हथौडे से सिर पर उचित रूप से कील ठोका . 
(defrule hit12
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 nail)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TokA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit12   "  ?id "  TokA )" crlf)
)
)

;ADDED BY PRACHI RATHORE 
;I was hit by a falling stone.  [OALD]
;मुझे एक गिरते हुए पत्थर के द्वारा चोट लगा गयी थी .  
;(defrule hit13
;(declare (salience 4720))
;(id-root ?id hit)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-karma ?id ?id2)
;(kriyA-by_saMbanXI ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id cowa_laga))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit13   "  ?id "  cowa_laga )" crlf)
;)
;)

;$$$ Modified by 14anu-ban-06 (03-02-2015)
;### [COUNTER EXAMPLE] ### Production has been badly hit by the strike. (cambridge)
;### [COUNTER EXAMPLE] ### उत्पादन स्ट्राइक के द्वारा बुरी तरह से प्रभावित हुआ है . (manual)
;@@@ ADDED BY PRACHI RATHORE
;The town was hit by bombs again last night. [OALD]
;नगर पर फिर से पिछली रात बमों के द्वारा आक्रमण किया गया था .  
(defrule hit14
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 bomb);uncommented by 14anu-ban-06 (03-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkramaNa_kara))
(assert (kriyA_id-subject_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit14   "  ?id "  AkramaNa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hit.clp   hit14   "  ?id " para )" crlf)
)
)

;@@@ ADDED BY PRACHI RATHORE
;  I was late because I hit a traffic jam on the way over.  [m-w learners dictionary]
;मैं देरी से था क्योँकि मुझे मार्ग पर एक ट्रेफिक जाम मिला .  
(defrule hit15
(declare (salience 4690))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id2)
(kriyA-object  ?id ?id1)
(id-root ?id1 jam|water)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit15   "  ?id "  mila)" crlf)
)
)

;ADDED BY PRACHI RATHORE
;Rural areas have been worst hit by the strike. [OALD]
;ग्रामीण क्षेत्र स्ट्राइक के द्वारा सर्वाधिक बुरा प्रभावित किए गये हैं . 
;(defrule hit16
;(declare (salience 4710))
;(id-root ?id hit)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-kriyA_viSeRaNa  ?id ?)
;(kriyA-by_saMbanXI  ?id ?)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id praBAviwa_kara))
;(assert (kriyA_id-object_viBakwi ?id ko))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit16   "  ?id "  praBAviwa_kara )" ;crlf)
;)
;)

;$$$ Modified by 14anu-ban-06 (22-11-2014)
;@@@ ADDED BY PRACHI RATHORE
;Spain was one of the hardest hit countries. [OALD]
;स्पेन सबसे अधिक प्रभावित देशों में से एक था .  
(defrule hit17
(declare (salience 4710))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
;(viSeRya-viSeRaNa ?id2 ?id);commented by 14anu-ban-06 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1 praBAviwa));spelling corrected from 'praBAvIwa' to  'praBAviwa' by 14anu-ban-06 (22-11-2014) and (- id 1) to ?id1
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  hit.clp 	hit17   " ?id  ?id1 "praBAviwa )" crlf)
)
)

;$$$ Modified by 14anu-ban-06 (22-11-2014)
;@@@ ADDED BY PRACHI RATHORE
;Hit the brakes! [OALD]
;ब्रेक दबाइए!  
(defrule hit18
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?);commented by 14anu-ban-06 (22-11-2014)
(kriyA-object  ?id ?id1);added by 14anu-ban-06 (22-11-2014)
(id-root ?id1 brake|switch|button|accelerator)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit18   "  ?id "  xabA )" crlf);spelling corrected from 'xaba' to  'xabA' by 14anu-ban-06 (22-11-2014)
)
)

;$$$ Modified by 14anu-ban-06 (22-11-2014)
;@@@ ADDED BY PRACHI RATHORE
;The project went smoothly at first, but then we started to hit some problems.[M-W LEARNERS DICT.]
;परियोजना शुरुअात में आसानी चल रही थीं, किंतु फिर हमें कुछ समस्याएँ आना शुरु हुयी.  
(defrule hit19
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?);commented by 14anu-ban-06 (22-11-2014)
(kriyA-object  ?id ?id1);added by 14anu-ban-06 (22-11-2014)
(id-root ?id1 problem)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id A));meaning changed from 'AnA' to 'A' by 14anu-ban-06 (22-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit19   "  ?id "  A )" crlf)
)
)


;@@@ ADDED BY PRACHI RATHORE
;Follow this footpath and you'll eventually hit the road. [OALD]
;इस पैदलपथ का अनुसरण कीजिये और आप अन्त में सडक तक पहुँचेंगे .   
(defrule hit20
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 road|home|ground|store|shelf|market|slope|note|house|bottom)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca))
(assert (kriyA_id-object_viBakwi ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit20   "  ?id "  pahuzca )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hit.clp   hit20   "  ?id " waka )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (26-11-2014)
;He hit twelve aces in the game.(ace.clp)
;उसने खेल में बारह ऐस मारी.(manual)
(defrule hit21
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 game)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit21   "  ?id "  mArA )" crlf)
)
)

;$$$ Modified by 14anu-ban-06 (08-12-2014)
;@@@ Added by 14anu13 on 19-06-14
;If , the arrow hits the target , the player jumps .
;यदि, तीर लक्ष्य पर लगता है, तो खिलाडी  कूदता है |
(defrule hit22
(declare (salience 4600))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1);added by 14anu-ban-06 (08-12-2014) to avoid clash with 'default rule(hit4)'
(id-root ?id1 arrow);added by 14anu-ban-06 (08-12-2014) to avoid clash with 'default rule(hit4)'
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit22   "  ?id "  laga )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hit.clp       hit22   "  ?id " para )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (13-01-2015)
;The player hit a smash.(smash.clp)
;खिलाड़ी ने स्मैश मारा.(manual)
;I saw him hit a shot at Shea Stadium.(COCA)
;मैंने देखा कि उसने शॆअ स्टॆडीअम पर शॉट मारा . (manual)
(defrule hit23
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 smash|shot)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit23   "  ?id "  mArA )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (13-01-2015)
;The film was a hit for him in 2008.(OALD)
;सिनेमा उसके लिए 2008 में हिट था .(manual) 
;The movie was a smash hit.(smash.clp)
;फिल्म ज़बर्दस्त हिट रही थी.(manual)
;The song was a smash hit at the 1915 Fair.(COCA)
;गाना 1915 फेयर में जबर्दस्त हिट था . (manual)
(defrule hit24
(declare (salience 4800));salience increased from '4700' by 14anu-ban-06 (16-02-2015)
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 film|movie|song|album|series);added 'film|series' by 14anu-ban-06 (16-02-2015)
;(viSeRya-kqxanwa_viSeRaNa ?id2 ?id);commented by 14anu-ban-06 (16-02-2015)
;(id-root ?id2 smash);commented by 14anu-ban-06 (16-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hita))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit24   "  ?id "  hita )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (03-02-2015)
;Three drug dealers were hit in the city over the weekend.(cambridge)
;तीन दवा व्यापारी सप्ताहान्त के दौरान शहर में मारे गये थे . (manual)
(defrule hit25
(declare (salience 4800));salience increased from '4700' by 14anu-ban-06 (16-02-2015)
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id2 dealer|criminal)
(kriyA-subject ?id ?id2)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 city|town|village)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit25   "  ?id "  mArA_jA )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (04-02-2015)
;He was the victim of a mafia hit.(cambridge)
;वह एक माफिया हमले का शिकार था . (manual)
(defrule hit26
(declare (salience 5100))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 mafia)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hamalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit26   "  ?id "  hamalA )" crlf))
)

;commented by 14anu-ban-06 (16-02-2015)
;@@@ Added by 14anu-ban-06 (04-02-2015)
;The film was a hit for him in 2008.(OALD)
;सिनेमा उसके लिए 2008 में हिट था .(manual) 
;(defrule hit27
;(declare (salience 5100))
;(id-root ?id hit)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(id-root ?id2 movie|film|album|series|song)
;(subject-subject_samAnAXikaraNa ?id2 ?id)
;(viSeRya-in_saMbanXI ?id ?id1)
;(id-cat_coarse ?id1 number)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id hita))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	;hit27   "  ?id "  hita )" crlf))
;)
;-------------------------- Default Rules ---------------------
(defrule hit3
(declare (salience 4700))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AGAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit3   "  ?id "  AGAwa )" crlf))
)

;"hit","N","1.AGAwa/saPala"
;bArdara eka 'hit'(saPala) Pilma ho cukI hE.
;
(defrule hit4
(declare (salience 4600))
(id-root ?id hit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prahAra_kara))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hit.clp 	hit4   "  ?id "  prahAra_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hit.clp       hit4   "  ?id " para )" crlf)
)
)

;"hit","V","1.prahAra_karanA"
;usane apanI lATI se usake sira para 'hit' (prahAra) kiyA.

