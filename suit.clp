;@@@ Added by jagriti(24.1.2014)
;The family brought suit against the landlord. 
;परिवार ने मकान मालिक के विरुद्ध मुकदमा लगाया . 
(defrule suit0
(declare (salience 5000))
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(kriyA-against_saMbanXI  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukaxamA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suit.clp 	suit0   "  ?id "  mukaxamA )" crlf))
)

;@@@ Added by jagriti(24.1.2014)
;Suit of marriage.
; शादी का निवेदन .  
(defrule suit1
(declare (salience 4900))
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(viSeRya-of_saMbanXI ?id ?id1)(viSeRya-viSeRaNa  ?id ?id1))
(id-root ?id1 intense|marriage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nivexana ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suit.clp 	suit1   "  ?id "  nivexana  )" crlf))
)

;;She bought a expensive suit for her child. 
;usane apane bacce ke liye eka mahazgA sUta KarIxA .
(defrule suit2
(declare (salience 100))
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suit.clp 	suit2   "  ?id "  sUta )" crlf))
)

;"suit","N","1.sUta"
;This suit is not suited for me.
;--"2.raga"
;The suits are spades, hearts, diamonds && clubs. 

;$$$ Modified by 14anu-ban-01 on (09-04-2015):added,removed conditions
;@@@ Added by jagriti(24.1.2014)
;This hot weather doesn't suit me.
;यह गरम मौसम मुझे नहीं जँचता है . 
(defrule suit3
(declare (salience 4800))
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-kriyA_niReXaka ?id ?) 	;commented by 14anu-ban-01 on (09-04-2015)
(id-root ?id1 climate|weather)	;added by 14anu-ban-01  on (09-04-2015)
(kriyA-subject ?id ?id1)	;added by 14anu-ban-01  on (09-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jazca))	;corrected "jazca" by 14anu-ban-01  on (09-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suit.clp 	suit3 " ?id "  jazca )" crlf))	;corrected "jazca" by 14anu-ban-01  on (09-04-2015)
)


;$$$ Modified by 14anu-ban-01 on (09-04-2015):changed meaning
;The job would suit someone with a business background.[oald]
;नौकरी  उद्योग अनुभव वाले  किसी व्यक्ति के लिये  उपयुक्त होगी .[self] 
(defrule suit4
(declare (salience 0))	;salience reduced from 100 to 0 by 14anu-ban-01  on (09-04-2015)
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayukwa_ho))	;changed  "suviXAjanaka_ho" to "upayukwa_ho" by 14anu-ban-01  on (09-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suit.clp 	suit4 " ?id "  upayukwa_ho )" crlf))	;changed  "suviXAjanaka_ho" to "upayukwa_ho" by 14anu-ban-01  on (09-04-2015)
)

;"suit","V","1.suviXAjanaka_honA"
;Bombay's climate doesn't suit me well.
;--"2.anukUla_honA"
;That colour doesn't suit your complexion.
;

;$$$ Modified by Bhagyashri Kulkarni (3-11-2016)
;Which of these two dresses will suit me better? (rapidex)
;इन दोनों लिबासों में से मुझे  कौनसा बेहतर फबेगा? 
;Modified again by 14anu-ban-01 on (09-04-2015)
;$$$ Modified by 14anu-ban-01 on (12-01-2015)
;@@@ Added by 14anu20 on 23/06/2014 
;Your personality suits you.
;आपका व्यक्तित्व आपको जँचता है .
;आपका व्यक्तित्व आपको फबता है .[Translation added by 14anu-ban-01]
(defrule suit5
(declare (salience 100))	
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 personality|me|you)	;added by 14anu-ban-01 on (09-04-2015) ;changed id2 as ?id1 for object and added 'me|you' by Bhagyashri
;(kriyA-subject ?id ?id2)	;added by 14anu-ban-01 on (09-04-2015) ;commented by Bhagyashri
(kriyA-object  ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Paba/jazca))           ;"Paba" added  by 14anu-ban-01 on (12-01-2015)
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "   suit.clp     suit5   "  ?id "  Paba/jazca )" crlf)       ;"Paba" added  by 14anu-ban-01 on (12-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  suit.clp      suit5   "  ?id " ko )" crlf)
)
)

;@@@ Added by 14anu-ban-01 on (09-04-2015)
;In a pack of cards there are four suits. [suit.clp]
;एक  ताश की गड्डी में चार रङ्ग [के पत्ते]  हैं . [self]
(defrule suit6
(declare (salience 4900))
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(kriyA-aBihiwa  ?id1 ?id)
(kriyA-in_saMbanXI  ?id1 ?id2)
(id-root ?id3 card)
(viSeRya-of_saMbanXI  ?id2 ?id3)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rafga ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suit.clp 	suit6   "  ?id "  rafga  )" crlf))
)



;@@@ Added by 14anu-ban-01 on (09-04-2015)
;The suits are spades, hearts, diamonds and clubs.  [suit.clp]
;ताश के सूट स्पेड,हार्ट,डायमण्ड और क्लब हैं .  [self]
(defrule suit7
(declare (salience 4900))
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 spade|heart|diamond|club)
(subject-subject_samAnAXikaraNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wASa_ke_sUta ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suit.clp 	suit7   "  ?id "  wASa_ke_sUta  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (09-04-2015)
;That colour doesn't suit your complexion. [suit.clp]
;वह रङ्ग आपके रंगरूप के अनुकूल नहीं  है .  [self]
(defrule suit8
(declare (salience 4900))
(id-root ?id suit)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 complexion|skin)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anukUla_ho ))
(assert (kriyA_id-object_viBakwi ?id ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  suit.clp      suit8   "  ?id " ke )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  suit.clp 	suit8  "  ?id "  anukUla_ho  )" crlf))
)


;LEVEL 
;Headword : suit
;
;Examples --
;
;"suit","N","jodZA"
;She bought a expensive suit for her child. 
;usane apane bacce ke liye eka mahazgA sUta KarIxA <--sUta
;
;"2.mukaxamA/xAvA"
;He brought a suit against us in the court.
;usane nyAyAlaya meM hamAre viruXxa mukaxamA calA xiyA <--xAvA loga isa Barose para karawe hEM ki kAnUna unakA sAWa xegA
;
;"3.wASa ke kisI raMga ke pawwoM kA samUha"
;In a pack of cards there are four suits. 
;wASa kI eka gaddI meM cAra raMga howe hEM<--eka raMga ke pawwe eka sAWa calawe hEM 
;
;"suit","V","1.anukUla/upayukwa honA"
;The ten o'clock train will suit us very well.
;xasa baje kI trena hamAre liye balakula anukUla rahegI <--isa vakwa kI trena use sUta karegI
;
;"2.PabanA/sajanA"
;This shirt suits him very much.
;yaha Sarta usa para bahuwa PabawI hE. <--yaha Sarta usake raMga ke sAWa bahuwa acCI jA rahI hE
;
;
;nota:--uparyukwa 'suit'Sabxa bola-cAla kI BARA meM awyaXika pracaliwa hone ke kAraNa  
;     borrow kiyA jA sakawA hE.Ora jahAz para isa Sabxa kA prayoga howA hE usa vAkya     
;     meM arWa ke anusAra 'sUta'kA arWa AsAnI se nikAlA jA sakawA hE
;     awaH sUta ke viBinna arWa sAriNI se nimna prakAra xarSA sakawe hEM
;
;                        pICe jAnA<--sAWa jAnA 
;                                     /\
;                                   /    \ 
;                                 /        \       
;                     eka samUha banAne      kisI vaswu ke pICe
;                     vAlA sAmAna              jAnA 
;                        /\                     /\
;                      /    \                 /    \
;            kapadZoM kA sUta    rahane kA sUta   xAvA    vivAha kA praswAva
;                                         karanA       svIkAra karanA
;    saMkRepa meM"suit"Sabxa kA mUla arWa nimna prakAra xe sakawe hEM  
;
;              
;  sUwra : sUta[<sAWa_yA_pICe_jAnA]  
;
