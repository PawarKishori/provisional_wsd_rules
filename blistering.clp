;@@@Added by 14anu-ban-02(31-03-2015)
;A blistering July day.[oald]	;run the sentence on parser no. 5
;जुलाई का तप्त  दिन . [self]
(defrule blistering1 
(declare (salience 100)) 
(id-word ?id blistering) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 day|heat) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id wapwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  blistering.clp  blistering1  "  ?id "  wapwa )" crlf)) 
)


;@@@Added by 14anu-ban-02(31-03-2015)
;A blistering attack.[oald]	;run the sentence on parser no. 2
;एक गम्भीर हमला . [self]
(defrule blistering2 
(declare (salience 100)) 
(id-word ?id blistering) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 attack) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id gamBIra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  blistering.clp  blistering2  "  ?id "  gamBIra )" crlf)) 
)


;--------------------default_rules-------------------


;@@@Added by 14anu-ban-02(31-03-2015)
;Sentence: The runners set off at a blistering pace.[oald]	;run the sentence on parser no.2
;Translation:धावक बहुत तेज  रफ्तार में दौड़े . [self]
(defrule blistering0 
(declare (salience 0)) 
(id-word ?id blistering) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id bahuwa_wejaZ)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  blistering.clp  blistering0  "  ?id "  bahuwa_wejaZ )" crlf)) 
) 

