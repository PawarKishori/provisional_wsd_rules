
(defrule rouse0
(declare (salience 5000))
(id-root ?id rouse)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id rousing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id SAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  rouse.clp  	rouse0   "  ?id "  SAnaxAra )" crlf))
)

;"rousing","Adj","1.SAnaxAra"
;She got a rousing reception in the party. 
;
(defrule rouse1
(declare (salience 4900))
(id-root ?id rouse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rouse.clp 	rouse1   "  ?id "  jAga )" crlf))
)

;"rouse","V","1.jAganA"
;Their visit roused her from her depression.
;She was roused from her sleep by the door bell ringing.
;The addition of new syllabus is bound to rouse a lot of contradiction.
;--"4.uwwejiwa_kara
;When roused my physics teacher can be quite frightening.
;The new law is bound to rouse the members of  opposition.
;
;@@@ Added by 14anu-ban-10 on (23-03-2015)
;A rousing. [dic.com]
;एक दावत.[manual]
(defrule rouse2
(declare (salience 5100))
(id-root ?id rouse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAvawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rouse.clp 	rouse2   "  ?id "  xAvawa )" crlf))
)
;@@@ Added by 14anu-ban-10 on (23-03-2015)
;When roused my physics teacher can be quite frightening.[hinkhoj]
;उत्तेजित करने पर  मेरे भौतिक विज्ञान शिक्षक काफी भयानक हो  साकते है. [manual]
(defrule rouse3
(declare (salience 5200))
(id-root ?id rouse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viSeRaNa  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  uwwejiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rouse.clp 	rouse3   "  ?id "   uwwejiwa_kara )" crlf))
)
;@@@ Added by 14anu-ban-10 on (23-03-2015)
;Richard couldn't rouse himself to say anything in reply.[hinkhoj]
;रिचर्ड स्वयं को जवाब में कुछ भी कहना से उकसा नहीं  सका . [manual]
(defrule rouse4
(declare (salience 5300))
(id-root ?id rouse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  ukasA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rouse.clp 	rouse4   "  ?id "   ukasA )" crlf))
)
;@@@ Added by 14anu-ban-10 on (23-03-2015)
;To rouse somebody’s anger.[hinkhoj]
;किसी का क्रोध उत्पन्न करना . [manual]
(defrule rouse5
(declare (salience 5400))
(id-root ?id rouse)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1 )
(id-root ?id1 anger)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  uwpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rouse.clp 	rouse5   "  ?id "   uwpanna_kara)" crlf))
)
;@@@ Added by 14anu-ban-10 on (23-03-2015)
;Chris is not easily roused.[hinkhoj]
;क्रिस  को आसानी से भड़काया नहीं  जा सकता है . [manual]
(defrule rouse6
(declare (salience 5500))
(id-root ?id rouse)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  BadZakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rouse.clp 	rouse6   "  ?id "   BadZakA)" crlf))
)
