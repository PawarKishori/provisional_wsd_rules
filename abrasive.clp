;@@@Added by 14anu-ban-02(22-04-2015)
;Rub down with fine abrasive paper.[oald]	;run the sentence on parser no. 2
;पतले खुरदरे कागज से नीचे की ओर घिसिए . [self]
(defrule abrasive2
(declare (salience 100))
(id-root ?id abrasive)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 paper)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KuraxarA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abrasive.clp 	abrasive2   "  ?id "  KuraxarA )" crlf))
)


;@@@Added by 14anu-ban-02(22-04-2015)
;An abrasive tone.[oald]
;कर्कश ध्वनि.[self] 
(defrule abrasive3
(declare (salience 100))
(id-root ?id abrasive)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 tone)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karkaSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abrasive.clp 	abrasive3   "  ?id "  karkaSa )" crlf))
)

;------------------------default_rules-----------------------------

(defrule abrasive0
(declare (salience 0))	;salience reduce to 0  from 5000 by 14anu-ban-02(22-04-2015)
(id-root ?id abrasive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cota_pahuzcAne_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abrasive.clp 	abrasive0   "  ?id "  cota_pahuzcAne_vAlA )" crlf))
)

;"abrasive","Adj","1.cota_pahuzcAne_vAlA"
;We should not have abrasive behavior towards others.
;
(defrule abrasive1
(declare (salience 0))	;salience reduce to 0  from 4900 by 14anu-ban-02(22-04-2015)
(id-root ?id abrasive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIsane_yA_ragadZane_kI_vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  abrasive.clp 	abrasive1   "  ?id "  pIsane_yA_ragadZane_kI_vaswu )" crlf))
)

;"abrasive","N","1.pIsane_yA_ragadZane_kI_vaswu"
;She used a pumic stone as an abrasive to polish the surface of the shelf.
;
