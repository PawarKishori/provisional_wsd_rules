;@@@ Added by 14anu-ban-05 on (09-09-2015)
;Unemployment is now rampant in most of Europe.[COCA]
;बेरोजगारी अधिकांश  यूरोप में अब तेजी से फैलने वाला है . [self]
(defrule rampant0
(declare (salience 100))
(id-root ?id rampant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_PElane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rampant.clp 	rampant0   "  ?id "  wejI_se_PElane_vAlA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-09-2015)
;Rampant inflation means that our wage increases soon become worth nothing. [CALD]
;अनियंत्रित महँगाई  का मतलब है कि हमारे मजदूरी में संवृद्धि  जल्द ही नगण्य हो जाते हैं [Manual]
(defrule rampant1
(declare (salience 101))
(id-root ?id rampant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 inflation)	;more words can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aniyaMwriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rampant.clp 	rampant1   "  ?id "  aniyaMwriwa )" crlf))
)

