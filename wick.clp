
;@@@ Added by 14anu-ban-11 on (11-03-2015)
;The physician put a wick in the wound to drain it.(hinkhoj)
;चिकित्सक ने  घाव  को सुखाने के लिये  पट्टी बाँधी. (self)
(defrule wick1
(declare (salience 10))
(id-root ?id wick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-in_saMbanXI  ?id1 ?id2)
(id-root ?id1 put)
(id-root ?id2 wound)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pattI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  wick.clp  	wick1   "  ?id "  pattI)" crlf))
)


;@@@ Added by 14anu-ban-11 on (11-03-2015)
;You need a new wick for the lantern.(hinkhoj)
;आपको लालटेन के लिए एक नयी बत्ती की आवश्यकता है . (self)
(defrule wick0
(declare (salience 00))
(id-root ?id wick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawwI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  wick.clp  	wick0  "  ?id "  bawwI)" crlf))
)



