
(defrule pledge0
(declare (salience 5000))
(id-root ?id pledge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pledge.clp 	pledge0   "  ?id "  praNa )" crlf))
)

(defrule pledge1
(declare (salience 4900))
(id-root ?id pledge)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawijFA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pledge.clp 	pledge1   "  ?id "  prawijFA_kara )" crlf))
)

(defrule pledge2
(declare (salience 4800))
(id-root ?id pledge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawijFA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pledge.clp 	pledge2   "  ?id "  prawijFA_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on 9-9-14
;The African Union has pledged $1m (£600,000) to help fight the disease. [Anusaaraka_Bing_14th_Aug_2014]
;अफ्रीकी संघ ने बीमारी से लड़ने वालो की मदद करने के लिए  1m$ (£600.000) देने की प्रतिज्ञा ली।  [Own Manual]


(defrule pledge3
(declare (salience 5000))
(id-root ?id pledge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id ?id1)
(id-root ?id1 help)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawijFA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pledge.clp 	pledge3   "  ?id "  prawijFA )" crlf))
)

;"pledge","V","1.prawijFA_kara[ho]"
;He pledged to fight for his nation's security.
;
;
