;$$$ Modified by 14anu21 on 18.06.2014
;$$$ Modified by 14anu-ban-06   (05-08-2014)
;combined two rules hard0 and hard1.
;added category and relation between ?id and ?id1.
; He is working hard.
;vaha kaTora pariSrama kara rahA hE.
; He is studying hard.
;vaha kaTora aXyayana kara rahA hE.
;The construction work of Ellora Caves have been done by chiselling high - hard rocks.(tourism corpus)
;एलोरा गुफाओं का निर्माण कार्य ऊँची-कठोर चट़्टानों को काटकर किया गया है .(tourism corpus)
(defrule hard0
(declare (salience 5000))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id) ; added by 14anu-ban-06,  14anu21
(id-cat_coarse ?id adverb)
(id-root ?id1 work|study|chisel) ;added 'study' by 14anu-ban-06,  14anu21;added 'chisel' by 14anu-ban-06 (27-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard0   "  ?id "  kaTora )" crlf))
)

;@@@ Added by 14anu21 on 18.06.2014
;I was resting on a hard surface.
;मैं एक सख्त सतह पर विश्राम कर रहा था. 
;मैं एक मुश्किल सतह पर टिक जाया था . (Translation before adding rule newhard1)
(defrule newhard1
(declare (salience 5000))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 surface)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saKwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	newhard1   "  ?id "  saKwa )" crlf))
)


;@@@ Added by 14anu-ban-06    (05-08-2014)
;Layers of snow start forming on this hard snow .  (Parallel corpus)
;इस कठोर बर्फ़ पर बर्फ़ की परतें बनने लगती हैं ।
;When only the blade of knife can enter in it then it is considered extremely hard snow . (Parallel Corpus)
;जब चाकू का ब्लेड ही उसमें घुस सके तो उसे बहुत कठोर बर्फ़ माना जाता है ।
;A rigid body generally means a hard solid object having a definite shape and size.(NCERT)  ;added by 14anu-ban-06 (07-11-2014)
;sAXAraNawayA xqDa piMda kA arWa howA hE eka EsA kaTora Tosa paxArWa jisakI koI niSciwa Akqwi waWA AkAra ho. (NCERT)
(defrule hard00
(declare (salience 4900))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 snow|object)   ;added object by 14anu-ban-06 (07-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard00   "  ?id "  kaTora )" crlf))
)

; He is working hard.
; He is studying hard.
(defrule hard2
(declare (salience 4800))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective|adjective_comparative|adjective_superlative);"_coarse" is added by sheetal:The last part of the course was hard because I was running against the wind.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard2   "  ?id "  kaTina )" crlf))
)

(defrule hard3
(declare (salience 4700))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard3   "  ?id "  kaTina )" crlf))
)

;Meaning is modified by sheetal
(defrule hard4
(declare (salience 4600))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard4   "  ?id "  kaTina )" crlf))
)

;"hard","Adv","1.kaTina"
;He tried hard to finish the work in time.
;
;

;Added by Prachi Rathore[23-11-13]
;She hit him hard with her purse. [m-w]
(defrule hard5
(declare (salience 4900))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-with_saMbanXI  ?id ?)(kriyA_viSeRaNa-kriyA_viSeRaNa_viSeRaka  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard5   "  ?id "  jora_se )" crlf))
)

;$$$ Modified by 14anu-ban-06   (05-08-2014) added (id-root ?id1 day|night|year|month)
;@@@ Added by Prachi Rathore[23-11-13]
;I've had a long hard day.[oald]
;merA eka lambA muSkila dina rahA.
(defrule hard6
(declare (salience 4900))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 day|night|year|month) ;added by 14anu-ban-06 (5-08-14) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muSkila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard6   "  ?id "  muSkila )" crlf))
)

;$$$ Modified by 14anu-ban-06 (08-09-2014)
;She turned her lantern on the monk and seeing how handsome he was , invited him to go with her to her house , for " this hard and rough ground is no bed for you . "(parallel corpus)
;उसने जब भिक्षु की ओर अपना दीर्पदान मोडऋआ तो उस सुंदर भिक्षु को देखकर हैरान रह गऋ . उसने उसे अपने यहां आने का आमंत्रण दिया और कहा , ऋयह कठोर और खुरदरी जमीन तुम्हारी शय्या नहीं . "
;@@@ Added by Prachi Rathore
;Her eyes were cruel and hard.[oald]
;उसकी आँखें क्रूर और कठोर थीं . 
(defrule hard7
(declare (salience 4900))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(conjunction-components  ? $?ids)
(id-root ?id1 cruel|rough);added 'rough' by 14anu-ban-06
(test (integerp (member$ ?id $?ids)));added by 14anu-ban-06
(test (integerp (member$ ?id1 $?ids)));added by 14anu-ban-06
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard7   "  ?id "  kaTora )" crlf))
)

;@@@ Added by 14anu-ban-06 (01-08-2014)
;Thus, when you push hard against a rigid brick wall, the force you exert on the wall does no work. (NCERT)
;jaba kaBI Apa kisI INtoM kI xqDa xIvAra ko jora se XakkA xewe hEM wo koI kArya nahIM howA hE.
(defrule hard8
(declare (salience 5000))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 push)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard8   "  ?id "  jora_se )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-09-2014)
;The hoofs are hard , black with narrow clefts .(parallel corpus)
;खुर कठोर , काले तथा संकीर्ण विदरवाले होते हैं .
;Stones of Parasnath are hard in comparison to stones of the surroundings so their erosion happens more in comparison to stones of this mountain .(parallel corpus)
;पारसनाथ के पत्थर आसपास के पत्थरों की तुलना में कठोर  हैं अतः उनका अपरदन इस पहाड़ के पत्थरों की तुलना में अधिक हो जाता है ।
(defrule hard9
(declare (salience 4900))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 hoof|stone)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard9   "  ?id "  kaTora)" crlf))
)

;$$$ Modified by 14anu-ban-06 (13-12-2014)
;@@@ Added by avni (14anu11) on 20/06/2014
;He's very hard working.(COCA);added by 14anu-ban-06 (13-12-2014)
;वह बहुत परिश्रमी है . (manual)
(defrule hard10
(declare (salience 5000))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id2) ; added by  14anu-ban-06 (13-12-2014)
(subject-subject_samAnAXikaraNa ?id2 ?id); added by  14anu-ban-06 (13-12-2014)
(id-root ?id1 work)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pariSramI));meaning changed from 'pariRrmI' to 'pariSramI' by 14anu-ban-06 (13-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard10  "  ?id " pariSramI  )" crlf))
)


;@@@ Added by 14anu11
;The sound ' box ' is a large pumpkin of about ninety centimeters in girth ; this fruit with a particularly hard shell is grown extensively
; in Maharashtra , near Pandharpur , though a certain amount was imported from African countries like Zanzibar ( Zaire ) 
;महाराष्ट्र में पंडरपुर के निकट विशेष रूप से कडे छिलके वाला यह फल बहुतायत में उगता है हालांकि कुछ मात्रा में जाजिबार ( जायर ) से इसका आयात भी किया जाता था .
;सूखने के लिए फल को धुंधुआती आंच के वाद्य यंत्र काफी ऊपर लटका दिया जाता है , कई वर्षों में यह पर्याप्त सूख जाता है .
(defrule hard11
(declare (salience 5000))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRaNa-viSeRaka  ?id ?id2)
(viSeRya-viSeRaka  ?id ?id2)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard11   "  ?id "  kaTora )" crlf))
)

;@@@Added by 14anu15 Gourav Sahni  (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;You must try harder.
;आपको कड़ी मेहनत करनी चाहिए.
(defrule hard12
(declare (salience 4800))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root =(- ?id 1) try)
(kriyA-kriyA_viSeRaNa  =(- ?id 1) ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard12   "  ?id "  kadZI )" crlf))
)

;$$$Modified by 14anu-ban-02(27-02-2016)
;###[COUNTER SENTENCE] The last part of the course was hard because I was running against the wind .[sd_verified]
;###[COUNTER SENTENCE] दौड़ने के लिये बने मार्ग का आखिरी भाग कठिन था क्योंकि मैं हवा के विरुद्ध दौड़ रहा था . [sd_verified]
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;This wood is very hard.
;यह लकडी अत्यन्त सख्त है . 
(defrule hard13
(declare (salience 4900))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 wood)	;Added by 14anu-ban-02(27-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saKwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard13   "  ?id "  saKwa )" crlf))
)

;@@@ Added by 14anu15 Gourav Sahni 14anu15 (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;It is very hard to believe you. 
;आपको मानना अत्यन्त मुश्किल है . 
(defrule hard14
(declare (salience 4950))  ;salience increased from '4900' by 14anu-ban-06 (13-12-2014)
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ? ?id)
(saMjFA-to_kqxanwa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muSkila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard14   "  ?id "  muSkila )" crlf))
)

;$$$ Modified by 14anu-ban-06 (09-12-2014)
;### [COUNTER EXAMPLE] ### The construction work of Ellora Caves have been done by chiselling high - hard rocks.(tourism corpus)
;### [COUNTER EXAMPLE] ### एलोरा गुफाओं का निर्माण कार्य ऊँची-कठोर चट़्टानों को काटकर किया गया है .(tourism corpus)
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;It was raining hard when we set off. 
;यह जोड से वर्षा हो रहा था जब हम रवाना हुए . 
;जोर से  वर्षा हो रही थी जब हम रवाना हुए . (manual);added by 14anu-ban-06 (09-12-2014)
(defrule hard15
(declare (salience 4700));salience reduced to avoid clash with 'hard12' by 14anu-ban-06 (09-12-2014)
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id);added 'id1' by 14anu-ban-06 (27-01-2015)
(id-root ?id1 rain)		;added by 14anu-ban-06 (27-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se));meaning changed from 'jodZa_se' to 'jora_se by 14anu-ban-06 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard15   "  ?id "  jora_se )" crlf));meaning changed from 'jodZa_se' to 'jora_se by 14anu-ban-06 (09-12-2014)
)

;$$$ Modified by 14anu-ban-06 (09-12-2014)
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;Do not hit it so hard!
;इसपर इतनी जोड से प्रहार मत कीजिए! 
;इसपर इतनी जोर से प्रहार मत कीजिए! (manual);added by 14anu-ban-06 (09-12-2014)
(defrule hard16
(declare (salience 4800))
(id-root ?id hard)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa  ? ?id)
(viSeRya-viSeRaka ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se));meaning changed from 'jodZa_se' to 'jora_se by 14anu-ban-06 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hard.clp 	hard16   "  ?id "  jora_se )" crlf));meaning changed from 'jodZa_se' to 'jora_se by 14anu-ban-06 (09-12-2014)
)
