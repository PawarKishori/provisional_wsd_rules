
(defrule us0
(declare (salience 5000))
(id-root ?id us)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse =(- ?id 1) determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id amarikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  us.clp 	us0   "  ?id "  amarikA )" crlf))
)

;$$$ Modified by Shirisha Manju Suggested by Chaitanya Sir 12-08-2016
;### Counter Ex : He has neither the time, nor the interest to bother about us!
;                 usake pAsa hamAre bAre meM ciMwiwa hone ke lie koI_BI_nahIM samaya, na rUci howA hE! (Anusaaraka)
(defrule us1
(declare (salience 4900))
(id-root ?id us)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
(pada_info (group_head_id ?id)(preposition 0)) ; added by Shirisha Manju Suggested by Chaitanya Sir
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hama))
(assert (id-H_vib_mng ?id ko))
(assert (id-wsd_number ?id p))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  us.clp 	us1   "  ?id "  hama )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* "  us.clp        us1   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number   " ?*prov_dir* "  us.clp        us1   "  ?id " p )" crlf))
)

;default_sense && category=pronoun	hama{vib:ko}	0
;"us","Pron","1.hameM/hama_ko"
;We took our pets with us for picnic.
;


;Modified by Meena(6.8.11)
;Let us go for a walk.
(defrule us2
(declare (salience 4900))
(id-root ?id us)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) let)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id caliye_hama))
(assert (id-wsd_number ?id p))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) caliye_hama))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " us.clp  us2  "  ?id "  " (- ?id 1) "  caliye_hama  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number  " ?*prov_dir* "  us.clp     us2   "  ?id " p )" crlf) ;added by Shirisha Manju
)
;(assert (root_id-TAM-vachan ?id p))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  us.clp        us2   "  ?id "  caliye_hama)" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number   " ?*prov_dir* "  us.clp        us2   "  ?id " p )" crlf))
 ; Added by Shirisha Manju (08-12-09) (suggested by Sheetal)
 ; Ex : Let us go to the market.

;@@@ Added by Shirisha Manju Suggested by Chaitanya Sir 12-08-2016
;Ex : He has neither the time, nor the interest to bother about us!
;     usake pAsa hamAre bAre meM ciMwiwa hone ke lie koI_BI_nahIM samaya, na rUci howA hE! (Anusaaraka)
(defrule us3
(declare (salience 4900))
(id-root ?id us)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
(pada_info (group_head_id ?id)(preposition ?p&~0)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hama))
(assert (id-wsd_number ?id p)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  us.clp       us3   "  ?id "  hama )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number   " ?*prov_dir* "  us.clp        us3   "  ?id " p )" crlf))
)


