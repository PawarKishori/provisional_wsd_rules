
(defrule focus0
(declare (salience 5000))
(id-root ?id focus)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id focussed )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sAPa_xiKanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  focus.clp  	focus0   "  ?id "  sAPa_xiKanA )" crlf))
)

;"focussed","Adj","1.sAPa xiKanA"
;As he regained consciousness, his eyes slowly got focussed on his friend standing in the room.
;
(defrule focus1
(declare (salience 4900))
(id-root ?id focus)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id keMxra_biMxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  focus.clp 	focus1   "  ?id "  keMxra_biMxu )" crlf))
)

;"focus","N","1.keMxra_biMxu"
;He was the focus of all attention at the party.
;He has taken the photographs without properly focussing his camera causing the images to blurr.
;
(defrule focus2
(declare (salience 4800))
(id-root ?id focus)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id keMxriwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  focus.clp 	focus2   "  ?id "  keMxriwa_ho )" crlf))
)

;"focus","V","1.keMxriwa_honA"
;Since he was the chairman, all praises for the better performance of the company were focussed on him.
;The speakers are kept in such a way that the sound is focussed at the centre of the room.
;--"2.sAPa_xiKAI_xenA"
;He fell as he could not focus his eyes in the semidarkness of the hall.
;--"3.kenxriwa_karanA"
;Please focus the lense of the microscope to see the specimen clearly.
;

;@@@ Added by 14anu-ban-05 on (25-09-2014)
;For a circle, the two foci merge into one and the semi-major axis becomes the radius of the circle.[NCERT]
;kisI vqwwa ke lie xonoM nABiyAz eka xUsare meM vilIna hokara eka ho jAwI hEM waWA arXa xIrGa akRa vqwwa kI wrijyA bana jAwI hE.
;To be tested
(defrule focus3
(declare (salience 5000))
(Domain physics)
(id-word ?id foci)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_word_mng ?id nABiyAz))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  focus.clp     focus3   "  ?id "  nABiyAz )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  focus.clp       focus3   "  ?id "  physics )" crlf)
)


;@@@ Added by 14anu-ban-05 on (25-09-2014)
;We shall maintain our focus on the needs of the customer.[OALD]
;हम ग्राहकों की जरूरतों पर अपना ध्यान केंद्रित रखेगें [MANUAL]
(defrule focus4
(declare (salience 5000))
(id-root ?id focus)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 maintain|keep)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_keMxriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  focus.clp 	focus4   "  ?id "  XyAna_keMxriwa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-09-2014)
;It lets light out of the eye and focus rays .[PARARLLEL]
;यह रोशनी को अपने अंदर से निकलने देती है और किरणों को केंद्रित करती है ।
(defrule focus5
(declare (salience 5000))
(id-root ?id focus)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)	
(id-root ?id1 rays|attention)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id keMxriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  focus.clp 	focus5   "  ?id "  keMxriwa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-10-2014)
;A good strategy is to focus first on the essential features, discover the basic principles and then introduce corrections to build a more refined theory of the phenomenon.[NCERT]
;eka acCI yukwi vahI hE ki pahale kisI pariGatanA ke paramAvaSyaka lakRaNoM para XyAna keMxriwa karake usake mUla sixXAnwoM ko KojA jAe Ora Pira saMSuxXiyoM ko sanniviRta karake usa pariGatanA ke sixXAnwoM ko Ora aXika pariSuxXa banAyA jAe.[NCERT]
(defrule focus6
(declare (salience 5000))
(Domain physics)
(id-root ?id focus)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_keMxriwa_kara))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  focus.clp 	focus6  "  ?id "   XyAna_keMxriwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  focus.clp       focus6   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (14-11-2014)
;Electron beams can be focussed by properly designed electric and magnetic fields.[NCERT]
;ilektroYna puFjoM ko uciwa rIwi se aBikalpiwa vExyuwa evaM cumbakIya kRewroM xvArA Pokasiwa kiyA jA sakawA hE.[NCERT]
(defrule focus7
(declare (salience 5000))
(Domain physics)
(id-root ?id focus)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 beam)		;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id Pokasiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  focus.clp 	focus7  "  ?id "   Pokasiwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  focus.clp       focus7   "  ?id "  physics )" crlf))
)

