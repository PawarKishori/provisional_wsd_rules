;@@@ Added by 14anu-ban-09 on (18-03-2015)
;Rock stars have to get used to being plagued by autograph hunters.	[oald]
;Rockstars को हस्ताक्षर शिकारियों के द्वारा परेशान होने की आदत हो गई है .		[manual]  

(defrule plague1
(declare (salience 2000))
(id-root ?id plague)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 hunter)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)
(id-root ?id2  autograph)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plague.clp 	plague1  "  ?id "  pareSAna_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-03-2015)
;Celebrity plagued by fans.		[oald-Modified]
;प्रतिष्ठित व्यक्ति प्रशंसको के द्वारा परेशान हो गये थे.	[manual]

(defrule plague2
(declare (salience 1050))
(id-root ?id plague)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(or(id-root ?id1 fan|hunter)(id-word ?id1 autograph-hunter|autograph-hunters))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plague.clp 	plague2   "  ?id "  pareSAna_ho_jA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-03-2015)
;Financial problems are plaguing the company.		[oald]
;आर्थिक समस्या उद्योग का विनाश कर रही हैं.			[Manual]

(defrule plague3
(declare (salience 1000))
(id-root ?id plague)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 company)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinASa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plague.clp 	plague3   "  ?id "  vinASa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-03-2015)
;The curse of celebrity plagued them for the rest of their eve.	[Archer-Jacky Gray]
;प्रतिष्ठित व्यक्ति के अपशब्द ने उनकी बची हुई पूर्वसन्ध्या में उनको तंग किया .		[Manual] 

(defrule plague4
(declare (salience 1000))
(id-root ?id plague)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 curse)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 star|celebrity|rockstar)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waMga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plague.clp 	plague4   "  ?id "  waMga_kara )" crlf))
)




;@@@ Added by 14anu-ban-09 on (18-03-2015)
;Kim Kardashian wasn't the only celebrity plagued by Vitalii Sediuk's antics for fashion month.   [http://fashionbombdaily.com/tag/dov-charney/]
;किम कॉडशॆन एकमात्र प्रतिष्ठित व्यक्ति नहीं थी जो वितालीई सेडीयुक के अद्भुत फैशन month के द्वारा परेशान हो गयी थी  . 		  [Manual]


(defrule plague5
(declare (salience 1000))
(id-root ?id plague)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 star|celebrity|rockstar)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plague.clp 	plague5  "  ?id "  pareSAna_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (18-03-2015)
;NOTE:- Run on parser no. 2.
;Heather Mills Another celebrity plagued by nanny trouble: Heather Mills , the ex-wife of former Beatle Paul McCartney. [http://abcnews.go.com/topics/entertainment/heather-mills.htm]
;हेदर मिल्स एक अन्य प्रतिष्ठित व्यक्ति nanny trouble के द्वारा  परेशान किया गया, जो भूतपूर्व बीटल पॉल मकॉर्ट्नी की ex-wife हैं.		[manual]	;Translation needs improvement

(defrule plague6
(declare (salience 1000))
(id-root ?id plague)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 star|celebrity|rockstar)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plague.clp 	plague6  "  ?id "  pareSAna_kara )" crlf))
)

;-----------------------------DEFAULT RULE------------------------------------

;@@@ Added by 14anu-ban-09 on (18-03-2015)
;Financial problems have been plaguing their new business partners.     [cald]
;आर्थिक समस्याएँ उनके नये व्यापारिक सहयोगीयो को त्रस्त करती रहती हैं .                       [manual] 

(defrule plague0
(declare (salience 000))
(id-root ?id plague)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wraswa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plague.clp   plague0   "  ?id "  wraswa_kara )" crlf))
)

