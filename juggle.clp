;@@@ Added by 14anu-ban-06 (02-03-2015)
;My uncle taught me to juggle. (OALD)
;मेरे चाचा ने मुझे करतब करना सिखाया . (manual)
;We all watched in amazement as he juggled with three flaming torches.(cambridge)
;हमने सब ने अचम्भे में देखा जब उसने तीन आग सी टॉर्च से करतब किया . (manual)
(defrule juggle0
(declare (salience 0))
(id-root ?id juggle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karawaba_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  juggle.clp 	juggle0   "  ?id "  karawaba_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-03-2015)
;Many parents find it hard to juggle children and a career.(cambridge)
;बहुत सारे माँ बाप बच्चे और जीविका एक साथ सम्भालना  कठिन पाते हैं . (manual)
(defrule juggle1
(declare (salience 2000))
(id-root ?id juggle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 career|child|job)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id eka_sAWa_samBAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  juggle.clp 	juggle1   "  ?id "  eka_sAWa_samBAla )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-03-2015)
; It won't matter if we juggle the figures - no one will know.(cambridge)
;यह महत्त्वपूर्ण नहीं होगा यदि हम आंकड़े बदल देते हैं  - कोई भी नहीं जाने पायेगा . (manual)
(defrule juggle2
(declare (salience 2500))
(id-root ?id juggle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-word ?id1 figures)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  juggle.clp 	juggle2   "  ?id "  baxala_xe )" crlf))
)
