

;$$$ Modified by 14anu-ban-10 on (06-01-2015)
;@@@ Added by 14anu07 0n 04/07/2014
;This is the last resort. 
;यह अखिरी उपाय है . 
(defrule resort2
(declare (salience 5100))
(id-root ?id resort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) last|first|final)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) upAya)) ;added by 14anu-ban-10 on (06-01-2015)
;(assert (id-wsd_root_mng ?id upAya)) ; commented out by 14anu-ban-10 on (06-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " resort.clp  resort2  "  ?id "  " - ?id 1 "  upAya ) " crlf)) ;added by 14anu-ban-10 on (06-01-2015)
)


;@@@Added by 14anu07 0n 04/07/2014
;The resort can accommodate upto 100 guests.
;सैरगाह 100 अतिथि तक समायोजित सकता है . 
(defrule resort3
(declare (salience 5000))
(id-root ?id resort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) The|the)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  sEragAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resort.clp 	resort3   "  ?id "   sEragAha)" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-02-2015)
;In the Idukki district of Kerala the hill resort of Munnar situated at an altitude of 1520 metres above the sea level is known as the Kashmir of South India.[tourism corpus]
;केरल  के  इडूकी  ज़िले  में  समुद्रतल  से  1,524  मीटर  की  ऊँचाई  पर  स्थित  मुन्नार  पर्वतीय  स्थल  दक्षिण  भारत  के  कश्मीर  के  रूप  में  जाना  जाता  है  ।[tourism corpus]
(defrule resort4
(declare (salience 5200))
(id-root ?id resort)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 hill)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resort.clp 	resort4   "  ?id "  sWala)" crlf))
)

;------------------- Default Rules -------------------
(defrule resort0
(declare (salience 5000))
(id-root ?id resort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ASraya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resort.clp 	resort0   "  ?id "  ASraya )" crlf))
)
;"resort","N","1.ASraya"
;A beach to which many people resort
;--"2.aMwima_cArA"
;I have come to you for advice as a last resort.
;

(defrule resort1
(declare (salience 4900))
(id-root ?id resort)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SaraNa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  resort.clp 	resort1   "  ?id "  SaraNa_le )" crlf))
)

;"resort","VT","1.SaraNa_lenA"
;If negotiations fail,we will have to resort to hunger strike. 
;
