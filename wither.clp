;##############################################################################
;#  Copyright (C) 2013-2014 Pramila (pramila3005 @gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@ Added by Pramila(BU) on 19-03-2014
;All the flowers have withered.  ;shiksharthi
;सारे फूल मुरझा चुके है.

(defrule wither1
(declare (salience 4900))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 flower|leaf|plant|tree)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muraJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither1   "  ?id "  muraJA )" crlf)
))

;@@@ Added by Pramila(BU) on 19-03-2014
;The sun has withered these plants.  ;shiksharthi
;धूप ने इन पौधो को कुम्हला दिया है.
(defrule wither2
(declare (salience 5000))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 sun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kumhalA_xe))
(assert (kriyA_id-subject_viBakwi ?id ne))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither2   "  ?id "  kumhalA_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  wither.clp 	wither2   "  ?id " ne )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  wither.clp 	wither2   "  ?id " ko )" crlf)
))

;@@@ Added by Pramila(BU) on 19-03-2014
;The cold withered the leaves.  ;shiksharthi
;सर्दी ने पत्तियों को मुरझा दिया है
(defrule wither3
(declare (salience 4950))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 flower|leaf|plant|tree)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muraJA_xe))
(assert (kriyA_id-subject_viBakwi ?id ne))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither3   "  ?id "  muraJA_xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  wither.clp 	wither3   "  ?id " ne )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  wither.clp 	wither3   "  ?id " ko )" crlf)
))

;@@@ Added by Pramila(BU) on 19-03-2014
;After his failure he has withered.   ;shiksharthi
;उसकी असफलता के बाद वह शिथिल पड़ गया है.
(defrule wither4
(declare (salience 4900))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id siWila_padZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither4   "  ?id "  siWila_padZa )" crlf)
))


;@@@ Added by 14anu-ban-11 on (13-04-2015)
;All our hopes just withered away.(oald)
;हमारी सभी आशाएँ  खत्म हो गईं है. (self)
(defrule wither7
(declare (salience 101))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 hope)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kawma_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither7   "  ?id "  Kawma_ho_jA)" crlf)
))


;@@@ Added by 14anu-ban-11 on (13-04-2015)
;His lower limbs are withered by polio.(coca)
;उसके नीचे के अङ्ग पोलियो के द्वारा खराब हो गये हैं . (self)
(defrule wither8
(declare (salience 102))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 limb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither8   "  ?id "  KarAba_ho)" crlf)
))


;@@@ Added by 14anu-ban-11 on (13-04-2015)
;As the river withered, so did recreational use.(oald)
;जब नदी सूख जायेगी,तो उसका उपयोग होगा . (self) 
(defrule wither9
(declare (salience 103))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 river)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUKa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither9   "  ?id "  sUKa_jA)" crlf)
))


;@@@ Added by 14anu-ban-11 on (13-04-2015)
;His face went all withered and serious.(coca)
;उसका चेह्रा पूरा झुर्रीदार और गम्भीर हो गया है . (self)
(defrule wither10
(declare (salience 10))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 face)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JurrIxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither10   "  ?id "  JurrIxAra)" crlf))
)

;@@@ Added by 14anu-ban-11 on (13-04-2015)  ;Note:- Working properly on parser no. 8.
;All flowers are get withered as the pluck off. (coca)
;सभी फूल  टूटने के बाद जैसा मुरझा जाते हैं . (self)
(defrule wither12
(declare (salience 101))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
(id-root ?id1 pluck)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muraJA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither12  "  ?id "  muraJA_jA)" crlf))
)



;-----------default rules------------------------------
;@@@ Added by Pramila(BU) on 19-03-2014
;Her beauty is withering.   ;shiksharthi
;उसकी सुंदरता मलीन हो रही है.
(defrule wither5
(declare (salience 100))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id malIna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither5   "  ?id "  malIna_ho )" crlf)
))

;@@@ Added by Pramila(BU) on 19-03-2014
(defrule wither6
(declare (salience 0))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id malIna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither6   "  ?id "  malIna_ho )" crlf)
))


;@@@ Added by 14anu-ban-11 on (13-04-2015)
;He's just some old guy with a withered twig.(coca)
;वह बूढ़े लोग कुछ पुराने फ़ैशन के साथ है . (self)
(defrule wither11
(declare (salience 00))
(id-root ?id wither)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id purAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wither.clp 	wither11   "  ?id "  purAnA)" crlf))
)
 
;----------------------------------------------------
