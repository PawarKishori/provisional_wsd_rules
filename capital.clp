(defrule capital0
(declare (salience 5000))
(id-root ?id capital)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUzjI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capital.clp 	capital0   "  ?id "  pUzjI )" crlf))
)


;$$$ Modified  by 14anu-ban-03 (06-04-2015)
;$$$  Modified by Preeti(10-12-13)
;We require a huge capital to start a business. [old clp]
;hameM uxyoga Suru karane ke lie eka viSAla pUMjI jarUrI howe hEM.
;hameM uxyoga Suru karane ke lie eka viSAla pUzjI jarUrawa howI hEM.  ;by 14anu-ban-03 (06-04-2015)
(defrule capital1
(declare (salience 4950))
(id-root ?id capital)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-object  ?id1 ?id) (saMjFA-to_kqxanwa  ?id ?id1))
(id-root ?id1  start|require)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUzjI))   ;meaning changed from 'pUMjI' to 'pUzjI' by 14anu-ban-03 (06-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capital.clp 	capital1   "  ?id "  pUzjI )" crlf))
)

;@@@ Added by 14anu-ban-03 (05-09-2014)
;And this change in thinking of human capital. [karan singla]
;और मानवीय पूँजी की यह सोच में परिवर्तन.
(defrule capital4
(declare (salience 5000))
(id-root ?id capital)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUzjI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capital.clp 	capital4   "  ?id " pUzjI )" crlf))
)

;@@@ Added by Preeti(10-12-13)
;Troops are stationed in and around the capital. 
;senA ko rAjaXAnI meM Ora usake Asa pAsa  wEnAwa kiyA gayA hE.
(defrule capital2
(declare (salience 4900))
(id-root ?id capital)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAjaXAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capital.clp 	capital2   "  ?id "  rAjaXAnI )" crlf))
)

;$$$ Modified by 14anu-ban-03(05-09-2014)
;### COUNTER EXAMPLE ### ;And this change in thinking of human capital. [karan singla]
;@@@ Added by Preeti(10-12-13)
;Please write in capitals. 
;kqpayA baDe akRaroM meM liKie.
(defrule capital3
(declare (salience 5000))
(id-root ?id capital)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(or(and(samAsa_viSeRya-samAsa_viSeRaNa  ? ?id) ;commented by 14anu-ban-03
;(kriyA-in_saMbanXI  ?id1 ?)) ;commented by 14anu-ban-03
(kriyA-in_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZe_akRara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capital.clp 	capital3   "  ?id "  baDZe_akRara)" crlf))
)

;@@@ Added by (14anu11)
; Use lower case rather than capitals.
;कैपिटल्स ( अंग्रेजी के बडे अक्षरों ) के बजाय छोटे अक्षरों का प्रयोग करें .
(defrule capital04
(declare (salience 5000))
(id-root ?id capital)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  kEpitalsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capital.clp 	capital04   "  ?id "   kEpitalsa )" crlf))
)

;@@@ Added by 14anu-ban-03 (06-04-2015)
;The capital city is swarming with police.[oald]
;राजधानी पुलिस से भर रही है . [self]
(defrule capital5
(declare (salience 5000))
(id-root ?id capital)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
(id-root ?id1 city)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 rAjaXAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " capital.clp   capital5  "  ?id "  " ?id1 "  rAjaXAnI  )" crlf))
)

;@@@ Added by Manasa ( 26-02-2016 )
;Capital from foreign countries may flow into the domestic country, or the domestic country may be exporting capital to foreign countries.
;विदेशी देशों से राजधानी घरेलू देश में बह सकती है, या घरेलू देश राजधानी विदेशी देशों में निर्यात कर . 
(defrule capital06
(declare (salience 5000))
(id-root ?id capital)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  saMpawwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  capital.clp  capital06   "  ?id "   saMpawwi )" crlf))
)



;################### Old Examples ####################
;default_sense && category=noun	rAjaXAnI	0
;"capital","N","1.rAjaXAnI"
;Delhi is the capital of India.
;--"2.pUzjI/mUlaXana"
;We require a huge capital to start a business.
;
;
;LEVEL 
;
;
;                    sUwra (nibanXa)
;capital adj
;badZA akRara 
;1.Proper nouns are always written in capital letters. 
;
;mqwyu xanda
;2.Some citizens do not believe in capital punishment for murder 
;
;rAjZaXAnI N 
;3.Delhi is the capital of India.
;
;pUzjI/mUlaXana
;4.We require a huge capital to start a business. 
;---------------------------------------------------------------
;
;sUwra : rAjaXAnI[<badA]/mUlapUzjI
;------------------
;
;vAkyoM ke AXAra para vivaraNa--
;---------------------
;--  1,3,4 vAkyoM ke arWa sUwra meM sAkRAw A gaye hE .        
;--  2 ke lie jahAz BI xaNda ke lie isa Sabxa kA prayoga hogA, vahAz xaNdavAcI
;    Sabxa BI hogA . isa kAraNa `badA' Sabxa se arWa boXa sugama hogA . 
; 
;sUwra-saMGaTaka waWya--
;--------------
;-- ye saBI prayoga `kEpitala' Sabxa ke viswAra hEM,jisakA mUlArWa `badA' yA `muKya'
;   socA jA sakawA hE . 
;-- `rAjaXAnI', Ora nagaroM kI apekRA badI yA muKya howI hE . 
;-- aMgrejI meM jina kEpitala akRaroM-`bade akRara' kI paripAtI hE, veM akRara 
;   sAkRAw bade xIKawe hEM .  
;-- mUla pUzjI ko muKya yA badA kahA jAnA hI cAhie . kAraNa- usake kAraNa
;   hI anya Xana kI bAwa socI jA sakawI hE . 
;-- jo BI xaNda xiye jAwe hEM, unameM mqwyu-xaNda sabase badA mAnA jAwA hE . awaH
;   mqwyuxaNda ke rUpa meM prayoga .
;
