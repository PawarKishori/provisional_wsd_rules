;@@@ Added by 14anu-ban-06 (14-04-2015)
; Her interpretation of Juliet was one of the best performances I have ever seen. (cambridge)
;उसका जूलीएट का प्रस्तुतीकरण सबसे सर्वोत्तम प्रदर्शनों में से एक था जिन्हें मैंने सबसे अच्छे तरीके से देखा . (manual)
(defrule interpretation1
(declare (salience 2000))
(id-root ?id interpretation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwIkaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interpretation.clp 	interpretation1   "  ?id "  praswuwIkaraNa )" crlf))
)

;@@@ Added by 14anu-ban-06 (14-04-2015)
;The rules are open to interpretation.(self referenced with cambridge)
;नियम विवेचना के लिए खुले हुए हैं .(manual) 
(defrule interpretation2
(declare (salience 2100))
(id-root ?id interpretation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI ?id1 ?id)
(id-root ?id1 open)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivecanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interpretation.clp 	interpretation2   "  ?id "  vivecanA )" crlf))
)
;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (14-04-2015)
;The dispute is based on two widely differing interpretations of the law.(cambridge)
;विवाद नियम के व्यापक रूप से भिन्न दो स्पष्टीकरणो  पर आधारित है . (manual)
(defrule interpretation0
(declare (salience 0))
(id-root ?id interpretation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id spaRtIkaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  interpretation.clp 	interpretation0   "  ?id "  spaRtIkaraNa )" crlf))
)
