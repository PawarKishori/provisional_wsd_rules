;@@@ Added by 14anu-ban-04 (10-09-2014)
;Spinning , weaving , dyeing , and the carpenter's and goldsmith's trade were also common . [karan singla]
;सूत कताई , बुनाई ,रंगना और बढई तथा सूनारों के व़्यापार भी आमतौर से बढ़ गये थे . 

(defrule dyeing0
(declare (salience 10))
(id-root ?id dye)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMganA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dyeing.clp 	dyeing0   "  ?id "  raMganA )" crlf))
)


;@@@ Added by 14anu-ban-04 (10-09-2014)
;Soaps, detergents and dyeing substances are wetting agents.  [ncert-corpus]
;साबुन, अपमार्जक तथा रंगने वाली वस्तुएँ, गीले कर्मक हैं.
(defrule dyeing1
(declare (salience 20))
(id-root ?id dyeing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 substance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raMgane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dyeing.clp 	dyeing1   "  ?id " raMgane_vAlA)" crlf))
)
