;@@@Added by 14anu-ban-02(13-04-2015)
;He cradled the child against his breast.[oald]
;उसने अपनी छाती के सहारे बच्चे को  पालने की तरह झुलाया.[self]
(defrule breast1 
(declare (salience 100)) 
(id-root ?id breast) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(id-root ?id1 his)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id CAwI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breast.clp  breast1  "  ?id "  CAwI )" crlf)) 
) 

;@@@Added by 14anu-ban-02(13-04-2015)
;A row of medals was pinned to the breast of his coat.[oald]
;पदकों की एक पङ्क्ति उसके कोट के सीने के पास लगाई गयी थी . [self]
(defrule breast2 
(declare (salience 100)) 
(id-root ?id breast) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 coat|shirt)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sInA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breast.clp  breast2  "  ?id "  sInA )" crlf)) 
) 


;@@@Added by 14anu-ban-02(13-04-2015)
;A feeling of love surged in his breast.[cald]
;प्यार का एक एहसास उसके हृदय में उमड़ा.[self] 
(defrule breast3 
(declare (salience 100)) 
(id-root ?id breast) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-in_saMbanXI  ?id1 ?id)
(id-root ?id1 surge)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id hqxaya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breast.clp  breast3  "  ?id "  hqxaya )" crlf)) 
) 


;-----------------------Default Rules-------------------------------------------


;@@@Added by 14anu-ban-02(13-04-2015)
;Sentence: She put the baby to her breast.[oald]
;Translation: उसने शिशु को अपने स्तन से लगाया .[self]
(defrule breast0 
(declare (salience 0)) 
(id-root ?id breast) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id swana)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breast.clp  breast0  "  ?id "  swana )" crlf)) 
) 


