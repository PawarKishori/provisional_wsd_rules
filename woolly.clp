;@@@ Added by 14anu-ban-11 on (08-04-2015)
;A woolly hat.(oald)
;एक ऊनी टोपी . (self)
(defrule woolly0
(declare (salience 00))
(id-root ?id accept)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  woolly.clp      woolly0   "  ?id "  UnI)" crlf))
)



;@@@ Added by 14anu-ban-11 on (08-04-2015)
;Woolly monkeys.(oald)
;घने बालो वाले बन्दर . (self)
(defrule woolly2
(declare (salience 20))
(id-word ?id woolly)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-saMKyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 monkey)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gane_bAlo_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  woolly.clp      woolly2   "  ?id "  Gane_bAlo_vAlA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (10-04-2015)    ;Note:-Working correctly on parser no:-5
;The Government have been woolly about the exact meaning of their proposals. (oald)
;सरकार  भ्रमित है उनके प्रस्तावों के वास्तविक मतलब के बारे में.   (self)
(defrule woolly3
(declare (salience 30))
(id-word ?id woolly)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-about_saMbanXI  ?id ?id1)
(id-root ?id1 meaning)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bramiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  woolly.clp      woolly3   "  ?id "  Bramiwa)" crlf))
)


