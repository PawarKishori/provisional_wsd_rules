;@@@Added by 14anu-ban-02(26-02-2015)
;Sentence: Our agent in New York deals with all US sales.[oald]
;Translation: न्यूयार्क में हमारा प्रतिनिधि  यूएस की पूरी  सेल के साथ डील करता है. .[self]
(defrule agent0 
(declare (salience 0)) 
(id-root ?id agent) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id prawiniXi)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  agent.clp  agent0  "  ?id "  prawiniXi )" crlf)) 
) 


;@@@Added by 14anu-ban-02(26-02-2015)
;Soaps, detergents and dying substances are wetting agents.[ncert 11_10]	;run the sentence on parser no.25.
;साबुन, अपमार्जक तथा रँगने वाली वस्तुएँ, गीले कर्मक हैं.[ncert]
(defrule agent1
(declare (salience 100)) 
(id-root ?id agent) 
?mng <-(meaning_to_be_decided ?id) 
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-word ?id1 wetting)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id karmaka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  agent.clp  agent1  "  ?id "  karmaka )" crlf)) 
) 


;@@@Added by 14anu-ban-02(14-03-2015)
;Vygotsky's theory tells us that adults are active agents in children's social development.[report set4]
;व्य्गोतस्की का सिद्धान्त हमें बताता है कि बालिग बच्चों के सामाजिक विकास में सक्रिय कारक हैं . [self]
(defrule agent2 
(declare (salience 100)) 
(id-root ?id agent) 
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1) 
(viSeRya-in_saMbanXI  ?id ?id2)
(id-root ?id1 active)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kAraka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  agent.clp  agent2  "  ?id "   kAraka )" crlf)) 
) 


