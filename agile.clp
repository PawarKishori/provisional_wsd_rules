;@@@Added by 14anu-ban-02(25-02-2015)
;Sentence: A strong and agile athlete.[oald]
;Translation:एक सशक्त और फुर्तीला व्यायामी .  [anusaaraka]
(defrule agile0 
(declare (salience 0)) 
(id-root ?id agile) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id PurwIlA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  agile.clp  agile0  "  ?id "  PurwIlA )" crlf)) 
) 


;@@@Added by 14anu-ban-02(25-02-2015)
;An agile mind.[oald]
;तेज दिमाग.[self]
(defrule agile1 
(declare (salience 100)) 
(id-root ?id agile) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 mind|brain|jaw|car)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id weja)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  agile.clp  agile1  "  ?id "  weja )" crlf)) 
) 
