
;@@@ Added by 14anu-ban-05 on (02-03-2015)
;‘How do you do?’ she said formally.[OALD]
;'आप कैसे करते हैं?' वह औपचारिक रूप से कहा.  [manual]

(defrule formally0
(declare (salience 100))
(id-root ?id formally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id OpacArika_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formally.clp   formally0   "  ?id "  OpacArika_rUpa_se )" crlf))
)

;@@@ Added by 14anu-ban-05 on (02-03-2015)
;The accounts were formally approved by the board.[OALD]
;खातों को विधिवत् रूप से बोर्ड द्वारा अनुमोदित किया गया.      [manual]

(defrule formally1
(declare (salience 101))
(id-root ?id formally)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id1 approve)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viXivaw_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  formally.clp   formally1   "  ?id "  viXivaw_rUpa_se )" crlf))
)

