


;Added by Meena(31.8.09)
;The labour was with the Union leader on this issue . 
(defrule union0
(declare (salience 5000))
(id-root ?id union)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMgaTana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  union.clp    union0   "  ?id "  saMgaTana )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabd on 27/6/2014***
;The workers' union went on strike.
;कार्यकर्ताओं का सङ्गठन स्ट्राइक पर गया . 
(defrule union1
(declare (salience 5000))
(id-root ?id union)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(id-root ?id1 worker|trader|labour|labor|people)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMgaTana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  union.clp    union1   "  ?id "  saMgaTana )" crlf))
)

;@@@ Added by (14anu06)Vivek Agarwal, MNNIT Allahabd on 27/6/2014***
;The union of traders went on strike.
;व्यापारियों का सङ्गठन स्ट्राइक पर गया 
(defrule union2
(declare (salience 5000))
(id-root ?id union)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 worker|trader|labour|labor|people)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMgaTana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  union.clp    union2   "  ?id "  saMgaTana )" crlf))
)
