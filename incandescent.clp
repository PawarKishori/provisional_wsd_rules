;@@@ Added by 14anu-ban-06 (10-03-2015)
;He was incandescent with rage.(cambridge)[parser no. 2]
;वह रोष से उत्तेजित था . (manual)
(defrule incandescent1
(declare (salience 2000))
(id-root ?id incandescent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-with_saMbanXI ?id ?id1)
(id-root ?id1 rage) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwejiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incandescent.clp 	incandescent1   "  ?id "  uwwejiwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (10-03-2015)
;An incandescent musical performance. (OALD) [parser no. 1]
;शानदार संगीत प्रदर्शन . (manual)
(defrule incandescent2
(declare (salience 2500))
(id-root ?id incandescent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 performance|career)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incandescent.clp 	incandescent2   "  ?id "  SAnaxAra )" crlf))
)


;---------------------- Default Rules -------------------

;@@@ Added by 14anu-ban-06 (10-03-2015)
;The mountain's snow-white peak was incandescent against the blue sky. (cambridge)[parser no. 2]
;पर्वत का हिमश्वेत शिखर नीले आसमान से अत्यधिक चमकीला था . (manual)
(defrule incandescent0
(declare (salience 0))
(id-root ?id incandescent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyaXika_camakIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incandescent.clp 	incandescent0   "  ?id "  awyaXika_camakIlA )" crlf))
)

