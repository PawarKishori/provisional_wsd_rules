;@@@ Added by 14anu-ban-04 (12-03-2015)
;The role of the extended family has been decaying for some time.                  [cald]            
;विस्तृत परिवार की भूमिका कुछ समय से कम होती जा रही है .                                              [manual]
(defrule decay2
(declare (salience 4910))
(id-root ?id decay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 role)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decay.clp 	decay2   "  ?id "  kama_ho )" crlf))
)

;@@@ Added by 14anu-ban-04 (12-03-2015)
;Pollution has decayed the surface of the stonework on the front of the cathedral.              [cald]
;प्रदूषण ने बड़े गिरजा घर  के अगले हिस्से पर पत्थरों की नक्काशी की सतह खराब की है .                                      [manual]
(defrule decay3
(declare (salience 4910))
(id-root ?id decay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 surface)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decay.clp 	decay3  "  ?id "  KarAba_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 (12-03-2015)
;The decay of the wood will spread if it is not removed.                   [oald]              
;लकड़ी की सड़न फैलेगी यदि यह हटाई नहीं गयी .                                          [self]
(defrule decay4
(declare (salience 5010))
(id-root ?id decay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(not (id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str))))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sadana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decay.clp 	decay4   "  ?id "  sadana )" crlf))
)

;------------------------ Default Rules ----------------------

;"decay","N","1.kRaya"
;The corpse was in an advanced state of decay
(defrule decay0
(declare (salience 5000))
(id-root ?id decay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decay.clp 	decay0   "  ?id "  kRaya )" crlf))
)

;"decay","VT","1.sadZanA/bigadZanA/GatanA/muraJAnA/nASa_honA"
;The unoccupied house started to decay
(defrule decay1
(declare (salience 4900))
(id-root ?id decay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  decay.clp 	decay1   "  ?id "  sadZa )" crlf))
)

