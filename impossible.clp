;@@@ Added by 14anu-ban-06  (02-09-14)
;Things that were thought impossible just a few years ago.(Parallel corpus)
;जो चीज़ें कुछ वर्ष पहले असंभव मानी जाती थी.
(defrule impossible0
(declare (salience 0))
(id-root ?id impossible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asamBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impossible.clp 	impossible0   "  ?id "  asamBava )" crlf))
)

;@@@ Added by 14anu-ban-06  (02-09-14)
;I've been placed in an impossible position. (OALD)
;muJe eka nirASAjanaka sWiwi meM DAla xiyA gayA.
(defrule impossible1
(declare (salience 2000))
(id-root ?id impossible)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 situation|position)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirASAjanaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impossible.clp 	impossible1   "  ?id "  nirASAjanaka )" crlf))
)
