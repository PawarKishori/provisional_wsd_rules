 
;@@@ Added by 14anu-ban-11 on (31-01-2015)
;She quickened her pace.(oxford)
;उसने अपनी चाल तेज की. (self)
(defrule quicken1
(declare (salience 20))
(id-root ?id quicken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 pace)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quicken.clp    quicken1   "  ?id "  weja_kA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (31-01-2015)
;I felt my pulse quicken.(oxford)
;मैंने मेरा श्वसन तेज महसूस किया . (anusaaraka)
(defrule quicken2
(declare (salience 30))
(id-root ?id quicken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 pulse)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quicken.clp    quicken2   "  ?id "  weja)" crlf))
)


;@@@ Added by 14anu-ban-11 on (31-01-2015)
;Her interest quickened.(oxford)
;उसकी रूचि बढ गयी.(anusaaraka)
(defrule quicken3
(declare (salience 40))
(id-root ?id quicken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 interest)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quicken.clp    quicken3   "  ?id "  baDa_jA)" crlf))
)

;----------------------------- Default Rules -----------------------

;@@@ Added by 14anu-ban-11 on (31-01-2015)
;Computers quicken the work.(hinkhoj) 
;सङ्गणक कार्य जल्दी करते हैं .(anusaaraka) 
(defrule quicken0
(declare (salience 10))
(id-root ?id quicken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalxI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quicken.clp    quicken0   "  ?id "  jalxI_kara )" crlf))
)


