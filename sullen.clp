;NOTE: Sullen is adjective..as specified in oald and cald.But parser is considering it as noun in almost all the cases.So, maximum sentences in this file have to be run on specific parse numbers.

;@@@ Added by 14anu-ban-01 on (25-02-2015)
;Bob turned out to be a sullen teenager.[self: with reference to oald]  :Parse no. 2(sullen is an adjective)
;बॉब एक हठी़/ज़िद्दी किशोर निकला .  [self]
(defrule sullen1
(declare (salience 100))
(id-root ?id sullen )
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 teenager)	;The following condition can be uncommented when 'teenager' will be added in 'human.gdbm' and 'animate.gdbm'
;(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hTZI/jZixxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  sullen.clp  	sullen1 "  ?id "   hTZI/jZixxI )" crlf))
)

;@@@ Added by 14anu-ban-01 on (25-02-2015)
;The sullen looks, the pain,the scars on older women's sagging breasts and aging men's sunken chests, are haunting.[coca] :parse no. 5
;वे उदास नज़रें, दर्द,बूढ़ी स्त्रियों के ढीली-ढाली छातियों एवं बूढ़े होते हुए पुरुषों की  धँसी हुई/सिकुडी हुई छातियों पर पड़े हुए दाग/निशान  बार बार याद आते हैं [self]
(defrule sullen2
(declare (salience 100))
(id-root ?id sullen )
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 look)	
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UxAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  sullen.clp  	sullen2 "  ?id "   UxAsa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (25-02-2015)
;A sullen grey sky.[oald]
;एक विषादपूर्ण/अन्धकारमय धुँधला आसमान .  [self]
(defrule sullen3
(declare (salience 100))
(id-root ?id sullen )
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 sky)	
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viRAxapUrNa/anXakAramaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  sullen.clp  	sullen3 "  ?id "   viRAxapUrNa/anXakAramaya )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-01 on (25-02-2015)
;She gave him a sullen glare.[oald]
;उसने उसे ख़िन्न नज़रों से देखा .[self]
(defrule sullen0
(declare (salience 0))
(id-root ?id sullen )
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KZinna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  sullen.clp    sullen0 "  ?id "   KZinna )" crlf))
)

