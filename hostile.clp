;@@@ Added by 14anu-ban-06 (17-04-2015)
;I'm not hostile to the idea of change as such. (cambridge)
;मैं ऐसे बदलाव के विचार के खिलाफ नहीं हूँ . (manual)
(defrule hostile1
(declare (salience 2000))
(id-root ?id hostile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI ?id ?id1)
(id-root ?id1 idea)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KilAPa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hostile.clp 	hostile1   "  ?id "  KilAPa )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-04-2015)
;A hostile climate. (cambridge)
;प्रतिकूल जलवायु . (manual)
(defrule hostile2
(declare (salience 2100))
(id-root ?id hostile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 condition|climate|environment)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawikUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hostile.clp 	hostile2   "  ?id "  prawikUla )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-04-2015)
;Hostile territory.(OALD)
;शत्रु का क्षेत्र . (manual)
(defrule hostile3
(declare (salience 2200))
(id-root ?id hostile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 territory|aircraft|force)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sawru_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hostile.clp 	hostile3   "  ?id "  Sawru_kA )" crlf))
)

;@@@ Added by 14anu-ban-06 (17-04-2015)
;The company convinced investors to reject a hostile bid from Enterprise Oil Plc worth Â£1.5 billion. (cambridge)
;कम्पनी ने  निवेशको को  एन्टर्प्राइज ऑयल पी एल सी से 1.5 अरब £ योग्य अवांछित बोली नामंजूर करने के लिए राजी किया . (manual) 
(defrule hostile4
(declare (salience 2300))
(id-root ?id hostile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 bid)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avAMCiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hostile.clp 	hostile4   "  ?id "  avAMCiwa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (17-04-2015)
;The proposals have provoked a hostile response from opposition parties.(OALD)
;यह प्रस्ताव विपक्ष पार्टियों से विद्वेशपूर्ण प्रतिक्रिया प्राप्त कर चुके हैं . (manual)
(defrule hostile0
(declare (salience 0))
(id-root ?id hostile)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixveRapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hostile.clp 	hostile0   "  ?id "  vixveRapUrNa )" crlf))
)


