;@@@ Added by 14anu-ban-06 (31-03-2015)
;To impeach somebody's motives. (OALD)
;किसी के प्रयोजन पर सन्देह करना . (manual)
(defrule impeach1
(declare (salience 2000))
(id-root ?id impeach)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 motive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMxeha_kara))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impeach.clp 	impeach1   "  ?id "  saMxeha_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  impeach.clp       impeach1   "  ?id " para )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (31-03-2015)
;The President was impeached by Congress for lying.(OALD)
;अध्यक्ष पर झूठ बोलने के लिए काङ्ग्रेस के द्वारा महाभियोग लगाया गया था . (manual)
(defrule impeach0
(declare (salience 0))
(id-root ?id impeach)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahABiyoga_lagA))
(assert (kriyA_id-subject_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impeach.clp 	impeach0   "  ?id "  mahABiyoga_lagA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  impeach.clp       impeach0   "  ?id " para )" crlf)
)
)
