;@@@Added by 14anu-ban-08 (18-04-2015)
;He ran his finger around the lip of the cup.  [oald]
;उसने अपनी उगँली कटोरे के किनारे फेरी.  [self]
(defrule lip1
(declare (salience 100))
(id-root ?id lip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 cup|plate|bowl)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  lip.clp 	lip1   "  ?id "  kinArA )" crlf))
)


;@@@Added by 14anu-ban-08 (18-04-2015)
;She produced a long silver whistle and placed it firmly between her lips. [oald]
;उसने एक लम्बी चाँदी सीटी निकाली और स्थिरता से उसके होंठो के बीच में रखा .     [Manual]
(defrule lip0
(declare (salience 0))
(id-root ?id lip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id hoMTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  lip.clp      lip0   "  ?id "  hoMTa )" crlf))
)

