
;@@@ Added by 14anu-ban-09 on (25-03-2015)
;The whole episode was a blatant attempt to gain publicity.	[mw]
;पूरी कड़ी लोक प्रसिद्धि प्राप्त करने की एक जबरदस्त कोशिश थी . 	[self]
(defrule publicity1
(declare (salience 5000))
(id-root ?id publicity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 gain)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id loka_prasixXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  publicity.clp 	publicity1   "  ?id "  loka_prasixXi )" crlf))
)

;-------------------------DEFAULT RULE------------------------------------------
;@@@ Added by 14anu-ban-09 on (25-03-2015)
;NOTE:- Example sentence need to be added.
(defrule publicity0
;(declare (salience 5000))
(id-root ?id publicity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  publicity.clp 	publicity0   "  ?id "  pracAra )" crlf))
)


;--------------------------------------------------------------------------------

