
(defrule final0
(declare (salience 5000))
(id-root ?id final)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 report)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  final.clp 	final0   "  ?id "  aMwima )" crlf))
)

(defrule final1
(declare (salience 4900))
(id-root ?id final)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  final.clp 	final1   "  ?id "  aMwima )" crlf))
)

(defrule final2
(declare (salience 4800))
(id-root ?id final)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwima_caraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  final.clp 	final2   "  ?id "  aMwima_caraNa )" crlf))
)

;"final","N","1.aMwima_caraNa"
;He has reached the finals in the match.
;
;
;@@@ Added by 14anu-ban-05 on (03-03-2015)
;Who has the final say around here?[OALD]
; यहाँ चारों ओर किसकी बात मानी जाती है?	[manual]
(defrule final3
(declare (salience 5001))
(id-root ?id final)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) say)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) bAwa_mAnI_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " final.clp  final3  "  ?id "  " (+ ?id 1) "  bAwa_mAnI_jA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-03-2015)
;The men's basketball final will be on Sunday.[CALD]
;पुरुषों की बास्केटबॉल फाइनल रविवार को होगा.   [manual]
(defrule final4
(declare (salience 5002))
(id-root ?id final)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 basketball)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PAinala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  final.clp 	final4   "  ?id "  PAinala )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-03-2015)
;Last year we got through to the final.[CALD]
;पिछले वर्ष हम अन्तिम चरण तक पहुँच गये थे.		[Manual]
(defrule final5
(declare (salience 5002))
(id-root ?id final)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-to_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwima_caraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  final.clp 	final5   "  ?id "  aMwima_caraNa )" crlf))
)
