
;"reasoning","N","1.warka_viwarka"
;Her reasoning on this point is correct. 
(defrule reason0
(declare (salience 5000))
(id-root ?id reason)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id reasoning )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id warka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  reason.clp  	reason0   "  ?id "  warka )" crlf))
)

;given_word=reasoning && word_category=noun	$warka_viwarka)


;@@@ Added by 14anu-ban-10 on (20-03-2015)
;The conflict between faith and reason is going on.[hinkhoj]
;भक्ति और तर्क शक्ति के बीच विरोध हो रहा है . [manual]
(defrule reason4
(declare (salience 5200))
(id-root ?id reason)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-between_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  warka_Sakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reason.clp 	reason4   "  ?id "   warka_Sakwi)" crlf))
)

;@@@ Added by 14anu-ban-10 on (20-03-2015)
;What is your reason for leaving the job?[hinkhoj]
;नौकरी छोडने के लिए आपका  क्या प्रयोजन है? . [manual]
(defrule reason5
(declare (salience 5300))
(id-root ?id reason)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 leave)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  prayojana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reason.clp 	reason5   "  ?id "   prayojana)" crlf))
)

;@@@ Added by 14anu-ban-10 on (20-03-2015)
;We have no reason to believe that he is lying.[hinkhoj]
;हमारे पास कोई विचार शक्ति नही है कि वह झूठ बोल रहा है . [manual]
(defrule reason6
(declare (salience 5400))
(id-root ?id reason)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to)
(id-word =(+ ?id 2) believe)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)(+ ?id 2) vicAra_Sakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " reason.clp  reason6  "  ?id "  " (+ ?id 1)(+ ?id 2) "  vicAra_Sakwi)" crlf))
)

;------------------------ Default Rules ----------------------

(defrule reason1
(declare (salience 4900))
(id-root ?id reason)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reason.clp 	reason1   "  ?id "  kAraNa )" crlf))
)

;"reason","VTI","1.warka_karanA"
;She reasoned that if she started at 5am she would be there by noon.
(defrule reason2
(declare (salience 4800))
(id-root ?id reason)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warka_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reason.clp 	reason2   "  ?id "  warka_kara )" crlf))
)

