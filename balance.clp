;$$$Modified by 14anu-ban-02(13-01-2015)
;Meaning changed from 'saMwulana' to 'saMwuliwa'
;Being a childminder is a marvellous opportunity to get the balance right for children building their self esteem.
;चाइल्डमाइंडर होना  एक अद्भुत् अवसर है बच्चों को संतुलित ढंग से आगे बढ़ने और उनका आत्मविश्वास विकसित कराने के लिये.(manual)
;@@@Added by 14anu24
;Being a childminder is a marvellous opportunity to get the balance right for children building their self esteem. 
;चाइल्डमाइंडर का काम करना एक अद्भुत् अवसर है . बच्चों को संतुलित ढंग से आगे बढऋआना , उनका आत्मविश्वास विकसित कराना .

(defrule balance_tmp
(declare (salience 5000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id) ;added '?id1' by 14anu-ban-02(13-01-2015)
(id-root ?id1 right)			   ;added by 14anu-ban-02(13-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMwuliwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp  balance_tmp   "  ?id "  saMwuliwa )" crlf))
)

;$$$ Modified by 14anu-ban-02 15-01-2015
;added 'good|bad|obligation' in the list
;Is there a balance between good and evil?
;क्या अच्छाई और बुराई के बीच संतुलन है?
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;He lost his balance and tumbled over.[cambridge]
;उसने उसका संतुलन खोया और गिर गया .
;I struggled to keep my balance on my new skates.[oald]
;मैं अपने नए स्केट्स पर मेरा संतुलन बनाये रखने के लिए संघर्ष किया.
;There must , therefore , be a balance between the nutrients , so that the feed is used most efficiently for egg production and body growth
;of the birds .
;इसलिए पौष्टिक पदार्थों में सन्तुलन होना चाहिए ताकि उस खुराक का अण्डा उत्पादन तथा मुर्गियों की शरीर वृद्धि के लिए अच्छे से अच्छा उपयोग हो सके .
(defrule balance2
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ?id ?id1)(kriyA-object  ?id1 ?id)(viSeRya-between_saMbanXI ?id ?id1))
(id-root ?id1 keep|lose|mind|nutrient|good|bad|obligation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMwulana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance2   "  ?id "  saMwulana )" crlf))
)

;$$$ Modified by 14anu-ban-02 (25-11-2014)
;###[COUNTER SENTENCE]The plate is balanced by weights on the other side, with its horizontal edge just over water.[NCERT]
;प्लेट के क्षैतिज निचले किनारे को पानी से थोडा ऊपर रखकर, तुला के दूसरी ओर बाट रखकर सन्तुलित कर लेते हैं.[NCERT]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;His lack of experience was balanced by a willingness to learn.[oald]
;उसके सीखने की इच्छा द्वारा उसके अनुभव की कमी की पूर्ती हुयी . 
(defrule balance3
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 willingness) ;Added by 14anu-ban-02
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrwI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance3   "  ?id "  pUrwI_ho )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;The cost of obtaining legal advice needs to be balanced against its benefits.[oald]
;कानूनी सलाह प्राप्त करने की कीमत को उसके लाभ के साथ तोलने की जरूरत है .  
(defrule balance4
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(kriyA-against_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance4   "  ?id "  wola )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;The long-term future of the space programme hangs in the balance.[oald]
;अंतरिक्ष कार्यक्रम का दीर्घकालिक भविष्य अधर में लटका हुआ है/अनिश्चित है.
(defrule balance5
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?kri ?id)
(id-root ?kri hang)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance5   "  ?id "  aXara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;I was thrown off balance by the sudden gust of wind.[oald]
;मैं हवा के अचानक झोंके से अस्थिर हो गया था /लड़खड़ा गया था. 
(defrule balance6
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?kri ?id)
(id-root ?kri throw)
(id-word =(+ ?kri 1) off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?kri 1) ?kri asWira_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " balance.clp 	balance6  "  ?id "  " (+ ?kri 1) " " ?kri "   asWira_ho )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;The balance of $500 must be paid within 90 days.[oald]
;$500 की शेष राशी 90 दिनों में देनी होगी . 
(defrule balance7
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(viSeRya-saMKyA_viSeRaNa  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SeRa_rASI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance7   "  ?id "  SeRa_rASI )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;The senator was clearly caught off balance by the unexpected question.[oald]
;सीनेटर स्पष्ट रूप से अप्रत्याशित प्रश्न द्वारा परेशान हो गया था. 
(defrule balance8
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?kri ?id)
(id-root ?kri catch)
(id-word =(+ ?kri 1) off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?kri 1) ?kri pareSAna_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " balance.clp 	balance8  "  ?id "  " (+ ?kri 1) " " ?kri "   pareSAna_ho )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;She balanced the cup on her knee.[oald]
;उसने कप को घुटने पर संतुलित रखा 
(defrule balance9
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-on_saMbanXI  ?id1 ?)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMwuliwa_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance9   "  ?id "  saMwuliwa_raKa )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 25-Feb-2014
;The balance of an account.[shiksharti kosh]
;हिसाब का बकाया
(defrule balance10
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-word ?id1 account)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bakAyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance10   "  ?id "  bakAyA )" crlf))
)
;commented by 14anu-ban-02(12-01-2015)
;correct meaning is coming from balance2
;@@@ Added by 14anu26     [01-07-14] and   14anu02  
;The balance between meritocracy and social obligations to the traditionally disadvantaged is a delicate one . 
;प्रतिभा और पारंपरिक रूप से सुविधाविहीन लोगों के प्रति सामाजिक दायित्व के बीच का संतुलन बहुत ही नाजुक मसला है .
;Is there a balance between good and evil? -- added by 14anu02
;क्या अच्छाई और बुराई के बीच संतुलन है?  -- added by 14anu02
;(defrule balance11
;(declare (salience 3000))
;(id-root ?id balance)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-between_saMbanXI ?id ?)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id saMwulana))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance11   "  ?id "  saMwulana )" crlf))
;)

;@@@ Added by 14anu-ban-02(26-08-2014)
;The horizontal forces exerted at its two ends must be balanced or the pressure at the two ends should be equal.[ncert]
;इसके दोनों सिरों पर कार्यरत क्षैतिज बल साम्य अवस्था विचारणीय बिंदु में होने चाहिए ; अर्थात् दोनों सिरों पर समान दाब होना चाहिए.[ncert]
;###[COUNTER SENTENCE]###The plate is balanced by weights on the other side, with its horizontal edge just over water.[ncert]
;प्लेट के क्षैतिज निचले किनारे को पानी से थोडा ऊपर रखकर, तुला के दूसरी ओर बाट रखकर सन्तुलित कर लेते हैं.[ncert]
(defrule balance12
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 force)      ;Added by 14anu-ban-02(25-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance12   "  ?id "  sAmya )" crlf))
)

;@@@ Added by 14anu-ban-02 (31-10-2014)
;The inward force exerted by the fluid on the piston is balanced by the outward spring force and is thereby measured.[ncert]
;पिस्टन पर तरल द्वारा आरोपित बल को कमानी द्वारा पिस्टन पर आरोपित बल से सन्तुलित करके तरल द्वारा पिस्टन पर आरोपित बल को माप लेते हैं.[ncert]
;The plate is balanced by weights on the other side, with its horizontal edge just over water.[ncert]
;प्लेट के क्षैतिज निचले किनारे को पानी से थोडा ऊपर रखकर, तुला के दूसरी ओर बाट रखकर सन्तुलित कर लेते हैं.[ncert]
;प्लेट के क्षैतिज निचले किनारे को पानी से थोडा ऊपर रखकर, तुला के दूसरी ओर बाट रखकर सन्तुलित किया गया हैं.[modified]
(defrule balance13
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 force|weight);'weight' is added by 14anu-ban-02(25-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanwuliwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance13   "  ?id "  sanwuliwa_kara)" crlf))
)

;@@@Added by 14anu-ban-02(09-02-2015)
;This point of balance is the center of gravity (CG) of the cardboard.[ncert 11_07]
;यह सन्तुलन बिंदु गत्ते के टुकडे का गुरुत्व केंद्र (CG) है.[ncert]
(defrule balance14
(declare (salience 3000))
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id1 ?id)	;needs sentences for restriction.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanwulana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance14   "  ?id "  sanwulana)" crlf))
)


;*********************DEFAULT RULES*****************************

(defrule balance0
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id warAjZU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance0   "  ?id "  warAjZU )" crlf))
)

;"balance","N","1.warAjZU"
;One should always buy only ISI marked balance.
;
(defrule balance1
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id balance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMwulana_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  balance.clp 	balance1   "  ?id "  saMwulana_raKa )" crlf))
)

;"balance","V","1.saMwulana_raKa"
;--"2.saMwulana_raKanA"
;Students should be taught to balance their time between games && studies.
;--"3.bakAyA_nikAlanA"
;Our accountant has not yet found the balance of our company's annual account.
;
