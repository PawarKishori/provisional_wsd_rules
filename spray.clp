;@@@ Added by 14anu-ban-11 on (24-02-2015)
;A spray of orchids. (oald)
;ऑर्किड की लडी . (self)
(defrule spray2
(declare (salience 5100))
(id-root ?id spray)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 orchid)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ladZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spray.clp 	spray2   "  ?id "  ladZI )" crlf))
)

;@@@ Added by 14anu-ban-11 on (24-02-2015)
;A spray of machine-gun bullets.(oald)
;मशीन-गन की गोलियों की बौछार . (self)
(defrule spray3
(declare (salience 5200))
(id-root ?id spray)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 bullet)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bOCAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spray.clp 	spray3   "  ?id "  bOCAra )" crlf))
)


;@@@ Added by 14anu-ban-11 on (24-02-2015)
;Pieces of glass sprayed all over the room.(oald)
;काँच के टुकडे पूरा कमरे मे बिखर गये . (self)
(defrule spray4
(declare (salience 4901))
(id-root ?id spray)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 piece)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id biKara_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spray.clp 	spray4   "  ?id "  biKara_jA)" crlf))
)

;------------------------ Default Rules ----------------------

(defrule spray0
(declare (salience 5000))
(id-root ?id spray)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PuhAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spray.clp 	spray0   "  ?id "  PuhAra )" crlf))
)

;"spray","V","1.CidZakanA"
;Farmers spray pesticides on the crops.
(defrule spray1
(declare (salience 4900))
(id-root ?id spray)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CidZaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spray.clp 	spray1   "  ?id "  CidZaka )" crlf))
)

