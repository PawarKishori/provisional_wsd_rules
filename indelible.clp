;@@@ Added by by 14anu-ban-06 (16-02-2015)
;The blood had left an indelible mark on her shirt.(cambridge)
;खून ने उसकी कमीज पर पक्का निशान छोडा . (manual)
(defrule indelible0
(declare (salience 0))
(id-root ?id indelible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indelible.clp 	indelible0   "  ?id "  pakkA )" crlf))
)

;@@@ Added by by 14anu-ban-06 (16-02-2015)
;In his 20 years working for the company, Joe Pearson made an indelible impression on it.(cambridge)
;उसके 20 वर्षों में कम्पनी के लिए काम करते हुए , जोव पिर्सन ने उस पर स्थायी प्रभाव डाला .(manual) 
(defrule indelible1
(declare (salience 2000))
(id-root ?id indelible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 memory|impression)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAyI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indelible.clp 	indelible1   "  ?id "  sWAyI )" crlf))
)
