;@@@ Added by 14anu-ban-11 on (16-04-2015)
;The snake writhed and hissed. (oald)
;साँप मुडा और फुफकारा . (self)
(defrule writhe0
(declare (salience 10))
(id-root ?id writhe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 snake)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mudZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  writhe.clp 	writhe0  "  ?id "  mudZa)" crlf))
)

;@@@ Added by 14anu-ban-11 on (16-04-2015)
;Don't writhe on him in so heathenish a manner. (coca)
;इतने वहशी तरीके में उस पर मत चिल्लाइए . (self)
(defrule writhe2
(declare (salience 20))
(id-root ?id writhe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 him)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cillA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  writhe.clp 	writhe2  "  ?id "  cillA)" crlf))
)

;------------------------------- Default Rules -----------------------------------

;@@@ Added by 14anu-ban-11 on (16-04-2015)
;All I could do was just lie in bed and writhe.(coca)
;मैं बस बिस्तर में लेट सका हूँ  और कराह सका हूँ . (self) 
(defrule writhe1
(declare (salience 00))
(id-root ?id writhe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  writhe.clp 	writhe1  "  ?id "  karAha)" crlf))
)


