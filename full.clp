;Added by Meena(1.4.10)
;When I run at my full speed . 
(defrule full00
(declare (salience 5000))
(id-root ?id full)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  full.clp      full00   "  ?id "  pUrA )" crlf))
)



(defrule full0
(declare (salience 5000))
(id-root ?id full)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 - )
(viSeRya-of_saMbanXI ?id1 ?id) ;Replaced viSeRya-of_viSeRaNa as viSeRya-of_saMbanXI programatically by Roja 09-11-13
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  full.clp 	full0   "  ?id "  BarA_huA )" crlf))
)

;@@@ Added by 14anu-ban-04 on 24-07-2014
;Manas National Park is very quiet, full of natural beauty, and is enchanting .
;mAnasa raStriya parka awyanwa SAnwa ,praAkqwika sunxarawA se BarapUra Ora mohaka hE.
;The city is ordinary and small but it is full of glory due to the presence of Jagannath .
;yaha Sahara svABAvika Ora CotA hE paranwu  jaganAWa kI  apasWiwi ke kAraNa kIrwi se BarapUra hE.

(defrule full4
(declare (salience 5000))
(id-root ?id full)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id adjective)
(id-root ?id1 beauty|glory)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarapUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  full.clp 	full4  "  ?id "  BarapUra )" crlf))
)

;@@@ Added by 14anu-ban-05 on (04-12-2014)
;The trees of Gulmohar , Amaltas also flower in their full bloom also .[TOURISM]
;gulamohara , amalawAsa ke pedZa BI apanI pUrI bahAra ke sAWa PUlawe hEM .[TOURISM]
(defrule full5
(declare (salience 5000))
(id-root ?id full)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(+ ?id 1) bloom)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) pUrI_bahAra_ke_sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " full.clp  full5  "  ?id "  " + ?id 1 "  pUrI_bahAra_ke_sAWa  )" crlf))
)

;------------------------- Default rules ------------------------

(defrule full1
(declare (salience 4900))
(id-root ?id full)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  full.clp 	full1   "  ?id "  BarA_huA )" crlf))
)

(defrule full2
(declare (salience 4800))
(id-root ?id full)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrA-))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  full.clp 	full2   "  ?id "  pUrA- )" crlf))
)

(defrule full3
(declare (salience 4700))
(id-root ?id full)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  full.clp 	full3   "  ?id "  BarA_huA )" crlf))
)


;"full","Adj","1.BarA_huA"
;The lake is full during rainy season.
;--"2.pUrNa"
;She is full of energy.
;--"3.Barapeta"
;I'm full.I cannot eat any more.
;--"4.pUrA"
;The woman gave full information of the incident.
;
;
