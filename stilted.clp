
;@@@ Added by  14anu-ban-01 on (10-04-2015)
;She said it in a stilted tone.[stilt.clp]
;उसने इसको रूखे लहजे में  कहा .[self]
(defrule stilted1
(declare (salience 1000))	
(id-root ?id stilted)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 tone|note|voice)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUKA ))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  stilted.clp  	stilted1   "  ?id "  rUKA )" crlf))	
)

;---------------------------- Defautl Rules ------------------------------------

;@@@ Added by  14anu-ban-01 on (10-04-2015)
;We made stilted conversation for a few moments.[oald]
;हमने कुछ क्षणों के लिए अस्वाभाविक/बनावटी वार्तालाप की . [self]
(defrule stilted0
(declare (salience 0))
(id-root ?id stilted)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asvABAvika/banAvatI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  stilted.clp   stilted0   "  ?id "  asvABAvika/banAvatI )" crlf))
)
