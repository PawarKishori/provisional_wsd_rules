;@@@ Added by 14anu-ban-06 (01-04-2015)
;Imperial units have in many cases been replaced by metric ones in Britain. (cambridge)[parser no.- 3]
;ब्रिटन में कई प्रकरणो में मापन इकाइयाँ मात्रिक के द्वारा बदली गयीं हैं . (manual)
(defrule imperial1
(declare (salience 2000))
(id-root ?id imperial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 unit)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mApana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imperial.clp 	imperial1   "  ?id "  mApana )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (01-04-2015)
;The imperial family. (OALD)
;शाही परिवार . (manual)
(defrule imperial0
(declare (salience 0))
(id-root ?id imperial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAhI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  imperial.clp 	imperial0   "  ?id "  SAhI )" crlf))
)
