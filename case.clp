;##############################################################################
;#  Copyright (C) 2002-2005 Preeti Pradhan (pradhan.preet@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by 14anu03 on 01-july-14
;It was a tricky case to solve.
;यह हल करने के लिये पेचीदा विषय था.
(defrule case101
(declare (salience 5500))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 tricky)
(test (=(- ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pecIxA_viRaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  case.clp     case101   "  ?id "  " ?id1 "  pecIxA_viRaya )" crlf))
)


;@@@ Added by 14anu04 on 28-June-2014
;The case of my glasses was broken. 
;मेरे चश्मे का डिब्बा टूटा हुअा था. 
(defrule case_tmp
(declare (salience 650))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 glass|champagne|jewellery|specs)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dibbA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case_tmp   "  ?id "  dibbA)" crlf))
)

;@@@ Added by 14anu-ban-03 (06-08-2014)
;Free fall is thus a case of motion with uniform acceleration.
;isa prakAra mukwa pawana ekasamAna wvaraNa vAlI gawi kA eka uxAharaNa hE .
(defrule case_uxAharaNa
(declare (salience 600))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxAharaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case_uxAharaNa   "  ?id "  uxAharaNa  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (09-12-2014)
;Added by Preeti(30-11-13)
;$$$ Moddified by 14anu17
;He is a hopeless case. 
;vaha eka nikammA AxamI hE.
(defrule case_AxamI
(declare (salience 600))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id1 ?id); added '?id1' by 14anu-ban-03 (09-12-2014)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))  ;added by 14anu-ban-03 (09-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AxamI))  ;changed Axami to mAmalA by 14anu17
                                      ;changed meaning from mAmalA to Axami by 14anu-ban-03 (09-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case_AxamI   "  ?id "  AxamI )" crlf))
)


;$$$ Modified by 14anu-ban-03 (24-02-2015)
;###[COUNTER EXAMPLE]### He is a defendant in the property case.[HINKHOJ]
;वह सम्पत्ति मामले में प्रतिवादी है .  [self]
;Added by Preeti(30-11-13)
;The statues are kept in a glass case. 
;prawimAez  kAzca kI petI meM raKI gayIM hEM.
(defrule case_petI
(declare (salience 650))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id3) 	;added id3 by 14anu-ban-03 (24-02-2015)
(id-root ?id3 glass)  				 ;added by 14anu-ban-03 (24-02-2015)
;(and(viSeRya-of_saMbanXI  ?id ?id1) (id-root ?id1 champagne|whisky))  ;commented  by 14anu-ban-03 (24-02-2015)
;(and(viSeRya-for_saMbanXI  ?id ?id2) (id-root ?id2 pill)))  ;commented  by 14anu-ban-03 (24-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id petI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case_petI   "  ?id "  petI)" crlf))
)


;Added by Preeti(30-11-13)
;Latin nouns have case, number and gender.
(defrule case_kAraka
(declare (salience 550))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAraka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case_kAraka   "  ?id "  kAraka )" crlf))
)


;@@@ Added by 14anu-ban-03 (11-10-2014)
;In the limiting case, when the slope of the second plane is zero (i.e. is a horizontal) the ball travels an infinite distance.[ncert]
;सीमान्त स्थिति में, जब दूसरे समतल का ढाल शून्य है (अर्थात् वह क्षैतिज समतल है) तब गेन्द अनन्त दूरी तक चलती है. [ncert]
;The field strength due to qm at a distance r from it is given by µ0qm/4πr2 ; The magnetic field due to the bar magnet is then obtained, both for the axial and the equatorial case, in a manner analogous to that of an electric dipole (Chapter 1).[ncert]
;@r xUrI para @qm ke kAraNa kRewra kI wIvrawA µ0@qm/4π@r2 hogI Ora waba akRIya evaM anuprasWa xonoM sWiwiyoM meM isa Cada cumbaka ke kAraNa cumbakIya kRewra usI prakAra jFAwa kiyA jA sakawA hE jEsA ki vExyuwa xviXruva ke lie kiyA gayA WA (xeKie aXyAya 1).[ncert]
(defrule case1
(declare (salience 5000))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa ?id ?id1 ) (subject-subject_samAnAXikaraNa  ?id1 ?id)) 
                                          ;added relation 'subject-subject_samAnAXikaraNa'by 14anu-ban-03 (13-10-2014)
(id-word ?id1 limiting|simpler|fall|intermediate|equatorial)  ;added word 'simple|fall|intermediate' by 14anu-ban-03 (13-10-2014)
                               ;added 'equatorial' by 14anu-ban-03 (08-12-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case1   "  ?id "  sWiwi )" crlf))
)

;$$$ Modified by 14anu-ban-03 (25-11-2014)
;@@@ Added by 14anu-ban-10 on (21-10-2014)
;A plot of pressure versus temperature gives a straight line in this case, as shown in Fig. 11.2.  [ncert corpus]
;ciwra 11.2 meM xarSAe anusAra, isa prakaraNa meM, xAba waWA wApa ke bIca grAPa eka sarala reKA howA hE.[ncert corpus]
(defrule case2
(declare (salience 1000))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa ?id ? ) ;commented by 14anu-ban-03 (25-11-2014)
(kriyA-in_saMbanXI ?id1 ?id)  ;added by 14anu-ban-03 (25-11-2014)
(id-root ?id1 give)  ;added by 14anu-ban-03 (25-11-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case2   "  ?id "  prakaraNa )" crlf))
)

;$$$ Modified by 14anu-ban-03 (24-11-2014)
;@@@ Added by 14anu-ban-10 on (21-10-2014)
;The restoring force per unit area in this case is called tensile stress.  [ncert corpus]
;isa sWiwi meM, ekAMka kRewraPala para prawyAnayana bala ko wanana prawibala kahawe hEM.[ncert corpus]
(defrule case3
(declare (salience 2000))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id1 ?id)
(id-root ?id1 area)    ;added by 14anu-ban-03 (24-11-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case3   "  ?id "  sWiwi)" crlf))
)

;@@@ Added by 14anu-ban-10 on (21-10-2014)
;Equation (6.2) is also a special case of the work-energy (WE) theorem: The change in kinetic energy of a particle is equal to the work done on it by the net force.  [ncert corpus]
;samIkaraNa (6.2) kArya - UrjA prameya kI eka viSeRa sWiwi hE jo yaha praxarSiwa karawI hE ki kisI vaswu para lagAe gae kula bala xvArA kiyA gayA kArya usa vaswu kI gawija UrjA meM parivarwana ke barAbara howA hE.[ncert corpus]
(defrule case4
(declare (salience 3000))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1  special)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viSeRa_sWiwi ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng    " ?*prov_dir* "  case.clp 	case4   "  ?id "  viSeRa_sWiwi)" crlf))
)

;@@@ Added by 14anu-ban-03 (24-11-2014)
;This summation can be converted to an integral in most cases. [ncert]
;aXikAMSa prakaraNoM meM safkalana ko samAkalana meM parivarwiwa kara lewe hEM. [ncert]
(defrule case5
(declare (salience 1000))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id1 ?id)
(id-root ?id1 integral)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case5   "  ?id "  prakaraNa )" crlf))
)

;@@@ Added by 14anu-ban-03 (25-11-2014)
;First consider the case of v perpendicular to B. [ncert]
;pahale usa sWiwi para vicAra kIjie jisameM vega @v cumbakIya kRewra @B ke lambavawa hE. [ncert]
(defrule case6
(declare (salience 1000))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)  
(id-root ?id1 perpendicular)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case6   "  ?id "  sWiwi )" crlf))
)

;@@@ Added by 14anu-ban-03 (25-11-2014)
;In the case of motion of a charge in a magnetic field, the magnetic force is perpendicular to the velocity of the particle.[ncert]
;cumbakIya kRewra meM AveSa kI gawi ke prakaraNa meM, cumbakIya bala kaNa ke vega kI xiSA ke lambavawa howA hE.[ncert]
(defrule case7
(declare (salience 1000))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)  
(id-root ?id1 motion)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case7   "  ?id "  prakaraNa )" crlf))
)

;@@@ Added by 14anu-ban-03 (05-12-2014)
;Picard slipped the documents into a case. (COCA)
;पिकॉर्ड ने पेटी में दस्तावेज छिपा कर रखे. (manual)
(defrule case8
(declare (salience 650))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-into_saMbanXI ?id1 ?id)
(id-root ?id1 slip)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id petI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case8   "  ?id "  petI)" crlf))
)

;@@@ Added by 14anu-ban-03 (03-02-2015)
;He was found not guilty because of holes in the prosecution case.(OALD)
;वह अभियोग पक्ष के मुकदमे में गलतियों की वजह से अपराधी नहीं पाया गया था . (manual)
(defrule case9
(declare (salience 650))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1) 
(id-root ?id1 prosecution)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukaxamA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case9   "  ?id "  mukaxamA)" crlf))
)


;@@@ Added by 14anu-ban-03 (20-03-2015)
;He flipped the lid open and looked inside the case. [oald]
;उसने ढक्कन खुला,पलटा और डिब्बे के अन्दर देखा. [manual]
(defrule case10
(declare (salience 650))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(kriyA-inside_saMbanXI  ?id1 ?id)
(id-root ?id1 look)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dibbA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case10    "  ?id "  dibbA)" crlf))
)

;#############################Defaults rule#######################################
;Added by Preeti(30-11-13)
;4,000 new cases of the disease are diagnosed every year. 
;bImArI ke 4,000 naye mAmale prawyeka varRa nixAna kie gaye hEM.
;If this is the case.  ;Added by 14anu-ban-03 (09-12-2014)
;यदि यह  मामला है.
(defrule case_mAmalA
(declare (salience 500))
(id-root ?id case)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAmalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  case.clp 	case_mAmalA   "  ?id "  mAmalA )" crlf))
)
;################### Additional Examples ####################
;There was a ten-foot long stuffed alligator in a glass case.
;The museum was full of stuffed animals in glass cases.
;A glass case full of china broke.
;All most all girls have a jewellery case.
;He has six cases of whisky.
;The winner will receive a case of champagne.
;She held up a blue spectacle case.
;I have a small case for holding pills.
;In some cases people have to wait several weeks for an appointment.
;The company only dismisses its employees in cases of gross misconduct.
;It's a classic case of bad planning.
;People were imprisoned, and, in some cases, killed for their beliefs. 
;We usually ask for references, but in your case it will not be necessary. 
;The whole film is based on a case of mistaken identity.
;If that is the case , we need more staff.
;It is simply not the case that prison conditions are improving.
;In your case, we are prepared to be lenient.
;I cannot make an exception in your case.
;Every application will be decided on a case-by-case basis.
;Police in the town have investigated 50 cases of burglary in the past month.
;He lost his case.[vaha apanA mukaxamA hAra gayA/usane apani petI GumAxI here mAmalA will not be a good usage but all the other cases it can be both]
;The case will be heard next week.
;When does her case come before the court?
;The most serious cases were treated at the scene of the accident.
;Bad diet can cause tiredness, but I don't think that's the case here.

