;@@@ Added by 14anu-ban-05 on (16-02-2015)
;He had a faculty for seeing his own mistakes.[OALD]
;उनके पास खुद की गलतियों को देखने की योग्यता थी.	      [manual]

(defrule faculty1
(declare (salience 101))
(id-root ?id faculty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yogyawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " faculty.clp 	faculty1 " ?id "  yogyawA)" crlf)) 
)

;@@@ Added by 14anu-ban-05 on (16-02-2015)
;He is 90 years old but still has most of his faculties.[word refernce forum]
;वह नब्बे वर्ष के हैं लेकिन  अभी भी उनकी लगभग सभी इंद्रियों की शक्ति बरकरार हैं. [manual]

(defrule faculty2
(declare (salience 102))
(id-root ?id faculty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id iMxriya_kI_Sakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " faculty.clp 	faculty2 " ?id "  iMxriya_kI_Sakwi)" crlf)) 
)

;@@@ Added by 14anu-ban-05 on (16-02-2015)
;Students have called for women and minorities to be placed on the faculty.[word reference forum]
;विद्यार्थी  स्त्रियों और अल्पसङ्ख्यकों   विभाग में रखने के लिये को बुला चुके हैं. [manual]

(defrule faculty3
(declare (salience 103))
(id-root ?id faculty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  viBAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " faculty.clp 	faculty3 " ?id "   viBAga)" crlf)) 
)


;--------------------- Default Rules ------------------
;@@@ Added by 14anu-ban-05 on (16-02-2015)
;IIPM faculties are far more superiour to those of other management institutions.[wordrefernce forum]
;आईआईपीएम सङ्काय अन्य प्रबंधन संस्थानों  सेकहीं अधिक बेहतर हैं [manual]
(defrule faculty0
(declare (salience 100))
(id-root ?id faculty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " faculty.clp    faculty0 " ?id "  safkAya )" crlf))
)


