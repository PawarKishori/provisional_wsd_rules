;@@@ Added by Adit/Soma (13-06-2014)
;It is easy to work in this setting.
;isa vAwAvaraNa meM kAma karanA AsAna hE.
(defrule setting1
(id-word ?id setting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vAwAvaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  setting.clp    setting1   "  ?id " vAwAvaraNa )" crlf))
)

;@@@ Added by Adit/Soma (13-06-2014)
;I caught a glimpse of the setting sun.
;मैंने डूबते हुए सूरज क एक झलक देखी
(defrule setting2
(declare (salience 5000))
(id-word ?id setting)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) sun|moon)
;(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dUbwA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  setting.clp    setting2   "  ?id " dUbwA_huA)" crlf))
)


