
;Added by Meena(17.1.11)
;All relevant Python features are carefully explained and exemplified, and you will quickly come to appreciate Python's suitability for this application area.
(defrule come_to_appreciate
(declare (salience 5000))
(id-root ?id appreciate)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 2) come)
(id-root =(- ?id 1) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2)  praSaMsA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " appreciate.clp  come_to_appreciate  "  ?id "  " (- ?id 1) "   " (- ?id 2) "   praSaMsA_kara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S) 17-dec-2013
;His talents are not fully appreciated in that company.[oald]
;उस कम्पनी में उसकी प्रतिभा की पूरी कदर नहीं है
(defrule appreciate1
(declare (salience 3000))
(id-root ?id appreciate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?sub)
(id-root ?sub talent)
(not(kriyA-object ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  appreciate.clp       appreciate1   "  ?id "  kaxara )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S) 17-dec-2013
;I'd appreciate some help.[oald]
;मैं थोड़ी मदद का आभारी रहूँगा
(defrule appreciate2
(declare (salience 3000))
(id-root ?id appreciate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?obj)
(id-root ?obj help)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA_ABArI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  appreciate.clp       appreciate2   "  ?id "  kA_ABArI_ho )" crlf))
)

;@@@ Added by 14anu-ban-02 (26-11-2014)
;You will appreciate from this table the multi-cultural, international character of the scientific endeavor.[ncert]
;इसके द्वारा आप वैज्ञानिक प्रयासों के बहु-सांस्कृतिक, अन्तर्राष्ट्रीय स्वरूप का मूल्याङ्कन करेँगे.[ncert]
;आप इस सारणी द्वारा वैज्ञानिक प्रयासों के  बहु-सांस्कृतिक अन्तर्राष्ट्रीय स्वरूप का मूल्याङ्कन करेँगे.[modified]
;आप इस सारणी द्वारा वैज्ञानिक प्रयासों के  बहु-सांस्कृतिक अन्तर्राष्ट्रीय स्वरूप कोज जान पायेंगे.[more appropriate];added by 14anu-ban-02(27-11-2014)
(defrule appreciate3
(declare (salience 3000))
(id-root ?id appreciate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI  ?id ?id1)
(id-root ?id1 table)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUlyAMkana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  appreciate.clp       appreciate3   "  ?id "  mUlyAMkana_kara )" crlf))
)

;********************DEFAULT RULE******************************

;@@@ Added by Garima Singh(M.Tech-C.S) 17-dec-2013
(defrule appreciate0
(declare (salience 0))
(id-root ?id appreciate)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarahanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  appreciate.clp       appreciate0   "  ?id "  sarahanA_kara )" crlf))
)

