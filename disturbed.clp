;@@@ Added by 14anu-ban-04 (10-01-2015)
;If you drop a little pebble in a pond of still water, the water surface gets disturbed.       [NCERT-CORPUS]
;यदि आप एक छोटे कङ्कड को किसी तालाब के शान्त जल में धीरे से गिराएँ, तो जल का पृष्ठ  अशांत हो जाता है.               [NCERT-CORPUS]
(defrule disturbed0
(declare (salience 10))
(id-root ?id disturbed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)                          
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aSAMwa/vikRubXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disturbed.clp 	disturbed0   "  ?id " aSAMwa/vikRubXa) " crlf))
)


;@@@ Added by 14anu-ban-04 (10-01-2015)
;He seems very disturbed about his work lately.         [merriam-webster]
;कुछ दिनों से  वह अपने कार्य को लेकर  अत्यन्त व्याकुल प्रतीत होता है .          [self]
(defrule disturbed1
(declare (salience 20))
(id-root ?id disturbed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)  
(viSeRya-about_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyAkula ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disturbed.clp 	disturbed1   "  ?id "  vyAkula )" crlf))
)

