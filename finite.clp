;@@@ Added by 14anu-ban-05 on (15-11-2014)
;It should be noted that though average speed over a finite interval of time is greater or equal to the magnitude of the average velocity, instantaneous speed at an instant is equal to the magnitude of the instantaneous velocity at that instant.[NCERT]
;yahAz yaha waWya XyAna meM raKanA hE ki jahAz kisI sImiwa samaya anwarAla meM vaswu kI Osawa cAla usake Osawa vega ke parimANa ke yA wo barAbara howI hE yA usase aXika howI hE vahIM kisI kRaNa para vaswu kI wAwkRaNika cAla usa kRaNa para usake wAwkRaNika vega ke parimANa ke barAbara howI hE .[NCERT]
(defrule finite0
(declare (salience 1000))
(id-root ?id finite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finite.clp 	finite0   "  ?id "  sImiwa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (15-11-2014)
;We applied the results of our study even to the motion of bodies of finite size, assuming that motion of such bodies can be described in terms of the motion of a particle.[NCERT]
;Pira, yaha mAnawe hue ki parimiwa AkAra ke piMdoM kI gawi ko biMxu kaNa kI gawi ke paxoM meM vyakwa kiyA jA sakawA hE, hamane usa aXyayana ke pariNAmoM ko parimiwa AkAra ke piMdoM para BI lAgU kara xiyA WA.[NCERT]
;During rolling, the surfaces in contact get momentarily deformed a little, and this results in a finite area (not a point) of the body being in contact with the surface.[NCERT]
;lotanika gawi ke samaya samparka pqRToM meM kRaNamAwra ke lie virUpaNa howA hE, waWA isake PalasvarUpa piMda kA kuCa parimiwa kRewraPala (koI biMxu nahIM), lotanika gawi ke samaya pqRTa ke samparka meM howA hE.[NCERT]
(defrule finite1
(declare (salience 1050))
(id-root ?id finite)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 size|area)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parimiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  finite.clp 	finite1   "  ?id "  parimiwa )" crlf))
)
