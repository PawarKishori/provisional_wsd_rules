

;Meaning modified by Meena(26.4.11)
;The arms-dealing affair has severely damaged the reputation of the government. 
;Modified by Meena(5.12.09)
;The explosion damaged his hearing.
(defrule damage2
(declare (salience 4800))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bigAdZa))
;(assert (id-wsd_root_mng ?id bigAdZa_xe))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp 	damage2   "  ?id "  bigAdZa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  damage.clp      damage2   "  ?id " ko )" crlf)
)
)

;"damage","VI","1.bigadZa_jAnA/naRta_ho_jAnA"
;Some parasites damage good crop.




;Added by Meena(10.5.10)
;The box contained many books , some of which were badly damaged .
(defrule damage3
(declare (salience 4700))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp    damage3   "  ?id "   KarAba_ho )" crlf))
)





;@@@ Added by Pramila(Banasthali university) on 01-04-2014
;Also, on Tuesday morning a tower was damaged when a tree in the forest near Suigawadi of Dodha fell on it.   ;news-dev corpus
;गौरतलब है कि मंगलवार सुबह डोडा के सुईगवाड़ी के पास जंगल में पेड़ गिरने से टावर क्षतिग्रस्त हुआ था।
(defrule damage5
(declare (salience 5000))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-samakAlika_kriyA  ?id ?id1)
(kriyA-kriyA_viSeRaNa  ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRawigraswa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp    damage5   "  ?id "   kRawigraswa_ho )" crlf))
)

;@@@ Added by Pramila(Banasthali university) on 01-04-2014
;The front part of the jeep was badly damaged.   ;news-dev corpus
;उसका अगला हिस्सा बुरी तरह क्षतिग्रस्त था.
(defrule damage6
(declare (salience 5000))
(id-word ?id damaged)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRawigraswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp 	damage6   "  ?id "  kRawigraswa )" crlf))
)

;@@@Added by 14anu-ban-01 on (31-07-14).
;Due to the deformity of arunshika there is high damage to the skin of the head. [Karan Singla]
;अरुंषिका की विकृति के कारण सिर की त्वचा को बहुत हानि पहुँचती है ।[Karan Singla]
;In fact the acid which is produced from the sugar stuck to the teeth , the damage to the teeth occurs due to that reason .[Karan Singla]
;वास्तव में जो चीनी दाँतों से चिपकी रहती है तथा इसमें से जो एसिड उत्पन्न होता है , उसके कारण दाँतों को नुकसान होता है ।[Karan Singla]
;On not maintaining precaution chemicals mixed in holy colors cause a lot of damage to skin because of which enthusiasm of holy fun goes cold when something like this happens .[Karan Singla]
;सावधानी न बरतने पर होली के रंग में मिले केमिकल त्वचा को नुकसान पहुँचाते हैं , जिससे होली की मस्ती का उत्साह ठंडा पड़ जाता है जब कुछ ऐसा हो जाए ।[Karan Singla]

(defrule damage07
(declare (salience 5000))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id ?h)
(pada_info (group_head_id ?h) (preposition  ?to ))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?to hAni))
(assert (id-wsd_viBakwi ?h ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " damage.clp	damage07  "  ?id "  " ?to"	 hAni )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  damage.clp 	damage07   "  ?h "  ko )" crlf)) 
)



;$$$ Modified by 14anu-ban-04 (09-12-2014)    ;changed the meaning  from 'nukZasAna_karanA' to 'nukasAna_karane_vAlA'
;[In this sentence there is a parser problem and it is sent for parser correction.Parser treating rubella as a 'verb' but here it is a 'noun'] The rule is working properly on parser no. 4.
;@@@ Added By 14anu17
;The damage rubella.
;नुकसान करने  वाला जर्मन खसरा.
(defrule damage7
(declare (salience 4901))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nukasAna_karane_vAlA))  ;changed meaning by 14anu-ban-04 (09-12-2014) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp 	damage7   "  ?id "  nukasAna_karane_vAlA )" crlf))
)


;@@@ Added  by 14anu-ban-04 (05-02-2015) 
;An electrical surge damaged the computer's disk drive.              [oald]
;वैद्युत आवेश ने संगणक की डिस्क ड्राइव को  नष्ट किया.                             [self]
(defrule damage8
(declare (salience 4901))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 drive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naRta_kara)) 
(assert (kriyA_id-object_viBakwi ?id ko)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  damage.clp      damage8   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp 	damage8  "  ?id "   naRta_kara )" crlf))
)

;@@@ Added  by 14anu-ban-04 (05-02-2015)       
;The police have been ordered to pay substantial damages to the families of the two dead boys.        [cald]
;पुलिस को दो मृत लड़कों के परिवारों को पर्याप्त हर्जाना देने का आदेश दिया गया है.                                                  [self]
(defrule damage9
(declare (salience 4901))
(id-word ?id damages)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id harjAnA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  damage.clp 	damage9  "  ?id "   harjAnA )" crlf))
)

;@@@ Added  by 14anu-ban-04 (26-02-2015)       
;The victim of the accident made claims for damages.                           [hinkhoj]
;दुर्घटना के शिकार ने क्षति के लिए मुआवजे की मांग की.                                            [self]
(defrule damage10
(declare (salience 4910))
(id-word ?id damages)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id1 ?id)
(id-root ?id1 claim)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kRawi))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  damage.clp 	damage10  "  ?id "   kRawi )" crlf))
)

;-------------------- Default Rules ------------------

;"damage","VT","1.hAni_pahuzcAnA/bigAdZanA"
;The snow damaged the roof
;Salience reduced by Meena(10.5.10)
(defrule damage4
(declare (salience 0))
;(declare (salience 4700))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAni_pahuzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp 	damage4   "  ?id "  hAni_pahuzcA )" crlf))
)

;Added by Sonam Gupta MTech IT Banasthali 2013
;The damaging effects of pollution.
;प्रदूषण के हानिकारक प्रभाव . 
(defrule damage0
(declare (salience 5000))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAnikAraka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp 	damage0   "  ?id "  hAnikAraka )" crlf))
)

;"damage","N","1.hAni/GAtA/kRawi"
;There were major damages to his brain after the accident.
;Modified by Sonam Gupta MTech IT Banasthali 2013
;removed '(id-cat_coarse ?id verb)' by Pramila(bu) on 17-02-2014
(defrule damage1
(declare (salience 4900))
(id-root ?id damage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAni))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damage.clp 	damage1   "  ?id "  hAni )" crlf))
)

