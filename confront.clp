;@@@ Added by 14anu-ban-03 (08-04-2015)
;You must confront your opponent. [hinkhoj]
;आपको अपने प्रतिपक्षी से मुकाबला करना चाहिए . [manual]
(defrule confront1
(declare (salience 10))  
(id-root ?id confront)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 opponent)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mukAbalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confront.clp 	confront1   "  ?id "  mukAbalA_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (08-04-2015)
;What would you do, if confronted with this situation again. [hinkhoj]
;आप क्या करेंगे, यदि फिर से इस परिस्थिति के साथ सामना होगा . [manual] 
(defrule confront2
(declare (salience 10))  
(id-root ?id confront)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confront.clp 	confront2   "  ?id "  sAmanA_ho )" crlf))
)

;@@@ Added by 14anu-ban-03 (08-04-2015)
;The economic problems confronting the country. [oald]
;अर्थशास्त्रीय समस्याएँ देश के सामने खडी हो रही हैं . [manual] 
(defrule confront3
(declare (salience 10))  
(id-root ?id confront)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAmane_KadI_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confront.clp 	confront3   "  ?id "  ke_sAmane_KadI_ho )" crlf))
)


;@@@ Added by 14anu-ban-03 (08-04-2015)
;The two accounts confronted fairly well. [oald]
;दोनों खातों का काफी अच्छा मिलान किया . [manual]
(defrule confront4
(declare (salience 10))  
(id-root ?id confront)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 account)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id milAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confront.clp 	confront4   "  ?id "  milAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (08-04-2015)
;The senator said he would confront the bill. [oald]
;सीनेटर ने कहा कि वह बिल का विरोध करेगा . [manual]
(defrule confront5
(declare (salience 10))  
(id-root ?id confront)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 bill)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viroXa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  confront.clp 	confront5   "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confront.clp 	confront5   "  ?id "  viroXa_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (08-04-2015)
;The child screamed when it confronted a fox. [hinkhoj]
;बच्चा चीखा जब उसने लोमडी का सामना किया . [manual]
(defrule confront0
(declare (salience 00))
(id-root ?id confront)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confront.clp         confront0   "  ?id "  sAmanA_kara )" crlf))
)


