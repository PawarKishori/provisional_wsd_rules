;@@@ Added by 14anu-ban-06 (18-04-2015)
;He looked at her hungrily. (cambridge)[parser no.-5]
;उसने बेसब्री से उसकी ओर देखा . (manual)
(defrule hungrily1
(declare (salience 2000))
(id-root ?id hungrily)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 look|kiss)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id besabrI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hungrily.clp 	hungrily1   "  ?id "  besabrI_se )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (18-04-2015)
;They sat down and ate hungrily. (cambridge)
;वे बैठे और भूखे लोगों की तरह खाया . (manual)
(defrule hungrily0
(declare (salience 0))
(id-root ?id hungrily)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUKe_logoM_kI_waraha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hungrily.clp 	hungrily0   "  ?id "  BUKe_logoM_kI_waraha )" crlf))
)
