;@@@ Added by 14anu-ban-01 on (27-12-2014)
;Can I scrounge a cigarette from you.[scrounge.clp]
;क्या मैं आपसे सिगरेट उधार ले सकता हूँ . [scrounge.clp]
(defrule scrounge0
(declare (salience 1))
(id-root ?id scrounge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uXAra_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrounge.clp 	scrounge0   "  ?id "  uXAra_le )" crlf))
)

;Commented the whole rule by 14anu-ban-01 on (27-12-2014) because "(kriyA-kriyArWa_kriyA  ?id1 ?id)" is not the correct condition and its meaning is coming from default rule.
;@@@ Added by 14anu26 [17-06-14]
;He's always scrounging free meals off us.
;वह हमेशा हमसे मु़फ़त का भोजन उधार ले ता रहता है 
;(defrule scrounge0
;(declare (salience 5000))
;(id-root ?id scrounge)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-kriyArWa_kriyA  ?id1 ?id)
;(kriyA-subject  ?id ?sub)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uXAra_le))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " scrounge.clp	 scrounge0  "  ?id "  " ?id1 "  uXAra_le )" crlf))
;)


;Commented the whole rule by 14anu-ban-01 on (27-12-2014) because "(kriyA-kriyA_viSeRaNa  ?id ?id1)" is not the correct condition and its meaning is coming from default rule.
;@@@ Added by 14anu26 [17-06-14]
;Can I scrounge a cigarette from you.
;क्या मैं आपसे सिगरेट उधार ले सकता हूँ . 
;(defrule scrounge1
;(declare (salience 5000))
;(id-root ?id scrounge)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-subject  ?id ?sub)
;(kriyA-kriyA_viSeRaNa  ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uXAra_le))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " scrounge.clp	 scrounge1 "  ?id "  " ?id1 "  uXAra_le )" crlf))
;)


;$$$ Modified by 14anu-ban-01 on (27-12-2014)
;@@@ Added by 14anu26 [17-06-14]
;What is she scrounging for this time?
;वह इस समय क्या सामान ढूण्ढ रही है? 
; इस बार वह किस चीज़ की याचना कर रही है?[Translation improved by 14anu-ban-01 on (27-12-2014)]
(defrule scrounge1
(declare (salience 5000))
;(id-word ?id scrounging) ;commented by 14anu-ban-01
(id-root ?id scrounge);added by 14anu-ban-01 on (27-12-2014)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?sub)
(kriyA-for_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 yAcanA_kara));changed "sAmAna_DUMDa" to "yAcanA_kara" by 14anu-ban-01 on (27-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " scrounge.clp	 scrounge1 "  ?id "  " ?id1 "  yAcanA_kara)" crlf));changed "sAmAna_DUMDa" to "yAcanA_kara" by 14anu-ban-01 on (27-12-2014)
)
