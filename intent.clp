;@@@ Added by 14anu-ban-06 (11-03-2015)
;I was so intent on my work that I didn't notice the time. (OALD)
;मैं मेरे कार्य में इतना लीन  था कि मैंने समय पर ध्यान नहीं दिया था . (manual)
(defrule intent2
(declare (salience 5200))
(id-root ?id intent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI ?id ?id1)
(id-root ?id1 work)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intent.clp 	intent2   "  ?id "  lIna )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-03-2015)
;His face filled with an intent curiosity.(OALD)[parser no.- 4]
;उसका चेहरा अत्यधिक  जिज्ञासा से भर गया. (manual)
(defrule intent3
(declare (salience 5300))
(id-root ?id intent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 curiosity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awyaXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intent.clp 	intent3   "  ?id "  awyaXika )" crlf))
)

;---------------------- Default Rules -------------------
(defrule intent0
(declare (salience 5000))
(id-root ?id intent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kqwasaMkalpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intent.clp 	intent0   "  ?id "  kqwasaMkalpa )" crlf))
)

(defrule intent1
(declare (salience 4900))
(id-root ?id intent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uxxeSya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intent.clp 	intent1   "  ?id "  uxxeSya )" crlf))
)

;"intent","N","1.uxxeSya"
;His intent was to provide a new translation.
;
;


