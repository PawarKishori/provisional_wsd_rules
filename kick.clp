;@@@Modified by 14anu-ban-07,(02-04-2015)
;His wife kicked him out. (cambridge)
;उसकी पत्नी ने  उसको बाहर निकाल दिया . (manual)
;@@@ Added by 14anu04 on 23-June-2014
;They were kicked out of the competition.
;वे स्पर्धा  से बाहर निकाल दिये गये थे. 
(defrule kick_tmp
(declare (salience 5000))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id kicked )  ;commented by 14anu-ban-07,(02-04-2015)
(id-word ?id1 out)
;(test (=(+ ?id 1) ?id1))   commented by 14anu-ban-07,(02-04-2015)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)  ;added by 14anu-ban-07,(02-04-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nikAla_xe))  ;meaning changed from nikAla to nikAla_xe by 14anu-ban-07,(02-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* " kick.clp	kick_tmp  "  ?id "  " ?id1 "  nikAla_xe  )" crlf))
)


(defrule kick0
(declare (salience 5000))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id kicking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id lAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  kick.clp  	kick0   "  ?id "  lAwa )" crlf))
)

;"kicking","N","1.lAwa"
;Get out from here fast before you get a kicking.
;
;

;$$$Modified by 14anu-ban-07,(01-04-2015)
;That project has been kicking around for weeks && nobody has worked on it.(same file)(parser no. 1)
;yaha prAjekta kiwane haPwoM se yUz hI padZA huA hE Ora kisI ne BI isa para kAma nahIM kiyA hE.(manual)
;There's a pen kicking around on my desk somewhere. (oald)(parser no. 1)
;कहीं मेरी मेज पर  एक कलम पड़ा होगा . (manual)
(defrule kick1
(declare (salience 4900))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)  ;commented by 14anu-ban-07,(01-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 padZA_ho)) ;meaning changed from padZe_raha 14anu-ban-07,(01-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick1  "  ?id "  " ?id1 "  padZA_ho  )" crlf))
)

;$$$Modified by 14anu-ban-07,(01-04-2015)
;I'm just sick of being kicked around by him.(same file)
;mEM usake bure vyavahAra se waMga A gayA hUz.
(defrule kick2
(declare (salience 5000))  ; salience increased from 4800 due to kick1 by 14anu-ban-07,(01-04-2015)
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)   ;commented by 14anu-ban-07,(01-04-2015)
(kriyA-by_saMbanXI ?id ?)   ;added by 14anu-ban-07,(01-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 burA_vyavahAra));meaning changed from burA_vyavahAra_kara 14anu-ban-07,(01-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick2  "  ?id "  " ?id1 "  burA_vyavahAra  )" crlf))
)


(defrule kick3
(declare (salience 4700))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xarxa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick3  "  ?id "  " ?id1 "  xarxa_ho  )" crlf))
)

;My leg is starting to kick up again after yesterday's run.
;kala kI xOdZa ke bAxa merI tAzga meM xobArA se xarxa honA SurU ho gayA hE 
;kisI_ke_sAWa_bahuwa_aXika_samaya_biwAnA	0
;I don't like the people she knocks around with.
;muJe ve loga bilakula pasaMxa nahIM hEM jinake sAWa vaha Ajakala iwanA aXika samaya biwAwI hE
(defrule kick4
(declare (salience 4600))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upaxrava));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " kick.clp kick4 " ?id "  upaxrava )" crlf)) 
)

(defrule kick5
(declare (salience 4500))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 upaxrava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick5  "  ?id "  " ?id1 "  upaxrava  )" crlf))
)

(defrule kick6
(declare (salience 4400))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAxaprahAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kick.clp 	kick6   "  ?id "  pAxaprahAra )" crlf))
)

(defrule kick7
(declare (salience 4300))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upaxrava));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " kick.clp kick7 " ?id "  upaxrava )" crlf)) 
)

(defrule kick8
(declare (salience 4200))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 upaxrava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick8  "  ?id "  " ?id1 "  upaxrava  )" crlf))
)

;@@@ Added by 14anu-ban-07, 03-08-2014
;If the connecting string is cut at this instant, the bob will execute a projectile motion with horizontal projection akin to a rock kicked horizontally from the edge of a cliff. (ncert corpus) 
;यायदि इस क्षण पर डोरी को काट दिया जाए तो गोलक एक क्षैतिज प्रक्षेप की भान्ति प्रक्षेप्य गति ठीक उसी प्रकार दर्शाएगा जैसा कि खडी चट्टान से क्षैतिज दिशा में किसी पत्थर को फेंकने पर होता है.
(defrule kick10
(declare (salience 4100))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PeMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kick.clp 	kick10   "  ?id "  PeMka )" crlf))
)


(defrule kick9
(declare (salience 4100))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAwa_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kick.clp 	kick9   "  ?id "  lAwa_mAra )" crlf))
)

;$$$ Modified by 14anu-ban-07,(11-12-2014)
;@@@ Added by 14anu22
;Kick the meeting off.
;सभा प्रारंंभ करो.
(defrule kick11
(declare (salience 6000))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)  ; changed id to id1 by 14anu-ban-07,(11-12-2014)
(or(kriyA-kriyA_viSeRaNa ?id ?id1)(kriyA-upasarga  ?id ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 prAraMBa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick11  "  ?id "  " ?id1 "  prAraMBa_kara )" crlf))
)

;default_sense && category=verb	lAwa mAra	0
;"kick","VT","1.lAwa mAranA"
;Being angry he kicked the door.
;After the examinations were over, children were kicking the ball in
;the  field.
;--"2.xulawwI_mAranA"
;The horse kicked the bucket of water.
;--"3.CutakArA_pAnA/CodZanA"
;It is difficult to kick out a bad habit.
;He kicked out his habit with great difficulty.
;--"4.XikkAranA"
;Having done poorly at the interview, I felt like kicking myself.
;
;

;@@@Added by 14anu-ban-07,(01-04-2015)
;Young people often kick against the rules. (oald)(parser problem)
;तरुण लोग अक्सर नियमों का विरोध करते हैं . (manual)
(defrule kick12
(declare (salience 6100))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 against)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viroXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick12  "  ?id "  " ?id1 "  viroXa_kara  )" crlf))
)

;@@@Added by 14anu-ban-07,(01-04-2015)
; Kick back and enjoy the summer.(oald)(parser no. 3)
;आराम कीजिए और ग्रीष्म का आनन्द उठाइए.(manual)
(defrule kick13
(declare (salience 6200))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ArAma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick13  "  ?id "  " ?id1 "  ArAma_kara  )" crlf))
)

;@@@Added by 14anu-ban-07,(01-04-2015)
;We’ll kick some ideas around and make a decision tomorrow.(oald)(parser no. 2)
;हम कुछ सुझावो पर विचार विमर्श करेंगे और कल निर्णय करेंगे . (manual)
(defrule kick14
(declare (salience 5300))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id2)
(id-root ?id2 idea)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vicAra_vimarSa_kara))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick14  "  ?id "  " ?id1 "  vicAra_vimarSa_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  kick.clp       kick14   "  ?id " para )" crlf))
)


;@@@Added by 14anu-ban-07,(01-04-2015)
;They spent the summer kicking around Europe.(oald)(parser no. 6)
;उन्होंने यूरोप घूमते  हुए ग्रीष्म (काल)बिताया . (manual)
(defrule kick15
(declare (salience 5400))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 GUmawe_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick15  "  ?id "  " ?id1 "  GUmawe_ho  )" crlf))
)

;@@@Added by 14anu-ban-07,(02-04-2015)
;As the storm moves into the desert, the wind and rain start to kick up.(cambridge)
;जब आँधी रेगिस्तान में आती है, हवा और वर्षा तेज होना शुरु हो जाती हैं . (manual)
(defrule kick16
(declare (salience 5400))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
(id-root ?id2 wind|rain|storm)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 weja_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick16  "  ?id "  " ?id1 "  weja_ho  )" crlf))
)

;@@@Added by 14anu-ban-07,(02-04-2015)
;Kick off your shoes in the front hall.(coca)
;सामने के हॉल में आपके जूते उतारिए . (manual)
(defrule kick17
(declare (salience 6100))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)  
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 shoe|shirt)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick17  "  ?id "  " ?id1 "  uwAra )" crlf))
)

;@@@Added by 14anu-ban-07,(02-04-2015)
;It takes half an hour for the medication to kick in.(cambridge)
;यह दवा के प्रयोग  का असर दिखाने के लिए  आधा एक घण्टा लेता है . (manual)
(defrule kick18
(declare (salience 6200))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)  
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 asara_xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick18  "  ?id "  " ?id1 "  asara_xiKA )" crlf))
)

;@@@Added by 14anu-ban-07,(02-04-2015)
;Another plan would require employers to kick in $20 per worker to set up the fund.(cambridge)(parser problem)
;मूलधन शुरू करने के लिए एक और योजना में मालिकों को  प्रति कार्यकर्ता पर $ 20 पूंजी लगाना आवश्यक होगा   . (manual)
(defrule kick19
(declare (salience 6000))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)  
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUMjI_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick19  "  ?id "  " ?id1 "  pUMjI_lagA )" crlf))
)

;@@@Added by 14anu-ban-07,(02-04-2015)
;He kicked out his habit with great difficulty. (same file)
;उसने बडी कठिनाई से उसकी आदत छोडी . (manual)
(defrule kick20
(declare (salience 6000))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)  
(kriyA-upasarga  ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(id-root ?id2 habit)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " kick.clp	kick20  "  ?id "  " ?id1 "  CodZa )" crlf))
)

;@@@Added by 14anu-ban-07,(02-04-2015)
;Having done poorly at the interview, I felt like kicking myself.(same file)
;साक्षात्कार में असंतोषजनक  प्रर्दश के बाद, मैंने स्वतः को धिक्कारना चाहा . (manual) 
(defrule kick21
(declare (salience 4500))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 himself|myself|yourself|themselves)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XikkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kick.clp 	kick21   "  ?id "  XikkAra )" crlf))
)

;@@@Added by 14anu-ban-07,(03-04-2015)
;He was stealing stuff just for kicks.(cambridge)
;वह केवल मज़े के लिए  सामग्री चोरी कर रहा था . (manual)
(defrule kick22
(declare (salience 4500))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-for_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id majZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kick.clp 	kick22   "  ?id "  majZA )" crlf))
)


;@@@Added by 14anu-ban-07,(03-04-2015)
;I get a kick out of driving fast cars.(oald)
;मुझे  तेज गाडियाँ  चला कर मज़े आते  हैं. (manual)
(defrule kick23
(declare (salience 4600))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id majZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kick.clp 	kick23   "  ?id "  majZA )" crlf))
)

;@@@Added by 14anu-ban-07,(03-04-2015)
;I get a real kick out of winning a race.(cambridge)
;मुझे दौड जीतने के बाद  बहुत ही हर्ष और उत्साह  का अनुभव हुआ. (manual)
(defrule kick24
(declare (salience 4700))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 real)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id harRa_Ora_uwsAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kick.clp 	kick24   "  ?id "  harRa_Ora_uwsAha )" crlf))
)

;@@@Added by 14anu-ban-07,(03-04-2015)
;This drink has quite a kick..(oald)
; यह पेय पदार्थ काफी नशीला है . (manual)
(defrule kick25
(declare (salience 4800))
(id-root ?id kick)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 drink)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naSIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kick.clp 	kick25   "  ?id "  naSIlA )" crlf))
)
