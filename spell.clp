;Added by Meena(14.4.10) 
;*   The hindi meaning needs discussion
;Someone laughed suddenly and the spell was broken .
(defrule spell0
(declare (salience 5000))
(id-root ?id spell)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAhOla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spell.clp     spell0   "  ?id "   mAhOla )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (09-01-2015)
;Examples changed and viBakwi removed from default rule.It can be added in a new rule when such a case arises.
;Both SEBI and RBI spell out what non-banking finance companies (NBFCs) mean, and how they should operate.[www.collinsdictionary.com]
; दोनों सेबी और आरबीआई व्याख्या करते हैं  कि गैर-बैंकिंग आर्थिक व्यवस्था कम्पनियाँ (च्स्फ्ब्न) क्या हैं , और उनको कैसे काम करना चाहिए . [self]
;The government will also spell out how it plans to control the costs of the gun registry program.[collinsdictionary.com]
;सरकार भी व्याख्या करेगी कि कैसे वह  बन्दूक पञ्जीयन कार्यालय प्रोग्राम के खर्चों को नियंत्रित रखने की योजना बनाती है   . [self]
;@@@ Added by 14anu26[30-06-14]

(defrule spell3
(declare (salience 5000))
(id-root ?id spell)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vyAKyA_kara))
;(assert (kriyA_id-object_viBakwi ?id kI))               ;commented by 14anu-ban-01  (09-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " spell.clp	spell3  "  ?id "  " ?id1 "  vyAKyA_kara  )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  spell.clp      spell3   "  ?id "  kI )" crlf)               ;commented by 14anu-ban-01 (09-01-2015)
)

;@@@ Added by 14anu-ban-01 on (09-01-2015)
;Example removed from spell3 and added here.I haven't added the condition to check for subject being human( i.e. in human.gdbm) right now to keep the rule generalized.It can be added later if needed.
;He spelled out a more rigorous alternative.[Sentence improved by 14anu-ban-01 on (09-01-2015)
;वह एक अधिक सही विकल्प की व्याख्या करता है .
;उसने एक अधिक परिशुद्ध विकल्प बताया.[Translation improved by 14anu-ban-01 on (09-01-2015)]
(defrule spell4
(declare (salience 5000))
(id-root ?id spell)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 out)
(kriyA-object  ?id ?id2)
(id-root ?id2 solution|alternative|option|consequence)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " spell.clp	spell4  "  ?id "  " ?id1 "  bawA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (09-01-2015)
;Can you really spell out in plain English for our viewers?[COCA]
;क्या आप सचमुच हमारे दर्शकों के लिए स्पष्ट अङ्ग्रेजी में बोल/कह सकते हैं? [self]
(defrule spell5
(declare (salience 5000))
(id-root ?id spell)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 out)
(kriyA-in_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bola/kaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " spell.clp	spell5  "  ?id "  " ?id1 "  bola/kaha )" crlf))
)


;@@@ Added by 14anu-ban-01 on (09-01-2015)
;The scarce data that is available does not spell out in detail what hardships children from simpler economic situations may have experienced.
;जो अपर्याप्त डेटा उपलब्ध है  वह  सविस्तार व्याख्या नहीं करता है कि साधारण आर्थिक स्थिति से आए हुए बच्चों ने किन  कठिनाइयों का अनुभव किया होगा . [self]
(defrule spell6
(declare (salience 5100))
(id-root ?id spell)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-in_saMbanXI ?id ?id2)
(id-root ?id2 detail|negative|positive|advance|contract|type|code);[with reference to COCA]
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vyAKyA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " spell.clp	spell6  "  ?id "  " ?id1 "   vyAKyA_kara )" crlf))
)

;------------------ Default Rules -------------------
;Salience reduced by Meena(13.4.10)
(defrule spell1
(declare (salience 0))
(id-root ?id spell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spell.clp 	spell1   "  ?id "  xOra )" crlf))
)

;"spell","N","1.xOra/vaSIkaraNa"
;She has reached great heights under the spell of her beauty.
;--"2.jAxU"
;The poet recited a spell.
;
(defrule spell2
(declare (salience 4900))
(id-root ?id spell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uccAraNa_karanA_yA_liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spell.clp 	spell2   "  ?id "  uccAraNa_karanA_yA_liKa )" crlf))
)

;"spell","V","1.uccAraNa_karanA_yA_liKanA"
;Some great novelist like Shakespeare used to spell their names differently at different places.
;

