
(defrule faint0
(declare (salience 5000))
(id-root ?id faint)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirbala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  faint.clp 	faint0   "  ?id "  nirbala )" crlf))
)

;"faint","Adj","1.nirbala"
;Illness has made her faint && inactive.
;--"2.aspaRta"
;I only have a faint idea of what she is doing at present.
;--"3.XImA"
;I suddenly felt faint from the pain
;
(defrule faint1
(declare (salience 4900))
(id-root ?id faint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUrCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  faint.clp 	faint1   "  ?id "  mUrCA )" crlf))
)

;"faint","N","1.mUrCA"
;She fell down from the staircase in a faint. 
;
(defrule faint2
(declare (salience 4800))
(id-root ?id faint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUrCiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  faint.clp 	faint2   "  ?id "  mUrCiwa_ho )" crlf))
)

;"faint","VI","1.mUrCiwa_honA"
;She fainted during assembly due to weakness.

;$$$Modified by 14anu-ban-02(09-04-2016)
;She answered in a faint murmur.[sd_verified]
;usane halke svara meM uwwara xiyA.[self]
;@@@ Added by 14anu-ban-07 Soshya Joshi Banasthali University (11-07-2014) 
;The village glittering in the faint light of hundreds of lanterns, ox and horse-carts moving around, camels, elephants and people enjoying the village riding on them are easily seen. (Parallel corpus)
;सैकड़ों  लालटेन  की  धुँधली  रोशनी  में  झिलमिल  करता  गाँता   जगह-जगह  चलते  बैल  व  घोड़ा-गाड़ी  ,  ऊँट  ,  हाथी  व  उस  पर  सवार  हो  कर  गाँव  में  घूमने  का  लुत्फ़  उठाते  लोग  सहज  ही  दिख  जाते  हैं  ।  (Parallel corpus)
;sekadoM lAlatena kE halkI roSanE miM Jilamila karawA gAzva jagaha-jagaha calawe bEla va GodA - gAdE , Uzta, hAWI vA vasa para savAra hokara gAzva meM GUma ne kA luwPa uaTAwe loga sahaja hE xiKa jAwe hE. (manually)

;There is still a faint hope that she may be cured. (COCA)
;uasake svasWa hone kI aBI BI eaka halkI ASA hE. (manual)

(defrule faint3
(declare (salience 5500))
(id-root ?id faint)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halkA))	;meaning changed from 'halkI' to 'halkA'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  faint.clp    faint3   "  ?id "  halkI )" crlf))
)

