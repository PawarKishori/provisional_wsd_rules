
;$$$ Modified by 14anu-ban-01 on (11-03-2015)
;"stabbing","Adj","1.CurA prahAra"
;He felt a stabbing pain in his heart.
;उसने अपने हृदय में एक चुभने वाला दर्द महसूस किया . [self]
(defrule stab0
(declare (salience 5000))
(id-root ?id stab)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id stabbing )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cuBane_vAlA/wIkRNa))	;changed "CurA_prahAra" to "cuBane_vAlA/wIkRNa" by 14anu-ban-01 on (11-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stab.clp  	stab0   "  ?id "  cuBane_vAlA/wIkRNa )" crlf))	;changed "CurA_prahAra" to "cuBane_vAlA/wIkRNa" by 14anu-ban-01 on (11-03-2015)
)


;@@@ Added by 14anu-ban-11 on (23-02-2015)
;He stabbed his finger angrily at my chest.(oald)
;उसने मेरी छाती में क्रोध से अपनी उङ्गली कोंची .  (self)
(defrule stab3
(declare (salience 4802))
(id-root ?id stab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
(id-root ?id1 chest)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id koMcI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  stab.clp 	stab3   "  ?id "  koMcI  )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (11-03-2015)
;@@@ Added by 14anu-ban-11 on (23-02-2015)
;She stabbed her cigarette into the ashtray.(oald)
;उसने राख दानी में उसकी सिगरेट दबाई . (self)
;उसने राख दानी में अपनी सिगरेट झट से बुझाई . (Translation Improved by 14anu-ban-01)
(defrule stab4
(declare (salience 4803))
(id-root ?id stab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?id1)
(id-root ?id1 ashtray)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jata_se_buJA))	;changed "xabA" to "Jata_se_buJA" by 14anu-ban-01 on (11-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stab.clp 	stab4   "  ?id "  Jata_se_buJA)" crlf))	;changed "xabA" to "Jata_se_buJA" by 14anu-ban-01 on (11-03-2015)
)


;@@@ Added by 14anu-ban-11 on (23-02-2015)
;The pain stabbed at his chest.(oald)
;उसकी छाती में दर्द  उठा . (self)
(defrule stab5
(declare (salience 4804))
(id-root ?id stab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 pain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stab.clp 	stab5   "  ?id "  uTa )" crlf))
)


;@@@ Added by 14anu-ban-11 on (23-02-2015)
;He made a good stab in the badminton tournament.(hinkhoj)
;उसने बैडमिंटन खेल प्रतियोगिता में एक अच्छी कोशिश की . (self)
(defrule stab6
(declare (salience 4903))
(id-root ?id stab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 good)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koSiSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stab.clp 	stab6   "  ?id "  koSiSa )" crlf))
)

;@@@ Added by 14anu-ban-11 on (23-02-2015)
;She felt a sudden stab of pain in the chest.      [oald]
;उसने छाती में अचानक टीस महसूस की . (self)
(defrule stab7
(declare (salience 4902))
(id-root ?id stab)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id2))
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 pain)
(id-root ?id2 of)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 ?id1 tIsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stab.clp  stab7  "  ?id "  " ?id2 " " ?id1 "  tIsa )" crlf))
)

;------------------------ Default Rules ----------------------

;"stab","N","1.GAva"
;He's got stab wounds all over his chest.
(defrule stab1
(declare (salience 4900))
(id-root ?id stab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stab.clp 	stab1   "  ?id "  GAva )" crlf))
)

;"stab","V","1.prahAra_karanA"
;He stabbe him in the stomach.
(defrule stab2
(declare (salience 4801))
(id-root ?id stab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prahAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stab.clp 	stab2   "  ?id "  prahAra_kara )" crlf))
)



;;"stab","N","1.GAva"
;He's got stab wounds all over his chest.
;--"2.xarxa"
;A stab of pain in his head.
;--"3.koSiSa"
;He made a good stab in the badminton tournament.
;


