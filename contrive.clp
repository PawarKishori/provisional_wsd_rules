
(defrule contrive0
(declare (salience 5000))
(id-root ?id contrive)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id contrived )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kAlpanika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  contrive.clp  	contrive0   "  ?id "  kAlpanika )" crlf))
)

;"contrived","Adj","1.kAlpanika"
;A contrived news about her missing intended to mislead her family.
;
(defrule contrive1
(declare (salience 00)) ;salience reduced by 14anu-ban-03 (28-04-2015)
(id-root ?id contrive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yukwi_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrive.clp 	contrive1   "  ?id "  yukwi_nikAla )" crlf))
)

;"contrive","V","1.yukwi_nikAlanA{samBavawaH_Cala_ke_lie}"

;$$$ Modified by 14anu-ban-03 (28-04-2015)
;I decided to contrive a meeting between the two of them. [oald]
;मैंने उन दोनों के बीच मुलाकात कराने का फैसला किया . [manual] 
(defrule contrive2
(declare (salience 4800))
(id-root ?id contrive)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 meeting)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karA)) ;meaning changed from 'yukwi_nikAla' to 'karA' by 14anu-ban-03 (28-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contrive.clp 	contrive2   "  ?id "  karA )" crlf))
)

;"contrive","VT","1.yukwi_nikAlanA"
;He can contrive a means of escape from the prison.
;They contrived to murder their boss.
;
