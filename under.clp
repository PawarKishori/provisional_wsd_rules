

;Added by Meena(19.3.10)
;He was convicted under an obscure 1990 law . 
(defrule under0
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 law)
(kriyA-under_saMbanXI  ? ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_wahawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp     under0   "  ?id "  ke_wahawa )" crlf))
)


;$$$ Modified by 14anu-ban-02 (31-07-2014)
;The area of Sultanpur National Park comes under the tropical climate area  .(tourism corpora)
;सुल्तानपुर  राष्ट्रीय  उद्यान  का  क्षेत्र  उष्णकटिबंधीय  जलवायु  के  अंतर्गत  आता  है  ।(tourism corpora)

(defrule under1
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(or(id-word =(+ ?id 1) law)(id-word =(- ?id 1) comes));added -1 word 'comes' by 14 anu-ban-02
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_aMwargawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under1   "  ?id "  ke_aMwargawa )" crlf))
)



(defrule under2
(declare (salience 4900))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_nIce))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under2   "  ?id "  ke_nIce )" crlf))
)

;@@@ ADDED BY PRACHI RATHORE
; These ancient woodlands are under threat from new road developments.[oald]
;$$$ Modified by 14anu21 on 18.06.2014 by added 'direction' in root fact and added (id-root =(+ ?id 2) direction) using or condition
;All work was done by the students under the direction of Rama. 
; सब कार्य राम के निदेशन में विद्यार्थियों के द्वारा किया गया था . 
;सब कार्य राम के निदेशन के नीचे विद्यार्थियों के द्वारा किया गया था . (Translation before modification)
(defrule under3
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition) ;added by 14anu21
(or(id-root =(+ ?id 1) pressure|pudding|art|threat|direction)(id-root =(+ ?id 2) direction))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under3   "  ?id "  meM )" crlf))
)

;"under","Prep","1.ke_nIce/ke_wale/se_kama/ke_aXIna"
;He is doing his doctorate under Prof.Mohan.
;

;$$$ modified by 14anu18
;$$$ Modified by 14anu-ban-07 (05-12-2014)
;These people have been living for years under the shadow of fear.(oald)
;ये लोग भय के साये मे वर्षों से रह रहे है.(manual)
;$$$ Modified by 14anu09 
;The Government maintains that there is no provision under the existing framework to afford protection to a tree .
;;@@@  ADDED BY PRACHI RATHORE
;Elements heavier than iron are also made inside stars [under] special conditions.[gyannidhi]
;  असाधारण स्थिति में लोहा की अपेक्षा भारी तत्व  भी तारों के अन्दर  बनाए गये हैं . 
;The police kept the accused patient under observation.[shiksharthi]
;पुलीस ने पर्यवेक्षण में अभियुक्त मरीज रखा .
;$$$ modified by 14anu24 [03-07-2014];Added drought in the list
;The state is reeling under drought and he has appealed to the Centre for relief .
;राज्य सूखे की चपेट में है और वे केंद्र से मदद की गुहार कर रहे हैं .
;Let me assure you that if you and your ministers are under such an impression,you are entirely mistaken.
;मुझे आपको आश्वासन देने दो यदि आप और आपके मन्त्री एक ऐसी इस धारणा में हैं, तो कि आप सम्पूर्णतया गलत समझे गये हैं 
;$$$ modified by 14anu19(26-06-2014)
(defrule under4
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-under_saMbanXI  ? ?id1)(kriyA-under_saMbanXI  ? ?id1))
(id-root ?id1 condition|guidance|observation|framework|drought|shadow|circumstance|impression)   ;impression is added in list by 14anu19;; added word framework by 14anu09 ;added "circumstance" by 14anu18
                             ;added shadow by 14anu-ban-07 (05-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under4   "  ?id "  meM )" crlf))
)

;$$$ Modified by 14anu-ban-07 (21-10-2014)
;The familiar example is the free fall of an object under gravity.(ncert)
;गुरुत्व के अधीन किसी पिंड का मुक्त पतन इसका सुपरिचित उदाहरण है.(ncert)
;Further 'biology-based' or 'ecological' techniques are under evaluation.(agriculture)
;इसके आगे 'जीव विज्ञान आधारित' या 'पर्यावरणीय' तकनीक मूल्यांकन के अधीन हैं।(agriculture)
;These two points are the temperatures at which pure water freezes and boils under standard pressure.(ncert)
;ये दो नियत बिंदु वह ताप हैं जिन पर शुद्ध जल मानक दाब के अधीन जमता तथा उबलता है.(ncert)
;@@@ ADDED BY PRACHI RATHORE
;But the fact is that for a while Gangotri was under the rule of the Gurkhas.[gyannidhi]
; बात यह थी कि गंगोत्री कुछ समय के लिए गोरख राज्य के अधीन हो गयी थी।
;Educated opinion in the country could not be expected to remain satisfied with a University of this type and the question of amending the Act of 1857 came under active consideration of the University as early as 1890.[gyannidhi]
;देश में शिक्षित लोग इस किस्म के विश्वविद्यालय से संतुष्ट रहें, यह आशा नहीं की जा सकती थी और 1857 के अधिनियम के संशोधन का प्रश्न 1890 में ही विश्वविद्यालय द्वारा सक्रिय रूप से सोच-विचार के लिए चुन लिया गया था।
(defrule under5
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(kriyA-under_saMbanXI  ? ?id1)
(id-root ?id1 rule|consideration|pressure|evaluation|gravity);added 'pressure' by 14anu-ban-07 on 21-10-14 
                        ;added 'evaluation'by 14anu-ban-07 on (03-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_aXIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under5   "  ?id "  ke_aXIna)" crlf))
)


;@@@ ADDED BY PRACHI RATHORE[10-1-14]
(defrule under6
(declare (salience 4900))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-under_saMbanXI  ? ?id1)
(id-root ?id1 teacher)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ki_xeKareKa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under6   "  ?id "  ki_xeKareKa_meM )" crlf))
)

;@@@ ADDED BY PRACHI RATHORE[28-1-14]
;Under Asutosh the Calcutta University assumed a cosmopolitan character. [gyan-nidhi]
;आशुतोष के नेतृत्व में कलकत्ता विश्वविद्shiksharthiयालय ने सही राष्ट्रीय स्वरूप प्राप्त किया।
(defrule under7
(declare (salience 4900))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-under_saMbanXI  ? ?id1)
(or(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-cat_coarse ?id1 PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_newqwva_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under7   "  ?id "  ke_newqwva_meM )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 04.07.14
;He was working under the flag of the United Nations.
;वह यूनाइटेड नेशंस के लिए काम कर रहा था.
(defrule under08
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-under_saMbanXI ? ?id1)
(id-root ?id1 flag)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lie))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " under.clp	under08  "  ?id "  " ?id1 "  lie  )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 04.07.14
;The report flagged up the dangers of under age drinking.
;रिपोर्ट ने निम्नायु पीने की विपत्तियाँ के ऊपर संकेत दीं . 
(defrule under09
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 age)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nimnAyu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " under.clp	under09  "  ?id "  " ?id1 "  nimnAyu  )" crlf))
)

;$$$ Modified by 14anu-ban-07 (22-10-2014)
;@@@ Added by 14anu-ban-06 (20-10-2014)
;The following relation for rectilinear motion under constant acceleration a has been encountered in Chapter 3.(NCERT)
;अध्याय 3 में, नियत त्वरण a के अन्तर्गत सरल रेखीय गति के लिए आप निम्न भौतिक सम्बन्ध पढ चुके हैं. (NCERT)
(defrule under8
(declare (salience 5200))      ;salience increased from 5000 to 5200 by 14anu-ban-07
(Domain physics)     ;added by 14anu-ban-07
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 acceleration|velocity)
(viSeRya-under_saMbanXI  ? ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_aMwargawa))
(assert (id-domain_type  ?id physics))   ;added by 14anu-ban-07
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp     under8   "  ?id "  ke_aMwargawa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  under.clp 	under8   "  ?id "  physics )" crlf))  ;added by 14anu-ban-07
)

;@@@ Added by 14anu-ban-07 (22-10-2014)
;The familiar example is the free fall of an object under gravity. (ncert)
;गुरुत्व के अधीन किसी पिंड का मुक्त पतन इसका सुपरिचित उदाहरण है.(ncert)
(defrule under9
(declare (salience 5000))
(Domain physics)
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(viSeRya-under_saMbanXI ? ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_aXIna))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under9   "  ?id "  ke_aXIna )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  under.clp 	under9   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-07 (22-10-2014)
;If the displacements are allowed to approach zero, then the number of terms in the sum increases without limit, but the sum approaches a definite value equal to the area under the curve in Fig. 6.3 (b). (ncert)
;यदि विस्थापनों को अतिसूक्ष्म मान लिया जाए तब योगफल में पदों की सङ्ख्या असीमित रूप से बढ जाती है लेकिन योगफल एक निश्चित मान के समीप पहुञ्च जाता है जो चित्र 6.3(b) में वक्र के नीचे के क्षेत्रफल के समान होता है .(ncert)
(defrule under10
(declare (salience 5100))
(Domain physics)
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(viSeRya-under_saMbanXI ? ?id1)
(id-root ?id1 curve)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_nIce))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under10   "  ?id "  ke_nIce )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  under.clp 	under10   "  ?id "  physics )" crlf))
)


;@@@ Added by Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;Children aged 12 and under are applicable for the test. 
;12 और से कम वयस्क हुए बच्चे परीक्षा के बाद लागू होना हैं . 
(defrule under11
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(conjunction-components  ? ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under11   "  ?id "  se_kama )" crlf))
)
;@@@Added by 14anu-ban-07,(07-02-2015)
;The matter is under investigation. (oald)
;विषय जाँच प्रक्रिया में है . (self)
(defrule under12
(declare (salience 5000))
(id-root ?id under)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-under_saMbanXI  ?id1 ?id2)
(id-root ?id2 investigation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakriyA_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  under.clp 	under12   "  ?id "  prakriyA_meM )" crlf))
)


