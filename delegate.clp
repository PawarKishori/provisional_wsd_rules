;@@@ Added by 14anu-ban-04 (04-03-2015)
;I've been delegated to organize the Christmas party.                   [oald]
;मैं क्रिसमस पार्टी  को आयोजित करने के लिए नियुक्त किया गया हूँ .                            [self]
(defrule delegate2
(declare (salience 20))
(id-root ?id delegate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  niyukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delegate.clp 	delegate2 "  ?id " niyukwa_kara )" crlf))
)

;------------------------ Default Rules ---------------------


;@@@ Added by 14anu-ban-04 (04-03-2015)
;Authority to make financial decisions has been delegated to a special committee.                [cald]
;आर्थिक निर्णय तय करने के लिए  एक विशेष समिति को अधिकार सौंपा गया है .                                                 [self]
;As a boss you have to delegate responsibilities to your staff.                               [cald]
;बॉस के जैसा आपको अपने कर्मचारियों को उत्तरदायित्व सौंपने है .                                               [self]
(defrule delegate1
(declare (salience 10))
(id-root ?id delegate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sOMpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delegate.clp 	delegate1 "  ?id "  sOMpa )" crlf))
)

;@@@ Added by 14anu-ban-04 (04-03-2015)
;Congress delegates rejected the proposals.                     [oald]
;कांग्रेस  प्रतिनिधियों ने प्रस्तावों को   अस्वीकार  किया .                         [self]
(defrule delegate0
(declare (salience 10))
(id-root ?id delegate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawiniXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delegate.clp 	delegate0  "  ?id "   prawiniXi )" crlf))
)
