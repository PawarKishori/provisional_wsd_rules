;@@@ Added by 14anu-ban-06 (18-02-2015)
;Katya delivered some sharp and incisive comments.(COCA)
;कॉट्य ने कुछ कटु और तीक्ष्ण टिप्पणियाँ दीं .(manual)
(defrule incisive0
(declare (salience 0))
(id-root ?id incisive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wIkRNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incisive.clp 	incisive0   "  ?id "  wIkRNa )" crlf))
)

;@@@ Added by 14anu-ban-06 (18-02-2015)
;An incisive performance.(OALD)
;प्रभावशाली प्रदर्शन . (manual)
;In his first assignment as the company's music director, delivered a nuanced and incisive performance. 
;(COCA)
;कम्पनी के सङ्गीत निर्देशक की तरह उसके पहले सौंपे हुए कार्य में ,सूक्ष्म भेद युक्त और प्रभावशाली प्रदर्शन पेश किया . (manual)
(defrule incisive1
(declare (salience 2000))
(id-root ?id incisive)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 performance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAvaSAlI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incisive.clp 	incisive1   "  ?id "  praBAvaSAlI )" crlf))
)
