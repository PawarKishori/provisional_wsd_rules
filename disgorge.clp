;@@@ Added by 14anu-ban-04 (22-04-2015)
;The bus disgorged a crowd of noisy children.            [oald]
;बस ने कोलाहल कारी बच्चों की एक भीड़ को पहुँचाया .                     [self]
(defrule disgorge1
(declare (salience 20))
(id-root ?id disgorge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id2)
(id-root ?id2 bus|train|plane|aeroplane|truck|ship)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzcA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgorge.clp 	disgorge1  "  ?id "  pahuzcA )" crlf))
)

;@@@ Added by 14anu-ban-04 (22-04-2015)
;The delayed commuter train disgorged hundreds of angry passengers.           [cald]
;आना-जाना करने वाली विलम्बित रेलगाडी ने सैंकडों  क्रोधित यात्रियों को पहुँचाया.                       [self]
(defrule disgorge2
(declare (salience 30))
(id-root ?id disgorge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id2)
(id-root ?id2 bus|train|plane|aeroplane|truck|ship)
(kriyA-object ?id ?obj)
(viSeRya-of_saMbanXI ?obj ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzcA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgorge.clp 	disgorge2  "  ?id "  pahuzcA )" crlf))
)

;-----------------------------------------------------DEFAULT RULE -----------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (22-04-2015)
;The pipe disgorges sewage into the sea.         [oald]
;पाइप समुद्र में गन्दा पानी छोड़ता है .                        [self]
(defrule disgorge0
(declare (salience 10))
(id-root ?id disgorge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Coda))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgorge.clp 	disgorge0  "  ?id "  Coda )" crlf))
)
