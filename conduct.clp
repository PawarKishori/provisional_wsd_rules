
(defrule conduct0
(declare (salience 5000))
(id-root ?id conduct)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMcAlana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conduct.clp 	conduct0   "  ?id "  saMcAlana )" crlf))
)

;$$$ Modified by 14anu-ban-03 (07-04-2015)  ; as there is not default rule for verb in this clp file
;$$$ Modified by 14anu07 0n 02/07/2014
;You cannot conduct business if you are not enterprising. 
;आप उद्योग नहीं चला सकते हैं यदि आप उद्योगी नहीं हैं . 
(defrule conduct1
(declare (salience 4900))
(id-root ?id conduct)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1)) ;added by 14anu07   ;commented by 14anu-ban-03 (07-04-2015)
;(id-root ?id1 business) ;added by 14anu07          ;commented by 14anu-ban-03 (07-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conduct.clp 	conduct1   "  ?id "  calA )" crlf))
)

;"conduct","V","1.calAnA"
;You cannot conduct business if you are not enterprising. 
;
;

;@@@ Added by Preeti(11-1-14)
;She lectured the girl on her conduct. [By mail]
;usane ladakI ko usake AcaraNa para  upaxeSa/BARaNa xiyA.
(defrule conduct2
(declare (salience 5050))
(id-root ?id conduct)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AcaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conduct.clp 	conduct2   "  ?id "  AcaraNa) )" crlf))
)

;@@@ Added by 14anu-ban-03(26-08-2014)
;The extraordinary and excellent religious functions and customs are conducted in the Parassinikkadavu Sree Muthappan Temple . [tourism corpus]
;परश्शिनिक्कटवु  श्रीमुत्तप्पन  मंदिर  में  अपूर्व  एवं  अनुपम  धार्मिक  अनुष्ठान  एवं  आचार  संपन्न  होते  हैं  ।
(defrule conduct3
(declare (salience 4900))
(id-root ?id conduct)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sampanna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conduct.clp 	conduct3   "  ?id "  sampanna )" crlf))
)
;@@@ Added by 14anu-ban-03(26-08-2014)
;This marble statue had come out during the excavation conducted in this area on 16 August, 1956. [tourism corpus]
;यह  संगमरमर  मूर्ति  इस  क्षेत्र  में  कराई  गई  खुदाई  के  दौरान  16  अगस्त  ,  1956  को  निकली  थी  ।
(defrule conduct4
(declare (salience 4900))
(id-root ?id conduct)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conduct.clp 	conduct4   "  ?id "  karA )" crlf))
)

;@@@ Added by 14anu-ban-03(26-08-2014)
;A yajna was conducted from the eleventh day to the fourteenth day of the fortnight in Garh Mukteshwar for the peace of the souls killed in the war of the Mahabharata .[tourism corpus]
;एकादशी  से  चतुर्दशी  तक  गढ़  मुक्तेश्वर  में  महाभारत  युद्ध  में  मारे  गए  सभी  लोगों  की  आत्मा  की  शांति  के  लिए  यज्ञ  किया  गया था ।
(defrule conduct5
(declare (salience 4900))
(id-root ?id conduct)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conduct.clp 	conduct5   "  ?id "  kiyA )" crlf))
)

;$$$ Modified by 14anu-ban-03 (13-12-2014)
;@@@ Added by 14anu07 0n 02/07/2014
;A survey was conducted among students.
;एक सर्वेक्षण के छात्रों के बीच आयोजित किया गया.
(defrule conduct04
(declare (salience 4900))
(id-root ?id conduct)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1)) ;commented by 14anu-ban-03 (13-12-2014)
(kriyA-among_saMbanXI  ?id ?id1)  ;added by 14anu-ban-03 (13-12-2014)
(id-root ?id1 student)  ;added by 14anu-ban-03 (13-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  Ayojana_kara))  ;meaning changed from 'Ayojana' to 'Ayojana_kara' by 14anu-ban-03 (13-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conduct.clp 	conduct04   "  ?id "   Ayojana_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (07-04-2015)
;He conducted himself with grace and dignity throughout the trial.[OALD]
;वह परीक्षण के दौरान शिष्टता और गरिमा के साथ पेश आया.            [MANUAL]
(defrule conduct6
(declare (salience 4900))
(id-root ?id conduct)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1)
(id-root ?id1 grace|dignity)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id peSa_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  conduct.clp 	conduct6   "  ?id "  peSa_A )" crlf))
)



