
(defrule scale0
(declare (salience 5000))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badZA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " scale.clp scale0 " ?id "  badZA )" crlf)) 
)

(defrule scale1
(declare (salience 4900))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 badZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " scale.clp	scale1  "  ?id "  " ?id1 "  badZA  )" crlf))
)

;@@@ Added by jagriti(31.12.2013)
;The prisoner scaled the high prison wall and ran off.[cald]
;कैदी उँची जेल की दीवार पर चढ़्ा और भाग गया.
(defrule scale2
(declare (salience 4800))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 wall|mountain|hill)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  caDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scale.clp 	scale2   "  ?id "   caDZa )" crlf))
)

;@@@ Added By jagriti(31.12.2013)
;Rise up the social scale. [iit-bombay]
;सामाजिक स्तर ऊपर उठाओ.
;;The world is facing economic crisis on a global scale.
;दुनिया को वैश्विक स्तर पर आर्थिक संकट का सामना करना पड़ रहा है.
(defrule scale3
(declare (salience 4800))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 social|political|global)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scale.clp 	scale3   "  ?id "  swara )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (12-12-2014)
;@@@ Added by 14anu01 on 27-06-2014
;They scale the fish to cook.
;वे पकाने के लिए मछलियाँ छिलका या परत उतारते हैं . 
;वे पकाने के लिए मछलियों की परत उतारते हैं . [Translation improved by 14anu-ban-01 on (12-12-2014)]
(defrule scale006
(declare (salience 5000))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1);added by 14anu-ban-01 on (12-12-2014)]
(id-root ?id1 wall|ceiling|fish|floor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parawa_uwAra))
(assert (kriyA_id-object_viBakwi ?id kA));added by 14anu-ban-01 on (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  scale.clp 	scale006   "  ?id " kA )" crlf);added by 14anu-ban-01 on (12-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scale.clp 	scale006   "  ?id "  parawa_uwAra )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (12-12-2014)
;@@@ Added by 14anu06(Vivek Agarwal) on 18/6/2014***********
;The runway was scaled for jumbo jets.
;दौडपथ बहुत बडे जेट विमानों के लिए नापा गया था . 
;विमान पट्टी   जंबो जेट/विशाल जेट वायुयानों के लिए नापी गयी थी . [Translation improved by 14anu-ban-01 on (12-12-2014)]
(defrule scale06
(declare (salience 5000))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma ?id ?id1)
(id-root ?id1 runway|road|highway|bridge|building)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nApa ));meaning changed from "nApA" to "nApa" by 14anu-ban-01 on (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " scale.clp   scale06   "   ?id " nApa )" crlf))
);meaning changed from "nApA" to "nApa" by 14anu-ban-01 on (12-12-2014)


;$$$ Modified by 14anu-ban-01 on (12-12-2014)
;@@@ Added by 14anu06(Vivek Agarwal) on 18/6/2014***********
;The fish was covered with scales.
;मछलियाँ पपडी कि परत के साथ ढकी गयीं थीं .  
;मछली शल्कों से ढकी हुई थी.[Translation improved by 14anu-ban-01 on (12-12-2014)]
(defrule scale7
(declare (salience 5000));salience reduced to 5000 from 6000 by 14anu-ban-01 on (12-12-2014)]
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI ?id1 ?id)
(id-root ?id1 cover|coat);14anu-ban-01 removed "has|had" from the list on (12-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Salka/XArI));meaning changed from "papadZI_ki_parawa" to "Salka" by 14anu-ban-01 on (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " scale.clp   scale7   "   ?id " Salka/XArI )" crlf))
);meaning changed from "papadZI_ki_parawa" to "Salka" by 14anu-ban-01 on (12-12-2014)


;$$$ Modified by 14anu-ban-01 on (12-12-2014)
;@@@ Added by 14anu06(Vivek Agarwal) on 18/6/2014***********
;The fish had scales.
;मछलियाँ पपडी कि परत के साथ ढकी गयीं थीं .  
;मछली के शल्क थे.[Translation improved by 14anu-ban-01 on (12-12-2014)]
;The snake had long and green scales. 
;साँप की लम्बी और हरी धारियाँ थी . 
(defrule scale8
(declare (salience 6000));salience reduced to 5000 from 6000 by 14anu-ban-01 on (12-12-2014)]
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id )
(id-root ?id1 has|had|have);14anu-ban-01 removed "cover|coat" from the list on (12-12-2014)
(id-root ?id2 fish|snake|reptile);added by 14anu-ban-01 on (12-12-2014)
(kriyA-subject ?id1 ?id2);added by 14anu-ban-01 on (12-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XArI/Salka));meaning changed from "papadZI_ki_parawa" to "Salka" by 14anu-ban-01 on (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " scale.clp   scale8   "   ?id " XArI/Salka )" crlf))
);meaning changed from "papadZI_ki_parawa" to "Salka" by 14anu-ban-01 on (12-12-2014)


;Commented the rule by 14anu-ban-01 on (12-12-2014) as this rule is unnecessary and the rule scale8 is giving the correct meaning required for this context.
;@@@ Added by 14anu23 on 26/6/14
;The snake had long and green scales. 
;साँप की लम्बी और हरी धारियाँ थी . 
;(defrule scale_tmp
;(declare (salience 4900))
;(id-word ?id scales)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaNa  ?id ?)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id  XAriyAz))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scale.clp 	scale_tmp   "  ?id "   XAriyAz )" ;crlf))
;)


;------------------ Default rules ---------------------....

;@@@ Added by 14anu-ban-01 on (19-10-2014)
;Absolute zero is the foundation of the Kelvin temperature scale or absolute scale temperature named after the British scientist Lord Kelvin.[NCERT corpus]
;parama SUnya britiSa vEjFAnika loYrda kelvina ke nAma para kelvina wApa mApakrama aWavA parama wApa mApakrama kA AXAra hE.[NCERT corpus]
(defrule scale6
(declare (salience 2))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(Domain physics)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id mApakrama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scale.clp 	scale6   "  ?id "  mApakrama)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* " scale.clp 	scale6    "  ?id "  physics )" crlf)
)
)


;@@@ Added By jagriti(5.12.2013)
(defrule scale4
(declare (salience 1))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEmAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scale.clp 	scale4   "  ?id "  pEmAnA )" crlf))
)

;He scalled the fish to cook.
(defrule scale5
(declare (salience 1))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CilakA_yA_parawa_uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scale.clp 	scale5   "  ?id "  CilakA_yA_parawa_uwAra )" crlf))
)

;default_sense && category=verb	CilakA yA parawa uwAra	0
;"scale","V","1.CilakA yA parawa uwAranA"
;He scalled the fish to cook.
;
;LEVEL 
;
;
;                 scale sUwra (nibanXa)
;                 ----- 
;
;`scale' Sabxa ke viviXa prayoga--
;-------------------------
; 
;"scale","N","1.CilakA" 
;Generally all the fishes have scales on their bodies.
;--"2.papadZI"  
;           ------ < CilakA
;Buds with a covering of sticky scales are beautiful to see.
;--"3.mApa" 
;          ----- < pAwra < CilakA 
;The world is facing economic crisis on a global scale.
;--"4.mApakrama" 
;           ---- < pAwra(kramarUpa meM) < CilakA  
;There was corruption on a grand scale.
;--"5.mAnaxaNda"  
;            ----- < pAwra(swara) < CilakA
;Decimal scale was first introduced in India.
;--"6.svaragrAma" 
;            ----- < mApa < pAwra < CilakA 
;Vanya practises scales on the piano.
;--"7.pEmAnA" 
;            ----- < mApa < pAwra < CilakA 
;It was impossible to comprehend the full scale disaster.
;The scale of fees in the schools now a days is very high.
;The Earthquake measured 76 on the Ritcher scale.
;This ruler has one scale in centimetre && another in inches.
;At the other end of the scale we find gross poverty.
;--"8.kIta"
;Scale is a kind of material that deposited inside the kettles.
;
;"scale","V","1.CilakA yA parawa uwAranA"
;                           ----- < CilakA 
;He scalled the fish to cook. 
;--------------------------------------------------
;
;sUwra : mApa`[<_CilakA/kIta]
;
;yA
;
;mApa`[<_CilakA]/kIta
;
;
;
;     `scale' ke Upara xiye gaye viviXa prayogoM ke mUla xo Sabxa hEM . xonoM skela
;Sabxa hI hEM . eka mUla Sabxa kA arWa hE- CilakA . xUsare kA arWa hE- kIta . 
;Cilake ke mUlavAle Sabxa kA anya arWa-viswAra ho rahA hE . 
;(vEse Cilake kA mUla BI kIta mAnA jA sakawA hE cUzki Cilake kA BOwika 
;                                           srowa BI kIta hI hE) . 
;
;-- Cilake ke mUlavAle Sabxa se mApane ke arWavAle SabxoM kA viswAra huA hE . kuCa 
;samaya pahale loga samuxrI-jIva va anya jIvoM ke SarIra ke CilakoM kA, pAwra rUpa meM 
;prayoga kiyA karawe We . (SaMKa Axi jina kItoM ke howe hEM, veM una kItoM ke SarIra
;ke Cilake howe hEM) . veM pAwra, mApane ke kAma meM Ane lage(Ajakala BI loga pAwroM 
;kA prayoga mApane ke lie karawe hEM) . isase yaha Sabxa- 
;         Cilake ke rUpa meM , CilakoM ke rUpa se pAwrarUpa meM, vahAz se mApane ke
;vaswuoM ke rUpa meM prayukwa huA .  
;
;-- mApavAcI saBI Sabxa tippaNiyoM se samaJe jA sakawe hEM . mUla meM mApa hI hE .
;usake viviXarUpoM (sAXana Axi) meM isa Sabxa kA prayoga hE .  
;
;-- svaragrAma Sabxa BI mApane ke mUlavAlA hI hE . svara kI jAnakArI mApane ke 
;xvArA hI samBava hE . (cAhe kisI sAXana xvArA svara mApA jAwA ho) .
;isase yaha Sabxa `svaragrAma' ke rUpa meM socA jA sakawA hE . 
;
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_scale3
(declare (salience 4800))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 social|political|global)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " scale.clp   sub_samA_scale3   "   ?id " swara )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_scale3
(declare (salience 4800))
(id-root ?id scale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 social|political|global)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " scale.clp   obj_samA_scale3   "   ?id " swara )" crlf))
)
