;##############################################################################
;#  Copyright (C) 2014-2015 Gurleen Bhakna (gurleensingh@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-05 on (09-01-2015)    ----corrected spelling from 'masUDA' to 'masUdA'
;uncommented the rule
;Commented by Shirisha Manju Suggested by Chaitanya Sir --- This rule is too restricted and it can be handled in gum5
;@@@ Added by 14anu05 GURLEEN BHAKNA on 17.06.14
;Bleeding gums is usually a sign of gum disease.
;खून बहता हुआ मसूढा आम तौर पर मसूढों की बीमारी का संकेत है .
(defrule gum1
(declare (salience 5000))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
;(id-word ?id1 bleeding|healthy)		;commented by 14anu-ban-05 on (09-01-2015)
;(samAsa_viSeRya-samAsa_viSeRaNa ?id2 ?id)	;commented by 14anu-ban-05 on (09-01-2015)
;(id-word ?id2 disease)				;commented by 14anu-ban-05 on (09-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id masUdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gum.clp      gum1    "  ?id "  masUdA )" crlf)
)
)

;@@@ Added by 14anu18
;He had a pain in his gums.
;उसके मसूडों में दर्द था . 
(defrule gum2
(declare (salience 5000))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa ?id ?id2)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id masUdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gum.clp 	gum2   "  ?id "  masUdA )" crlf))
)

;$$$ Modified by Shirisha Manju 22-07-16
;@@@ Added by 14anu18
;He was chewing a gum.
;वह च्यूइंग गम चबा रहा था.
(defrule gum3
(declare (salience 5000))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(or(id-word =(- ?id 1) chewing)(and(kriyA-object ?id2 ?id)(id-root ?id2 chew))) commented by Shirisha Manju
(id-word =(- ?id 1) chewing) ; added by shirisha Manju
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id  cyUiMga_gama)) commented by Shirisha Manju
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) cyUiMga_gama)) ;added by Shirisha Manju
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gum.clp    gum3  "  ?id "  " (- ?id 1) " cyUiMga_gama )" crlf) 
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gum.clp 	gum3   "  ?id "  cyUiMga_gama )" crlf)
)
)

;@@@ Added by 14anu23 on 30/6/14
;. A tooth broken off just above the gum.  [google meaning ]
;दाँत मसूडा के ऊपर बस अचानक रुका हुआ . 
;दाँत मसूडे के ठीक ऊपर से टूट गया	; translation modified by 14anu-ban-05 on (09-01-2015)
(defrule gum04
(declare (salience 5000))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
(kriyA-above_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id masUdZA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gum.clp       gum04   "  ?id "  masUdZA )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (09-01-2015)
;@@@ Added by 14anu23 on 30/6/14   [google meaning ]
;Gum disease
;मसूडा की बीमारी .
(defrule gum5
(declare (salience 5005))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-object  ?id ?id1)	;commented by 14anu-ban-05
(id-root =(+ ?id 1) disease)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id masUdZA))
(assert  (id-wsd_viBakwi   ?id  kA))	;added by 14anu-ban-05 on (09-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  gum.clp 	gum5   "  ?id "  masUdZA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  gum.clp      gum5   "  ?id " kA )" crlf)
) ;added by 14anu-ban-05 on (09-01-2015)
) 



;$$$ Modified by Shirisha Manju 18-12-2014
;#### The receipts are gummed into a special book.
;@@@ Added by 14anu23 on 30/6/14
;The two-year-old gummed his mother's plastic-coated ration card.  [google meaning ] 
;two-year-old ने उसकी माँ का plastic-coated राशन कार्ड गोंद लगाया . 
(defrule gum6
(declare (salience 5000))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not (kriyA-for_saMbanXI  ?id ?)) ;added by Shirisha Manju 18-12-14 suggested by Chaitanya Sir 
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id goMxa_lagAyA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  gum.clp       gum6   "  ?id "  goMxa_lagAyA )" crlf))
)

;$$$ Modified by Shirisha Manju 18-12-2014
;@@@ Added by 14anu23 
;Gum up the works. [thefreedictionary.com]
;रचना बिगाडना . 
(defrule gum7
(declare (salience 5000))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 up) ;added by Shirisha Manju 18-12-14
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bigAdZanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gum.clp	gum7 "  ?id "  " ?id1 "  bigAdZanA )" crlf))
)

;@@@ Added by shirisha Manju Suggested by Chaitanya Sir (22-07-2016)
;Bacteria stay between our gums and teeth.
;हमारे मसूढों और दाँतों के बीच बैक्टीरिया मौजूद होते हैं .
(defrule gum8
(declare (salience 3010))
(id-word ?id gums)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id masUdZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gum.clp      gum8   "  ?id "  masUdZA )" crlf)
)
)


;-------------DEFAULT RULE-----------
;Same rule added by 14anu18 and 14anu23
;@@@ Added by 14anu05 GURLEEN BHAKNA on 17.06.14, 14anu18 and 14anu23
(defrule gum0
(declare (salience 3000))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id goMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gum.clp      gum0    "  ?id "  goMxa )" crlf)
)
)
;@@@ Added by 14anu18
;The receipts are gummed into a special book.
;रसीदें एक विशेष पुस्तक के अन्दर गोंद से चिपकाई जाती हैं. 
(defrule gum4
(declare (salience 500))
(id-root ?id gum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gonxa_se_cipakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gum.clp 	gum4   "  ?id "  gonxa_se_cipakA )" crlf))
)
