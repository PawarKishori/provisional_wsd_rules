
;@@@Added by 14anu-ban-02(25-03-2015)
;Sentence: The whole episode was a blatant attempt to gain publicity.[mw]
;Translation: पूरी कड़ी लोक प्रसिद्धि प्राप्त करने की एक जबरदस्त कोशिश थी . [self]
(defrule blatant1 
(declare (salience 100)) 
(id-root ?id blatant) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id2)
(id-root ?id2 gain)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id jabaraxaswa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blatant.clp  blatant1  "  ?id "  jabaraxaswa )" crlf)) 
) 

;-----------------default_rules-----------------------------------------------------------

;@@@Added by 14anu-ban-02(25-03-2015)
;Sentence: It was a blatant lie.[oald]
;Translation: यह एक खुला झूठ था . [self]
(defrule blatant0 
(declare (salience 0)) 
(id-root ?id blatant) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id KulA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blatant.clp  blatant0  "  ?id "  KulA )" crlf)) 
) 
