;@@@ Added by 14anu-ban-04 (04-02-2015)
;The tyre of the bus deflated near the bus stop.                   [hinkhoj]
;बस स्टाप  के निकट बस के टायर की  हवा  निकल गयी.                              [self]
(defrule deflate0
(declare (salience 10))
(id-root ?id deflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id havA_nikala_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deflate.clp 	deflate0   "  ?id " havA_nikala_jA )" crlf))
)

;@@@ Added by 14anu-ban-04 (04-02-2015)
;The price of essential items could not be deflated even at the time of full crop.            [hinkhoj]
;पूरी पैदावार के समय में भी आवश्यक चीजों का मूल्य  कम नहीं   हो  सका .                                                 [self]
(defrule deflate1
(declare (salience 20))
(id-root ?id deflate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 price)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deflate.clp 	deflate1   "  ?id " kama_ho )" crlf))
)


