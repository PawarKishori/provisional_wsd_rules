;@@@Added by 14anu-ban-02(07-03-2015)
;My father was a distant, austere man.[oald]
;मेरे पिता एक अलग,सख्त आदमी थे . [self]
(defrule austere1 
(declare (salience 100)) 
(id-root ?id austere) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id) 
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id saKwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  austere.clp  austere1  "  ?id "  saKwa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(07-03-2015)
;The monks’ austere way of life.[oald]
;भिक्षुओं का कठिन जीने का ढङ्ग.[self]
(defrule austere2 
(declare (salience 100)) 
(id-root ?id austere) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id) 
(id-root ?id1 way|childhood)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id kaTina)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  austere.clp  austere2  "  ?id "  kaTina )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(07-03-2015)
;Sentence: Their clothes were always austere.[oald]
;Translation: उनके वस्त्र हमेशा सादे होते थे .[self] 
(defrule austere0
(declare (salience 0))
(id-root ?id austere)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  austere.clp  austere0  "  ?id "  sAxA )" crlf))
)

