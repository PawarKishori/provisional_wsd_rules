;I suppose his criticism was fair comment .
;mEM mAna lewA hUz ki usakI AlocanA uciwa tippaNI WI
;Added by sheetal(1-10-09).
(defrule fair2
(declare (salience 5000))
(id-root ?id fair)
?mng <-(meaning_to_be_decided ?id)
;(link_name-lnode-rnode AN ?id ?id1)
(or (viSeRya-viSeRaNa ? ?id)(samAsa_viSeRya-samAsa_viSeRaNa ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fair.clp      fair2   "  ?id "  uciwa )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (10-12-2014)
;@@@ Added by 14anu01 on 26-06-2014
;The weather faired up .
; मौसम अच्छा बन गया .
(defrule fair3
(declare (salience 5000))
(id-root ?id fair)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)	;Added by 14anu-ban-05 on (10-12-2014)
(id-root ?id1 up) ;Added by 14anu-ban-05 on (10-12-2014)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id  acCA_bana)) ;commented by  14anu-ban-05 on (10-12-2014)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) acCA_bana_jA  ))	;added by 14anu-ban-05 on (10-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fair.clp  fair3  "  ?id "  " + ?id 1 "   acCA_bana_jA )" crlf))			;modified by 14anu-ban-05 on (10-12-2014)
)

;@@@ Added by 14anu-ban-05 on 21.08.2014
;More than 220 years of free and fair elections.
;जहाँ २२० सालो से स्वतंत्र एवं निष्पक्ष चुनाव होते रहे है 
(defrule fair4
(declare (salience 5000))
(id-root ?id fair)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 election)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niRpakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fair.clp 	fair4   "  ?id "  niRpakRa )" crlf))
)

;@@@ Added by 14anu-ban-05 on 21.08.2014
;In such a situation people think that it is not fair to experiment the inventions of other countries in India .
;ऐसी हालत में लोग सोचते हैं कि भारत में अन्य देशों के अविष्कारों का प्रयोग  उचित नहीं है
(defrule fair5
(declare (salience 5000))
(id-root ?id fair)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 it)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fair.clp 	fair5   "  ?id "  uciwa )" crlf))
)

;-------------------------- Default rules -----------------------

(defrule fair0
(declare (salience 100)) ;salience decreased from 5000  to 100 by Shirisha Manju 02-08-2016
(id-root ?id fair)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niRpakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fair.clp 	fair0   "  ?id "  niRpakRa )" crlf))
)

;default_sense && category=noun	svacCa	0
; This entry has come from Sabdasuwra, without refering to the category!
(defrule fair1
(declare (salience 4900))
(id-root ?id fair)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id melA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fair.clp 	fair1   "  ?id "  melA )" crlf))
)


;"fair","N","1.melA"
;On Sunday we went to the fair held in Paradise circle.
;
;
;LEVEL 
;Headword : fair
;
;Examples --
;
;"fair","Adj","1.gorA"
;She is a fair girl.
;vaha gorI ladakI hE.<--sAPa_raMgavAlI<--svacCa_raMga
;--"2.nyAyociwa"
;The referee was fair in the game.
;rePZarI Kela meM nyAyociwa WA <--niRpakRa <--sAPa<--svacCa
;--"3.svacCa"
;I don't find that city fair at all
;muJe vaha Sahara svacCa nahIM lagawA hE
;--"4.acCA"
;Fair weather
;acCA mOsama <----sAPa<--svacCa
; 
;
;"fair","N","1.melA"
;I bought these bangles from the village fair.
;mEMne yaha cUdZiyAz gAzva ke mele se KarIxI hEM.
; 
;vyAKyA - uparyukwa vAkya 2.Kela meM niRpakRawA ke praBAva se "sAPa" hI niRkarRa
;hE Ora anya vAkyoM meM BI `sAPa' yA 'svacCa' hI mUlArWa nikalawA hE. xUsarA
;arWa 'melA' isase asaMbanXiwa prawIwa howA hE. awaH isakA sUwra banegA -
; 
; sUwra : svacCa^niRpakRa/melA 
