
(defrule minimum0
(declare (salience 5000))
(id-root ?id minimum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_se_kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  minimum.clp 	minimum0   "  ?id "  kama_se_kama )" crlf))
)

;"minimum","Adj","1.kama_se_kama"
;We pay the minimum taxes.
;
(defrule minimum1
(declare (salience 4900))
(id-root ?id minimum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_se_kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  minimum.clp 	minimum1   "  ?id "  kama_se_kama )" crlf))
)

;"minimum","N","1.kama_se_kama"
;The minimum you can do is grant her leave.
;


;@@@ Added by 14anu-ban-08 (28-11-2014)
;Take another example: the maximum and minimum temperatures on a particular day are 35.6 °C and 24.2 °C respectively.   [NCERT]
;हम एक दूसरे उदाहरण पर विचार करेंगे: यदि किसी एक दिन का अधिकतम एवं न्यूनतम ताप क्रमशः 35.6 °C तथा 24.2 °C है.    [NCERT]
(defrule minimum2
(declare (salience 5001))
(id-root ?id minimum)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 temperature)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nyUnawama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  minimum.clp 	minimum2   "  ?id "  nyUnawama )" crlf))
)

