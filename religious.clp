;@@@ Added by 14anu-ban-10 on (03-02-2015)
;Here the famous temple of Lord Jagannath , calm sea shore , religious places of followers of Jainism , Buddhism , Shaiva and Vaishnav and several handicrafts and arts all are worth visit and charming .[tourism corpus]
;यहाँ  भगवान  जगन्नाथ  का  प्रसिद्ध  मंदिर  ,  शांत  समुद्र  तट  ,  जैन  ,  बौद्ध  ,  शैव  और  वैष्णव  मतावलंबियों  के  धर्मस्थल  और  अनेक  हस्तशिल्प  और  कलाएँ  सभी  दर्शनीय  और  मोहक  हैं  ।[tourism corpus]
(defrule religious2
(declare (salience 5200))
(id-root ?id religious)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 place)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xarma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  religious.clp 	religious2   "  ?id "  Xarma )" crlf))
)

;@@@ Added by 14anu-ban-10 on (05-02-2015)
;There are several such places in Punjab which has historical  , religious and culturual importance  .[tourism corpus]
;पंजाब में ऐसे अनेक स्थान हैं जिनका ऐतिहासिक , धार्मिक एवं सांस्कृतिक महत्त्व है ।[tourism corpus]
;The pilgrims sang a religious chant. [hinkhoj];added by 14anu-ban-10 on (11-02-2015)
;तीर्थयात्रियों ने एक धार्मिक भजन गाया . [self];added by 14anu-ban-10 on (11-02-2015)
(defrule religious3
(declare (salience 5300))
(id-root ?id religious)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 importance|chant);added chant by 14anu-ban-10 on (11-02-2015)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XArmika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  religious.clp 	religious3   "  ?id "  XArmika )" crlf))
)

;----------------------- Default Rules --------------------
(defrule religious0
(declare (salience 5000))
(id-root ?id religious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XArmika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  religious.clp 	religious0   "  ?id "  XArmika )" crlf))
)

(defrule religious1
(declare (salience 4900))
(id-root ?id religious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id majZahabI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  religious.clp 	religious1   "  ?id "  majZahabI )" crlf))
)

;"religious","Adj","1.majZahabI/XArmika"
;Temple is a religious place for Hindus.
;There is a religious house at the country side.
;--"2.XArmika_svaBAva_kA"
;She's very religious. 
;--"3.niRTApUrvaka"
;Pay religious attention to detail.
;
;
