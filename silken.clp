;@@@ Added by 14anu-ban-11 on (23-03-2015)  ;Note:- Working properly on parser no.4
;The princess in the fairy story had long silken hair. (cambridge.dic)
;परी कहानी में राजकुमारी के लम्बे रेशमी बाल थे . 
(defrule silken0
(declare (salience 00))
(id-root ?id silken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id reSamI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silken.clp 	silken0   "  ?id "  reSamI)" crlf))
)

;@@@ Added by 14anu-ban-11 on (23-03-2015) ;Note:- Working properly on parser no.2
;The actor delivered his speech in a silken voice. (cambridge.dic)
;अभिनेता ने  मधुर आवाज में अपना भाषण दिया . (self)
(defrule silken1
(declare (salience 10))
(id-root ?id silken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 voice)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maXura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silken.clp 	silken1   "  ?id "  maXura)" crlf))
)



