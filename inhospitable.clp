;@@@ Added by 14anu-ban-06 (18-03-2015)
;I'll have to cook them a meal or they'll think I'm inhospitable.(cambridge)
;मुझे उनको भोजन पकाना पडेगा कि या वे सोचेंगे कि मैं असत्कारी हूँ .(manual) 
(defrule inhospitable0
(declare (salience 0))
(id-root ?id inhospitable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asawkArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inhospitable.clp 	inhospitable0   "  ?id "  asawkArI )" crlf))
)

;@@@ Added by 14anu-ban-06 (18-03-2015)
;An inhospitable climate.(OALD)
;असुखकर जलवायु . (manual)
;They had to trek for miles through inhospitable countryside. (cambridge)
;उनको असुखकर ग्रामीण क्षेत्र में कई मील दूर के लिए लम्बी यात्रा करना पडी . (manual)
(defrule inhospitable1
(declare (salience 2000))
(id-root ?id inhospitable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 terrain|climate|countryside)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asuKakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inhospitable.clp 	inhospitable1   "  ?id "  asuKakara )" crlf))
)
