
(defrule walk0
(declare (salience 5000))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id walking )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id calawA-PirawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  walk.clp  	walk0   "  ?id "  calawA-PirawA )" crlf))
)

;"walking","Adj","1.calawA-PirawA"
;"walking","N","1.calanA"
;Walking improves health
;Added below sentences by 14anu21 on 26.06.2014 :
;I would like to have a walk in the evening. 
;मैं सन्ध्या को चाल लेना पसन्द करूँगा . (Translation before modification)
;I would like to have a walk. 
;मैं चाल लेना पसन्द करूँगा . (Translation before modification)
;I want to have a walk. 
;मैं चाल लेना चाहता हूँ . (Translation before modification)
;

;$$$ Modified by 14anu-ban-11 on (22-01-2015)
;They walk about 2 kilometers a day looking for food.(coca)
;वे आहार ढूँढता हुआ लगभग 2 दिन टहल .(anusaaraka)
(defrule walk1
(declare (salience 4900))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 about)  		;commented by 14anu-ban-11 (22-01-2015)
;(kriyA-upasarga ?id ?id1)  	;commented by 14anu-ban-11 (22-01-2015)
(kriyA-object  ?id ?id1)
;(id-cat_coarse ?id verb); commented by 14anu-ban-11 (22-01-2015)
(id-cat_coarse ?id noun); Added by 14anu-ban-11 (22-01-2015)
(id-root ?id1 kilometer); Added by 14anu-ban-11 (22-01-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 tahala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " walk.clp	walk1  "  ?id "  " ?id1 "  tahala  )" crlf))
)



(defrule walk2
(declare (salience 4700))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 curA_le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " walk.clp	walk2  "  ?id "  " ?id1 "  curA_le_jA  )" crlf))
)



(defrule walk3
(declare (salience 4500))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id walking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id calanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  walk.clp  	walk3   "  ?id "  calanA )" crlf))
)

;@@@ Added by 14anu21 on 26.06.2014
;The baby is learning to walk.
;शिशु चलने के लिए सीख रहा है . (Translation before adding the rule)
;Then added rule "to39_walk" to remove "के लिए"
;शिशु चलना सीख रहा है . (Translation after adding rule "to39_walk") 
;
(defrule walk10
(declare (salience 4500))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(to-infinitive  ?idto ?id)
(kriyA-kriyArWa_kriyA  ?idverb ?id)
(id-root ?idverb learn)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  walk.clp  	walk10   "  ?id "  cala )" crlf))
)

;Added by Meena(26.6.10)
;She has gone for a walk in the woods . 
;$$$ Modified by 14anu21 on 26.06.2014 by adding relation (viSeRya-for_saMbanXI  ?id1 ?id) and "come" to the list (id-root ?id1 go|come)
;Would you like to come for a walk?
;क्या आप चाल के लिए आना पसन्द करेंगे? (Translation before modification)
;क्या आप सैर के लिए आना पसन्द करेंगे? 
;She has taken the dog for a walk.
;उसने चाल के लिए कुत्ते को लिया है . (Translation before modification)
;उसने सैर के लिए कुत्ते को लिया है . 
(defrule walk4
(declare (salience 4400))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(or(and(id-root ?id1 go|come)(kriyA-for_saMbanXI  ?id1 ?id))(viSeRya-for_saMbanXI  ?id1 ?id));Added viSeRya-for_saMbanXI  by 14anu21
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  walk.clp      walk4   "  ?id "  sEra )" crlf))
)

;@@@ Added by 14anu21 on 26.06.2014 
;I would like to have a walk in the evening. 
;मैं सन्ध्या को चाल लेना पसन्द करूँगा . (Translation before adding the rule)
;मैं सन्ध्या को सैर लेना पसन्द करूँगा .(Modified rule have6)
;मैं सन्ध्या को सैर करना पसन्द करूँगा .
;I want to have a walk. 
;मैं चाल लेना चाहता हूँ . (Translation before modification)
;मैं सैर लेना चाहता हूँ . 
(defrule walk11
(declare (salience 4400))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-kriyArWa_kriyA  ?id1 ?id2)
(kriyA-object  ?id2 ?id)
(not(viSeRya-like_saMbanXI  ?id ?id3))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sEra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  walk.clp      walk11 "  ?id "  sEra )" crlf))
)


(defrule walk6
(declare (salience 4300))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) out )
(id-word =(+ ?id 2) of)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calA_jA))
(assert (kriyA_id-object_viBakwi ?id se))
;(assert (id-H_vib_mng ?id ed))
(assert (id-H_vib_mng ?id yA));Suggested by Sukhada(20-05-13)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  walk.clp 	walk6   "  ?id "  calA_jA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  walk.clp      walk6   "  ?id " se )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* "  walk.clp      walk6   "  ?id " ed )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* "  walk.clp      walk6   "  ?id " yA )" crlf))
)


(defrule walk8
(declare (salience 4200))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) on)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) calawA_jA))
(if ?*debug_flag* then
 (printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  walk.clp    walk8   "  ?id " "(+ ?id 1) " calawA_jA )" crlf)
)
)

;Added by Roja (20-05-13) Suggested by Chaitanya Sir.
;Removed walked.clp and merged walked.clp rule here.
(defrule walk9
(declare (salience 4350))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 anxara_AyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " walk.clp        walk9  "  ?id "  " ?id1 "  anxara_AyA  )" crlf))
)

;@@@ Added by Shirisha Manju (02-05-14) Suggested by Chaitanya Sir.
;His eyes lit up when she walked into the room.  OALD
;usakI AzKeM KuSI_se_camaka uTIM jaba vaha kamare ke anxara gaI.
(defrule walk_into
(declare (salience 4350))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-into_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " walk.clp        walk_into  "  ?id "    jA  )" crlf))
)


;@@@ Added by 14anu-ban-11 on (11-03-2015)
;She just tossed her head and walked off. (oald)
;उसने अपना सिर जरा  सा हिलाया और चली गयी. (manual)
(defrule walk12
(declare (salience 4800))
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 off)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 calI_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " walk.clp	walk12  "  ?id "  " ?id1 "  calI_jA  )" crlf))
)

;------------------------ Default rules -----------------------------
;"walk","V","1.calanA"
;They walked for two kilometers yesterday.
(defrule walk7
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  walk.clp     walk7   "  ?id "  cala )" crlf))
)

;"walk","N","1.cAla"
;His walk is not straight
(defrule walk5
(id-root ?id walk)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  walk.clp     walk5   "  ?id "  cAla )" crlf))
)


; Removed salience for default rules by Shirisha Manju 2/5/14
