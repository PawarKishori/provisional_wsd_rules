;@@@ Added by 14anu-ban-08 on (10-09-2014)
;Indian sufi saints like Moinuddin Chisti , Baba Farid and Nizzamuddin were part of this linkage.  [karan singla]
;मोइनुदीन चिश्ती बाबा फरीद निज़ामुदीन जैसे भारतिय सूफी संत इसी कड़ी का हिस्सा थे ।
(defrule linkage0
(declare (salience 0))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadIZ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage0   "  ?id "  kadIZ )" crlf))
)

;@@@ Added by 14anu-ban-08 on (10-09-2014)
;Even a good linkage of the Semi - conductor Complex Ltd.SCL with consumers in the country has not been achieved .  [karan singla]
;यहां तक कि देश में उपभोक़्ताओं का सेमीकंडक़्टर कॉम़्प़्लेक़्स लि . एस . सी . एल . से सीधा सम़्बऩ्ध नहीं जुड़ सका है .
(defrule linkage1
(declare (salience 100))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-word ?id1 good)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambaMXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage1   "  ?id "  sambaMXa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (10-09-2014)
;Unfortunately , such a linkage between phenotype and genotype is riddled with many difficulties in the case of man .  [karan singla]
;परंतु दुर्भाग़्यवश मनुष़्य के आनुवंशिकता तथा बाह्म रूपों में इस प्रकार का संबंध स़्थापित करने में बहुत समस़्याएं उत़्पऩ्न होती हैं .
(defrule linkage2
(declare (salience 102))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-between_saMbanXI ?id ?id1)
(id-word ?id1 genotype|phenotype)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sambaMXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage2   "  ?id "  sambaMXa )" crlf))
)

;@@@ Added by 14anu-ban-08 on (10-09-2014)
;An important offshoot of linkage theory is that the degree or strength of linkage depends upon the distance between the linked genes in the chromosome .  [karan singla]
;सहलग़्नता सिद्धांत से प्राप़्त एक महत़्वपवूर्ण तथ़्य यह है कि सहलग़्नता की कोटि अथवा दृढ़ता गुणसूत्र में उपस़्थित जीनों के बीच के अंतर पर निर्भर करती है .
;This is the most usual type and is the one always intended when sex - linkage is mentioned .    [karan singla]
;ये एक अत्यंत साधारण किस़्म के जीन हैं तथा जब भी कभी लिंग सहलग़्न जीनों पर विचार किया जाता है , तब इऩ्हीं जीनों की ओर संकेत किया जा सकता है .
(defrule linkage3
(declare (salience 103))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 differ|theory|sex)
(id-cat_coarse ?id noun|adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahalagZnawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage3   "  ?id "  sahalagZnawA )" crlf))
)

;@@@ Added by 14anu-ban-08 on (10-09-2014)
;Consequently partial sex - linkage differs from total sex - linkage in two ways .  [karan singla]
;अंत : आंशिक तथा संपूर्ण लिंग सहलग़्नता में दो प्रमुख भिऩ्नताएं होती है .
(defrule linkage4
(declare (salience 104))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-word ?id1 sex)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahalagZnawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage4   "  ?id "  sahalagZnawA )" crlf))
)

;@@@ Added by 14anu-ban-08 on (10-09-2014)
;For only homologous genes located in this portion can possibly cross over from one to another and thus permit detection and measurement of linkage between them .   [karan singla]
;इस खंड के समजात जीनों में ही अऩ्योऩ्य गमन संभव है तथा इन्हीं के कारण इन जीनों के बीच की सहलग़्नता को मापन संभव होता है .
;An important offshoot of linkage theory is that the degree or strength of linkage depends upon the distance between the linked genes in the chromosome .  [karan singla]
;सहलग़्नता सिद्धांत से प्राप़्त एक महत़्वपवूर्ण तथ़्य यह है कि सहलग़्नता की कोटि अथवा दृढ़ता गुणसूत्र में उपस़्थित जीनों के बीच के अंतर पर निर्भर करती है .
(defrule linkage5
(declare (salience 105))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-word ?id1 detection|degree)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahalagZnawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage5   "  ?id "  sahalagZnawA )" crlf))
)

;@@@ Added By 14anu-ban-08 on (10-09-2014)
;This is only a stray illustration of the vast amount of work that has been done on linkage in a number of organisms , notably banana fly and maize .  [karan singla]
;केला मुक़्खी और मक़्का के पौधों पर जीनों की सहलग़्नता के संदर्भ में जो विस़्तृत अनुसंधान कार्य किया गया है उसका यह एक बहुत छोटा उदाहरण है .
(defrule linkage6
(declare (salience 106))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 organism)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahalagZnawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage6   "  ?id "  sahalagZnawA )" crlf))
)

;@@@ Added by 14anu-ban-08 on (10-09-2014)
;Nevertheless , the linkage between genes located on the same chromosome is not absolute .  [karan singla]
;लेकिन एक ही गुणसूत्र में उपस़्थित जीवों में पायी जाने वाली सहलग़्नता कोई परम स़्थिति नहीं है .
(defrule linkage7
(declare (salience 107))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-between_saMbanXI ?id ?id1)
(id-root ?id1 gene)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahalagZnawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage7   "  ?id "  sahalagZnawA )" crlf))
)

;@@@ added by 14anu23 on 04/07/14
;There is no proof of linkage to a terrorist organization.
;एक आतङ्कवादी सङ्गठन से जुडे होने की स्थिति का प्रमाण नहीं है .
(defrule linkage8
(declare (salience 0))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id judZe_hone_kI_sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage8   "  ?id "  judZe_hone_kI_sWiwi)" crlf))
)
;@@@ added by 14anu23  on 04/07/14
;A complex linkage of nerves.
;नसों का एक जटिल लिंक का सिस्टम . 
(defrule linkage9
(declare (salience 500))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-word ?id1 complex|mechanical|dense)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id liMka_kA_sistama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage9   "  ?id " liMka_kA_sistama )" crlf))
)
;@@@ added by 14anu23  on 04/07/14
;This chapter explores the linkage between economic development and the environment.
;यह अध्याय आर्थिक विकास के बीच और पर्यावरण के बीच की कडी का अन्वेषण करता है . 
(defrule linkage10
(declare (salience 1000))
(id-root ?id linkage)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-between_saMbanXI   ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kadI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  linkage.clp 	linkage10   "  ?id "  kadI)" crlf))
)

