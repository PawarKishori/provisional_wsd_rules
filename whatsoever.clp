;@@@ Added by 14anu22
;she remembered whatsover i did for her
;उसने याद किया जो कुछ भी मैने उसके लिये किया
(defrule whatsoever0
(declare (salience 5000))
(id-root ?id whatsoever)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo_kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " whatsoever.clp	whatsoever0  "  ?id " jo_kuCa  )" crlf))
)

;@@@ Added by 14anu22
;he said so whatsoever
;उसने ऐसे ही कुछ भी बोल दिया
(defrule whatsoever2
(declare (salience 5000))
(id-root ?id whatsoever)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " whatsoever.clp	whatsoever2  "  ?id " kuCa_BI  )" crlf))
)

;@@@ Added by 14anu22
;she remembered whatsover i did for her
;उसने याद किया जो कुछ भी मैने उसके लिये किया
(defrule whatsoever3
(declare (salience 5000))
(id-root ?id whatsoever)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo_kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " whatsoever.clp	whatsoever3  "  ?id " jo_kuCa  )" crlf))
)
