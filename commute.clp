;@@@ Added by 14anu-ban-03 (23-03-2015)
;The death sentence was commuted to life imprisonment. [oald]
;प्राणदण्ड की सजा को आजीवन कारावास मे कम कर दिया गया था . [manual]
(defrule commute1
(declare (salience 10))
(id-root ?id commute)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 imprisonment)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " commute.clp 	commute1  "  ?id "   kama_kara_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (23-03-2015)
;People used to believe that you could commute base metals into gold. [cald]
;लोग मानते थे कि आप मूल धातु को सोने मे बदल सकते थे. [manual]
(defrule commute2
(declare (salience 20))
(id-root ?id commute)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 metal)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " commute.clp 	commute2  "  ?id "   baxala_xe )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (23-03-2015)
;We commute from Indore to Dewas daily. [hinkhoj]
;हम प्रतिदिन इंदौर से देवास तक यात्रा करते हैं . [manual]
(defrule commute0
(declare (salience 00))
(id-root ?id commute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAwrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " commute.clp   commute0  "  ?id "   yAwrA_kara )" crlf))
)

