;@@@ Added by (14anu06),Vivek Agarwal, BTech in CSE, MNNIT Allhabad, on 7 June 2014 ( vivek17.agarwal@gmail.com )
;Gave them credit for a job well done.
;उनको एक काम अच्छी तरह किया हुआ प्रशंसा दीजिए . 

(defrule credit2
(declare (salience 5000))
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI ?id ?id1)
(id-word ?id1 job|work|activity|assignment|this|that)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praSaMsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit2   "  ?id "  praSaMsA )" crlf))
)

;@@@ Added by (14anu06) Vivek Agarwal, BTech in CSE, MNNIT Allhabad, on 7 June 2014  ( vivek17.agarwal@gmail.com )
; He was given credit for a work they did.
;उसको एक ऐसे कार्य के लिए प्रशंसा दी गयी थी जहाँ उन्होंने किया . 

(defrule credit3
(declare (salience 5500))
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-for_saMbanXI ?id1 ?id2)
(id-root ?id1 give|accord)
(id-word ?id2 job|work|activity|assignment|this|that)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praSaMsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit3   "  ?id "  praSaMsA )" crlf))
)

;@@@ Added by (14anu06) Vivek Agarwal, BTech in CSE, MNNIT Allhabad, on 7 June 2014 (vivek17.agarwal@gmail.com )
; At the end of the film we stayed to watch the credits.
;सिनेमा के अन्त में हम क्रेडिट्स को देखने के लिए रुके रहे . 

(defrule credit4
(declare (salience 5500))
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word ?id credits)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kreditsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp   credit4   "  ?id "  kreditsa )" crlf))
)


;@@@ Added by 14anu-ban-06 (30-07-2014)
;The credit of laying the foundation of modern Manipuri Travelogue Literature goes to a travel article of Hijam Iravat published in 1937 AD  .   (Parallel Corpus)
;आधुनिक मणिपुरी यात्रा साहित्य की नींव रखने का श्रेय जननेता हिजम इरावत के सन् 1937 में छपे एक यात्रा-लेख को है ।
(defrule credit5
(declare (salience 5100))
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sreya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit5   "  ?id "  Sreya )" crlf))
)

;@@@ added by 14anu11
;IF WE HAVE SPOKEN of the socio - religious revolution of the twelfth century as Basava ' s revolution , it is only to emphasise the
; fact that he was its ' acknowledged leader and that the majdr portion of credit for its success should go to him .
;था और इसकी सफलता का अधिकांश श्रेय उसी को जाना चाहिए .
(defrule credit6
(declare (salience 4050)) 
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sreya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit6   "  ?id "  Sreya )" crlf))
)


;@@@ Added by 14anu-ban-03 (05-12-2014)
;For the authorised travels of tourist groups Indian Railways have taken a step to fix fares as per their credit ability .[tourism]
;भारतीय रेल ने पर्यटक समूहों की अधिकृत यात्राओं के लिए उनकी क्रयक्षमता के अनुरूप किरायों को तय करने के लिए कदम उठाया है .[tourism]
(defrule credit7
(declare (salience 5050))
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 ability)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kraya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit7   "  ?id "  kraya )" crlf))
)

;@@@ Added by 14anu-ban-03 (09-02-2015)
;The Cheetah is generally credited as the world's fastest animal. [same clp file]
;आमतौर पर चीता विश्व के सबसे तेज दौडने वाले पशु के रूप में माना जाता है . [manual]
(defrule credit8
(declare (salience 4900))
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnA_jA))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit8   "  ?id "  mAnA_jA )" crlf))
)

;@@@ Added by 14anu-ban-03 (09-02-2015)
;Credit his account with Rs.100.  [same clp file]
;100 रूपए  उसके खाते में जमा कीजिए . [manual]
(defrule credit9
(declare (salience 4900))
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 account)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jamA_kara))
(assert (id-H_vib_mng ?id1 meM))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  credit.clp 	credit9  "  ?id1 "  meM )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit9   "  ?id "  jamA_kara )" crlf))
)



;------------------------- Default rules ---------------------
(defrule credit0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (15-12-2014)
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uXAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit0   "  ?id "  uXAra )" crlf))
)

;$$$ Modified by 14anu-ban-03 (09-02-2015)
;The technological exploitation of this property is generally credited to the Chinese. [ncert]
;isa guNa ke wakanIkI upayoga kA Sreya AmawOra para cIniyoM ko xiyA jAwA hE. [ncert]
;"credit","VT","1.Sreya_xenA"
;We credited her for saving our jobs
;I credit you with saving his life
(defrule credit1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (09-02-2015)
(id-root ?id credit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sreya_xe))
(assert (kriyA_id-subject_viBakwi ?id kA)) ;added by 14anu-ban-03 (09-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  credit.clp 	credit1   "  ?id " kA  )" crlf)          ;added by 14anu-ban-03 (09-02-2015) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  credit.clp 	credit1   "  ?id "  Sreya_xe )" crlf))
)

;"credit","VT","1.Sreya_xenA"
;We credited her for saving our jobs
;I credit you with saving his life
;--"2.viSvAsa_xilAnA"
;The Cheetah is generally credited as the world's fastest animal. 
;--"3.mAnyawA"  
;Would you credit it,if she won first prize?
;--"4.kisI_KAwe_meM_jamA_karanA"  
;Credit his account with Rs.100.  
;
;LEVEL 
;
;
;
;Headword : credit
;
;Examples --
;
;'credit' Sabxa kA sUwra banAne kA eka prayAsa
;
;--"1.viSvAsa"
;I bought it on credit.
;meM use viSvAsa para KarIxa lAI.
;--"2.prawiRTA"
;I hope he will be a credit to our institute.
;muJe ummIxa hE,ki ye hamArI saMsWA kI prawiRTA ko baDAegA.
;--"3.Sreya"
;This credit goes to rahul.
;yaha Sreya rAhula ko jAwA hEM.
;--"4.jamAXana"
;please place this sum to my credit.
;kqpayA mere jamAXana meM ye rakama dAla xIjie
;--"5.uXAra"
;The bank refused further credit to the company.
;bEMka ne kampanI ko uXAra xene se manA kara xiyA.
;--"6.sAKa"
;The company approved the credit.
;kampanI ne sAKa kI maMjUrI xe xI.
;
;yahAz hama xeKawe hE ki "credit" Sabxa kA prayoga alaga alaga saMrxaBo meM howe
;hue BI unakA eka sAmAnya arWa nikalawA hEM.yahAz para xie gae uxAharaNo kI ora hama
;XyAna se xeKe wo uname eka sAmAnyawA xiKawI hEM.
;
;isakA sUkRma arWa kuCa isa prakAra hE yaxi kisI para viSvAsa ho wo hI use gOrava
;prApwa hogA.waba hI use sAKa yA uXAra/jamA xiyA jAegA.jo ki eka krama me kuCa
;isa prakAra se judZA hE.
;
;(viSvAsa)-sAkRya-[sAKa]-gOrava-[Sreya]{yogaxAna}
;|                                     |
;|                                     | 
;[uXAra]/[jamA]---------------------------
;
;isa DAzce se yaha niRkarRa nikalawA hE ki isakA 'mUla' arWa uXAra yA 'sAKa' 
;A rahA hE. isalie isakA sUwra hogA, 
;
;sUwra : sAKa[>uXAra/jamA]
;
;
