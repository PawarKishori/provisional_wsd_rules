
;@@@ Added by 14anu-ban-05 on (18-04-2015)
;His father had always taught him to be graceful in defeat. 	[OALD]
;उसके पिता ने हमेशा उसको हार में विनम्र रहने के लिए सिखाया था . 			[MANUAL]
(defrule graceful1
(declare (salience 101))
(id-root ?id graceful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-kriyArWa_kriyA  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinamra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  graceful.clp 	graceful1   "  ?id "  vinamra )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-04-2015)
;At the age of 70, she withdrew into graceful retirement.	[OALD]	
;70 की उम्र में, उसने  विनीत भाव से  सेवा-निवृत्ति ले लिया . 			[MANUAL]
(defrule graceful2
(declare (salience 102))
(id-root ?id graceful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 retirement)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinIwa_BAva_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  graceful.clp 	graceful2   "  ?id "  vinIwa_BAva_se )" crlf))
)

;-------------------------------------- Default Rules ------------------------------

;@@@ Added by 14anu-ban-05 on (18-04-2015)
;The dancers were all tall and graceful. [OALD]
;सभी नर्तकियाँ  लंबी और सुंदर थी.         [MANUAL]
(defrule graceful0
(declare (salience 100))
(id-root ?id graceful)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suMxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  graceful.clp         graceful0   "  ?id "  suMxara )" crlf))
)


