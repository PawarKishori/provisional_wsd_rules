;--------------------------------DEFAULT MEANING--------------------------------------------
;@@@ Added by 14anu-ban-09 on (05-09-2014)
;It is impossible to predict what will happen. [OALD]
;bawalAnA ki kyA hone vAlA hE asamBava hE. [Own Manual]

(defrule predict0
(id-root ?id predict)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaviRya_bawalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  predict.clp 	predict0   "  ?id " BaviRya_bawalA )" crlf))
)

;----------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (05-09-2014)
;Nobody could predict the outcome. [OALD]
;koI BI pariRAma kA pUrvAnumAna  nahIM lagA sakawA. [Own Manual]

(defrule predict1
(declare (salience 4900))
(id-root ?id predict)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 outcome)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvAnumAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  predict.clp 	predict1   "  ?id "  pUrvAnumAna )" crlf))
)


;@@@ Added by 14anu-ban-09 on (05-09-2014)
;However, measurements on real gases deviate from the values predicted by the ideal gas law at low temperature. [NCERT CORPUS]
;दाब डिग्री के समान है, अतः इन मापक्रमों के तापों में संबंध इस प्रकार है: दाब गैस ताप तथापि, निम्न ताप पर वास्तविक गैसों पर ली गई मापों तथा आदर्श गैस नियम द्वारा प्रागुक्त मानों में अंतर पाया गया है.
;दाब डिग्री के समान है, अतः इन मापक्रमों के तापों में संबंध इस प्रकार है: दाब गैस ताप तथापि, निम्न ताप पर वास्तविक गैसों पर ली गई मापों तथा आदर्श गैस नियम द्वारा अनुमानित मानों में अंतर पाया गया है. [Own Manual]

(defrule predict2
(declare (salience 4900))
(id-root ?id predict)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 value)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAniwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  predict.clp 	predict2   "  ?id "  anumAniwa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (06-09-2014)
;Science is a systematic attempt to understand natural phenomena in as much detail and depth as possible, and use the knowledge so gained to predict, modify and control phenomena.  [NCERT CORPUS]
;विज्ञान प्राकृतिक परिघटनाओं को यथासम्भव विस्तृत एवं गहनता से समझने के लिए किए जाने वाला सुव्यवस्थित प्रयास है, जिसमें इस प्रकार अर्जित ज्ञान का उपयोग परिघटनाओं के भविष्य कथन, संशोधन, एवं नियन्त्रण के लिए किया जाता है.

(defrule predict3
(declare (salience 4900))
(id-root ?id predict)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 phenomenon) ;more constraints can be added
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaviRya_kaWana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  predict.clp 	predict3   "  ?id "  BaviRya_kaWana )" crlf))
)

;@@@ Added by 14anu-ban-09 on (06-09-2014)
;Science is exploring, experimenting and predicting from what we see around us.  [NCERT CORPUS]
;जो कुछ भी हम अपने चारों ओर देखते हैं उसी के आधार पर अन्वेषण करना, प्रयोग करना तथा भविष्यवाणी करना विज्ञान है.

(defrule predict4
(declare (salience 5000))
(id-root ?id predict)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI  ?id ?id1)
(id-root ?id1 see) ;more constraints can be added
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaviRyavANI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  predict.clp 	predict4   "  ?id "  BaviRyavANI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-10-2014)
;For example, we may not know the complicated forces that act during a collision of two automobiles; yet momentum conservation law enables us to bypass the complications and predict or rule out possible outcomes of the collision.  [NCERT CORPUS]
;uxAharaNa ke lie, xo svacAliwa vAhanoM kI takkaroM kI avaXi meM lagane vAle jatila baloM kA hameM jFAna nahIM howA; Pira BI saMvega saMrakRaNa niyama hameM isa yogya banAwA hE ki hama jatilawAoM se bAhara nikala kara, takkara ke samBAviwa pariNAmoM kA anumAna lagAez aWavA unheM niyama viruxXa GoRiwa kareM. [NCERT CORPUS]

(defrule predict5
(declare (salience 5000))
(id-root ?id predict)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id3 ?id1)
(kriyA-subject  ?id ?id2)
(kriyA-subject  ?id3 ?id2)
(id-root ?id1 outcome) ;more constraints can be added
(id-root ?id2 law) ;more constraints can be added
(id-root ?id3 rule) ;more constraints can be added
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  predict.clp 	predict5   "  ?id "  anumAna_lagA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-11-2014)
;The corpuscular model predicted that if the ray of light (on refraction) bends towards the normal then the speed of light would be greater in the second medium. [NCERT CORPUS]
;kaNikA moYdala ne prAgukwa kiyA ki yaxi prakASa kI kiraNa (apavarwana ke samaya) aBilamba kI ora mudawI hE, waba xUsare mAXyama meM prakASa kI cAla aXika hogI. [NCERT CORPUS]

(defrule predict6
(id-root ?id predict)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 model)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAgukwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  predict.clp 	predict6   "  ?id " prAgukwa_kara )" crlf))
)
