;$$$ Modified by 14anu-ban-07 Soshya Joshi Banasthali University , 23-07-2014
;"grab","VT","1.pakadZanA"
;The police grabbed the thief.
(defrule grab1
(declare (salience 5100)) 
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object  ?id ?)) ;added by 14anu-ban-07 23-7-14 
(kriyA-subject  ?id ?id1)   ;added by 14anu-ban-07 23-7-14
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))  ;added by 14anu-ban-07 23-7-14
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grab.clp 	grab1   "  ?id "  pakadZa )" crlf))
)


;@@@ Added by 14anu-ban-07 Soshya Joshi Banasthali University  17-07-2014
;KIMca:
;Idols made in aritistic workmanship grab the attention of common people towards themselves but it lacks proper care.(parallel corpus)
;कलापूर्ण  कारीगरी  में  जो  मूर्तियाँ  बनी  हैं  ,  वे  आम  लोगों  का  ध्यान  अपनी  ओर  खींचती  हैं  ,  पर  समुचित  देखभाल  का  अभाव  है  ।
;The play grabs the audience's attention from the very start. (oald)
;nAtaka Suru se hI xarSakoM ka XyAna KIMca lewA hE.
(defrule grab2
(declare (salience 5300))
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 attention)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KIMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grab.clp 	grab2   "  ?id "  KIMca )" crlf))
)


;@@@ Added by 14anu-ban-07 Soshya Joshi Banasthali University 18-07-2014
;surKiyoM_meM:
;Glasgow's drugs problem has grabbed the headlines tonight.(oald)
;glAsago kI XAvAoM vAlI samasyA Aja rAta ke surKiyoM meM He.
;Tech stocks have grabbed the headlines.(COCA)
;teka SeyareM surKiyoM meM He.

(defrule grab3
(declare (salience 5500))
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) the)
(id-root =(+ ?id 2) headline)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2) surKiyoM_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grab.clp	 grab3  "  ?id "  " (+ ?id 1) (+ ?id 2) " surKiyoM_meM  )" crlf))
)


;@@@ Added by 14anu-ban-07 Soshya Joshi Banasthali University 18-07-2014
;KAnA:
;Let's grab a sandwich before we go.(oald)
;jAne se pahale calo eka sEMdavica KAlewe hE.

(defrule grab4
(declare (salience 5600))
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 food|fruit|sandwich)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grab.clp 	grab4   "  ?id "  KA )" crlf))
)

;@@@ Added by 14anu-ban-07 Soshya Joshi Banasthali University 18-07-2014
;uTAnA:
;Jim grabbed a cake from the plate.(oald)
;jima ne WAlI se eka keka uTAyA.
(defrule grab5
(declare (salience 5700))
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grab.clp 	grab5   "  ?id "  uTA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (19-02-2015)
;The dream of Peshwa Baji Rao of grabbing Delhi did not fulfill .[tourism]
;peSavA bAjIrAva kA xillI haWiyAne kA sapanA wo pUrA nahIM huA .[tourism]

(defrule grab6
(declare (salience 5000))
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haWiyAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grab.clp 	grab6   "  ?id "  haWiyAnA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (19-02-2015)
;Deoraj Indra feared whether Mahabali would grab his throne .[tourism]
;xevarAja iMxra ko lagA ki kahIM mahAbalI unakA siMhAsan hI haWiyA le. [tourism] 
(defrule grab7
(declare (salience 5701))
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 throne|property)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haWiyA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grab.clp 	grab7   "  ?id "  haWiyA_le )" crlf))
)

;@@@ Added by 14anu-ban-05 on (19-02-2015)
;I extended my left hand to grab onto the iron railing.[COCA]
;मैंने  लोहे की रेलिंग को पकड़ने के लिए अपना बाएँ हाथ बढ़ाया.	[manual]

(defrule grab8
(declare (salience 5702))
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) onto)
(kriyA-onto_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert  (id-wsd_viBakwi   ?id1  ko))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " grab.clp  grab8  "  ?id "  " (+ ?id 1) " pakadZa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  grab.clp  grab8  "  ?id1 " ko )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-04-2015)
;I grabbed him, but he slipped from my grasp. [OALD]
;मैंने उसे पकड़ लिया, लेकिन वह मेरी मुट्ठी से फिसल गया. [MANUAL]
(defrule grab9
(declare (salience 5010)) 
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?) 
(kriyA-subject  ?id ?id1)   
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakadZa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grab.clp 	grab9   "  ?id "  pakadZa_le )" crlf))
)

;------------------------ Default Rules ----------------------

;"grab","V","1.Japata"
(defrule grab0
(declare (salience 5000))
(id-root ?id grab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Japata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grab.clp 	grab0   "  ?id "  Japata )" crlf))
)

