
(defrule roll0
(declare (salience 5000))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-down_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dulaka));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " roll.clp roll0 " ?id "  Dulaka )" crlf)) 
)

(defrule roll1
(declare (salience 4900))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Dulaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " roll.clp	roll1  "  ?id "  " ?id1 "  Dulaka  )" crlf))
)

(defrule roll2
(declare (salience 4800))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-over_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id luDZaka));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " roll.clp roll2 " ?id "  luDZaka )" crlf)) 
)

(defrule roll3
(declare (salience 4700))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 luDZaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " roll.clp	roll3  "  ?id "  " ?id1 "  luDZaka  )" crlf))
)

;$$$ Modified by 14anu13 on 27-06-14 [added condition]
;If he wishes , the earth rolls itself up for him , if he wishes , he can walk on the water and in the air.
;अगर वह चाहे तो जमीन उसके लिए घूमती है , यदि वह चाहे तो पानी पर और हवा में चल सकता है |
(defrule roll6
(declare (salience 4400))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1) ; added by 14anu13 
(id-root ?id1 earth|moon|boll|planet) ; added by 14anu13 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUmA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roll.clp 	roll6   "  ?id "  GUmA )" crlf))
)

;"roll","VTI","1.GUmA"
;Keep the cameras rolling.
;--"2.luDakanA"
;She rolled over to her back.                
;The ball rolled down the hill.
;The marble fell && rolled away.                                       
;--"3.lapetanA"
;He always roll his own cigarettes.                                   
;Roll the cheese in mashed potato.                                    
;--"4.GumAnA"
;The high tides rolled the boat from side to side.                 
;--"5.XIme_calanA"
;The traffic rolled slowly forward.
;--"6.belanA"
;Roll out the pizza dough.                  
;

;@@@ Added by Anita -23-06-2013
;A body like a ring or a sphere rolling without slipping over a horizontal plane will suffer no friction in principle. [ncert]
;सिद्धान्त रूप से क्षैतिज समतल पर एक छल्ले के समान वस्तु अथवा गोल गेन्द जैसे पिण्ड जो बिना सरके केवल लुढक रहा  है, पर किसी भी प्रकार का कोई घर्षण बल ;नहीं लगेगा  ।
(defrule roll07
(declare (salience 4500))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(kriyA-without_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id luDZaka_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roll.clp 	roll07   "  ?id "  luDZaka_raha )" crlf))
)


;@@@ Added by 14anu-ban-10 on (23-09-2014)
;You can stop a ball rolling down an inclined plane by applying a force against the direction of its motion. [ncert] 
;kisI Anawa wala para nIce kI ora luDakawI kisI genxa ko usakI gawi kI viparIwa xiSA meM bala lagAkara rokA jA sakawA hE. [ncert]
;किसी आनत तल पर नीचे की ओर लुढकती किसी गेन्द को उसकी गति की विपरीत दिशा में बल लगाकर रोका जा सकता है.
(defrule roll7
(declare (salience 5000))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1 ) ;removed (kriyA_upasarga ?id ?id2) by 14anu-ban-10 on (11-11-2014)
(id-root ?id1 plane)
;(id-root ?id2 down) ;commented out condition by 14anu-ban-10 on (11-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id luDakawI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roll.clp 	roll7   "  ?id "  luDakawI )" crlf))
)

;@@@ Added by 14anu-ban-10 on (23-09-2014)
;We know, in practice, this will not happen and some resistance to motion (rolling friction) does occur, i.e. to keep the body rolling, some applied force is needed.  
;hama jAnawe hEM ki vyavahAra meM EsA nahIM hogA, waWA gawi meM kuCa na kuCa avaroXa (lotanika GarRaNa) avaSya rahawA hE, arWAw, piNda ko niranwara lotanika gawi karawe rahane ke lie usa para kuCa bala lagAne kI AvaSyakawA howI hE.
;हम जानते हैं कि व्यवहार में ऐसा नहीं होगा, तथा गति में कुछ न कुछ अवरोध (लोटनिक घर्षण) अवश्य रहता है, अर्थात्, पिण्ड को निरन्तर लोटनिक गति करते रहने के लिए उस पर कुछ बल लगाने की आवश्यकता होती है.
(defrule roll8
(declare (salience 5200))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lotanika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roll.clp 	roll8   "  ?id "  lotanika)" crlf))
)

;@@@ Added by 14anu-ban-10 on (15-12-2014)
;We know, in practice, this will not happen and some resistance to motion (rolling friction) does occur, i.e. to keep the body rolling, some applied force is needed.  [ncert]
;hama jAnawe hEM ki vyavahAra meM EsA nahIM hogA, waWA gawi meM kuCa na kuCa avaroXa (lotanika GarRaNa) avaSya rahawA hE, arWAw, piNda ko niranwara lotanika gawi karawe rahane ke lie usa para kuCa bala lagAne kI AvaSyakawA howI hE. [ncert]
;हम जानते हैं कि व्यवहार में ऐसा नहीं होगा, तथा गति में कुछ न कुछ अवरोध (लोटनिक घर्षण) अवश्य रहता है, अर्थात्, पिण्ड को निरन्तर लोटनिक गति करते रहने के लिए उस पर कुछ बल लगाने की आवश्यकता होती है. [ncert]
(defrule roll9
(declare (salience 5300))
(id-root ?id rolling)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ? )
(id-cat_coarse ?id noun )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lotanika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roll.clp 	roll9   "  ?id "  lotanika)" crlf))
)

;@@@ Added by 14anu-ban-10 on (07-02-2015)
;I rolled down the jeep's window pane .[tourism corpus]
;जीप की खिड़की के शीशे नीचे कर लिए मैंने ।[tourism corpus]
(defrule roll10
(declare (salience 5400))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 down)
(id-cat_coarse ?id verb )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIce_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roll.clp 	roll10   "  ?id "  nIce_kara)" crlf))
)

;--------------------Default rules ---------------------------

(defrule roll4
(declare (salience 4600))
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id golA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roll.clp 	roll4   "  ?id "  golA )" crlf))
)

;"roll","N","1.golA"
;He has wasted a roll of film by exposing it.
;book wrapper is sold in rolls.                                  
;--"2.sUcI"
;They have not included her parents name in electoral roll.  
;--"3.gadZagadZAhata"
;There was a loud drum roll from the restaurant.
;
(defrule roll5
(declare (salience 4000)) ;salience reduced by 14anu-ban-10 on (15-12-2014)
(id-root ?id roll)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id luDZaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roll.clp 	roll5   "  ?id "  luDZaka )" crlf))
)

;"roll","V","1.luDZakanA"


;LEVEL 
;
;
;Headword : roll
;
;Examples --
;
;--1.lipatA_huA <--gola-gola GUmA huA
;A roll of carpet.
;lipatA huA kArapeta
;
;--2.lapetanA <--gola-gola GumAnA
;Roll those papers in a right manner.
;una kAgajZAwoM ko sahI DaMga se lapeto/modZo.
;
;--3.gole
;A man with rolls of fat around his stomach.
;carabI ke gole jisake cAroM ora lataka rahe hoM EsA AxamI. <--lipatI huI carabI
;<--gola GUmI 
;
;--4.luDZakanA
;The stone rolled down the hill.
;pawWara pahAdZa se luDaka gayA. <--gola-gola Gumawe hue
;
;--5.gaja_gawi
;A slow, steady roll of the ship made.
;nAzva gaja gawi se Age baDZIM. <--manWara gawi <- laharoM ke sAWa GUmawI huI
;
;
;yahAz isa Sabxa ke lie wIna arWa nikalawe hE,jo ki sAre prayoga me lAe gae hE.
;
;sUwra : lipatanA[<gola-gola GUmanA]
;
;
