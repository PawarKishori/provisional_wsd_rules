;@@@ Added by 14anu-ban-03 (04-03-2015)
;He used political clout to get his work done. [same clp]
;उसने अपना कार्य करने के लिए राजनैतिक शक्ति/अधिकार का उपयोग किया . [manual]
(defrule clout2
(declare (salience 5001))
(id-root ?id clout)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sakwi/AXikAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clout.clp 	clout2   "  ?id "  Sakwi/AXikAra )" crlf))
)

;@@@ Added by 14anu-ban-03 (16-04-2015)
;If you upload a photo, people can clout the people in it. [oald]
;यदि आप तस्वीर अपलोड करे, तो इसमें लोगों को जोड सकते हैं . [manual]
(defrule clout3
(declare (salience 4900))
(id-root ?id clout)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 person)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id joda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clout.clp 	clout3   "  ?id "  joda )" crlf))
)


;------------------------ Default Rules ----------------------
;"clout","N","1.GUzsA"
;He gave a clout on his head.
(defrule clout0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (16-04-2015)
(id-root ?id clout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUzsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clout.clp 	clout0   "  ?id "  GUzsA )" crlf))
)

;"clout","V","1.GUzsA_mAranA"
;The robbers clouted him before looting his shop.
(defrule clout1
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (16-04-2015)
(id-root ?id clout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUzsA_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clout.clp 	clout1   "  ?id "  GUzsA_mAra )" crlf))
)

;"clout","N","1.GUzsA"
;He gave a clout on his head.
;--"2.praBAva"
;He used political clout to get his work done.
;

