
;(id-word ?id1 movie) Commented by Meena(19.01.10) to make the rule more general
;$$$ Modified by 14anu09[12-6-14] -- added movie|act|work|task|job 
;We saw a great movie.

(defrule great0
(declare (salience 5000))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 movie);###It is a great honor to be present here.
(id-root ?id1 movie|act|work|task|job) ; added  by 14anu09[12-6-14]
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great0   "  ?id "  baDZiyA )" crlf))
)


;@@@ Added by Bhagyashri Kulkarni (09-09-2016)
;There was a great big pile of books on the table.
;मेज पर पुस्तकों का एक बहुत बडा ढेर था . 
;He cut himself a great thick slice of cake.
;उसने स्वयं को केक का एक बहुत मोटा टुकडा काटा .
(defrule great24
(declare (salience 5000))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRaka-viSeRaNa ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great24   "  ?id "  bahuwa)" crlf))
)

;$$$ Modified by 14anu13 on 21-06-14
;The loss of his wife was a great blow to him.
;उसकी पत्नी की मृत्यु उसके लिये बड़ा धक्का थी |
;Added by Meena(20.01.10)
;She is a great fan of Lata.
;वह लता की एक बडी प्रशंसक है .  (translation added by Bhagyashri )
(defrule great1
(declare (salience 4900))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 fan|blow|loss)  ;added "blow" & "loss" by 14anu13 
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp     great1   "  ?id "  badZA )" crlf))
)



;commented by Bhagyashri Kulkarni (15.09.2016) as great7 takes care of the condition given here and meaning is also similar.
;Added by Meena(19.01.10)
;The painted doors look great . 
;Danish food is great.
;(defrule great2
;(declare (salience 4800))
;(id-root ?id great)
;?mng <-(meaning_to_be_decided ?id)
;(subject-subject_samAnAXikaraNa  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id baDZiyA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great2   "  ?id "  baDZiyA )" crlf))
;)


;$$$Modified by Bhagyashri Kulkarni (15.09.2016).
;4 is greater than 3. ;example added by bhagyashri (15.09.2016)
;Salience reduced  by Meena(19.01.10)
(defrule great3
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badZA));inconsistency in the mng in assert & print statement has been corrected by Sukhada (15.3.10); corrected the spelling from baDZA to badZA by bhagyashri 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp     great3   "  ?id "  badZA )" crlf))
)


;commented by bhagyashri. great is noun only in the phrase 'all-time great'. e.g. He was one of the 'all-time greats'. (oald)
;"great","Adj","1.badZA"
;Gandhiji was a great human being.
;(defrule great4
;(declare (salience 4800))
;(id-root ?id great)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id badZA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp     great4   "  ?id "  badZA )" crlf))
;)

;@@@ Added by Roja(22-04-14)
;Harishchandra was a great king.
;harIScanxra eka mahAna rAjA WA.
;God is great.

(defrule great5
(declare (salience 5100))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1  ?id)
(or (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) (id-root ?id1 God|god))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp     great5   "  ?id "  mahAna )" crlf))
)

;
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_great5
(declare (salience 5100))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(or (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) (id-root ?id1 God|god)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " great.clp   sub_samA_great5   "   ?id " mahAna )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_great5
(declare (salience 5100))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa ?id1 ?id)
(or (id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-root ?id1 God|god))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " great.clp   obj_samA_great5   "   ?id " mahAna )" crlf))
)

;$$$Modified by Bhagyashri Kulkarni (15-09-2016).
;The car is running with a great speed.
;गाडी एक तेज रफ्तार से दौड रही है . 
;@@@ Added by 14anu13 on 20-06-14
; The tree fell with a great crash.
;पेड़ तेज धमाके के साथ गिर गया |
(defrule great06
(declare (salience 5000))
(id-root ?id great)
(id-root ?id1 crash|lightening|thunder|speed) ;added speed by bhagashri (15.09.2016)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great06   "  ?id "  weja )" crlf))
)


;@@@ Added by 14anu09[12-6-14]
;It is a great honor to be present here.
;यहाँ उपस्थित होना एक बडा सम्मान है . 
;People were arriving in great numbers.
;लोग बडी सङ्ख्याएँ आ रहे थे .
(defrule great6
(declare (salience 4800))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great6   "  ?id "  badA )" crlf))
)


;$$$ ?Modified by Shruti Singh M.Tech(CS) Banasthali University (02-09-2016) 
;They look great.
;वे अच्छे दिखते हैं . 
;@@@ Added by 14anu09[12-6-14]
;She is great in dancing.
;वह नृत्य पर बहुत अच्छी है . 
(defrule great7
(declare (salience 5200));for higher preference from sub_samA_great5
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-at_saMbanXI ?id ?id1)(viSeRya-in_saMbanXI ?id ?id1) (subject-subject_samAnAXikaraNa ?id1 ?id )); added by Shruti (subject-subject_samAnAXikaraNa ?id1 ?id )
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA));changed from bahuwa_acCA to acCA by Shruti
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great7   "  ?id "  acCA )" crlf));changed from bahuwa_acCA to acCA by Shruti
)

;$$$ Modified by 14anu-ban-01 on (14-02-2016)
;From the sixteenth century onwards, great strides were made in science in Europe.	[NCERT corpus]
;सोलहवीं शताब्दी से यूरोप में विज्ञान के क्षेत्र में अत्यधिक प्रगति हुई.	[NCERT corpus]
;@@@ Added by 14anu-ban-05 on (08-11-2014)
;Application and exploitation of physical laws to make useful devices is the most interesting and exciting part and requires great ingenuity and persistence of effort.[NCERT]
;BOwika niyamoM ke anuprayoga waWA svArWasAXanoM xvArA upayogI yukwiyoM kA nirmANa karanA BOwikI kA awyanwa rocaka waWA uwwejanApUrNa BAga hE, jisake lie awyaXika pravINawA waWA sawaw prayAsoM kI AvaSyakawA howI hE.[NCERT]
;The steam engine, as we know, is inseparable from the Industrial Revolution in England in the eighteenth century, which had great impact on the course of human civilisation.[NCERT]
;jEsA ki hama jAnawe hEM ki BApa kA iMjana, iMglEMda meM aTAharavIM SawAbxI meM huI Oxyogika krAnwi, jisane mAnava saByawA ko awyaXika praBAviwa kiyA WA, se apqWakkaraNIya hE.[NCERT]
(defrule great8
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ingenuity|impact|stride)	;added 'stride' to the list by 14anu-ban-01 on (14-02-2016)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id awyaXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great8   "  ?id "  awyaXika )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp       great8   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;As late as 1933, the great physicist Ernest Rutherford had dismissed the possibility of tapping energy from atoms.[NCERT]
;varRa 1933 waka mahAna BOwika vijFAnI arnasta raxaraPorda paramANuoM se UrjA niRkAsana kI samBAvanA ko mana se xUra kara cuke We.[NCERT]
;A great insight of the twentieth century physics is that these different forces occurring in different contexts actually arise from only a small number of fundamental forces in nature.[NCERT]
;bIsavIM SawAbxI kI eka mahAna anwarxqRti yaha hE ki viBinna sanxarBoM meM pAe jAne vAle viviXa bala, vAswava meM, prakqwi ke kuCa mUla baloM se hI uwpanna howe hEM.[NCERT]
(defrule great9
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(or (id-root ?id1 physicist|insight)(id-cat_coarse ?id1 PropN))
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id mahAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great9   "  ?id "  mahAna )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp       great9   "  ?id "  physics )" crlf))
)


;@@@ Added by 14anu-ban-05 on (08-11-2014)
;You will find that this exercise is very educative and also great fun.[NCERT]
;Apa yaha pAezge ki yaha aByAsa bahuwa SikRApraxa waWA manoraFjaka hE.[NCERT]
;The proportional region within the elastic limit of the stress-strain curve (region OA in Fig. 9.3) is of great importance for structural and manufacturing engineering designs.[NCERT]
;saMracanAwmaka waWA nirmANa aBiyAnwrikI dijAina ke lie prawibala-vikqwi vakra meM prawyAsWa sImA ke anxara kA anukramAnupAwI kRewra (ciwra 9.3 meM kRewra @OA) bahuwa mahawvapUrNa howA hE.[NCERT]
(defrule great10
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 fun|importance)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great10   "  ?id "  bahuwa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great10   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;The property of this extremely important quantity I as a measure of rotational inertia of the body has been put to a great practical use. [NCERT]
;kisI piMda ke GUrNana ke jadawva ke mApa ke rUpa meM isa awyanwa mahawvapUrNa rASi @I ke bahuwa se vyAvahArika upayoga hEM.[NCERT]
(defrule great11
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 use)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id bahuwa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great11   "  ?id "  bahuwa_se )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great11   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;In one experiment, using a measuring instrument of resolution 0.1 cm, the measured value is found to be 3.5 cm, while in another experiment using a measuring device of greater resolution, say 0.01 cm, the length is determined to be 3.38 cm.[NCERT]
;eka prayoga meM 0.1 @cm viBexana kA mApaka-yanwra prayoga karake isakA mAna 3.5 @cm mApA gayA, jabaki, xUsare prayoga meM aXika viBexana vAlA (mAnA 0.01 @cm ) mApaka yanwra prayoga karake usI lambAI ko 3.38 @cm mApA gayA.[NCERT]
;If the planes are smooth, the final height of the ball is nearly the same as the initial height (a little less but never greater).[NCERT]
;yaxi xonoM Anawa samawaloM ke pqRTa aXika rukRa nahIM hEM wo geMxa kI anwima UzcAI usakI AramBika UzcAI ke lagaBaga samAna (kuCa kama, paranwu aXika kaBI nahIM) howI hE.[NCERT]
(defrule great12
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat ?id adjective_comparative)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 resolution|never|change|force|speed|acceleration|rate)		;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great12   "  ?id "  aXika )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great12   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;For the shells of radius greater than r, the point P lies inside.[NCERT]
;una saBI KoloM ke lie jinakI wrijyA @r se aXika hE, biMxu @P unake BIwara hE.[NCERT]
(defrule great13
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat ?id adjective_comparative)
(viSeRya-viSeRaka  ? ?id)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great13   "  ?id "  aXika )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great13   "  ?id "  physics )" crlf))
)


;@@@ Added by 14anu-ban-05 on (08-11-2014)
;In short, the greater the rate of change of momentum, the greater is the force.[NCERT]
;safkRepa meM, saMvega parivarwana kI xara aXika hE, wo bala aXika howA hE .[NCERT]
;It should be noted that though average speed over a finite interval of time is greater or equal to the magnitude of the average velocity, instantaneous speed at an instant is equal to the magnitude of the instantaneous velocity at that instant.[NCERT]
;yahAz yaha waWya XyAna meM raKanA hE ki jahAz kisI sImiwa samaya anwarAla meM vaswu kI Osawa cAla usake Osawa vega ke parimANa ke yA wo barAbara howI hE yA usase aXika howI hE vahIM kisI kRaNa para vaswu kI wAwkRaNika cAla usa kRaNa para usake wAwkRaNika vega ke parimANa ke barAbara howI hE .[NCERT]
(defrule great14
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat ?id adjective_comparative)
(or (saMjFA-saMjFA_samAnAXikaraNa  ? ?id)(subject-subject_samAnAXikaraNa  ? ?id)(viSeRya-det_viSeRaNa  ?id ?))
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id aXika_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great14   "  ?id "  aXika_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great14   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;This shows that speed is, in general, greater than the magnitude of the velocity.[NCERT]
;isase spaRta hE ki vaswu kI cAla sAmAnyawayA vega ke parimANa se aXika howI hE .[NCERT]
(defrule great15
(declare (salience 5200))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat ?id adjective_comparative)
(viSeRya-than_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id aXika_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great15   "  ?id "  aXika_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great15   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;The practice of drawing free-body diagrams is of great help in solving problems in mechanics.[NCERT]
;yAnwrikI kI samasyAoM ko hala karane meM bala nirxeSaka AreKa KIMcane kI praWA awyanwa sahAyaka hE.[NCERT]
;Application and exploitation of physical laws to make useful devices is the most interesting and exciting part and requires great ingenuity and persistence of effort.[NCERT]
;BOwika niyamoM ke anuprayoga waWA svArWasAXanoM xvArA upayogI yukwiyoM kA nirmANa karanA BOwikI kA awyanwa rocaka waWA uwwejanApUrNa BAga hE, jisake lie awyaXika pravINawA waWA sawaw prayAsoM kI AvaSyakawA howI hE.[NCERT]
(defrule great16
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 help)		;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id awyanwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great16   "  ?id "  awyanwa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great16   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;In karate or boxing we talk of 'powerful' punches ; These are delivered at a great speed.[NCERT]
;karAte yA boYksiMga meM SakwiSAlI mukkA vahI mAnA jAwA hE jo weja gawi se mArA jAwA hE.[NCERT]
(defrule great17
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 speed)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great17   "  ?id "  weja )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great17   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (08-11-2014)
;These laws were known to Newton and enabled him to make a great scientific leap in proposing his universal law of gravitation.[NCERT]
;ye niyama nyUtana ko jFAwa We; ina uwkqRta niyamoM ne nyUtana ko apanA guruwvAkarRaNa kA sArvawrika niyama praswAviwa karake asAXAraNa vEjFAnikoM kI pafkwi meM SAmila hone yogya banAyA.[NCERT]
(defrule great18
(declare (salience 5000))
(Domain physics)
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 leap)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id asAXAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great18   "  ?id "  asAXAraNa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  great.clp      great18   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (20-11-2014)
;Soil aggregation allows greater nutrient retention and utilization, decreasing the need for added nutrients.[agriculture]
;भूमि सङ्ग्रह अतिरिक्त पोषक के लिए जरूरत कम करके बढिया पोषक अवधारण और उपयोग की अनुमति देता है.[Manual]
(defrule great19
(declare (salience 5000))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat ?id adjective_comparative)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 retention)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great19   "  ?id "  baDiyA )" crlf))
)


;@@@ Added by 14anu17
;The great upheavel changed the direction of the flow of the waters to the south .
(defrule great20
(declare (salience 5100))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-word =(+ ?id 1)  upheavel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great20   "  ?id "  mahAna )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (09-01-2015)
;changed meaning from 'bade' to 'badA'
;@@@Added by 14anu19(23-06-2014)
;As before , the people received Siddhartha with great enthusiasm .
;पूर्ववत, लोगों ने बडे उत्साह से सिद्धार्थ का स्वागत किया . 
(defrule great21
(declare (salience 5000))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-word ?id1 enthusiasm)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp     great21   "  ?id "  badA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-03-2015)
;He played with great imagination and flair. [CALD]
;वह ऊँची कल्पना और स्वभाव के साथ खेला. 		[manual]

(defrule great22
(declare (salience 5000))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-word ?id1 imagination|thought)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp     great22   "  ?id "  UzcA )" crlf))
)

;@@@Added by 14anu-ban02(15-04-2016)
;His great knowledge, his wonderful memory and his untiring energy have been devoted to this purpose for nearly twenty years.[sd_verified]
;usakA pracura jFAna, usakI baDiyA smqwi Ora usakI aWaka UrjA karIba karIba bIsa varRoM se isa uxxeSya ko samarpiwa kie gaye hEM.[self]
(defrule great23
(declare (salience 4900))
(id-root ?id great)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 knowledge)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  great.clp 	great23   "  ?id "  pracura )" crlf))
)


