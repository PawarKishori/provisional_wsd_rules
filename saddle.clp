

;@@@ Added by 14anu-ban-11 on (27-02-2015)
;Their horses were saddled and waiting. (oald)
;उनके घोडे लादे हुऐ थे और प्रतीक्षा कर रहे थे . (self)
(defrule saddle0
(declare (salience 00))
(id-root ?id saddle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAxa_huE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  saddle.clp 	saddle0   "  ?id "  lAxa_huE)" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-02-2015)
;She swung herself into the saddle. (oald)
;उसने स्वयं को जीन में  झूलाया.  (self)
(defrule saddle1
(declare (salience 00))
(id-root ?id saddle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  saddle.clp 	saddle1   "  ?id "  jIna)" crlf))
)



;@@@ Added by 14anu-ban-11 on (27-02-2015)
;A saddle of lamb.(oald)
;मेमने का पीठ का मांस .(self)
(defrule saddle2
(declare (salience 20))
(id-root ?id saddle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 lamb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pITa_kA_mAMsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  saddle.clp 	saddle2   "  ?id "  pITa_kA_mAMsa)" crlf))
)

