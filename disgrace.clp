;@@@ Added by 14anu-ban-04 (22-04-2015)
;That sort of behaviour is a disgrace to the legal profession.          [oald]
;इस तरह का बर्ताव कानूनी पेशे के लिए शर्म की बात है .                                    [self]
(defrule disgrace2
(declare (salience 20))
(id-root ?id disgrace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sarma_kI_bAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgrace.clp    disgrace2   "  ?id "  Sarma_kI_bAwa )" crlf))
)

;@@@ Added by 14anu-ban-04 (22-04-2015)
;He had disgraced the family name.             [oald]
;उसने परिवार के  नाम को कलंकित किया था .                 [self]	
(defrule disgrace3
(declare (salience 30))
(id-root ?id disgrace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1) 
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id kalaMkiwa_kara))         ; 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disgrace.clp    disgrace3   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgrace.clp   disgrace3   "  ?id "  kalaMkiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 (22-04-2015)
;It's a disgrace that they are paid so little.                    [cald]
;यह शर्म की बात है कि उनको इतने थोड़े पैसे दिए जाते हैं .                            [self] 
(defrule disgrace4
(declare (salience 20))
(id-root ?id disgrace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-vAkyakarma  ?id ?id1)
(kriyA-vAkya_viBakwi ?id1 ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sarma_kI_bAwa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgrace.clp    disgrace4   "  ?id "  Sarma_kI_bAwa )" crlf))
)

;@@@ Added by 14anu-ban-04 (22-04-2015)
;You have disgraced us all with your behaviour.             [cald]
;अपने बर्ताव से आप  हमें  पूर्ण रूप से लज्जित कर चुके हैं .                      [self]
(defrule disgrace5
(declare (salience 20))
(id-root ?id disgrace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lajjiwa_kara))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgrace.clp   disgrace5   "  ?id "  lajjiwa_kara )" crlf))
)

(defrule disgrace0
(declare (salience 10))       ;salience reduced from '5000' to '10' by 14anu-ban-04 (22-04-2015)
(id-root ?id disgrace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalaMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgrace.clp 	disgrace0   "  ?id "  kalaMka )" crlf))
)

;"disgrace","N","1.kalaMka"
;His behavior has brought disgrace on himself && on his family.
;

;$$$ Modified by 14anu-ban-04 (22-04-2015)
;He was publicly disgraced and sent into exile.                [oald]
;उसका सार्वजनिक रूप से  अपमान किया था  और  उसको निर्वासन में भेजा गया था .               [self]
(defrule disgrace1
(declare (salience 10))                    ;salience reduced from '4900' to '10' by 14anu-ban-04 (22-04-2015)
(id-root ?id disgrace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id kA))                ;added by 14anu-ban-04 (22-04-2015)
(assert (id-wsd_root_mng ?id apamAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "   disgrace.clp     disgrace1   "  ?id " kA  )" crlf)                              ;added by 14anu-ban-04 (22-04-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disgrace.clp 	disgrace1   "  ?id "  apamAna_kara )" crlf))
)

;"disgrace","VT","1.apamAna_karanA"
;He got drunk && disgraced himself in the party.
;
