;@@@ Added by Pramila(BU) on 25-02-2014
;He waded through a dull novel.   ;shiksharthi
;उसने एक नीरस उपन्यास जैसे-तैसे पढ़ डाला.
(defrule wade1
(declare (salience 5000))
(id-root ?id wade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-through_saMbanXI  ?id ?id1)
(id-root ?id1 novel|book|magazine)
(id-word =(+ ?id 1) through) 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) jEse-wEse_paDZa_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wade.clp	wade1  "  ?id "  " (+ ?id 1) "  jEse-wEse_paDZa_dAla  )" crlf))
)

;He waded into the water to push the boat out. [oxford learner's dictionary]-- added sentence by Anita
;उन्होंने नाव को बाहर धकेलने के लिए पानी को कठिनाईपूर्वक पैदल पार किया ।
;"wade","V","1.kaTinAI_pUrvaka_pExala_pAra_karanA{pAnI,kIcadZa,rewa...}"
(defrule wade2
(declare (salience 5500));change salience 100 to 5000 by Anita
(id-root ?id wade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTinAI_pUrvaka_pExala_pAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wade.clp 	wade2   "  ?id "  kaTinAI_pUrvaka_pExala_pAra_kara )" crlf))
)

;$$$ Modified by Anita--21-07-2014
;Sometimes they had to wade waist-deep through mud. [oxford learner's dictionary]-- add sentence by Anita
;कभी कभी उन्हें कमर तक गहरे कीचड़ से होकर गुज़रना पड़ा ।
(defrule wade3
(declare (salience 4900))
(id-root ?id wade)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI  ?id ?sam) ; Added relation by Anita--21.7.2014
(id-root ?sam mud|swamp|water)	   ; Added by Anita--21.7.2014
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gujZara)); Changed 'pExala_pAra_kara' to 'gujZara' by Anita--22.7.2014
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wade.clp 	wade3   "  ?id "  gujZara )" crlf))
)

;"wade","VTI","1.pExala_pAra_karanA"
;He waded through waters

;@@@ Added by Anita--21-07-2014
;They waded the river at a shallow point.  [oxford learner's dictionary]
;उन्होंने एक उथले बिंदु पर नदी पार की ।
(defrule wade4
(declare (salience 4800))
(id-root ?id wade)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wade.clp 	wade4   "  ?id "  pAra_kara )" crlf))
)

;@@@ Added by Anita--21-07-2014
;The police waded into the crowd with batons. [oxford learner's dictionary]
;पुलिस ने डंडे के साथ भीड़ में आक्रमण किया ।
(defrule wade5
(declare (salience 5100))
(id-root ?id wade)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 baton)
(kriyA-with_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkrmaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  wade.clp 	wade5   "  ?id "  AkrmaNa_kara )" crlf))
)

;@@@ Added by Anita--22-07-2014
; You should not have waded in with all those unpleasant accusations. [oxford learner's dictionary]
;आपको  इन सभी अप्रिय आरोपों को आक्रामकरूप से नहीं लेना चाहिए ।
(defrule wade6
(declare (salience 5200))
(id-root ?id wade )
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) in)
(kriyA-in_with_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) AkrAmakarUpa_se_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wade.clp  wade6 "  ?id "  " (+ ?id 1) "  AkrAmakarUpa_se_le )" crlf))
)

;@@@ Added by Anita--22-07-2014
;She waded into him as soon as he got home. [oxford learner's dictionary]
;जैसे ही वह घर पहुँचा उसने उस पर आक्रमण कर दिया । 
(defrule wade7
(declare (salience 5300))
(id-root ?id wade )
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) into)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) AkrAmaNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wade.clp  wade7 "  ?id "  " (+ ?id 1) "  AkrAmaNa_kara )" crlf))
)

;@@@ Added by Anita--22-07-2014
;I spent the whole day , wading through the paperwork on my desk. [oxford learner's dictionary]
;मैंने पूरा दिन अपनी मेज पर कागजी कार्रवाई को बड़ी मेहनत से  पूरा करने में बिताया ।
(defrule wade8
(declare (salience 4400))
(id-root ?id wade)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through) 
(kriyA-through_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id   ?id1 badZI_mehanawa_se_pUrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " wade.clp	wade8  "  ?id "  "?id1"  badZI_mehanawa_se_pUrA_kara  )" crlf))
)
