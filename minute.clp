
(defrule minute0
(declare (salience 5000))
(id-root ?id minute)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) creature)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUkRma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  minute.clp 	minute0   "  ?id "  sUkRma )" crlf))
)

(defrule minute1
(declare (salience 4900))
(id-root ?id minute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id minata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  minute.clp 	minute1   "  ?id "  minata )" crlf))
)

(defrule minute2
(declare (salience 4800))
(id-root ?id minute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUkRma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  minute.clp 	minute2   "  ?id "  sUkRma )" crlf))
)

;"minute","Adj","1.sUkRma"
;It was a minute particle but hurt the eye badly.
;
;

;@@@ Added by 14anu01 on 27-06-2014
;The Secretary shall minute the proceedings of each meeting.
;सचिव हर एक बैठक के कार्रवाई तैयार करेगा . 
(defrule minute3
(declare (salience 4800))
(id-root ?id minute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  minute.clp 	minute3   "  ?id "   wEyAra_kara )" crlf))
)

;@@@ Added by 14anu20 on 27/06/2014.
;Come down this minute.
;अभी नीचे आइए .
(defrule minute4
(declare (salience 5000))
(id-root ?id minute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) this)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) aBI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " minute.clp	minute4  "  ?id "  " (- ?id 1) "    aBI  )" crlf))
)

;@@@ Added by 14anu20 on 27.06.2014.
;The train arrived at 9.05 to the minute.
;रेलगाडी यथार्थतः 9.05 पर पहुँच गई . 
(defrule minute5
(declare (salience 5000))
(id-root ?id minute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) the)
(id-root =(- ?id 2) to)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (- ?id 2) yaWArWawaH ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " minute.clp	minute5  "  ?id "  " (- ?id 1) "  " (- ?id 2)"   yaWArWawaH  )" crlf))
)



