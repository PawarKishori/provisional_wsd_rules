
(defrule prune0
(declare (salience 5000))
(id-root ?id prune)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id pruning )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kAta-CAzta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  prune.clp  	prune0   "  ?id "  kAta-CAzta )" crlf))
)

;"pruning","N","1.kAta-CAzta"
;We appointed a gardener for pruning.
;
(defrule prune1
(declare (salience 4900))
(id-root ?id prune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AlUbuKArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prune.clp 	prune1   "  ?id "  AlUbuKArA )" crlf))
)

;"prune","N","1.AlUbuKArA"
;Prunes cause stomach ache.
;


;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;On both sides of the gravel-road were acre upon acre of tea-bushes, all neatly pruned to the same height.  [Gyannidhi]
;कंकरीली सड़क के दोनों और सैकड़ों एकड़ जमीन पर चाय के पौधे एक ही ऊंचाई के छाँटे हुए खड़े थे।
(defrule prune2
(declare (salience 4800))
(id-root ?id prune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CAzte_hue))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prune.clp 	prune2   "  ?id "  CAzte_hue )" crlf))
)


(defrule prune3
(declare (salience 4700))
(id-root ?id prune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAtanA-CAzta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prune.clp 	prune3   "  ?id "  kAtanA-CAzta )" crlf))
)

;@@@ Added by 14anu-ban-09 on 25-9-14
;The artist & conservationist says he visits each one twice every year - in the spring to prune and in late summer to graft - for three years, until the tree is established.[Anusaaraka_4th_september]
;kalAkAra Ora saMrakRaNavAxI kahawe hE ki vaha hara eka prawyeka sAla meM wIna sAloM ke lie kaTora pariSrama karege jaba waka ki peda nIMva dAlawe hE. [Own Manual]

(defrule prune4
(declare (salience 5000))
(id-root ?id prune)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
(id-root ?id1 spring)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CaztAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prune.clp 	prune4   "  ?id "  CAztaI )" crlf))
)

;"prune","V","1.kAtanA-CAztanA"
;She has been pruning the roses in the rose garden.
;
