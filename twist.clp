;$$$ Modified by 14anu-ban-07 (29-09-2014)
;She gave a small twisted smile. (oald)
;उसने एक छोटी विकृत मुस्कराहट दी . (manual)
;Added by Meena(25.1.11)
;The letter was clearly the product of a twisted mind. 
(defrule twist0
(declare (salience 5000))
(id-root ?id twist)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id twisted)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 mind|smile) ;added by 14anu-ban-07
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikqwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  twist.clp     twist0   "  ?id "  vikqwa )" crlf))
)

(defrule twist1
(declare (salience 5000))
(id-root ?id twist)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  twist.clp 	twist1   "  ?id "  GumAva )" crlf))
)

;"twist","N","1.GumAva"
;He broke the handle with a violent twist.
;


;Meaning modified by Meena(25.1.11)
(defrule twist2
(declare (salience 4900))
(id-root ?id twist)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moda))
;(assert (id-wsd_root_mng ?id muda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  twist.clp 	twist2   "  ?id "  moda )" crlf))
)

;"twist","VT","1.mudanA"
;He twisted his head round to speak to his friend.
;--"2.gUWanA"
;Twist the creeper into the arch.
;--"3.vikqwa_karanA"
;He twisted his face after eating bittergourd.
;--"4.lipatanA"
;The nurse twisted the bandage round her arm.
;--"5.miWyA_arWa_xenA"
;The papers twisted everything.
;

;@@@ Added by Prachi Rathore[7-2-14]
;I twisted off the lid and looked inside.[oald]
;मैंने ढक्कन खोला और अन्दर देखा . 
(defrule twist3
(declare (salience 5000))
(id-root ?id twist)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " twist.clp 	twist3  "  ?id "  " ?id1 "  Kola )" crlf))
)

;@@@ Added 14anu-ban-07 (29-09-2014)
;Due to this torque, the suspended wire gets twisted till such time as the restoring torque of the wire equals the gravitational torque.(ncert corpus)
;इस बल आघूर्ण के कारण, निलम्बन तार में तब तक ऐण्ठन आती है जब तक प्रत्यानयन बल आघूर्ण गुरुत्वीय बल आघूर्ण के बराबर नहीं होता. (ncert corpus)
(defrule twist4
(id-root ?id twist)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ENTana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  twist.clp 	twist4   "  ?id "  ENTana )" crlf))
)

;@@@ Added by 14anu21 on 27.06.2014
;The snake was twisting around her arm.
;साँप उसकी बाहु के चारों ओर मोड रहा था . (Translation before adding rule) 
;साँप उसकी बाहु के चारों ओर लिपट रहा था . 
;He was twisting around the pillar. 
;वह खम्भे के चारों ओर मोड रहा था .(Translation before adding rule) 
;वह खम्भे के चारों ओर लिपट रहा था . 
(defrule twist5
(declare (salience 5000))
(id-root ?id twist)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-around_saMbanXI  ?id ?id1)
(kriyA-subject  ?id ?idsub)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lipata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  twist.clp 	twist5   "  ?id "  lipata )" crlf))
)

;@@@ Added by 14anu21 on 27.06.2014
;She twisted my words.
;उसने मेरे शब्द मोडे .
;उसने मेरे शब्दों का अर्थ विकृत किया . 
;I have been misunderstood and my words have been twisted. 
;मैं गलत समजा गया हो चुका है और मेरे शब्द मोडे गये हैं . 
;मैं गलत समजा गया हो चुका है और मेरे शब्द का अर्थ विकृत करा गया है . 
(defrule twist6
(declare (salience 5000))
(id-root ?id twist)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object  ?id ?id1)(kriyA-subject ?id ?id1))
(id-root ?id1 news|fact|word)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kA_arWa_vikqwa_kara))

(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  twist.clp 	twist6   "  ?id "  kA_arWa_vikqwa_kara )" crlf)
)
)






