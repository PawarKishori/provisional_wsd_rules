;@@@Added by 14anu-ban-02(18-03-2015)
;A backstage diplomacy.[oald]
;एक गुप्त कूटनीति . [self]
(defrule backstage1 
(declare (salience 100)) 
(id-root ?id backstage) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 diplomacy|area|door)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id gupwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  backstage.clp  backstage1  "  ?id "  gupwa )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(18-03-2015)
;Sentence: Should he stick to a backstage role?[coca]
;Translation: क्या उसे नेपथ्य भूमिका में बने रहना चाहिये? [manual]
(defrule backstage0 
(declare (salience 0)) 
(id-root ?id backstage) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nepaWya)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  backstage.clp  backstage0  "  ?id "  nepaWya )" crlf)) 
) 
