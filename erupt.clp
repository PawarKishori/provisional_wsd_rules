;@@@ Added by 14anu-ban-04 (28-02-2015)
;Violence erupted outside the embassy gates.                         [oald]
;राजदूत  द्वारों के बाहर हिंसा  फूट पड़ी .                                          [self]
(defrule erupt1
(declare (salience 20))
(id-root ?id erupt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 violence|Violence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PUta_pada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erupt.clp 	erupt1   "  ?id "  PUta_pada )" crlf))
)


;@@@ Added by 14anu-ban-04 (28-02-2015) 
;A rash had erupted all over his chest.                          [oald]
;उसकी पूरी  छाती पर  फुंसी  निकल आई थी.                                  [self]
(defrule erupt2
(declare (salience 20))
(id-root ?id erupt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 rash)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikala_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erupt.clp 	erupt2   "  ?id "  nikala_A )" crlf))
)

;@@@ Added by 14anu-ban-04 (02-03-2015) 
;The crowd erupted in applause and cheering.                          [oald]
;भीड़ वाहवाही और जयजयकार में जोर से चिल्लाई .                                    [self]
(defrule erupt3
(declare (salience 20))
(id-root ?id erupt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jora_se_cillA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erupt.clp 	erupt3  "  ?id "  jora_se_cillA )" crlf))
)

;----------------------------------------------------------DEFAULT RULES ------------------------------------------------------------------


;@@@ Added by 14anu-ban-04 (28-02-2015)
;The volcano could erupt at any time.                  [oald]
;ज्वालामुखी किसी भी समय  फट सकता है .                          [self]   
(defrule erupt0
(declare (salience 10))
(id-root ?id erupt)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  erupt.clp 	erupt0   "  ?id "  Pata )" crlf))
)

