;@@@ Added by 14anu-ban-06 (06-02-2015)
;It is a simple melody with complex harmonies. (cambridge)
;यह जटिल सुरों से युक्त एक साधारण मधुर सङ्गीत है . (manual)
(defrule harmony1
(declare (salience 2000))
(id-root ?id harmony)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-with_saMbanXI ?id1 ?id)
(id-root ?id1 melody)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harmony.clp 	harmony1   "  ?id "  sura )" crlf))
)

;@@@ Added by 14anu-ban-06 (06-02-2015)
;Your students will have the opportunity to sing in harmony. (COCA)
;आपके विद्यार्थियों के पास लय में गाने का मौका होगा . (manual)
(defrule harmony2
(declare (salience 2500))
(id-root ?id harmony)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI ?id1 ?id)
(id-root ?id1 sing)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harmony.clp 	harmony2   "  ?id "  laya )" crlf))
)

;------------------ Default Rules -------------------
;@@@ Added by 14anu-ban-06 (06-02-2015)
;Sarada Devi must have been a woman of uncommon character , tact and patience to keep such a household together and in harmony .(parallel corpus)
;शारदा देवी , सचमुच एक विशिष्ट स्वभाव की महिला थीं - जिन्होंने बड़ी कुशलता और धैर्य के साथ इतने बड़े घराने में यथोचित तालमेल बनाए रखा था .
;(parallel corpus)
(defrule harmony0
(declare (salience 0))
(id-root ?id harmony)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAlamela))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  harmony.clp  harmony0   "  ?id "  wAlamela )" crlf))
)

