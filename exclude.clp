;@@@ Added by 14anu-ban-04 (03-02-2015)
;Women were excluded from the club.                          [oald]
;स्त्रियों को क्लब से निकाल दिया गया था .                               [self]
(defrule exclude1
(declare (salience 30))
(id-root ?id exclude)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?sub)
(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) 
(kriyA-from_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exclude.clp 	exclude1   "  ?id "   nikAla_xe )" crlf))
)

;@@@ Added by 14anu-ban-04 (03-02-2015)
;We have seen that the frictional force is excluded from the category of conservative forces.               [NCERT-CORPUS]
;हम पहले ही देख चुके हैं कि घर्षण बल को संरक्षी बलों की श्रेणी से हटा दिया गया है.                                                      [NCERT-CORPUS]
(defrule exclude2
(declare (salience 20))
(id-root ?id exclude)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?sub)
(id-root ?sub ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(kriyA-from_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exclude.clp 	exclude2   "  ?id "  hatA_xe )" crlf))
)

;---------------------------- Default Rules ----------------

;@@@ Added by 14anu-ban-04 (03-02-2015)
;We should not exclude the possibility of negotiation.            [oald]
;हमें  मुआमला की सम्भावना नहीं निकालनी चाहिए .                                 [self]
;The possibility of error cannot be absolutely excluded.          [oald]
;त्रुटि की सम्भावना पूर्णतः नहीं निकाली जा सकती है .                              [self]
(defrule exclude0
(declare (salience 10))
(id-root ?id exclude)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  exclude.clp  exclude0   "  ?id "  nikAla )" crlf))
)

