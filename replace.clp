(defrule replace1
(declare (salience 1000))
(id-root ?id replace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  replace.clp      replace1   "  ?id "  baxala )" crlf))
)
;------ Suggestions by Chaitanya Sir
;Based on formal and informal 'replace' meaning need to be decided.
;Ex: In Fig. 6.2 the bar magnet is [replaced] by a second coil C2 connected to a battery.
;Here replace meaning should be 'prawisWApiwa'

;@@@ Added by  14anu-ban-10 on (24-11-2014)
;If the point 1 under discussion is shifted to the top of the fluid (say water), which is open to the atmosphere, P 1 may be replaced by atmospheric pressure (Pa) and we replace P2 by P.[ncert corpus]
;yaxi vicAraNIya biMxu 1 ko warala (mAnA pAnI) ke SIrRa Palaka para sWAnAMwariwa kara xiyA jAe jo vAyumaNdala ke lie KulA hE wo @P 1 ko vAyumaMdalIya xAba ( @P @a ) xvArA waWA @P 2 ko @P se prawisWApiwa kiyA jA sakawA hE.[ncert corpus]
(defrule replace2
(declare (salience 1000))
(id-root ?id replace)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=> 
(retract ?mng)
(assert (id-wsd_root_mng ?id prawisWApiwa_kiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  replace.clp      replace2   "  ?id "  prawisWApiwa_kiyA )" crlf))
)
