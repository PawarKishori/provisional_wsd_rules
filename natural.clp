
(defrule natural0
(declare (salience 5000))
(id-root ?id natural)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAkqwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  natural.clp 	natural0   "  ?id "  prAkqwika )" crlf))
)

;"natural","Adj","1.prAkqwika"
;This place is a natural beauty.
;--"2.svABAvika"
;After her recent accident it is quite natural for her to be scared of speeding vehicles.
;
(defrule natural1
(declare (salience 4900))
(id-root ?id natural)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAkqwika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  natural.clp 	natural1   "  ?id "  prAkqwika )" crlf))
)


;@@@ Added by 14anu-ban-08 (13-10-2014)
;A natural query that arises in our mind is the following: can we throw an object with such high initial speeds that it does not fall back to the earth?            [NCERT]
;तब स्वाभाविक रूप से हमारे मस्तिष्क में यह विचार उत्पन्न होता है क्या हम किसी पिंड को इतने अधिक आरम्भिक चाल से ऊपर फेंक सकते हैं कि वह फिर पृथ्वी पर वापस न गिरे?    [NCERT]
(defrule natural2
(declare (salience 5000))
(id-root ?id natural)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 query|view|question)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svABAvika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  natural.clp 	natural2   "  ?id " svABAvika  )" crlf))
)

;"natural","N","1.prAkqwika"
;I believe in naturals.
;
