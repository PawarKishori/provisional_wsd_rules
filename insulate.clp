
(defrule insulate0
(declare (salience 5000))
(id-root ?id insulate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id insulated )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id AvaraNayukwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  insulate.clp  	insulate0   "  ?id "  AvaraNayukwa )" crlf))
)

;"insulated","Adj","1.AvaraNayukwa"
;Insulated tapes are used in the electric wires.
;
(defrule insulate1
(declare (salience 4900))
(id-root ?id insulate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvaraNa_caDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  insulate.clp 	insulate1   "  ?id "  AvaraNa_caDA )" crlf))
)

;"insulate","V","1.AvaraNa_caDAnA"
;Tapes can be used to insulate electric wires.
;--"alaga_karanA"
;Guest room was insulated during winter.
;
;
;@@@ Added by 14anu-ban-06 (05-09-14)
;The vessel is kept inside a wooden jacket which contains heat insulating materials like glass wool etc.. (NCERT)
;इस बर्तन को एक लकड़ी के आवरण के भीतर, जिसमें ऊष्मारोधी पदार्थ जैसे काँच तंतु भरा होता है, रखा जाता है.
(defrule insulate2
(declare (salience 4950))
(id-root ?id insulate)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 heat)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 URmA_roXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " insulate.clp	 insulate2  "  ?id "  " ?id1  " URmA_roXI  )" crlf))
)


