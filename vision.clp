;@@@ Added by 14anu-ban-07 (04-12-2014)
;She has very little vision in her left eye.(cambridge )
;उसकी बाईं आंख की दृष्टि  बहुत कम है।(manual)
(defrule vision0
(id-root ?id vision)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xqRti))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vision.clp 	vision0   "  ?id "  xqRti )" crlf))
)

;@@@ Added by 14anu-ban-07 (04-12-2014)
;The vision of Dr. Meghnath Saha created a 37" Cyclotron in the Saha Institute of Nuclear Physics in Kolkata in 1953. (ncert)
;डॉ. मेघनाद साहा की दूरदर्शिता के कारण सन् 1953 में कोलकाता के साहा नाभिकीय भौतिकी संस्थान ने 37" साइक्लोट्रॉन स्थापित कर लिया था.(ncert)
;He had a vision of a world in which there would be no wars.(oald)
;I had visions of us getting hopelessly lost.(oald)
(defrule vision1
(declare (salience 1000))
(id-root ?id vision)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUraxarSiwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vision.clp 	vision1   "  ?id "  xUraxarSiwA )" crlf))
)

;@@@ Added by 14anu-ban-07 (04-12-2014)
;The idea came to her in a vision.(oald)
;यह विचार उसे स्वप्न में आया था.(manual) 
(defrule vision2
(declare (salience 1100))
(id-root ?id vision)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svapna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  vision.clp 	vision2   "  ?id "  svapna )" crlf))
)
