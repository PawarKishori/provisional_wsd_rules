;##############################################################################
;#  Copyright (C) 2014-2015 Komal Singhal (komalsinghal1744@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;@@@Added by 14anu19 (05-07-2014)                
;Check that the plug has not come loose.
;जाँचिए कि प्लग ढीला नहीं हुआ है .
(defrule loose0
(declare (salience 0))                 ;salience added by 14anu-ban-08 (28-01-2015)
(id-root ?id loose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DIlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loose.clp         loose0   "  ?id "  DIlA )" crlf))
)

;$$$Modified by 14anu-ban-08 (28-01-2015)  ;relation added ,commented relation,changed meaning, added viBakwi, run on parser 2,salience added
;@@@Added by 14anu19 (05-07-2014)
;The potatoes were sold loose, not in bags. 
;आलू थैलों में नहीं खुले मे, बेचे गये थे 
(defrule loose1
(declare (salience 100))                     ;salience added by 14anu-ban-08 (28-01-2015)
(id-root ?id loose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka ?id1 ?id)           		;relation added by 14anu-ban-08 (28-01-2015)
(id-root ?id1 not)                       	;constraint added by 14anu-ban-08 (28-01-2015)
;(kriyA-karma  ?id1 ?id2)           		;commented relation by 14anu-ban-08 (28-01-2015)
;(id-root ?id1 sell)                  		;commented relation by 14anu-ban-08 (28-01-2015)
;(subject-subject_samAnAXikaraNa  ?id2 ?id)     ;commented relation by 14anu-ban-08 (28-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KulA))          ;changed meaning from 'Kule_me' to 'KulA' by 14anu-ban-08 (28-01-2015)
(assert (id-wsd_viBakwi ?id meM))            ;added viBakwi by 14anu-ban-08 (28-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loose.clp         loose1   "  ?id "  KulA )" crlf)                                        ;changed meaning from 'Kule_me' to 'KulA_me' by 14anu-ban-08 (28-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  loose.clp 	loose1   "  ?id " meM)" crlf))                                        ;added viBakwi by 14anu-ban-08 (28-01-2015)
)

;$$$Modified by 14anu-ban-08 (28-01-2015)            ;changed meaning, run on parser 5, salience added
;@@@Added by 14anu19 (05-07-2014)
;She let her hair loose.[oald]......(run with parse number 5).
;उसने  अपने केश खुले छोडे .
(defrule loose2
(declare (salience 101))	;salience added by 14anu-ban-08 (28-01-2015)
(id-root ?id loose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-word ?id1 let)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kule_Coda))    ;changed meaning from 'KulA_Coda' to 'Kule_Coda' by 14anu-ban-08 (28-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " loose.clp loose2  "  ?id "  " ?id1 "  Kule_Coda  )" crlf))                              ;changed meaning from 'KulA_Coda' to 'Kule_Coda' by 14anu-ban-08 (28-01-2015)
)


;$$$Modified by 14anu-ban-08 (28-01-2015)               ;modify constraint, change 'id-word' to 'id-root',salience added
;A young man was of loose morals.
;एक युवक गलत विचारो का था .
;@@@Added by 14anu19 (05-07-2014)
(defrule loose3
(declare (salience 102))                              ;salience added by 14anu-ban-08 (28-01-2015)
(id-root ?id loose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 translation|guess|moral|thinking)        ;modify constraint from 'morals' to 'moral', change 'id-word' to 'id-root' by 14anu-ban-08 (28-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id galawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loose.clp         loose3   "  ?id "  galawa )" crlf))
)

