;Modified by Meena(26.7.11)
;Hooker's philosophy was to build and sell.
(defrule sell0
(declare (salience 1000))
(id-root ?id sell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id beca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sell.clp     sell0   "  ?id "  beca )" crlf))
)

;@@@ Added  by 14anu01 on 23-06-2014
;The net sell today is 1000.
;वास्तविक कमाई आज 1000 है . 
;आज की कुल कमाई १००० है[Translation improved by 14anu-ban-01 on (29-12-2014)]
(defrule sell_noun
(declare (salience 2000))
(id-root ?id sell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sell.clp     sell_noun   "  ?id "   kamAI )" crlf))
)

;@@@ Added by 14anu02 on 26.06.14
;They sold up and retired to the West Country.
;उन्होंने सम्पत्ति बेच दी और पश्चिमी प्रदेश के लिये अवकाश प्राप्त किया . 
;उन्होंने सम्पत्ति बेच दी और पश्चिमी प्रदेश चले गए[Translation improved by 14anu-ban-01 on (29-12-2014)]
(defrule sell1
(declare (salience 3000))
(id-root ?id sell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saMpawwi_beca_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sell.clp	sell1  "  ?id "  " ?id1 "  saMpawwi_beca_xe  )" crlf))
)

;@@@ Added by 14anu02 on 26.06.14
;The Church sold off the land for housing.
;गिरजाघर घर के लिए भूमि सस्ते में बेच कर मुक्त हुआ . 
(defrule sell2
(declare (salience 3000))
(id-root ?id sell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saswe_meM_beca_kara_mukwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sell.clp	sell2  "  ?id "  " ?id1 "  saswe_meM_beca_kara_mukwa_ho  )" crlf))
)

;@@@ Added by 14anu02 on 26.06.14
;When you say that John isn't interested in music, you're selling him short. 
;जब आप कहते हैं, कि जॉन संगीत में रुचि रखनेवाला नहीं है तो आप उसको कम आँक रहे हैं . 
(defrule sell3
(declare (salience 3000))
(id-root ?id sell)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(object-object_samAnAXikaraNa  ?id1 ?id2)
(id-word ?id2 short)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 kama_Azka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " sell.clp	sell3  "  ?id "  " ?id2 "  kama_Azka  )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (29-12-2014)
;@@@ Added by 14anu17
;These  wraps  or  bags  containing heroin might sell for perhaps 5 or 10 .
;मादक पदार्थ से युक्त  ये शाल या थैले शायद 5 या 10 के लिए बेचते हैं .
;मादक पदार्थ से युक्त  ये शाल अथवा थैले संभवतः ५ या १० तक बिक सकते हैं. [Translation improved by 14anu-ban-01 on (29-12-2014)]
(defrule sell4
(declare (salience 1000))
(id-root ?id sell)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-for_saMbanXI ?id ?id1)	;commented by 14anu-ban-01 on (29-12-2014)
(kriyA-subject ?id ?id1)	;added by 14anu-ban-01 on (29-12-2014)
(id-root ?id1 ?str)		;added by 14anu-ban-01 on (29-12-2014)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE))) ;added by 14anu-ban-01 on (29-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bika));changed "becanA" to "bika" by 14anu-ban-01 on (29-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sell.clp     sell4   "  ?id "  bika )" crlf));changed "becanA" to "bika" by 14anu-ban-01 on (29-12-2014)
)

