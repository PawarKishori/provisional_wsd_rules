
(defrule process0
;(declare (salience 5000)) ;salience commented by 14anu-ban-09 on (10-11-2014)
(id-root ?id process)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prakriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  process.clp 	process0   "  ?id "  prakriyA )" crlf))
)

; prakriya is in Sabdasutra, hence prakrama  prakriya : AMBA
;default_sense && category=noun	prakrama	0
;"process","N","1.prakrama"
;Learning a foreign language is a slow process.
;--"2.prakriyA"
;Irregular eating habits interfere with the digestive process.
;--"3.viXi"
;Computer graphics have simplified the process of movie-making.
;
(defrule process1
(declare (salience 4900))
(id-root ?id process)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMsAXiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  process.clp 	process1   "  ?id "  saMsAXiwa_kara )" crlf))
)

;"process","V","1.saMsAXiwa_karanA"
;They processed many fruits to make jams, jellies etc..
;--"2.agresara_honA"
;They processed through the cathedral was slow.
;

;@@@ Added by 14anu-ban-09 on (10-11-2014)
;The protein cake (oilcake meal) residue from oil processing is used as an animal feed and as a soil fertilizer. [Agriculture]
;तेल प्रसंस्करण से प्राप्त प्रोटीन केक-अवशिष्ट का (खली का मोटा आटा) पशुओं के लिए आहार के रुप में और मृदा-उर्वरक के रुप में उपयोग किया जाता है . [Self]

(defrule process2
(declare (salience 1000))
(id-root ?id process)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 oil)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasaMskaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  process.clp 	process2   "  ?id "  prasaMskaraNa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-11-2014)
;Soybeans can be processed to produce a texture and appearance similar to many other foods. [Agriculture]
;सोयाबीन अन्य अनेक आहारो के समान बनावट और प्रतीति देने के लिए ढाले जा सकते हैं. [Self]

(defrule process3
(declare (salience 4900))
(id-root ?id process)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 soybean)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  process.clp 	process3   "  ?id "  DAla )" crlf))
)

;@@@ Added by 14anu-ban-01 on (05-02-2016)
;They have their own way of processing.	[COCA]
;उनका सोचने का अपना तरीका है .[self]
(defrule process4
(declare (salience 4900))
(id-word ?id processing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?id2)	
(id-root ?id2 their|our|your|his|her|my)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id socanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  process.clp 	process4   "  ?id "  socanA )" crlf))
)

