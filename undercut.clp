;$$$ Modified by 14anu-ban-07 (15-12-2014)
;@@@ Added by 14anu06(Vivek Agarwal) on 19/6/2014*****
;You should not undercut my authority. 
;तुम्हे मेरे अधिकार का अवमूल्यन नही करना चाहिए.
(defrule undercut0
(declare (salience 5000))
(id-root ?id undercut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)  ;cat_coarse changed from noun to verb by 14anu-ban-07 (15-12-2014)
(kriyA-object ?id ?id1)
(id-root ?id1 authority)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avamUlyana_kara)) ;meaning changed from avamUlyana karanA to avamUlyana_kara 14anu-ban-07 (15-12-2014)
(assert  (id-wsd_viBakwi   ?id1  kA))   ;added viBakwi  14anu-ban-07 (15-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " undercut.clp  undercut0   "  ?id "  avamUlyana_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  undercut.clp      undercut0   "  ?id1 " meM )" crlf)
)
)

;@@@ Added by 14anu-ban-07 (15-12-2014)
;We were able to undercut our European rivals by 5%.(oald)
;हम 5 % के द्वारा हमारे युरोपीय प्रतिद्वन्दी को कम दाम में देने में समर्थ थे . (manual)
(defrule undercut1
(id-root ?id undercut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_xAma_meM_xe))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " undercut.clp  undercut1   "  ?id "  kama_xAma_meM_xe )" crlf))
)

