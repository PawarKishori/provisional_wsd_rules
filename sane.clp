;@@@ Added by 14anu-ban-11 on (16-02-2015)  ;Note:- working properly on parser no. 9.
;Its hard to stay sane under such awful pressure.(hinkhoj)
;यह कठिन है ऐसे बुरे दाब के अधीन स्वस्थचित्थ रहना . (self)
(defrule sane1
(declare (salience 20))
(id-root ?id sane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 hard)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id svasWaciwWa)) ; Commented by 14anu-ban-11 on (19-02-2015)
(assert (id-wsd_root_mng ?id svasWaciwwa))  ;Added by 14anu-ban-11 on (19-02-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sane.clp 	sane1   "  ?id "  svasWaciwWa )" crlf)) ; Commented by 14anu-ban-11
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sane.clp 	sane1   "  ?id "  svasWaciwwa )" crlf));Added by 14anu-ban-11 on (19-02-2015) 
)


;@@@ Added by 14anu-ban-11 on (16-02-2015)
;Appears to be completely sane.(hinkhoj)
;पूरी तरह से समझदारीपूर्ण प्रतीत हुआ .(self)
(defrule sane2
(declare (salience 30))
(id-root ?id sane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
(id-root ?id1 completely)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJaxArIpUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sane.clp 	sane2   "  ?id "  samaJaxArIpUrNa )" crlf))
)

;-------------- Default Rules ----------------------------

;@@@ Added by 14anu-ban-11 on (16-02-2015)
;My english teacher is sane.(hinkhoj)
;मेरा अङ्ग्रेज शिक्षक समझदार है . (anusaaraka)
(defrule sane0
(declare (salience 10))
(id-root ?id sane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJaxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sane.clp     sane0   "  ?id "  samaJaxAra )" crlf))
)

