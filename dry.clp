
(defrule dry0
(declare (salience 5000))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 suKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dry.clp	dry0  "  ?id "  " ?id1 "  suKA  )" crlf))
)

;She was drying off her hair in the sun.
;vaha XUpa meM apane bAla suKA rahI WI
(defrule dry1
(declare (salience 4900))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrI_waraha_se_suKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dry.clp	dry1  "  ?id "  " ?id1 "  pUrI_waraha_se_suKA  )" crlf))
)

;Please dry you out with this towel otherwise you'll get cold.
;kqpyA isa wOlie se apane Apa ko pUrI waraha se suKA lo nahIM wo wumhe TaNda laga jAegI
(defrule dry2
(declare (salience 4800))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sUKa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dry.clp	dry2  "  ?id "  " ?id1 "  sUKa_jA  )" crlf))
)

;The pond dried up in the heat.
;wAlAba garmI ke kAraNa sUKa gayA
(defrule dry3
(declare (salience 4700))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BUla_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dry.clp	dry3  "  ?id "  " ?id1 "  BUla_jA  )" crlf))
)

;She suddenly dried up during her speech.
;vaha BARaNa xewe samaya bolawe-bolawe BUla gaI
(defrule dry4
(declare (salience 4600))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUKa_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " dry.clp dry4 " ?id "  sUKa_jA )" crlf)) 
)

(defrule dry5
(declare (salience 4500))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sUKa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dry.clp	dry5  "  ?id "  " ?id1 "  sUKa_jA  )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-04-2015)
;A shampoo for dry hair.                         [oald]
;रूखे केश के लिए शैंपू .                                [self]
(defrule dry8
(declare (salience 105))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 hair) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUKA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dry.clp 	dry8   "  ?id "  rUKA )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-04-2015)
;On the whole, I like dry wine better than sweet.                     [cald]
;कुल मिला कर, मैं फीकी वाइन  की तुलना में मिठाई पसन्द करता हूँ .                      [self]
;This wine is too dry for me.                                      [oald]
;यह वाइन मेरे लिए ज्यादा ही फीकी है .                                           [self]
(defrule dry9
(declare (salience 105))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 wine|sherry|cider|martini) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PIkA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dry.clp 	dry9   "  ?id "  PIkA )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-04-2015)
;A dry voice.                    [oald]
;एक नीरस आवाज .                    [self]
;This is dry work.                [oald]
;यह नीरस कार्य है .                     [self]
(defrule dry10
(declare (salience 106))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 voice|reading|work) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIrasa))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dry.clp 	dry10   "  ?id "  nIrasa )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-04-2015)
;I'm a bit dry.                   [oald]
;मैं थोड़ा सा प्यासा हूँ .                   [self]
(defrule dry11
(declare (salience 106))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pyAsA))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dry.clp 	dry11   "  ?id "  pyAsA )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-04-2015)
;Dry the clothes outside.                    [same clp file]  
;वस्त्रों को बाहर  सुखाइए .                                [self]
(defrule dry12
(declare (salience 4410))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suKA))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  dry.clp     dry12   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dry.clp 	dry12   "  ?id "  suKA )" crlf))
)

;@@@ Added by 14anu-ban-04 (10-04-2015)
;Use this towel to dry your hands.                        [oald]
;अपने हाथों को पोंछने के लिए इस तौलिया का उपयोग कीजिए .                 [self]
;Dry your eyes.                                           [oald]
;अपनी आँखों को पोंछिए . 	                                  [self]
(defrule dry13
(declare (salience 4420))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 hand|tear|eye|tears)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id poMCa))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  dry.clp     dry13   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dry.clp 	dry13   "  ?id "  poMCa )" crlf))
)

;--------------------------------------------- Default Rules --------------------------------------
;@@@ Added by 14anu-ban-04 (10-04-2015)
;Store onions in a cool dry place.            [oald] 
;प्याजों को  ठण्डे सूखे स्थान में रखिए.                     [self]
(defrule dry7
(declare (salience 100))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng) 
(assert (id-wsd_root_mng ?id sUKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dry.clp 	dry7   "  ?id "  sUKA )" crlf))
)


;The fruit is dried in the sun.               [oald]      ;added  by 14anu-ban-04 on (10-04-2015)
;फल धूप में सूखा हुआ है .                              [self]     ;added  by 14anu-ban-04 on (10-04-2015)
(defrule dry6
(declare (salience 4400))
(id-root ?id dry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dry.clp 	dry6   "  ?id "  sUKa )" crlf))
)

;default_sense && category=verb	suKA	0
;"dry","VT","1.suKAnA"

;
;
