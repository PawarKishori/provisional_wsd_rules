;$$$ Modified by 14anu-ban-08 (12-12-2014)   ;comment the relation and category, new relation is added and category is added, runs on parser 4
;@@@ Added by 14anu17
;The report sets out a number of ways to improve maternity care.
;प्रसूति देखभाल को बेहतर करने के लिए कई तरीकों से रिपोर्ट में बताया गया है.  [Self]    ;Hindi Translation is added by 14anu-ban-08 (12-12-2014)
(defrule maternity0
(declare (salience 5000))
(id-root ?id maternity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)      ;changed category adjective as noun by 14anu-ban-08 (12-12-2014) maternity is noun but it takes as adjective, noun is coming on parser 4
;(viSeRya-viSeRaNa  ?id1 ?id)       ;commented by 14anu-ban-08 (12-12-2014) maternity is noun, and viSeRya-viSeRaNa is not coming in noun
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)    ;added by 14anu-ban-08 (12-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prasUwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  maternity.clp    maternity0   "  ?id "  prasUwi )" crlf))
)

;@@@ Added by 14anu-ban-08 (12-12-2014)       ;runs on parser 4 otherwise it takes as adjective
;He took his wife to the maternity hospital for checkup.     [Hindkhoj]
;वह अपनी पत्नी को परीक्षण के लिए मातृत्व अस्पताल ले गया.          [Self]
(defrule maternity1
(declare (salience 0))
(id-root ?id maternity)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAwqwva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  maternity.clp    maternity1   "  ?id "  mAwqwva )" crlf))
)

