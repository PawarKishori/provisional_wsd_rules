;@@@ Added by 14anu-ban-04 (16-02-2015)
;They will debut the products at the trade show.                     [oald]
;वे व्यापार प्रदर्शन में उत्पाद प्रस्तुत करेंगे .                                         [self]
(defrule debut1
(declare (salience 20))
(id-root ?id debut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praswuwa_kara/peSa_kara))          
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  debut.clp 	debut1   "  ?id "  praswuwa_kara/peSa_kara )" crlf))
)

;------------------------- Default Rules -------------------

;@@@ Added by 14anu-ban-04 (16-02-2015)
;She is hoping her US debut will be the first step on the road to fame and fortune.            [oald]
;वह उम्मीद कर रही है कि उसका यूएस में  प्रथम अभिनय प्रसिद्धि और  सफलता के मार्ग पर पहला कदम रहेगा .                         [self]
(defrule debut2
(declare (salience 10))
(id-root ?id debut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praWama_aBinaya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  debut.clp 	debut2   "  ?id "  praWama_aBinaya)" crlf))
)

;@@@ Added by 14anu-ban-04 (16-02-2015)
;The ballet will debut next month in New York.                      [oald]
;नृत्यनाटिका का न्यूयार्क में अगले महीने प्रथम प्रदर्शन होगा .                             [self]
(defrule debut0
(declare (salience 10))
(id-root ?id debut)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)     
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id praWama_praxarSana_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  debut.clp   debut0   "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  debut.clp 	debut0   "  ?id "  praWama_praxarSana_ho)" crlf))
)


