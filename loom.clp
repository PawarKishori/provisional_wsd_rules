(defrule loom0
(declare (salience 100))
(id-root ?id loom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id karaGA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loom.clp 	loom0   "  ?id "  karaGA )" crlf))
)

(defrule loom1
(declare (salience 100))
(id-root ?id loom)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aspaRta_CAyA_xIKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loom.clp 	loom1   "  ?id "  aspaRta_CAyA_xIKa )" crlf))
)

;"loom","V","1.aspaRta CAyA xIKanA"
;I was relaxing on the ground when an enormous figure suddenly appeared from somewhere && loomed at me.
;--"2.samBAvanA honA"
;The possibility of a sense storm loomed large in the eastern horizon.
;
;

;@@@ Added by 14anu05 on 28.06.14
;A dark shape loomed up ahead of us.
;एक काली आकृति हमारे अागे प्रकट हुई .
(defrule loom2
(declare (salience 4900))
(id-root ?id loom)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 up)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 prakata_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  loom.clp     loom2   "  ?id "  " ?id1 "  prakata_ho)" crlf))
)


;@@@ Added by 14anu05 on 28.06.14
;There was a crisis looming.
;वहाँ एक संकट मँडरा रहा था .
(defrule loom3
(declare (salience 4900))
(id-root ?id loom)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 crisis|trouble|problem)
(viSeRya-kqxanwa_viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mazdarA_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  loom.clp 	loom3   "  ?id "  mazdarA_raha)" crlf))
)


