
;Added by Meena(18.9.10)
;In the first experiment , a large magnet and a small magnet are weighed separately , and then one magnet is hung from the pan of the top balance so that it is directly above the other magnet .
(defrule top00
(declare (salience 5000))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 ?)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Upara_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp       top00   "  ?id "  Upara_vAlA )" crlf))
)

;$$$Modified by 14anu-ban-07,(04-02-2015)
;There were impressions around her ankles made by the tops of her socks. (cambridge)
;उसके टखने के इधर उधर निशान उसके मोजों के ऊपरी सिरे से बन गए थे . [manual]

;Added by Meena(27.7.10)
;She was standing at the top of the stairs . 
(defrule top0
(declare (salience 5000))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 stairs|socks)  ;added socks by 14anu-ban-07,(04-02-2014)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UparI_sirA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp       top0   "  ?id "  UparI_sirA )" crlf))
)

(defrule top1
(declare (salience 4900))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 muzha_waka_Bara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " top.clp	top1  "  ?id "  " ?id1 "  muzha_waka_Bara  )" crlf))
)



;You top the cake with cream && chocolate.
;--"2.SiKara_para_pahuzcanA"
;We topped the mountain to have a clear view.
;--"3.se_baDakara_rahanA"
;He has topped the list by securing majority of votes.
;--"5.niSAna_CUtanA [golPa_meM]"
;He lost the game because he topped the ball in the last round.
;
;LEVEL 
;Headword : top
;
;
;Examples --
;
;"top","Adj","1.sarvocca_sWAna"
;All his relatives have occupied the top positions in the company.
;usake parivAra ke saBI saxasyoM ne kampanI ke sarvocca_sWAnoM ko Gera liyA hE.
;--"2.muKya"
;He is a top man in the management.
;vaha prabanXa_vyavasWA kA eka pramuKa vyakwi hE.
;
;"top","N","1.UparI_BAga"
;The mower cuts off the tops of the grass.
;lOYna moara GAsa ke UparI BAga ko kAta xewA hE.
;--"2.Upara"
;Put your books on top of the desk.
;apanI kiwAbeM deska ke Upara para raKa xo. 
;--"3.lattU"
;The boy is playing with a top.
;ladZakA lattU se Kela_rahA_hE.
;--"4.paMcama_svara"
;The girl shouted at the top of her voice when the intruder entered.
;jaba anajAnA_AxamI(GusapETiyA) GusA wo ladZakI paMcama_svara meM cillAyI
;--"5.kurwA"
;He got a bright red top && a bead string for his birthday.
;use apane janmaxina para eka BadZakIlA lAla kurwA Ora eka mowiyoM kI mAlA milI. 
;--"6.Dakkana"
;He removed the top of the carton.
;usane dibbe kA Dakkana hatA xiyA.
;
;"top","VT","1.Upara_lagAnA"
;You top the cake with cream && chocolate.
;wuma keka ke Upara krIma Ora cOYkaleta lagAo. 
;--"2.SiKara_para_pahuzcanA"
;We topped the mountain to have a clear view.
;spaRta xqSya ke liye hama pahAdZa ke SiKara para pahuzce.
;He has topped the list by securing majority of votes.
;aXikawara vota pAkara vaha sUcI ke SiKara para hE. 
;--"3.niSAna_cUkanA{golPa_meM}"
;He lost the game because he topped the ball in the last round.
;vaha Kela meM hAra gayA kyoMki anwima rAuMda meM vaha geMxa kA niSAnA cUka gayA.
;
;
;Upara xiye uxAharaNoM se 'top' Sabxa kA eka spaRta arWa jo uBara kara AwA hE vaha hE 'Upara_honA' yA 'UparI_BAga'. isake SeRa saBI asambanXiwa arWa isI se judZe hue hEM.
;
;anwarnihiwa sUwra ;
;
;sarvocca_sWAna -UparI_BAga{SiKara, SIrRa}-UparI hisse para jo howA hE vaha {Dakkana, kurwA}--UparI hisse se sambanXiwa kriyA {Upara jAnA, Upara lagAnA, Upara se nikala jAnA(niSAna_cUkanA)}
;
;
;kinwu isake kaI arWoM ke liye hinxI meM BI 'tOYpa' kAPI pracaliwa ho gayA hE.
;awaH PilahAla isakA sUwra hogA --
;
;sUwra : tOYpa`
;

;@@@ Added by Prachi Rathore[3-2-14]
;She topped off her performance with a dazzling encore.
;??
(defrule top3
(declare (salience 4900))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 purA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " top.clp	top3  "  ?id "  " ?id1 "  purA_kara )" crlf))
)

;@@@ Added by Prachi Rathore[3-2-14]
;Inflation topped out at 12%.
;मुद्रा स्फीति 12 % के शिर्ष पर पहुँची . 
(defrule top4
(declare (salience 4900))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 SirRa_para_pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " top.clp	top4  "  ?id "  " ?id1 "  SirRa_para_pahuzca)" crlf))
)

;@@@ Added by Prachi Rathore[3-2-14]
;I need a top to go with these trousers.
;मुझे  इस पतलून के साथ एक टाप की जरूरत होती है . 
(defrule top7
(declare (salience 4900))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-anaBihiwa_subject  ?id1 ?id)
(kriyA-with_saMbanXI  ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id toYpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top7   "  ?id "  toYpa )" crlf))
)

;$$$Modified by 14anu-ban-07 (21-01-2015)
;@@@ Added by 14anu15 Gourav Sahni (MNNIT ALLAHABAD) on 28.06.2014 email-id:sahni.gourav0123@gmail.com
;She wore a black top. 
;उसने एक काला टॉप पहना . 
(defrule top8
(declare (salience 4800))	;added by 14anu-ban-07 (21-01-2015)
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)  	;added ?id1 by 14anu-ban-07 (21-01-2015)
(id-root ?id1 wear)		;added by 14anu-ban-07 (21-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id toYpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp      top8   "  ?id "  toYpa )" crlf))
)


;@@@ Added by 14anu-ban-07,(18-10-2014)
;Thus, in more general cases of rotation, such as the rotation of a top or a pedestal fan, one point and not one line, of the rigid body is fixed.(ncert)
;GUrNana gawi ke aXika sArvika mAmaloM meM, jEse ki lattU yA pITikA-pafKe ke GUmane meM, xqDa piMda kA eka biMxu sWira rahawA hE, na ki eka reKA.
(defrule top9
(declare (salience 5000))
(Domain physics)
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lattU))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top9   "  ?id "  lattU )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  top.clp 	top9   "  ?id "  physics )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (31-03-2016): changed meaning 
;@@@ Added by 14anu-ban-07,(18-10-2014)
; A stone released from the top of a building accelerates downward due to the gravitational pull of the earth.(ncert)
;kisI Bavana ke SiKara se binA aXomuKI XakkA xiye mukwa kiyA gayA pawWara pqWvI ke guruwvIya KiMcAva ke kAraNa wvariwa ho jAwA hE.(ncert)
(defrule top10
(declare (salience 5100))
;(Domain physics)	;commented by 14anu-ban-01 not required
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)	;NOTE: rule can be restricted on occurence of counter example
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SiKara/UparI_BAga))	;changed meaning from "sarvocca_sWAna" to "SiKara/UparI_BAga"
;(assert (id-domain_type  ?id physics))	;commented by 14anu-ban-01 not required
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top10   "  ?id "  SiKara/UparI_BAga )" crlf))	;changed meaning from "sarvocca_sWAna" to "SiKara/UparI_BAga"
;(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  top.clp 	top10   "  ?id "  physics )" crlf))	;commented by 14anu-ban-01 not required
)

;@@@ Added by 14anu-ban-07,(27-10-2014)
;The rigid body in this problem, namely the cylinder, shifts from the top to the bottom of the inclined plane, and thus, has translational motion. (ncert)
;यह दृढ पिंड (बेलन) नत तल के सर्वोच्च स्थान से उसकी तली तक स्थानान्तरित होता है, अतः इसमें स्थानान्तरण गति है.(manual)
(defrule top11
(declare (salience 5100))
(Domain physics)
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvocca_sWAna))
(assert (id-domain_type  ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top11   "  ?id "  sarvocca_sWAna )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  top.clp 	top11   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-07,(03-02-2015)
;He filled my glass to the top.(oald)
;उसने मेरी गिलास  ऊपर तक  भरी . (self)
(defrule top12
(declare (salience 5200))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI  ?id1 ?id)
(id-root ?id1 fill)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Upara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top12  "  ?id "  Upara )" crlf))
)

;@@@ Added by 14anu-ban-07,(02-02-2015)
;Write your name at the top.(oald)
;ऊपर आपका नाम लिखिए . (self)
(defrule top13
(declare (salience 5300))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-at_saMbanXI  ?id1 ?id)
(id-root ?id1 write)
(viSeRya-det_viSeRaNa  ?id ?id3)
(pada_info (group_head_id ?id)(preposition ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 ?id3 Upara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " top.clp	top13  "  ?id "  " ?id ?id2 ?id3 "  Upara  )" crlf))
)

;@@@ Added by 14anu-ban-07,(02-02-2015)
;The title is right at the top of the page.(oald)
;नाम  पृष्ठ के  ठीक ऊपर  है .(self) 
(defrule top14
(declare (salience 5400))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 page)
(viSeRya-det_viSeRaNa  ?id ?id3)
(pada_info (group_head_id ?id)(preposition ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 ?id3 Upara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " top.clp	top14  "  ?id "  " ?id ?id2 ?id3 "  Upara  )" crlf))
)

;@@@ Added by 14anu-ban-07,(03-02-2015)
;Where's the top of this pen?(oald)
;इस कलम का ढक्कन कहाँ है ? (self)
;He unscrewed the top of the bottle.(coca)
;उसने बोतल का ढक्कन  खोला . (self)
(defrule top15
(declare (salience 5500))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 pen|bottle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Dakkana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top15  "  ?id "  Dakkana )" crlf))
)

;@@@ Added by 14anu-ban-07,(12-02-2015)
;Our grandmother would in no time prepare us a glass of warm milk with saffron sprinkled on top when we went crying to her with bruise on our knee or wound on the elbow.(email)
;हमारी दादी बिना समय लगाये गर्म दूध के गिलास के ऊपर से केसर छिड़क कर तैयार करती हैं जब हम रोते हुए हाथ और घुटने में लगी चोट के साथ उनके पास जाते हैं.(manual)
(defrule top16
(declare (salience 5600))
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI ?id1 ?id)
(id-root ?id1 sprinkle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Upara))
(assert (id-wsd_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top16  "  ?id "  Upara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  top.clp top16  "  ?id " se)" crlf)
)
)

;xxxxxxxxxxxxxxxxxxx default rules xxxxxxxxxxxxxxxxxxxxxxxxxxx
;default_sense && category=verb	Upara_dAla	0
;"top","VT","1.Upara_dAlanA"
(defrule top2
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upara_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top2   "  ?id "  upara_dAla )" crlf))
)

;@@@ Added by Prachi Rathore[3-2-14]
;He's at the top of his profession.[oald]
;वह उसके पेशे के सर्वोच्च स्थान पर है . 
(defrule top5
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvocca_sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top5   "  ?id "  sarvocca_sWAna )" crlf))
)

;@@@ Added by Prachi Rathore[3-2-14]
;She got top marks for her essay.[oald]
;उसने उसके निबन्ध के लिये सर्वोत्तम अङ्क प्राप्त किए . 
(defrule top6
(id-root ?id top)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarvowwama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  top.clp 	top6   "  ?id "  sarvowwama )" crlf))
)
