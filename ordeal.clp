
;----------------------DEAFAULT RULE-----------------------------------------------------
;@@@ Added by 14anu-ban-09 on (12-03-2015)
;It was my luck that carried me through his determination carried him through the ordeal. [oald]	;added by 14anu-ban-09 on (13-03-2015)
;यह उसका दृढ़ निश्चय था जिसने उसे कठिन परिक्षा से सफलतापूर्वक बाहर निकाला.		[manual]	;added by 14anu-ban-09 on (13-03-2015)
(defrule ordeal0
(declare (salience 000))
(id-root ?id ordeal)
?mng <-(meaning_to_be_decided ?id)  
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id agni_parIkRA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ordeal.clp 	ordeal0   "  ?id "  agni_parIkRA )" crlf))
)

;------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (12-03-2015)
;The hostages spoke openly about the terrible ordeal they had been through. [oald]
;बन्धक व्यक्ति अपने भयानक कटु अनुभव के बारे में खुलेआम बोले जिसमे वे रह रहे थे .		    [manual] 

(defrule ordeal1
(declare (salience 1000))
(id-root ?id ordeal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 terrible)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id katu_anuBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ordeal.clp 	ordeal1   "  ?id "  katu_anuBava )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-03-2015)
;The interview was less of an ordeal than she'd expected.		    [oald]
;बसाक्षात्कार  कटु अनुभव की तुलना में उम्मीद से कम था  . 		    		    [manual] 
(defrule ordeal2
(declare (salience 1000))
(id-root ?id ordeal)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(subject-subject_samAnAXikaraNa  ?id2 ?id1)
(id-root ?id2 interview)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id katu_anuBava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ordeal.clp 	ordeal2   "  ?id "  katu_anuBava )" crlf))
)


