;@@@ Added by 14anu-ban-11 on (24-04-2015)
;I was still woozy from flu/the anaesthetic/the medication/the wine.(cald)
;मैं अभी भी बीमार थी  बुखार/निश्चेतक/वाइन से.(self) 
(defrule woozy0
(declare (salience 10))
(id-root ?id woozy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
(id-root ?id1 still)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bImAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  woozy.clp 	woozy0   "  ?id "  bImAra)" crlf))
)

;@@@ Added by 14anu-ban-11 on (24-04-2015)
;Note:- working properly on parser no.2
;I felt a little woozy this morning. (coca)
;मैं आज सुबह थोडा चक्कर महसूस कर रही थी . (oald)
(defrule woozy2
(declare (salience 30))
(id-root ?id woozy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 little)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakkara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  woozy.clp 	woozy2  "  ?id "  cakkara)" crlf))
)

;------------------------------------ Default Rules ------------------------------

;@@@ Added by 14anu-ban-11 on (24-04-2015)
;I'm also starved and woozy.(coca)
;मैं भी क्षुधातुर और स्तम्भित हूँ . (self)
(defrule woozy1
(declare (salience 00))
(id-root ?id woozy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swaMBiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  woozy.clp 	woozy1   "  ?id "  swaMBiwa)" crlf))
)




