;@@@ Added by 14anu-ban-06  (26-08-14)
;An optical microscope uses visible light to ' look ' at the system under investigation.(NCERT)
;एक प्रकाशीय सूक्ष्मदर्शी द्वारा किसी निकाय की जाँच के लिए दृश्य - प्रकाश का उपयोग किया जाता है.
;Says Surat Police Commissioner V . K . Gupta : " We are forwarding the details of SIMI's US connection to the Union Government to be passed on to the FBI for further investigation in the US . "(Parallel corpus)
;सूरत के पुलिस आयुकंत वी . के . गुप्ता कहते हैं , ' ' हम सिमी के अमेरिकी संबंधों की जानकारी केंद्र को भेज रहे हैं ताकि वह जांच के लिए एफबीआइ को सूचना दे सके .
;Goel , whose shrill campaign ended single - digit lottery in the country , has demanded a CBI investigation into the matter .(Parallel corpus)
;गोयल के अभियान के बाद देश में एकांकीय लॅटरियों पर रोक लगी . अब उन्होंने इस मामले की सीबीआइ जांच की मांग की है .
(defrule investigation0
(declare (salience 0))
(id-root ?id investigation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  investigation.clp 	investigation0   "  ?id "  jAzca )" crlf))
)

;@@@ Added by 14anu-ban-06  (26-08-14)
;In Nepal , Nepal Force , Nepali Military Flight Service , Nepal Armed Security Force , Nepal Security Force , Nepal Armed Forest Guard , National Investigation Bureau , Guards and Secret Services .(Parallel corpus)
;नेपालमे नेपाली सेनानेपाली सैनिक विमान सेवानेपाल ससस्त्र प्रहरी बलनेपाल प्रहरीनेपाल ससस्त्र वनरक्षक तथा राष्ट्रीय अनुसन्धान विभाग नेपाल लगायत सस्सत्र तथा गुप्तचर सुरक्षा निकाय रहेहै ।
(defrule investigation1
(declare (salience 2000))
(id-root ?id investigation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-root ?id1 bureau|Bureau|center|centre|Center|Centre)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anusanXAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  investigation.clp 	investigation1   "  ?id "  anusanXAna )" crlf))
)

;@@@ Added by 14anu-ban-06  (26-08-14)
;The investigation on Quran .(Parallel corpus)
;कुरान पर शोध.
(defrule investigation2
(declare (salience 2500))
(id-root ?id investigation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI ?id ?id1)
(id-root ?id1 Quran|Bible|book)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SoXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  investigation.clp 	investigation2   "  ?id "  SoXa )" crlf))
)
