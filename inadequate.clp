;@@@ Added by 14anu-ban-06  (16-10-2014)
;With a tremendous backlog and multiplying transport needs , the progress made or envisaged must be considered utterly inadequate .(Parallel corpus)
;इतने अधिक शेष काम और बढ़ती हुई यातायात आवश़्यकताओं की दृष़्टि से जो भी प्ऱगति हुई अथवा निश़्चित की गई , वह बिल़्कुल ही अपर्याप्त थी .(Parallel corpus)
(defrule inadequate0
(declare (salience 0))
(id-root ?id inadequate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aparyApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inadequate.clp 	inadequate0   "  ?id "  aparyApwa )" crlf))
)

;@@@ Added by 14anu-ban-06  (16-10-2014)
;Classical physics is inadequate to handle this domain and Quantum Theory is currently accepted as the proper framework for explaining microscopic phenomena.(NCERT)
;चिरसम्मत भौतिकी इस प्रभाव क्षेत्र से व्यवहार करने में अक्षम है तथा हाल ही में क्वान्टम सिद्धान्त को ही सूक्ष्म परिघटनाओं की व्याख्या करने के लिए उचित ढाञ्चा माना गया है.(NCERT)
(defrule inadequate1
(declare (salience 2000))
(id-root ?id inadequate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root =(+ ?id 1) to)
;(to-infinitive =(+ ?id 1) ?)
;(id-root ?id1 handle|understand)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id akRama ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inadequate.clp 	inadequate1   "  ?id "  akRama )" crlf))
)
