;@@@ added by 14anu22
(defrule reinforce0
(declare (salience 3000))
(id-root ?id reinforce)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suxraDa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reinforce.clp	reinforce0	"  ?id "  suxraDa_kara )" crlf))
)

;@@@ added by 14anu22
(defrule reinforce1
(declare (salience 4000))
(id-root ?id reinforce)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 army)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 senA_ke_liye_sahAyawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng	" ?*prov_dir* "	reinforce.clp	reinforce1	" ?id " " ?id1 "  senA_ke_liye_sahAyawA_kara)" crlf))
)

