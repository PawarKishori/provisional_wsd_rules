
(defrule glance0
(declare (salience 5000))
(id-root ?id glance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id najara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glance.clp 	glance0   "  ?id "  najara )" crlf))
)

;"glance","N","1.najara"
;We got to see a glance of the superstar.
;
(defrule glance1
(declare (salience 4900))
(id-root ?id glance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id najara_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glance.clp 	glance1   "  ?id "  najara_dAla )" crlf))
)

;"glance","V","1.najara_dAlanA"
;He glanced at me.
;

;@@@ Added by 14anu-ban-05 on (14-11-2014)
;This proves the following result: when two equal masses undergo a glancing elastic collision with one of them at rest, after the collision, they will move at right angles to each other.[NCERT]
;isase sixXa howA hE ki jaba samAna xravyamAna ke xo piMda jinameM se eka sWira hE, pqRTasarpI prawyAsWa safGatta karawe hEM wo safGatta ke paScAw, xonoM eka-xUsare se samakoNa banAwe hue gawi kareMge.[NCERT]
(defrule glance2
(declare (salience 5000))
(Domain physics)
(id-root ?id glance)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id gerund_or_present_participle)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 collision)	;more constraints can be added
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (id-wsd_root_mng ?id pqRTasarpI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glance.clp 	glance2   "  ?id "  pqRTasarpI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  glance.clp      glance2   "  ?id "  physics )" crlf))
)

