;@@@ Added by Anita--05-08-2014
;I blush whenever I think about it. [cambridge dictionary]
;जब भी मैं इसके बारे में सोचता हूँ मैं शरमाता हूँ ।
;Whenever I go there they seem to be in bed. 
;जब भी मैं वहाँ जाता हूँ ऐसा लगता है वे बिस्तर में  होते हैं ।
;We can do it whenever we want .
;यह हम कभी भी कर सकते हैं ।
(defrule whenever0
(declare (salience 100))
(id-root ?id whenever)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-kriyA_viSeRaNa  ?kri ?id)(viSeRya-viSeRaka  ? ?id))
(id-cat_coarse ?id wh-adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaba_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " whenever.clp  whenever0 " ?id "  Jaba_BI )" crlf)) 
)

;@@@ Added by Anita--05-08-2014
;Do it in a spare moment at the weekend or whenever - it really does not matter. [cambridge dictionary]
;सप्ताहान्त या जब कभी भी खाली समय हो इसको करिये- वास्तव में इससे कोई फर्क नहीं पड़ता ।
(defrule whenever1
(declare (salience 200))
(id-root ?id whenever)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 matter)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id wh-adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaba_kaBI_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " whenever.clp  whenever1 " ?id "  Jaba_kaBI_BI )" crlf)) 
)

;@@@ Added by Anita--06-08-2014
;Whenever did Jane see a fortune teller? [cambridge dictionary]
; जैन एक भविष्य वक्ता को कब मिला था ?
(defrule whenever2
(declare (salience 300))
(id-root ?id whenever)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 see|find)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id wh-adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " whenever.clp  whenever2 " ?id "  kaba )" crlf)) 
)

;@@@ Added by Anita--06-08-2014
;It's not urgent—we can do it next week or whenever. [parse prob.] [oxford learner's dictionary]
;यह अत्यावश्यक नहीं है- यह हम अगले सप्ताह कर सकते हैं या कभी भी ।
(defrule whenever3
(declare (salience 400))
(id-root ?id whenever)
?mng <-(meaning_to_be_decided ?id)
(vAkya-vAkya_saMbanXI  ? ?id)
;(id-cat_coarse ?id wh-adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaBI_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " whenever.clp  whenever3 " ?id "  kaBI_BI )" crlf)) 
)

;############################default-rule##############################
;@@@ Added by Anita--13-08-2014
(defrule whenever_default-rule
(declare (salience 0))
(id-root ?id whenever)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jisa_samaya_BI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " whenever.clp  whenever_default-rule " ?id "  jisa_samaya_BI )" crlf)) 
)
