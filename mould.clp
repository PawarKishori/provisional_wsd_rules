
(defrule mould0
(declare (salience 5000))
(id-root ?id mould)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id moulding )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id gaDZawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mould.clp  	mould0   "  ?id "  gaDZawa )" crlf))
)

;"moulding","N","1.gaDZawa"
;See the protective mouldings round the car.
;
(defrule mould1
(declare (salience 4900))
(id-root ?id mould)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mould.clp 	mould1   "  ?id "  sAzcA )" crlf))
)

(defrule mould2
(declare (salience 4800))
(id-root ?id mould)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mould.clp 	mould2   "  ?id "  gaDZa )" crlf))
)

;"mould","V","1.gaDZanA"
;The bricks are moulded by hand.
;

;@@@Added by 14anu-ban-08 (14-04-2015)     ;Run on parser 8
;There's mould on the cheese.  [oald]
;पनीर पर फफूँदी लगी हैं.  [self]
(defrule mould3
(declare (salience 4904))
(id-root ?id mould)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI ?id ?id1)
(id-root ?id1 cheese|food|vegetable|fruit|bread)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PaPUzxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mould.clp 	mould3   "  ?id "  PaPUzxI )" crlf))
)

;@@@Added by 14anu-ban-08 (14-04-2015)
;She doesn’t fit the traditional mould of an academic.  [oald]
;वह वैज्ञानिक की सनातन शैली के योग्य नहीं हैं.  [self]
(defrule mould4
(declare (salience 4903))
(id-root ?id mould)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 academic)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanAwana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mould.clp 	mould4   "  ?id "  sanAwana )" crlf))
)
