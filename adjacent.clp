;@@@Added by14anu-ban-02(09-02-2015)
;Sentence: Adjacent strips are viewed in the next orbit, so that in effect the whole earth can be viewed strip by strip during the entire day.[ncert 11_08]
;Translation: संलग्न पट्टियों को अगली कक्षा में देखा जाता है ; इस प्रकार प्रभावी रूप में पूरे एक दिन में पट्टी दर पट्टी पूरी पृथ्वी का सर्वेक्षण किया जा सकता है.[ncert]
(defrule adjacent1 
(declare (salience 100)) 
(id-root ?id adjacent) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 strip) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id saMlagna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  adjacent.clp  adjacent1  "  ?id "  saMlagna )" crlf)) 
) 

;@@@Added by 14anu-ban-02(09-02-2015)
;There is a row of houses immediately adjacent to the factory.[oald]
;वहा़ँ फैक्टरी के ठीक बगल में घरों की कतार हैं.[manual]
(defrule adjacent2 
(declare (salience 100)) 
(id-root ?id adjacent) 
?mng <-(meaning_to_be_decided ?id) 
(id-word =(- ?id 1) immediately)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id bagala)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  adjacent.clp  adjacent2  "  ?id "  bagala )" crlf)) 
) 


;-------------------------- Default Rules ------------
;@@@Added by14anu-ban-02(09-02-2015)
;Sentence: The planes landed on adjacent runways.[oald]
;Translation: विमान सटे हुए दौडपथ पर उतरे .[anusaaraka]
(defrule adjacent0
(declare (salience 0))
(id-root ?id adjacent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id satA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  adjacent.clp  adjacent0  "  ?id "  satA_huA )" crlf))
)

