;@@@ Added by Shirisha Manju 29-4-14 Suggested by Chaitanya Sir
;The contribution of women in society is the highest.
;samAja meM swriyoM kA yogaxAna sabase aXika hE.
(defrule high00
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 contribution)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp      high00   "  ?id "  - )" crlf))
)


;$$$ Modified by Prachi Rathore(21-10-13)
;Meaning changed from aXika to UzcA and also salience is reduced.
 
;Modified by Meena(3.3.11) ; added (viSeRya-viSeRaNa ?id1 ?id) and deleted (samAsa_viSeRya-samAsa_viSeRaNa ?id2 ?id)
;Added by Meena(4.12.09)
;High income taxes are important .
;A high building.[CAMBRIDGE learnersdictionary]
;एक ऊँची इमारत . 
(defrule high0
(declare (salience 4700))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(or (samAsa_viSeRya-samAsa_viSeRaNa ?id2 ?id1)(viSeRya-viSeRaNa ?id1 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp      high0   "  ?id "  UzcA )" crlf))
)


;Added by Meena
;It is high time we updated our thinking on women 's issues .
(defrule high4
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
(id-root =(+ ?id 1) time|noon)
(viSeRya-viSeRaNa  =(+ ?id 1)  ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp    high4   "  ?id "  TIka )" crlf))
)



;@@@ Added by Prachi Rathore(21-10-13)
;Avoid foods that are high in salt.[CAMBRIDGE learnersdictionary]
;आहार को टालिए जिनमें नमक ज्यादा हैं . 
(defrule high5
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-in_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high5   "  ?id "  jyAxA )" crlf))
)

;$$$ Modified by 14anu04 on 28-June-2014
;@@@ Added by Prachi Rathore(21-10-13)
;The whole band seemed to be high on heroin.[CAMBRIDGE learnersdictionary]
;पूरा बैंड मादक पदार्थ पर धुत प्रतीत  होता है .
;He was high on alcohol.
;Old rule:वह मद्य पर उतेजित था. 
;Modified rule:वह मद्य के नशे में धुत था . 
(defrule high6
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI ?id ?id1)
(id-root ?id1 heroin|cocain|drink|drug|dope|alcohol|beer) ;added dope|alcohol|beer to the list by 14anu04
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naSe_meM_Xuwa))    ;Meaning changed from 'Xuwa' to 'naSe_meM_Xuwa' by 14anu04
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high6   "  ?id "  naSe_meM_Xuwa )" crlf))
)

;@@@ Added by Prachi Rathore(21-10-13)
;The coach is very high on this new player. [M-W learnersdictionary]
;प्रशिक्षक इस नये खिलाडी पर अत्यन्त उतेजित है . 
(defrule high7
(declare (salience 4900))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwejiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high7   "  ?id "  uwejiwa )" crlf))
)

;@@@ Added by Prachi Rathore
;The ball looped high up in the air.[oald]
;गोले ने वायु मेँ ऊँचाई पर फन्दा बनाया . 
(defrule high8
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA_viSeRaNa-kriyA_viSeRaNa_viSeRaka ?id1 ?id)
(id-root ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 UzcAI_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " high.clp	high8  "  ?id "  " ?id1 " UzcAI_para  )" crlf))
)

;$$$ Modified by 14anu-ban-06   (19-08-2014) added temperature|concentration|acidity|turnover to id-root
;added resistance by 14anu-ban-06 (07-11-2014)
;added 'dose' by 14anu-ban-06 (11-11-2014)
;added 'content' by 14anu-ban-06 (12-11-2014) 
;It consists of a U-tube containing a suitable liquid i.e. a low density liquid (such as oil) for measuring small pressure differences and a high density liquid (such as mercury) for large pressure differences.(NCERT);added by 14anu-ban-06 (14-11-2014)
;इस युक्ति में एक U आकार की नली होती है जिसमें उपयुक्त द्रव भरा होता है अर्थात् कम दाबान्तर मापने के लिए कम घनत्व का द्रव (जैसे तेल) तथा अधिक दाबान्तर के लिए अधिक घनत्व का द्रव (जैसे पारा) भरा जाता है.(NCERT)
;Many people have claimed soybeans in Asia were historically only used after a fermentation process, which lowers the high phytoestrogens content found in the raw plant.(agriculture) ;added by 14anu-ban-06 (12-11-2014)
;एशिया में बहुत से लोग दावा कर चुके हैं कि ऐतिहासिक रूप से सोयाबीनों को किण्वन प्रक्रिया के बाद ही उपयोग किया जाता है ,जो कच्चे पौधे में पाये जाने वाले phytoestrogens की  अधिक मात्रा को कम कर देता है | (manual)
;Results from some animal studies (and limited evidence from human subjects) suggest that the dose of peanuts is an important mediator of peanut sensitisation and tolerance; low doses tend to lead to sensitisation and higher doses tend to lead to tolerance.(agriculture)
;कुछ जानवरो के अध्ययन के परिमाण (और मानव विषयो से सीमित सबूत) सुझाव देते हैं कि मूंगफली की खुराक मूंगफली के संवेदीकरण और सहिष्णुता का एक महत्त्वपूर्ण  मध्यस्थ हैं,कम खुराक संवेदीकरण  का कारण बनती हैं  और अधिक खुराक सहिष्णुता  का कारण बनती हैं  .(manual)
;Most of the non-metals like glass, porcelain, plastic, nylon, wood offer high resistance to the passage of electricity through them.(NCERT)   ;added by 14anu-ban-06 (07-11-2014)
;kAzca, poYrselena, plAstika, noYyalona, lakadI jEsI aXikAMSa aXAwuez apane se hokara pravAhiwa hone vAlI vixyuwa para aXika prawiroXa lagAwI hEM.(NCERT-improvised)
;added 'pressure|precision' by 14anu-ban-06 (22-10-2014)
;For example, an air bubble in a liquid, would have higher pressure inside it ; See Fig 10.20 (b).(NCERT);added by 14anu-ban-06 (22-10-2014)
;उदाहरण के लिए, यदि किसी द्रव के भीतर कोई वायु का बुलबुला है, तो यह वायु का बुलबुला अधिक दाब पर होगा [ चित्र 10.20 (b) ] .(NCERT)
;Using instruments of higher precision, improving experimental techniques, etc., we can reduce the least count error.(NCERT);added by 14anu-ban-06 (22-10-2014)
;अधिक परिशुद्ध मापन यन्त्रों के प्रयोग करके, प्रायोगिक तकनीकों में सुधार, आदि के द्वारा, हम अल्पतमाङ्क त्रुटि को कम कर सकते हैं.(NCERT)
;The snow starts melting and the frozen water starts flowing in summers due to sun and high temperature .(Parallel Corpus)
;गर्मियों में सूर्य और अधिक तापमान के कारण बर्फ़ पिघलने लगती है और जमा हुआ पानी बहने लगता है ।
;In this scale , a low pH means high concentration of the hydrogen ion and high acidity .(Parallel corpus)
;इस पैमाने में , कम पी एच का अर्थ है हाइड्रोजन आयन की अधिक सांद्रता तथा अधिक अम्लीयता .
;And it's a system with very high turnover .(Parallel corpus)
;और यह प्रणाली है बहुत अधिक बिक्री युक्त है ।
;@@@ Added by Prachi Rathore[19-2-14]
;High income taxes are important .
;अधीक आय कर महत्त्वपूर्ण हैं . 
(defrule high9
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 tax|price|workload|load|temperature|concentration|acidity|turnover|pressure|precision|resistance|dose|content|liquid);added temperature|concentration|acidity|turnover by 14anu-ban-06 
;added 'liquid' by 14anu-ban-06 (14-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high9   "  ?id "  aXika )" crlf))
)

;@@@ Added by Sukhada[17-4-14]
;The income taxes are high .
;Aya kara aXika hE. 
;The workload is very high. 
;kAma_kA boJa bahuwa aXika hE.
(defrule high9s
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 tax|price|workload|load)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp     high9s   "  ?id "  aXika )" crlf))
)

;$$$ Modified by 14anu-ban-06 (22-08-2014)
;It gives high grades to non-fried fruits and vegetable. (COCA)
;yaha binA wale huye PaloM Ora sabjI ko acCI SreNiyAz xewA hE.
;@@@ Added by Prachi Rathore[19-2-14]
;She got very high marks in her geography exam.[cambridge]
;उसने उसकी भूगोल परीक्षा में बहुत अच्छे अङ्क प्राप्त किये है. 
(defrule high10
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 mark|grade);added grade by 14anu-ban-06
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high10   "  ?id "  acCA )" crlf))
)

;$$$ Modified by 14anu-ban-06  (19-08-2014) added perfomance to id-root
;When an individual sees that the effort she will make may lead to high performance.(COCA)
;jaba eka vyakwi xeKawA hE ki prayAsa use ucca kotI ke praxarSana ka mArga xiKAwa hE.
;@@@ Added by Prachi Rathore[19-2-14]
;She demands very high standards from the people who work for her.[cambridge]
;वह लोगों से बहुत उच्च कोटी के स्तर की माँग करती है जो उसके लिये काम करते हैं . 
(defrule high11
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 standard|performance);added 'performance' by 14anu-ban-06 (19-08-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ucca_kotI_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high11   "  ?id "  ucca_kotI_kA )" crlf))
)

;@@@ Added by Prachi Rathore[19-2-14]
;This meat is rather high - shall I throw it out?[cambridge]
;यह माँस काफी बदबूदार- क्या मैं इसे बाहर फेंक दूँ? 
(defrule high12
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 meat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxabUxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high12   "  ?id "  baxabUxAra )" crlf))
)

;@@@ Added by Prachi Rathore[19-2-14]
;High winds caused delays on the ferries.[cambridge]
;प्रचंड हवाएँ बडी नावों के विलम्ब का कारण हुईं . 
(defrule high13
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 wind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracaMda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high13   "  ?id "  pracaMda )" crlf))
)

;@@@ Added by Prachi Rathore[19-2-14]
;A high proportion of our staff are female.[oald]
;हमारे कर्मचारियों का एक बडा हिस्सा स्त्रियाँ हैं .  
(defrule high14
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 proportion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high14   "  ?id "  badA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (10-10-2014)
;A natural query that arises in our mind is the following: can we throw an object with such high initial speeds that it does not fall back to the earth?(NCERT)
;waba svABAvika rUpa se hamAre maswiRka meM yaha vicAra uwpanna howA hE kyA hama kisI piMda ko iwane weja AramBika cAla se Upara PeMka sakawe hEM ki vaha Pira pqWvI para vApasa na gire?(manual)
;@@@ Added by Prachi Rathore[6-3-14]
;He is listening music on high volume. [verified sentence]
;वह तेज आवाज पर सङ्गीत सुन रहा है . 
(defrule high15
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 volume|speed) ; added 'speed' by 14anu-ban-06 on (10-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high15   "  ?id "  weja )" crlf))
)

;@@@ Added by 14anu-ban-01 on (30-07-2014)
;Due to the deformity of arunshika there is high damage to the skin of the head .[Karan Singla]
;अरुंषिका की विकृति के कारण सिर की त्वचा को बहुत हानि पहुँचती है ।[Karan Singla]
(defrule high16
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 damage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high16   "  ?id "  bahuwa )" crlf))
)

;@@@ Added by 14anu-ban-06  (19-08-2014)
;Ahyaagatoan given the farewell if you are an experienced cave diver , in search of an adventure , chapada da diamantina should be high on your list .(Parallel corpus)
;अभ्यागतों की विदाई दी गई ।यदि आप साहसिक रोमांच की तलाश में एक अनुभवी गुहागोताखोर हैं तो चापडा डा डायमेंटाइना आपकी सूची में ऊपर होना चाहिए .
(defrule high17
(declare (salience 5100))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI ?id ?id1)
(id-root ?id1 list)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Upara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high17   "  ?id "  Upara )" crlf))
)

;@@@ Added by 14anu-ban-06 (12-11-2014)
;It has higher levels of protein, thiamine, riboflavin, phosphorus, calcium, and iron than wheat flour.(agriculture)
;गेहूं के आटे की अपेक्षा यह प्रोटीन,थइमिन,रिबोफ्लाविन,फास्फोरस,कैल्शियम और आयरन का उच्च स्तर है.(manual)
(defrule high18
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 level)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ucca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high18   "  ?id "  ucca )" crlf))
)

;@@@ Added by 14anu-ban-06 (19-11-2014)
;The clover cover crop enhanced songbird habitat by providing cover and nesting sites, and an increased food source from higher arthropod populations.(agriculture)
;तिपतिया झाडी वाली फसल गानेवाले पक्षी के प्राकृतिक वास को शरण और घोसले की जगहों को परिष्कृत करती/देती है और जो अधिक संधिपाद प्राणी की आबादी उनके खाद्य/आहार स्रोत  को बढाती हैं.(manual)
(defrule high19
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 population)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high19   "  ?id "  aXika )" crlf))
)


;@@@ Added by 14anu-ban-06 (03-12-2014)
;We know the unemployment rates are particularly high among young adults. (COCA)
;हम जानते हैं कि बेरोजगारी दरें युवाओ में विशेष रूप से ज्यादा हैं . (manual)
;Illiteracy rates are still high among women.(COCA)
;निरक्षरता दरें स्त्रियों में अभी भी ज्यादा हैं .(manual)
;Tax avoidance is also high among business owners.(COCA)
;कर के लिए टालमटोल उद्योग मालिकों में भी ज्यादा है .(manual)
(defrule high20
(declare (salience 5000))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-among_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp 	high20   "  ?id "  jyAxA )" crlf))
)
;----------------------------------- Default rules ----------------------------------
;Salience reduced by Meena(4.12.09)
(defrule high1
(declare (salience 0))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp     high1   "  ?id "  UzcA )" crlf))
)

; Changed from paxa_meM_UzcA to UzcA : Amba
;"high","Adj","1.paxa_meM_UzcA"
;jilAXISa kA paxa jile meM sabase'high' howA hE.
;
(defrule high2
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcAI_para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp     high2   "  ?id "  UzcAI_para )" crlf))
)

;"high","Adv","1.UzcAI_para"
;bADZa se bacane ke liye loga'high' Gara banAwe hE'

;Salience reduced by Meena(3.12.09)
(defrule high3
(declare (salience 0))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UzcA_sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  high.clp     high3   "  ?id "  UzcA_sWAna )" crlf))
)

;"high","N","1.UzcA_sWAna"
;Sera bAjAra Ajakala'high' nahIM hE.


;$$$ Modified by 14anu-ban-06 (03-12-2014)
;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
;It was , therefore , no surprise that the industry's cost of production was high , quality poor and productivity of operatives low .(parallel corpus)   ;added by 14anu-ban-06 (03-12-2014)
;अतएव यह कोई आश़्चर्यजनक बात नहीं थी कि उद्योग का लागत मूल़्य अधिक , गुणवत्ता घटिया और उत़्पादनशीलता बहुत कम थी .(parallel corpus)
(defrule sub_samA_high9
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 tax|price|workload|load|cost)     ;added 'cost' by 14anu-ban-06 (03-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   sub_samA_high9   "   ?id " aXika )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_high9
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 tax|price|workload|load)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   obj_samA_high9   "   ?id " aXika )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_high10
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 mark)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   sub_samA_high10   "   ?id " acCA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_high10
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 mark)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   obj_samA_high10   "   ?id " acCA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_high11
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 standard)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ucca_kotI_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   sub_samA_high11   "   ?id " ucca_kotI_kA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_high11
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 standard)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ucca_kotI_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   obj_samA_high11   "   ?id " ucca_kotI_kA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_high13
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 wind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracaMda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   sub_samA_high13   "   ?id " pracaMda )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_high13
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 wind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pracaMda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   obj_samA_high13   "   ?id " pracaMda )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_high14
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 proportion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   sub_samA_high14   "   ?id " badA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_high14
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 proportion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   obj_samA_high14   "   ?id " badA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_high15
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 volume)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   sub_samA_high15   "   ?id " weja )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_high15
(declare (salience 5050))
(id-root ?id high)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 volume)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id weja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " high.clp   obj_samA_high15   "   ?id " weja )" crlf))
)


