
(defrule alternate0
(declare (salience 5000))
(id-root ?id alternate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ekAnwarawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  alternate.clp 	alternate0   "  ?id "  ekAnwarawa )" crlf))
)

;"alternate","Adj","1.ekAnwarawa"
;The maid servant comes on alternate days.
;
(defrule alternate1
(declare (salience 4900))
(id-root ?id alternate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArI-bArI_se_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  alternate.clp 	alternate1   "  ?id "  bArI-bArI_se_kara )" crlf))
)

;"alternate","V","1.bArI-bArI_se_karanA"
;Most farmers alternate their crops.
;

;@@@ Added by 14anu-ban-02 (19-11-2014)
;Related to crop destruction is alternate, low-price use of agricultural products.[agriculture]
;कम कीमत प्रयोग ,फसल विनाश के लिए  सम्बन्धित कृषि उत्पादों का विकलप है .[manual]
(defrule alternate2
(declare (salience 5000))
(id-root ?id alternate)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikalpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  alternate.clp 	alternate2   "  ?id "  vikalpa )" crlf))
)

;@@@ Added by 14anu-ban-02 (20-11-2014)
;Crop rotation also mitigates the build-up of pathogens and pests that often occurs when one species is continuously cropped, and can also improve soil structure and fertility by alternating deep-rooted and shallow-rooted plants.[agriculture]
;सस्यावर्तन  रोगाणुओं और कीटों की वृद्धि को भी कम करता है जो अक्सर घटित होता है जब एक प्रजाति  फसल की लगातार पैदावार  होती है ,और  मृदा  संरचना और उपजाऊपन को भी बारी-बारी से   गहरे-जड़वत् और सतही-जड़वत्  पौधों को  लगाने से  सुधार सकता है ।[manual]
(defrule alternate3
(declare (salience 4900))
(id-root ?id alternate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArI-bArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  alternate.clp 	alternate3   "  ?id "  bArI-bArI )" crlf))
)


