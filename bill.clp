;@@@ Added bt 14anu23 on 25/6/14
;Let's ask for the bill.
;चलिये हम बिल के लिए कहें . 
(defrule bill0
(declare (salience 5000))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bila ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp bill0 " ?id " bila )" crlf)) 
)

;$$$Modified by 14anu-ban-02(29-01-2015)
;meaning changed from 'bEMka_nota' to 'nota'
;A ten-dollar bill.
;एक दस डॉलर का  नोट .(manual)
;A ten-dollar bill.
;एक दस डॉलर का  बैंक नोट .(manual)
;@@@ Added bt 14anu23 on 25/6/14
;A ten-dollar bill.
;एक ten-dollar बैंक नोट . 
(defrule bill1
(declare (salience 5000))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1) ;added ?id1 by 14anu-ban-02(29-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nota))	
(assert (id-wsd_viBakwi ?id1 kA));Added by 14anu-ban-02(29-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  bill.clp 	bill1   "  ?id1 " kA)" crlf);Added by 14anu-ban-02(29-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp  bill1 " ?id "  nota )" crlf)) 
)

;$$$Modified by 14anu-ban-02(12-02-2015)
;###[COUNTER STATEMENT]###They complained about the size of their gas bill.[oald]
;###[COUNTER STATEMENT]###उन्होंने उनके गैस के बिल की कुल राशि के बारे में शिकायत की . [self]
;@@@ Added bt 14anu23 on 25/6/14
;The Education Reform Bill.
;शिक्षा सुधार विधेयक .
(defrule bill2
(declare (salience 5000))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 Reform)	;added by 14anu-ban-02(12-02-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viXeyaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp  bill2 " ?id "  viXeyaka )" crlf)) 
)

;@@@ Added bt 14anu23 on 25/6/14
; A good bill at the movies.
;चलचित्रों पर एक अच्छा कार्यक्रम . 
(defrule bill3
(declare (salience 5000))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-object ? ?id)(viSeRya-at_saMbanXI  ?id ?))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAryakrama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp  bill3 " ?id "  kAryakrama )" crlf)) 
)


;@@@ Added bt 14anu23 on 25/6/14
;He has been hard at work bill posting in a poster and sticker campaign.
;वह बहुत मेहनत से  पोस्टर और स्टीकर अभियान की लेखा पत्र के काम  में लगा हुअा  है.
; वह बहुत मेहनत से  पोस्टर और स्टीकर में posting अभियान की लेखा पत्र के काम  में लगा हुअा  है.
(defrule bill4
(declare (salience 5000))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-at_saMbanXI ?id1 ?id) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id leKA_pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp  bill4 " ?id "  leKA_pawra )" crlf)) 
)

;$$$Modified by 14anu-ban-02(29-01-2015)
;@@@ Added bt 14anu23 on 25/6/14
;The gull held the fish in its bill.
;एक प्रकार की समुद्री पक्षी ने  अपनी चोंच में मछलियों को पकडा . 
(defrule bill5
(declare (salience 5000))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id1 ?id)	;added ?id1 by 14anu-ban-02(29-01-2015)
(id-root ?id1 hold)		;added by 14anu-ban-02(29-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id coMca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp  bill5 " ?id "  coMca )" crlf)) 
)


;$$$Modified by 14anu-ban-02(29-01-2015)
;To bill the play for two weeks.
;खेल अगले सप्ताह से घोषित करने के लिये . (manual)
;@@@ Added bt 14anu23 on 25/6/14
;To bill the play for two weeks.
;खेल अगले सप्ताह से घोषित किया गया है . 
(defrule bill6
(declare (salience 5000))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-karma  ?id ?)		;commented by 14anu-ban-02(29-01-2015)
(to-infinitive  ?id1 ?id)	;Added by 14anu-ban-02(29-01-2015) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GoRiwa_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp  bill6 " ?id " GoRiwa_kara  )" crlf)) 
)

;$$$Modified by 14anu-ban-02(29-01-2015)
;We shall be billing them for the damage caused.
;हमें उन्हें नुकसान उठाने के लिये बिल भेजना चाहिये.
;@@@ Added bt 14anu23 on 25/6/14
;We shall be billing them for the damage caused.
;हम कारण बनी हुई हानि के लिए उनको बिल तैयार करते रहेंगे . 

(defrule bill7
(declare (salience 5000))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)	;added ?id1 by 14anu-ban-02(29-01-2015)
(id-root ?id1 damage)		;added by 14anu-ban-02(29-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bila_Beja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp  bill7 " ?id " bila_Beja  )" crlf)) 
)

;$$$Modified by 14anu-ban-02(29-01-2015)
;We billed 2000rs.(manual)
;हमने 2000rs का बिल तैयार किया .(manual)
;@@@ Added bt 14anu23 on 25/6/14
;We billed Â£400,000. 
;हमने £ 400,000 बिल तैयार किया . 
(defrule bill8
(declare (salience 0))
(id-root ?id bill)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bila_wEyAra_kara))
(assert (kriyA_id-object_viBakwi ?id kA));added by 14anu-ban-02(29-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  bill.clp 	bill8   "  ?id " kA )" crlf);added by 14anu-ban-02(29-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " bill.clp  bill8 " ?id " bila_wEyAra_kara  )" crlf)) 
)
