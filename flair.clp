;@@@ Added by 14anu-ban-05 on (16-03-2015)
;He has a flair for languages. [OALD]
;उसके पास भाषाओं की  विशिष्ट योग्यता है .	[manual]

(defrule flair1
(declare (salience 101))
(id-root ?id flair)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta_yogyawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flair.clp 	flair1   "  ?id "  viSiRta_yogyawA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (16-03-2015)
;It's a competent enough piece of writing but it lacks flair.[CALD]
;यह लिखने की एक सक्षम  कलाकृति है परन्तु इसमें  अंतः प्रेरणा की कमी होना है . [manual]

(defrule flair2
(declare (salience 102))
(id-root ?id flair)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 lack) 		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMwaH_preraNA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flair.clp 	flair2   "  ?id "  aMwaH_preraNA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-05 on (16-03-2015)
;He played with great imagination and flair. [CALD]
;वह ऊँची कल्पना और स्वभाव के साथ खेला.            [manual]

(defrule flair0
(declare (salience 100))
(id-root ?id flair)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svaBAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  flair.clp    flair0   "  ?id "  svaBAva )" crlf))
)

