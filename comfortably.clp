;@@@ Added by 14anu-ban-03 (16-03-2015)
;All the rooms were comfortably furnished. [oald]
;सभी कमरे आरामदायक ढङ्ग से सुसज्जित थे . [manual]
(defrule comfortably1
(declare (salience 10))   
(id-root ?id comfortably)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka ?id1 ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAmaxAyaka_DaMga_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comfortably.clp 	comfortably1   "  ?id "  ArAmaxAyaka_DaMga_se )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (16-03-2015)
;He can comfortably afford the extra expense. [oald]
;वह अधिक  लागत  का आराम से खर्च उठा सकता है . [manual]
(defrule comfortably0
(declare (salience 00))
(id-root ?id comfortably)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  comfortably.clp      comfortably0   "  ?id "  ArAma_se )" crlf))
)

