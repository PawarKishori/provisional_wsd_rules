
;@@@ Added by 14anu04 on 2-July-2014
; After ten years travelling the world, she felt it was time to put down roots somewhere. 
;दस वर्षों तक विश्व यात्रा करने के बाद, उसने महसूस किया अब कहीं बसने का समय था . 
(defrule root_tmp
(declare (salience 5100))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) down)
(id-word =(- ?id 2) put)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2) basa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " root.clp	root_tmp  "  ?id "  " =(- ?id 1) =(- ?id 2) "  basa  )" crlf))
)

;@@@ Added by 14anu04 0n 2-July-2014
;We need to root out corruption at all levels. 
;हमें सब स्तरों में भ्रष्टाचार को जड से उखाडने की जरूरत है . 
(defrule root_tmp2
(declare (salience 5000))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jadZa_se_uKAdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " root.clp	root_tmp2  "  ?id "  " ?id1 "  jadZa_se_uKAdZa  )" crlf))
)

;@@@ Added by 14anu04 0n 2-July-2014
;We're rooting for you. 
;हम आपको प्रोत्साहित कर रहे हैं . 
(defrule root_tmp3
(declare (salience 5000))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prowsAhiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  root.clp 	root_tmp3   "  ?id "  prowsAhiwa_kara )" crlf))
)


;@@@ Added by Anita--9.7.2014
; Every member of society had to come forward to have complete control over it, because corruption now has roots that are so ;deep that it is no longer possible for the government acting alone to uncover it and stop it. [news-dev]
;इस पर नकेल कसने के लिए समाज के प्रत्येक व्यक्ति को आगे आना होगा, क्योंकि भ्रष्टाचार की जड़ इतनी गहरी हो चुकी है कि इसे उखाड़कर फेंकना सिर्फ सरकार के लिए ;संभव नहीं रह गया है।
(defrule root2
(declare (salience 5200))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-jo_samAnAXikaraNa  ?id ?samA)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  root.clp 	root2   "  ?id "  jadZa )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (14-01-2015)
;@@@ Added by 14anu21 on 28.06.2014
;We have to get to the root of the problem.[oxford]
;हमें समस्या के मूल के लिए प्राप्त करना है . (Translation before adding rule)
;हमें समस्या की जड़ के लिए प्राप्त करना है .(Then added rule: get83)
;हमें समस्या की जड़ तक पहुँचना है . 
(defrule root3
(declare (salience 5100))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 tree|grass|problem|trouble|shrub|herb)          ;removed plant by 14anu-ban-10 on (14-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  root.clp 	root3   "  ?id "  jada )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (14-01-2015)
;@@@ Added by 14anu21 on 28.06.2014
;I pulled the plant up by the roots. [oxford]
;मैंने मूल के पास वनस्पति ऊपर उठा ली . (Translation before adding the rule)
;मैंने जड़ों के पास वनस्पति ऊपर उठा ली . (Then modified rule:by25 )
;मैंने जडों से वनस्पति ऊपर उठा ली .(Rule pull6 was getting fired)
;(Then added rule:pull37 ) 
;;मैंने जडों से पौधा उखाडा .
;and modified rule plant1 to change वनस्पति to पौधा
(defrule root4
(declare (salience 5100))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-by_saMbanXI  ?idverb ?id)
;(kriyA-by_saMbanXI  ?idverb ?id)	;commented out by 14anu-ban-10 on (14-01-2015)
;(kriyA-object  ?idverb ?id1) 		;commented out by 14anu-ban-10 on (14-01-2015)
;(id-root ?id1 plant|tree|grass|problem|trouble|shrub|herb) ;commented out by 14anu-ban-10 on (14-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  root.clp 	root4   "  ?id "  jada )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (14-01-2015)
;@@@ Added by 14anu-ban-10 on (06-12-2014)
;The grubs bore into the wood of trees or sometimes into the roots or pith of herbaceous plants.[agriculture domain]
;सूण्डी जन्म लेते है पेडों की लकडी में या कभी कभी औषधेय पौधों के मूल  या मज्जा मे.[manual]
(defrule root5
(declare (salience 5200))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ? ?id) 	;added by 14anu-ban-10 on (14-01-2015)
;(viSeRya-det_viSeRaNa ?id ?  )	;commented out by 14anu-ban-10 on (14-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  root.clp 	root5   "  ?id "  mUla)" crlf))
)

;@@@ Added by 14anu-ban-10 on (16-02-2015)
;Modern communication has its roots in the 19 th and 20 th century in the work of scientists like J.C. Bose, F.B. Morse, G. Marconi and Alexander Graham Bell.[ncert corpus]
;AXunika saFcAra kI jadeM 19 vIM waWA 20 vIM SawAbxiyoM meM sara jagaxISa canxra bosa, ePa.bI. morsa, jI mArkonI waWA alekjeMdara grAhma bela ke kArya xvArA dAlI gaIM.[ncert corpus]
(defrule root6
(declare (salience 5300))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id )
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 communication)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jadeM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  root.clp 	root6   "  ?id "  jadeM)" crlf))
)


;--------------------- Default Rules -------------------

(defrule root0
(declare (salience 5000))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  root.clp 	root0   "  ?id "  mUla )" crlf))
)

(defrule root1
(declare (salience 4900))
(id-root ?id root)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWApiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  root.clp 	root1   "  ?id "  sWApiwa_kara )" crlf))
)


;"root","VT","1.sWApiwa_kara"
;She always roots the tender saplings very carefully. 
;
;LEVEL 
;
;
;Headword : root
;
;Examples --
;
;"root","N"
;
;--1.jadZa
;roots of some trees are very strong.
;kuCa pedZoM kI jadZe bahuwa majabUwa howI hEM.
;
;--2.waha
;i would like to go to roots of this case .                             
;mEM isa mAmale ke waha waka pahuzcanA cAhawA hUz.
;
;--3.mUla
;ram is the root cause for this incident.
;isa GatanA kA mUla kAraNa rAma hEM.
;
;--4.kanxamUla(jisakI jadZa KAI jAwI hEM)
;some roots are good for health.
;kuCa kanxamUla svAsWya ke lie lABaxAyaka howe hEM.
;
;--5.XAwu
;The root of the word 'killed' is 'kill'
;'killed' Sabxa kI XAwu 'kill' hE
;
;--6.AXAra
;Is there any root for this statement?
;kyA isa vakwavya kA koI AXAra hEM.
;
;yahAz"root"Sabxa kA prayoga alaga alaga saMxarBo meM alaga huA hEM,lekina ina saBI
;vAkyoM meM eka sAmAnya arWa sAmane A rahA hEM.yahAz para inakA arWa AwA hE
;mUla me,yAni kisI bAwa meM,yA kisI kA mUla yA AXAra kyA hEM?isalie sUwra
;ke rUpa meM "mUla"ko leMgeM.
;
;sAre vAkyo kI purnaracanA,
;
;--kuCa pedZoM kI jadZe kAPI majabUwa howI hEM.
;--mEM isa mAmale ke mUla waka pahuzcanA cAhawA hUz.     --- [_jadZa]
;--isa GatanA kA mUla kAraNa rAma hEM.                 <--jadZa]
;--kuCa kanxamUla svAsWya ke lalie lABaxAyaka howe hEM. <--kuCa jadZe jo KAI jAwI hE]
;--kyA isa vakwavya kA koI AXAra hEM. <--mUla
;--kyA isa vakwavya kA koI mUla kAraNa hEM. <--jadZa]
;
;ina vAkyoM ke AXAra para sUwra banegA 
;
;sUwra : jadZa-mUla
;
;
;
;
