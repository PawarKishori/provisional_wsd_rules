;@@@ Added by 14anu-ban-06 (16-12-2014)
;The king has matters of great import to discuss with his counselors. (COCA)
;राजा के पास अपने सलाहकारो के साथ विचार-विमर्श करने के लिए अधिक महत्व के विषय  हैं . (manual)
;We always talk about issues of great import .(COCA)
;हम हमेशा अधिक महत्व के विषयों के बारे में बातचीत करते हैं .(manual) 
(defrule import2
(declare (salience 5100))
(id-root ?id import)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 matter|issue)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahawva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  import.clp 	import2   "  ?id "  mahawva )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-12-2014)
; It is difficult to understand the full import of this statement.(OALD)
; इस कथन का पूरा तात्पर्य समझना मुश्किल है . (manual)
(defrule import3
(declare (salience 5100))
(id-root ?id import)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 statement)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAwparya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  import.clp 	import3   "  ?id "  wAwparya )" crlf))
)

;---------------------------Default rules ---------------------
;$$$ Modified by 14anu-ban-06 (03-09-2014) Changed meaning from AyAwa_kara to AyAwa 
;Imports stopped and the demand increased .(parallel corpus)
;आयात रूक गया और मांग बढ़ गयी .
(defrule import0
(declare (salience 5000))
(id-root ?id import)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AyAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  import.clp 	import0   "  ?id "  AyAwa )" crlf))
)

(defrule import1
(declare (salience 4900))
(id-root ?id import)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AyAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  import.clp 	import1   "  ?id "  AyAwa_kara )" crlf))
)

;"import","V","1.AyAwa karanA"
;Technical equipments imported from abroad.
;
;
