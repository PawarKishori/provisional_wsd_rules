;@@@Added by 14anu-ban-08 (13-04-2015)
;Critics accused him of murdering the English language.  [oald]
;आलोचक ने उसपर अग्रेजी भाषा को बिगाड़ देने का आरोप लगाया.  [self]
(defrule murder2
(declare (salience 100))
(id-root ?id murder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-of_saMbanXI ?id1 ?id)
(id-root ?id1 accuse)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bigAdZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  murder.clp    murder2   "  ?id " bigAdZa_xe)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-08 (13-04-2015)
;He was found guilty of murder.  [oald]
;वह हत्या का अपराधी पाया गया.  [self]
(defrule murder0
(declare (salience 0))
(id-root ?id murder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hawyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  murder.clp    murder0   "  ?id " hawyA)" crlf))
)

;@@@Added by 14anu-ban-08 (13-04-2015)
;The murdered woman was well known in the area.  [oald]
;मारी गयी औरत शहर में जानी-मानी जाती थी.  [self]
(defrule murder1
(declare (salience 0))
(id-root ?id murder)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  murder.clp    murder1   "  ?id " mArA_jA)" crlf))
)

