;@@@ Added by 14anu-ban-10 on (11-03-2015)
;The defence of the realm.[oald]
;देश की रक्षा . [manual] 
(defrule realm1
(declare (salience 200))
(id-root ?id realm)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ? ?id)
;(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realm.clp 	realm1   "  ?id "  xeSa)" crlf))
)

;@@@ Added by 14anu-ban-10 on (11-03-2015)
;Zamindars realm is nowhere now in India.[oald]
;जामीनदार भारत  के किसी भी राज्य मे नहीं है  अब . [manual]
(defrule realm2
(declare (salience 300))
(id-root ?id realm)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ? )
;(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  rAjya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realm.clp 	realm2   "  ?id "   rAjya)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (11-03-2015)
;In the realm of literature.[oald]
 ;साहित्य के  क्षेत्र मै .[manual]
(defrule realm0
(declare (salience 100))
(id-root ?id realm)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRewra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  realm.clp    realm0   "  ?id "  kRewra)" crlf))
)

