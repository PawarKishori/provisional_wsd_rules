;@@@ Added by 14anu-ban-06 (20-04-2015)
;I have an illegitimate child. (COCA)
;मेरे एक नाजायज बच्चा है . (manual)
(defrule illegitimate1
(declare (salience 2000))
(id-root ?id illegitimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 child)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAjAyaja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illegitimate.clp 	illegitimate1   "  ?id "  nAjAyaja )" crlf)
)
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (20-04-2015)
;Illegitimate use of company property. (OALD)
;कम्पनी की सम्पत्ति का अवैध उपयोग .(manual) 
(defrule illegitimate0
(declare (salience 0))
(id-root ?id illegitimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avEXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  illegitimate.clp 	illegitimate0   "  ?id "  avEXa )" crlf))
)
