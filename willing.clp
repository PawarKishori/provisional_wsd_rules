

;@@@ Added by 14anu-ban-11 on (23-03-2015)
;I am willing to give it a shot. (oald)
;मैं उसको दृश्य देकर उत्सुक हूँ . (self)
(defrule willing0
(declare (salience 00))
(id-root ?id willing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsuka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  willing.clp      willing0   "  ?id "  uwsuka)" crlf))
)


;@@@ Added by 14anu-ban-11 on (23-03-2015)
;They keep a list of people willing to work nights. (oald)
;उन्होने लोगों की सूची रखी जो रातों मे कार्य करने  को राजी  हैं .(self)
(defrule willing1
(declare (salience 10))
(id-root ?id willing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 night)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAjZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  willing.clp      willing1   "  ?id "  rAjZI)" crlf))
)


