;##############################################################################
;#  Copyright (C) 2013-2014 Prachi Rathore (prachirathore02 at gmail dot com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;The key to success is preparation. [oald]
; तैयारी सफलता की कुंजी है . 

(defrule key0
(declare (salience 5000))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 question|success|solve)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuMjI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key0   "  ?id "  kuMjI )" crlf))
)

;Check your answers in the key at the back of the book.[OALD]
;पुस्तक के पीछे कुंजी में आपके उत्तर जाँचें . 
(defrule key1
(declare (salience 5000))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-in_saMbanXI  ?id1 ?id)
(kriyA-object  ?id1 ?id2)
(id-root ?id2 answer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuMjI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key1   "  ?id " kuMjI )" crlf))
)

;The song changes key halfway through. [cambridge]
; गाना  बीच मे सुर बदलता है . 

(defrule key2
(declare (salience 5000))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 song|minor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key2   "  ?id "  sura )" crlf))
)

;@@@ Added by Prachi Rathore (06-12-13)
;A sonata in the key of E flat major.  [oald]
; वाद्य सङ्गीत रचना E flat major सुर में.
(defrule key5
(declare (salience 5000))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 song|sonata|piece)
(viSeRya-in_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sura))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key5   "  ?id "  sura )" crlf))
)


;@@@ Added by (14anu11)avni
;That is , the musical scale is divided into twelve equal steps ( seven white and five black keys ) .
;वह यह है कि संगीत का सप्तक बारह ' बराबर ' चरणों में बांट दिया जाता है ( सात सफेद और पांच काली चाबियां ) जबकि भारतीय संगीत में सुरों की बारीकियां समझाने के लिए 
;अनगिनत  क़ुँजिय की  आवश्यकता है 
(defrule key6
(declare (salience 5000))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?id1 )
(viSeRya-viSeRaNa  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuMjI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key6   "  ?id "  kuMjI )" crlf))
)

;@@@Added by 14anu-ban-07,(14-03-2015)
;  She dangled her car keys nervously as she spoke.(oald)
;उसने  अपनी गाड़ी की चाबियाँ हिलाईं जब वह घबराते हुए बोली . (manual)
(defrule key7
(declare (salience 4800))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
(id-root ?id1 car|house|office)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAbI))
(assert  (id-wsd_viBakwi ?id1 kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key7   "  ?id "  kI_cAbI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  key.clp      key7   "  ?id1 " kI )" crlf))
)

;@@@Added by 14anu-ban-07,(16-03-2015)
;The classes are keyed to the needs of advanced students.(oald)(parser problem)
;कक्षाएँ प्रयोजनीय होती  हैं उच्च कक्षाओ के विद्यार्थियों की जरूरत के लिए . (manual)
(defrule key8
(declare (salience 4900))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 to)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prayojanIya_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key8   "  ?id "  prayojanIya_ho )" crlf))
)

;xxxxxxxxxxxxxxxxDefault Rulexxxxxxxxxxxxxxx
;A bunch of keys. [OALD]
;चाबियों का गुच्छा . 
(defrule key3
(declare (salience 4700))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAbI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key3   "  ?id "  cAbI )" crlf))
)

(defrule key4
(declare (salience 4700))
(id-root ?id key)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramuKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  key.clp 	key4   "  ?id "  pramuKa )" crlf))
)
