;@@@ Added by 14anu-ban-01 on (24-02-2015)
;The sullen looks, the pain,the scars on older women's sagging breasts and aging men's sunken chests, are haunting.[coca]
;वे उदास निगाहें, दर्द,बूढ़ी स्त्रियों के ढीली-ढाली छातियों एवं बूढ़े होते हुए पुरुषों की  धँसी हुई/सिकुडी हुई छातियों पर पड़े हुए दाग/निशान  बार बार याद आते हैं .[self]
(defrule sunken1
(declare (salience 4000))
(id-root ?id sunken )
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 chest|garden|eye)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XazsA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  sunken.clp  	sunken1 "  ?id "   XazsA_huA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-02-2015)
;His eyes were dark and sunken.[oald]
;उसकी आँखें गहरी और धँसी हुई थीं . [self]
(defrule sunken2
(declare (salience 4100))
(id-root ?id sunken )
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 eye)
(subject-subject_samAnAXikaraNa  ?id1 ?id2)
(id-root ?id3 and)
(conjunction-components  ?id3 ?id2 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XazsA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  sunken.clp  	sunken2 "  ?id "   XazsA_huA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-01 on (24-02-2015)
;They found a sunken ship.[self: with reference to oald]
;उनको एक जलमग्न जहाज मिला . [self]
(defrule sunken0
(declare (salience 0))
(id-root ?id sunken )
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalamagna/dUbA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  sunken.clp    sunken0 "  ?id "   jalamagna/dUbA_huA )" crlf))
)

