;$$$ Modified by 14anu-ban-04 (04-03-2015)            ------meaning changed from 'tAla_matola_kara'   to ' tAla'
;For two weeks they evaded the press.                          [oald]
;उन्होंने दो सप्ताह के लिए  प्रैस को टाला .                                   [self]
;"evade","VT","1.tAla_matola_karanA/tAlanA"
;Party leader  managed to  evade all difficult questions.
(defrule evade1
(declare (salience 4900))
(id-root ?id evade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tAla))
(assert (kriyA_id-object_viBakwi ?id ko))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  evade.clp     evade1   "  ?id " ko  )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  evade.clp 	evade1   "  ?id "  tAla )" crlf))
)


;@@@ Added by 14anu-ban-04 (04-03-2015)                          
;The answer evaded him.                        [oald]
;उसको उत्तर  समझ में नहीं आया .                        [self]
(defrule evade2
(declare (salience 200))                 
(id-root ?id evade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)                               
(id-root ?id1 answer)                                 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa_meM_nahIM_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  evade.clp 	evade2   "  ?id "  samaJa_meM_nahIM_A )" crlf))
)


;-----------------------------------------------------------DEFAULT RULE-------------------------------------------------------------------

;"evade","V","1.baca_nikalanA"
(defrule evade0
(declare (salience 100))                   ;salience reduced from '5000' to '100'   by 14anu-ban-04 (04-03-2015) 
(id-root ?id evade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baca_nikala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  evade.clp 	evade0   "  ?id "  baca_nikala )" crlf))
)


