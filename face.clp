
(defrule face0
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id facing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sAmanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face0   "  ?id "  sAmanA )" crlf))
)

;"facing","N","1.sAmanA"
;He is afraid of facing the facts.
;
;
(defrule face1
(declare (salience 4900))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kisI_kaTina_parisWiwi_kA_sAmanA_karanA_yA_svIkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " face.clp	face1  "  ?id "  " ?id1 "  kisI_kaTina_parisWiwi_kA_sAmanA_karanA_yA_svIkAra  )" crlf))
)



;Added by Meena(29.3.11)
;When you stand on this rock and face the east, the waves of the bay of bengal lap your feet.

(defrule face3
(declare (salience 4800))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 east|west|north|south)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI_ora))
(assert (id-wsd_root_mng ?id muzha_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  face.clp      face3   "  ?id "  muzha_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  face.clp      face3   "  ?id " kI_ora )" crlf))
)




;$$$ Modified by Bhagyashri Kulkarni (29-10-2016)
;In the button hole surgery the patient faces much less pain after operation. (health)
;बटन होल सर्जरी में मरीज शल्य चिकित्सा के बाद कुछ कम दर्द का सामना करता है . 
;Added by Meena(24.02.10)
;The people of Orissa are facing grave adversities due to the cyclone .
;ओडिशा के लोग तूफान के कारण गम्भीर कठिनाइयों का सामना कर रहे हैं . ;translation by Bhagyashri
(defrule face4
(declare (salience 4700))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 pain|adversity) ;added by Bhagyashri
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id sAmanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  face.clp      face4   "  ?id "  sAmanA_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  face.clp      face4   "  ?id " kA )" crlf))
)

;Added by Pramila(Banasthali University) on 24-10-2013
;A hexagon has six faces.
;eka RatakoNa ke CaH Palaka howe hEM.
;As seen from the figure, only the two faces 1 and 2 will contribute to the flux; electric field lines are parallel to 
;the other faces and they, therefore, do not contribute to the total flux.
;जैसा चित्र से दृष्टिगोचर होता है , केवल दो फलक 1 तथा 2 ही फ्लक्स में योगदान देङ्गे ; विद्युत क्षेत्र रेखाएँ अन्य फलकों के समान्तर हैं और वे इसीलिए कुल फ्लक्स में योगदान नहीं देतीं .
(defrule face5
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-to_saMbanXI  ?id1 ?id)(viSeRya-saMKyA_viSeRaNa  ?id ?id1))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Palaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face5   "  ?id "  Palaka )" crlf))
)

;Added by Pramila(Banasthali University) on 24-10-2013
;With India's defeat in the world cup match the captain lost his face.
;viSvakapa meM hAra kara BArawIya kapwAna ne apanI prawiRTA Ko xI hE.
(defrule face6
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(kriyA-with_saMbanXI  ?id1 ?id2)
(id-root ?id2 defeat)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prawiRTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face6   "  ?id "  prawiRTA )" crlf))
)

;Added by Pramila(Banasthali University) on 24-10-2013
;The south face of the mountain was very steep.
;pahAdZa kA xakRiNI aBimuKa ekaxama KadZI caDZAI vAlA WA.
(defrule face7
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aBimuKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face7   "  ?id "  aBimuKa )" crlf))
)

;Added by Pramila(Banasthali University) on 24-10-2013
;In this exercise, the gymnast should face the floor.
;isa vyAyAma meM kasarawa karanevAle ko apanA muKa jZamIna kI ora raKanA howA hE.
(defrule face8
;(declare (salience 0))
(declare (salience 4500)) ; Salience reduced by Bhagyashri from '5000' to '4500'
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora_muKa_karanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  face.clp 	face8   "  ?id "  kI_ora_muKa_karanA )" crlf))
)

;added by Pramila(Banasthali University) on 28-10-2013
;The changing face of Britain.  ;oald
;britena ka baxalawA huA rUpa.
(defrule face9
(declare (salience 4900))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  face.clp 	face9   "  ?id "  rUpa)" crlf))
)

;added by pramila(Banasthali University) on 28-10-2013
;The birds build their nests in the rock face. ;oald  
;pakSI apanA GOsalA cattAnoM kI ora banAwe hEM.
(defrule face10
(declare (salience 4800))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  face.clp 	face10   "  ?id "  ora )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-11-2014)
;To represent a vector, we use a bold face type in this book.[NCERT]
;saxiSa ko vyakwa karane ke lie isa puswaka meM hama mote akRaroM kA prayoga kareMge .[NCERT]
;Since bold face is difficult to produce, when written by hand, a vector is often represented by an arrow placed over a letter.[NCERT]
;paranwu hAWa se liKawe samaya kyoMki mote akRaroM kA liKanA WodA muSkila howA hE, isalie eka saxiSa ko akRara ke Upara wIra lagAkara vyakwa karawe hEM.[NCERT]
(defrule face13
(declare (salience 5000))
(Domain physics)
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) bold)
=>
(retract ?mng)
(assert (id-domain_type  ?id physics))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) mote_akRara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " face.clp  face13  "  ?id "  " (- ?id 1) "  mote_akRara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  face.clp       face13   "  ?id "  physics )" crlf))
)


;$$$ Modified by 14anu-ban-05 on (01-04-2015)  ;changed meaning from 'cehre' to 'cehrA'
;@@@ Added by 14anu11
;Science has two faces like Janus : science has its destructive side and a constructive , creative side .
;जैनस नामक ग्रीक देवता की तरह विज्ञान के भी दो चेहरे हैं . एक उसका विनाशकारी पहलू है तो दूसरा रचनात्मक या सृजनात्मक पहलू है .
(defrule face14
(declare (salience 6000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(or (viSeRya-like_saMbanXI  ?id ?id2)(viSeRya-saMKyA_viSeRaNa  ?id ?id1))	;added 'or' by 14anu-ban-05 on (01-04-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id cehrA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face14   "  ?id "  cehrA )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 23.06.2014 email-id:sahni.gourav0123@gmail.com
;Which direction are you facing?
;आप कौनसा दिशा की ओर मुँह कर? 
;किस दिशा कि अोर आपने मुँह किया है?  ;translation modified by 14anu-ban-05 on (15-01-2015)
(defrule face15
(declare (salience 5000))
(id-word ?id facing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 direction)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id muzha_kara))
(assert (kriyA_id-object_viBakwi ?id kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face15   "  ?id "  muzha_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  face.clp      face15   "  ?id " kI_ora )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-12-2014)
;However, if two equal and opposite deforming forces are applied parallel to the cross-sectional area of the cylinder, as shown in Fig. 9.2(b), there is relative displacement between the opposite faces of the cylinder.[NCERT]
;yaxi xo barAbara Ora viroXI virUpaka bala belana ke anuprasWa paricCexa ke samAnwara lagAe jAez, jEsA ciwra 9.2(@b) meM xiKAyA gayA hE, wo belana ke sammuKa PalakoM ke bIca sApekRa visWApana ho jAwA hE.[NCERT]
(defrule face16
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) opposite)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1)  sammuKa_Palaka ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " face.clp  face16  "  ?id "  " (- ?id 1) "   sammuKa_Palaka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-12-2014)
;The strain so produced is known as shearing strain and it is defined as the ratio of relative displacement of the faces Δx to the length of the cylinder L.[NCERT]
;isa prakAra uwpanna vikqwi ko aparUpaNa vikqwi kahawe hEM Ora ise PalakoM ke sApekRa visWApana Δ@x waWA belana kI lambAI @L ke anupAwa se pariBARiwa karawe hEM.[NCERT]
(defrule face17
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id2 ?id)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id1 displacement) ;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id Palaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face17   "  ?id "  Palaka )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-12-2014)
;The field lines remain continuous, emerging from one face of the solenoid and entering into the other face.[NCERT]
;kRewra reKAez sanwawa banI rahawI hEM, eka sire se bAhara nikalawI hEM Ora xUsare sire se anxara praveSa karawI hEM.[NCERT]
(defrule face18
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (kriyA-from_saMbanXI  ? ?id)(kriyA-into_saMbanXI  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sirA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face18   "  ?id "  sirA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-12-2014)
;In that case, the South-pole due to the induced current will face the approaching North-pole of the magnet.[NCERT]
;usa xaSA meM, preriwa XArA ke kAraNa xakRiNI Xruva pAsa Awe hue cumbaka ke uwwarI Xruva kI ora hogA.[NCERT]
;To counter this decrease in magnetic flux, the induced current in the coil flows in clockwise direction and its South-pole faces the receding North-pole of the bar magnet.[NCERT]
;cumbakIya Plaksa ke isa Gatane kA viroXa karane ke lie kuNdalI meM preriwa XArA xakRiNAvarwa xiSA meM bahawI hE waWA isakA xakRiNI Xruva xUra hatawe xaNda cumbaka ke uwwarI Xruva kI ora howA hE.[NCERT]
(defrule face19
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 North-pole)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ho))
(assert (kriyA_id-object_viBakwi ?id kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face19   "  ?id "  ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  face.clp      face19   "  ?id " kI_ora )" crlf))
)

;@@@ Added by 14anu-ban-05 on (09-12-2014)
;Welders wear special glass goggles or face masks with glass windows to protect their eyes from large amount of UV produced by welding arcs.[NCERT]
;veldiMga karane vAle loga, veldiMga cinagAriyoM se nikalane vAlI UV kiraNoM se apanI AzKoM kI surakRA ke lie viSiRta kAzca yukwa XUpa ke caSme pahanawe hEM yA kAzca kI KidakiyoM se yukwa muKOte apane cehare para lagAwe hEM.[NCERT]
(defrule face20
(declare (salience 5000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) mask)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  muKOtA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " face.clp  face20  "  ?id "  " (+ ?id 1) "   muKOtA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (01-04-2015)
;His face was bloodless with fear.[mw]
;उसका चेहरा भय से  पीला पड़ गया था . [self]
(defrule face21
(declare (salience 5001))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-RaRTI_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ceharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  face.clp  	face21   "  ?id "  ceharA )" crlf))
)



;--------------------------------default rules----------------------------------

;Many people find it hard to face up to the fact that they are getting old.
;bahuwa se loga isa kaTina parisWiwi kA sAmanA karane se darawe hEM ki vaha bUDZe ho rahe hEM
(defrule face11
(declare (salience 4000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ceharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  face.clp 	face11   "  ?id "  ceharA )" crlf))
)
;Salience reduced by Meena(24.02.10)
(defrule face12
(declare (salience 0))
;(declare (salience 4000))
(id-root ?id face)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  face.clp 	face12   "  ?id "  sAmanA_kara )" crlf))
)

;default_sense && category=verb	sAmanA-honA/kara	0
;"face","V","1.sAmanA-honA/karanA"
;In this exercise, the gymnast should face the floor
;--"2.sAmanA_honA"
;He was faced with a new problem in this period of crisis.
;
;LEVEL 
;Headword : face
;
;Examples --
;                   
;"face","N","1.ceharA"
;He washed his face with soap
;usane sAbuna se apanA ceharA XoyA.
;--"2.prawiRTA"
;With India's defeat in the world cup match the captain lost his face.
;viSvakapa meM hAra kara BArawIya kapwAna ne apanI prawiRTA Ko xI hE.
;--"3.aBimuKa"
;The south face of the mountain was very steep.
;pahAdZa kA xakRiNI aBimuKa ekaxama KadZI caDZAI vAlA WA.
;--"4.Palaka"
;A hexagon has six faces.
;eka RatakoNa ke CaH Palaka howe hEM.
;
;"face","V","1.sAmanA-honA[karanA]"
;He faced the calamity quite bravely.
;usane vipawwi kA sAmanA kAPI bahAxurI se kiyA.
;--"2.kI_ora_muKa_karanA"
;In this exercise, the gymnast should face the floor.
;isa vyAyAma meM kasarawa karanevAle ko apanA muKa jZamIna kI ora raKanA howA hE.
;--"3.muzha_xiKAnA" 
;He had to face his father after his misconduct
;usako apane isa bure vyavahAra ke bAxa apane piwA ko BI muzha xiKAnA WA.
;
;"face-lift","N","1.sOnxaryavarXana"<-- cehare_kA_sOnxarya_baDZAnA
;She looks beautiful because she's had a face-lift
;vaha sunxara lagawI hE kyoMkiM usane cehare para suXAra kI
;
;"face value","N","1.prawyakRa_mUlya"-muKa_muxrA
;
;vyAKyA : uparyukwa saMjFA va kriyA vAkyoM kA "muKa/ceharA mUlArWa hE"
;muKyawayA kriyA vAkya meM "sAmane_honA/AnA" kA BAva hE. awaH isakA sUwra hogA -
;
;sUwra : ceharA^sAmanA
;                                       
