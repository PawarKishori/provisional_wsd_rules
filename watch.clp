
(defrule watch0
(declare (salience 5000))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-over_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOkasI_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " watch.clp watch0 " ?id "  cOkasI_kara )" crlf)) 
)

(defrule watch1
(declare (salience 4900))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cOkasI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " watch.clp	watch1  "  ?id "  " ?id1 "  cOkasI_kara  )" crlf))
)

(defrule watch2
(declare (salience 4800))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  watch.clp 	watch2   "  ?id "  GadZI )" crlf))
)

;"watch","VTI","1.xeKanA"
;The man watched the children crossing the lane
(defrule watch3
(declare (salience 4700))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  watch.clp 	watch3   "  ?id "  xeKa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  watch.clp     watch3   "  ?id " ko )" crlf)
)
)

;@@@ Added by 14anu21 on 25.06.2014
;I will keep watch while you go through his papers.
;मैं जब कि आप उसके पेपरों में से जाते हैं घडी रखूँगा .(Translation before adding rule watch4)
;मैं नज़र रखूँगा जब तक कि आप उसके काग़ज़ देखेंगे.
;While you do this I will keep watch at the gate. 
;जब कि आप यह करते हैं मैं द्वार में घडी रखूँगा .(Translation before adding rule watch4)
;जब कि आप यह करते हैं मैं द्वार में नजर रखूँगा . (modified rule at10 to change में to पर)
;जब कि आप यह करते हैं मैं द्वार पर नजर रखूँगा .

(defrule watch4
(declare (salience 4850))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object =(- ?id 1) ?id)
(id-root =(- ?id 1) keep)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng (- ?id 1) ?id najaZra_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  watch.clp     watch4   "  (-  ?id 1) " " ?id " najaZra_raKa )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;The government is keeping a tight watch on how the situation develops. 
;सरकार  विकसित स्थिति पर कड़ी निगरानी कैसे रखे हुए है.
(defrule watch5
(declare (salience 4900))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ? ?id)
(viSeRya-det_viSeRaNa ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nigarAnI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  watch.clp 	watch5   "  ?id "  nigarAnI )" crlf)
)
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 26.06.2014 email-id:sahni.gourav0123@gmail.com
;I go on watch in an hour. 
;मैं एक घण्टे में चौकीदारी पर जाता हूँ . 
(defrule watch6
(declare (salience 4900))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOkIxArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  watch.clp 	watch6   "  ?id "  cOkIxArI )" crlf)
)
)

;@@@Added by 14anu18
;Watch out for the the card frauds.
;कार्ड छल का ध्यान रखिए . 
(defrule watch20
(declare (salience 4800))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) XyAna_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " watch.clp	watch20  "  ?id "  " (+ ?id 1 )"  XyAna_raKa  )" crlf))
)

;@@@Added by 14anu18 
;This movie’s an engrossing watch.
;यह चलचित्र तल्लीन कर लेती हुई है . 
(defrule watch23
(declare (salience 4800))
(id-root ?id watch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 movie|play|act|show|series)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  watch.clp 	watch23   "  ?id "  - )" crlf))
)


