;@@@ Added by 14anu-ban-04 (14-04-2015)
;The daffodils were dancing in the breeze.                [cald]
;डैफोडिल मन्द समीर में लहरा रहे थे .                                  [self]
(defrule dance2
(declare (salience 4910))
(id-root ?id dance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(kriyA-in_saMbanXI ?id ?id2)
(id-root ?id2 breeze|water|air)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laharA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dance.clp 	dance2   "  ?id "  laharA)" crlf))
)


;$$$ Modified by 14anu-ban-04 (16-03-2015)           ------- changed meaning from 'nqwya' to 'nAca'
;Dance master Prabhudeva has given divorce to his wife.            [Anusaaraka-Agama team obervation]
;नृत्य शिक्षक प्रभुदेवा ने अपनी पत्नी को तलाक दिया.                        [self]
;"dance","N","1.nAca/nqwya"
;It was evident that the dancer enjoyed the dance as much as the audience.
(defrule dance0
(declare (salience 5000))
(id-root ?id dance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nqwya/nAca))                         
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dance.clp 	dance0   "  ?id "   nqwya/nAca )" crlf))
)                                                         

;"dance","VI","1.nqwya_karanA/nAcanA"
;Lets dance && sing && make merry.
(defrule dance1
(declare (salience 4900))
(id-root ?id dance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nqwya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dance.clp 	dance1   "  ?id "  nqwya_kara )" crlf))
)

