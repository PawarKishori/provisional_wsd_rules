
(defrule prejudice0
(declare (salience 5000))
(id-root ?id prejudice)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id prejudiced )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pUrvAgrAhI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  prejudice.clp  	prejudice0   "  ?id "  pUrvAgrAhI )" crlf))
)

;"prejudiced","Adj","1.pUrvAgrAhI"
;He came to me alleging that I was prejudiced against him.
;
(defrule prejudice1
(declare (salience 4900))
(id-root ?id prejudice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrvAgraha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prejudice.clp 	prejudice1   "  ?id "  pUrvAgraha )" crlf))
)

;"prejudice","N","1.pUrvAgraha"
;Henry has strong prejudice against john.
;
;

;@@@ Added by 14anu23 on 1/7/14
;Their decision was based on ignorance and prejudice.   [oxfordlearnersdictionaries.com]
;उनका निर्णय अज्ञानता  और पक्षपात पर आधारित था . 
(defrule prejudice3
(declare (salience 4900))
(id-root ?id prejudice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-on_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRapAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prejudice.clp 	prejudice3   "  ?id "  pakRapAwa )" crlf))
)

;@@@ Added by 14anu23 on 1/7/14   
;There is little prejudice against workers from other EU states.   [oxfordlearnersdictionaries.com]
;यूरोपीय संघ के अन्य राज्यों के कार्यकर्ताओं के खिलाफ थोड़ा पक्षपात   है.
(defrule prejudice4
(declare (salience 4900))
(id-root ?id prejudice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-against_saMbanXI  ?id ?id1)
(id-root ?id1 nation|worker|country|blacks|whites|fast-food|people|race)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRapAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prejudice.clp 	prejudice4   "  ?id "  pakRapAwa )" crlf))
)


;@@@ Added by 14anu23 on 1/7/14
;A victim of racial prejudice   [oxfordlearnersdictionaries.com]
;प्रजातीय पक्षपात का शिकार . 
(defrule prejudice5
(declare (salience 4900))
(id-root ?id prejudice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root =(- ?id 1) racial|religious|sexual)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakRapAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prejudice.clp 	prejudice5   "  ?id "  pakRapAwa )" crlf))
)


;@@@ Added by 14anu23 on 1/7/14
;Many people have a prejudice against goat's milk due to its peculiar flavour .    [oxfordlearnersdictionaries.com]
;बकरी के दूध की विशेष गन्ध के कारण अनेक लोगों में इसके प्रति अरुचि होती है .
(defrule prejudice2
(declare (salience 4900))
(id-root ?id prejudice)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-against_saMbanXI  ?id ?id1)
(id-root ?id1 goat|milk|tea|coffee|beverage|shakes|beer|alcohol|wine|rum)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aruci))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  prejudice.clp 	prejudice2   "  ?id "  aruci )" crlf))
)

