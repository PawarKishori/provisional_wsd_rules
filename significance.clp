;########################################################################

;#  Copyright (C) 2013-2014 Jagrati Singh (singh.jagriti5@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################

;...Default Rule...
;$$$ Modified by 14anu-ban-01 on 20-08-14:changed the meaning of default rule from 'mawalaba' to 'mahawva' .
;We shall first see what the center of mass of a system of particles is and then discuss its significance.[NCERT corpus]
;पहले हम यह देखेङ्गे कि  कणों के निकाय का द्रव्यमान केन्द्र क्या है और फिर इसके महत्व पर प्रकाश डालेङ्गे. [NCERT corpus:added " कणों के निकाय का"]
;@@@ Added by jagriti(4.4.2014)
;I didn't understand the significance of his remarks until later. [oald]
;मैं बाद  तक भी उसकी टिप्पणियों का मतलब नहीं समझा पाया . 
(defrule significance1
(declare (salience 100));reduced to 0 from 100 by 14anu-ban-01.
(id-root ?id significance)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mahawva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* " significance.clp  	significance1   "  ?id "   mahawva)" crlf))
)


;$$$ Modified by 14anu-ban-01 on 20-08-14:changed the meaning of rule from  'mahawva'  to 'mawalaba' .
;I didn't understand the significance of his remarks until later. [oald]
;मैं बाद  तक भी उसकी टिप्पणियों का मतलब नहीं समझ पाया . [improvised]
;@@@ Added by jagriti(4.4.2014)
;According to the Hindu religious book, buying metallic things during the Dhanteras festival has a special significance.
;शास्त्रों में धनतेरस के पर्व पर धातु की चीजें खरीदने का विशेष महत्व है। 
(defrule significance0
(declare (salience 5000))
(id-root ?id significance)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaNa  ?id ?id1) :commented
(viSeRya-of_saMbanXI  ?id ?id1);added by 14anu-ban-01 on 20-08-14
(id-root ?id1 remark)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mawalaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* " significance.clp  	significance0   "  ?id "   mawalaba )" crlf))
)

