;@@@ Added by 14anu-ban-06 (16-04-2015)
;A hypnotic state.(OALD)
;सम्मोहक अवस्था.(manual)
;She went into a hypnotic trance.(cambridge)
;वह सम्मोहक भाव समाधि में चली गई . (manual)
(defrule hypnotic1
(declare (salience 2000))
(id-root ?id hypnotic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 state|trance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammohaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hypnotic.clp 	hypnotic1   "  ?id "  sammohaka )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (16-04-2015)
;His voice had an almost hypnotic effect.(OALD) 
;उसकी आवाज में लगभग निद्राजनक प्रभाव था . (manual)
(defrule hypnotic0
(declare (salience 0))
(id-root ?id hypnotic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nixrAjanaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hypnotic.clp 	hypnotic0   "  ?id "  nixrAjanaka )" crlf))
)
