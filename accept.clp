
;@@@ Added by 14anu-ban-02(12-08-14)
;We shall not justify this fact, but we shall accept it.[NCERT]
;हम इस तथ्य के समर्थन या पुष्टि के लिए कोई तर्क नहीं देङ्गे, बस यह स्वीकार करेंगे.[ncert] 
(defrule accept0
(declare (salience 0))
(id-root ?id accept)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svIkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accept.clp      accept0   "  ?id "  svIkAra_kara )" crlf))
)

;@@@ Added by 14anu-ban-02(12-08-14)
;###[COUNTER EXAMPLE]### The wave theory was not readily accepted primarily because of Newton's authority.[ncert 12_10]
;मुख्यतः न्यूटन के प्रभाव के कारण तरङ्ग सिद्धान्त को सहज ही स्वीकार नहीं किया गया.[ncert]
;Measurement of any physical quantity involves comparison with a certain basic, arbitrarily chosen, internationally accepted reference standard called unit.  [NCERT CORPUS]
;किसी भौतिक राशि का मापन, एक निश्चित, आधारभूत, यादृच्छिक रूप से चुने गए मान्यताप्राप्त, सन्दर्भ - मानक से इस राशि की तुलना करना है ; यह सन्दर्भ - मानक मात्रक कहलाता है.[NCERT CORPUS]
;The system of units which is at present internationally accepted for measurement is the Système Internationale d' Unites (French for International System of Units), abbreviated as SI.[NCERT CORPUS]
;आजकल अन्तर्राष्ट्रीय स्तर पर मान्यताप्राप्त प्रणाली " सिस्टम इन्टरनेशनल डि यूनिट्स" है (जो फ्रेञ्च भाषा में "मात्रकों की अन्तर्राष्ट्रीय प्रणाली" कहना है) ; इसे सङ्केताक्षर में SI लिखा जाता है.[NCERT CORPUS]

(defrule accept1
(declare (salience 10))
(id-root ?id accept)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 internationally)	;added by 14anu-ban-02(05-02-2015) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnyawA_prApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accept.clp      accept1   "  ?id "  mAnyawA_prApwa )" crlf))
)


;@@@ Added by 14anu-ban-02(12-08-14)
;Classical physics is inadequate to handle this domain and Quantum Theory is currently accepted as the proper framework for explaining microscopic phenomena.[NCERT CORPUS]
;चिरसम्मत भौतिकी इस प्रभाव क्षेत्र से व्यवहार करने में सक्षम नहीं है तथा हाल ही में क्वान्टम सिद्धान्त को ही सूक्ष्म परिघटनाओं की व्याख्या करने के लिए उचित ढाञ्चा माना गया है.[NCERT CORPUS]

(defrule accept2
(declare (salience 20))
(id-root ?id accept)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  accept.clp      accept2   "  ?id "  mAnA )" crlf))
)
