;@@@ Added by 14anu-ban-06 (07-04-2015)
;A list of the fifteen most heavily indebted nations.(OALD)
;पन्द्रह सबसे अधिक ऋणग्रस्त राष्ट्रों की सूची . (manual)
(defrule indebted1
(declare (salience 2000))
(id-root ?id indebted)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 nation|country)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id qNagraswa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indebted.clp 	indebted1   "  ?id "  qNagraswa )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (07-04-2015)
;We're deeply indebted to you for your help.(cambridge)
;हम आपकी सहायता के लिए आपके बहुत अधिक आभारी हैं . (manual)
(defrule indebted0
(declare (salience 0))
(id-root ?id indebted)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ABArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indebted.clp 	indebted0   "  ?id "  ABArI )" crlf))
)
