
;$$$ Modified by 14anu-ban-11 on (06-04-2015)
;Soft iron is one such material. (ncert 12_05)
;नरम लोहा एक ऐसा पदार्थ है . (self)
(defrule soft2
(declare (salience 5010))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id) ;added by 14anu-ban-11 on (06-04-2015)
(id-root ?id1 iron) ;added by 14anu-ban-11 on (06-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id narama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft2   "  ?id "  narama )" crlf))
)


;@@@ Added by 14anu-ban-11 on (06-04-2015)
;The grass was soft and springy.(oald)
;घास मुलायम और लचीली थी . (self)
(defrule soft4
(declare (salience 5001))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 grass)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mulAyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft4  "  ?id "  mulAyama)" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-04-2015)
;This season's fashions focus on warm tones and soft lines.(oald) 
;इस ऋतु के फैशन  चमकीले  रंग और सीधी लाइनों पर केंद्रित  हैं .  (self)
(defrule soft5
(declare (salience 5002))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 line)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft5   "  ?id "  sIXI)" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-04-2015)
;The soft glow of candlelight.(oald)
;मोमबत्ती की हलकी रोशनी  . (self)
(defrule soft6
(declare (salience 5003))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 glow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halakI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft6   "  ?id "  halakI)" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-04-2015)
;If you're too soft with these kids they'll never respect you.(oald)
;यदि आप इन बच्चों के साथ ज्यादा ही विनम्र  हैं तो वे आपका कभी सम्मान नहीं करेंगे . (self)
(defrule soft7
(declare (salience 5004))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-with_saMbanXI  ?id ?id1)
(id-root ?id1 kid)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinamra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft7   "  ?id "  vinamra)" crlf))
)

;@@@ Added by 14anu-ban-11 on (06-04-2015)
;He must be going soft in the head.(oald)
;वह दिमाग से मन्द होता जा रहा है . (self)
(defrule soft8
(declare (salience 5005))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-in_saMbanXI  ?id ?id1)
(id-root ?id1 head)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft8   "  ?id "  maMxa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (06-04-2015)
;You won't need much soap—the water here is very soft.(oald)
;आपको अधिक साबुन की आवश्यकता नहीं है  —यहां का पानी  बहुत स्वच्छ है . (self)
(defrule soft9
(declare (salience 5006))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
(id-root ?id1 very)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svacCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft9   "  ?id "  svacCa)" crlf))
)

;------------------------ Default Rules ----------------------

(defrule soft0
(declare (salience 5000))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id komala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft0   "  ?id "  komala )" crlf))
)

(defrule soft1
(declare (salience 4900))
(id-root ?id soft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mqxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  soft.clp 	soft1   "  ?id "  mqxu )" crlf))
)


;"soft","Adj","1.narama"
;Small children like to play with soft things.
;--"2.sAxA"
;People, who have dark complexion should wear soft colours.
;--"3.maXura"
;Old melodies are soft to listen.
;--"4.kamajZora"
;One should not be of a soft character.
;
;
