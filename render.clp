;@@@ Added by June 2014 workshop participants under Aditi Mam and Soma Mam guidance (27-06-14)
;They were rendered homeless by the flood.
;bADa kI vajaha se ve beGara ho gaye.
(defrule render0
(declare (salience 100))
(id-root ?id render)
?mng<-(meaning_to_be_decided ?id)
(root-verbchunk-tam-chunkids render ? is_en|are_en|were_en|was_en|am_en|has_been_en|have_been_en|had_been_en|will_be_en|would_be_en|could_be_en|may_be_en|must_be_en $? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  render.clp 	render0   "  ?id "  ho )" crlf))
)

;@@@ Added by June 2014 workshop participants under Aditi Mam and Soma Mam guidance (27-06-14)
;Yovraj's back injury rendered him unfit.
;युवराज की पीठ की चोट ने उसको अयोग्य कर दिया . 
(defrule render1
(declare (salience 100))
(id-root ?id render)
?mng<-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?sub)
(id-root ?sub injury)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  render.clp 	render1   "  ?id "  kara_xe )" crlf)
)
)

;@@@ Added by June 2014 workshop participants under Aditi Mam and Soma Mam guidance (27-06-14)
;They rendered assistance to the disaster victims. 
;उन्होंने आपदा पीडितों को सहायता दी .
(defrule render_default
(id-root ?id render)
?mng<-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  render.clp 	render_default   "  ?id "  xe )" crlf))
)

