;@@@Added by 14anu-ban-10 on (17-03-2015)
;Their stage act is a little too raunchy for television.[oald]
;उनका मञ्च प्रदर्शन  टेलिविजन के लिए थोडा ज्यादा ही अश्लील है . [manual]
(defrule raunchy1
(declare (salience 200))
(id-root ?id raunchy)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-for_saMbanXI  ?id ? )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aSlIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  raunchy.clp  raunchy1  "  ?id "  aSlIla)" crlf))
)

;@@@Added by 14anu-ban-10 on (17-03-2015)
;A raunchy old man.[oald]
;एक मैला-कुचैला बुढ्ढा आदमी . [manual]
(defrule raunchy2
(declare (salience 300))
(id-root ?id raunchy)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka  ? ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  mElA_kucElA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  raunchy.clp  raunchy2  "  ?id "   mElA_kucElA)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-10 on (17-03-2015)
;That film was fairly raunchy.[oald]
;वह सिनेमा काफी उत्तेजक था . [manual]
(defrule raunchy0
(declare (salience 100))
(id-root ?id raunchy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwejaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  raunchy.clp  raunchy0  "  ?id "  uwwejaka)" crlf))
)

