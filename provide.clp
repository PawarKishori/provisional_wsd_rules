;Commented by 14anu02 on 24.06.14 since its completely wrong and not needed too.
;"provided","Conj","1.baSarwe_ki"
;I will present you cycle provided (that) you would pass in 1st class.
;
;(defrule provide0
;(declare (salience 5000))
;(id-root ?id provide)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 shelter)
;(kriyA-shelter_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ASraya_xe));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " provide.clp provide0 " ?id "  ASraya_xe )" crlf)) 
;)

(defrule provide1
(declare (salience 4900))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 shelter)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ASraya_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " provide.clp	provide1  "  ?id "  " ?id1 "  ASraya_xe  )" crlf))
)

(defrule provide2
(declare (salience 4800))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id provided )
(id-cat_coarse ?id conjunction)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id baSarwe_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  provide.clp  	provide2   "  ?id "  baSarwe_ki )" crlf))
)

;$$$ MOdified by 14anu-ban-09 on (21-01-2015)
;Enchanting natural topography of this park , the dense forest here and several rivers and water sources provide a place worth visit . [Tourism_Corpus]					;added 'river' by 14anu-ban-09 on (21-01-2015)
;इस पार्क की विस्मयपूर्ण प्राकृतिक दिशा  , यहाँ के घने जंगल तथा बहुत-सी नदियाँ एवं जल स्रोत एक दर्शनीय स्थल का प्रदर्शन प्रदान करते हैं . [Manual]			;added by 14anu-ban-09 on (21-01-2015)		
;इस पार्क की विस्मयपूर्ण प्राकृतिक स्थलाकृति , यहाँ के घने जंगल तथा बहुत-सी नदियाँ एवं जल स्रोत देखने/घुमने योग्य एक दर्शनीय स्थल प्रदान करते हैं .	 [Self]			;added by 14anu-ban-09 on (21-01-2015)
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;This booklet provides useful information about local services.
;यह किताब स्थानीय सेवाओं के बारे में उपयोगी जानकारी प्रदान करती है.
(defrule provide3
(declare (salience 4700))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 book|booklet|novel|notebook|river) 		;added 'river' by 14anu-ban-09 on (21-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide3   "  ?id "  praxAna_kara )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;Section 17 provides that all decisions must be circulated in writing.
;सेक्शन १७ यह क़ानूनी अधिकार देता है कि सारे निर्णय लिखितरूप में सबको भेजें जाए .
(defrule provide4
(declare (salience 4600))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 section|Affidavit|appeal|bar|case|court|judge|judgement|jury|legal|law|megistrate|petition|prosecutor)
(kriyA-subject  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAnUnI_aXikAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide4   "  ?id "  kAnUnI_aXikAra )" crlf))
)


;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;He has a wife and two young children to provide for.
;उसके ऊपर अपनी पत्नी और दो बच्चों का दायित्व है.
(defrule provide5
(declare (salience 4500))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(and(kriyA-prayojya_karwA  ?id ?)(kriyA-preraka_kriyA  ?id ?)(kriyA-vAkyakarma  ? ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAyiwva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide5   "  ?id "  xAyiwva )" crlf))
)

;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;We must provide for depreciation when calculating the costs.
;कीमतों को आंकते समय हमें अवमूल्यन के बारे में भी योजना बनानी चाहिए .
(defrule provide6
(declare (salience 5500))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-samakAlika_kriyA  ?id ?)
(kriyA-for_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yojanA_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide6   "  ?id "  yojanA_banA )" crlf))
)

;@@@Added by Sonam Gupta MTech IT Banasthali 2013
;This pressure is available, however, provided the star is not too massive. 
;इस दबाव का असर तभी पड़ता है यदि तारा बहुत अधिक विशाल न हो।
(defrule provide7
(declare (salience 4300))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yaxi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide7   "  ?id "  yaxi )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 24-1-2014
;The new regulation had provided for 'moot courts' for training of Law students. [Gyannidhi]
;नये नियमों के अंतर्गत कानून के छात्रों के प्रशिक्षण के लिए कानून सभाओं की व्यवस्था की गई थी।
(defrule provide8
(declare (salience 5000))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavasWA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide8   "  ?id "  vyavasWA_kara )" crlf))
)

(defrule provide9
(declare (salience 4200))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide9   "  ?id "  xe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (09-10-2014)
;Today there is a lack of such people in in country who can provide leadership to the society . [Tourism Corpus] ;added by 14anu-ban-09 on (05-12-2014)
;आज देश में ऐसे लोगों का अभाव है जो समाज को नेतृत्व प्रदान कर सकें . [Tourism Corpus] ;added by 14anu-ban-09 on (05-12-2014)
;Biological controls—Natural biological processes and materials can provide control, with acceptable environmental impact, and often at lower cost. [Agriculture] ;added by 14anu-ban-09 on (02-12-2014)
;जैविक नियंत्रण-प्रकृतिक जैविक प्रक्रिया और  पदार्थ  सन्तोषजनक  पर्यावरिक  प्रभाव के साथ अक्सर  कम लागत में नियंत्रण प्रदान कर सकते हैं . [Manual] ;added by 14anu-ban-09 on (02-12-2014)
;The conservation laws can still provide useful results. [NCERT CORPUS]
;waWApi saMrakRaNa niyama EsI parisWiwiyoM meM BI upayogI pariNAma praxAna kara sakawe hEM. [NCERT CORPUS]

(defrule provide10
(declare (salience 4600))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 result|control|leadership) ;added 'control' by 14anu-ban-09 on (02-12-2014) ;added 'leadership' by 14anu-ban-09 on (05-12-2014)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide10   "  ?id "  praxAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (10-10-2014)
;This centripetal force is provided by the gravitational force. [NCERT CORPUS]
;aBikenxra bala guruwvAkarRaNa bala xvArA praxAna kiyA jAwA hE. [NCERT CORPUS]

(defrule provide11
(declare (salience 4600))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 force)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide11   "  ?id "  praxAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-10-2014)
;Generally a large margin of safety (of about a factor of ten in the load) is provided. [NCERT CORPUS]
;sAXAraNawayA, surakRA ke lie BAra meM eka badZA mArjina (lagaBaga xasa ke guNaka kA) xiyA jAwA hE. [NCERT CORPUS]

(defrule provide12
(declare (salience 4600))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 margin)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiyA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide12   "  ?id "  xiyA_jA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (15-10-2014)
;When we hold a book in our hand, we are balancing the gravitational force on the book due to the huge mass of the earth by the 'normal force' provided by our hand. [NCERT CORPUS]
;jaba hama kisI puswaka ko hAWa para raKawe hEM, waba hama apane hAWa xvArA praxAna kie jAne vAle 'sAmAnya bala' se pqWvI ke viSAla xravyamAna ke kAraNa puswaka para lage guruwvAkarRaNa bala ko sanwuliwa karawe hEM. [NCERT CORPUS]

(defrule provide13
(declare (salience 4600))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 hand)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide13   "  ?id "  praxAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (14-11-2014)
;The answer to the question why the maximum height of a mountain on earth is ~ 10 km can also be provided by considering the elastic properties of rocks.  [NCERT CORPUS]
;pqWvI para kisI parvawa kI aXikawama UzcAI lagaBaga 10 @km howI hE, isa praSna kA uwwara BI cattAnoM ke prawyAsWa guNoM para vicAra karane se mila sakawA hE. [NCERT CORPUS]
;These tutoring services can be provided at a reasonable cost. [COCA]
;yaha paDAne kI sevAez uciwa mUlya para mila sakawI hE. [Self]

(defrule provide14
(declare (salience 4600))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 answer|service)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide14   "  ?id "  mila )" crlf))
)



;"provide","V","1.xenA"
;In Board exams some schools provide writing pads also with answer sheets.
;
;

;@@@ Added by 14anu24
;Some manufacturers are also willing to replace or offer refunds on new cars provided they are returned within a certain time of mileage limit .
;कुछ निर्माता गाडऋयिओं या उनके बदलने के प्रस्ताव रख सकते हैं बशर्ते कि वे किसी निर्धारित समय या चलाऋ गऋ मीलों की सीमा के अंदर हो .
(defrule provide15
(declare (salience 5500))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baSarwe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide15   "  ?id "  baSarwe )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-12-2014)
;It has been stated that a view of the Saptapuris in the Chaturmas is worth providing Moksha for . [Tourism-Corpus]
;ऐसा कहा गया है कि चतुर्मास में इन सप्तपुरियों का दर्शन मोक्ष प्रदान करने वाला होता है . [Tourism-Corpus]
;ऐसा कहा गया है कि चतुर्मास में इन सप्तपुरियों के दर्शन से मोक्ष प्राप्त होता है . [Self]

(defrule provide16
(declare (salience 5500))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 Moksha) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide16   "  ?id "  prApwa_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-12-2014)
;Watching the Chitrakot Waterfall in flood lights provides quite a different kind of experience . [Tourism Corpus]
;फ्लड लाइट में जलप्रपात को देखना अलग ही अनुभव प्रदान करता है . [Tourism Corpus]

(defrule provide17
(declare (salience 5500))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(viSeRya-of_saMbanXI ?id2 ?id1)
(id-root ?id1 experience)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide17   "  ?id "  praxAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (04-12-2014)
;The Arabian sea touching the lands of Daman has provided Daman with unique natural beauty and greenery . [Tourism Corpus]
;दमन की जमीन को छूते अरब सागर ने दमन को अप्रतिम प्राकृतिक सुंदरता व हरियाली प्रदान की है .  [Tourism Corpus]
;दमन की जमीन को छूते अरब सागर ने दमन को अप्रतिम प्राकृतिक सुंदरता व हरियाली प्रदान करी है . [Self]

(defrule provide18
(declare (salience 5500))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id2)
(viSeRya-with_saMbanXI  ?id2 ?id1)
(id-root ?id1 beauty)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide18   "  ?id "  praxAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-12-2014)
;These plants are provided free of cost by Forest Department to the school children . [Tourism Corpus]
;ये पौधे वन विभाग द्वारा बच्चों को मुफ़्त में उपलब्ध कराए जाते हैं . [Tourism Corpus]

(defrule provide19
(declare (salience 4300))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?)
(kriyA-object  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id1 free)
(id-root ?id2 cost)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upalabXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide19   "  ?id "  upalabXa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-12-2014)
;After reaching Chindi the extremely beautiful hotel Mamleshwar named Apple Valley Resort of Himachal Pradesh Tourism Corporation provides you extremely comfortable rooms and tasty food . [Tourism Corpus]
;चिंडी पहुँचकर हिमाचल प्रदेश पर्यटन निगम का  एप्पल वैली रिसोर्ट  नामक खूबसूरत होटल  मम़्लेश़्वर  आप को बेहद आरामदायक कमरे और स्वादिष़्ठ भोजन उपलब्ध करवाता है . [Tourism Corpus]

(defrule provide20
(declare (salience 4300))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id2 ?id1)
(id-root ?id1 hotel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upalabXa_karavA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide20   "  ?id "  upalabXa_karavA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-12-2014)
;The art and architecture , beautiful handicraft items and unique wild life of districts like Malwa , Vindhyachal , Bundelkhand and Nimad etc . of the state provide tourists with extremly adventurous experience . [Tourism Corpus]
;राज्य के मालवा , विंध्याचल , बुंदेलखंड और निमाड आदि अंचलों की कला और स्थापत्य , सुंदर हस्तकरघा वस्तुएँ और अनुपम वन्यजीवन पर्यटकों को अत्यंत ही रोमांचकारी अनुभव प्रदान करता है . [Tourism Corpus]

(defrule provide21
(declare (salience 4300))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(viSeRya-with_saMbanXI  ?id1 ?id2)
(id-root ?id1 tourist)
(id-root ?id2 experience)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxAna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide21   "  ?id "  praxAna_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-12-2014)
;60 % of salt is also provided by Gujarat . [Tourism Corpus]
;60 % नमक भी गुजरात प्रदेश द्वारा दिया जाता है . [Tourism Corpus]

(defrule provide22
(declare (salience 4300))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 Gujarat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide22   "  ?id "  xe_jA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-12-2014)
;The mountain bicycles , helmets , gloves etc for the expedition are provided by the youth hostels . [Tourism Corpus]
;अभियान के लिए माउंटेन बाईसाइकिल , हैलमेट , ग्लव्स आदि यूथ हॉस्टल द्वारा उपलब्ध कराए जाते हैं . [Tourism Corpus]

(defrule provide23
(declare (salience 4300))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 hostel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upalabXa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide23   "  ?id "  upalabXa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-03-2015)
;How will the toy help provide a foundation for my child’s future learning and development? [Report set 4]
;खिलौना मेरे बच्चे के भविष्य ज्ञान और विकास की बुनियाद तैयार करने में कैसे सहायता करेगा?  [Self]
(defrule provide24
(declare (salience 5500))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 foundation) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide24   "  ?id "  wEyAra_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (01-04-2015)
;The organization provides food and shelter for refugees. [oald]
;सङ्गठन शरणार्थियों के लिए आहार और आश्रय उपलबध कराता है . 	                    [Self]
(defrule provide25
(declare (salience 5500))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 shelter|food)
(viSeRya-for_saMbanXI ?id1 ?id2)
(id-root ?id2 refugee|tourist) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upalabaXa_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  provide.clp 	provide25   "  ?id "  upalabaXa_karA )" crlf))
)


;@@@ Added by Bhagyashri Kulkarni (15-09-2016)
;The man is provided with medicine in the right time. 
;आदमी को ठीक समय में औषधी दी गयी है.
(defrule provide26
(declare (salience 4200))
(id-root ?id provide)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI  ?id ?id1)
(id-tam_type ?id passive)
(pada_info (group_head_id ?id1) (preposition ?with_id))
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?with_id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng " ?*prov_dir* "  provide.clp 	provide26   "  ?id " " ?with_id "  xe )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  provide.clp     provide26   "  ?id " ko )" crlf))
)
