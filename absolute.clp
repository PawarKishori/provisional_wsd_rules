;$$$ Modified by 14anu-ban-01 on (19-10-2014)
;Since all substances change dimensions with temperature, an absolute reference for expansion is not available.[NCERT corpus]
;चूँकि ताप के साथ सभी पदार्थों की विमाएँ परिवर्तित होती हैं अतः प्रसार के लिए कोई निरपेक्ष सन्दर्भ उपलब्ध नहीं है.[NCERT corpus]
;@@@ Added by Garima Singh(M.tech-C.S, Banasthali Vidyapith) 15/03/2014
;Suppose two physical quantities A and B have measured values A ± ΔA B ± ΔB respectively where ΔA and ΔB are their absolute errors.[ncert]
;मान लीजिए, कि दो भौतिक राशियों A एवं B के मापित मान क्रमशः A ± ΔA, B ± ΔB हैं ; जहाँ, ΔA एवं ΔB क्रमशः इन राशियों की निरपेक्ष त्रुटियाँ हैं.
(defrule absolute2
(declare (salience 3000))
(id-root ?id absolute)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 error|reference);added reference by 14anu-ban-01 (19-10-2014)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirapekRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  absolute.clp 	absolute2   "  ?id "  nirapekRa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_absolute2
(declare (salience 3000))
(id-root ?id absolute)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 error)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirapekRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " absolute.clp   sub_samA_absolute2   "   ?id " nirapekRa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_absolute2
(declare (salience 3000))
(id-root ?id absolute)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-root ?id1 error)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirapekRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " absolute.clp   obj_samA_absolute2   "   ?id " nirapekRa )" crlf))
)

;@@@Added by 14anu-ban-02(06-02-2015)
;I need absolute silence when I'm working.[oald]
;जब मैं काम कर रहा हूँ तब मुझे पूर्णतया शांति चाहिए.[self]
(defrule absolute3
(declare (salience 3000))
(id-root ?id absolute)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 silence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNawayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " absolute.clp   absolute3   "   ?id " pUrNawayA )" crlf))
)

;**********************DEFAULT RULES*************************

;$$$ Modified by 14anu-ban-01 on (19-10-2014)
;The absolute minimum temperature for an ideal gas, ther efore, inferr ed by extrapolating the straight line to the axis, as in Fig. 11.3.[NCERT corpus]
;चित्र 11.3 में दर्शाए अनुसार, सरल रेखा को बहिर्वेशित करके किसी आदर्श गैस के लिए परम निम्निष्ठ ताप प्राप्त किया जा सकता है.[NCERT corpus]
;This temperature is found to be — 273.15 ° C and is designated as absolute zero.[NCERT corpus]
;इस ताप का मान – 273.15 0 C पाया गया तथा इसे परम शून्य कहा जाता है.[NCERT corpus]
;Absolute zero is the foundation of the Kelvin temperature scale or absolute scale temperature named after the British scientist Lord Kelvin.[NCERT corpus]
;परम शून्य ब्रिटिश वैज्ञानिक लॉर्ड केल्विन के नाम पर केल्विन ताप मापक्रम अथवा परम ताप मापक्रम का आधार है.[NCERT corpus]
(defrule absolute0
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id absolute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrA/parama));corrected spelling of 'pUrA' and added 'parama'by 14anu-ban-01  on (19-10-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  absolute.clp 	absolute0   "  ?id "  pUrA/parama)" crlf))
)

(defrule absolute1
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id absolute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parama_sixXAMwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  absolute.clp 	absolute1   "  ?id "  parama_sixXAMwa )" crlf))
)

;"absolute","N","1.parama_sixXAMwa"
;I always have a desire for absolutes in this uncertain world.
;
;
;LEVEL 
;
;
;Headword : absolute
;
;Examples --
;
;"absolute","Adj","1.pUrNawayA"
;There is no absolute standard for beauty.
;sunxarawA kA koI pUrNa mAnaka nahIM hE.
;
;"2.niSciwa"-pUrNarUpa se-pUrNawayA
;You should have absolute proof of her ablity.
;hamAre pAsa usakI yogyawA kA niSciwa pramANa honA cAhiye.
;
;"3.niraMkuSa"-aniyaMwriwa-asIma-pUrNarUpa se-pUrNawayA  
;Aurangzeb was an absolute ruler. 
;OraMgajZeba eka niraMkuSa SAsaka WA.
;
;"absolute","N","1.parama_wawva"
;I always have a desire for absolutes in this uncertain world.
;muJe isa aniSciwa xuniyA meM hameSA parama_wwva kI cAhawa rahawI hE.
;
;"absolute majority","N","1.pUrNa"
;The communist party won an absolute majority.
;kamyUnista pArtI pUrNa bahumawa se jIwI.
;
;nota:-- "absolute",Sabxa ke liye saMjFA Ora viSeRaNa ke vAkyoM se nimna sUwra 
;                nikAlA jA sakawA hE.
;
;sUwra : parama[<pUrNa]  
;
;
;
;
;

