;-----------------------DEFAULT RULE-----------------------------------------------------------------
(defrule ourselves0
(declare (salience 0000))
(id-root ?id ourselves)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hama_svayaM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ourselves.clp 	ourselves0   "  ?id "  hama_svayaM )" crlf))
)

;--------------------------------------------------------------------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-09 on 9-8-14
;We restricted ourselves to simpler situations of rigid bodies. [NCERT CORPUS]
;hamane svayaM aXyayana ko kevala xqDZa piNdoM kI sarala sWiwiyoM waka hI sImiwa raKA WA.
;Indeed, to be consistent, in that circumstance, we ourselves would crumble under our own weight![NCERT CORPUS]
;vAswava meM isase sAmaFjasya raKawe hue EsI parisWiwiyoM meM hama svayaM apane BAra ke aXIna tukade - tukade hokara biKara jAwe!
;We shall confine ourselves to the study of motion of objects along a straight line, also known as rectilinear motion. [NCERT CORPUS]
;isa aXyAya meM hama svayaM aXyayana vaswu ke eka sarala reKA ke anuxiSa gawi waka hI sImiwa raKefge ;isa prakAra kI gawi ko sarala reKIya gawi BI kahawe hEM.

(defrule ourselves1
(declare (salience 5000))
(id-root ?id ourselves)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
(id-root ?id2 we)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svayaM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ourselves.clp 	ourselves1   "  ?id "  svayaM )" crlf))
)

;@@@ Added by 14anu-ban-09 on 9-8-14
;The only people there were ourselves. [OALD]
;kevala hama hI loga We. [Own Manual]
;Ourselves too poor to help, we were forced to turn them away. [FD]
;vivaSawA meM hame unahe xUra hatAnA padA kyoki hama hI iwane garIba We ki maxaxa waka nahIM kara pAe. [Own Manual]

(defrule ourselves2
(declare (salience 5050))
(id-root ?id ourselves)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id pronoun)
(id-cat_coarse ?id1 noun)
(viSeRya-viSeRaNa  ?id1 ?id2)
(subject-subject_samAnAXikaraNa ?id1 ?id)
;(id-root ?id2 only)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hama_hI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ourselves.clp 	ourselves2   "  ?id "  hama_hI )" crlf))
)

