
;@@@ Added by 14anu-ban-11 on (05-02-2015)
;An electrical surge damaged the computer's disk drive. (oald)
;वैद्युत आवेश ने सङ्गणक की डिस्क ड्राइव को  नष्ट किया. (self)
(defrule surge0
(declare (salience 10))
(id-root ?id surge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AveSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  surge.clp  	surge0   "  ?id "   AveSa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (05-02-2015)
;Cricket fans surged into the stadium.(hinkhoj)
;क्रिकेट खेल प्रशंसक स्टेडियम् में उमडे .(self) 
(defrule surge1
(declare (salience 20))
(id-root ?id surge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 fan)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id umada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  surge.clp  	surge1   "  ?id "   umada)" crlf))
)


;@@@ Added by 14anu-ban-11 on (05-02-2015)
;Share prices surged.(oald)
;शेयर मूल्य तेजी से चढे .(self)
(defrule surge2
(declare (salience 30))
(id-root ?id surge)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 price)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se_caDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  surge.clp  	surge2   "  ?id "   wejI_se_caDa)" crlf))
)

