;@@@ Added by 14anu-ban-04 (06-04-2015)
;We had to completely disassemble the engine to find the problem.              [oald]
;हमें समस्या का पता लगाने के लिए इंजन को पूरी तरह से खोलना पड़ा .                                 [self]
(defrule disassemble1
(declare (salience 20))
(id-root ?id disassemble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id Kola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disassemble.clp    disassemble1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disassemble.clp  disassemble1   "  ?id " Kola )" crlf))
)

;@@@ Added by 14anu-ban-04 (06-04-2015)
;The concert ended and the crowd disassembled.            [oald]
;संगीत गोष्ठी समाप्त हुई और भीड़ हटी .                             [self]
(defrule disassemble2
(declare (salience 20))
(id-root ?id disassemble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 crowd)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disassemble.clp  disassemble2   "  ?id " hata )" crlf))
)

;@@@ Added by 14anu-ban-04 (06-04-2015)
;It is permissible for a lawful user to disassemble a computer program.       [free.dictionary.com]  ;run on parse no. 11 
;एक वैध प्रयोगकर्त्ता के लिए अनुज्ञेय है एक संगणक प्रोग्राम का अनुवाद करना  .                      [self]
(defrule disassemble3
(declare (salience 30))
(id-root ?id disassemble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 program|code)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA)) 
(assert (id-wsd_root_mng ?id anuvAxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disassemble.clp    disassemble3   "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disassemble.clp  disassemble3   "  ?id " anuvAxa_kara )" crlf))
)
;-----------------------------------DEFAULT RULE ---------------------------------------

;@@@ Added by 14anu-ban-04 (06-04-2015)
;The spectators began to disassemble.                [freedictionary.com]
;प्रेक्षक ने अलग करना शुरु किया .                                     [self]
(defrule disassemble0
(declare (salience 10))
(id-root ?id disassemble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disassemble.clp  disassemble0   "  ?id " alaga_kara )" crlf))
)
