
;@@@ Added by 14anu-ban-05 on (04-11-2014)
;Have fun![OALD]
;आनंद लें![MANUAL]
(defrule fun0
(declare (salience 1000))
(id-root ?id fun)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AnaMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fun.clp 	fun0   "  ?id "  AnaMxa)" crlf))
)

;@@@ Added by 14anu-ban-05 on (04-11-2014)
;You will find that this exercise is very educative and also great fun.[NCERT]
;Apa yaha pAezge ki yaha aByAsa bahuwa SikRApraxa waWA manoraFjaka hE.[NCERT]
(defrule fun1
(declare (salience 5000))
(Domain physics)
(id-root ?id fun)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 exercise)	
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manoraFjaka))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fun.clp 	fun1  "  ?id " manoraFjaka)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fun.clp       fun1   "  ?id "  physics )" crlf))
)
