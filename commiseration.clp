
;@@@ Added by 14anu-ban-03 (21-03-2015)        ;note- when root problem is solved word meaning is changed to root meaning.
;I offered him my commiseration.[oald]
;मैंने उसको अपनी हमदर्दी दिखाई . [manual]
(defrule commiseration0
(declare (salience 00))  
(id-word ?id commiseration)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hamaxarxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commiseration.clp 	commiseration0   "  ?id "  hamaxarxI )" crlf))
)



;@@@ Added by 14anu-ban-03 (21-03-2015)
;Commiserations to the losing team!  [oald]
;हारने वाले दल को सहानुभूति! [manual]
(defrule commiserations1
(declare (salience 10))  
(id-word ?id commiserations)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAnuBUwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  commiseration.clp 	commiserations1   "  ?id "  sahAnuBUwi )" crlf))
)


