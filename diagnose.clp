;$$$ Modified by 14anu-ban-04 (11-12-2014)    ;changed meaning from 'pIdZiwa_pAe' to 'pIdZiwa_pA'
;@@@ Added by 14anu03 on 16-june-14
;Mr. Smith was diagnosed with diabetes.
;श्रीमान स्मिथ मधुमेह रोग से  पीडित पाए गए थे .        ;translation changed by 14anu-ban-04 (11-12-2014)
(defrule diagnose0
(declare (salience 100))                ;salience reduced by 14anu-ban-04 (11-12-2014)
(id-root ?id diagnose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI ?id ?id1)
(id-word ?id1 diabetes|typhoid|cholera)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIdZiwa_pA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  diagnose.clp      diagnose0   "  ?id "  pIdZiwa_pA)" crlf))
)

;$$$ Modified by 14anu-ban-04 (11-12-2014)          ;changed meaning from 'pawA_lagAyA' to 'pawA_lagA'
;@@@ Added by 14anu03 on 17-june-14
;The doctor diagnosed diabetes.
;डाक्टर ने मधुमेह रोग पता लगाया .
(defrule diagnose1
(declare (salience 150))                                   ;salience reduced by 14anu-ban-04 (11-12-2014)
(id-root ?id diagnose)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-word ?id1 doctor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  diagnose.clp      diagnose1   "  ?id "  pawA_lagA)" crlf))
)
