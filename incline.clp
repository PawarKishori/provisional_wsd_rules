(defrule incline1
(declare (salience 4900))
(id-root ?id incline)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Juka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incline.clp 	incline1   "  ?id "  Juka )" crlf))
)

;default_sense && category=verb	JukanA/JukA	0
;"incline","V","1.JukanA/JukAnA"
;Radha inclined her head in prayer.
;
;
;@@@ Added by 14anu-ban-06 (04-09-14)
;If you are artistically inclined , you might like to pick up some kangra and basholi miniatures from the bhuri singh museum in chamba and the kangra art museum in lower dharamshala .(parallel corpus)
;यदि आप कलात्मक प्रवृत्ति के हैं , तो भूरी सिंह संग्रहालय से तथा लोवर धमर्शाला में कंगरा आटर् संग्रहालय से , कंगरा और बसोली लघुमूतिर्या लेना , आपको पसंद आ सकता हैं .
(defrule incline2
(declare (salience 5050))
(id-root ?id incline)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 artistically|spiritually)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pravqwwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incline.clp 	incline2   "  ?id "  pravqwwi )" crlf))
)

;@@@ Added by 14anu-ban-06 (04-09-14)
;He is in a chastened , meditative mood , inclined to view his fellow - men's misdeeds in the wide perspective of history .(parallel corpus)
;बल्कि संयत और ध्यानस्थ मनोदशा में , अपने सहयोगियों के अपराधों दुष्कर्मों को इतिहास के बृहत्तर परिदृश्य में देखने को प्रवृत्त हुए थे .
(defrule incline3
(declare (salience 5000))
(id-root ?id incline)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyArWa_kriyA ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pravqwwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incline.clp 	incline3   "  ?id "  pravqwwa_ho )" crlf))
)
