;@@@Added by 14anu-ban-02(17-03-2015)
;Life can be a real bastard sometimes.[mw]
;जिन्दगी कभी कभी  वास्तव में धोखेबाज़ हो सकती है .[self] 
(defrule bastard1 
(declare (salience 100)) 
(id-root ?id bastard) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 real)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id XoKebAjZa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bastard.clp  bastard1  "  ?id "  XoKebAjZa )" crlf)) 
) 


;@@@Added by 14anu-ban-02(17-03-2015)
;He was born in 1798, the bastard son of a country squire and his mistress.[cald]
;वह 1798 में पैदा हुआ था,एक देशी अनुरक्षक और उसकी मालकिन का नजायज बेटा . [self]
(defrule bastard3
(declare (salience 100)) 
(id-root ?id bastard) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 son)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id najAyajZa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bastard.clp  bastard3  "  ?id "  najAyajZa )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(17-03-2015)
;Sentence: He was a bastard to his wife.[oald]
;Translation: वह अपनी पत्नी के लिये दोगला था . [self]
(defrule bastard0 
(declare (salience 0)) 
(id-root ?id bastard) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id xogalA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bastard.clp  bastard0  "  ?id "  xogalA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(17-03-2015)
;A bastard knockoff of a far superior thriller.[mw]
;एक अधिक  अच्छी  रोमांचक फिल्म का एक घटिया अन्त हुआ . [self]
(defrule bastard2 
(declare (salience 0)) 
(id-root ?id bastard) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id GatiyA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bastard.clp  bastard2  "  ?id "  GatiyA )" crlf)) 
) 
