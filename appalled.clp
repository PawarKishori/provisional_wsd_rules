;@@@Added by 14anu-ban-02(05-03-2015)
;Sentence: An appalled expression.[oald]
;Translation:एक स्तम्भित भाव . [self]
(defrule appalled0 
(declare (salience 0)) 
(id-word ?id appalled) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id swamBiwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  appalled.clp  appalled0  "  ?id "  swamBiwa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(05-03-2015)
;We watched appalled as the child ran in front of the car.[oald]
;हमने चकित होकर देखा जैसे ही बच्चा गाड़ी के सामने दौड़ा . [self]
(defrule appalled1 
(declare (salience 100)) 
(id-word ?id appalled) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)	;need sentences to restrict the rule.
=> 
(retract ?mng) 
(assert (id-wsd_word_mng ?id cakiwa_hokara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  appalled.clp  appalled1  "  ?id "  cakiwa_hokara )" crlf)) 
) 
