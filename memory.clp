;@@@Added by 14anu-ban-08 (27-02-2015)
;People have short memories. [oald]
;लोगों की याददाश्त कमजोर होती हैं. [self]
(defrule memory0
(declare (salience 0))
(id-root ?id memory)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAxaxASwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  memory.clp  	memory0   "  ?id "  yAxaxASwa )" crlf))
)

;@@@Added by 14anu-ban-08 (27-02-2015)
;What is your earliest memory?  [oald]
;तुम्हारी सबसे पुरानी याद क्या हैं?  [self]
(defrule memory1
(declare (salience 50))
(id-root ?id memory)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 early)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  memory.clp  	memory1   "  ?id "  yAxa )" crlf))
)

;@@@Added by 14anu-ban-08 (13-03-2015)
;Have you got enough memory available to run the program?  [oald]
;क्या तुमको प्रोग्राम को चलाने के लिए पर्याप्त मेमोरी प्राप्त हो गई.  [self]
(defrule memory2
(declare (salience 90))
(id-root ?id memory)
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 enough)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id memorI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  memory.clp  	memory2   "  ?id "  memorI )" crlf))
)

;@@@Added by 14anu-ban-08 (13-03-2015)
;Her memory lives on. [oald]
;उसकी स्मृति अभी भी जिन्दा हैं.  [self]
(defrule memory3
(declare (salience 190))
(id-root ?id memory)
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)
(id-root ?id1 live)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id smqwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  memory.clp  	memory3   "  ?id "  smqwi )" crlf))
)


