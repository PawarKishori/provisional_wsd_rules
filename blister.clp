;@@@Added by 14anu-ban-02(11-03-2015)
;Her face had been blistered by the sun.[oald]
;उसका चेहरा धूप से जल गया था . [self]
(defrule blister2 
(declare (salience 100)) 
(id-root ?id blister) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 sun)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id jala_jA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blister.clp  blister2  "  ?id "  jala_jA)" crlf)) 
) 

;@@@Added by 14anu-ban-02(11-03-2015)
;By applying the juice of the leaves of dhatura on the blisters of arunshika two times a day after adding camphor the blisters are fast destroyed .[karan singla]
;धतूरे के पत्तों के रस में कपूर मिलाकर अरुंषिका की फुंसियों पर दिन में दो-तीन बार लगाने से  फुंसियाँ शीघ्र नष्‍ट होती हैं ।[karan singla]
(defrule blister3 
(declare (salience 100)) 
(id-root ?id blister) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)
(id-word ?id1 arunshika|face|forehead)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id PunsI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blister.clp  blister3  "  ?id "  PunsI)" crlf)) 
) 

;@@@Added by 14anu-ban-02(11-03-2015)
;By applying the juice of the leaves of dhatura on the blisters of arunshika two times a day after adding camphor the blisters are fast destroyed .[karan singla]
;धतूरे के पत्तों के रस में कपूर मिलाकर अरुंषिका की फुंसियों पर दिन में दो-तीन बार लगाने से  फुंसियाँ शीघ्र नष्‍ट होती हैं ।[karan singla]
(defrule blister4 
(declare (salience 100)) 
(id-root ?id blister) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject  ?id1 ?id)
(kriyA-karma  ?id1 ?id)
(id-root ?id1 destroy)
=>
(retract ?mng) 
(assert (id-wsd_root_mng ?id PunsI)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blister.clp  blister4  "  ?id "  PunsI)" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(11-03-2015)
;Sentence: Don't blister for anyone.[self]
;Translation: किसी के लिए कटु आलोचना मत कीजिए . [self]
(defrule blister0 
(declare (salience 0)) 
(id-root ?id blister) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id katu_AlocanA_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blister.clp  blister0  "  ?id "  katu_AlocanA_kara )" crlf)) 
)
;@@@Added by 14anu-ban-02(11-03-2015)
;He’d got blisters on his feet from running.[oald]
;दौड़ने से उसके पाँवों पर छाले पड़ जायेंगे. [self]
(defrule blister1 
(declare (salience 0)) 
(id-root ?id blister) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id CAlA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blister.clp  blister1  "  ?id "  CAlA )" crlf)) 
)  
