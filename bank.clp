;$$$Modified by 14anu-ban-02(16-01-2015)
;I know a bank where the wild thyme blows.
; मैं एक ऐसा किनारा जानता हूँ जहाँ  एक प्रकार का जङ्गली सुगन्धयुक्त पौधा उड़ता है . 
;@@@ Added by 14anu03 on 14-june-2014
;I know a bank where the wild thyme blows.
; मैं एक ऐसा किनारा जानता हूँ जहाँ जङ्गली एक प्रकार का सुगन्धयुक्त पौधा उडाता है . 
(defrule bank100
(declare (salience 5700))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa ?id ?id1)	;commented by 14anu-ban-02(16-01-2015)
;(id-word ?id1 a)			;commented by 14anu-ban-02(16-01-2015)
(kriyA-object  ?id1 ?id)		;added by 14anu-ban-02(16-01-2015)
(viSeRya-jo_samAnAXikaraNa  ?id ?id2)	;added by 14anu-ban-02(16-01-2015)
(kriyA-aXikaraNavAcI  ?id3 ?id2)	;added by 14anu-ban-02(16-01-2015)
(id-root ?id3 blow)			;added by 14anu-ban-02(16-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank100   "  ?id "  kinArA )" crlf))
)



(defrule bank0
(declare (salience 5000))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BarosA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " bank.clp	bank0  "  ?id "  " ?id1 "  BarosA_kara  )" crlf))
)

;One should not bank on the strangers.
;ajanabiyoM para BarosA nahIM karanA cAhie
(defrule bank1
(declare (salience 4900))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id banking )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id mahAjanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  bank.clp  	bank1   "  ?id "  mahAjanI )" crlf))
)

;"banking","N","1.mahAjanI"
;Sita choose banking as a career.

(defrule bank2
(declare (salience 4800))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 burrow )
(viSeRya-in_saMbanXI ?id1 ?id) ;Replaced viSeRya-in_viSeRaNa as viSeRya-in_saMbanXI programatically by Roja 09-11-13
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank2   "  ?id "  kinArA )" crlf))
)

;Crocodiles dig burrows in the banks
(defrule bank3
(declare (salience 4700))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) on)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank3   "  ?id "  kinArA )" crlf))
)

(defrule bank4
(declare (salience 4600))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) along)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank4   "  ?id "  kinArA )" crlf))
)

(defrule bank5
(declare (salience 0)); salience reduced by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)9-dec-2013
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bEMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank5   "  ?id "  bEMka )" crlf))
)

(defrule bank6
(declare (salience 4400))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bEMka_mez_rUpayA_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank6   "  ?id "  bEMka_mez_rUpayA_raKa )" crlf))
)

;"bank","V","1.bEMka_mez_rUpayA_raKanA"
;Ram banks his money for safety.


;$$$ Modified by 14anu15 17.06.2014
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)9-dec-2013
;On reaching Gangnani, Aditya jumped down from the bus and went to the bank of the Ganga.
;गंगनानी पहुँचने पर, आदित्य बस पर से  कूदा और गंगा के तट पर गया . 
;$$$ Modified by 14anu21 on 16.06.2014
;I am going to the river bank.
;मैं नदी के तट तक जा रहा हूँ.
(defrule bank7
(declare (salience 4600))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-of_saMbanXI  ?id ?id1)(and(kriyA-to_saMbanXI  ?idverb ?id)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)))
;(viSeRya-det_viSeRaNa  ?id1 ?id2)
;(id-word ?id2 the)
(id-word ?id1 river|ganga|yamuna|Brahmputra|Kosi|Gandak|Gomti|Chambal);MODIFIED by Gourav Sahni (MNNIT ALLAHABAD) on 17.06.2014 email-id:sahni.gourav0123@gmail.com
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank7   "  ?id "  wata )" crlf))
)

;@@@ Added by 14anu21 on 26.06.2014
;I am going to the river bank.
;मैं नदी बैंक को जा रहा हूँ . 
;मैं नदी के तट पर जा रहा हूँ.
(defrule bank10
(declare (salience 4600))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(and(kriyA-to_saMbanXI  ?idverb ?id)(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1))
(id-word ?id1 river)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank10   "  ?id "  wata )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)9-dec-2013
;Driving at this speed on a banked road will cause little wear and tear of the tyres.[ncert]
;इस चाल से ढालू सडक पर कार चलाने पर कार के टायरों की कम घिसाई होती है.
(defrule bank8
(declare (salience 4600))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 road)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAlU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank8   "  ?id "  DAlU )" crlf))
)

;$$$Modified by 14anu-ban-02(02-02-2015)
;He jumped in and swam to the opposite bank.
;वह कूद पड़ा और किनारे के  दूसरी ओर तैरा. [manual]
;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 17.06.2014 email-id:sahni.gourav0123@gmail.com
;He jumped in and swam to the opposite bank. 
;वह प्रति कूदा और दूसरी ओर किनारे को तैरा 
(defrule bank9
(declare (salience 5000))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
;(id-root =(- ?id 1) opposite)	;commented by 14anu-ban-02(02-02-2015)
(id-root ?id1 opposite)		;Added by 14anu-ban-02(02-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(assert (id-wsd_viBakwi ?id kA))	;Added by 14anu-ban-02(02-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  bank.clp     bank9  "  ?id " kA)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank9   "  ?id "  kinArA )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 17.06.2014 email-id:sahni.gourav0123@gmail.com
; It's on the north bank of the Thames. 
;यह टेम्स के उत्तर किनारे पर है . 
(defrule bank11
(declare (salience 5000))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-cat_coarse ?id noun)
(id-root =(- ?id 1) north|west|east|south)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kinArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bank.clp 	bank11   "  ?id "  kinArA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_bank8
(declare (salience 4600))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 road)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAlU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bank.clp   sub_samA_bank8   "   ?id " DAlU )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_bank8
(declare (salience 4600))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id1 ?id)
(id-word ?id1 road)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAlU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bank.clp   obj_samA_bank8   "   ?id " DAlU )" crlf))
)

;@@@ Added by Shruti singh (20-08-2016)
;The sun disappeared behind a bank of clouds.
;sUraja bAxalon kI paMkwi ke pICe oJala gayA.
(defrule bank1000
(declare (salience 8000))
(id-root ?id bank)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 cloud)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paMkwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " bank.clp  bank1000  "  ?id " paMkwi)" crlf))
)

