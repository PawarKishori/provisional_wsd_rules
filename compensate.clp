;@@@ Added by 14anu-ban-03 (25-03-2015)
;Victims of the crash will be compensated for their injuries. [cald]
;धमाके से पीड़ितों को उनकी चोटों के लिए हरजाना दिया जाएगा. [manual]
(defrule compensate0
(declare (salience 00))
(id-root ?id compensate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id harajAnA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compensate.clp  compensate0  "  ?id "  harajAnA_xe )" crlf))
)


;@@@ Added by 14anu-ban-03 (25-03-2015)
;Nothing will ever compensate for his lost childhood. [cald]
;कुछ भी उसके खोए हुए बचपन की क्षतिपूर्ति नही कर सकता . [manual]
(defrule compensate1
(declare (salience 10))
(id-root ?id compensate)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI ?id ?id1)
(id-root ?id1 childhood|lack|miss|suffering)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRawipUrwi_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compensate.clp  compensate1  "  ?id "  kRawipUrwi_kara )" crlf))
)
