
(defrule convert0
(declare (salience 5000))
(id-root ?id convert)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paraXarma_avalaMbI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  convert.clp 	convert0   "  ?id "  paraXarma_avalaMbI )" crlf))
)

;"convert","N","1.paraXarma_avalaMbI"
;She converts to the Jewish Faith.
;
(defrule convert1
(declare (salience 4900))
(id-root ?id convert)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  convert.clp 	convert1   "  ?id "  baxala )" crlf))
)

;"convert","VT","1.baxalanA"
;We converted from 220 to 11.Volt
;Could you convert my dollars into pounds?
;Convert lead into gold
;The substance converts to an acid
;--"2.Xarma_parivarwana_karanA"
;She converted to Buddhism


;@@@ Added by 14anu-ban-03 (09-10-2014)
;In a nuclear process mass gets converted to energy (or vice-versa). [NCERT CORPUS]
;nABikIya prakriyAoM meM xravyamAna UrjA meM parivarwiwa ho jAwA hE (aWavA vilomawaH BI howA hE). [NCERT CORPUS]
(defrule convert2
(declare (salience 4900))
(id-root ?id convert)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 energy)    ;added by 14anu-ban-03 (24-11-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwiwa_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  convert.clp 	convert2   "  ?id "  parivarwiwa_ho_jA )" crlf))
)

;@@@ Added by 14anu-ban-03 (24-11-2014)
;This summation can be converted to an integral in most cases. [ncert]
;aXikAMSa prakaraNoM meM safkalana ko samAkalana meM parivarwiwa kara lewe hEM.[ncert]
(defrule convert3
(declare (salience 4900))
(id-root ?id convert)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1) 
(id-root ?id1 integral)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  convert.clp 	convert3   "  ?id "  parivarwiwa_kara )" crlf))
)

;@@@ Added by 14anu07
(defrule convert02
(declare (salience 5000))
(id-root ?id convert)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) to)
(id-root =(+ ?id 2) Buddhism|Catholicism|Hinduism|Christianity|Sikhism)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  parivarwiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  convert.clp 	convert02   "  ?id "  parivarwiwa)" crlf))
)






