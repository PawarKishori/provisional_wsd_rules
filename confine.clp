;########################################################################
;#  Copyright (C) 2014-2015 14anu26 (noopur.nigam92@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################

;$$$ Modified by 14anu-ban-03 (13-12-2014)
;@@@ Added by 14anu26 [14-06-14]
;He was confined to a wheelchair after the accident.
;वह दुर्घटना के बाद पहियेदार कुर्सी  तक ही सीमित हो गया था . 
(defrule confine1
(declare (salience 4900))
(id-root ?id confine)  ;modified confined as confine by 14anu-ban-03 (13-12-2014)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 wheelchair|bed)  ;added by 14anu-ban-03 (13-12-2014)  ;added 'bed' by 14anu-ban-03 (01-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka_hI_sImiwa_ho ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confine.clp 	confine1  "  ?id "  waka_hI_sImiwa_ho )"crlf))
)

;@@@ Added by 14anu-ban-03 (01-04-2015)
;Keep the dog confined in a suitable travelling cage. [oald]
;एक  सुविधाजनक यात्रा के लिए कुत्ते को पिन्जरे में बन्द करके रखिए . [manual]
(defrule confine3
(declare (salience 5000))
(id-root ?id confine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 cage)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confine.clp 	confine3   "  ?id " banxa_kara )" crlf))
)


;@@@ Added by 14anu-ban-03 (01-04-2015)
;The soldiers concerned were confined to barracks. [oald]
;सम्बन्धित सैनिक बैरक में कैद किए गये थे . [manual]
(defrule confine4
(declare (salience 5000))
(id-root ?id confine)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 barracks|jail)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kExa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confine.clp 	confine4   "  ?id " kExa_kara)" crlf))
)

;@@@ Added by 14anu-ban-03 (01-04-2015)
;A confine town. [self with refrence to oald]  ;working on aprser no. 2
;एक सीमान्त नगर . [manual]
(defrule confine5
(declare (salience 5000)) 
(id-root ?id confine)  
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 town|zone|post)
(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confine.clp 	confine5   "  ?id "  sImAnwa)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu26 [14-06-14]
;His message does not confine to high politics.
;ऊँची राजनीति को उसका सन्देश सीमित नहीं करता है . 
(defrule confine0
(declare (salience 0))
(id-root ?id confine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confine.clp 	confine0   "  ?id " sImiwa_kara)" crlf))
)

;$$$ Modified by 14anu-ban-03 (13-12-2014)
;@@@ Added by 14anu26 [14-06-14]
;The narrow confines of political life.
;राजनैतिक जीवन की सङ्कीर्ण सीमाएं. 
(defrule confine2
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (01-04-2015)
(id-root ?id confine)  ;modified confines as confine by 14anu-ban-03 (13-12-2014)
?mng <-(meaning_to_be_decided ?id)
;(or(viSeRya-viSeRaNa ?id ?id1)(viSeRya-of_saMbanXI  ?id ?id1))  ;commented by 14anu-ban-03 (01-04-2015)
;(id-root ?id1 life)  ;added by 14anu-ban-03 (13-12-2014)   ;commented by 14anu-ban-03 (01-04-2015)
(id-cat_coarse ?id noun) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  confine.clp 	confine2   "  ?id "  sImA)" crlf))
)

