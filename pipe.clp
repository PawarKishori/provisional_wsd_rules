
;"piping","Adj","1.sItI_jEsA"
;Her piping voice irritates me a lot.
(defrule pipe0
(declare (salience 5000))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id piping )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sItI_jEsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  pipe.clp  	pipe0   "  ?id "  sItI_jEsA )" crlf))
)

;"piping","N","1.viSeRa`nApa_kA_pAipa"
;The require 1.meters of lead piping.
(defrule pipe1
(declare (salience 4900))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id piping )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id viSeRa_nApa_kA_pAipa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  pipe.clp  	pipe1   "  ?id "  viSeRa_nApa_kA_pAipa )" crlf))
)

(defrule pipe2
(declare (salience 4800))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bola));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " pipe.clp pipe2 " ?id "  bola )" crlf)) 
)

;She soon piped up when i asked the question.
;jEse hI mEne praSna kiyA vaha PatAPata bola padZI
(defrule pipe3
(declare (salience 4700))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pipe.clp	pipe3  "  ?id "  " ?id1 "  bola  )" crlf))
)

;She soon piped up when i asked the question.
;jEse hI mEne praSna kiyA vaha PatAPata bola padZI

;@@@ Added by 14anu-ban-09 on (10-09-2014)
;Lord Krishna plays pipe very well. [Same CLP file]
;BagavAna kqaRNa bahuwa acCI bAsurI bajAwe hE. [Own Manual] 

(defrule pipe6
(declare (salience 5000))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 play)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAsurI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pipe.clp 	pipe6   "  ?id "  bAsurI )" crlf))
)

;@@@ Added by 14anu-ban-09 on (10-09-2014)
;We can’t simply fix the Colorado by piping water from another place, as Brian Richter recently pointed out. [AnusaarakaGoogle_17_Jul_2014]
;hama kolArAdo ko mahajZa eka anya jagaha se pAnI ko pAepa se Beja kara TIka nahIM kara sakawe, jEsA briyAna ricatara ne hAla hI meM XyAna xilAyA. [Own Manual]

(defrule pipe7
(declare (salience 5000))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 water)
(id-word =(- ?id 1) by)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id (- ?id 1) pAIpa_se_Beja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng   " ?*prov_dir* " pipe.clp  pipe7  "  ?id "  " (- ?id 1) "  pAIpa_se_Beja_kara  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-02-2015)
;The bird is piping a song on the tree.  	[same clp file]
;चिड़िया पेड़ पर गाना चहचहा रही हैं.	 		[self]

(defrule pipe8
(declare (salience 5000))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 bird)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cahacahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pipe.clp 	pipe8   "  ?id "  cahacahA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-02-2015)
;She piped in her suggestions. 				[same clp file]
;उसने अपने/उसके सुझावों में बताया .	 			[self]

(defrule pipe9
(declare (salience 5000))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 suggestion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pipe.clp 	pipe9   "  ?id "  bawA )" crlf))
)

;NOTE-Parser problem. Run on parser no. 581.
;@@@ Added by 14anu-ban-09 on (19-02-2015)
;Will you please pipe down, you two?	[cald] 
;क्या तुम दोनो बाते बंद करोगे .			[self]

(defrule pipe10
(declare (salience 5000))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 down)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id ?id1 bAwe_baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng   " ?*prov_dir* " pipe.clp  pipe10  "  ?id "  " ?id1 "  bAwe_baMxa_kara  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-02-2015)
;The cake had strawberries piped on it.				[oald] 
;केक जिस पर गुलाबी-लाल रङ्ग का फल सजा हुआ था  . 			[self]

(defrule pipe11
(declare (salience 5000))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 cake|food)
(kriyA-kriyA_viSeRaNa  ?id ?id2)
(id-root ?id2 strawberry|cherry)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pipe.clp 	pipe11   "  ?id "  sajA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-02-2015)
;In the silence that followed, a lone voice piped up from the back of the room.	[cald]
;फैली हुई ख़ामेशी में एक मंद आवाज कमरे के पीछे से आने लगी .  	[self]

(defrule pipe12
(declare (salience 4700))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 up)
(kriyA-subject ?id ?id2)
(id-root ?id2 voice)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Ane_laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " pipe.clp	pipe12  "  ?id "  " ?id1 "  Ane_laga  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-02-2015)
;The pipers piped while the drummers drummed.	[merrian-webster]
;वेणु वादकों ने वेणु बजाया जब कि ढोल बजाने वालों ने ढोल बजाया  . 	[Self]

(defrule pipe13
(declare (salience 5000))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 piper)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id veNu_bajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pipe.clp 	pipe13   "  ?id "  veNu_bajA )" crlf))
)

;------------------------ Default Rules ----------------------

;"pipe","V","1.nala_se_le_jAnA"
;She is piping water in to the garden.
(defrule pipe5
(declare (salience 4500))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nala_se_le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pipe.clp 	pipe5   "  ?id "  nala_se_le_jA)" crlf))
)

;"pipe","N","1.pAipa"
(defrule pipe4
(declare (salience 4600))
(id-root ?id pipe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAipa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pipe.clp 	pipe4   "  ?id "  pAipa )" crlf))
)



;"pipe","V","1.nala_se_le_jAnA"
;She is piping water in to the garden.
;--"2.cahakanA"
;The bird is piping a song on the tree.
;
;"pipe","N","1.pAipa"
;--"2.nalikA"
;Nurse was told to put oxyzen pipe on patient's nose
;--"3.cilama/pEpa"
;Raju is a chain pipe smoker
;--"4.bAzsurI"
;Lord Krishna plays pipe very well ;wrong sentence
;
;"piping","N","1.viSeRa`nApa_kA_pAipa"
;The require 1.meters of lead piping.
;--"2.gota"
;Her skirt was decorated by piping.

;LEVEL 
;Headword : pipe
;
;Examples --
;1.This is a new drainage pipe.
;yaha pAnI ke nikAsa kI pAipa naI hE.
;2.He smokes his pipe occasionally.
;vaha kaBIkabAra apanI pAipa PUzkawA hE.
;3.They played old tunes on the pipe.
;unhoMne pAipa para purAnI XuneM bajAI.
;4.They are planning to pipe water to the rural areas.
;grAmINa kRewroM meM ve pAipa se pAnI le jAne kI yojanA banA rahe hEM.
;5.The birds are piping away in the backyard.
;piCavAde meM paMCI cahaka rahe hEM.
;6.She piped in her suggestions.
;vaha apane suJAva cahaka uTI.
;
;
;vyAKyA --
;
;uparaliKiwa vAkyoM meM "pipe" Sabxa ke lie jo Binna arWa A rahe hEM, unameM vAswava 
;meM saMbaMXa hE. inameM jo saMbaMXa hEM, use hama isa prakAra samaJa sakawe hEM.
;
;vAkya 1-3 meM "pipe" kA jo arWa A rahA hE, vaha hE, "nalI" kA. vAkya 1. meM nalI
;kA arWa pAnI kI pAipa ke saMxarBa meM A rahA hE. vAkya 2. meM nalI kA arWa XumrapAna 
;karane meM prayoga kI jAnevAlI nalI ke saMxarBa meM A rahA hE. vAkya 3. meM nalI kA arWa
;saMgIwa vAxya ke saMxarBa meM A rahA hE.
;
;vAkya 4. meM "pipe" kA jo arWa A rahA hE, vaha hE "nalI ke xvArA pAnI le jAnA". wo
;nalI kA arWa yahAz kriyA ke rUpa meM prakata howA hE.
;
;vAkya 5 -6 meM "pipe" kA arWa "cahakane" ke arWa meM A rahA hE. nalI yA bAzsurI kI
;avAjZa Ora paMCIoM kI AvAjZa meM samAnawA hone se, Ora kisI swrI ke wIkRNa svara Ora 
;paMCIoM kI cahaka meM samAnawA hone se, nalI ke arWa se cahakane kA arWagrahaNa kiyA jA 
;sakawA hE.
;
;anwarnihiwa sUwra ;
;
;nalI - bAzsurI - ke jEsI AvAjZa
;
;wo aba, hama "pipe" ke lie sUwra kuCa isa prakAra xe sakawe hEM.
;
;sUwra : nalI`
