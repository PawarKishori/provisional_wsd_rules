
(defrule contemporary0
(declare (salience 5000))
(id-root ?id contemporary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AXunika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contemporary.clp 	contemporary0   "  ?id "  AXunika )" crlf))
)


;@@@ Added by 14anu-ban-03 on (04-10-2014)
;Sentence: Contemporary accounts attest to his courage and determination.[oald]  ;this sentence run on parser 21
;Translation:  समकालीन विवरण उसकी हिम्मत और  दृढ़ता को प्रमाणित करता है. [manual]
;To resist contemporary claims of state. [bnc_gold]
;राज्य के समकालीन दावों का प्रतिरोध करना. [manual] 
(defrule contemporary2
(declare (salience 5000))
(id-root ?id contemporary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 claim|account)      ;added 'account' by 14anu-ban-03 (02-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samakAlIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contemporary.clp 	contemporary2   "  ?id "  samakAlIna )" crlf))
)

(defrule contemporary1
(declare (salience 4900))
(id-root ?id contemporary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samakAlIna_vyakwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  contemporary.clp 	contemporary1   "  ?id "  samakAlIna_vyakwi )" crlf))
)

;"contemporary","N","1.samakAlIna_vyakwi"
;He && my mother were contemporaries at college.
;
;
