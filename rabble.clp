;@@@ Added by 14anu-ban-10 on (16-02-2015)
;A speech that appealed to the rabble.[hinkoj]
; एक भाषण जिसने निम्न वर्ग  को अपील करी. [manual]
(defrule rabble1
(declare (salience 200))
(id-root ?id rabble)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimna_varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rabble.clp 	rabble1   "  ?id "  nimna_varga)" crlf))
)

;$$$ Modified by 14anu-ban-06 (24-04-2015)
;@@@ Added by 14anu-ban-10 on (16-02-2015)
;He was met by a rabble of noisy, angry youths.[oald]
;उसने  भड़कीले , क्रोधित युवकों की एक भीड़ से मुलाकात की थी ।[manual]
(defrule rabble2
(declare (salience 300))
(id-root ?id rabble)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BIda))            ;meaning changed from 'BIdZ' to 'BIda' by 14anu-ban-06 (24-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rabble.clp 	rabble2   "  ?id "  BIda)" crlf))
)

;@@@ Added by 14anu-ban-06 (24-04-2015)
; Her speech stirred the emotions of the rabble. (cambridge)
;उसके भाषण ने निम्न वर्ग की भावनाएँ हिला दी. (manual)
(defrule rabble3
(declare (salience 400))
(id-root ?id rabble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 emotion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimna_varga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rabble.clp 	rabble3   "  ?id "  nimna_varga)" crlf))
)

;----------------- Default Rules --------------

;$$$ Modified by 14anu-ban-06 (24-04-2015)
;@@@ Added by 14anu-ban-10 on (16-02-2015)
;Some politicians are merely rabble.[hinkhoj] [parser no.- 4]
;कुछ राजनीतिज्ञ मात्र प्राकृत गण हैं . [manual]
(defrule rabble0
(declare (salience 100))
(id-root ?id rabble)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)                ;changed category from 'adjective' to 'noun' by 14anu-ban-06 (24-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prAkqwa_gaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rabble.clp   rabble0   "  ?id "  prAkqwa_gaNa )" crlf))
)

