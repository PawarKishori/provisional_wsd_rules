;commented by 14anu-ban-03 (19-03-2015)  ;because same rule is in closed.clp here is no need of this rule
;(defrule close0
;(declare (salience 5000))
;(id-root ?id close)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id closed )
;(id-cat_coarse ?id adjective)
;=>
;(retract ?mng)
;(assert (id-wsd_word_mng ?id banXA_huA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  close.clp  	close0   "  ?id "  banXA_huA )" crlf))
;)

;"closed","Adj","1.banXA_huA"
;She belongs to a closed family.

;$$$ Modified by 14anu-ban-03 (19-03-2015)
;Many of the city's leading restaurants closed down for the whole year. [same clp]  
;Sahara ke bahuwa se reswarAM pUre sAla ke liye baMxa ho gaye. [same clp]
(defrule close1
(declare (salience 4900))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-for_saMbanXI ?id ?id2)  ;added by 14anu-ban-03 (19-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baMxa_ho_jA))  ;meaning changed from 'sWAyI_wOra_pe_baMxa_kara' to 'baMxa_ho_jA' by 14anu-ban-03 (19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close1  "  ?id "  " ?id1 "  baMxa_ho_jA  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (19-03-2015)  ;working on parser no.- 2
;The truck was closing in to the child. [same clp]
;traka bacce ke pAsa A rahA WA. [same clp]
(defrule close2
(declare (salience 4800))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(kriyA-to_saMbanXI ?id ?id2)  ;added by 14anu-ban-03 (19-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pAsa_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close2  "  ?id "  " ?id1 "  pAsa_A  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (19-03-2015)
;In the autumn the days begin to close in. [same clp]
;pawaJadZa meM xina Cote hone lagawe hEM. [same clp]
(defrule close3
(declare (salience 4700))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(kriyA-subject ?id ?id2)  ;added by 14anu-ban-03 (19-03-2015)
(id-root ?id2 day)  ;added by 14anu-ban-03 (19-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Cote_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close3  "  ?id "  " ?id1 "  Cote_ho  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (19-03-2015)
;They closed up in the house to prevent from thieves. [same clp]
;unhone coroM se bacane ke lie apane ko Gara meM baMxa kara liyA. [same clp]
(defrule close4
(declare (salience 4600))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-kriyArWa_kriyA ?id ?id2) ;added by 14anu-ban-03 (19-03-2015)
(id-root ?id2 prevent)  ;added by 14anu-ban-03 (19-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baMxa_kara_le)) ;meaning changed from 'pUrI_waraha_se_baMxa_kara_xe' to 'baMxa_kara_le' by 14anu-ban-03 (19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close4  "  ?id "  " ?id1 "  baMxa_kara_le  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (19-03-2015)
;The petals of some flowers close up while touching them. [same clp]
;कुछ फूलों की पँखुडी को स्पर्श करते ही वह इकट्ठा हो जाती हैं . [manual]
(defrule close5
(declare (salience 4500))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)  ;commented by 14anu-ban-03 (19-03-2015)
(kriyA-subject ?id ?id2) ;added by 14anu-ban-03 (19-03-2015)
(id-root ?id2 petal) 	 ;added by 14anu-ban-03 (19-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ikatTA_ho_jA))  ;meaning changed from 'pAsa_AnA_yA_ikatTe_ho' to 'ikatTA_ho_jA' by 14anu-ban-03 (19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close5  "  ?id "  " ?id1 "  ikatTA_ho_jA  )" crlf))
)


(defrule close6
(declare (salience 4300))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AliMgana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close6  "  ?id "  " ?id1 "  AliMgana_kara  )" crlf))
)


(defrule close7
(declare (salience 4100))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close7  "  ?id "  " ?id1 "  samApwa_kara  )" crlf))
)


(defrule close8
(declare (salience 3900))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CotA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close8  "  ?id "  " ?id1 "  CotA_ho  )" crlf))
)


(defrule close9
(declare (salience 3700))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AliMgana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close9  "  ?id "  " ?id1 "  AliMgana_kara  )" crlf))
)



(defrule close10
(declare (salience 3600))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 round)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AliMgana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close10  "  ?id "  " ?id1 "  AliMgana_kara  )" crlf))
)


(defrule close11
(declare (salience 3400))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close11  "  ?id "  " ?id1 "  banxa_kara  )" crlf))
)


(defrule close12
(declare (salience 3200))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 with)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Amane_sAmane_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close12  "  ?id "  " ?id1 "  Amane_sAmane_ho  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (19-03-2015)
;The doors open and close automatically. [same clp]
;दरवाजे अपने आप से  खुले और बन्द हुए .  [manual]
;$$$ Modified by 14anu21
;The doors open and close automatically. -- added by 14anu21
(defrule close13
(declare (salience 3000))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id closed|close ) ;added 'close' by 14anu21   ;commented by 14anu-ban-03 (19-03-2015)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)  	;added by 14anu-ban-03 (19-03-2015)
(id-root ?id1 door)		;added by 14anu-ban-03 (19-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_huA))   ;meaning changed from 'banXa' to 'banxa_hua' by 14anu-ban-03 (19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  close.clp  	close13   "  ?id "  banxa_huA )" crlf))
)

(defrule close14
(declare (salience 2900))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  close.clp 	close14   "  ?id "  nikata )" crlf))
)



(defrule close15
(declare (salience 2800))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  close.clp 	close15   "  ?id "  pAsa )" crlf))
)




;Modified by Meena(7.6.11) ;added (id-root ?id1 industrials|share)
;Added by Meena(20.5.11)
;The Dow Jones industrials closed at 2569.26. 
(defrule close16
(declare (salience 2700))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 industrials|share|dollar|pound) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  close.clp     close16   "  ?id "  baMxa_ho )" crlf))
)               



;Salience reduced by Meena(20.5.11)
(defrule close17
(declare (salience 0))
;(declare (salience 2700))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  close.clp 	close17   "  ?id "  baMxa_kara )" crlf))
)

;commented by 14anu-ban-03 (19-03-2015) same rule as close11  
;(defrule close18
;(declare (salience 2600))
;(id-root ?id close)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 up)
;(kriyA-upasarga ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banxa_kara))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close18  "  ?id "  " ?id1 "  banxa_kara  )" crlf))
;)


;@@@ Added by 14anu21 on 23.06.2014 
;The meeting will close after one hour.
;बैठक एक घण्टे के बाद बन्द करेगी . (Translaton before adding rule)
; बैठक एक घण्टे के बाद समाप्त होगी . 
(defrule close19
(declare (salience 4900))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?idsub)
(id-root ?idsub meeting|play|game|movie|show|conference|subject)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samApwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  close.clp 	close19   "  ?id "  samApwa_ho )" crlf))
)
;@@@ Added by 14anu21 on 23.06.2014
;These measures are aimed at closing the differences between rich and poor.
;ये उपाय अमीर और दीन के बीच अन्तर बन्द करने में लक्षित की गयीं हैं . (Translation before addind rule)
;ये उपाय अमीर और दीन के बीच अन्तर कम करने में लक्षित किए गये हैं 
(defrule close20
(declare (salience 4900))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?id1))
(id-root ?id1 gap|difference)
(or(kriyA-at_saMbanXI  ?idverb ?id)(kriya-in_saMbanXI ?idverb ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  close.clp 	close20   "  ?id "  kama_kara )" crlf))
)

;@@@ Added by 14anu21 on 23.06.2014
;The gap between the two teams is closing all the time.
;दो टीमों के बीच अन्तर हर समय कम कर रहा है . 
;दो  टीमों के बीच अन्तर हर समय कम हो रहा है . 
(defrule close21
(declare (salience 4850))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?id1))
(id-root ?id1 gap|difference)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  close.clp 	close21   "  ?id "  kama_ho )" crlf))
)

;@@@ Added by 14anu-ban-03 (23-01-2015)
;A source close to Krishna says "he has a taste for the elegant things in life" . (taste4)
;कृष्ण के करीबी सूत्र का कहना है कि  उसे जीवन में नफीस चीजें पसंद हैं .  [manual]
(defrule close22
(declare (salience 5000))
(id-root ?id close)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) ke_karIbI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " close.clp	close22  "  ?id "  " (+ ?id 1) "  ke_karIbI  )" crlf))
)



;The gap between the two top teams is closing all the time.
;default_sense && category=noun	samApwi	0
;"close","N","1.samApwi"
;At the close of first innings the score was 158 for 2 wickets.
;
;
;LEVEL 
;
;
;Headword : close
;
;Examples --
;
;`close' ke viviXa prayoga--
;
;"close","Adj","1.pAsa"
;Our school is close to the theatre.
;hamArA skUla Wiyetara ke pAsa hE.
;            ---- < paxArWa-sambanXiwa nikatawA ----< yAni xUrI kama honA 
;<-- yAni banxa honA 
;--"2.sUkRma"
;He took a close look at the text.
;            ---- < nikata <-- nikatawA se xeKanA <--bArIkI se xeKanA < banxa honA
;--"3.bArIka"
;The razor gave a close shave.
;           ---- < paxArWa-sambanXiwa banxa hone meM nikatawA(bArIka) < banxa honA
;--"4.najZaxIkI"
;She is a close relative of mine.
;            ---- < pAsa < paxArWa-sambanXiwa banxa hone meM nikatawA < banxa honA
;--"5.samAna"
;There is no close resemblance between those twins.
;            ---- < pAsa < paxArWa-sambanXiwa banxa hone meM nikatawA < banxa honA
;--"6.barAbara_kA"
;The match was a close contest.
;            ---- < samAna < pAsa < paxArWa-sambanXiwa banxa hone meM nikatawA 
;                                                     < banxa honA
;--"7.ekasAWa"
;The army advanced in close formation.
;            ---- < barAbara kA < samAna < pAsa < paxArWa-sambanXiwa banxa
;                                             hone meM nikatawA < banxa honA
;--"8.banxa"
;He is kept in close confinement of her house.
;           ---- < banxa honA
;--"9.gopanIya"
;She was a bit close about her marriage.
;           ---- < banxa raKane yogya < banxa honA
;--"10.savqwa"
;The English vowel /i:/ is a close vowel.
;           ---- < saMkoca kA BAva < banxa honA
;--"11.kaMjUsa"
;He is very close with his things.
;           ---- < sakocI mAnasikawA vAlA vyakwi < saMkoca kA BAva < banxa honA
;
;"close","Adv","1.pAsa_WaeM"
;They sat close to each other in the photo.
;           ---- < pAsa < paxArWa-sambanXiwa banxa hone meM nikatawA < banxa honA
;
;"close","V","1.banxa_honA"
;The shops in the city close at 11:00 pm.
;--"2.banxa_karanA"
;The door was closed at night.
;           ---- < banxa honA
;--"3.samApwa_honA"
;The function closed with a vote of thanks.
;          ---- kisI cIja ke pravAhamAna aswiwwva kA banxa honA < banxa honA
;--"4.najZaxIka_AnA"
;The gap between the runners are beginning to close.
;          ---- < pAsa < paxArWa-sambanXiwa banxa hone meM nikatawA < banxa honA
;---------------------------------------------------------
;
;sUwra : pAsa`[<banxa_honA`]
;--------------
;
;     aMgrejI ke isa `close' Sabxa ke hinxI meM upariliKiwa viBinna arWa 
;prayogAnusAra samaJe jAwe hEM . ina viBinna arWoM meM eka sAmAnya arWa `banxa honA' 
;xeKA jA sakawA hE, jisase anya arWoM kA viswAra huA hE . banxa hone kI eka 
;pUrNa kriyA meM anya bahuwa sI gONa kriyAoM kA honA howA hE . ina viviXa honevAlI 
;iwara kriyAoM ko rUpaka banAkara ina viswqwa arWoM ko samaJA jA sakawA hE . 
;
;-- banxa hone kI kriyA ke bIca honevAlI kriyAoM ko xeKeM wo nimna kriyAez 
;sAmAnyawaH xeKI jA sakawI hEM . 
;(yahAz yaha XyAna raKanA cAhie ki kisI cIja ke pravAhamAna aswiwwva ke samApwa
;hone ko BI banxa honA kahawe hEM)
;1- banxa hone meM nikatawA spaRtawaH xeKI jA sakawI hE . jEse- kinhIM kivAda Axi 
;ke banxa hone meM sAXaka vaswuoM kA nikata meM AnA ho rahA hE . 
;(kivAdoM ke pAsa meM AnA, unake Apasa kA saMkocana BI kahalA sakawA hE . 
;saMkocana se saMkocIsvaBAva ko samaJA jA sakawA hE,saMkocI svABava kqpaNa kA howA hE) 
;2- sAXaka vaswuoM ke nikata Ane meM maXaya meM sWiwa rikwawA XIre-XIre samApwa ho
;rahI howI hE . 
;3- rikwawA ke XIre-XIre samApwa hone meM, maXya meM sWiwa rikwawA sUkRma se 
;sUkRma howI jA rahI howI hE . 
;
;-- pAsa meM honevAlA vyakwi samAna howA hE . isako hama isa prakAra soca sakawe hEM ki
;jo vixyArjana meM pAsa-pAsa hEM, veM vixyA kI xqRti se samAna hEM . isa waraha hara 
;kRewra meM socA jA sakawA hE . 
;
;-- pAsa se samAna,samAna se barAbara evaM barAbaravAloM se ekasAWa kA sambanXa jodA jA
;sakawA hE . kAraNa- barAbaravAloM kA eka sAWa rahanA . 
;
;    isa xiye gaye vivaraNa se yaha spaRta samaJA jA sakawA hE ki kisa prakAra isa
;eka `banxa hone' kI pUrNa-kriyA ke viviXArWa hue hEM .
;
;
