
(defrule show0
(declare (salience 5000))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-off_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKAvA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " show.clp show0 " ?id "  xiKAvA_kara )" crlf)) 
)

(defrule show1
(declare (salience 4900))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xiKAvA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " show.clp	show1  "  ?id "  " ?id1 "  xiKAvA_kara  )" crlf))
)
;@@@ Added by jagriti(30.11.2013)
;If you'd like to come this way, I'll show you out. [veena mam]
;अगर आप इस तरफ आना पसंद करेंगे ,तो मैं आपको दरबाजे तक छोड़ दूंगा .  
(defrule show2
(declare (salience 4800))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CodZa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " show.clp	show2 "  ?id "  " ?id1 "  CodZa_xe  )" crlf))
)
(defrule show3
(declare (salience 4700))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  show.clp 	show3   "  ?id "  xiKA )" crlf))
)

;$$$ Modified by Shirisha Manju ; added flower|fashion in the list
(defrule show4
(declare (salience 4600))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
(id-word ?id1 dog|flower|fashion)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praxarSanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  show.clp 	show4   "  ?id "  praxarSanI )" crlf))
)

(defrule show7
(declare (salience 4400))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiKA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  show.clp 	show7   "  ?id "  xiKA )" crlf))
(assert (kriyA_id-object2_viBakwi ?id ko))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object2_viBakwi   " ?*prov_dir* "  show.clp      show7   "  ?id " ko )" crlf)
)

;"show","V","1.xiKAnA"
;I will show him how to do it.
;They showed me their wedding photos.
;--"2.bawalAnA"
;I showed him the way out.
;--"3.sixXa karanA/pramANiwa karanA"
;They think I can't win but I'll show them.
;--"4.xiKAI xenA"
;I waited for him whole day but he never showed.
;
;


;@@@ Added by 14anu-ban-01 on (07-11-2014)
;Experiments show that all gases at low densities exhibit same expansion behavior.[NCERT corpus]
;प्रयोग यह दर्शाते हैं कि सभी गैसें कम घनत्व होने पर समान प्रसा-रंउचयआचरण दर्शाती हैं.[NCERT corpus]
;The arrangement of iron filings is shown in Fig. 5.2.[NCERT corpus]
;लौह रेतन की यह व्यवस्था चित्र 5.2 में दर्शायी गई है.[NCERT corpus]
(defrule show8
(declare (salience 4700))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 experiment|observation|result|output|arrangement)	;added arrangement by 14anu-ban-01 on (03-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xarSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  show.clp 	show8   "  ?id "  xarSA )" crlf))
)

;$$$ Modified by Bhagyashri Kulkarni (1-11-2016)
;###Because of AIDS several such symptoms start showing up which give the inkling of AIDS. (health)
;###एड्स की वजह से कई ऐसे लक्षण सामने आतें हैं कि जो एड्स का सङ्केत देता है .
;@@@ Added by 14anu-ban-01 on (02-02-2015)
;The taxi showed up on the dot.[oald]
;टैक्सी सही समय पर आ गई[self]
(defrule show9
(declare (salience 5000))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id2)	;Added by Bhagyashri to restrict
(id-root ?id2 taxi)             ;Added by Bhagyashri 
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " show.clp	show9 "  ?id "  " ?id1 "  A )" crlf))
)


;@@@ Added by 14anu-ban-11 on (09-03-2015)
;She's the star of the show! [oald]
;वह नाटक की मुख्य कलाकार है! (self)
(defrule show10
(declare (salience 5000))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 star)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAtaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  show.clp 	show10   "  ?id "  nAtaka)" crlf))
)

;$$$ Modified by Bhagyashri Kulkarni (1-11-2016)
;Because of AIDS several such symptoms start showing up which give the inkling of AIDS. (health)
;एड्स की वजह से कई ऐसे लक्षण सामने आतें हैं कि जो एड्स का सङ्केत देता है .
;@@@ Added by Bhagyashri Kulkarni (16.09.2016)
;The following consequences show up if a person is caught up with AIDS. (health)
;निम्नलिखित परिणाम सामने आते हैं यदि व्यक्ति को एड्स हुआ है .
(defrule show11
(declare (salience 5000))
(id-root ?id show)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-subject ?id ?id2)
(id-root ?id2 consequence|symptom)	;Added 'symptom' by Bhagyashri
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sAmane_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " show.clp	show11 "  ?id "  " ?id1 "  sAmane_A )" crlf))
)

;----------------- Removed rules ---------------------
;show5	
;	if (samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id) and (id-word ?id1 flower) then praxarSanI
;	Note : Merged in show4
;show6 
;	if (samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id) and (id-word ?id1 fashion) then praxarSanI
;	Note : Merged in show4
