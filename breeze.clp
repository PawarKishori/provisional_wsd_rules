;@@@Added by 14anu-ban-02(05-03-2016)
;Going down the hill would be a breeze after the long climb up![oald]
;लम्बी चढ़ाई के बाद पहाड़ी से उतरना आसान रहेगा![self]
(defrule breeze1
(declare (salience 100))
(id-root ?id breeze)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-after_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AsAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breeze.clp  	breeze1   "  ?id "  AsAna )" crlf))
)

;-----------------Default_rules-----------------------------
;@@@Added by 14anu-ban-02(05-03-2016)
;The flowers were gently swaying in the breeze.[oald]
;फूल मन्द हवा में हल्के से लहरा रहे थे.[self]
(defrule breeze0
(declare (salience 0))
(id-root ?id breeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manxa_havA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breeze.clp  	breeze0   "  ?id "  manxa_havA )" crlf))
)
