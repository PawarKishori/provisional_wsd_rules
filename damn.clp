;@@@Added by 14anu20 on 17/06/2014
;This is damn all.
;यह कुछ भी नही है.
(defrule damn2
(declare (salience 2400)) 
(id-root ?id damn)
?mng <-(meaning_to_be_decided ?id)                     
(id-root =(+ ?id 1) all)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) kuCa_BI_nahI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " damn.clp  damn2  "  ?id "  " (+ ?id 1) "  kuCa_BI_nahi )" crlf))
)

;Same rule added by 14anu22 with meaning "vyarWa"
;@@@Added by 14anu20 on 18/06/2014 and 14anu22 
;It was not worth a damn.
;वह मूल्यहीन था.
(defrule damn3
(declare (salience 2600))              
(id-root ?id damn)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) a)
(id-word =(- ?id 2) worth)
(id-word =(- ?id 3) not)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) =(- ?id 2) =(- ?id 3) mUlyahIna/vyarWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " damn.clp  damn3  "  ?id "  " (- ?id 1) "  " (- ?id 2) "  " (- ?id 3) "  mUlyahIna/vyarWa)" crlf))
)

;@@@ Added by 14anu22
;I do not give a damn.
;मैं परवाह नहीं  करता. 
(defrule damn5
(declare (salience 5000))
(id-root ?id damn)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) a)
(id-word =(- ?id 2) give)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng (- ?id 2)  (- ?id 1) ?id paravAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  damn.clp damn5 " (- ?id 2) " " (- ?id 1) " " ?id " paravAha)" crlf)))


;$$$ Modified by 14anu-ban-04 (12-01-2015)
;###[COUNTER EXAMPLE]### I damn them.
;###[COUNTER EXAMPLE]### मैं उनको धिक्कारता हूँ .
;@@@ Added by 14anu22
;Damn him.
;उसको लानत है .           
;भाड में जाइए .              ;translation corrected by 14anu-ban-04 (12-01-2015)
(defrule damn6
(declare (salience 5000))
(id-root ?id damn)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1)  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(AjFArWaka_kriyA ?id)              ;added by 14anu-ban-04 (12-01-2015)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) BAda_meM_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  damn.clp damn6 " ?id " " (+ ?id 1) " BAda_meM_jA)" crlf))
)

;-------------------------------DEFAULT RULES--------------------------------------------------------------------
;@@@Added by 14anu20 on 17/06/2014
;I damn them.
;मैं उनको धिक्कारता हूँ .
(defrule damn1
(declare (salience 40))
(id-root ?id damn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XikkAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damn.clp        damn1   "  ?id "  XikkAra )" crlf))
)

;$$$ Modified by 14anu-ban-04 (12-01-2015)       ------meaning corrected from 'lAnawa_hE' to 'lAnawa'
;@@@ Added by 14anu22
(defrule damn4
(declare (salience 30))                ;salience decreased from '4900' to '30' by 14anu-ban-04 (12-01-2015)
(id-root ?id damn)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAnawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  damn.clp 	damn4   "  ?id "  lAnawa )" crlf))
)



