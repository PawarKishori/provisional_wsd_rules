

;@@@ Added by Anita-11.4.2014
;His comments reduced her to tears . [cambridge dictionary]
;उसकी टिप्पड़ियों ने उसे रुला दिया ।
(defrule reduce1
(declare (salience 4000))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 tear)
(kriyA-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rulA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce1   "  ?id "  rulA_xe )" crlf))
)
;@@@ Added by Anita-11.4.2014
;The sergeant was reduced to the ranks for his cowardice. [cambridge dictionary]
;सार्जेंट के उसकी कायरता के लिए पद घटा दिये गये थे । 
(defrule reduce2
(declare (salience 4000))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 rank)
(kriyA-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GatA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce2   "  ?id "  GatA_xe )" crlf))
)
;@@@ Added by Anita-11.4.2014
;I'd run out of cigarettes and was reduced to smoking the butts left in the ashtrays. [cambridge dictionary]
; मेरी सिगरेट खतम हो गई थी और मैं एशट्रे में सिगरेट के बचे हुए टुकड़ों के धूम्रपान पर उतर आया था ।
(defrule reduce3
(declare (salience 4000))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 smoke)
(kriyA-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwara_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce3   "  ?id "  uwara_A )" crlf))
)
;$$$ Modified by 14anu-ban-10 on (13-10-2014)
;This shape reduces the weight of the beam without sacrificing the strength and hence reduces the cost.  [ncert corpus]
;isa prakAra kI Akqwi se prabalawA ko nyoCAvara kiye binA hI xaNda ke BAra ko kama kiyA jA sakawA hE, awaH lAgawa BI kama ho jAwI hE.
;[ncert corpus]
;@@@ Added by Anita-11.4.2014
;My weight reduces when I stop eating sugar. [cambridge dictionary]
;जब मैं चीनी खाना बंद कर देता हूँ तो मेरा वजन कम हो जाता है ।
(defrule reduce4
(declare (salience 4500))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-samakAlika_kriyA  ?id ?)(kriyA-kriyA_viSeRaNa ?id ? )) ; added 'kriyA-kriyA_viSeRaNa' relation by 14anu-ban-10 (13-10-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce4   "  ?id "  kama_ho )" crlf))
)
;@@@ Added by Anita -11.4.2014
;I reduced the problem to a few simple questions.  [cambridge dictionary]
;मैंने इस समस्या को कुछ सामान्य प्रश्नों का रूप  दिया ।
(defrule reduce5
(declare (salience 5000))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 question)
(kriyA-to_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce5   "  ?id "  rUpa_xe )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (16-04-2015)
;@@@ Added by 14anu-ban-10 on (21-11-2014)
;Efficient fallow management is an essential part of reducing erosion in a crop rotation system.[agriculture domain]
;फसल आवर्तन प्रणाली में  सक्षम बंजर भूमि प्रबंधन भू  परिवर्तक का  महत्वपूर्ण हिस्सा है.[manual]
(defrule reduce8
(declare (salience 5100))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)		
(id-root ?id1 erosion)		;added by 14anu-ban-05 on (16-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce8   "  ?id "  parivarwaka )" crlf))
)

;@@@ Added by 14anu-ban-10 on (13-10-2014)
;This shape reduces the weight of the beam without sacrificing the strength and hence reduces the cost.  [ncert corpus]
;isa prakAra kI Akqwi se prabalawA ko nyoCAvara kiye binA hI xaNda ke BAra ko kama kiyA jA sakawA hE, awaH lAgawa BI kama ho jAwI hE.[ncert corpus]
(defrule reduce6
(declare (salience 5200))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 weight)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kiyA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce6  "  ?id " kama_kiyA_jA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-11-2014)
;Your angular speed is reduced. [ncert corpus]
;ApakI koNIya cAla Gata jAwI hE. [ncert corpus]
(defrule reduce7
(declare (salience 5300))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-karma ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  Gata_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce7  "  ?id "  Gata_jA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (13-01-2015)
;The government's drive to reduce energy consumption
;सरकार का ऊर्जा खपत कम करने के लिए अभियान है . 
(defrule reduce9
(declare (salience 6000))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(to-infinitive  ? ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce9  "  ?id "  kama_kara) " crlf))
)

;@@@ Added by 14anu-ban-10 on (16-01-2015)
;As expected, the light from the lamp is reduced in intensity on passing through P2 alone.[ncert corpus]
;apekRAnusAra, lEMpa se Ane vAle prakASa kI wIvrawA kevala @P 2 se hI pAriwa hone meM kama ho jAegI.[ncert corpus]
(defrule reduce10
(declare (salience 6100))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce10  "  ?id "  kama_ho) " crlf))
)

;$$$ Modified by 14anu-ban-05 on (16-04-2015)
;Research has shown that hormone replacement therapy can reduce the risk of fracture by 50 to 60 per cent.[OALD]
;शोध दिखाता है कि हार्मोन प्रतिस्थापन उपचार  -50 से 60 प्रतिशत तक हड्डी टूटने  का खतरा कम कर सकता है .  [MANUAL]
;@@@ Added by 14anu-ban-10 on (04-02-2015)
;They drew up and executed a plan to reduce fuel consumption.[oald]
;उन्होंने  ईंधन  खपत को कम करने के लिए योजना बनाई और  कार्यान्वित की .[self]
(defrule reduce11
(declare (salience 6200))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)		
(id-root ?id1 consumption|reduce)		;added 'reduce'by 14anu-ban-05 on (16-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce11   "  ?id "  kama_kara )" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-02-2015)
;Her accusations reduced him to silence .[oald]
;उसके दोषारोपण ने उसे चुप कर दिया [self]
(defrule reduce12
(declare (salience 6300))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 silence )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   cupa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce12  "  ?id "  cupa_kara_xe) " crlf))
)
;
;###########################################default-rule#################################
;@@@ Added by Anita -11.4.2014
;The plane reduced speed as it approached the airport. [cambridge dictionary]
;हवाई -जहाज जैसे ही हवाई अड्डा पहुँचा , गति कम कर दी  ।
(defrule reduce_default-rule
(declare (salience 0))
(id-root ?id reduce)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-vAkya_subject  ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reduce.clp 	reduce_default-rule   "  ?id " kama_kara )" crlf))
)


