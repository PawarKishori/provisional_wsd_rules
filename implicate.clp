;@@@ Added by 14anu-ban-06 (09-02-2015)
;He tried to avoid saying anything that would implicate him further. (OALD)
;उसने कुछ भी कहने से टलने का प्रयास किया जो उसको और अधिक फँसाएगा . (manual)
(defrule implicate0
(declare (salience 0))
(id-root ?id implicate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PazsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implicate.clp 	implicate0   "  ?id "  PazsA )" crlf))
)

;@@@ Added by 14anu-ban-06 (09-02-2015)
;These findings implicate an inflammatory process in the pathogenesis . (COCA)
;ये जाँच परिणाम pathogenesis में उत्तेजक  प्रक्रिया की ओर इशारा करते हैं . (manual)
(defrule implicate1
(declare (salience 2000))
(id-root ?id implicate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 result|study|finding)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora_iSArA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  implicate.clp 	implicate1   "  ?id "  kI_ora_iSArA_kara )" crlf))
)
