;@@@ Added by Manasa ( 12-02-2016 )
;They stood in awe of the king.
(defrule awe2
(declare (salience 5000))
(id-root ?id awe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1 )
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vismayapUrNa_Axara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  awe.clp      awe2   "  ?id "  vismayapUrNa_Axara )" crlf))
)

;------------------------ Default Rules ----------------------

;$$$ Modified by Manasa ( 12-02-2016 )   meaning changed from vismayapUrNa_Axara to vismayapUrNa
;The inquiring and imaginative human mind has responded to the wonder and awe of nature in different ways.
;"awe","N","1.vismayapUrNa_Axara[dara]"
;My first view of the Lal Quila filled me with awe.
(defrule awe0
(declare (salience 5000))
(id-root ?id awe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vismayapUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  awe.clp 	awe0   "  ?id "  vismayapUrNa )" crlf))
)

;"awe","VT","1.vismayapUrNa_Axara[dara]_uwpanna_karanA"
;Ram's scholarship awed Sita.
;rAma kI vixvawwA sIwA ke mana meM vismayapUrNa Axara uwpanna karawI WI.
(defrule awe1
(declare (salience 4900))
(id-root ?id awe)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vismayapUrNa_Axara_uwpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  awe.clp 	awe1   "  ?id "  vismayapUrNa_Axara_uwpanna_kara )" crlf))
)

