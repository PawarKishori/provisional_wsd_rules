;##############################################################################
;#  Copyright (C) 2014-2015 Shalu Sneha (shalusneha1994@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;@@@ Added by 14anu01 on 26-06-2014
;Some  remote has too many buttons. 
;कुछ  रिमोट के ज्यादा ही बहुत बटन हैं .
(defrule remote0
(declare (salience 5000))
(id-root ?id remote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rimota))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remote.clp       remote0   "  ?id "  rimota )" crlf))
)


;@@@ Added by 14anu01 on 26-06-2014
;A remote Welsh valley.
;एक दूरवर्ती वेल्स की घाटी . 
(defrule remote1
(declare (salience 4000))
(id-root ?id remote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUravarwI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remote.clp       remote1   "  ?id "   xUravarwI )" crlf))
)


;@@@ Added by 14anu01 on 26-06-2014
;A remote cousin.
;एक दूरदराज के चचेरे भाई.
(defrule remote2
(declare (salience 5000))
(id-root ?id remote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root ?id1 cousin|relation|family)
;(id-root ?id1 cousin|genetic_relationship|relation|family)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUraxarAja_ke))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remote.clp       remote2   "  ?id "   xUraxarAja_ke)" crlf))
)

;@@@ Added by 14anu01 on 26-06-2014
; a golden age in the remote past.
;सुदूर अतीत में एक स्वर्ण युग.
(defrule remote3
(declare (salience 5000))
(id-root ?id remote)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root ?id1 past|future|antiquity|life_story|possibility)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  suxUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  remote.clp       remote3   "  ?id "    suxUra)" crlf))
)
