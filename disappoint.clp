;COMMENTED BY 14anu-ban-04 on (03-04-2015) BECAUSE meaning is coming from  dictionary and this rule should be in disappointing.clp
;Added by Meena(28.4.11)
;The debate was a pretty disappointing affair. 
;(defrule disppoint0
;(declare (salience 4000))
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id disappoint)
;(id-word ?id disappointing)
;(or(viSeRya-viSeRaNa  ?id1 ?id)(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id nirASAjanaka))
;;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disappoint.clp       disappoint0   "  ?id "  nirASAjanaka )" crlf))
;)

;@@@ Added by 14anu-ban-04 on (03-04-2015)
;The new government had soon disappointed the hopes of many of its supporters.              [oald]
;नयी सरकार ने अपने समर्थकों में से बहुतों की आशाएँ   सरलता से निष्फल की थीं .                           [self]
(defrule disappoint1
(declare (salience 30))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disappoint)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niRPala_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disappoint.clp       disappoint1   "  ?id "  niRPala_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 on (03-04-2015)
;We don't want to disappoint the fans.         [cald]	
;हम  प्रशंसकों का दिल नहीं तोड़ना  चाहते हैं .                    [self]
(defrule disappoint2
(declare (salience 40))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disappoint)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 fan)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kA))
(assert (id-wsd_root_mng ?id xila_woda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disappoint.clp     disappoint2   "  ?id " kA  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disappoint.clp       disappoint2  "  ?id "  xila_woda )" crlf))
)
;------------------------------------------------------DEFAULT RULE -----------------------------------------------------------------------


;@@@ Added by 14anu-ban-04 on (03-04-2015)
;The movie had disappointed her.              [oald] 
; फिल्म ने उसको निराश किया था .                       [self]     
(defrule disappoint0
(declare (salience 10))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disappoint)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id nirASa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disappoint.clp     disappoint0   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disappoint.clp       disappoint0  "  ?id "  nirASa_kara )" crlf))
)

