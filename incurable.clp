;@@@ Added by 14anu-ban-06  (10-09-2014)
;All the ancient medical texts state that prameha of any variety , if neglected , will finally lead to madhumeha and in due course become incurable .(parallel corpus)
;प्राचीन चिकित़्सा शास़्त्रों में यह कहा गया है कि यदि किसी भी प्रकार के प्रमेह को उपेक्षित किया जाये तो अंतत : वह मधुमेह बन जाता है जो बाद में लाइलाज हो जाता है .
;As a result the condition of the cells begins to worsen more , it moves more towards being incurable .(parallel corpus)
;फलस्वरूप कोशिका की दशा और बिगड़ने लग जाती है , वह और लाइलाज होने की ओर बढ़ जाती है ।
(defrule incurable0
(declare (salience 0))
(id-root ?id incurable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lAilAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incurable.clp 	incurable0   "  ?id "  lAilAja )" crlf))
)

;@@@ Added by 14anu-ban-06  (10-09-2014)
;Psychological disease and supposed progressive paralysis are the last and incurable forms .(parallel corpus)
;मानसिक रोग और तथाकथित वृद्धिशील लकवा प्रोग्रेसिव पैरेलिसिस अंतिम और असाध्य रूप हुआ करते हैं ।
;It has proved to be very beneficial in incurable diseases .(parallel corpus)
;यह आसाध्य रोगों में बहुत लाभकारी सिद्ध हुआ है ।
;From studies and researches it we have also considered it a disease seizing vegetable which reduces the risk of incurable diseases like cancer .(parallel corpus)
;अध्ययन और शोधों से इसे रोग हरने वाली सब्जी भी माना है जो कैंसर जैसे असाध्य रोग का खतरा कम करती है ।
;Treatment of incurable diseases take place in the magnificent hospital built by the American Missionary .(parallel corpus)
;अमेरिकी मिशनरी द्वारा निर्मित भव्य अस्पताल में असाध्य रोगों का उपचार किया जाता है ।
(defrule incurable1
(declare (salience 2000))
(id-root ?id incurable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 disease|illness|form)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asAXya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incurable.clp 	incurable1   "  ?id "  asAXya )" crlf))
)

