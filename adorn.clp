;@@@Added by 14anu-ban-02(24-02-2015)
;Sentence: The children adorned themselves with flowers.[oald]
;Translation: बच्चों ने फूलों से खुद को सजाया .  [anusaaraka]
(defrule adorn1
(declare (salience 100)) 
(id-root ?id adorn) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ?id1) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sajA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  adorn.clp  adorn1  "  ?id "  sajA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(27-02-2016)
; Beautiful paintings adorned the walls of the palace. [sd_verified]
;सुन्दर कलाकृतियों ने महल की दीवारों की शोभा बढाई . [self]
(defrule adorn2 
(declare (salience 100)) 
(id-root ?id adorn) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id SoBA_baDZA)) 
(assert (kriyA_id-object_viBakwi ?id kI))
(assert (kriyA_id-subject_viBakwi ?id ne))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  adorn.clp  adorn2  "  ?id "  SoBA_baDZA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  adorn.clp  adorn2   "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   "?*prov_dir* "  adorn.clp  adorn2   "  ?id " ne )" crlf)) 
) 

;-----------Default_rule-----------------------
;@@@Added by 14anu-ban-02(24-02-2015)
;Sentence: Her paintings adorn the walls.[mw]
;Translation:  उसकी कलाकृतियाँ दीवारों की शोभा बढाती हैं . [anusaaraka]
(defrule adorn0 
(declare (salience 0)) 
(id-root ?id adorn) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id SoBA_baDZA)) 
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  adorn.clp  adorn0  "  ?id "  SoBA_baDZA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  adorn.clp  adorn0   "  ?id " kI )" crlf)) 
) 
