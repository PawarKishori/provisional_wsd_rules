;@@@ Added by 14anu23 21/06/2014
;The cleft of the hoof should be narrow . 
;खुर की विदर को तंग होना चाहिए .
(defrule cleft0
(declare (salience 5000))
(id-root ?id cleft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cleft.clp 	cleft0   "  ?id "  vixara  )" crlf))
)


;Default rule 
;@@@ Added by 14anu23 21/06/2014
;A cleft chin.        [oxforddictionaries.com]
;एक विदीर्ण ठुड्डी . 
(defrule cleft1
(declare (salience 5000))
(id-root ?id cleft)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vixIrNa )) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cleft.clp 	cleft1   "  ?id "  vixIrNa  )" crlf))
)

