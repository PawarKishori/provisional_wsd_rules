;@@@ Added by 14anu-ban-06 (08-04-2015)
;We should forgive him a few youthful indiscretions.(cambridge)
;हमें उसके जवानी के कुछ अविवेकपूर्ण कार्य माफ कर देने चाहिए .(manual) 
(defrule indiscretion1
(declare (salience 2000))
(id-root ?id indiscretion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 youthful)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avivekapUrNa_kArya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indiscretion.clp 	indiscretion1   "  ?id "  avivekapUrNa_kArya )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (08-04-2015)
;He talked to the press in a moment of indiscretion. (OALD)
;उसने अविवेकपूर्ण कथन के एक क्षण में प्रैस से बातचीत की . (manual)
(defrule indiscretion0
(declare (salience 0))
(id-root ?id indiscretion)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avivekapUrNa_kaWana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  indiscretion.clp 	indiscretion0   "  ?id "  avivekapUrNa_kaWana )" crlf))
)
