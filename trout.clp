;@@@Added by 14anu-ban-07,(18-03-2015)
;Thousands of young salmon and trout have been killed by the pollution.(cambridge)
; हजारों छोटी सल्मोन् और ट्राउट मछलियाँ प्रदूषण के द्वारा मारी गयी हैं . (manual)
(defrule trout0
(id-word ?id trout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id trAuta_maCalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trout.clp 	trout0   "  ?id "  trAuta_maCalI )" crlf))
)

;@@@Added by 14anu-ban-07,(18-03-2015)
;She's a miserable old trout who complains about everything.(cambridge)
;वह एक दुखी बूढ़ी चिडचिडी बुढिया है जो सब-कुछ के बारे में शिकायत करती है .(manual)
(defrule trout1
(declare (salience 1000))
(id-word ?id trout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 old)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cidacidI_buDiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trout.clp 	trout1   "  ?id "  cidacidI_buDiyA )" crlf))
)

