
(defrule spring0
(declare (salience 0));salience reduced from 5000 to 0 by 14anu-ban-01 on (27-10-2014)
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id spriMga_lagA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spring.clp 	spring0   "  ?id "  spriMga_lagA_huA )" crlf))
)

;"spring","Adj","1.spriMga_lagA_huA"
;example added by 14anu-ban-01 on (27-10-2014)
;The springs in the sofa have gone.[oald]
;सोफे के स्प्रिंग खराब हो चुके हैं.[self]
(defrule spring1
(declare (salience 4900))
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id spriMga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spring.clp 	spring1   "  ?id "  spriMga)" crlf))
)

;"spring","N","1.spriMga"
;--"2.vasanwa"
;The best season is the spring season.
;
(defrule spring2
(declare (salience 4800))
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uCala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spring.clp 	spring2   "  ?id "  uCala )" crlf))
)
;"spring","V","1.uCalanA"
;Don't spring on bed it will break.


;@@@ Added by jagriti(11.12.2013)
;They would bathe in the hot spring there across the Ganga.
;वे गंगा के किनारे गर्म पानी के झरने में स्नान करेंगे.
(defrule spring3
(declare (salience 5000))
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JaranA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spring.clp 	spring3   "  ?id "  JaranA )" crlf))
)


;$$$ Modified by 14anu-ban-11 on (03-03-2015)
;@@@ Added by 14anu-ban-01 on (28-10-2014)
;The best season is the spring season.[spring.clp]
;वसन्त ऋतु ही सर्वोत्तम ऋतु है.[self]
(defrule spring4
(declare (salience 4900))
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1) ;Added by 14anu-ban-11 on (03-03-2015)
(id-root ?id1 season)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vasanwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spring.clp 	spring4   "  ?id "  vasanwa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-02-2015)
;The attacker sprang out at her from a doorway. (oald)
;आक्रामक उसपर द्वारपथ से  कूदा . (self)
(defrule spring5
(declare (salience 4901))
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ?id ?id1)
(id-root ?id1 doorway)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kUxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spring.clp 	spring5   "  ?id "  kUxa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-02-2015)
;The branch sprang back and hit him in the face.(oald)
;शाखा वापिस एकाएक आयी  और उसके  मुख पर  प्रहार किया . (self)
(defrule spring6
(declare (salience 4902))
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(kriyA-subject  ?id ?id2)
(id-root ?id1 back)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id ekAeka_AyI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  spring.clp 	spring6   "  ?id "  ekAeka_AyI)" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-02-2015)
;Plans to spring the hostages have failed.(oald)
;बन्धक व्यक्ति को निकालने की योजनाएँ असफल हो रहीं हैं . (self)
(defrule spring7
(declare (salience 4903))
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 hostage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spring.clp 	spring7   "  ?id "  nikAla)" crlf))
)


;@@@ Added by 14anu-ban-11 on (27-02-2015)
;Tears sprang to her eyes.(oald) 
;आँसू उसकी आँखों से निकला . (self)
(defrule spring8
(declare (salience 4904))
(id-root ?id spring)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI  ?id ?id1)
(id-root ?id1 eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  spring.clp 	spring8   "  ?id "  nikala)" crlf))
)


;
;LEVEL 
;
;
;"spring","N","1.basanwa"-basanwa qwu meM hara jagaha pIlI sarasoM sPutiwa howI hE va  
;              KuSiyAlI kA vAwAvaraNa rahawA hE-sPuraNa howA hE-uCAla
;
;The best season is the spring season.
;
;"2.GumAvaxAra_wAra/sprigaM"-xabAne ke uparAnwa punaH uCala kara vApisa apane sWAna para 
;                AjA  jAwI hE-uCAla
;Don't bounce on the mattress,you will break the springs. 
;
;"3.JaranA" -pqWvI ke anxara se sPuriwa howA hE-uCAla
;There are some springs which give hot water. 
;
;"4.praPullawA/sPUrwi" -sPuriwa honA-uCAla
;The springs of student action,shows their keen intrest.   
;
;"5.kUxa/CalAMga"-uCAla
;With an easy spring the dog reached the boundry wall.
;
;"spring","V","1.uCalanA"-uCAla
;Don't spring on bed it will break.
;
;"2.uwpanna honA/pExA honA"-sPuriwa honA-uCAla
;The belief has sprung up.
;
;"3.ekAeka prakata honA"-sPuriwa honA -uCAla
;Where did you spring from?
;
;"4.visPota honA/calAnA" -acAnaka kArya kA honA -sPuriwa honA -uCAla
;At last the engine sprang into life.  
;
;    nota :-- uparyukwa aMkiwa 'spring'Sabxa ke saBI kriyA Ora saMjFA vAkyoM kA
;         nirIkRaNa karane para xqRtigocara howA hE ki saBI arWa eka nABikIya Sabxa
;         'uCAla'se spaRta ho sakawe hEM.isakA sUwra nimna prakAra liKa sakawe hEM
;
;sUwra : uCAla   
;
