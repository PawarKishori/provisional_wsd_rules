
(defrule struck0
(declare (salience 5000))
(id-root ?id struck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  struck.clp 	struck0   "  ?id "  praBAviwa )" crlf))
)


;Commented by 14anu-ban-01 on (02-02-2015) because it should be handled in strike.clp [and the rule strike13 is already there with the same meaning]
;(defrule struck1
;(declare (salience 4900))
;(id-root ?id struck)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id mArA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  struck.clp 	struck1   "  ?id "  mArA )" crlf))
;)

;Teacher struck me because I was talking.
;
;
;@@@ Added by 14anu-ban-01 on (02-02-2015)
;Sentence - In other words, it is a struck bow.
;[source:https://www.google.co.in/search?q=google&ie=UTF-8&sa=Search&channel=fe&client=browser-ubuntu&hl=en&gws_rd=cr&ei=PPnMVI2YCeHMmwXRl4LgDQ#channel=fe&hl=en-IN&q=%22In+other+words%2C+it+is+a+struck+bow.%22]
;अन्य शब्दों में, यह एक धनुष-वाद्यम् है .[self]
(defrule struck1
(declare (salience 5000))
(id-root ?id struck)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 bow)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 XanuRa-vAxyam))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " struck.clp 	struck1  "  ?id "  " ?id1 "  XanuRa-vAxyam  )" crlf))
)

