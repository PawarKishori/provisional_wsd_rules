;@@@Added by 14anu-ban-02(24-03-2015)
;Regular exercise has a direct bearing on fitness and health.[oald]
;नियमित व्यायाम का  दुरुस्ती और स्वास्थ्य से सीधा सम्बन्ध है . [self]
(defrule bearing1 
(declare (salience 100)) 
(id-root ?id bearing) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 direct)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sambanXa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bearing.clp  bearing1  "  ?id "  sambanXa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(24-03-2015)
;They lost their bearings in the dark.[cald]
;उन्होंने अन्धेरे में अपना पता  खो दिया. [self]
(defrule bearing2 
(declare (salience 100)) 
(id-root ?id bearing) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object  ?id1 ?id)
(kriyA-in_saMbanXI  ?id1 ?id2)
(id-root ?id2 dark)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pawA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bearing.clp  bearing2  "  ?id "  pawA )" crlf)) 
)


;-----------------default_rule--------------------------


;@@@Added by 14anu-ban-02(24-03-2015)
;Sentence: Her whole bearing was alert.[oald]
;Translation: उसका पूरा आचरण सतर्क था . [self]
(defrule bearing0 
(declare (salience 0)) 
(id-root ?id bearing) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id AcaraNa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bearing.clp  bearing0  "  ?id "  AcaraNa )" crlf)) 
) 


