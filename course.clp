;$$$Modified by 14anu-ban-02(27-02-2016)
;Added by Meena(17.02.10)
;The last part of the course was hard because I was running against the wind . 
;दौड़ने के लिये बने मार्ग का आखिरी भाग कठिन था क्योंकि मैं हवा के विरुद्ध दौड़ रहा था . [sd_verified] 	;Added by 14anu-ban-02(27-02-2016)
(defrule course0
(declare (salience 5000))
(id-root ?id course)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
;(id-root ?id1 run)	;commented by 14anu-ban-02(27-02-2016)
(id-root ?id1 part)	;Added by 14anu-ban-02(27-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOdZane_ke_liye_bane_mArga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  course.clp    course0   "  ?id "  xOdZane_ke_liye_bane_mArga )" crlf))
)


;$$$ Modified 14anu-ban-03 (15-12-2014)
;;@@@ Added by 14anu23 24/06/2014
;The course of the disease is rapid and the animal dies within a few days .
;यह रोग बडी तेजी से असर करता है और जानवर कुछ दिन के भीतर ही मर जाता है .
;बीमारी की अवधि शीघ्र है और पशु कुछ दिनों में मर जाता है . 
(defrule course3
;(declare (salience 0)) ;commented by 14anu-ban-03 (15-12-2014)
(declare (salience 5000))  ;uncommented by 14anu-ban-03 (15-12-2014)
(id-root ?id course)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(kriyA-subject ?id1 ?id)   ;commented by 14anu-ban-03 (15-12-2014)
(viSeRya-of_saMbanXI ?id ?id1)  ;added by 14anu-ban-03 (15-12-2014)
(id-root ?id1 disease)  ;added by 14anu-ban-03 (15-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  course.clp 	course3   "  ?id "  avaXi )" crlf))
)

;@@@ Added by 14anu13 on 03-07-14
;The president appears likely to change course on some key issues.    [sentence from http://www.oxfordlearnersdictionaries.com/definition/english/course_1]
;राष्ट्रपति द्वारा कुछ महत्वपूर्ण मुद्दों पर नियम बदलने की संभावना दिखाई देती है. 
(defrule course03
(declare (salience 5000))
(id-root ?id course)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object 	?id1  ?id)
(id-root ?id1 change|make|amend)
(kriyA-on_saMbanXI  ?id1  ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  course.clp 	course03   "  ?id "  niyama )" crlf))
)

;@@@ Added by 14anu13 on 03-07-14
;She was overtaken on the last stretch of the course.    [sentence from http://www.oxfordlearnersdictionaries.com/definition/english/course_1]
;वह  दौड़ की अंतिम अवधि में पिछड़ गयी थी|
(defrule course4
(declare (salience 5100))
(id-root ?id course)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id2  ?id)
(kriyA-on_saMbanXI ?id1  ?id2)
(id-root ?id1 overtake)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xOdZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  course.clp 	course4   "  ?id "  xOdZa )" crlf))
)

;@@@ Added by 14anu13 on 03-07-14
;The path follows the course of the river.    [sentence from http://www.oxfordlearnersdictionaries.com/definition/english/course_1]
;मार्ग नदी की दिशा का अनुसरण करता है |
(defrule course5
(declare (salience 5400))
(id-root ?id course)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1  ?id)
(kriyA-subject ?id1  ?)
(id-root ?id1 follow)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xiSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  course.clp 	course5   "  ?id "  xiSA )" crlf))
)

;@@@ Added by 14anu13 on 03-07-14
;It is possible that in the course of time a cure for cancer will be found.   [sentence from http://www.oxfordlearnersdictionaries.com/definition/english/course_1]
;यह संभव है कि समय के साथ कैंसर की दवा मिल जाये |
(defrule course6
(declare (salience 5700))
(id-root ?id course)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 time)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  course.clp 	course6   "  ?id "  sAWa )" crlf))
)

;@@@ Added by 14anu13 on 03-07-14
;Don't you like my mother?’ ‘Of course, I like her!’   [sentence from http://www.oxfordlearnersdictionaries.com/definition/english/course_1]
;क्या तुम मेरी माँ को पसंद नही करते ? 'बेशक,मैं उन्हें पसंद करता हूँ!'
(defrule course7
(declare (salience 5900))
(id-root ?id course)
(id-word =(- ?id 1) of)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) beSaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " course.clp	course7  "  ?id "  "(- ?id 1) "  beSaka  )" crlf))
)

;----------------------------- Default rules ------------------------
(defrule course1
(declare (salience 0))
;(declare (salience 5000))
(id-root ?id course)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pATyakrama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  course.clp 	course1   "  ?id "  pATyakrama )" crlf))
)

(defrule course2
(declare (salience 4900))
(id-root ?id course)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  course.clp 	course2   "  ?id "  baha )" crlf))
)

;"course","V","1.bahanA"
;The river coursed down the hill.
;
;
