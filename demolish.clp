;@@@ Added by 14anu-ban-04 (18-02-2015)
;The car had skidded across the road and demolished part of the wall.         [olad]
;गाड़ी सड़क के उस पार फिसली थी और दीवार का भाग नष्ट कर दिया था .                                  [self]         
(defrule demolish1
(declare (salience 20))
(id-root ?id demolish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id naRta_kara_xe/samApwa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " demolish.clp 	demolish1   "  ?id "    naRta_kara_xe/samApwa_kara_xe )" crlf))
)

;@@@ Added by 14anu-ban-04 (18-02-2015)
;The children demolished their burgers and chips.                  [oald]
;बच्चों ने अपने बर्गर और चिप्स खाए .                                          [self]
;Joe demolished an enormous plateful of chicken and fries.         [cald]
;जोव ने मुर्गी और चिप्स की बहुत बड़ी प्लेट भर कर खायी .                                [self]
(defrule demolish2
(declare (salience 30))
(id-root ?id demolish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-object ?id ?id1)(and(kriyA-object ?id ?id2)(viSeRya-of_saMbanXI ?id2 ?id1)))
(id-root ?id1 burger|chip|chicken|fries)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " demolish.clp 	demolish2   "  ?id "   KA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (18-02-2015)
;A number of houses were demolished so that the supermarket could be built.              [oald]
;कई घर ध्वस्त  कर दिए गये थे जिससे कि सुपर बाजार बनाया जा सके .                          [self]  ;modified translation by 14anu-ban-04 on (13-03-2015)
(defrule demolish0
(declare (salience 10))
(id-root ?id demolish)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xvaswa_kara_xe))               ;changed meaning from 'girA' to  'Xvaswa_kara_xe' by 14anu-ban-04 on (13-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " demolish.clp 	demolish0   "  ?id "  Xvaswa_kara_xe)" crlf))    ;changed meaning from 'girA' to  'Xvaswa_kara_xe'    by 14anu-ban-04 on (13-03-2015)
)


