;----------------------------------DEFAULT RULE-------------------------------------
;@@@ Added by 14anu-ban-01 on (24-09-14)
;This slab is very heavy.[self]
;यह शिला बहुत भारी है.[self]
;You will observe that the wire passes through the ice slab. [NCERT Corpus]
;आप यह देखेंगे कि तार हिमशिला में से पार हो जाता है.[NCERT Corpus]
(defrule slab0
(id-root ?id slab)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SilA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slab.clp 	slab0   "  ?id " SilA )" crlf))
)

;---------------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on (24-09-14)
;Show me a slab of marble.[self: with reference to oald]
;मुझे एक संगमरमर की पटिया दिखाओ.[self]
(defrule slab1
(id-root ?id slab)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 marble|concrete)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id patiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slab.clp 	slab1  "  ?id "  patiyA" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-09-14)
;The road was paved with smooth stone slabs.[oald]
;सडक/मार्ग को चिकनी पत्थर की पट्टियों से प्रशस्त किया गया था. [self]
;Keep this bowl on the kitchen slab.[self]
;यह कटोरा रसोईघर की पट्टी पर रख दो.[self]
(defrule slab2
(id-root ?id slab)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 kitchen|stone)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pattI))
(assert (id-wsd_viBakwi ?id1 kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slab.clp 	slab2  "  ?id "  pattI" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  slab.clp     slab2  "  ?id1 " kI)" crlf))
)
