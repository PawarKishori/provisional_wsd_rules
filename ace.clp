;He was an ace in his field.	;added by 14anu-ban-02(16-01-2015) run this sentence on parser 15
;वह अपने क्षेत्र में बहुत अच्छा था. 
(defrule ace0
(declare (salience 5000))
(id-root ?id ace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ace.clp 	ace0   "  ?id "  bahuwa_acCA )" crlf))
)

;"ace","Adj","1.bahuwa_acCA"
;V.Anand is an ace chess player.
;
(defrule ace1
(declare (salience 4900))
(id-root ?id ace)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ikkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ace.clp 	ace1   "  ?id "  ikkA )" crlf))
)

;"ace","N","1.ikkA{wASa_kA)"
;I just need an ace of diamond to win the game.
;--"2.kuSala"
;V.Anand is an ace chess player of India.
;--"3.{tenisa_meM)sarvisa_jo_prawixvanxI_nahIM_Kela_pAwA"
;Pete Sampras hit 21 aces in yesterday's match.
;

;commented by 14anu-ban-02(16-01-2015)
;correct meaning is coming from ace0 on running it on parser15
;@@@ Added by 14anu06 Vivek Agarwal, MNNIT Allahabad on 28/6/2014*****
;He was an ace in his field.
;वह अपने क्षेत्र में बहुत अच्छा था. 
;(defrule ace2
;(declare (salience 4900))
;(id-root ?id ace)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(subject-subject_samAnAXikaraNa  ?id1 ?id)
;(id-root ?id1 he|she|they|them)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id bahuwa_acCA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ace.clp 	ace2   "  ?id "  bahuwa_acCA )" crlf))
;)

;$$$ Modified by 14anu-ban-02 (26-11-2014)
;@@@ Added by 14anu06 Vivek Agarwal, MNNIT Allahabad on 28/6/2014*****
;He hit twelve aces in the game.
;उसने खेल में बारह मारी.
;उसने खेल में बारह ऐस मारी.[manual] added by 14anu02
(defrule ace3
(declare (salience 4900))
(id-root ?id ace)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id);Added by 14anu-ban-02
(id-cat_coarse ?id noun)
(id-root ?id1 tennis|badminton|play|player|point|hit|shot)
;(test(!= ?id1 0)) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Esa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ace.clp 	ace3   "  ?id "  Esa )" crlf))
)
;
