;@@@ Added by 14anu-ban-04 (19-11-2014)
;If you have attachment to birds and the environment then just leave this weekend for Sultanpur Bird Sanctuary .  [tourium-corpus]
;पक्षी और पर्यावरण से आपको लगाव है तो बस अब इस वीकएंड पर निकल जाइए सुल्तानपुर बर्ड सेंचुरी ।                                              [tourium-corpus]
(defrule environment0
(declare (salience 100))
(id-root ?id environment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun )
(id-word =(- ?id 1) the)              ;added by 14anu-ban-04 (27-01-2015)
=>
(retract ?mng) 
(assert (id-wsd_root_mng ?id paryAvaraNa))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  environment.clp 	environment0   "  ?id " paryAvaraNa )" crlf))
)

;@@@ Added by 14anu-ban-04 (19-11-2014)
;In addition, plants produce root exudates and other chemicals which manipulate their soil environment as well as their weed environment.  [agriculture] 
;इसके अलावा,पौधें जड़  एक्स्युडेट और अन्य रसायनों को बनाते हैं जो उनके मिट्टी के परिवेश के साथ ही उनके घास फूस परिवेश से काम लेती  हैं |  [manual]
(defrule environment1
(declare (salience 200))
(id-root ?id environment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun )
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)
=>
(retract ?mng) 
(assert (id-wsd_root_mng ?id pariveSa))         
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  environment.clp  environment1   "  ?id " pariveSa)" crlf))
)

;@@@ Added by 14anu-ban-01 (14-02-2016)
;One kind of response from the earliest times has been to observe the physical environment carefully, look for any meaningful patterns and relations in natural phenomena, and build and use new tools to interact with nature.	[ncert corpus]
;प्राचीन काल से मानव की एक प्रकार की प्रतिक्रिया यह रही है कि उसने अपने भौतिक पर्यावरण का सावधानीपूर्वक प्रेक्षण किया है, प्राकृतिक परिघटनाओं में अर्थपूर्ण पैटर्न तथा सम्बन्ध खोजे हैं, तथा प्रकृति के साथ क्रिया-प्रतिक्रिया कर सकने के लिए नए औजारों को बनाया तथा उनका उपयोग किया है.	[translation improved by Chaitanya Sir]
(defrule environment3
(declare (salience 200))
(id-root ?id environment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun )
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 physical)
=>
(retract ?mng) 
(assert (id-wsd_root_mng ?id paryAvaraNa))         
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  environment.clp  environment3   "  ?id " paryAvaraNa)" crlf))
)

;---------------------------------------------------------DEFAULT RULE --------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (27-01-2015)
;Even with the rapidly changing environment of industrialization , you can find unwavering , pure delight in Jharkhand .    [tourism corpus]
;तेजी  से  औद्योगीकरण  के  बदलते  हुए  माहौल  में  भी  झारखंड  में  आप  निर्मल  और  निश्छल  खुशी  प्राप्त  कर  सकते  हैं  ।                                    [tourism corpus]
(defrule environment2
(declare (salience 10))
(id-root ?id environment)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun )
=>
(retract ?mng) 
(assert (id-wsd_root_mng ?id vAwAvaraNa))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  environment.clp 	environment2   "  ?id " vAwAvaraNa )" crlf))
)

