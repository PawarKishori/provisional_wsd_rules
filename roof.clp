;@@@ Added by 14anu-ban-10 on (25-03-2015)
;Roof of your mouth .[oald]
;आपके मुँह की  तालू . [manual]
(defrule roof2
(declare (salience 5100))
(id-root ?id roof)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1 )
(id-root ?id1 mouth)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAlU))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof2   "  ?id "  wAlU)" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-03-2015)
;The roof of the cave was very high.[oald]
;गुफा का छप्पर बहुत ऊँचा था | [manual]
(defrule roof3
(declare (salience 5200))
(id-root ?id roof)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1 )
(id-root ?id1 cave)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cappara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof3   "  ?id "  Cappara)" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-03-2015)
;Birds like to sit on the roof of a house .[oald]
;चिडियाँ  घर की छान मे अड्डे पर बैठना  पसन्द करती हैं .  [manual]
(defrule roof4
(declare (salience 5300))
(id-root ?id roof)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 bird)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof4   "  ?id "  CAna)" crlf))
)

;;@@@ Added by 14anu-ban-10 on (25-03-2015)
;;The shopping centre is not roofed over.[oald]
;;मौल से उपर  छप्पर डाला नहीं गया है . [manual]
;(defrule roof5
;(declare (salience 5400))
;(id-root ?id roof)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-upasarga  ?id ? )
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Cappara_dAlA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof5   "  ?id "  Cappara_dAlA)" crlf))
;)

;@@@ Added by 14anu-ban-10 on (25-03-2015)
;The shopping centre is not roofed over.[oald]
;मौल से उपर  छप्पर डाला नहीं गया है . [manual]
(defrule roof6
(declare (salience 5400))
(id-root ?id roof)
?mng <-(meaning_to_be_decided ?id)
(kriyA-upasarga  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cappara_dAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof6   "  ?id "  Cappara_dAlA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-03-2015)
;Their cottage was roofed with green slate.[oald]
;उनका मकान का छप्पर हरारंग की स्लेट की पट्टी से  बनी  थी.[manual]
(defrule roof7
(declare (salience 5500))
(id-root ?id roof)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cappara_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof7   "  ?id "  Cappara_banA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-03-2015)
;House prices here have gone through the roof.[oald]
;यहा पर घर के मूल्य शीर्ष  तक चुके हैं . [manual]
(defrule roof8
(declare (salience 5600))
(id-root ?id roof)
?mng <-(meaning_to_be_decided ?id)
(kriyA-through_saMbanXI  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 price)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SIrRa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof8   "  ?id "  SIrRa )" crlf))
)

;------------------------ Default Rules ----------------------

;"roof","N","1.Cawa"
;Our house is very small but at least we've got a roof  over our heads. 
(defrule roof0
(declare (salience 5000))
(id-root ?id roof)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof0   "  ?id "  Cawa )" crlf))
)

;"roof","VT","1.Cawa_pAtanA"
;Her house is roofed with red tiles.
(defrule roof1
(declare (salience 4900))
(id-root ?id roof)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cawa_pAta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  roof.clp 	roof1   "  ?id "  Cawa_pAta )" crlf))
)


