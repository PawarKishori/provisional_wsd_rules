
;The movie could not catch on.
;Pilma viKyAwa nahIM ho sakI
(defrule catch0
(declare (salience 5000))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 viKyAwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " catch.clp	catch0  "  ?id "  " ?id1 "  viKyAwa_ho  )" crlf))
)

;The audience soon catched on that he is only joking.
;SrowA wuranwa samaJa gae ki vaha sirPa majZAka kara rahA hE
(defrule catch1
(declare (salience 4900))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " catch.clp	catch1  "  ?id "  " ?id1 "  samaJa  )" crlf))
)

;You go on ahead,and i'll catch you up later.
;wuma Age jAo ,mEM wumhe bAxa meM pakadZa lUzgA
(defrule catch2
(declare (salience 4800))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " catch.clp	catch2  "  ?id "  " ?id1 "  pakadZa  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (20-02-2015)
;We caught him up although he was walking very fast. [same clp]
;hama usake pAsa pahuzca gae hAlAMki vaha bahuwa wejZa cala rahA WA.[same clp]
(defrule catch3
(declare (salience 4700))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
;(kriyA-upasarga ?id ?id1)  ;commented by 14anu-ban-03 (20-02-2015)
(kriyA-kriyA_viSeRaNa ?id ?id1)  ;added by 14anu-ban-03 (20-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pAsa_pahuzca_jA))  ;meaning changed from 'kisI_ke_pAsa_pahuzca' to 'pAsa_pahuzca_jA' by 14anu-ban-03 (20-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " catch.clp	catch3  "  ?id "  " ?id1 "  pAsa_pahuzca_jA  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (20-02-2015)
;She had a lot of work to catch up on after the vacations. [same clp]
;CuttiyoM ke bAxa use bahuwa sA kAma pUrA karanA WA. [same clp]
(defrule catch4
(declare (salience 4900))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)		;commented by 14anu-ban-03 (20-02-2015)
(viSeRya-of_saMbanXI ?id2 ?id3)	;added by 14anu-ban-03 (20-02-2015)
(id-root ?id2 lot)		;added by 14anu-ban-03 (20-02-2015)
(id-root ?id3 work)		;added by 14anu-ban-03 (20-02-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrA_kara)) ;meaning changed from 'kisI_aXUre_kAma_ko_pUrA_kara' to 'pUrA_kara' by 14anu-ban-03 (20-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " catch.clp	catch4  "  ?id "  " ?id1 " pUrA_kara  )" crlf))
)

(defrule catch5
(declare (salience 4500))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " catch.clp	catch5  "  ?id "  " ?id1 "  pakadZa  )" crlf))
)

;$$$  Modified by Preeti(19.12.13) ,Translation added
;Added by Meena(11.11.09)
;The punch caught him in the back. 
;usako pITa meM mukkA  lagA.
(defrule catch6
(declare (salience 4100))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 rock|punch|blow) ;Uncommented by Preeti
;(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "inanimate.gdbm" ?str)))
(id-root ?id1 ?str);As suggested by Chaitanya Sir removed inanimate.gdbm and modified the fact as shown by Roja (03-12-13) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(kriyA-object ?id ?id2)
;(id-root ?id2 him|her|me|them|us|Hari)
(or(id-root ?id2  ?str1&:(and (not (numberp ?str1))(gdbm_lookup_p "animate.gdbm" ?str1)))(id-cat_coarse ?id2 PropN))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catch.clp     catch6   "  ?id "  laga )" crlf))
)


;@@@ Added by Prachi Rathore[11-3-14]
; I caught the aroma of coffee.[wordnet]
;MuJe kaZPI kI sugaMXa AyI
(defrule catch9
(declare (salience 4000))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 aroma)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id A))
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (kriyA_id-object_viBakwi ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catch.clp 	catch9   "  ?id "  A )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  catch.clp 	catch9    "  ?id "  ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  catch.clp 	catch9    "  ?id "  kI )" crlf))
)

;$$$ Modified by 14anu-ban-06  Karanveer Kaur (Banasthali Vidyapith) [19-07-2014] 
;She caught sight of a car in the distance.
;usane xUrI para kAra kA xarSya xeKA.
;@@@ Added by Prachi Rathore[11-3-14]
; Catch a glimpse.[wordnet]
;Jalaka xeKo.
(defrule catch10
(declare (salience 4000))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 glimpse|sight) ;added sight by 14anu-ban-06
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catch.clp 	catch10   "  ?id "  xeKa )" crlf))
)

;@@@ Added by 14anu11
;Caught in this conflict , he asked Basava for cart - loads of grains .
;इस द्वन्द्व में घिर कर उसने बसव से छकडा भर अनाज मांगा .
(defrule catch11
(declare (salience 8000))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(AjFArWaka_kriyA  ?id)
(kriyA-in_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gira_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catch.clp 	catch11   "  ?id "  Gira_kara )" crlf))
)

;@@@ Added by 14anu-ban-03 (20-02-2015)
;The rabbit's foot was caught in a snare.(oald)
;खरगोश का पाँव जाल में फँस  गया था . (self)
(defrule catch12
(declare (salience 4500))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 snare)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pazsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catch.clp 	catch12   "  ?id "  Pazsa )" crlf))
)

;@@@ Added by Bhagyashri Kulkarni (16.09.2016)
;The following consequences show up if a person is caught up with AIDS. (health)
;निम्नलिखित परिणाम सामने आते हैं यदि व्यक्ति को एड्स हुआ है .
(defrule catch13
(declare (salience 4800))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-with_saMbanXI ?id ?id2)
(id-root ?id2 AIDS)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ho))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " catch.clp	catch13  "  ?id "  " ?id1 "  ho  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  catch.clp 	catch13    "  ?id "  ko )" crlf))
)


;======================== Default Rules =====================

;"catch","N","1.pakadZa"
;Jonty Rhodes made a superb catch of the ball at the boundaryline.
;Salience reduced by Meena(11.11.09)
(defrule catch7
(declare (salience 1000))
;(declare (salience 4100))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catch.clp    catch7   "  ?id "  pakadZa )" crlf))
)

;$$$ Modified by Shirisha Manju (20-05-2016) Suggested by Chaitanya Sir
;He caught the hare with some hesitation. 
;usane kuCa hicakicAhata ke sAWa KaragoSa ko pakadA.
;Salience reduced by Meena(11.11.09)
;Sentence added by Preeti (19.12.13)
;It had caught a bull by the throat and was dragging it away. [By mail]
;usane  sAzda ko gale se pakadzA WA Ora use KIMca rahA WA.
;"catch","V","1.pakadZanA"
;He caught the ball superbly.
(defrule catch8
(declare (salience 1000))
;(declare (salience 4000))
(id-root ?id catch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakada))
(assert (kriyA_id-object_viBakwi ?id ko)) ;added by Shirisha manju (20-05-2016)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  catch.clp    catch8   "  ?id "  pakada )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  catch.clp    catch8   "  ?id " ko )" crlf))
)

