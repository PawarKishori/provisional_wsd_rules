;@@@ Added by 14anu03 on 18-june-14
; In 1969 Susan moved from Ithaca to California where she met her husband-to-be, who, ironically, also came from upstate New York.
;1969 में सुसं ने इथाका कैलिफोर्निया को स्थानान्तरित किया जहाँ वह उसके husband-to-be को, मिली व्यङ्ग्यपूर्वक,जो राज्य के वह भाग जो मुख्य शहर से दूर हो नेव् यौर्क से भी आया., 
(defrule who100
(declare (salience 6500))
(id-root ?id who)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-word ?id1 came)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  who.clp      who100   "  ?id " jo)" crlf))
)

(defrule who0
(declare (salience 5000))
(id-root ?id who)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) tell)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kOna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  who.clp 	who0   "  ?id "  kOna )" crlf))
)

(defrule who1
(declare (salience 4900))
(id-root ?id who)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) to)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kOna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  who.clp 	who1   "  ?id "  kOna )" crlf))
)

;I did not know who to ask
(defrule who2
(declare (salience 4800))
(id-root ?id who)
?mng <-(meaning_to_be_decided ?id)
(subject_question_wh_type )
;(praSnAwmaka_vAkya      )
;;(id-word 1 ?id)
(test (eq ?id 1)) ;Commented above line and added test condition by Roja 04-11-13 automatically by a programme.
(id-root ?id1  ?)
(kriyA-subject ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kOna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  who.clp 	who2   "  ?id "  kOna )" crlf))
)

(defrule who3
(declare (salience 4700))
(id-root ?id who)
?mng <-(meaning_to_be_decided ?id)
(wh_question)
;(praSnAwmaka_vAkya      )
(id-cat_coarse ?id wh-pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kOna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  who.clp 	who3   "  ?id "  kOna )" crlf))
)

;Rama who is my brother is coming tomorrow.(Ex: and translation suggested by Chaitanya Sir)
;rAma jo merA BAI hE kala A rahA hE.
(defrule who4
(declare (salience 4600))
(id-root ?id who)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-jo_samAnAXikaraNa ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  who.clp    who4   "  ?id "  jo )" crlf))
)
 

;"who","Pron","1.kOna"
;Who is the woman in the red coat?.
;


;@@@ Added by 14anu-ban-11 on (12-11-2014)
;Soy-based infant formula (SBIF) is sometimes given to infants who are not being strictly breastfed; it can be useful for infants who are either allergic to pasteurized cow milk proteins or who are being fed a vegan diet.(agriculture)
;सोया आधारित शिशु फार्मूला ( SBIF ) कभी कभी दिया जाता है शिशुओं को  जिन्हे  पूरी तरह से  स्तनपान नहीं किया जा रहा है  ; यह हो सकता है  उपयोगी  हो शिशुओं के लिए जिन्हे  या एलर्जी कर रहे हैं  पाश्चरीकृत गाय दूध प्रोटीन या जिन्हे   एक शाकाहारी आहार खिलाया जा रहा  हैं  
;It was Nikolai Vavilov, the Russian botanist who first realized the importance of crop wild relatives in the early 20th century.(agriculture)
;Nikolai Vavilov था, एक Russian वनस्पतिज्ञ जिसने पहले जंगली फसल संबंधी महत्तव को श्रवण के २०th शतक में समझाया था.(manual)
;This experiment was later performed around 1911 by Hans Geiger (1882 — 1945) and Ernst Marsden (1889 — 1970, who was 20 year-old student and had not yet earned his bachelor's degree).(NCERT)
;यह प्रयोग कुछ समय पश्चात सन् 1911 में हैंस गाइगर (1882 – 1945) तथा अर्नेस्ट मार्सडन (1889 – 1970, जो 20 वर्षीय छात्र थे तथा जिन्होंने अभी स्नातक की उपाधि भी ग्रहण नहीं की थी) ने किया.(manual)
(defrule who5
(declare (salience 4000))
(id-root ?id who)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-root ?id1 pasteurize|feed|realize|student) ;Added "student" by 14anu-ban-11 on (28-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  who.clp    who5   "  ?id "  jo)" crlf))
)

;@@@ Added by 14anu17
;They are also given to immediate family members and anyone else who is in close contact with the person who has the disease . 
;वे तात्कालिक परिवारों सदस्यों को और किसी को भी दिए गया हैं  जो  कोई व्यक्ति के साथ निकट सम्पर्क में है जिसको बीमारी है . 
(defrule who6
(declare (salience 100))
(id-root ?id who)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaka ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jo_koI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  who.clp    who6   "  ?id "  jo_koI )" crlf))
)
