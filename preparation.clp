;---------------------------------DEFAULT RULE------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (22-11-2014)
;Careful preparation for the exam is essential. [OALD]
;परीक्षा के लिए सावधानी से तैयारी आवश्यक हैं. [Self]

(defrule preparation0
(id-root ?id preparation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEyArI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  preparation.clp 	preparation0   "  ?id "  wEyArI )" crlf))
)

;--------------------------------------------------------------------------------------------------------------------------


;@@@ Added by 14anu-ban-09 on (22-11-2014)
;This has been achieved by purification and special preparation of materials such as quartz. [NCERT CORPUS]
;ise kvAtrja jEse paxArWoM ke SoXana waWA viSiRta viracana xvArA banAyA jAwA hE. [NCERT CORPUS]

(defrule preparation1
(declare (salience 1000))
(id-root ?id preparation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 material)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viracana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  preparation.clp 	preparation1   "  ?id "  viracana )" crlf))
)
