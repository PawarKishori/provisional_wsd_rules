;##############################################################################
;#  Copyright (C) 2014-2015 Gurleen Bhakna (gurleensingh@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;$$$ Modified by 14anu-ban-03 (12-12-2014)
;NOTE: Parser is taking as noun and it is adjective. Rule is ok and running on parser no.-3
;@@@ Added by 14anu05 GURLEEN BHAKNA on 12th June, 2014.
;The government denied that there had been any collateral damage during the bombing raid.
;सरकार ने इनकार किया कि वहाँ पर बमबारी धावे के दौरान कुछ सम्पार्श्विक क्षति हानि हुई थी.
;सरकार ने इनकार किया कि वहाँ पर बमबारी धावे के दौरान कुछ सम्पार्श्विक हानि हुई थी. [translation given by 14anu-ban-03 (12-12-2014)]
(defrule collateral1
(declare (salience 2600))
(id-root ?id collateral)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-root ?id1 damage)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMpArSvika)) ;meaning changed from 'saMpArSvika_kRawi' to 'saMpArSvika'by 14anu-ban-03 (12-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  collateral.clp    collateral1   "  ?id " saMpArSvika )" crlf))
)


;--------DEFAULT RULE-------
;@@@ Added by 14anu05 GURLEEN BHAKNA on 12th June, 2014.
; We had put our house up as collateral for our bank loan. 
; हमने हमारा घर बैंक ऋण के लिए समपार्श्व रखा था.
(defrule collateral0
(declare (salience 1300))
(id-root ?id collateral)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(viSeRya-for_saMbanXI ?id ?id1)
;(id1-word ?id1 finance|money|loan|repayment|loans)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samapArSva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  collateral.clp    collateral0   "  ?id " samapArSva   )" crlf))
)
