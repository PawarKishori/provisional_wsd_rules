;@@@ Added by 14anu-ban-03 (05-03-2015)
;The clutch of the scooter has become loose. [same clp]
;स्कूटर का क्लच ढीला हो गया है . [manual]
(defrule clutch2
(declare (salience 5000))
(id-root ?id clutch)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 scooter|bike|bus|car|van|bicycle|vehicle) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id klaca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clutch.clp 	clutch2   "  ?id "  klaca )" crlf))
)

;@@@ Added by 14anu-ban-03 (05-03-2015)
;He managed to escape from their clutches. [oald]
;वह उनके चङ्गुल से बच निकलने मे सफल हुआ . [manual]
(defrule clutch3
(declare (salience 5000))   
(id-root ?id clutch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI ?id1 ?id)
(id-root ?id1 escape)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caMgula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clutch.clp 	clutch3   "  ?id "  caMgula )" crlf))
)

;@@@ Added by 14anu-ban-03 (18-04-2015)
;Put the car in gear, and then slowly disengage the clutch while pressing on the gas pedal.    [merriam-webster]
;गाड़ी  गियर में रखिए, और बाद में  क्लच को धीरे से  छोड़िए जब कि गैस पैडल को दबाइए  .   [self]
(defrule clutch4
(declare (salience 5000))
(id-root ?id clutch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 disengage) 
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id klaca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clutch.clp 	clutch4  "  ?id "  klaca )" crlf))
)


;--------------------------Default Rules---------------------

;"clutch","N","1.paMjA_[yAMwrika]"
;The clutch of the scooter has become loose.
(defrule clutch0
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (05-03-2015)
(id-root ?id clutch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paMjA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clutch.clp 	clutch0   "  ?id "  paMjA )" crlf))
)

;"clutch","V","1.pakadZanA"
;She clutched the baby in her arms.
(defrule clutch1
(declare (salience 4900))
(id-root ?id clutch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clutch.clp 	clutch1   "  ?id "  pakadZa )" crlf))
)

