;########################################################################
;#  Copyright (C) 2014-2015 14anu26 (noopur.nigam92@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################
;@@@ Addded by 14anu26  [27-06-14]
;America exports grain.
;अमरीका अनाज का निर्यात करता है .
(defrule grain0
(declare (salience 0))
(id-root ?id grain)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anAja ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grain.clp 	grain0   "  ?id "  anAja  )" crlf))
) 

;@@@ Added by 14anu05 GURLEEN BHAKNA on 17.06.14
;He always eats the grain I leave for him.
;वह हमेशा वह अनाज खाता है जो मैं उसके लिए रखता हूँ .
(defrule grain00
(declare (salience 5000))
(id-root ?id grain)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 eat|cook|have|taste)
;(samAsa_viSeRya-samAsa_viSeRaNa ?id2 ?id)
;(id-word ?id2 disease)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grain.clp      grain00    "  ?id "  anAja )" crlf)
)
)

;$$$ Modified by 14anu-ban-05 on (09-01-2015)
;We have only 20 acres, which is like a grain of sand on the face of the earth.[COCA]
;हमारे पास केवल २० एकड़ जमीन है, जो कि पृथ्वी के पृष्ठ पर रेत के एक कण की तरह है. [MANUAL]
;@@@ Added by 14anu05 GURLEEN BHAKNA on 17.06.14
;There isn't a grain of truth in those rumours. ;wrong example sentence
;उन अफवाहों में सच का एक कण नहीं है .
(defrule grain01
(declare (salience 5000)) 
(id-root ?id grain)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
;(id-root ?id1 truth|sensitive|true)	;commented by 14anu-ban-05 on (09-01-2015)
(id-root ?id1 sand|salt|dust)		;added by 14anu-ban-05 on (09-01-2015)
;(samAsa_viSeRya-samAsa_viSeRaNa ?id2 ?id)
;(id-word ?id2 disease)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grain.clp      grain01    "  ?id "  kaNa )" crlf)
)
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 18.06.14
;He always cuts the wood along the grains.
;वह हमेशा रेशों के साथ लकडी़ काटता है .
(defrule grain02
(declare (salience 5000))
(id-root ?id grain)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-along_saMbanXI  ?id1 ?id)
(id-root ?id1 wood|log)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id reSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grain.clp      grain02    "  ?id "  reSA )" crlf)
)
)


;$$$ Modified by 14anu-ban-05 on (09-01-2015)
;@@@ Addded by 14anu26  [27-06-14]
;A few grains of rice. 
;चावल के कुछ दाने .
(defrule grain1
(declare (salience 400))
(id-root ?id grain)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 rice|corn|salt|sugar)	;removed 'sand' by 14anu-ban-05 on (09-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAnA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grain.clp 	grain1   "  ?id "  xAnA  )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (09-01-2015)
;modified translation of sentence and meaning from 'rawwI' to 'rawwI_Bara_BI'
;@@@ Addded by 14anu26  [27-06-14]
;There isn't a grain of truth in those rumours.
;उन अफवाहों में सच की एक रत्ती नहीं है .
;उन अफवाहों में रत्ती भर भी सच्चाई नहीं है 	;modified by 14anu-ban-05 on (09-01-2015)
(defrule grain2
(declare (salience 400))
(id-root ?id grain)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 truth|sensitivity)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rawwI_Bara_BI ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grain.clp 	grain2   "  ?id "  rawwI_Bara_BI  )" crlf))
)

;@@@ Addded by 14anu26  [27-06-14] 
;A given pollen grain blowing in the wind is thus unlikely to land on a receptive stigma. 
;इस प्रकार एक दिया हुआ पराग कण हवा में उड़ते हुए एक ग्रहणशील वर्तिकाग्र पर उतरना असम्भव है .
(defrule grain3
(declare (salience 500))
(id-root ?id grain)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) pollen)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaNa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  grain.clp 	grain3   "  ?id "  kaNa  )" crlf))
)
