;@@@ Added by  14anu-ban-04 (06-02-2015)
;My job doesn't really allow me fully to deploy my talents.                  [cald]
;मेरा  काम वास्तव में  मुझे पूर्ण रूप से मेरी प्रवीणता  उपयोग करने की  अनुमति नहीं देता है .                [self]
(defrule deploy1
(declare (salience 20))
(id-root ?id  deploy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayoga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deploy.clp 	 deploy1   "  ?id "   upayoga_kara )" crlf))
)

;@@@ Added by  14anu-ban-04 (06-02-2015)
;The company is reconsidering the way in which it deploys its staff.               [cald]
;कंपनी वह तरीका फिर से  सोच रही है जिसमें वह अपने  कर्मचारी काम में लगाती है .                             [self]  
(defrule deploy2
(declare (salience 30))
(id-root ?id  deploy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAma_meM_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deploy.clp 	 deploy2   "  ?id "   kAma_meM_lagA )" crlf))
)

;-------------------- Default Rules ----------------

;@@@ Added by  14anu-ban-04 (06-02-2015)
;2000 troops were deployed in the area.                     [oald]
;क्षेत्र में 2000 दल  तैनात किए गये थे .                                  [self]
(defrule deploy0
(declare (salience 10))
(id-root ?id  deploy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wEnAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deploy.clp    deploy0   "  ?id "   wEnAwa_kara )" crlf))
)

