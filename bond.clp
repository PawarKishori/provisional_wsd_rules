;@@@ Added by 14anu-ban-02 (10-10-2014)
;Sentence: Quran is the rope of Allah which means it bonds all the Muslims.[karan singla]
;Translation: कुरान अल्लाह की रस्सी इस अर्थ में भी है कि यह मुसलमानों को आपस में जोड़ कर रखता है ।[karan singla]
(defrule bond0 
(declare (salience -1)) 
(id-root ?id bond) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) ;rule is correct but parser is treating it as noun.
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id jodZa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bond.clp  bond0  "  ?id "  jodZa_kara_raKa )" crlf)) 
) 

;@@@ Added by 14anu-ban-02 (10-10-2014)
;Sentence:These are bonded together by interatomic or intermolecular forces and stay in a stable equilibrium position.[ncert]
;Translation: यह अन्तरा-परमाणविक या अन्तरा-आणविक बलों द्वारा आपस में जुड़े होते हैं और एक स्थिर साम्य अवस्था में रहते हैं.[ncert]
(defrule bond1 
(declare (salience 100)) 
(id-root ?id bond) 
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ?id ?) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id judZA_ho)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bond.clp  bond1  "  ?id "  judZA_ho )" crlf)) 
)

;$$$Modified by 14anu-ban-02(12-01-2015)
;@@@ Added by 14anu06 on 23/6/2014******
;The familial bond.
;पारिवारिक रिश्ता.                      
(defrule bond2
(declare (salience 5000))
(id-root ?id bond);changed root from 'spin' to 'bond' by 14anu-ban-02(12-01-2015)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 family|maternal|familial)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 riSawA));commented by 14anu-ban-02(12-01-2015)
(assert (id-wsd_root_mng ?id riSwA)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bond.clp  bond2  "  ?id "  riSwA )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bond.clp 	bond2   "  ?id "  riSawA )" crlf))
)

;$$$Modified by 14anu-ban-02(12-01-2015)
;The government bond.
;सरकारी बांड.
;@@@ Added by 14anu06(Vivek Agarwal) on 23/6/2014******
;The government bond.
;सरकारी बांड.
(defrule bond3
(declare (salience 4900))
(id-root ?id bond)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
;(test(!= ?id1 0))
;(id-root ?id1 government|comapny|industry|sector);commented by 14anu-ban-02(12-01-2015)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id);added by 14anu-ban-02(12-01-2015),condition is correct but there is a parser problem.
(id-root ?id1 government)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAMda));commented by 14anu-ban-02(12-01-2015)
(assert (id-wsd_root_mng ?id bAMda));added by 14anu-ban-02(12-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bond.clp  bond3  "  ?id "  bAMda )" crlf));added by 14anu-ban-02(12-01-2015)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  bond.clp 	bond3   "  ?id "  bAMda )" crlf));commented by 14anu-ban-02(12-01-2015)
)
