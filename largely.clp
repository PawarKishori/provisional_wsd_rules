;@@@ Added by 14anu-ban-08 (01-12-2014)
;Thus, electric force is largely zero and gravitational force dominates terrestrial phenomena.     [NCERT]
;इस प्रकार वैद्युत बल अधिकांश रूप में शून्य होता है तथा पार्थिव परिघटनाओं में गुरुत्वाकर्षण बल का प्रभुत्व रहता है.     [NCERT]
(defrule largely0
(declare (salience 0))
(id-root ?id largely)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXikAMSa_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  largely.clp 	largely0   "  ?id "  aXikAMSa_rUpa_se )" crlf))
)


;@@@ Added by 14anu-ban-08 (01-12-2014)
;After eleven years , the finding of the age of a camel becomes largely a matter of guess - work .    [Karan singla]
;11 वर्ष की उम्र के बाद तो ऊंट की उम्र का पता लगाना मुख़्य रूप से अनुमान की बात ही रह जाती है .    [Karan singla]
(defrule largely1
(declare (salience 100))
(id-root ?id largely)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 become)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id muKZya_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  largely.clp 	largely1   "  ?id "  muKZya_rUpa_se )" crlf))
)
