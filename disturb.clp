;$$$ Modified by 14anu-ban-04 (11-03-2015)   ------------changed meaning from 'pareSAna_kara' to  'pareSAna_ho'
;Rule disturb0 added by meena to incoporate the meaning "pareSAna_kara"(3-08-09)
;I was disturbed by the news of her death.                 
;उसके मृत्यु  के  समाचार से मैं परेशान हो गया था .                           ;translation given by 14anu-ban-04 on (11-03-2015)
;She was disturbed about the violence breaking out in the old city.
;पुराने शहर में यकायक होते हुए हिंसा से वह परेशान हो  गयी थी .                  ;translation given by 14anu-ban-04 on (11-03-2015)
;The letter disturbed me . 
(defrule disturb0
(declare (salience 6000))
(id-root ?id disturb)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)                           ;added by Meena
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))       ;added by 14anu-ban-04 on (11-03-2015)
;(or(id-cat_coarse ?id2 pronoun)(kriyA-about_saMbanXI ?id ?id2)(kriyA-by_saMbanXI ?id ?id2)) ;added by Meena    ;commented by 14anu-ban-04 on (11-03-2015)
(id-cat_coarse ?id verb)        ;commented by Meena       ;uncommented by 14anu-ban-04 on (11-03-2015)
(id-tam_type ?id passive)        ;added by 14anu-ban-04 on (11-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pareSAna_ho ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disturb.clp 	disturb0   "  ?id "  pareSAna_ho )" crlf))
)

;@@@ Added by 14anu-ban-04 (11-03-2015)
;Several fans were arrested and charged with disturbing the peace after the game.            [cald]
;कई प्रशंसकों को   पकड़ा गया था  और खेल के बाद शान्ति भंग करने का आरोप लगाया गया था .                              [manual]
(defrule disturb3
(declare (salience 4810))
(id-root ?id disturb)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 peace|silence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BaMga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disturb.clp 	disturb3   "  ?id "  BaMga_kara )" crlf)
)
)

;@@@ Added by 14anu-ban-04 (11-03-2015)
;Some scenes are violent and may disturb younger viewers.               [cald]
;कुछ दृश्य हिंसात्मक हैं और युवा दर्शकों को चिंतित कर सकते है .                              [manual]
(defrule disturb4
(declare (salience 4810))
(id-root ?id disturb)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cinwiwa_kara))  
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disturb.clp     disturb4   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disturb.clp 	disturb4   "  ?id "  cinwiwa_kara )" crlf)
)
)


;Rule disturb1 modified=>  added the relation kriyA-object instead of id-cat verb(Meena 03-08-09) to incoporate the meaning "CedZa"(3-08-09)
;Do not disturb the sleeping kids.
;Don't disturb the patient's wounds by moving him too rapidly!
;Don't disturb the clothes in my closet.
(defrule disturb1
(declare (salience 4800))
(id-root ?id disturb)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CedZa))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disturb.clp 	disturb1   "  ?id "  CedZa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disturb.clp   disturb1   "  ?id " ko )" crlf)
)
)

;"disturb","V","1.viGna_padZa[dAla]"

;$$$ Modified by 14anu-ban-04 (11-03-2015) 
;###[COUNTER EXAMPLE]### The letter disturbed me .                   [same clp file]
;###[COUNTER EXAMPLE]### पत्र ने मुझे  चिंतित किया .                            [manual]
;Rule disturb2 changed=> added the relation kriyA-subject instead of cat(Meena 03-08-09) 
;Some bad elements in the crowd disturbed the speech.
;भीड़ में  कुछ  बुरे लोगों ने भाषण में  विघ्न डाला.
(defrule disturb2
(declare (salience 5000))
(id-root ?id disturb)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)                     ;uncommented by 14anu-ban-04 on (11-03-2015)
(id-root ?id1 speech)                        ;uncommented and added 'speech' by 14anu-ban-04 on (11-03-2015)
(kriyA-object ?id ?id1)                     ;relation changed from 'kriyA-subject' to 'kriyA-object' by 14anu-ban-04 on (11-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  viGna_dAla))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disturb.clp 	disturb2   "  ?id "   viGna_dAla )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disturb.clp   disturb2   "  ?id " meM )" crlf)
)
)

;"disturb","VT","1.viGna_dAlanA/SAMwi_BaMga_karanA"
;Some bad elements in the crowd disturbed the speech.
;--"2.GabadZAnA"
;She was disturbed about the violence breaking out in the old city.
;--"3.CedZanA/sawAnA"
;Don't disturb the patient's wounds by moving him too rapidly!
;
;LEVEL 
;Headword : disturb
;
;Examples --
;
;"disturb","VT","1.viGna_dAlanA"
;Some bad elements in the crowd disturbed the speech.
;BIdZa meM mOjUxa kuCa bure wawwvoM ne BARaNa meM viGna dAlA.<---BARaNa kI vyavasWA ko CedZanA
;--"2.ciMwiwa_honA"
;She was disturbed about the violence breaking out in the old city.
;purAne Sahara meM hiMsAwmaka GatanAoM ke kAraNa vaha cinwiwa WI.<--mana vyAkula honA<--mana kI SAnwi BaMga honA<--cinwAoM xvArA mana kI SAnwi meM viGna dAlA jAnA<--mana kI SAMwi ko CedZanA
;--"3.aswavyaswa_karanA"
;Don't disturb the clothes in my closet.
;merI almArI meM kapadZoM ko aswavyaswa nahIM karo.<--kapadZoM kI vyavasWA ko CedZanA
;--"4.CedZanA"
;Don't disturb the patient's wounds by moving him too rapidly!
;marIjZa wejZI se hilA kara usakI cotoM ko mawa CedZo.
;
;Upara xiye saBI uxAharaNoM se 'disturb' Sabxa meM 'kisI vyavasWA ko aswavyaswa karane' kA BAva lagawA hE. awaH isakA sUwra hogA ; 
;
;sUwra : viGna_dAlanA[<aswavyaswa_karanA]
;
