;$$$Modified by 14anu-ban-08 (16-02-2015)  ;added constraint, modify 'id-word' to 'id-root'
;Student numbers are expanding rapidly.  [oald]
;विद्यार्थीयों की संख्या शीघ्रता से बढ़ रही हैं .   [self]
;@@@ Added by 14anu03 on 16-june-14
;The number of people in the lift must not exceed 10.
;सङ्ख्या लिफ्ट लोग 10 से अधिक नहीं होनी चाहिए .
(defrule number100
(declare (salience 5500))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)
(id-root ?id1 exceed|expand)   ;modify 'id-word' to 'id-root', added 'expand' by 14anu-ban-08 (16-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  number.clp 	number100   "  ?id "  saMKyA )" crlf))
)

;$$$ Modified by 14anu-ban-08 (11-12-2014)     
;@@@ Added by 14anu24
;You'd be self employed,childminding in your own home,so the hours you'd work and the number and ages of the children you'd care for would,to an extent, be up to you.
;आप अगर चाइल्डमाइंडिंग करेंगे , तो आप स्वत : रोजगार होंगे और आप अपने घर से काम करेंगे ,इसलिए आप जितने घंटे काम करेंगे , कितने बच्चों की देखभाल करेंगे और उनकी उम्र क्या होगी , ये सब आप तय करेंगे .
(defrule number_tmp
(declare (salience 5001))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)            ;added 'id1' by 14anu-ban-08 (11-12-2014)
(id-root ?id1 child)                       ;added by 14anu-ban-08 (11-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiwanA))         ;Meaning changed from 'kiwane' to 'kiwanA' by 14anu-ban-08 (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  number.clp   number_tmp   "  ?id "  kiwanA )" crlf))                ;Meaning changes from 'kiwane' to 'kiwanA' by 14anu-ban-08 (11-12-2014)
)

;$$$Modified by 14anu-ban-08 (11-12-2014)       ;constraint is added
;@@@ Added by 14anu02 on 23.06.14
;As the number of cars on the road increases, pollution increases.
;जैसे जैसे सड़क पर गाडियो की संख्या मे वृद्धि होती है प्रदूषड मे वृद्धि होती है.
(defrule number4
(declare (salience 5001))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)                ;added 'id1' by 14anu-ban-08 (11-12-2014)
(id-root ?id1 car)                            ;added by 14anu-ban-08 (11-12-2014)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " number.clp   number4   "   ?id " saMKyA )" crlf))
)

;$$$ Modified by 14anu-ban-08 on (01-08-2014)
;The result of a measurement of a physical quantity is expressed by a number accompained by a unit.
;kisI BI BOwika rAsi kI mApa ko mAwrka ke Age eka saMKyA liKakara vyakwa kiyA jAwA hE.
;$$$ Modified by Manasa-Gurukul Arsh sodh sansthan 12-07-14
;It also fulfills the important role of providing employment to people in big number.
;yaha kAPI saMKyA mE logoM rojagAra muhEyA karane meM BI ahama BUmikA kA nirvAha kara rahA hEm. 
(defrule number0
(declare (salience 5000))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-viSeRaNa ?id ?id1)(viSeRya-det_viSeRaNa ?id ?id2)) ; added 'viSeRya-det_viSeRaNa' relation by 14anu-ban-08 01-08-14
;(id-word ?id1 large|big) ; commented by 14anu-ban-08 01-08-14
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  number.clp 	number0   "  ?id "  saMKyA )" crlf))
)

(defrule number1
(declare (salience 4900))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  number.clp 	number1   "  ?id "  aMka )" crlf))
)

;"number","N","1.aMka/saMKyA"
;Every number has a unique position in the sequence.
;--"2.bahuwa_sAre"
;He had a number of chores to do.
;--"1.gAnA/kaviwA"
;She sang a beautiful number.
;
(defrule number2
(declare (salience 4800))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gina))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  number.clp 	number2   "  ?id "  gina )" crlf))
)

;"number","VT","1.ginanA"
;You should number the pages of the thesis.
;
;@@@ Added by Nandini(9-1-14)
;He put his phone number on a napkin. [via mail]
;usane rumAla para usakA telIPona nambara liKA.
(defrule number3
(declare (salience 4950))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 phone)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nambara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  number.clp 	number3   "  ?id "  nambara )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_number0
(declare (salience 5000))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-word ?id1 large)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " number.clp   sub_samA_number0   "   ?id " saMKyA )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_number0
(declare (salience 5000))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(object-object_samAnAXikaraNa ?id ?id1)
(id-word ?id1 large)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " number.clp   obj_samA_number0   "   ?id " saMKyA )" crlf))
)

;@@@ Added by 14anu04 on 24-June-2014
;They were out numbered by their enemies in the war. 
;वे युद्ध में उनके शत्रुओं से सँख्या में पिछड गये थे. 
(defrule number_tmp1
(declare (salience 4900))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-kriyA_viSeRaNa  ?id ?id1)
(test (=(- ?id 1) ?id1))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  sazKyA_meM_piCada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " number.clp	number_tmp1  "  ?id "  " ?id1 "  sazKyA_meM_piCada  )" crlf))
)

;$$$Modified by 14anu-ban-08 (16-02-2015)  ;relation added, Runs on parser 2
;@@@ Added by 14anu01
;Give me your mobile number.
;मुझे आपका मोबाइल नम्बर दीजिए . 
(defrule number03
(declare (salience 5500))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ?id ?id1)     ;added by 14anu-ban-08 (16-02-2015)
(id-root ?id1 phone|telephone|mobile|account|house|colony|room|hotel|appartment|roll);added words like telephone,mobile,house
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nambara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  number.clp   number03  "  ?id "  nambara )" crlf))
)

;@@@Added by 14anu-ban-08 (18-03-2015)
;The government troops were superior in numbers.[cald]
;सरकारी दल संख्या में अत्यधिक थे .  [self]
(defrule number5
(declare (salience 5500))
(id-root ?id number)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id1 ?id)
(id-root ?id1 superior)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMKyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  number.clp   number5  "  ?id "  saMKyA )" crlf))
)
