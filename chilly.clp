;$$$  Modified by Preeti(13-5-14)
;They gave him a chilly reception. [merriam-webster.com]
;unhoMne usako eka rUKA svAgawa xiyA.[AO]
;unhoMne usakA rUKA svAgawa kiyA.
(defrule chilly1
(declare (salience 1000));salience changed
(id-root ?id chilly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 look|reception)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUKA));meaning changed
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chilly.clp 	chilly1   "  ?id "  rUKA )" crlf))
)

;@@@ Added by 14anu-ban-03 (06-02-2015)
;She is rather chilly to her guests. [same clp file]
;वह अपने अतिथियों के प्रति काफी उत्साह हीन है . [Manual]
(defrule chilly2
(declare (salience 5000))
(id-root ?id chilly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 guest)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwsAha_hIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chilly.clp 	chilly2   "  ?id "  uwsAha_hIna )" crlf))
)

;----------------- Default Rules -----------------

;$$$  Modified by Preeti(13-5-14)
;It is a little chilly outside. [merriam-webster.com]
;bAhara WodZA TaMdA  hE.
(defrule chilly0
;(declare (salience 5000));salience changed
(id-root ?id chilly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TaMdA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  chilly.clp 	chilly0   "  ?id "  TaMdA )" crlf))
)

;"chilly","Adj","1.TaNdA"
;I'm very lazy in this chilly weather. 
;--"2.uwsAha_hIna"
;She is rather chilly to her guests.
;
;
