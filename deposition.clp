;@@@ Added by 14anu-ban-04 (10-01-2015)
;Deposition of government is not in the interest of the country.            [hindikhoj dic]
;सरकार की पदच्युति देश के हित में नहीं है.                                                [self]
(defrule deposition0
(declare (salience 100))
(id-root ?id deposition)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun) 
(viSeRya-of_saMbanXI  ?id ?id1)                      ;added by 14anu-ban-04 (23-04-2015)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))         ;added by 14anu-ban-04 (23-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paxacyuwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposition.clp 	deposition0  "  ?id "  paxacyuwi )" crlf))
)

;@@@ Added by 14anu-ban-04 (23-04-2015)
;His attorneys took depositions from the witnesses.               [merriam-webster]
;उसके मुख्तार ने साक्षियों से बयान लिए .                                          [self]
(defrule deposition2
(declare (salience 40))
(id-root ?id deposition)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)    
(kriyA-object ?kri ?id)
(kriyA-from_saMbanXI ?kri ?id1)     
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bayAna))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposition.clp 	deposition2  "  ?id "  bayAna )" crlf))
)

;@@@ Added by 14anu-ban-04 (23-04-2015)
;She gave a videotaped deposition about what she saw that night.   [merriam-webster]
;उसने जो  उस रात देखा उस के बारे में उसने  वीडियोटेप  साक्ष्य  दिया.                      [self]
(defrule deposition3
(declare (salience 30))
(id-root ?id deposition)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)    
(kriyA-object ?kri ?id)
(kriyA-about_saMbanXI ?kri ?id1)   
(id-root ?kri give)  
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAkRya/sabUwa))     
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposition.clp 	deposition3  "  ?id "  sAkRya/sabUwa )" crlf))
)

;-----------------------------------------------------------DEFAULT RULE ------------------------------------------------------------------

;[NOTE: EARLIER THIS RULE IS IN 'depose.clp' but its root is 'deposition' so this rule is here.]
;$$$ Modified by 14anu-ban-04 (10-01-2015)    [root is changed from 'depose' to 'deposition']
;@@@ Added by 14anu13 on 17-6-14
;It is now believed that this is not necessarily so but is due to deposition of fatty matter and fibrous substances in the arteries as per one's lifestyle .
;अब समझा जाता है कि ऐसा नहीं है , बल्कि धमनियों में वसीय एवं रेशेदार पदार्थों के निक्षेपण के कारण ऐसा हो सकता है .
(defrule deposition1
(declare (salience 10))                         ;salience reduced from '200' to '10' by 14anu-ban-04 (23-04-2015)
(id-root ?id deposition)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)          ;category is changed from 'verb' to 'noun' by 14anu-ban-04 (10-01-2015)
;(viSeRya-of_saMbanXI  ?id ?id1)              ;commented by 14anu-ban-04 (23-04-2015)
;(id-root ?id1 substance|matter)          ;added by 14anu-ban-04 (10-01-2015)  ;commented by 14anu-ban-04 (23-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikRepaNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deposition.clp 	deposition1  "  ?id "  nikRepaNa )" crlf))
)
