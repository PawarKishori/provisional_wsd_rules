;@@@ Added by Nandini(30-11-13)
;"limit","V","1.sImiwa_karanA"
;Limit your speech to five minutes.
;pAzca minatoM meM apanA BASaNa sImiwa kIjie.
;$$$ Modified by 14anu18 (14-06-14)
;commented ;(id-word ?id1 speech) 
;He limited his expences.
;उसने उसके खर्चे सीमित किये. 
(defrule limit2
(declare (salience 5500))
(id-root ?id limit)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 speech)   
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  limit.clp 	limit2   "  ?id "  sImiwa_kara )" crlf))
)

;Remove this rule because 'limited' root is 'limited' but they make in 'limit' file and correct meaning is come from 'dictionary' by 14anu- ban-08 (13-01-2015)
;@@@ Added by 14anu26   [28-06-14]
;Limited strategic understanding.
;सीमित युक्ति विषयक समझ. 
;(defrule limit3
;(declare (salience 4900))
;(id-root ?id limit)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object 	?id ?id1)
;(id-word ?id1 understanding)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id sImiwa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  limit.clp 	limit3   "  ?id "  sImiwa )" crlf))
;)

;@@@ Added By 14anu-ban-08 (15-10-2014)
;In the limiting case, when the slope of the second plane is zero (i.e. is a horizontal) the ball travels an infinite distance.   [NCERT]
;सीमान्त स्थिति में, जब दूसरे समतल का ढाल शून्य है (अर्थात् वह क्षैतिज समतल है) तब गेंद अनन्त दूरी तक चलती है.      [NCERT]
(defrule limit4
(declare (salience 4901))
(id-root ?id limit)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word ?id1 case)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  limit.clp 	limit4   "  ?id "  sImAnwa )" crlf))
)

;===========Default-rules ==============
;Hindi traslation added by Nandini(30-11-13)
;"limit","N","1.sImA"
;Limits of metropolitan towns are expanding day-by-day.
;mahAnagarIya nagaroM ke sImA day-by-day PEla rahe hEM.
(defrule limit0
(declare (salience 5000))
(id-root ?id limit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sImA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  limit.clp 	limit0   "  ?id "  sImA )" crlf))
)

;Commented by 14anu18
;(defrule limit1
;(declare (salience 4900))
;(id-root ?id limit)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id sImiwa_raha))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  limit.clp 	limit1   "  ?id "  sImiwa_raha )" crlf))
;)

;==========Additional-examples==========
;There is a limit to the amount of pain we can bear.
;The team performed to the limit of its capabilities.
;She knew the limits of her power.
;His arrogance knew no limits.
;The EU has set strict limits on levels of pollution.
;They were travelling at a speed that was double the legal limit.
;You can't drive you're over the limit
;We were reaching the limits of civilization.
;We were reaching the limits of civilization.
