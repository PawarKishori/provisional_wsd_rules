;@@@ Added by 14anu-ban-06  (15-10-2014)
;This shape reduces the weight of the beam without sacrificing the strength and hence reduces the cost.(NCERT)
;इस प्रकार की आकृति से प्रबलता को न्योछावर किये बिना ही दण्ड के भार को कम किया जा सकता है, अतः लागत भी कम हो जाती है.(NCERT)
;Hence the rule: When two quantities are added or subtracted, the absolute error in the final result is the sum of the absolute errors in the individual quantities.(NCERT)
;अतः, नियम यह है: जब दो राशियों को सङ्कलित या व्यवकलित किया जाता है, तो अन्तिम परिणाम में निरपेक्ष त्रुटि उन राशियों की निरपेक्ष त्रुटियों के योग के बराबर होती है.
;(NCERT)

(defrule hence0
(declare (salience 0))
(id-root ?id hence)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id awaH))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hence.clp 	hence0   "  ?id "  awaH )" crlf))
)

;@@@ Added by 14anu-ban-06 (15-10-2014)
;Hence why Remi is so different from Sam .(parallel corpus)
;इस कारण से रेमी सॉम से इतना अलग है .(parallel corpus)
(defrule hence1
(declare (salience 2000))
(id-root ?id hence)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) why)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1)  isa_kAraNa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hence.clp	 hence1  "  ?id "  " =(+ ?id 1)  " isa_kAraNa_se  )" crlf))
)

