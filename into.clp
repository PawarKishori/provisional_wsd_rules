
(defrule into0
(declare (salience 5000))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) integer|whole number|number )
(id-word =(+ ?id 1) integer|whole number|number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guNA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into0   "  ?id "  guNA )" crlf))
)

(defrule into1
(declare (salience 4900))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) inquiry)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into1   "  ?id "  kI )" crlf))
)

;$$$ Modified by 14anu-ban-06    (12-08-2014) added 'existence' in the word list
;Towers , forts and citadels came into existence for their protection .  (tourism corpus)
;इनकी  रक्षा  के  लिए  गढ़  ,  किले  व  दुर्ग  अस्तित्व  में  आए  ।
(defrule into2
(declare (salience 4800))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) custody|business|human|coins|consideration|existence) ; Added coins in the list by Aditya and Hardik, IIT(BHU)
; $$$ Added consideration in the list by Prachi Rathore[03-12-13]
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into2   "  ?id "  meM )" crlf))
)

;$$$ Modified by 14anu-ban-06 (05-12-2014)
;[### COUNTER EXAMPLE ###]The soybean was introduced into North America in 1765 but for the next 155 years the crop was grown primarily for forage.(agriculture)
;[### COUNTER EXAMPLE ###]सोयाबीन को   उत्तरी अमेरिका  में 1765 में  लाया गया था, लेकिन अगले 155 सालो तक फसल को मुख्यतः चारे के लिए उगाया गया था.(manual)
;$$$ Modified by 14anu02 on 24.06.14
;counter ex-He threw the letter into the fire.
;Come into the house.
;घर के अन्दर आइए . 
(defrule into3
(declare (salience 5000))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(pada_info  (group_head_id ?id1) (preposition ?id))   ;added by 14anu-ban-06 (05-12-2014)
(id-cat_coarse ?id preposition)
(kriyA-into_saMbanXI ?id2 ?id1)	;added by 14anu02         ;added 'id2' by 14anu-ban-06 (05-12-2014)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))	;added by 14anu02
(id-root ?id2 come)           ;added  by 14anu-ban-06 (05-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_aMxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into3   "  ?id "  ke_aMxara )" crlf))
)

;"into","Prep","1.aMxara_ko"
;You come into the house.
;--"2.guNiwa_karane_para"
;5 into 1.is fifty.
;

;@@@ Added by Prachi Rathore[03-12-13]
; If nothing else, the school will turn her into an individual.[03-12-13][cambridge]
;यदि कुछ भी नही अन्य, तो विद्यालय एक विशिष्ट व्यक्ति में उसको बदल देगा . 
;$$$ modified by 14anu19 (17-06-2014)
;Channels flow into the sea.
;नहरें समुद्र में  से बहती हैं . (before modification)
;नहरें समुद्र में बहती हैं . (after modification)
(defrule into4
(declare (salience 5000))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(id-root =(+ ?id 1) an|the|a)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into4   "  ?id "  meM )" crlf))
)

;;@@@   ---Added by Prachi Rathore
; We need to prod him into making a decision. [cambridge]
;हमें निर्णय  करने के लिये उसको उकसाने की जरूरत है . 
(defrule into5
(declare (salience 5000))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-into_saMbanXI  ? ?id2)
(kriyA-object  ?id2 ?id1)
(id-root ?id1 team|decision)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into5   "  ?id "  ke_liye )" crlf))
)


;;@@@   ---Added by Prachi Rathore[12-12-13]
; In the hills, ghee freezes into a solid mass.[gyannidhi]
;पहाडियों में, घी एक घनीभूत परिमाण में जमा देता है . 
(defrule into6
(declare (salience 5000))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-into_saMbanXI  ?id2 ?)
(kriyA-subject  ?id2 ?id1)
(id-root ?id1 ghee)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into6   "  ?id "  meM)" crlf))
)

;$$$ Modified by 14anu13 on 20-06-14    [word added]
; The car crashed into the lamp post.
; कार लैंप पोस्ट से टकराई | 
;@@@ Added by Prachi Rathore 6-1-14
;The robber-chief insisted that he must have bumped into a sharp stone in the dark.[gyan-nidhi]
;सरदार को यही विश्वास था कि अंधेरे में वह किसी नुकीले पत्थर से टकरा गया होगा।
(defrule into7
(declare (salience 5000))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-into_saMbanXI  ?id1 ?id2)
(id-root ?id1 bump|crash) ;added "crash" by 14anu13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into7   "  ?id "  se )" crlf))
)

;$$$ Modified by 14anu-ban-06     (08-08-2014) added 'microphone|depression|tension' to root fact
;Soy grits are similar to soy flour except the soybeans have been toasted and cracked into coarse pieces.
;(agriculture);added by 14anu-ban-06 (13-11-2014)
;सोया जई का आटा सोया आटा के समान हैं सिवाय सोयाबीन  भुने और मोटे टुकड़ों में तोडे जाते है.(manual)
;Speak clearly into the microphone.  (OALD)
;mAika meM spaRta rUpa se bolie.
;She was sliding into depression.   (OALD)
;vaha uxAsI meM pada gaI WI.
;Suddenly, a simple art undertaking to interrupt and transform the everyday, had morphed into a serious conservation project!(Anusaaraka_4th_September)
;अचानक, एक साधारण कला उपक्रम बीच में  और हर रोज़ परिणत करने के लिए, एक गम्भीर संरक्षण परियोजना  में  मॊर्फ्ट था! [manual]
;$$$ Modified by 14anu21 on 27.06.2014 by adding knot to the list (id-root ?id1 job|knot)
;Her hair was twisted into a knot on top of her head.[oxford]
;उसके केश उसके सिर के ऊपर गाँठ पर मोडे गये थे . (Translation before modification)
;उसके केश उसके सिर के ऊपर गाँठ में मोडे गये थे . 
;@@@ Added by Prachi Rathore[3-3-14]
;She's thrown herself into this new job.[cambridge]
; उसने अपने को इस नई नौकरी/काम में पूरी तरह से लिप्त कर लिया है.
;At maximum compression the kinetic energy of the car is converted entirely into the potential energy of the spring. (NCERT)
;kAra kI gawija UrjA aXikawama sampIdana para sampUrNa rUpa se spriMga kI sWiwija UrjA meM parivarwiwa ho jAwI hE.(NCERT)
;These forces bind atoms into molecules, molecules into polymeric chains, etc.(NCERT)
;ye bala paramANuoM ko aNuoM meM Ora aNuoM ko poYlImerika SqfKalA iwyAxi meM bAzXa xewe hEM.(NCERT)
;Matter might change its phase, e.g. glacial ice could melt into a gushing stream, but matter is neither created nor destroyed;(NCERT)
;uxAharaNArWa, himAnI barPa piGalakara eka pravAhI naxI ke rUpa meM baha sakawI hE lekina xravya na wo uwpanna kiyA jA sakawA hE Ora na hI naRta;(NCERT)
;If some of the forces involved are non-conservative, part of the mechanical energy may get transformed into other forms such as heat,light and sound.(NCERT)
;yaxi kAryarawa kuCa bala asaMrakRI hEM wo yAnwrika UrjA kA kuCa aMSa xUsare rUpoM; jEse-URmA, prakASa Ora Xvani UrjAoM meM rUpAnwariwa ho jAwA hE.(NCERT)
;A matchstick ignites into a bright flame when struck against a specially prepared chemical surface.(NCERT)
;mAcisa kI eka wIlI jaba viSeRa rUpa se wEyAra kI gaI rAsAyanika sawaha para ragadI jAwI hE wo eka camakIlI jvAlA ke rUpa meM prajvaliwa howI hE.(NCERT)
;$$$modified by 14anu19(28-05-2014)
;They have entered into a relationship.
;वे सम्बन्ध में प्रवेश कर चुके हैं 
(defrule into8
(declare (salience 5000))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-into_saMbanXI  ?id2 ?id1)
(id-root ?id1 job|microphone|depression|tension|project|energy|flame|molecule|stream|form|account|vector|piece|knot|relationship) ;relationship is added(by 14anu19)
; added 'microphone|depression|tension' by 14anu-ban-06 ;added 'project' by 14anu-ban-06 (27-09-2014)
;added 'energy|flame|molecule|stream|form|account|vector' by 14anu-ban-06 (15-10-2014)
;added 'piece' by 14anu-ban-06 (13-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into8   "  ?id "  meM)" crlf))
)

;$$$ Modified by 14anu-ban-06 (05-12-2014)
;[### COUNTER EXAMPLE ###]The patient had slipped into a coma.(OALD)
;[### COUNTER EXAMPLE ###]मरीज बेहोशी की नींद मे चला गया था . (manual)
;$$$ Modified by 14anu02 on 24.06.14
;@@@ Added by Anita - 22.5.2014
;She rubbed the lotion into her skin.
;उसने अपनी त्वचा पर लोशन लगाया ।
;We rubbed some polish into the surface of the wood. 
;हमने लकड़ी की सतह पर थोड़ी पॉलिश लगाई ।
(defrule into9
(declare (salience 3000))	;salience reduced from 5500 by 14anu02 to avoid clash with 'into3'
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(pada_info  (group_head_id ?sam) (preposition ?id))  ;added by 14anu-ban-06 (05-12-2014)
(kriyA-into_saMbanXI  ?kri ?sam)
(id-root ?kri rub) ;commented by 14anu02      ;uncommented by 14anu-ban-06 (05-12-2014)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " into.clp  into9 "  ?id "   para )" crlf))
)

;@@@ Added by 14anu-ban-06 (13-11-2014)
;The soybean was introduced into North America in 1765 but for the next 155 years the crop was grown primarily for forage.(agriculture)
;सोयाबीन को   उत्तरी अमेरिका  में 1765 में  लाया गया था, लेकिन अगले 155 सालो तक फसल को मुख्यतः चारे के लिए उगाया गया था.(manual)
(defrule into10
(declare (salience 4900))    ;salience reduced from 5500 by 14anu-ban-06 to avoid clash with 'into3' (05-12-2014)
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(pada_info  (group_head_id ?id1) (preposition ?id))  
(kriyA-into_saMbanXI ? ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " into.clp  into10 "  ?id "   meM )" crlf))
)

;$$$ Modified by 14anu-ban-06 (05-12-2014)
;He threw the letter into the fire. (into.clp)
;उसने आग में खत फेंका . (manual)
;The patient had slipped into a coma.(OALD)
;मरीज बेहोशी की नींद मे चला गया था . (manual)
;@@@ Added by 14anu20 on 25/06/2014.
;They drill deeply into the wood.
;वे जङ्गल में दूर तक छेद करते हैं . 
(defrule into11
(declare (salience 5500))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(pada_info  (group_head_id ?sam) (preposition ?id)) ;added by 14anu-ban-06 (05-12-2014)
(kriyA-into_saMbanXI  ?kri ?sam)
(id-cat_coarse ?id preposition)
(id-root ?sam wood|coma|fire)            ;added 'coma|fire' by 14anu-ban-06 (05-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " into.clp  into11 "  ?id "   meM )" crlf))
)

;$$$ Modified by 14anu-ban-06 (05-12-2014)
;Picard slipped the documents into a case.(COCA)
;पिकॉर्ड ने पेटी में दस्तावेज छिपा कर रखे. (manual)
;@@@ Added by 14anu20 on 01.07.2014.
;He slipped the letter into the envelope.
;उसने   एनवलप में पत्र छिपा कर  रखा.
(defrule into12
(declare (salience 5600))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(pada_info  (group_head_id ?sam) (preposition ?id))  ;added by 14anu-ban-06 (05-12-2014)
(kriyA-into_saMbanXI  ?kri ?sam)
(id-cat_coarse ?id preposition)
(kriyA-object  ?kri ?id1)
(id-root ?id1 letter|document|paper|file)   ;modified 'document' from 'documents' by 14anu-ban-06 (05-12-2014)
(id-root ?sam envelope|post_card|case)       ;added 'case' by 14anu-ban-06 (05-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " into.clp  into12 "  ?id "   meM )" crlf))
)

;commented by 14anu-ban-06 (17-12-2014),because example sentence is not given and it is over generalisation
;@@@ Added by 14anu19
;(defrule into13
;(declare (salience 5000))
;(id-root ?id into)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id preposition)
;(kriyA-into_saMbanXI =(- ?id 1) =(+ ?id 1))
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) -))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " into.clp       into13  "  ?id "  " ;=(- ?id 1) "  -  )" crlf))
;)

;@@@ Added by 14anu-ban-06 (13-01-2015)
;He ran square into me.(shabdkosh)
;वह सीधा मेरी तरफ भागा. (manual)
(defrule into14
(declare (salience 4900))    
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(pada_info  (group_head_id ?id1) (preposition ?id))  
(kriyA-into_saMbanXI ?id2 ?id1)
(id-root ?id2 run)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waraPa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " into.clp  into14 "  ?id "   waraPa )" crlf))
)

;@@@ Added by 14anu-ban-06 (03-02-2015)
;All the family felt that Stephen had been hustled into the engagement by Claire.(OALD)
;सभी परिवार ने महसूस किया कि स्टीवन को क्लेर के द्वारा सगाई के लिए धकेला गया था .(manual) 
(defrule into15
(declare (salience 5000))
(id-root ?id into)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-into_saMbanXI  ? ?id1)
(id-root ?id1 engagement|marriage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  into.clp 	into15   "  ?id "  ke_liye )" crlf))
)
