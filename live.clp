
;Rules added by Human being
(defrule live0
(declare (salience 5000))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) cell)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live0   "  ?id "  jIviwa )" crlf))
)

(defrule live1
(declare (salience 4900))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) organism)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live1   "  ?id "  jIviwa )" crlf))
)

(defrule live2
(declare (salience 4800))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) substance)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live2   "  ?id "  jIviwa )" crlf))
)

(defrule live3
(declare (salience 4700))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 substance)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live3   "  ?id "  jIviwa )" crlf))
)

(defrule live4
(declare (salience 4600))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 organism)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live4   "  ?id "  jIviwa )" crlf))
)

;...  a living organisms ...
(defrule live5
(declare (salience 4500))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 organism)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live5   "  ?id "  jIviwa )" crlf))
)

;...  all living organisms ...
(defrule live6
(declare (salience 4400))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id living )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  live.clp  	live6   "  ?id "  jIviwa )" crlf))
)

(defrule live7
(declare (salience 4300))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kalaMka_xUra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " live.clp	live7  "  ?id "  " ?id1 "  kalaMka_xUra_kara  )" crlf))
)

;She has a very bad reputation over here && its hard to live on.
;usakA nAma yahAz bahuwa KZarAba ho cukA hE Ora isa kalaMka ko xUra karanA bahuwa hI kaTina hE
(defrule live8
(declare (salience 4200))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nirBara_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " live.clp	live8  "  ?id "  " ?id1 "  nirBara_raha  )" crlf))
)

;$$$Modified by 14anu-ban-08 (24-03-2015)      ;constraint added
;@@@ Added by Nandini (14-12-13) 
;They were living in the forest.[via mail]
;ve jafgala meM raha rahe We.
(defrule live_forest
(declare (salience 5001))         ;salience increased by 14anu-ban-08 (24-03-2015)
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id living )
(kriyA-in_saMbanXI  ?id ?id1)       ;added 'id1' by 14anu-ban-08 (24-03-2015)
(id-root ?id1 forest)               ;added by 14anu-ban-08 (24-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  live.clp  	live_forest   "  ?id "  raha )" crlf))
)


(defrule live9
(declare (salience 4100))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id living )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  live.clp  	live9   "  ?id "  jIviwa )" crlf))
)

;She && her children live on her brother these days.
;ina xinoM vo Ora usake bacce usake BAI para nirBara hEM
(defrule live10
(declare (salience 4000))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIviwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live10   "  ?id "  jIviwa )" crlf))
)

;particle_in_- && category=verb	raha	14.68737419363
;PP_null_in && category=verb	raha	14.68737419363
(defrule live11
(declare (salience 3900))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live11   "  ?id "  raha )" crlf))
)

;@@@ Added by Nandini (3-12-13)
;The president's speech was broadcast live. 
;aXyakRIya BARaNa kA sIXA prasAraNa prasAriwa kIyA gayA WA.
(defrule live12
(declare (salience 3900))
(id-word ?id live)
?mng <-(meaning_to_be_decided ?id)
;(subject-subject_samAnAXikaraNa  3 4)
(not(id-cat_coarse ?id verb))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sIXA_prasAraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  live.clp 	live12   "  ?id "  sIXA_prasAraNa )" crlf))
)

;@@@ Added by Nandini (5-12-13)
;We like live cricket on the TV.
(defrule live13
(declare (salience 5001))
(id-word ?id live)
?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 ?str)
;(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(id-word ?id1 cricket|show)
(viSeRya-viSeRaNa  ?id1 ?id)
(not(id-cat_coarse ?id verb))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vAswava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  live.clp 	live13   "  ?id "  vAswava )" crlf))
)

;Remove by 14anu-ban-08 (24-03-2015) because same condition is given in live18, not required this rule
;@@@ Added by 14anu21 on 05.07.2014
;He lives a simple life.
;;वह एक सरल जीवन रहता है . [Translation before adding rule simple0]
;वह एक सादा जीवन रहता है . [Translation after adding rule simple0]
;वह एक सादा जीवन जीता है . 
;(defrule live14
;(declare (salience 5001))
;(id-root ?id live)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id verb)
;(kriyA-object  ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id jI))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live14   "  ?id "  jI )" crlf))
;)

;@@@ Added by 14anu11
;the million - strong Indian population in the US proved its undying love for Hindi cinema , lining up at theatres to see the latest 
;offerings from Bollywood and turning out in droves for the live shows of Hindi film stars and musicians 
;सन् - ऊण्श्छ्ष् - 2001 में अमेरिका में रहने वाले 10 लख भारतीयों ने ताजा हिंदी फिल्में और हिंदी फिल्मी सितारों , संगीतकारों के शो देखने के लिए भारी संया में जुटकर हिंदी सिनेमा के प्रति अपना 
;अगाध प्रेम प्रदर्शित किया .
(defrule live15
(declare (salience 5000))
(id-root ?id live)
(id-root =(+ ?id 1) show)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAjA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  live.clp 	live15   "  ?id "  wAjA )" crlf))
)

;$$$ Modified by 14anu-ban-08 (16-01-2015)           ;changed translation, changed meaning 
;@@@Added by 14anu15 Gourav Sahni MNNIT Allahabad on 13/06/2014
;I am living with my family. 
;मैं मेरा परिवार के साथ रह रहे रहा हूँ.     (before translation)
;मै मेरे परिवार के साथ रह रहा हूँ.     (after translation)    ;changed translation by 14anu-ban-08 (16-01-2015)
(defrule live16
(declare (salience 5000))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id living )
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))        ;changed meaning from 'raha_rahe' to 'raha' by 14anu-ban-08 (16-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  live.clp  	live16   "  ?id "  raha )" crlf))                                     ;changed meaning from 'raha_rahe' to 'raha' by 14anu-ban-08 (16-01-2015)
)

;@@@Added by 14anu-ban-08 (16-03-2015)
;They lived in fear of their master.  [oald]
;वे अपने मालिक के डर से जीते थे.  [self]
(defrule live17
(declare (salience 5000))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 fear)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jI))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  live.clp  	live17   "  ?id "  jI )" crlf))                                     
)

;@@@Added by 14anu-ban-08 (18-03-2015)
;He lives a simple life.  [same clp file]
;;वह एक सादा जीवन जीता है .  [same clp file]
;He's lived here all his life.  [oald]
;उसने अपनी पूरी जिंदगी यँहा जी हैं.  [self]
(defrule live18
(declare (salience 5002))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 life)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jI))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  live.clp  	live18   "  ?id "  jI )" crlf))                                     
)

;@@@Added by 14anu-ban-08 (24-03-2015)
;According to folk tales Agastya Forest Area was the living place of Agastya the sage.  [Tourism]
;आदिवासी  जनों  का  विश्वास  है  कि  यहाँ  ब्रह्माचारी  महर्षि  अगस्त्य  निवास  करते  थे  ।  [Tourism]
(defrule live19
(declare (salience 4700))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 place)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id nivAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  live.clp  	live19   "  ?id "  nivAsa )" crlf))
)

;@@@ Added by Bhagyashri Kulkarni (2-11-2016)
;It is necessary to maintain the secrecy of the persons living with H.I.V.. (health)
;एच्.आई.वी. के साथ रहने वाले व्यक्तियों की गुप्तता रखना आवश्यक है . 
(defrule live20
(declare (salience 4500))
(id-root ?id live)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id living )
(id-cat_coarse ?id verb)
(id-cat_coarse =(- ?id 1) noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rahane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  live.clp  	live20   "  ?id "  rahane_vAlA )" crlf))
)


;============need-to-be-handled==============
;Was the concert recorded or it was live?


;default_sense && category=verb	jIviwa raha	0
;"live","V","1.jIviwa rahanA"
;Due to his ailment he will not live for long.
;I wish to live a peaceful life.
;He lived a full life.
;--"2.rahanA"
;Where do you live?
;--"3.yAxa rahanA"
;Pleasant memories live longer.
;
;
