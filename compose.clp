
;$$$Modified by 14anu-ban-03 (26-03-2015)
;Greed and ambition composed his personality. [hinkhoj]
;लालसा  और महत्वाकाङ्क्षा ने उसका व्यक्तित्व बनाया . [manual]
;Added by Meena(20.02.10)
;There must be another sort of rules according to which sentences are composed .
(defrule compose0
(declare (salience 4900))
(id-root ?id compose)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1))
(id-root ?id1 sentence|poem|music|story|personality)  ;added 'personality' by 14anu-ban-03 (26-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compose.clp       compose0   "  ?id "  banA )" crlf))
)

;@@@ Added by 14anu01 on 24-06-2014
;She tried to compose herself. 
;उसने स्वयं केा शान्त करने के लिए प्रयास किया . 
(defrule compose1
(declare (salience 5001)) ;salience increased by 14anu-ban-03 (08-01-2015)
(id-root ?id compose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id  verb)
(kriyA-object ?id ?id1)
(id-cat_coarse =(+ ?id 1) pronoun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compose.clp       compose1   "  ?id "  SAnwa_kara )" crlf))
)

;@@@ Added by 14anu13   on  24-06-2014
;I was so confused that I could hardly compose my thoughts. 
;मैं इतना भ्रमित था कि मैंने मुश्किल से अपने विचार शान्त किए|
(defrule compose01
(declare (salience 5500))
(id-root ?id compose)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-kriyA_viSeRaNa  ?id  ?id1)(kriyA-vAkyakarma ?id2  ?id))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   SAnwa_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compose.clp 	compose01    "  ?id "   SAnwa_karA )" crlf))
)

;$$$Modified by 14anu-ban-03 (08-01-2015)
;@@@ Added by 14anu13   on  24-06-2014
;Emma frowned, making an effort to compose herself. 
;एम्मा ने खुद को   ढाढ़स बंधाते हुए त्योरियाँ चढ़ा ली |
(defrule compose02
(declare (salience 5500))
(id-root ?id compose)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-object  ?id ?id1)  ;commented by 14anu-ban-03 (08-01-2015)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)     ;added by 14anu-ban-03 (08-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id   DADZasa_baMXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compose.clp 	compose02    "  ?id "   DADZasa_baMXA )" crlf))
)


;@@@ Added by 14anu-ban-03 (26-03-2015)
;Water is composed of hydrogen and oxygen. [hinkhoj]
;पानी उदजन और आक्सीजन से बनाया गया है . [manual]
(defrule compose3
(declare (salience 5001)) 
(id-root ?id compose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id  verb)
(kriyA-of_saMbanXI ?id ?id1)
(id-root ?id1 hydrogen|oxygen)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compose.clp       compose3   "  ?id "  banA )" crlf))
)

;-------------------------------------------------defaut rule-------------------------------

;@@@ Added by 14anu01 on 24-06-2014 and 14anu13 
;He has composed his first song.
;वह अपना पहला गीत की रचना की  है.
;She composed a nice song.
;उसने एक बढिया गाने की रचना की |
(defrule compose2
(declare (salience 5000))
(id-root ?id compose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id  verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  compose.clp       compose2   "  ?id "  raca )" crlf))
)


