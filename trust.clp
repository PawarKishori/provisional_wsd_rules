
(defrule trust0
(declare (salience 5000))
(id-root ?id trust)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id trusting )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id viSvAsa_karane_vAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  trust.clp  	trust0   "  ?id "  viSvAsa_karane_vAlA )" crlf))
)

;"trusting","Adj","1.viSvAsa_karane_vAlA"
;You have a trusting nature.
;
(defrule trust1
(declare (salience 4900))
(id-root ?id trust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nyAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trust.clp 	trust1   "  ?id "  nyAsa )" crlf))
)

(defrule trust2
(declare (salience 4800))
(id-root ?id trust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarosA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trust.clp 	trust2   "  ?id "  BarosA_kara )" crlf))
)

;"trust","VTI","1.BarosA_karanA"
;I trust you.
;You have to trust the woman before leaving your daughter.
;--"2.ASA_karanA"
;I trust [that]he is having good health.
;
;


;@@@ Added by Prachi Rathore[29-3-14]
;It is written in Hitler's Autobiography "Mein Kampf" that the size of the lie is the main reason for trust.[news]
;हिटलर ने आत्मकथा 'मीन कॉफ' में लिखा है कि झूठ का आकार भरोसे का मुख्य कारण होता है।
(defrule trust3
(declare (salience 4900))
(id-root ?id trust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarosA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trust.clp 	trust3   "  ?id "  BarosA )" crlf))
)

;commented by 14anu-ban-07 (08-12-2014) cat_coarse of trust in this sentence is verb not noun
;@@@ Added by 14anu17
;Trust your instincts.
;आपके सहज पर विश्वास .
;(defrule trust4
;(declare (salience 4901))
;(id-root ?id trust)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(viSeRya-viSeRaNa  ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id viSvAsa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trust.clp 	trust4   "  ?id "  viSvAsa )" crlf))
;)

;@@@ Added by 14anu-ban-07 (12-02-2015)
;She betrayed his trust over and over again.(oald)
;उसने उसके विश्वास का  बारम्बार  विश्वासघात किया. 
(defrule trust5
(declare (salience 5000))
(id-root ?id trust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 betray)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSvAsa))
(assert (id-wsd_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  trust.clp 	trust5   "  ?id "  viSvAsa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  trust.clp trust5  "  ?id " kA)" crlf))
)


