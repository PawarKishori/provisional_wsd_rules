
;@@@ Added by 14anu-ban-03 (23-04-2015)
;His decision can only be understood in that context. [oald]
;उसका निर्णय केवल उस परिस्थिति में समझा जा सकता है . [manual]
(defrule context1
(declare (salience 10))  
(id-root ?id context)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parisWiwi))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  context.clp 	context1   "  ?id "  parisWiwi )" crlf))
)

;--------------------------------- Default Rules ---------------------------------------------

;@@@ Added by 14anu-ban-03 (23-04-2015)
;You should be able to guess the meaning of the word from the context. [oald]
;आप शब्द के अर्थ का अन्दाज़ा इस सन्दर्भ से लगा सकते हैं.  [manual]
(defrule context0
(declare (salience 00))
(id-root ?id context)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMxarBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  context.clp  context0   "  ?id "  saMxarBa )" crlf))
)


