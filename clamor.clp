;@@@ Added by 14anu-ban-03 (12-02-2015)
;Even then there is no clamor . [tourism]
;फिर भी यहाँ शोर बिल्कुल नहीं होता . [manual]
(defrule clamor0
(declare (salience 00))
(id-root ?id clamor)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clamor.clp 	clamor0   "  ?id "  Sora )" crlf))
)


;@@@ Added by 14anu-ban-03 (12-02-2015)
;Away from clamor and din Auli will sooth and will also be exciting . [tourism]
;कोलाहल और शोरगुल से दूर औली सुकून देगा और रोमांचित भी करेगा . [manual]
(defrule clamor1
(declare (salience 100))
(id-root ?id clamor)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-from_saMbanXI  ?id1 ?id)
(id-root ?id1 away)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kolAhala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  clamor.clp 	clamor1   "  ?id "  kolAhala )" crlf))
)



