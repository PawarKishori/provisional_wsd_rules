;@@@ Added by 14anu-ban-06 (23-02-2015)
;Yet the intricacy of the problems involved is pointed up by the delays and difficulties involved in forging.(COCA)
;फिर भी सम्मिलित समस्याओं की कठिनता  विलम्बों और विकसित करने में सम्मिलित कठिनाईयों  के द्वारा  दर्शायी जाती है . (manual)
(defrule intricacy0
(declare (salience 0))
(id-word ?id intricacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTinawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intricacy.clp 	intricacy0   "  ?id "  kaTinawA )" crlf))
)

;@@@ Added by 14anu-ban-06 (23-02-2015)
;The intricacy of the needlework. (cambridge)
;सूची शिल्प की बारीकी. (manual)
;The intricacy of the design. (OALD)
;डिज़ाइन की बारीकी.(manual)
;The richness of the many stories, the complexity of thought, and the intricacy of design ultimately can not be summarized but must be discovered for themselves.  (COCA)
;बहुत सारी कहानियों की धनाढ्यता, विचारो  की जटिलता, और डिज़ाइन की बारीकी अंततः संक्षिप्त नहीं हो सकती है परन्तु खुद के लिए आविष्कृत करनी होगी . (manual)
(defrule intricacy1
(declare (salience 2000))
(id-word ?id intricacy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 needlework|design)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArIkI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intricacy.clp 	intricacy1   "  ?id "  bArIkI )" crlf))
)

