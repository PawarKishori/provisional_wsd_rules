;$$$ Modified by Shirisha Manju
;added kriyA_id-object_viBakwi fact
;@@@Added by 14anu-ban-02(19-03-2015)
;The police barricaded the crime scene.[mw]
;पुलिस ने अपराध दृश्य रोक लगाई. [self]
;पुलिस ने अपराध दृश्य पर रोक लगाई. 
(defrule barricade2
(declare (salience 100))
(id-root ?id barricade)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 scene)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka_lagA))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  barricade.clp 	barricade2   "  ?id "  roka_lagA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  barricade.clp  barricade2   "  ?id " para )" crlf)
)
)

;@@@Added by 14anu-ban-02(19-03-2015)
;The city barricaded the flooded streets.[mw]
;शहर ने जलप्लावित सडकें बन्द कर दीं . [self]
(defrule barricade3
(declare (salience 100))
(id-root ?id barricade)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 street)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banxa_kara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  barricade.clp 	barricade3   "  ?id "  banxa_kara_xe )" crlf))
)

;@@@Added by 14anu-ban-02(19-03-2015)
;Police erected barricades to keep the crowds from approaching the crime scene.[mw]
;पुलिस ने भीड़ को अपराध दृश्य के निकट आने से रोकने  के लिए अस्थायी दीवारें खड़ी कीं .[self]
(defrule barricade4
(declare (salience 100))
(id-root ?id barricade)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 erect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asWAyI_xIvAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  barricade.clp 	barricade4   "  ?id "  asWAyI_xIvAra )" crlf))
)

;------------------------ Default Rules ----------------------

;"barricade","N","1.moracA"
;The soldiers stormed the barricades erected by the rioters.
(defrule barricade0
(declare (salience 0))	;reduced salience to 0 from 5000 by 14anu-ban-02(19-03-2015)
(id-root ?id barricade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moracA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  barricade.clp 	barricade0   "  ?id "  moracA )" crlf))
)

;"barricade","VT","1.moracA_bAzXanA"
;The soldiers barricaded in the valley.
(defrule barricade1
(declare (salience 0))	;reduced salience to 0 from 4900 by 14anu-ban-02(19-03-2015)	
(id-root ?id barricade)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id moracA_bAzXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  barricade.clp 	barricade1   "  ?id "  moracA_bAzXa )" crlf))
)

