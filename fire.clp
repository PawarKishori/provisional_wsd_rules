
;@@@ Added by 14anu04 on 30-June-2014
;She's fired up about her new job. (oxford) 
;वह उसके नये काम के बारे में उत्साहित है . 
(defrule fire_1
(declare (salience 5000))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwsAhiwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fire.clp  fire_1  "  ?id "  " ?id1 "  uwsAhiwa_ho  )" crlf))
)

(defrule fire0
(declare (salience 5000))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id firing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id baMxUka_calAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fire.clp  	fire0   "  ?id "  baMxUka_calAnA )" crlf))
)

;"firing","N","1.baMxUka_calAnA"
;The soldiers were being trained in firing small arms at the firing range.
;
(defrule fire1
(declare (salience 4900))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id fired )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id agni_prajjvalana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fire.clp  	fire1   "  ?id "  agni_prajjvalana )" crlf))
)

;"fired","Adj","1.agni_prajjvalana"
;In India most of the power is produced by coal fired thermal power stations.
;
;

;@@@ Added by Ajay singh [work shop 20.01.2014]
;You are fired. [Cambridge]
;Apa nIkAla xiye gaye. 
;$$$ Modified by 14anu21 on 24.06.2014 by adding relation:"(not(kriyA-object ?id ?idobj))"
;He fired another shot.
;उसने एक और गोली निकाल दी . (Translation before modification)
;उसने एक और गोली दागी . 
(defrule fire4
(declare (salience 4800))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(not(kriyA-object ?id ?obj)) ;intransitive verb
(kriyA-subject  ?id ?sub)
(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fire.clp     fire4   "  ?id "  nikAla_xe )" crlf))
)

;$$$ Modified by Shirisha Manju 07-05-2015 -- interchanged the ids in assert and print statement [?id ?id1 as ?id1 ?id]
;@@@ Added by 14anu05 GURLEEN BHAKNA on 29.05.14
;The police opened fire on the public.
;पुलीस ने जनता पर गोली चलाई.
(defrule fire5
(declare (salience 5600))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 open)
(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id golI_calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  fire.clp     fire5   "  ?id1 "  " ?id "  golI_calA  )" crlf))
)

;@@@ Added by 14anu21 on 24.06.2014
;He fired Mohan from the office.
;उसने दफ्तर से मोहन को निकाल दिया . 
(defrule fire6
(declare (salience 4800))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?sub)
(kriyA-object ?id ?obj);Transitive verb here requires a human object
(id-root ?sub ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-root ?obj ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikAla_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fire.clp     fire6   "  ?id "  nikAla_xe )" crlf))
)

;@@@ Added by 14anu07 0n 01/07/2014
;The impression that Pakistan would have a role to play had been gaining ground through the cease - fire phase .
;संघर्ष विराम के दौरान ही यह भावना बलवती होने लगी थी कि पाकिस्तान की भूमिका जरूरी है .
(defrule fire7
(declare (salience 5000))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) phase)
(id-word =(- ?id 1) cease)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) (+ ?id 1) saMGarRa_virAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fire.clp    fire7  "  ?id "  " (- ?id 1) " " (+ ?id 1) "  saMGarRa_virAma)" crlf))
)

(defrule fire2
(declare (salience 4800))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Aga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fire.clp 	fire2   "  ?id "  Aga )" crlf))
)


(defrule fire3
(declare (salience 4700))
(id-root ?id fire)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fire.clp 	fire3   "  ?id "  xAga )" crlf))
)

;default_sense && category=verb	prahAra_kara{baMxUka_yA_wIra_se}	0
;"fire","V","1.prahAra_karanA{baMxUka_yA_wIra_se}"
;He fired at the robbers.
;--"2.nikAla_xenA"
;He has recently been fired from his job.
;--"3.uwwejiwa_honA"
;The words of the boss fired his imagination for developing a new application for this product.
;--"4.BattI_meM pakAnA"
;Bricks are fired in fire klins to make them stong.
;
;LEVEL 
;"fire"                  "sUwra" (nibanXa)
;----------
;
;"fire","N","1.Aga"
;The fire at the cinema hall resulted in many deaths.
;--"2.golI_calAnA"
;The police opened fire to hold back the violent mob.
;
;"fire","V","1.prahAra_karanA{baMxUka_yA_wIra_se}"
;He fired at the robbers.
;--"2.nikAla_xenA"
;He has recently been fired from his job.
;--"3.uwwejiwa_honA"
;The words of the boss fired his imagination for developing a new application for
; this product.
;--"4.BattI_meM pakAnA"
;Bricks are fired in fire klins to make them stong.
;------------------------------------------------------------
;
;sUwra : Aga`[<PeMkanA]
;
;
;sUwra ko samaJane hewu uparisWiwa vAkyoM ke AXAra para sUwra-saMGataka waWya--
;---------------------------------------------------
; (Aga_lagAnA<wopa_meM_palIwA_lagAnA_<golI_yA_wIra_calAnA<nikAla_PeMkanA)
;
;  saBI arWa `Aga' se Aye hue hEM .
;
;  Aga Sabxa uwwejanA ke arWa meM wo sAXAraNawayA pracaliwa hE . garmI ke kAraNa .
;saBI BARAoM meM Ama prayoga hE . 
;
;  pahale wopa calAne hewu usameM Aga lagAnA padawA WA . jise palIwA lagAnA kahA jAwA hE . 
;isase wopa yA banxUka calAne ke arWa meM AyA lagawA hE, yaha Sabxa . golI calAne ke 
;arWa se wIra calAne ke arWa meM .
;
;  wopa Axi ke calAwe samaya Aga ke prayoga se hI bArUxa yA goloM ko nikAlA
;yA PeMkA jAwA WA . usase nikAlane ke arWa meM, vo BI balAw arWAw-
;nikAla PeMkanA .  
; 
;  BattI meM pakAnA Aga meM pakAnA hI hE . ye wo sAkRAw samaJA jA sakawA hE . 
;
