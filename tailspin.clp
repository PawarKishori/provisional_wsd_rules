
;@@@Added by 14anu-ban-07,(14-04-2015)
;The stock went into a tailspin.(coca)
;स्टॉक अनियन्त्रित गिरावट में गया . (manual)
(defrule tailspin1
(declare (salience 1000))
(id-root ?id tailspin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id2)
(kriyA-into_saMbanXI  ?id1 ?id)
(id-root ?id2 stock|price|share|economy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aniyaMwriwa_girAvata))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tailspin.clp 	tailspin1   "  ?id "  aniyaMwriwa_girAvata )" crlf))
)

;@@@Added by 14anu-ban-07,(14-04-2015)
;It might also help explain why he went into an emotional tailspin.(coca)
;यह बताने में  सहायता भी कर सकता है कि वह भावनात्मक भँवर में क्यों चला गया था.(manual)
(defrule tailspin2
(declare (salience 1100))
(id-root ?id tailspin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 emotional)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bazvara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tailspin.clp 	tailspin2   "  ?id "  Bazvara )" crlf))
)

;-------------------------------------- Default Rules ------------------------------------

;@@@Added by 14anu-ban-07,(14-04-2015)
;A small plane goes into a tailspin and plummets to the earth killing four people on board.(coca)
;छोटा विमान चक्राकार गोते  में जाता है और पृथ्वी में अचानक गिर जानेसे  चार लोगों की मृत्यु  होती है  विमान पर . (manual)
(defrule tailspin0
(id-root ?id tailspin)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakrAkAra_gowA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  tailspin.clp         tailspin0   "  ?id "  cakrAkAra_gowA )" crlf))
)

