
;@@@ Added by 14anu-ban-11 on (20-04-2015)
;He travelled from town to town selling his wares.(oald)
;वह अपना सौदा बेचने के लिये नगर से नगर तक यात्रा की .  (self)
(defrule ware1
(declare (salience 10))
(id-root ?id ware)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(id-root ?id1 sell)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sOxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ware.clp 	ware1   "  ?id "  sOxA)" crlf))
)


;-------------------------------------- Default Rules ----------------------------------------

;@@@ Added by 14anu-ban-11 on (20-04-2015)
;A collection of local ware.(oald)
;स्थानीय सामान का एक सङ्ग्रह . (self)
(defrule ware0
(declare (salience 10))
(id-root ?id ware)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ware.clp     ware0   "  ?id "  sAmAna)" crlf))
)


