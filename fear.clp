
(defrule fear0
(declare (salience 5000))
(id-root ?id fear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-for_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye_dara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " fear.clp fear0 " ?id "  ke_liye_dara )" crlf)) 
)

(defrule fear1
(declare (salience 4900))
(id-root ?id fear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-for_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye_dara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " fear.clp fear1 " ?id "  ke_liye_dara )" crlf)) 
)

(defrule fear2
(declare (salience 4800))
(id-root ?id fear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-for_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_liye_dara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " fear.clp fear2 " ?id "  ke_liye_dara )" crlf)) 
)

(defrule fear3
(declare (salience 4700))
(id-root ?id fear)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ke_liye_dara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fear.clp	fear3  "  ?id "  " ?id1 "  ke_liye_dara  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-02-2015)
;More than 80 people were feared dead last night after flash floods, the worst in 34 years.[oald]
;आकस्मिक बाढ के बाद 80 से ज्यादा लोगों के मृत्यु की आशंका की गई थी , जो कि 34 वर्षों में सबसे अधिक खराब था.[oald]
(defrule fear5
(declare (salience 5001))
(id-root ?id fear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-after_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ASaMkA_kara))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fear.clp 	fear5   "  ?id "  ASaMkA_kara)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  fear.clp 	fear5  "  ?id " kA )" crlf))
)

;--------------------- Default Rules ----------------

(defrule fear4
(declare (salience 4600))
(id-root ?id fear)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fear.clp 	fear4   "  ?id "  dara )" crlf))
)

;default_sense && category=verb	Baya_kara	0
;default_sense && category=verb	dara	0
;"fear","V","1.dara"
;She does not fear death.
;
;
