;@@@Added by 14anu-ban-07,(21-03-2015)
;What did you do that for, you turkey?(cambridge)
;आपने किया क्या हैं इस के लिए , नासमझ  ? (manual)
;Dick, you turkey, pull the tiller towards you NOW! (coca)
;डिक ,नासमझ,खींचो हत्था आपनी ओर अभी! (manual)
(defrule turkey1
(declare (salience 1000))
(id-root ?id turkey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nAsamaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turkey.clp 	turkey1   "  ?id "  nAsamaJa )" crlf))
)

;@@@Added by 14anu-ban-07,(21-03-2015)
;His latest movie is a real turkey.(oald)
;उसका  नवीनतम चलचित्र बहुत ही असफल है . (manual)
(defrule turkey2
(declare (salience 1200))
(id-root ?id turkey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 film|movie)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asaPala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turkey.clp 	turkey2   "  ?id "  asaPala )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-07,(21-03-2015)
;We raise turkeys mainly for the Christmas market.(oald)
;हम पीरू पक्षी क्रिसमस बाजार के लिए प्रमुख रूप से बड़ा (manual) (manual)
(defrule turkey0
(id-root ?id turkey)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pIrU_pakRI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turkey.clp   turkey0   "  ?id "  wqNAcCAxiwa_BUmi )" crlf))
)

