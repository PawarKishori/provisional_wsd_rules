;@@@ Added by 14anu-ban-10 on (17-02-2015)
;The injection gave the patient much relief.[hinkhoj]
;इंजेक्शन ने मरीज को बहुत  आराम दिया।[manual]
(defrule relief2
(declare (salience 5100))
(id-root ?id relief)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?  )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ArAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relief.clp 	relief2   "  ?id "  ArAma)" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-02-2015)
;He did duty from 9a.m.to8p.m.with only one hours relief.[hinkhoj]
;उसने  9a. से 7  काम किया  सिर्फ एक घण्टा  विश्राम लेकर  . [manual]
(defrule relief3
(declare (salience 5200))
(id-root ?id relief)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1 )
(id-root ?id1 duty)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSrAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relief.clp 	relief3   "  ?id "  viSrAma)" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-02-2015)
;A profile of Julius Caesar in relief.[hinkhoj]
; एक प्रोफ़ाइल  में जूलियस सीजर  के उभरी हुई नक्काशी की.[manual]
(defrule relief4
(declare (salience 5200))
(id-root ?id relief)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-in_saMbanXI  ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uBarI_huI_nakkASI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relief.clp 	relief4   "  ?id "   uBarI_huI_nakkASI)" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-02-2015)
;They got temporary financial relief from the tax due to severe drought.[hinkhoj]
;उनहे अस्थायी  आर्थिक छूट मिली  टैक्स से  भारी अकाल की वजह से ।[manual]
(defrule relief5
(declare (salience 5300))
(id-root ?id relief)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1 )
(id-word =(- ?id 1) financial)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) CUta ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " relief.clp  relief5  "  ?id "  " - ?id 1 " CUta   )" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-02-2015)
;She sent some relief to refugees.[hinkhoj]
;उसने शरणार्थियों को कुछ सहायता भेजा है।[manual]
(defrule relief6
(declare (salience 5400))
(id-root ?id relief)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id1 ?id)
(id-root ?id1 send)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAyawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relief.clp 	relief6   "  ?id "  sahAyawA)" crlf))
)

;@@@ Added by 14anu-ban-10 on (17-02-2015)
;A relief map of India,i.e.showing mountains,valleys,gulfs,seas,etc.[hinkhoj]
;एक उभरी हुई नक्काशी भारत  के नक्षा की ,i.e.  प्रदर्शत  पहाड़ों, घाटियों,कुंड  , समुद्र etc.[manual]
(defrule relief7
(declare (salience 5500))
(id-root ?id relief)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id1 ?id )
(id-root ?id1 map)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uBarI_huI_nakkASI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relief.clp 	relief7   "  ?id "   uBarI_huI_nakkASI)" crlf))
)

;------------------------ Default Rules ----------------------
;"relief","Adj","1.sahAyawA_koRa"
;Relief fund was collected for helping the quake victims.
(defrule relief0
(declare (salience 5000))
(id-root ?id relief)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sahAyawA_koRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relief.clp 	relief0   "  ?id "  sahAyawA_koRa )" crlf))
)

(defrule relief1
(declare (salience 4900))
(id-root ?id relief)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAhawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  relief.clp 	relief1   "  ?id "  rAhawa )" crlf))
)

;default_sense && category=noun	uBarI_huI_nakkASI	0
;"relief","N","1.uBarI_huI_nakkASI"
;A relief map of India,i.e.showing mountains,valleys,gulfs,seas,etc. 
;--"2.raMga_Ora_SediMga_xvArA_UzcAI_kA_praxarSana"
;The hills stood out in sharp relief against the dawn sky.
;--"1.sahAyawA{koRa}"
;She sent some relief to refugees.
;--"2.ArAma"
;The injection gave the patient much relief.
;--"3.viSrAma"
;He did duty from 9a.m.to8p.m.with only one hour's relief.   
;--"4.rAhawa"
;She heaved a sigh of relief when her son returned home safely. 
;--"5.CUta"
;They got temporary financial relief from the tax due to severe drought.     
;
