;@@@ Added by 14anu-ban-04 (28-03-2015)
;She embroidered the cushion cover with flowers.         [oald]
;उसने फूलों से तकीया आवरण पर कढ़ाई की .                           [self]             
(defrule embroider1
(declare (salience 20))
(id-root ?id embroider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id para)) 
(assert (id-wsd_root_mng ?id kaDAI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " embroider.clp     embroider1   "  ?id " para  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  embroider.clp 	embroider1 "  ?id " kaDAI_kara )" crlf))
)


;@@@ Added by 14anu-ban-04 (28-03-2015)
;Naturally, I embroidered the tale a little to make it more interesting.              [cald]
;स्वभाविक रूप से, मैंने  कहानी को थोड़ा अलंकृत किया उसे अधिक रोचक बनाने के लिए   .                          [self]
(defrule embroider2
(declare (salience 30))
(id-root ?id embroider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 tale|story|fact|picture)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id alaMkqwa_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " embroider.clp     embroider2   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  embroider.clp 	embroider2 "  ?id " alaMkqwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-04 (28-03-2015)
;She embroidered flowers on the cushion covers.            [oald]      ;run this sentence on psarse no. 3
;उसने तकीया आवरणों पर फूलों की कढ़ाई की .                      [self]
(defrule embroider3
(declare (salience 30))
(id-root ?id embroider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(kriyA-on_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id kI)) 
(assert (id-wsd_root_mng ?id kaDAI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " embroider.clp     embroider3   "  ?id " kI  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  embroider.clp 	embroider3 "  ?id " kaDAI_kara )" crlf))
)

;---------------------------------------------------------DEFAULT RULE -------------------------------------------------------------------


;@@@ Added by 14anu-ban-04 (28-03-2015)
;The sleeves were embroidered in gold.                      [oald]      
;बाँहों को सोने में काढ़ा गया था .                                        [self]
(defrule embroider0
(declare (salience 10))
(id-root ?id embroider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko)) 
(assert (id-wsd_root_mng ?id kADa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* " embroider.clp     embroider0   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  embroider.clp 	embroider0  "  ?id " kADa )" crlf))
)
