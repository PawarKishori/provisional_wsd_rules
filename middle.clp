
(defrule middle0
(declare (salience 5000))
(id-root ?id middle)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id middling )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id maXya_SreNI_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  middle.clp  	middle0   "  ?id "  maXya_SreNI_kA )" crlf))
)

;"middling","Adj","1.maXya SreNI kA"
;Don't always buy middling objects.
;
(defrule middle1
(declare (salience 4900))
(id-root ?id middle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bIca_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  middle.clp 	middle1   "  ?id "  bIca_kA )" crlf))
)

;"middle","Adj","1.bIca_[kA]"
;You have to reach some middle point in this case.
;
(defrule middle2
(declare (salience 4800))
(id-root ?id middle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maXyasWala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  middle.clp 	middle2   "  ?id "  maXyasWala )" crlf))
)

;"middle","N","1.maXyasWala"
;Don't leave things in the middle of the room.

;@@@ Added by 14anu-ban-08 (07-10-2014)
;Wikipedia's radical openness also means that any given article may be,at any given moment,in a bad state,such as in the middle of a large edit,or a controversial rewrite.    [wiki]
;विकिपीडिया के अतिवादी खुलेपन का यह भी अर्थ है कि किसी दिए गए समय पर कोई दिया गया लेख किसी बुरी स्थिति जैसेकि एक बड़े संपादन या एक विवादग्रस्त पुनर्लेखन के मध्य में हो सकता है.   [manual]
(defrule middle3
(declare (salience 4900))
(id-root ?id middle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maXya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  middle.clp 	middle3   "  ?id " maXya )" crlf))
)

;
