;@@@Added by 14anu-ban-02(20-03-2015)
;A baleful look.[oald]
;एक डरावनी नजर . [self]
(defrule baleful1 
(declare (salience 100)) 
(id-root ?id baleful) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 look|appearance)
 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id darAvanA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  baleful.clp  baleful1  "  ?id "  darAvanA )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(20-03-2015)
;Sentence: The baleful effects of water pollution. [mw]
;Translation: जल प्रदूषण के हानिकारक प्रभाव .  [self]
(defrule baleful0 
(declare (salience 0)) 
(id-root ?id baleful) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id hAnikAraka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  baleful.clp  baleful0  "  ?id "  hAnikAraka )" crlf)) 
) 
