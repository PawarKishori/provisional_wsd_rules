;@@@Added by 14anu-ban-02(28-02-2015)
;Sentence: His aptitude for dealing with children got him the job.[oald]
;Translation: उसकी बच्चों से तालमेल की अभिक्षमता के कारण उसे नौकरी मिली. [self]
(defrule aptitude0 
(declare (salience 0)) 
(id-root ?id aptitude) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id aBikRamawA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  aptitude.clp  aptitude0  "  ?id "  aBikRamawA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(28-02-2015)
;She showed a natural aptitude for the work.[oald]
;उसने कार्य के लिए प्राकृतिक रुझान दिखाया . [self]
(defrule aptitude1 
(declare (salience 100)) 
(id-root ?id aptitude) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id ?id1) 
(id-root ?id1 natural)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id ruJAna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  aptitude.clp  aptitude1  "  ?id "  ruJAna )" crlf)) 
) 
