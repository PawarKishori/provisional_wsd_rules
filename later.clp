;@@@Added by 14anu-ban-08 (09-02-2015)
;She later became a doctor.   [oald]
;वह  बाद में डाक्टर बन गयी.   [self]
(defrule later0
(declare (salience 0))
(id-root ?id later)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAxa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  later.clp    later0   "  ?id "  bAxa_meM)" crlf))
)

;@@@Added by 14anu-ban-08 (09-02-2015)
;It was a tragedy that darkened his later life.    [oald]
;यह एक ऐसी दुःखद घटना थी जिसने उसकी पिछली ज़िंदगी अन्धकारमय डाल दी .  [self]
(defrule later1
(declare (salience 10))
(id-root ?id later)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 life)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  later.clp    later1   "  ?id " piCalA )" crlf))
)


