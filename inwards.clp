;@@@ Added by 14anu-ban-06 (02-03-2015)
;The door opens inwards.(OALD)[parser no. 4]
;दरवाजा अन्दर की ओर खुलता है . (manual)
(defrule inwards0
(declare (salience 0))
(id-root ?id inwards)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anxara_kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inwards.clp 	inwards0   "  ?id "  anxara_kI_ora )" crlf))
)


;@@@ Added by 14anu-ban-06 (02-03-2015)
;Her thoughts turned inwards. (OALD)[parser no. 4]
;उसके विचार अन्तर्मुखी हो गए . (manual)
(defrule inwards1
(declare (salience 2000))
(id-root ?id inwards)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id2 thought)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwarmuKI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inwards.clp 	inwards1   "  ?id "  anwarmuKI )" crlf))
)
