
;@@@ Added by 14anu-ban-05 on (05-12-2014)
;The adult beetles, which visit flowers and feed on pollen and buds, may also gnaw at young branches and leaves.[AGRICULTURE]
;वयस्क  भृङ्ग जो  फूलों पर भ्रमण  करते हैं और पराग और कलियों से  भोजन  ग्रहण करते हैं, नन्हीं शाखाओं  और पत्तियों को भी कुतर सकते हैं.[MANUAL]
(defrule gnaw0
(declare (salience 1000))
(id-root ?id gnaw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  gnaw.clp 	gnaw0   "  ?id "  kuwara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (05-12-2014)
;Self-doubt began to gnaw away at her confidence.[OALD]
;आत्म शक उसके आत्मविश्वास का नाश करना शुरू कर दिया.[MANUAL]
(defrule gnaw1
(declare (salience 1000))
(id-root ?id gnaw)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 away)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nASa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " gnaw.clp  gnaw1  "  ?id "  " + ?id 1 "  nASa_kara  )" crlf))
)
