;Added By Vivek Singh IIT(BHU)
;We have located the target.
(defrule locate0
(declare (salience 6000))
(id-root ?id locate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id pawA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  locate.clp  	locate0   "  ?id "  pawA_lagA )" crlf))
)

;$$$--- Modified by Nandini(19-5-14) ;condition Added by Nandini(kriyA-near_saMbanXI  ?id ?id1)
;Added By Vivek Singh IIT(BHU)
;The headquarters of the Northern railways is located here. 
;uwwarI relave kA muKyAlaya yahAz sWiwa hE. ;Added hin-sentence  by Nandini(19-5-14)  
(defrule locate1
(declare (salience 6000))
(id-root ?id locate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-in_saMbanXI  ?id ?id1)(kriyA-aXikaraNavAcI  ?id ?id1)(kriyA-near_saMbanXI  ?id ?id1))

=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWiwa_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  locate.clp  	locate1   "  ?id "  sWiwa_hE )" crlf))
)

;@@@--- Added by Nandini(19-5-14)
;The company chose to locate its factory near the airport.[OALD]
;kampanI ne vimAnapawwana ke nikata unakI PEktarI kA sWAna nirXAraNa karanA waya kiyA.
(defrule locate2
(declare (salience 6000))
(id-root ?id locate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
(id-root ?id1 choose)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWAna_nirXAraNa_karanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  locate.clp  	locate2   "  ?id "  sWAna_nirXAraNa_karanA )" crlf))
)

;@@@ Added by 14anu-ban-06 Karanveer Kaur (23-07-2014)
;Nadala Basin , where the elephant research camp is located .   (Parallel Corpus)
;नडाला नदी क्षेत्र , जहाँ हाथी शोध कैंप स्थित है ।
(defrule locate3
(declare (salience 6000))
(id-root ?id locate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWiwa_hE))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  locate.clp  	locate3   "  ?id "  sWiwa_hE )" crlf))
)

;@@@ Added by 14anu-ban-08 (05-12-2014)     ;Need to be improved
;Let the masses 100g, 150g and 200g be located at O, A and B be respectively.   [NCERT]
;माना कि 100g, 150g एवं 200g के द्रव्यमान क्रमशः O, A एवं B पर स्थित हैं.    [NCERT]
(defrule locate4
(declare (salience 0))     ;salience decreased by 6001 to 0 by 14anu-ban-08 (02-03-2015)
(id-word ?id located)             ;modify 'id-root' to 'id-word' by 14anu-ban-08 (02-03-2015) 
?mng <-(meaning_to_be_decided ?id)
;(kriyA-at_saMbanXI  ?id ?id1)   ;commented by 14anu-ban-08 (02-03-2015)
;(id-cat_coarse ?id1 symbol)      ;commented by 14anu-ban-08 (02-03-2015)
;(id-root ?id1 x|O|A|B)           ;commented by 14anu-ban-08 (02-03-2015)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  locate.clp  	locate4   "  ?id "  sWiwa )" crlf))
)

;@@@Added by 14anu-ban-08 (02-03-2015)
;They located their headquarters in Swindon.  [oald]
;उन्होनें उसके मुख्यालय स्विंदोन में स्थापित किए. [self]
(defrule locate5
(declare (salience 6000))
(id-root ?id locate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 headquarters|hospital|school|industry|temple|monument)  ;added 'hospital|school|industry|temple|monument' by 14anu-ban-08 (17-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWApiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  locate.clp  	locate5   "  ?id "  sWApiwa_kara )" crlf))
)

