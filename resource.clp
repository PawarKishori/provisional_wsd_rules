;$$$ Modified by 14anu02 on 24.06.14
;The local library is a valuable resource.
;स्थानीय पुस्तकालय एक मूल्यवान संसाधन है . 
(defrule resource0
(declare (salience 5000))
(id-root ?id resource)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)	;uncommented by 14anu02
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saMsAXana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  resource.clp     resource0   "  ?id " saMsAXana )" crlf))
)

;@@@ Added by 14anu-ban-10 on (09-02-2015)
;While seeing the rich resource of wild animals of Manyara National Park we arrived at the grassy land between Masasa and Chem rivers .[tourism corpus]
;मनयारा नेशनल पार्क की समृद्ध वन्य पशु सम्पदा के दर्शन करते-करते हम मसासा व केम नदियों के बीच के घासीले मैदान में जा पहुँचे । [tourism corpus]
(defrule resource3
(declare (salience 5100)) 
(id-root ?id resource)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ? )
(id-cat_coarse ?id noun)	
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id saMpaxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  resource.clp     resource3   "  ?id " saMpaxA)" crlf))
)

;------------------- Default Rules -----------------------

;@@@ Added by 14anu02 on 24.06.14
;Schools in the area have been adequately resourced.;Added by 14anu-ban-10 on (08-12-2015)
;क्षेत्र में विद्यालय के लिये पर्याप्त रूप से साधन जुटाए गए है.
(defrule resource1
(declare (salience 5000))
(id-root ?id resource)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAXana_jutA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  resource.clp     resource1   "  ?id " sAXana_jutA )" crlf))
)

;@@@ Added by 14anu02 on 24.06.14
;Schools in the area are still inadequately resourced. ;Added by 14anu-ban-10 on (08-12-2015)
;क्षेत्र में विद्यालय अभी भी अपर्याप्त रूप से संसाधित है.
(defrule resource2
(declare (salience 5000))
(id-root ?id resource)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMsAXiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  resource.clp     resource2   "  ?id " saMsAXiwa )" crlf))
)
