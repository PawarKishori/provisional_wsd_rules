
;Added by human
(defrule quiet0
(declare (salience 5000))
(id-root ?id quiet)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 keep)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAMwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quiet.clp 	quiet0   "  ?id "  SAMwa )" crlf))
)

(defrule quiet1
(declare (salience 4900))
(id-root ?id quiet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIrava))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quiet.clp 	quiet1   "  ?id "  nIrava )" crlf))
)

(defrule quiet2
(declare (salience 4800))
(id-root ?id quiet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAMwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quiet.clp 	quiet2   "  ?id "  SAMwa_kara )" crlf))
)

;"quiet","VT","1.SAMwa_karanA"
;Quiet the dragons of worry && fear
;

;@@@ Added by 14anu-ban-04 on 28-07-2014
;Manas National Park is very quiet, full of natural beauty, and is enchanting.
;mAnasa raStriya parka awyanwa SAnwa ,praAkqwika sunxarawA se BarapUra Ora mohaka hE.
;According to nature lovers the quiet atmosphere and clean environment attracts migratory birds .
;prakarwi premiyoM ke muwAbika yahAz kA SAnwa mAhOla svacCa paryAvaraNa pravAsI panCiyoM ko AkarRiwa karawA hE.

(defrule quiet3
(declare (salience 4500))
(id-root ?id quiet)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quiet.clp 	quiet3   "  ?id "  SAnwa )" crlf))
)
