;@@@ Added by 14anu-ban-06 (14-03-2015)
;Her novels are infused with sadness.(OALD)
;उसके उपन्यास विषाद के साथ भरे होते हैं . (manual)
(defrule infuse1
(declare (salience 2000))
(id-root ?id infuse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 novel|book)
(kriyA-with_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BarA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " infuse.clp   infuse1   "   ?id " BarA_ho )" crlf))
)


;@@@ Added by 14anu-ban-06 (14-03-2015)
;Politics infuses all aspects of our lives. (OALD)
;राजनीति हमारे जीवन के सभी पहलूओ को प्रभावित करती है . (manual)
(defrule infuse2
(declare (salience 2100))
(id-root ?id infuse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 aspect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praBAviwa_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " infuse.clp   infuse2   "   ?id " praBAviwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  infuse.clp       infuse2   "  ?id " ko )" crlf))
)

;@@@ Added by 14anu-ban-06 (14-03-2015)
;Allow the tea to infuse for five minutes.(cambridge)
;चाय को पाँच मिनट के लिए घुल जाने दीजिए . (manual)
(defrule infuse3
(declare (salience 2200))
(id-root ?id infuse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id2 ?id1)
(saMjFA-to_kqxanwa ?id1 ?id)
(id-root ?id1 tea|coffee|herb)
(id-root ?id2 allow)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gula_jA))
(assert  (id-wsd_viBakwi   ?id1  ko)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " infuse.clp   infuse3   "   ?id " Gula_jA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  infuse.clp      infuse3   "  ?id1 " ko )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (14-03-2015)
;The arrival of a group of friends on Saturday infused new life into the weekend.(cambridge)
;शनिवार को मित्रों के समूह के आगमन ने सप्ताहान्त में नया जीवन भर दिया . (manual)
(defrule infuse0
(declare (salience 0))
(id-root ?id infuse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " infuse.clp   infuse0   "   ?id " Bara_xe )" crlf))
)

