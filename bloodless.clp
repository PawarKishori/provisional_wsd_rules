;@@@Added by 14anu-ban-02(31-03-2015)
;Bloodless lips.[oald]	;run the sentence on parser no. 14
;रूखे ओंठ . [self]
(defrule bloodless1 
(declare (salience 100)) 
(id-root ?id bloodless) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id rUKA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloodless.clp  bloodless1  "  ?id "  rUKA )" crlf)) 
)


;@@@Added by 14anu-ban-02(31-03-2015)
;Her speeches are bloodless and dull .[oald]
;उसके भाषण भावहीन और सुस्त हैं . [self]
(defrule bloodless2 
(declare (salience 100)) 
(id-root ?id bloodless) 
?mng <-(meaning_to_be_decided ?id) 
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 speech)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id BAvahIna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloodless.clp  bloodless2  "  ?id "  BAvahIna )" crlf)) 
)


;@@@Added by 14anu-ban-02(31-03-2015)
;His face was bloodless with fear.[mw]
;उसका चेहरा भय से  पीला पड़ गया था . [self]
(defrule bloodless3 
(declare (salience 100)) 
(id-root ?id bloodless) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-with_saMbanXI  ?id ?id1)
(id-root ?id1 fear )
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pIlA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloodless.clp  bloodless3  "  ?id "  pIlA )" crlf)) 
)

;--------------default_rules-------------------


;@@@Added by 14anu-ban-02(31-03-2015)
;Sentence: A bloodless revolution.[oald]
;Translation: रक्तहीन क्रान्ति . [self]
(defrule bloodless0 
(declare (salience 0)) 
(id-root ?id bloodless) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id rakwahIna)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  bloodless.clp  bloodless0  "  ?id "  rakwahIna )" crlf)) 
) 


