;@@@Added by 14anu-ban-08 (09-02-2015)
;The incident put her in a bad mood.         [oald]
;घटना ने उसको खराब मनोदशा में डाल दिया.               [self]
(defrule mood0
(declare (salience 0))
(id-root ?id mood)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manoxaSA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mood.clp 	mood0   "  ?id "  manoxaSA  )" crlf))
)

;@@@Added by 14anu-ban-08 (09-02-2015)
;Her mood darkened at the news.       [oald]
;उसका मन समाचार सुन कर उदास हो गया .      [self]
(defrule mood1
(declare (salience 10))
(id-root ?id mood)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)
(id-root ?id1 darken)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mood.clp 	mood1   "  ?id "  mana )" crlf))
)
