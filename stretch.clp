
(defrule stretch0
(declare (salience 5000))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PElA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stretch.clp	stretch0  "  ?id "  " ?id1 "  PElA  )" crlf))
)

(defrule stretch1
(declare (salience 4900))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PEla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stretch.clp	stretch1  "  ?id "  " ?id1 "  PEla  )" crlf))
)

;commented by 14anu-ban-01 0n (08-09-2014) as this rule is totally similar to stretch0. 
;(defrule stretch2
;(declare (salience 4800))
;(id-root ?id stretch)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 out)
;(kriyA-upasarga ?id ?id1)
;(kriyA-object ?id ?)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PElA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stretch.clp	stretch2  "  ?id "  " ?id1 "  PElA  )" crlf))
;)

;commented by 14anu-ban-01 0n (08-09-2014) as this rule is totally similar to stretch1.
;(defrule stretch3
;(declare (salience 4700))
;(id-root ?id stretch)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 out)
;(kriyA-upasarga ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PEla))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stretch.clp	stretch3  "  ?id "  " ?id1 "  PEla  )" crlf))
;)




;@@@ Added by 14anu-ban-01 on (08-09-2014).
;Sentence1: Sita stretched the rope tightly.[stretch.clp] <<This sentence is interpretted wrongly by the parser>>
;Translation: sIwA ne kasakara rassI ko KIMcA[manual]
;सीता ने कसकर रस्सी को खींचा[manual]
;Sentence2: Sita stretched the rope.
;सीता ने रस्सी को खींचा[manual]
(defrule stretch6 
(declare (salience 4600)) ;salience increased from 4500 by 14anu-ban-01 on (17-02-2015).
(id-word ?id stretched) 	;changed 'root' to 'word' by 14anu-ban-01 on (17-02-2015).
?mng <-(meaning_to_be_decided ?id) 
(id-root ?id1 rope)
(kriyA-object ?id ?id1)
(kriyA-subject ?id ?id2) 	;Added by 14anu-ban-01 on (17-02-2015)
(id-cat_coarse ?id2 PropN)	;Sita cannot be recognized as 'human/animate' by the parser;Added by 14anu-ban-01 on (17-02-2015)
(id-cat_coarse ?id verb)
 => 
(retract ?mng) 
(assert (id-wsd_word_mng ?id KIMcA/wAnA)) ;corrected spelling of 'KIMcA/wAnA' by 14anu-ban-01 on (21-10-2014).
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (kriyA_id-subject_viBakwi ?id ne))
(if  ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stretch.clp  stretch6  "  ?id "  KIMcA/wAnA)" crlf);changed id-wsd-root to id-wsd_word_mng by 14anu-ban-01 on (21-10-2014).
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  stretch.clp    stretch6   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  stretch.clp    stretch6   "  ?id " ne )" crlf) 
)
) 

;@@@ Added by 14anu-ban-01 0n (10-09-2014).
;Sentence: After waking we usually stretch our body.[stretch.clp]
;Translation:आमतौर पर उठने के बाद हम अंगड़ाई लेते हैँ.[manual]
(defrule stretch7
(declare (salience 4500))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 body)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) ?id1 aMgadZAI_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stretch.clp	stretch7  "  ?id " "(+ ?id 1)"   " ?id1 " aMgadZAI_le  )" crlf))
)

;@@@ Added by 14anu-ban-01 0n (10-09-2014).
;Sentence:We can stretch the life of fruits by keeping them in the refrigerator.[stretch.clp]
;Translation1:हम फलों को रेफ्रिजरेटर  में रखकर उनकी आयु बढा सकते हैं[on the basis of meaning given in the file below]
;Translation2:फलों को रेफ्रिजरेटर  में रखने से हम उन्हे लम्बे समय तक ताज़ा रख सकते हैं.[more natural translation]
(defrule stretch8
(declare (salience 4500))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 life)
(viSeRya-of_saMbanXI ?id1 ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id2 kI))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1 Ayu_baDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  stretch.clp    stretch8   "  ?id " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stretch.clp	stretch8  "  ?id "    " ?id1 " Ayu_baDZA  )" crlf))
)

;@@@ Added by 14anu-ban-01 0n (14-09-2014).
;Influence of Pandit Nehru stretched all across the country.
;पंडित नेहरू का प्रभुत्व पूरे देश में  यहाँ  से वहाँ तक फैल गया था.
(defrule stretch9
(declare (salience 4500))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 influence)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEla_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch9   "  ?id "  PEla_jA)" crlf))
)

;@@@ Added by 14anu-ban-01 0n (14-09-2014).
;He stretched all his energy to come in the rain.
;गिरते हुए पानी में आने के लिये उसने अपनी सारी शक्ति पूर्णरूप से लगा दी.
(defrule stretch10
(declare (salience 4500))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 energy)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNarUpa_se_lagA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch10   "  ?id " pUrNarUpa_se_lagA_xe)" crlf))
)

;@@@ Added by 14anu-ban-01 0n (19-09-2014).
;He stretch the limits of truth to make himself popular.
;खुद को मशहूर बनाने के लिये उसने सच   <की सारी सीमाएँ पार कर दीं/को ज़रूरत से ज्यादा खींच दिया> .
(defrule stretch11
(declare (salience 4500))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 limit)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZarUrawa_se_jZyAxA_KIMca_xe))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  stretch.clp    stretch11   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch11   "  ?id " jZarUrawa_se_jZyAxA_KIMca_xe)" crlf))
)

;@@@ Added by 14anu-ban-01 0n (20-10-2014).
;If you stretch a helical spring by gently pulling its ends, the length of the spring increases slightly.[NCERT corpus]
;yaxi kisI kuNdaliwa spriMga ke siroM ko XIre se KIMcakara viswAriwa kiyA jAe wo spriMga kI lambAI WodI baDa jAwI hE.[NCERT corpus]
(defrule stretch12
(declare (salience 3000))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 spring)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viswAriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch12   "  ?id " viswAriwa_kara )" crlf))
)

;@@@ Added by 14anu13 on 03-07-14
;She was overtaken on the last stretch of the course.    [sentence from http://www.oxfordlearnersdictionaries.com/definition/english/course_1]
;वह  दौड़ की अंतिम अवधि में पिछड़ गयी थी|
(defrule stretch13
(declare (salience 6000))
(id-root ?id stretch)
(id-root ?id1 last|first|before|after)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avaXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch13   "  ?id "  avaXi )" crlf))
)

;@@@ Added by 14anu-ban-01 on (17-02-2015): Stretch5 split into 2 rules--stretch14 and stretch5.
;But in reality, bodies can be stretched, compressed and bent.[NCERT corpus]
;परन्तु वास्तव में पिंडों को तनित, सम्पीडित अथवा बङ्कित किया जा सकता है.[NCERT corpus]
(defrule stretch14
(declare (salience 4500))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-root ?id1 ?str)		
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waniwa_kara))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  stretch.clp 	stretch14  "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch14   "  ?id " waniwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-01 on (17-02-2015)
;Stretching the arms increases I about the axis of rotation, resulting in decreasing the angular speed ω.[NCERT corpus]
;भुजाओं को फैलाने से घूर्णन अक्ष के परितः I बढ जायेगा, परिणामस्वरूप कोणीय वेग ω कम हो जायेगा[NCERT corpus]
;While the chair is rotating with considerable angular speed stretch your arms horizontally.[NCERT corpus]
;जबकि कुर्सी पर्याप्त कोणीय चाल से घूम रही हो अपनी भुजाओं को क्षैतिज दिशा में फैलाइये[NCERT corpus]
(defrule stretch15
(declare (salience 4500))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 arms)
(kriyA-object ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA))
;(assert (make_verbal_noun ?id))		;commented by 14anu-ban-01 on (18-02-2015)
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun   " ?*prov_dir* "  stretch.clp 	stretch15   "  ?id " )" crlf)				;commented by 14anu-ban-01on (17-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  stretch.clp 	stretch15 "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch15   "  ?id " PElA)" crlf))
)

;------------------------ Default Rules ----------------------
(defrule stretch4
(declare (salience 4600))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viswAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch4   "  ?id "  viswAra )" crlf))
)

;Modified again by 14anu-ban-01 on (17-02-2015): Split the rule into stretch5 and stretch14;removed previous modifications,added new meaning and added vibhakti in the original rule.
;$$$ Modified by 14anu-ban-01 on (21-10-2014)
;If you stretch a helical spring by gently pulling its ends, the length of the spring increases slightly.[NCERT corpus]
;"stretch","V","1.KIMcakara baDZAnA/wAnA jAnA"
;Stretch the rubber band to make it loose.[stretch.clp]
;रबरबैंड को ढीला करने के लिए उसे खींचिए /खींचकर बढाइए. [self]
(defrule stretch5
(declare (salience 4500))
(id-root ?id stretch)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KIMcakara_baDZA/KIMca))	;added " KIMca" by 14anu-ban-01 on (17-02-2015)
(assert (kriyA_id-object_viBakwi ?id ko))		;added by 14anu-ban-01 on (17-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  stretch.clp 	stretch5   "  ?id " ko )" crlf)		;added by 14anu-ban-01 on (17-02-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stretch.clp 	stretch5   "  ?id " KIMcakara_baDZA/KIMca )" crlf))		;added " KIMca" by 14anu-ban-01 on (17-02-2015)
)



;"stretch","V","1.KIMcakara baDZAnA/wAnA jAnA"
;Stretch the rubber band to make it loose.
;--"2.jZora se KIMcanA"
;Sita stretched the rope tightly.
;--"3.aMgadZAI lenA"
;After waking we usually stretch our body.
;--"4.kisI vaswu kA Ayu baDZAnA"
;We can stretch the life of fruits by keeping them in the refrigerator.
;--"5.PElAnA"
;Influence of Pandit Nehru stretched all across the country.
;--"6.pUrNarUpa_se_lagAnA"
;He stretched all his energy to come in the rain.
;--"7.jZarUrawa se jyAxA KIMcanA"
;He stretch the limits of truth to make himself popular.
;
;
