;@@@ Added by 14anu-ban-08 (24-02-2015)  ;Run on parser 213
;Just as static charges produce an electric field, the currents or moving charges produce (in addition) a magnetic field, denoted by B( r), again a vector field.  [NCERT]
;जिस प्रकार स्थिर आवेश विद्युत क्षेत्र उत्पन्न करते हैं, विद्युत धाराएँ अथवा गतिमान आवेश (विद्युत क्षेत्र के साथ-साथ) चुम्बकीय क्षेत्र उत्पन्न करते हैं जिसे B(r) द्वारा निर्दिष्ट किया जाता है तथा यह भी एक सदिश क्षेत्र है.  [NCERT]
(defrule moving0
(declare (salience 0))
(id-root ?id moving)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gawimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  moving.clp 	moving0   "  ?id " gawimAna)" crlf))
)

;@@@ Added by 14anu-ban-08 (24-02-2015)   ;Run on parser 19
;The teacher gave a moving talk during her fare well. [hindkhoj]
;उनकी विदाई में अध्यापक ने  हृदय स्पर्शी बातें की. [self]
(defrule moving1
(declare (salience 10))
(id-root ?id moving)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 talk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hqxaya_sparSI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  moving.clp 	moving1   "  ?id " hqxaya_sparSI)" crlf))
)
