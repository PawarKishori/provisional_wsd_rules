;@@@ Added by 14anu-ban-11 on (18-02-2015)
;You can already use a phone to scout products and compare prices.(coca)
;आप  पहले से ही  टेलीफोन का  उपयोग कर सकते हैं उत्पाद पता लगाने के लिए और मूल्य तुलना  के लिये . (self)
(defrule scout2
(declare (salience 30))
(id-root ?id scout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 product)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_lagAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scout.clp    scout2   "  ?id "  pawA_lagAnA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (18-02-2015)
;Enemy scout sighted.(coca)
;शत्रु गुप्तचर ने देखा .(anusaaraka)
(defrule scout0
(declare (salience 10))
(id-root ?id scout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gupwacara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scout.clp 	scout0   "  ?id "  gupwacara)" crlf))
)

;@@@ Added by 14anu-ban-11 on (18-02-2015)
;They scouted the area for somewhere to stay the night.(oald)
;उन्होंने रात मे  कही रुकने  के लिए मैदान खोजा .(self)
(defrule scout1
(declare (salience 20))
(id-root ?id scout)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KojanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scout.clp 	scout1  "  ?id "  KojanA )" crlf))
)

