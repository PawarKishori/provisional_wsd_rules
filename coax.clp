;@@@ Added by 14anu-ban-03 (09-03-2015)   ;run on parser no.-4
;No amount of coaxing will make me change my mind.[oald]
;किसी भी तरह की चापलूसी  मेरा मन नहीं बदलेगी . [manual]
(defrule coax2
(declare (salience 4900))
(id-root ?id coax)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cApalUsI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coax.clp 	coax2  "  ?id "  cApalUsI )" crlf))
)

;------------------------ Default Rules ----------------------

;"coaxing","N","1.manAnA"
;It took a lot of coaxing before her mother agreed.
(defrule coax0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (09-03-2015)
(id-root ?id coax)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id coaxing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id manAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  coax.clp  	coax0   "  ?id "  manAnA )" crlf))
)

;"coax","V","1.manAnA"
;She coaxed her mother to buy her a new dress.
(defrule coax1
(declare (salience 4900))
(id-root ?id coax)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  coax.clp 	coax1   "  ?id "  manA )" crlf))
)

