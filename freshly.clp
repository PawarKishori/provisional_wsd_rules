;@@@ Added by 14anu-ban-05 on (07-04-2015)
;She had had her hair freshly washed and styled.	[OALD]
;उसने अपने बालों को हाल में धोया और स्टाइल किया  था.			[MANUAL]

(defrule freshly1
(declare (salience 101))
(id-root ?id freshly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAla_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freshly.clp 	freshly1   "  ?id "  hAla_meM )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-05 on (07-04-2015)
;Run this sentence on parse 1 of multi-parse
;flowers freshly picked from the garden.        [OALD]
;उद्यान से ताजे फूल चुने .              [MANUAL]

(defrule freshly0
(declare (salience 100))
(id-root ?id freshly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wAjA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freshly.clp  freshly0   "  ?id "  wAjA )" crlf))
)

