;@@@ Added by 14anu-ban-01 on (17-11-2014)
;Stale has a foul smell.[self]
;पशुमूत्र की गन्ध बदबूदार होती है [self]
(defrule stale0
(declare (salience 0))
(id-root ?id stale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paSumUwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stale.clp     stale0   "  ?id " paSumUwra)" crlf))
)


;@@@ Added by 14anu-ban-01 on (17-11-2014)
;Do not crack stale jokes.[self:with reference to oald]
;पुराने चुटकुले मत सुनाओ[self]
(defrule stale1
(declare (salience 0))
(id-root ?id stale)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id purAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stale.clp     stale1   "  ?id " purAnA)" crlf))
)

;@@@ Added by 14anu-ban-01 on (17-11-2014)
;Their marriage had gone stale.[oald]
;उनकी शादी नीरस हो चुकी थी[self]
(defrule stale2
(declare (salience 0))
(id-root ?id stale)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(kriyA-subject ?id1 ?id2)
(id-root ?id2 marriage|relationship|relation|friendship)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nIrasa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stale.clp     stale2   "  ?id " nIrasa)" crlf))
)

;@@@ Added by 14anu-ban-01 on (17-11-2014)
;Stale food is harmful for the body.[self:with reference to oald]
;बासी/फफूंदा _हुआ खाना शरीर के लिये हानिकारक होता है[self]
(defrule stale3
(declare (salience 0))
(id-root ?id stale)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 food|bread|cake)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAsI/PaPUMxA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  stale.clp     stale3  "  ?id " bAsI/PaPUMxA_huA)" crlf))
)


