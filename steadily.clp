;@@@ Added by 14anu-ban-01 on (28-02-2015)
;The sheer number of steel band programs has steadily increased.[coca]
;ढोल बजानेवाले समूह के प्रोग्रामों की असली सङ्ख्या  स्थिरतापूर्वक/लगातार बढी है . [self]
(defrule steadily0
(declare (salience 0))
(id-root ?id steadily)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWirawApUrvaka))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " steadily.clp  steadily0   "  ?id "  sWirawApUrvaka )" crlf))	
)


;@@@ Added by 14anu-ban-01 on (28-02-2015)
;He was looking at her steadily. [self: with respect to oald]
;वह लगातार/निरन्तर उसकी ओर देख रहा था .[self]
(defrule steadily1
(declare (salience 100))
(id-root ?id steadily)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 look|see|watch)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niranwara/lagAwAra))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " steadily.clp  steadily1   "  ?id "  niranwara/lagAwAra )" crlf))	
)

;@@@ Added by 14anu-ban-01 on (28-02-2015)
;The rain fell steadily.[oald]
;लगातार/निरन्तर वर्षा हुई. [self]
(defrule steadily2
(declare (salience 100))
(id-root ?id steadily)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 rain)
(kriyA-subject  ?id2 ?id1)
(kriyA-kriyA_viSeRaNa  ?id2 ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niranwara/lagAwAra))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " steadily.clp  steadily2   "  ?id "  niranwara/lagAwAra )" crlf))	
)


;@@@ Added by 14anu-ban-01 on (28-02-2015)
;She returned his gaze steadily.[cald]
;उसने  उसकी टकटकी का शान्तिपूर्ण एवं संयत रूप से जवाब दिया. [self]
(defrule steadily3
(declare (salience 100))
(id-root ?id steadily)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id1 ?id)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-subject  ?id1 ?id2)
(id-root ?id3 gaze|look|peek|peep)
(kriyA-object  ?id1 ?id3)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAnwipUrNa_evaM_saMyawa_rUpa_se))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " steadily.clp  steadily3  "  ?id "  SAnwipUrNa_evaM_saMyawa_rUpa_se )" crlf))	
)
