;@@@ Added by 14anu03 on 18-june-2014
;This is the worst humanitarian disaster within living memory.
;यह जीवित स्मृति के भीतर वही सबसे अधिक खराब मानवीय आपदा है .
(defrule humanitarian100
(declare (salience 5500))
(id-root ?id humanitarian)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 disaster)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mAnavIya_ApaxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  humanitarian.clp     humanitarian100   "  ?id "  " ?id1 "  mAnavIya_ApaxA )" crlf))
)

(defrule humanitarian0
(declare (salience 5000))
(id-root ?id humanitarian)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnavikI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  humanitarian.clp 	humanitarian0   "  ?id "  mAnavikI )" crlf))
)

;"humanitarian","Adj","1.mAnavikI"
(defrule humanitarian1
(declare (salience 4900))
(id-root ?id humanitarian)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnavikI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  humanitarian.clp 	humanitarian1   "  ?id "  mAnavikI )" crlf))
)

;"humanitarian","N","1.mAnavikI"
;maxera weresA'Humanitarian' WI jo jIvanaparyanwa anAWa baccoM kI sevA kI.
;
