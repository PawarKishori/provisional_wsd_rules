;@@@ Added by Shirisha Manju --- Suggested by Sukhada (4-7-14)
;I will meet you at a quarter past five. 
;mEM savA pAzca baje Apase milUzgA.
(defrule at_quarter
(declare (salience 5000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ? ?id1)
(id-root ?id1 quarter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp       at_quarter   "  ?id "  - )" crlf))
)

;Removed by 14anu-ban-02(02-09-2014)
;Sentence added by Shirisha Manju Suggested by Chaitanya Sir
;So at end of the day, you have to make better choices, ladies.  (NPR_TellMore -COCA - freq:184)
;(defrule at0
;(declare (salience 5000))
;(id-root ?id at)
;?mng <-(meaning_to_be_decided ?id)
;(id-root =(+ ?id 1) end)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at0   "  ?id "  meM )" crlf))
;)



;$$$Modified by 14anu-ban-02(19-03-2015)
;She fluttered her eyelashes at him.[oald]
;उसने  अपनी बरौनियाँ  उसकी ओर झपकाईं .[self]
;Modified by Meena(18.5.11)
;When my name was called, he looked across at me. 
(defrule at1
(declare (salience 4900))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 look|flutter)	;flutter is added in the list by 14anu-ban-02(19-03-2015).
(kriyA-at_saMbanXI  ?id1  ?id2)
;(id-root  ?id2  me|us|you|her|him|them)	;commented by 14anu-ban-02(19-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at1   "  ?id "  kI_ora )" crlf))
)




(defrule at2
(declare (salience 4800))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) annoy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at2   "  ?id "  se )" crlf))
)

;Order is important. Do not change the order between above rule && the rule below.
(defrule at3
(declare (salience 4700))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) time)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at3   "  ?id "  para )" crlf))
)

;$$$ Modified by 14anu-ban-02(02-09-2014)
;rule 0,6,7 and 8 are merged.example is changed.
;At the time of evolution.[incorrect]
;At night you can see the stars.[oald]
;आप रात में तारो को देख सकते हैं.[manual]
(defrule at4
(declare (salience 5100))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) end|night|evening|morning|afternoon|activity|human|enclosed|place|topographic|point|spot|location|geographical|geographic|region|vicinity|locality|neighborhood|neck|area|country|office|home|residence|station|plaza|piazza|public|square|piece|parcel|blank|surface|expanse) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at4   "  ?id "  meM )" crlf))
)

(defrule at5
(declare (salience 4500))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) action|act|human action|human activity|activity|military action|group action|natural action|action mechanism|legal action|action at law)
(id-word =(+ ?id 1) action|act|activity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at5   "  ?id "  para )" crlf))
)

;Removed by 14anu-ban-02(03-09-2014)
;(defrule at6
;(declare (salience 4400))
;(id-root ?id at)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) activity|human activity)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at6   "  ?id "  meM )" crlf))
;)

;Removed by 14anu-ban-02(03-09-2014)
;(defrule at7
;(declare (salience 4300))
;(id-root ?id at)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) enclosed)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at7   "  ?id "  meM )" crlf))
;)

;Removed by 14anu-ban-02(03-09-2014)
;(defrule at8
;(declare (salience 4200))
;(id-root ?id at)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) place|topographic point|point|spot|location|geographical area|geographic area|geographical region|region|vicinity|locality|neighborhood|neck of the woods|area|country|office|home|residence|geographic point|geographical point|station|plaza|piazza|public square|square|piece of land|piece of ground|parcel of land|blank space|surface area|expanse)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at8   "  ?id "  meM )" crlf))
;)


;$$$Modified by 14anu-ban-02(26-02-2016)
;He is always looking at himself in the mirror .[sd_verified]
;वह हमेशा दर्पण में स्वयं को देखता रहता है . [self]
(defrule at9
(declare (salience 5000))	;salience increased to 5000 from 4100 by 14anu-ban-02(26-02-2016)
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
;(or (id-word =(+ ?id 1) time|time period|period of time|period|moment|minute|second|instant|point in time|clock time|hour)	;commented by 14anu-ban-02(26-02-2016)
;(id-root =(- ?id 1) look));Added by sheetal (2-01-10) ;He is always looking at himself in the mirror .		;commented by 14anu-ban-02(26-02-2016)
(kriyA-at_saMbanXI  ?id1 ?id2)	;Added by 14anu-ban-02(26-02-2016)
(id-root ?id1 look)	;Added by 14anu-ban-02(26-02-2016)
(id-root ?id2 time|period|moment|minute|second|instant|hour|himself)	;Added by 14anu-ban-02(26-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at9   "  ?id "  ko )" crlf))
)


;Added by Meena(1.4.11)
;We were bunched up at the back of the room. 
(defrule at_the_back
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id2)
(id-root ?id2 back)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp        at_the_back   "  ?id "  - )" crlf))
)

;commented by 14anu-ban-02(11-03-2015)
;correct meaning is coming from at14.
;$$$ Modified by 14anu-ban-01 on (27-10-2014)
;###[COUNTER STATEMENT]### He was standing at the bus station in my anticipation .[karan singla]
;###[COUNTER STATEMENT]### वह मेरी प्रतीक्षा में बस अड्डे पर खडा हुआ था . [self]
;### Restricted the rule: Let us first consider the simple case in which an object is stationary, e.g. a car standing still at x = 40 m.[NCERT corpus]
;### Restricted the rule: हम सर्वप्रथम एक सरल स्थिति पर विचार करेंगे, जिसमें वस्तु उदाहरणार्थ, एक कार x = 40 m पर स्थित है .[NCERT corpus]
;$$$ Modified by 14anu-ban-02(03-09-2014)
;###[COUNTER EXAMPLE]####The city is at an elevation of 2000 metres.[oald]
;###[COUNTER EXAMPLE]####शहर 2000 मीटरों के ऊँचाई में है .[self]
;###[COUNTER EXAMPLE]####The phase of the supply is adjusted so that when the positive ions arrive at the edge of D1, D2 is at a lower potential and the ions are accelerated across the gap.[ncert 12_04]
;स्रोत का कला का समायोजन इस प्रकार किया जाता है कि जब धनायन D1 के छोर पर पहुँचता है तो उस समय D2 निम्न विभव पर होता है तथा आयन इस रिक्त स्थान में त्वरित होते हैं.[ncert]
;###[COUNTER EXAMPLE]#### We are flying at an altitude of 6 000 metres.[oald]
;हम 6 000 मीटर कि ऊँचाई पर उड़ रहे हैं.[manual]
; added 'centre' in the list
;### COUNTER EXAMPLE #### Thus a nucleus in an atom is as small in size as the tip of a sharp pin placed at the center of a sphere of radius about a metre long.[ncert] 
;### COUNTER EXAMPLE ### अतः किसी परमाणु में नाभिक आमाप में उतना ही छोटा है जितनी छोटी लगभग 1m व्यास के गोले के केन्द्र पर रखे गए तीक्ष्ण पिन की नोक होती है.[ncert]
;###COUNTER EXAMPLE ###To find the sum A+B, we place vector B so that its tail is at the head of the vector A, as in Fig. 4.4(b).[ncert](29-11-2014)
;योग A+B प्राप्त करने के लिए चित्र 4.4(b) के अनुसार हम सदिश B इस प्रकार रखते हैं किउ सकी पुच्छ सदिश A के शीर्ष पर हो .[ncert]
;Modified by Meena(1.4.11);added "back" in the list; Ex: "We were bunched up at the back of the room."
;Modified by Meena(24.10.09)
;Guess who I saw at the party last night ! 
;$$$ Modified by 14anu21 on 26.06.2014 by adding relation (not(id-root ?id2 gate|door))
; While you do this I will keep watch at the gate. 
;जब कि आप यह करते हैं मैं द्वार में घडी रखूँगा .(Translation before adding rule watch4)
;जब कि आप यह करते हैं मैं द्वार पर नजर रखूँगा .
;Modified by 14anu21 on 07.07.2014 by adding bus_stop  and traffic_light to the list-(not(id-root ?id2 death|back|light|stop))
;###The car stopped at the traffic lights. [oxford]
;###गाडी ट्रैफिक लाईट में रुकी . [Translation before modification] 
;गाडी ट्रैफिक लाईट पर रुकी . 
;###The car stopped at the bus stop.[self]
;###गाडी बस अड्डे में रुकी .[Translation before modification] 
;गाडी बस अड्डे पर रुकी . 
;(defrule at10
;(declare (salience 4000))
;(id-root ?id at)
;?mng <-(meaning_to_be_decided ?id)
;(pada_info (group_head_id ?id2)(preposition ?id))    ;added by 14anu-ban-02(29-11-2014)
;(kriyA-at_saMbanXI  ?id1 ?id2)
;(not(id-root ?id2 death|back|centre|light|stop|gate|door|head|altitude|edge|potential|distance|elevation|station))  ;'head' is added by ;14anu-ban-02(29-11-2014)	;'altitude' is added by 14anu-ban-02(06-02-2015)	;'edge','potential' and 'distance' is added  by 14a;nu-ban-02(11-02-2015)	;'elevation' is added by 14anu-ban-02(20-02-2015)	;'station' is added by 14anu-ban-02(26-02-2015)
;(not(id-cat_coarse ?id2 symbol|PropN))   ;Added by 14anu-ban-01 on (27-10-2014)
;(id-word =(+ ?id 1) conference|meeting|discussion|party)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id meM))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at10   "  ?id "  meM )" crlf))
;)

;commented by 14anu-ban-02(11-03-2015)
;correct meaning is coming from at14
;@@@ Added by 14anu21 on 02.07.2014
;The boys were throwing sticks and stones at the dog. [oxford]
;लडके कुत्ते में लाठियाँ और पत्थर फेंक रहे थे . (Transaltion before adding rule;Rule at10 was getting fired)
;लडके कुत्ते पर लाठियाँ और पत्थर फेंक रहे थे . 

;(defrule at_living
;(declare (salience 4000))
;(id-root ?id at)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-at_saMbanXI  ?id1 ?id2)
;(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id para))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at_living   "  ?id "  para )" crlf))
;)
 

;commented by 14anu-ban-02(11-03-2015)
;correct meaning is coming from at14
;$$$ Modified by Shirisha Manju Suggested by Aditiji and somaji on 08-11-14 --- added 'suggestion' to the list  
;At the suggestion of Ernst Rutherford, in 1911, H. Geiger and E. Marsden performed some experiments. (ncert)
;san 1911 meM raxaraPorda ke suJAva para eca . gAigara waWA I. mArsadana ne kuCa prayoga kie .(ncert)
;Modified by Meena(26.7.11);added "point" in the list for the examples like ;At this point,the Dow was down about 35 points. 
;Modified by Meena(20.5.11)
;$$$ Modified by 14anu21 on 14.06.2014 by adding "top"  to the list.
;She was standing at the top of stairs.
;वह सीढियों की ऊपरी सिरा में खडी हुई थी . (Before modification)
;वह सीढियों की ऊपरी सिरे पर खडी हुई थी.
;The Dow Jones industrials closed at 2569.26. 
;Added by Meena(24.10.09)
; I yelled at her for going to the party .
;The fact that he smiled at me gives me hope .
;(defrule at11
;(declare (salience 4000))
;(id-root ?id at)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-at_saMbanXI  ?id1 ?id2)
;(or(id-root ?id2  point|me|us|you|her|him|them|suggestion|top)(id-cat_coarse ?id2 number))  
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id para))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp        at11   "  ?id "  para )" crlf))
;)




;Added by Meena(20.4.10)
;She wept bitterly at the news.
(defrule at12
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id2)
(id-root  ?id2   news)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp        at12   "  ?id "  suna_kara )" crlf))
)


;$$$Modified by 14anu-ban-02(31-01-2015)
;Breakfast is served at 8 on the dot.(oald)
;नाश्ता ठीक 8 बजे परोसा गया है.(manual)
;Added by Meena(27.5.11)
;The performance starts at seven. 
(defrule at13
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(kriyA-at_saMbanXI  ?id1 ?id2)
;(id-root  ?id2 one|two|three|four|five|six|seven|eight|nine|ten|eleven|twelve)	;commented by 14anu-ban-02(31-01-2015)
(id-cat_coarse ?id2 number)	;Added by 14anu-ban-02(31-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baje))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp        at13   "  ?id "  baje )" crlf))
)


;$$$ Modified by 14anu-ban-02(02-08-2014)
;There is a very relaxed atmosphere between staff and pupils at the school.  [cald]
;विद्यालय में कर्मचारियों और विद्यार्थियों के बीच एक अत्यन्त तनाव मुक्त वातावरण है . [self]
;A number of exercises at the end of this chapter will help you develop skill in dimensional analysis.[ncert]  ;added by 14anu-ban-02(11-11-2014)
;इस अध्याय के अन्त में दिए गए कई अभ्यास प्रश्न, आपकी विमीय विश्लेषण की कुशलता विकसित करने में सहायक होंगे.[ncert]  ;added by 14anu-ban-02(11-11-2014)
;Laboratory is added in the list.
;In India, this is available at the National Physical Laboratory (NPL), New Delhi. [ncert]
;अन्तर्राष्ट्रीय माप - तोल ब्यूरो द्वारा दिए गए अन्तर्राष्ट्रीय मानक किलोग्राम के आदिप्ररूप विभिन्न देशों की बहुत सी प्रयोगशालाओं में उपलब्ध हैं.[ncert]
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)3-Feb-2014
;She had a jolly time at the party.
;उसने पार्टी में एक मस्ती भरा समय बिताया
;Sentence added by Shirisha Manju Suggested by Chaitanya Sir
;This show, for the beer-drinkers at Party on the Plaza, will presumably focus on more adult-oriented material.  (COCA - Houston Chronicle)
(defrule at15
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-at_saMbanXI  ?id1 ?id2)
(id-root ?id2 conference|meeting|discussion|party|Laboratory|end|school)    ;'end' is added in the list by 14anu-ban-02(11-11-2014) ;'school' is added in the list by 14anu-ban-02(13-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp        at15   "  ?id "  meM )" crlf))
)

;$$$ Modified by 14anu-ban-02(10-01-2015)
;@@@ Added by 14anu01
;I did not sleep at all the night before.
;मैं पहले रात बिलकुल नहीं सोया था . 
(defrule at16
(declare (salience 6000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) all)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id bilakula));Commented by 14anu-ban-02(10-01-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) bilakula));Added by 14anu-ban-02(10-01-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at16   "  ?id "  bilakula )" crlf));Commented by 14anu-ban-02(10-01-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " at.clp	at16  "  ?id "  " (+ ?id 1) " bilakula   )" crlf));Added by 14anu-ban-02(10-01-2015)
)

;$$$ Modified by 14anu-ban-02(10-01-2015)
;She is very good at cracking jokes. [Hinkhoj]
;वह चुटकुले चटकाने में अत्यन्त माहिर है .  [manual]
;She is good at French.
;वह फ्रेन्च में अच्छी है.
;@@@ Added by 14anu09
;She is good at French.
;वह french में अच्छी है.
(defrule at17
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id)) ;added by 14anu-ban-02(10-01-2015)
(viSeRya-at_saMbanXI  ?id1 ?id2);changed '(viSeRya-at_saMbanXI  ?id2 ?id1)' into (viSeRya-at_saMbanXI  ?id1 ?id2) by 14anu-ban-02(10-01-2015) 
(id-root ?id1 good|bad|great|exception|pathetic)
;(id-root ?id2 French|English|German|Latin|Russian|Spanish|Telugu|Tamil|Kanada|Bangla|Punjabi);id-word changed into id-root by 14anu-ban-02(10-01-2015)	;commented by 14anu-ban-02(11-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp        at17   "  ?id "  meM )" crlf))
)


;@@@Added by 14anu20 on 17/06/2014
;He is good at recognizing wild flowers.
;वह जङ्गली फूल पहचानने में बहुत अच्छा है . 
(defrule at18
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-at_saMbanXI  ?id1 ?id2)
(id-root ?id2 recognize|understand|perform|grasp|make|teach|read)
(id-cat_coarse ?id2 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp        at18   "  ?id "  meM )" crlf))
)

;@@@Added by 14anu-ban-02(11-02-2015)
;1 Law of orbits: All planets move in elliptical orbits with the Sun situated at one of the foci of the ellipse (Fig. 8.1a).[ncert 11_08]
;1 कक्षाओं का नियम: सभी ग्रह दीर्घवृत्तीय कक्षाओं में गति करते हैं तथा सूर्य इसकी, एक नाभि पर स्थित होता है (चित्र 8.1a).[ncert]
(defrule at19
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-at_saMbanXI  ?id1 ?id2)
(id-root ?id1 situate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at19   "  ?id "  para )" crlf))
)

;@@@Added by 14anu-ban-02(11-03-2015)
;Guess who I saw at the party last night ! [at10]
;अन्दाज लगाइए कि मैंने कल रात पार्टी में किसे देखा! 
;The runners set off at a blistering pace.[oald]
;धावक बहुत तेज  रफ्तार में दौड़े . [self]
(defrule at20
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-at_saMbanXI  ?id1 ?id2)
(id-root ?id2 party|pace|forum|school)	;pace is added in the list by 14anu-ban-02(31-03-2015)	;'forum' is added by 14anu-ban-02(06-02-2016)	;school is added in the list by 14anu-ban-02(15-03-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at20   "  ?id "  meM )" crlf))
)

;@@@Added by 14anu-ban-02(14-03-2015)
;At the dawn of a new millennium, a fresh set of theories of child development has blossomed.[report set4]
;नये सहस्त्राब्द के प्रारम्भ में, बाल विकास के सिद्धान्तों का नया सेट विकसित किया है.[self]
(defrule at21
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-at_saMbanXI  ?id1 ?id2)
(viSeRya-of_saMbanXI  ?id2 ?id3)
(id-root ?id3 millennium)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at21   "  ?id "  meM )" crlf))
)

;@@@ Added by 14anu-ban-05 on (07-09-2015)
;However, neither the total population of India nor the rate of population growth at this stage was very high . [social science]
;हालाँकि, ना तो भारत की कुल जनसङ्ख्या ना ही इस समय तक जनसङ्ख्या वृद्धि की दर बहुत ज्यादा था . [manual]

(defrule at22
(declare (salience 4901))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(viSeRya-at_saMbanXI  ? ?id2)
(id-root ?id2 stage)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at22   "  ?id "  waka )" crlf))
) 


;@@@Added by 14anu-ban-02(26-02-2016)
;He lives at Chandinichok in Delhi in India.[sd_verified] 	;added by 14anu-ban-02(27-02-2016)
;vaha BArawa meM xillI meM chandinichok meM rahawA hE.[sd_verified] 	;added by 14anu-ban-02(27-02-2016)
;He was awakened at dawn by the sound of crying .[sd_verified]
;vaha rone kI AvAja se uRAkAla meM jagAyA gayA WA.[sd_verified]
(defrule at23
(declare (salience 4000))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-at_saMbanXI  ?id1 ?id2)
(id-root ?id1 awaken|live)	;live is added by 14anu-ban-02(27-02-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at23   "  ?id "  meM )" crlf))
)
 

;***********************DEFAULT RULE*************************************

(defrule at14
(declare (salience 0))
;(declare (salience 3900))
(id-root ?id at)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  at.clp 	at14   "  ?id "  para )" crlf))
)

;"at","Prep","1.para"
;He was waiting for her at the platform.
;--"2.meM"
;She works at a school for the handicapped.
;He joined the army at twenty-two.
;--"3.ke_yahAz"
;She stayed at her parents' for the whole vacation.
;--"4.ko{samaya}"
;At night the sky was totally clear.
;--"5.kI_ora"
;Don't throw stones at others.
;
