;@@@ Added by 14anu20 on 23/06/2014.
(defrule make23_1
(declare (salience 6000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)    
(kriyA-preraka_kriyA  ?id1 ?id)
(id-cat_coarse ?id1 verb)
(id-cat_coarse ?id verb)
(id-root ?id1 feel)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mahasUsa_karA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp  make23_1  "  ?id "  " ?id1 "  mahasUsa_kara  )" crlf))
)

;Added by Meena(27.1.11)
;She gave the cap another twist to make sure it was tight. 
(defrule make_sure0
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) sure)
(id-cat_coarse ?id verb) ;$$$ Added this fact by Nandini 18-12-13
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) niSciwa_kara))
;(assert (id-wsd_root_mng ?id saMvexanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp  make_sure0  "  ?id "  " (+ ?id 1) "  niSciwa_kara  )" crlf))
)


;Added by Meena(16.10.10)
;It makes sense that the charge approaches zero, since the balloon is losing its charge.
(defrule make_sense0
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) sense)
(id-cat_coarse ?id verb) ;$$$ Added this fact by Nandini 18-12-13
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) sahI))
;(assert (id-wsd_root_mng ?id saMvexanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp  make_sense0  "  ?id "  " (+ ?id 1) "  sahI  )" crlf))
)




;Added by Meena(16.10.10)
;The guard was sitting in a cabin made for him. 
(defrule make00
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb) ;$$$ Added this fact by Nandini 18-12-13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bana))
;(assert (id-wsd_word_mng ?id bane_hue))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  make.clp     make00   "  ?id "  bana)" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  make.clp     make00   "  ?id "  bane_hue)" crlf)
)
)


;Modified by sheetal(3-9-10)
;Added by Meena(25.8.09)
;I made him make some changes in the programme.
(defrule causative_make
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-prayojya_karwA  ?id ?id2)
(id-cat_coarse ?id verb) ;$$$ Added this fact by Nandini 18-12-13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  make.clp     causative_make   "  ?id "  kara)" crlf)
)
)



(defrule make-speedy-progress
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) speedy)
(id-word =(+ ?id 2) progress)
(id-cat_coarse ?id verb) ;$$$ Added this fact by Nandini 18-12-13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  make.clp     make-speedy-progress   "  ?id "  kara)" crlf))
)
;Please do accept the same and bless the Anusaaraka project to make speedy progress 

(defrule make_kriyA_mUla_clear
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_mUla ?id ?id1)
(id-root ?id1 clear)
(id-cat_coarse ?id verb) ;$$$ Added this fact by Nandini 18-12-13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  make.clp     make_kriyA_mUla_clear   "  ?id "  kara)" crlf))
)

(defrule make0
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id making )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id banAne_kI_prakriyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  make.clp  	make0   "  ?id "  banAne_kI_prakriyA )" crlf))
)

;"making","N","1.banAne_kI_prakriyA"
;The film beautifully depicts the making of a great man.
;--"2.guNa"
;He has the making of a successful  lawyer.
;
;

;$$$Modified by 14anu-ban-08 (26-03-2015)    ;commeneted relation, added relation, meaning changed
;He suddenly got up && made for the exit.  [same clp file]
;वह अचानक उठा और बाहर की ओर जाने लगा. [same clp file]
(defrule make1
(declare (salience 5001))  ;salience increased by 14anu-ban-08 (26-03-2015)
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 for)             ;commented by 14anu-ban-08 (26-03-2015)
;(kriyA-upasarga ?id ?id1)      ;commented by 14anu-ban-08 (26-03-2015)
;(kriyA-object ?id ?)          ;commented by 14anu-ban-08 (26-03-2015)
(kriyA-for_saMbanXI ?id ?id1)    ;added by 14anu-ban-08 (26-03-2015)
(id-root ?id1 exit)                ;added by 14anu-ban-08 (26-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kI_ora_jA))  ;commented by 14anu-ban-08 (26-03-2015)
(assert (id-wsd_word_mng ?id kI_ora_jAnA))   ;changed meaning from 'kI_ora_jA' to 'kI_ora_jAnA' by 14anu-ban-08 (26-03-2015)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make1  "  ?id "  " ?id1 "  kI_ora_jA  )" crlf))  ;commented by 14anu-ban-08 (26-03-2015)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  make.clp  	make1   "  ?id "  kI_ora_jAnA )" crlf))   ;changed meaning from 'kI_ora_jA' to 'kI_ora_jAnA' by 14anu-ban-08 (26-03-2015)
)

;He suddenly got up && made for the exit.
;
(defrule make2
(declare (salience 4800))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BAga_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make2  "  ?id "  " ?id1 "  BAga_jA  )" crlf))
)

;The students made off as soon as the teacher arrived.
;
(defrule make3
(declare (salience 4700))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hisAba_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make3  "  ?id "  " ?id1 "  hisAba_lagA  )" crlf))
)

;Please make out the budget of this year.
;
(defrule make4
(declare (salience 4600))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saPala_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make4  "  ?id "  " ?id1 "  saPala_ho  )" crlf))
)

;Don't worry-i'll make out all right.
;
(defrule make5
(declare (salience 4500))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BAzpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make5  "  ?id "  " ?id1 "  BAzpa  )" crlf))
)

;@@@ Added by Nandini (2-11-13)
;When you look at a tree in the distance you cannot make out its individual leaves.[send by suchita shukla]
;जब आप दूर के किसी पेड़ को देखते हैं, तो आपको उसके अलग-अलग पत्ते दिखाई नहीं देते।
(defrule make5-a
(declare (salience 4750))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
(id-word ?id2 leaves)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make5-a  "  ?id "  " ?id1 "  xeKa  )" crlf))
)

;Can you make out how they did it?
;
(defrule make6
(declare (salience 4400))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 haswAMwaraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make6  "  ?id "  " ?id1 "  haswAMwaraNa  )" crlf))
)

;Make the cheque over to her.
;
(defrule make7
(declare (salience 4300))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kRawipUrwI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make7  "  ?id "  " ?id1 "  kRawipUrwI_kara  )" crlf))
)

;Nobody can make up for his husband's death.
;
(defrule make8
(declare (salience 4200))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 miwrawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make8  "  ?id "  " ?id1 "  miwrawA_kara  )" crlf))
)

;She has done nothing for me but i don't know why i always want to make it up to her.
;
(defrule make9
(declare (salience 4100))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ikatTA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make9  "  ?id "  " ?id1 "  ikatTA_kara  )" crlf))
)

;Have you made whole the money up for charity?
;
(defrule make10
(declare (salience 4000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saMpUrNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make10  "  ?id "  " ?id1 "  saMpUrNa_kara  )" crlf))
)

;If i'm late for work,i'll just make it up tomorrow.
;
(defrule make11
(declare (salience 3900))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sajjA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make11  "  ?id "  " ?id1 "  sajjA_kara  )" crlf))
)

;I forgot to take my make-up off before i went to bed.
;
(defrule make12
(declare (salience 3800))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kalpiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make12  "  ?id "  " ?id1 "  kalpiwa_kara  )" crlf))
)

;Can't you make up a story for me?
;
(defrule make13
(declare (salience 3700))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 fun)
(kriyA-fun_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hazsI_udZA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " make.clp make13 " ?id "  hazsI_udZA )" crlf)) 
)

(defrule make14
(declare (salience 3600))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 fun)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hazsI_udZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make14  "  ?id "  " ?id1 "  hazsI_udZA  )" crlf))
)

(defrule make15
(declare (salience 3500))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 it)
(kriyA-it_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " make.clp make15 " ?id "  pahuzca )" crlf)) 
)

(defrule make16
(declare (salience 3400))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 it)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make16  "  ?id "  " ?id1 "  pahuzca  )" crlf))
)

(defrule make17
(declare (salience 3300))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 possible)
(kriyA-possible_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samBava_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " make.clp make17 " ?id "  samBava_kara )" crlf)) 
)

(defrule make18
(declare (salience 3200))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 possible)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samBava_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make18  "  ?id "  " ?id1 "  samBava_kara  )" crlf))
)

(defrule make19
(declare (salience 3100))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 room)
(kriyA-room_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAna_xe));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " make.clp make19 " ?id "  sWAna_xe )" crlf)) 
)

(defrule make20
(declare (salience 3000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 room)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 sWAna_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make20  "  ?id "  " ?id1 "  sWAna_xe  )" crlf))
)


(defrule make24
(declare (salience 2600))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 impact)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make24   "  ?id "  dAla )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (21-10-2014)
;The early civilisations of Egypt, India, China, Greece, Mesopotamia and many others made vital contributions to its progress.  [NCERT CORPUS]
;misra, BArawa, cIna, yUnAna, mEsopotAmiyA waWA saMsAra ke anya xeSoM kI prAcIna saByawAoM ne vijFAna kI pragawi meM awyAvaSyaka yogaxAna xiyA hE. [NCERT CORPUS]
;Modified by Meena(11.01.10)
;The company made him a tempting offer of a high salary . 
(defrule make26
(declare (salience 2400))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 offer|contribution) ;added 'contribution' by 14anu-ban-09 (21-10-2014)
;(kriyA-object ?id1 ?id)  ;as ?id1 is not a verb here(Meena(11.01.10))
(or(kriyA-object ?id ?id1)(kriyA-object_2 ?id ?id1))
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make26   "  ?id "  xe )" crlf))
)

(defrule make39
(declare (salience 1100))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 money)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make39   "  ?id "  kamA )" crlf))
)

(defrule make40
(declare (salience 1000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 profit)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make40   "  ?id "  kamA )" crlf))
)

(defrule make41
(declare (salience 900))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 fortune)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make41   "  ?id "  kamA )" crlf))
)


(defrule make43
(declare (salience 700))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 bed)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make43   "  ?id "  bAnA )" crlf))
)

(defrule make44
(declare (salience 600))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 fire)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make44   "  ?id "  bAnA )" crlf))
)

(defrule make47
(declare (salience 300))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-off_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAga_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " make.clp make47 " ?id "  BAga_jA )" crlf)) 
)

(defrule make48
(declare (salience 200))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BAga_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make48  "  ?id "  " ?id1 "  BAga_jA  )" crlf))
)

(defrule make49
(declare (salience 100))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anumAna_lagA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " make.clp make49 " ?id "  anumAna_lagA )" crlf)) 
)

(defrule make50
(declare (salience 0))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 anumAna_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make50  "  ?id "  " ?id1 "  anumAna_lagA  )" crlf))
)

(defrule make51
(declare (salience -100))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 mind)
(viSeRya-up_saMbanXI ?id1 ?id) ;Replaced viSeRya-up_viSeRaNa as viSeRya-up_saMbanXI programatically by Roja 09-11-13
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 niScaya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make51  "  ?id "  " ?id1 "  niScaya_kara  )" crlf))
)

; He made up his mind to work on this project.
(defrule make52
(declare (salience -200))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 story)
(viSeRya-up_saMbanXI ?id1 ?id) ;Replaced viSeRya-up_viSeRaNa as viSeRya-up_saMbanXI programatically by Roja 09-11-13
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make52   "  ?id "  banA_le )" crlf))
)

; She is very good at making up stories.
(defrule make53
(declare (salience -300))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 decision)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make53   "  ?id "  le )" crlf))
)

(defrule make54
(declare (salience -400))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " make.clp make54 " ?id "  banA )" crlf)) 
)

(defrule make55
(declare (salience -500))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make55  "  ?id "  " ?id1 "  banA  )" crlf))
)

;@@@ Added by 14anu07 Karishma Singh, MNNIT Allahabad on 2/07/2014
;He made no bones about this fact.
;उसने इस तथ्य के बारे में नहीं छिपाया.
(defrule make70
(declare (salience 4000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) no)
(id-root =(+ ?id 2) bone)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) (+ ?id 2) nahIM_CipA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp    make70  "  ?id "  " (+ ?id 1) " " (+ ?id 2) "  nahIM_CipA)" crlf))
)


;$$$Modified by 14anu-ban-08 (13-03-2015)  ;constraint added, meaning changed
;@@@ Added by Nandini(14-12-13)
;As they drew near the bungalow they could make out that something important had happened.
;जैसे वह बङ्गला के पास गया उसने भाप लिया कि कुछ महत्त्वपूर्ण काम हुआ था. [self]   ;added by 14anu-ban-08 (26-03-2015)
;जैसे वह बंगलो के पास आए उसने भांप लिया कि कुछ अत्यावश्यक हो सकता हैं. [self]  ;added by 14anu-ban-08 (13-03-2015)
(defrule make_out
(declare (salience 4701))   ;salience increased by 14anu-ban-08 (13-03-2015)
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(id-root ?id2 happen)        ;added by 14anu-ban-08 (13-03-2015)
(kriyA-upasarga ?id ?id1)
(kriyA-vAkyakarma  ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BAzpa_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp	make_out  "  ?id "  " ?id1 "  BAzpa_le  )" crlf))
)


;He made negative comments to the press .
;usane #prEsa ko nakArAwmaka tippaNiyAz xIM
;Added by sheetal(29-09-09).
(defrule make59
(declare (salience 4950))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 comment)
(kriyA-object ?id ?id1)
(kriyA-to_saMbanXI ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make59   "  ?id "  xe )" crlf))
)


;$$$Modified by 14anu-ban-08 (04-03-2015)     ;added 'claims' constraint ,Run on parser 2
;The government has made a gesture towards public opinion.         [OALD]
;सरकार ने जनता की राय की ओर एक संकेत किया है.            [MANUAL]
;I tried to make commune.  [oald]
;मैंने बातचीत करने का प्रयास किया . [manual]
;The family made discreet enquiries about his background. [cald]
;परिवार ने  उसकी पृष्ठभूमि के बारे में  पृथक पूछताछ की।         [self]
;The victim of the accident made claims for damages.   [hinkhoj]
;दुर्घटना के शिकार ने क्षति के लिए मुआवजे की माङ्ग की.   [self]
;$$$ Modiifed by 14anu19
;$$$ Added ("remark" in the list.) by Nandini.
;Added "mention" in the list(Meena 27.4.11)
;The book does not make any mention of his love affair. 
;Added by sheetal (04-02-10). 
; after removing rules- [21,22,23,25,27,28,29,30,31,32,33,34,35,36,37,38,42,45,46,56]
(defrule make60
(declare (salience 4950))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-word ?id1 mention|attempt|effort|excuse|suggestion|decision|exception|enquiry|call|mistake|request|fuss|noise|arrangement|journey|statement|love|war|peace|comment|observation|inspection|remark|preparations|arrangement|hole|claims|enquiries|commune|gesture) ; list Added by Manju (04-02-10).;preparations|arrangement|hole are added in list(by 14anu19)  ;added 'claims' by 14anu-ban-08 (26-02-2015)  ;added 'enquiries' by 14anu-ban-08 (18-03-2015)  ;added 'commune' by 14anu-ban-08 (24-03-2015)   ;added 'gesture' by 14anu-ban-08 (08-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make60   "  ?id "  kara )" crlf))
)

;The Inspector made an accidental inspection of the school .
;She was asked about the pay increase but made no comment .
;She made an request.
;She made an observation.
;He made a request that there be an investigation.
;He made a mistake in inviting John.

;@@@ Added by Nandini (11-11-13)
;She always makes me laugh. 
;vaha hameSA muJe hazsAwI hE.
(defrule make061
(declare (salience 4850))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 laugh|shout|cry)
(kriyA-preraka_kriyA  ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make061   "  ?id "  - )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (21-10-2014)
;From the sixteenth century onwards, great strides were made in science in Europe.  [NCERT CORPUS]
;solahavIM SawAbxI se yUropa meM vijFAna ke kRewra meM awyaXika pragawi huI. [NCERT CORPUS]
;@@@ Added by Nandini (18-11-13)
;The report reveals that the company made a loss of £ 20 million last year.
;riporta bawAwI hE ki kampanI ko piCale varRa 20 miliyana pOMda  kA GAtA huA.
(defrule make61
(declare (salience 4870))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 loss|stride) ;Added 'stride' and changed 'id-word' to 'id-root' by 14anu-ban-09 on (21-10-2014)
(or(kriyA-object  ?id ?id1)(kriyA-subject  ?id ?id1)) ;added '(kriyA-subject  ?id ?id1)' by 14anu-ban-09 on (21-10-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make61   "  ?id " ho)" crlf))
)

;@@@ Added by Nandini (2-1-14)
;So the King  had to make arrangements for her departure.
;We need a drill to make holes in wood. [via mail] Added by Nandini (18-1-14)
;hameM lakadI meM Cexa karane ke liye Cexa_karane_vAlI maSIna kI jarUrawa howI hE.
;isalie rAjA ko usakI ravAnagI ke liye prabanXa karane pade.
;added hole in the list (18-01-14) by Nandini 
(defrule make62
(declare (salience 5050))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 arrangement|hole) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make62   "  ?id " kara)" crlf))
)

;$$$Modified by 14anu-ban-08 (24-04-2015)       ;added constraint
;She made a dart for the door.         [oald]
;उसने दरवाजे की ओर दौड़ लगाई .                [self]
;$$$ Modified by 14anu21 on 19.06.2014 by addind guess to the list.
;He made a guess.
;उसने अनुमान लगाया .
;उसने अनुमान बनाया. (Translation before modification)
;@@@ Added by Nandini(2-1-14)
;I made a dash for the bathroom
(defrule make63
(declare (salience 6000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 dash|guess|dart)        ;added 'dart' by 14anu-ban-08 (24-04-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make63   "  ?id " lagA)" crlf))
)

;@@@ Added by Nandini(2-1-14)
;They tickle my throat and make me cough.
(defrule make64
(declare (salience 6050))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kqxanwa_karma  ?id ?id1)
(id-word ?id1 cough)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id majabUra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make64   "  ?id " majabUra_kara)" crlf))
)

;@@@ Added by Nandini(6-1-14)
;He made strong critical remark on Salman's new film.
;usane salman ke naye sineme para kaDZI samIkRAwmaka tippaNI kI.
(defrule make65
(declare (salience 6050))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 remark)
(viSeRya-viSeRaNa  ?id1 ?id2)
(id-root ?id2 critical)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make65   "  ?id "  kara )" crlf))
)

;Help Suggested by Chaitanya Sir
;-- make <something> public
; make public either object_1 or object_2 then -> mng -> sArvajanika
; He gave me a book. (This type construction)
; public ko sampradana laba hE
;


;@@@ Added by Sukhada(2-4-14)
;If I make a promise, I like to keep it. 
;yaxi mEM vAxA karawA hUz, wo mEM use niBAnA pasanxa karawA hUz.
(defrule make_promise
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 promise)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make_promise   "  ?id "  kara )" crlf))
)


;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_make65
(declare (salience 6050))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 remark)
(subject-subject_samAnAXikaraNa ?id1 ?id2)
(id-root ?id2 critical)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   sub_samA_make65   "   ?id " kara )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
;$$$ Modified by 14anu19(26-06-2014)
;The news made him very happy.
;समाचार ने उसको अत्यन्त खुश किया .
(defrule obj_samA_make65
(declare (salience 6050))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 remark|him|her|them) ;him,her,them are added in list  (by 14anu19)
(object-object_samAnAXikaraNa ?id1 ?id2)
(id-root ?id2 critical|happy|sad|shock) ;happy,sad,shock are added in list(by 14anu19)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   obj_samA_make65   "   ?id " kara )" crlf))
)

;$$$ Modified by 14anu-ban-08 (11-12-2014)        ;meaning changed from 'kiyA' to 'kara'
;@@@ Added by 14anu24
;India has made a lot of progress in agriculture since independence in terms of growth in output , yields and area under many crops .
;उत्पाद , पैदावार , और विभिन्न फसलों के लिए निर्धारित भूमि की वृद्धि की दृष्टि से भारत ने स्वतंत्रता के बाद से कृषि में काफी प्रगति  किया है.
(defrule make66
(declare (salience 6500))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id1 lot)
(id-root ?id2 progress)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))             ;changed meaning from 'kiyA' to 'kara' by 14anu-ban-08 (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp     make66   "  ?id "  kara )" crlf))                               ;changed meaning from 'kiyA' to 'kara' by 14anu-ban-08 (11-12-2014)
)


;$$$ Modified by 14anu-ban-08 (11-12-2014)      ;changed meaning from 'bawAao' to root meaning 'bawA'
;@@@ Added by 14anu17
;If You are Dissatisfied with Your NHS Services If you have any concerns about your NHS services , please make them know to the people who provide those services.
(defrule make67
(declare (salience 6052))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_subject ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bawA))       ;changed meaning from 'bawAao' to root meaning 'bawA' by 14anu-ban-08 (11-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   make67   "   ?id " bawA )" crlf))             ;changed meaning from 'bawAao' to root meaning 'bawA' by 14anu-ban-08 (11-12-2014)
)

;@@@Added by 14anu-ban-08 (04-02-2015)
;There were impressions around her ankles made by the tops of her socks. [Cambridge]
;उसके टखने के इधर उधर निशान उसके मोजों के ऊपरी सिरे से बन गए थे .   [Self]
(defrule make68
(declare (salience 6059))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(viSeRya-around_saMbanXI ?id2 ?id1)
(id-root ?id2 impression)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bana_jA))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   make68   "   ?id " bana_jA )" crlf))             
)

;@@@Added by 14anu-ban-08 (23-02-2015)
;To make this analogy more firm we calculate the axial field of a finite solenoid depicted in Fig. 5.4 (a).   [NCERT]
;इस साम्यता को और अधिक सुदृढ करने के लिए हम चित्र 5.4 (a) में दर्शायी गई सीमित परिनालिका के अक्षीय क्षेत्र की गणना करते हैं.  [NCERT]
;इस साम्यता को समझने में और अधिक सुदृढ करने के लिए हम चित्र 5.4 (a) में दर्शायी गई सीमित परिनालिका के अक्षीय क्षेत्र की गणना करते हैं.  [self]
(defrule make69
(declare (salience 6059))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 analogy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(assert  (id-wsd_viBakwi   ?id meM))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   make69   "   ?id " samaJa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* " make.clp 	make69 "  ?id "  meM )" crlf))             
)

;@@@Added by 14anu-ban-08 (04-03-2015)
;Authority to make financial decisions has been delegated to a special committee.   [cald]
;आर्थिक निर्णय लेने के लिए  एक विशेष समिति को अधिकार सौंपा गया है .    [self]
(defrule make71
(declare (salience 6000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 decision)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   make71   "   ?id " le )" crlf))             
)

;@@@ Added by 14anu-ban-08 (07-03-2015)
;The kids made a mess in the bathroom.  [OLAD]
;baccoM ne snAnaGara meM gaMxagI PElAI.  [self]
(defrule make72
(declare (salience 6000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 mess)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PElA))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   make72   "   ?id " PElA )" crlf))             
)

;@@@Added by 14anu-ban-08 (13-03-2015)     ;Run on parser 2
;Making things happen is an art. [Set7-76-77pdf]
;चीजों को करना एक कला हैं. [self]
(defrule make73
(declare (salience 6053))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1) 
(id-root ?id1 thing)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kara))
(assert (kriyA_id-object_viBakwi ?id ko))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   make73   "   ?id " kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  make.clp 	make73     "  ?id " ko )" crlf))             
)

;@@@Added by 14anu-ban-08 (13-03-2015)
;Chandrababu naidu made a oath that he will build a new capital city.  [Anusaaraka - Agama Team Observations]
;चंद्रबाबू नाइडु ने शपथ ली कि वह एक बड़े शहर का निर्माण करेगें.  [self]
(defrule make74
(declare (salience 6000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 oath)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lI))       
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " make.clp   make74   "   ?id " lI )" crlf))             
)

;@@@ Added by 14anu-ban-08 (31-03-2015)
;Can you document the claims you're making?                  [oald]
;क्या आप उन दावों का लिखित प्रमाण दे सकते हैं जिन्हें आप  लगा रहे हैं?          [self]  
(defrule make75
(declare (salience 6000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id ?id1)
(id-root ?id1 you)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp      make75  "  ?id " lagA)" crlf))
)

;@@@Added by 14anu-ban-08 (07-04-2015)
;Each household must make daily offerings to the gods. [oald]
;हर एक परिवार को देवताओं को रोज चढावे चढाने चाहिए .  [self]
(defrule make76
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(kriyA-object ?id ?id1)
(kriyA-to_saMbanXI ?id ?id2)
(id-root ?id1 offering)
(id-root ?id2 god)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id caDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  make.clp     make76   "  ?id "  caDA)" crlf)
)
)

;@@@ Added by Sukhada(12-09-2015)
;Make the most of the good weather because rain is forecast for tomorrow. There'll be a lot of travelling involved in my new job and I plan to make the most of it. She planned to make the most of her trip to Europe. The class quickly made the most of the teacher's absence.
(defrule make_the_most_of
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) the)
(id-root =(+ ?id 2) most)
(id-root =(+ ?id 3) of)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2)   pUrA_lABa_uTA))
;(assert (id-wsd_root_mng ?id saMvexanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " make.clp  make_the_most_of  "  ?id "  " (+ ?id 1) "  " (+ ?id 2)  "  pUrA_lABa_uTA  )" crlf))
)

;@@@Added by 14anu-ban-02(08-02-2016)
;I have an important announcement to make.[oald]
;मुझे एक महत्वपूर्ण घोषणा करनी है[self]]
(defrule make77
(declare (salience 5000))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id1 ?id)
(id-root ?id1 announcement)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  make.clp     make77   "  ?id "  kara)" crlf)
)
)

;------------------------ Default Rules ----------------------

(defrule make57
(declare (salience -700))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make57   "  ?id "  banA )" crlf))
)

;"make","N","1.gaTana"
;The machinery is of an excellent make.
(defrule make58
(declare (salience -800))
(id-root ?id make)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaTana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  make.clp 	make58   "  ?id "  gaTana )" crlf))
)

