;meaning changed from 'vixveRa' to 'xveRa' by 14anu-ban-02(28-02-2015)
;@@@Added by 14anu-ban-02(27-02-2015)
;Sentence: Despite the deep antipathies between them, the two sides have managed to negotiate an agreement.[cambridge]
;Translation: उनके बीच गहरे विद्वेष के बावजूद, दोनों पक्षों समझौता हस्तांतरित करना सफल हो चुकीं हैं . [self]
(defrule antipathy0 
(declare (salience 0)) 
(id-root ?id antipathy) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id xveRa))	;meaning changed from 'vixveRa' to 'xveRa' by 14anu-ban-02(28-02-2015)
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  antipathy.clp  antipathy0  "  ?id "  xveRa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(27-02-2015)
;Declarations of racial antipathy against ethnic minorities will not be tolerated.[cambridge]
;अल्पसंख्यक  प्रजातीय के विरुद्ध  प्रजातीय विरोध की घोषणाएँ नहीं सही जायेंगी.[self]
(defrule antipathy1 
(declare (salience 100)) 
(id-root ?id antipathy) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 declaration|level)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id viroXa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  antipathy.clp  antipathy1  "  ?id "  viroXa )" crlf)) 
) 

