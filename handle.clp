
;@@@ Added by 14anu04 on 3-July-2014
;The handle of the door was loose. 
;दरवाजे का हत्था ढीला था. 
(defrule handle_tmp
(declare (salience 5000))
(id-root ?id handle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 door|window|drawer|cabin|jug|knife|bag)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id hawWA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  handle.clp  	handle_tmp   "  ?id "  hawWA )" crlf))
)

(defrule handle0
(declare (salience 5000))
(id-root ?id handle)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id handling )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prabanXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  handle.clp  	handle0   "  ?id "  prabanXa )" crlf))
)

;"handling","N","1.prabanXa"
;Parents handling of the case was not proper.
;
(defrule handle1
(declare (salience 4900))
(id-root ?id handle)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id handled )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id hawWA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  handle.clp  	handle1   "  ?id "  hawWA_lagA )" crlf))
)

;"handled","Adj","1.hawWA lagA"
;yaha cAkU hAWIxAzwa"handled"wAlA hE. 
;
;
;@@@ Added by 14anu-ban-06 (16-10-2014)
;Classical physics is inadequate to handle this domain and Quantum Theory is currently accepted as the proper framework for explaining microscopic phenomena.(NCERT)
;चिरसम्मत भौतिकी इस प्रभाव क्षेत्र से व्यवहार करने में सक्षम नहीं है तथा हाल ही में क्वान्टम सिद्धान्त को ही सूक्ष्म परिघटनाओं की व्याख्या करने के लिए उचित ढाञ्चा माना गया है.(NCERT)
(defrule handle3
(declare (salience 4850))
(id-root ?id handle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 domain)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavahAra_kara))
(assert  (id-wsd_viBakwi   ?id1  se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handle.clp 	handle3   "  ?id "  vyavahAra_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  handle.clp      handle3   "  ?id1 " se )" crlf))
)


(defrule handle2
(declare (salience 4800))
(id-root ?id handle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samBAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handle.clp 	handle2   "  ?id "  samBAla )" crlf))
)

;@@@Added by Gourav Sahni (MNNIT ALLAHABAD) on 25.06.2014 email-id:sahni.gourav0123@gmail.com
;She turned the handle and opened the door.
;उसने हैन्डल मोडा और दरवाजा खोला . 
(defrule handle4
(declare (salience 4700))
(id-root ?id handle)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ? ?id)
(viSeRya-det_viSeRaNa ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hEndala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  handle.clp 	handle4   "  ?id "  hEndala )" crlf))
)

;default_sense && category=verb	hAWa se CU	0
;"handle","V","1.hAWa se CUnA"
;dAinAmAida eka KawaranAka visPotaka hE jise sAvaXAnI se Handle karanA hE 
;--"2.prabanXa karanA"
;vakIla ne apane muvakkila kA kesa cawurAI se"handle"kiyA. 
;
;
