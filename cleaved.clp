;NOTE- here is a root problem in word cleave

;@@@ Added by 14anu-ban-03 (25-02-2015)
;His parents are still cleaved to old ways of living. [oald]
;उसके माँ बाप आजीविका के पुराने मार्गों में अभी भी चिपके हुए रहना चाहते हैं . [manual]
(defrule cleaved1
(declare (salience 10))
(id-word ?id cleaved)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
;(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) ;uncommented when the entry is resolve in human.gdbm
(id-root ?id1 parent) 
(kriyA-to_saMbanXI ?id ?id2)
(id-root ?id2 way)
(viSeRya-of_saMbanXI ?id2 ?id3)
(id-root ?id3 living)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cipake_hue_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cleaved.clp 	cleaved1   "  ?id "  cipake_hue_raha )" crlf))
)


;@@@ Added by 14anu-ban-03 (25-02-2015)
;The huge boat cleaved the darkness. [oald]
;विशाल नाव अंधकार को चीरते हुए निकल गई. [manual]
(defrule cleaved2
(declare (salience 20))
(id-word ?id cleaved)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 darkness)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cIrawe_hue_nikala_jA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng    " ?*prov_dir* "  cleaved.clp 	cleaved2  "  ?id "  cIrawe_hue_nikala_jA )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-03 (25-02-2015)
;The carpenter cleaved the wood into two pieces. [hinkhoj]
;बढई ने लकडी को दो टुकड़ो में काट दिया .  [manual]
(defrule cleaved0
(declare (salience 00))
(id-word ?id cleaved)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAta_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cleaved.clp  cleaved0   "  ?id "  kAta_xe )" crlf))
)

