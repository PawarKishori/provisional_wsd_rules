;@@@ Added by 14anu04 on 27-5-2014
;You will surely get through these tough times.
;आप इन मुश्किल समय में से निश्चित रूप से निकल जाएँगे .
(defrule get100
(declare (salience 2650))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(test (> ?id1 ?id))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikala_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get100   "  ?id "  nikala_jA )" crlf))
)


;@@@ Added by 14anu03 on 27-june-2014
;Get on the board,We are leaving soon.
;डेक पर चढ़ जायें, हम शीघ्र जा रहे हैं .
(defrule get102
(declare (salience 5500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(test (=(+ ?id 1) ?id1))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 para_caDZa_jAyeM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  get.clp     get102   "  ?id "  " ?id1 "  para_caDZa_jAyeM )" crlf))
)


;$$$ Modified by 14anu-ban-05 on (03-03-2015)
;changed meaning from 'pahuzcanA' to 'pahuzca'
;@@@ Added by 14anu03 on 14-june-14
;He wanted to get through to him.
;उसने उसको पहुँचने के लिए चाहा.
(defrule get101
(declare (salience 2850))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(id-word ?id2 to)
(test (and(> ?id2 ?id1)(> ?id1 ?id)))
;(id-word  =(+ ?id 1) in)
;(id-word =(+ ?id 2) with)
(kriyA-upasarga ?id ?id1)     
;(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp    get101  "  ?id "  " ?id1 " pahuzca  )" crlf))
)


;Added by Meena(12.3.10)
;She had gotten her family to go against convention. 
;And she declared that out of love for the poor she had gotten her family to go against convention . 
(defrule get0
(declare (salience 5000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(not(id-root ?id1 book|topic|chance))
(saMjFA-to_kqxanwa  ?id1 ?id2);renamed saMjFA-kqxanwa as saMjFA-to_kqxanwa by Manju (05-02-11)
(to-infinitive  =(+ ?id1 1) ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  preriwa_kara ))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp       get0   "  ?id " preriwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  get.clp       get0   "  ?id " ko )" crlf)
)
)


;Modified by Meena(16.8.11)
;He has got a cow and two dogs.
(defrule get1
(declare (salience 5000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) is|have|has)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hE))
;(assert (id-wsd_root_mng ?id hE_nahIM))
(assert (kriyA_id-subject_viBakwi ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get1   "  ?id "  hE )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  get.clp       get1   "  ?id " ke_pAsa )" crlf)
)
)



;Modified by Meena(17.8.11)
;He had got a cow and two dogs.
(defrule get2
(declare (salience 4800))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) had)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WA))
;(assert (id-wsd_root_mng ?id WA_nahIM))
(assert (kriyA_id-subject_viBakwi ?id ke_pAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get2   "  ?id "  WA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  get.clp       get2   "  ?id " ke_pAsa )" crlf)
)
)


(defrule get3
(declare (salience 4600))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aPavAha_PEla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get3  "  ?id "  " ?id1 "  aPavAha_PEla  )" crlf))
)

;I don't know how the story got about that she'd got married.
;mEM nahIM jAnawA ki yaha aPavAha kEse PEla gaI ki usakI SAxI ho gaI
(defrule get4
(declare (salience 4500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 across)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get4  "  ?id "  " ?id1 "  samaJA  )" crlf))
)

;This is the mesage that we want to get across to the people.
;yahI vaha saMxeSa hE jo hama logoM ko samaJAnA cAhawe We
(defrule get5
(declare (salience 4400))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 after)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pICA_kara\pICe_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get5  "  ?id "  " ?id1 "  pICA_kara\pICe_jA  )" crlf))
)

;Get after her && tell her not to wait for us.
;usake pICe jAo Ora kaho ki hamArA iMwajZAra na kare
(defrule get6
(declare (salience 4300))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 ahead)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saPala_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get6  "  ?id "  " ?id1 "  saPala_ho  )" crlf))
)




;It's tough for any woman to get ahead in politics.
;rAjanIwi meM saPala honA kisI BI Orawa ke lie bahuwa kaTina hE
(defrule get7
(declare (salience 4200))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 along)
(kriyA-upasarga ?id ?id1)
(or (kriyA-without_saMbanXI ?id ?id2)(kriyA-with_saMbanXI ?id ?id2))
(id-word ?id2 help)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_baDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get7  "  ?id "  " ?id1 "  Age_baDZa  )" crlf))
)

;I can't get along without some help.
;mEM binA maxaxa ke Age nahIM baDZa sakawA
(defrule get8
(declare (salience 4100))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 along)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 acCe_saMbaMXa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get8  "  ?id "  " ?id1 "  acCe_saMbaMXa_ho  )" crlf))
)


;I get along very well with her.
;mere usake sAWa bahuwa acCe saMbaMXa hEM
(defrule get9
(declare (salience 4000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aPavAha_PEla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get9  "  ?id "  " ?id1 "  aPavAha_PEla  )" crlf))
)

;I don't know how the story got about that she'd got married.
;mEM nahIM jAnawA ki yaha aPavAha kEse PEla gaI ki usakI SAxI ho gaI
(defrule get10
(declare (salience 3900))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 around)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 hala_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get10  "  ?id "  " ?id1 "  hala_nikAla  )" crlf))
)

;We don't see any way of getting around these difficulties.
;hama ina kaTinAiyoM se nikalane kA koI BI hala nahIM nikAla pA rahe hEM
(defrule get11
(declare (salience 3800))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 at)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AlocanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get11  "  ?id "  " ?id1 "  AlocanA_kara  )" crlf))
)

;His boss is always getting at him for arriving late.
;usakA bAsa hameSAM xerI se Ane para usakI AlocanA karawA hE
(defrule get12
(declare (salience 3700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 at)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get12  "  ?id "  " ?id1 "  samaJa  )" crlf))
)

;What do you think she's getting at? I've no idea what she wants!
;wumhe kyA lagawA hE ki usane kyA samaJA hE?muJe nahIM pawA vaha kyA cAhawI hE!
(defrule get13
(declare (salience 3600))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pakadZa_meM_na_A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get13  "  ?id "  " ?id1 "  pakadZa_meM_na_A  )" crlf))
)

;He gets away with murder.
;vaha kawla karane para BI pakadZa meM nahIM AyA
(defrule get14
(declare (salience 3500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 dAka_se_Beja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get14  "  ?id "  " ?id1 "  dAka_se_Beja  )" crlf))
)

;You must get this letter away today itself.
;wumheM yaha citTI Aja hI dAka se Beja xenI cAhie
(defrule get15
(declare (salience 3400))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CutakArA_pA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get15  "  ?id "  " ?id1 "  CutakArA_pA  )" crlf))
)

;The thieves got away from the police.
;coroM ne pulisa se CutakArA pA liyA
(defrule get16
(declare (salience 3300))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 baxalA_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get16  "  ?id "  " ?id1 "  baxalA_le  )" crlf))
)

;One day i'll get back at this insult.
;eka xina mEM isa beijjawI kA baxalA lUzgA
(defrule get17
(declare (salience 3200))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 by)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAma_calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get17  "  ?id "  " ?id1 "  kAma_calA  )" crlf))
)

;Although this money is not sufficient but i'll get by it.
;hAlAMki yaha pEsA kAPI nahIM hE paraMwu mEM isase kAma calA lUMgA
(defrule get18
(declare (salience 3100))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Suru_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get18  "  ?id "  " ?id1 "  Suru_kara  )" crlf))
)

;I find it difficult to get down to doing my work after the vacations.
;CuttiyoM ke bAxa kAma Suru karane meM muJe pareSAnI howI hE
(defrule get19
(declare (salience 3000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uxAsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get19  "  ?id "  " ?id1 "  uxAsa_kara  )" crlf))
)

;The miserable weather in winter really gets me down.
;sarxiyoM kA yaha KZarAba mOsama muJe uxAsa kara xewA hE
(defrule get20
(declare (salience 2900))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 nigala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get20  "  ?id "  " ?id1 "  nigala  )" crlf))
)

;Her throat was so swollen that she couldn't get anything down.
;usakA galA iwanA sUjA huA WA ki vaha kuCa BI nahIM nigala pA rahI WI
(defrule get21
(declare (salience 2800))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 liKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get21  "  ?id "  " ?id1 "  liKa  )" crlf))
)

;Did you get down everything he said?
;kyA wumane saba kuCa liKa liyA jo usane kahA?

;$$$ Modified by 14anu-ban-05 on (19-03-2015)
;He's always ready to get in with the right person. [same clp]
;vaha sahI AxamI se miwrawA karane meM hameSAM wEyAra rahawA hE. [same clp]
(defrule get22
(declare (salience 2700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 in)		;commented by 14anu-ban-05 on (19-03-2015)  
;(id-word  =(+ ?id 1) in)
;(id-word =(+ ?id 2) with)
(kriyA-upasarga ?id ?id1)
(id-root ?id1 in)		;Added by 14anu-ban-05 on (19-03-2015) 
(kriyA-with_saMbanXI  ?id ?id2) ;Added by 14anu-ban-05 on (19-03-2015)     
;(kriyA-object ?id ?)           (commented by Meena)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 miwrawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get22  "  ?id "  " ?id1 "  miwrawA_kara  )" crlf))
)



;This rule get24 was also modified by Meena on 16.10.08 to avoid the clash with other senses.The statement no 6 was added and the statements 8 & 9 were commented.


;$$$ Modified by 14anu-ban-05 on (19-03-2015) 
;I finally managed to get a word in.  ;run this sentence on parser no.- 2 of mutiple-parse
;AKirakAra mEne vo Sabxa kaha hI xiyA

(defrule get23
(declare (salience 2852))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(test (> ?id1 ?id))
(id-cat_coarse ?id verb)
(kriyA-upasarga ?id ?id1)		;uncommented by  14anu-ban-05 on (19-03-2015)
(kriyA-object ?id ?id2)			;uncommented by  14anu-ban-05 on (19-03-2015)
(id-root ?id2 word)			;added by  14anu-ban-05 on (19-03-2015)
;(id-cat_coarse ?id verb)		
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id kaha_xe)) ;commented by 14anu-ban-05 on (19-03-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kaha_xe));added by 14anu-ban-05 on (19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get23  "  ?id "  " ?id1 "  kaha_xe  )" crlf))
)

(defrule get24
(declare (salience 2500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AvaSyakawA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get24  "  ?id "  " ?id1 "  AvaSyakawA_ho  )" crlf))
)

;There is a problem in the wire-we have to get an electrician in.
;wAra meM kuCa KarAbI hE,hameM eka bijalI vAle kI AvaSyakawA hE
;Modified by Shirisha Manju (03-08-13) Suggested by Chaitanya Sir
;Removed kriyA-upasarga relation, instead added kriyA-into_saMbanXI and saMbanXI list
(defrule get25
(declare (salience 2400))
(id-root ?id get)
(id-word =(+ ?id 1) into)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI  ?id ?id1)
(id-word ?id1 shirt|burka|gown|skirt|suit|sweater|trouser|pant|robe)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) pahana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get25  "  ?id "  " (+ ?id1 1) "  pahana  )" crlf))
)
;It's very difficult to get into this tight shirt.
;isa waMga kamIjZa ko pahananA bahuwa kaTina hE


;Added by Aditya and Hardik, IIT(BHU)
;I want to get into a good college.
(defrule get_into
(declare (salience 2400))
(id-root ?id get)
(id-root =(+ ?id 1) into)
?mng <-(meaning_to_be_decided ?id)
(kriyA-into_saMbanXI ?id ?id1)
(or (id-word ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))(id-word ?id1 bus|car|train|flight))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) meM_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp  get_into  "  ?id "  " (+ ?id 1) "  meM_jA  )" crlf))
)


;$$$ Modified by 14anu05 on 27.06.14
;The rule didn't fire for : He was always [getting into] scraps at school.
;Anusaaraka translation : वह हमेशा विद्यालय पर झगडों पर [प्राप्त कर] रहा था .
;Man : वह हमेशा विद्यालय में झगडों उलझ रहा था.
;Added by Aditya and Hardik, IIT(BHU)
;I do not want to get into problem.
;It is easy to get into a fight.
(defrule get_into1
(declare (salience 2400))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(test (=(+ ?id 1) ?id1))
(kriyA-into_saMbanXI ?id ?id2)  
(id-root ?id2 problem|situation|war|fight|illusion|argument|scrap) ;Added a new word scrap by 14anu05.
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 meM_ulaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp  get_into1  "  ?id "  " ?id1 "  meM_ulaJa  )" crlf))
)


(defrule get26
(declare (salience 2400))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) into)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?id1)
(id-root ?id1 trouble)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) padZa_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get26  "  ?id "  " (+ ?id 1) "  padZa_jA  )" crlf))
)

;You'll get into a trouble if you don't do this work.
;yaxi wuma yaha kAma nahIM karoge wo musIbawa meM padZa jAoge 
;PP_null_off && transitivity=TR && category=verb	SArIrika_saMbaMXa_sWApiwa_kara	0
(defrule get27
(declare (salience 2200))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-word ?id1 bicycle)
(viSeRya-off-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwara))
(assert (kriyA_id-object_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get27  "  ?id "  " ?id1 "  uwara  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  get.clp       get27   "  ?id " se )" crlf)
)


;He'd got off with a girl at the party.
;usane pArtI meM eka ladZakI ke sAWa SArIrika saMbaMXa sWApiwa kiyA
(defrule get28
(declare (salience 2100))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get28  "  ?id "  " ?id1 "  uwAra  )" crlf))
)

;I'll not get off these shoes.
;mEM ye jUwe nahIM uwArUzgA
;PP_null_off && transitivity=TR && category=verb	baxala	0
;Can we get off this subject?
;kyA hama yaha viRaya baxala sakawe hEM?
(defrule get29
(declare (salience 2000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 AlocanA_karawe_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get29  "  ?id "  " ?id1 "  AlocanA_karawe_raha  )" crlf))
)

;You're always getting on at me!
;wuma hameMSAM merI AlocanA karawe rahawe ho
(defrule get30
(declare (salience 1900))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saMparka_sWApiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get30  "  ?id "  " ?id1 "  saMparka_sWApiwa_kara  )" crlf))
)

;Did you get on to the passport office?
;kyA wumane pAsaporta APisa se saMparka sWApiwa kiyA?
(defrule get31
(declare (salience 1800))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_baDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get31  "  ?id "  " ?id1 "  Age_baDZa  )" crlf))
)

;I can't get on without some help.
;mEM binA maxaxa ke Age nahIM baDZa sakawA






;Added by Meena(17.6.10)
;According to Sarah they are not getting on very well at the moment . 
(defrule get32
(declare (salience 2500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-at_saMbanXI  ?id  ?id3)
(kriyA-kriyA_viSeRaNa  ?id  ?id2)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 mEwrIpUrNa_saMbaMXa_cala))
(assert (kriyA_id-subject_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get32  "  ?id "  " ?id1 "  mEwrIpUrNa_saMbaMXa_cala  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  get.clp       get32   "  ?id " kA )" crlf))
)


;We get on very well together.
;hama xonoM meM mEwrIpUrNa saMbaMXa hEM





(defrule get33
(declare (salience 1600))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bUDZe_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get33  "  ?id "  " ?id1 "  bUDZe_ho  )" crlf))
)

;How old is Sam?He must be getting on.
;sEma kI umra kyA hE ?vaha BI bUDZA ho rahA hogA
(defrule get34
(declare (salience 1500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAhara_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get34  "  ?id "  " ?id1 "  bAhara_nikAla  )" crlf))
)

;How can i get him out of this situation?
;mEM use isa sWiwi se kEse bAhara nikAla sakawA hUz?
(defrule get35
(declare (salience 1400))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aPavAha_PEla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get35  "  ?id "  " ?id1 "  aPavAha_PEla  )" crlf))
)

;I don't know how the story got out that she'd got married.
;mEM nahIM jAnawA ki yaha aPavAha kEse PEla gaI ki usakI SAxI ho gaI
(defrule get36
(declare (salience 1300))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kaha_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get36  "  ?id "  " ?id1 "  kaha_xe  )" crlf))
)
;I finally managed to get the word out
;AKirakAra mEne vo Sabxa kaha hI xiyA
(defrule get37
(declare (salience 1200))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CApa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  get.clp 	get37   "  ?id " "?id1" CApa )" crlf)
)
)

;We'll have to get this newspaper out before next week.
;hameM yaha aKZabAra agale haPwe se pahale CApanA hogA

(defrule get38
(declare (salience 1000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 TIka_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get38  "  ?id "  " ?id1 "  TIka_ho  )" crlf))
)

;He has got over his fever now.
;usakA buKZAra aba TIka ho gayA hE
(defrule get39
(declare (salience 900))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get39  "  ?id "  " ?id1 "  samaJA  )" crlf))
)

;This is the mesage that we want to get over to the people.
;yahI vaha saMxeSa hE jo hama logoM ko samaJAnA cAhawe We
(defrule get40
(declare (salience 800))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 round)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get40  "  ?id "  " ?id1 "  samaJA  )" crlf))
)

;I know she is not agree with me at the moment but i'll get round her.
;mEM jAnawA hUz ki vo aBI muJase sahamawa nahIM hE lekina mEM use samaJA lUzgA
(defrule get41
(declare (salience 700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get41  "  ?id "  " ?id1 "  bAwa_kara  )" crlf))
)

;I couldn't get through (to my friend) yesterday.
;mEM kala apane xoswa se bAwa nahIM kara sakA
(defrule get42
(declare (salience 600))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pAsa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get42  "  ?id "  " ?id1 "  pAsa_ho  )" crlf))
)

;I can't get through in this exam!
;mEM isa parIkRA meM pAsa nahIM ho sakawA
(defrule get43
(declare (salience 500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaJA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get43  "  ?id "  " ?id1 "  samaJA  )" crlf))
)

;I can't get through to him that drinking (alcohal) is bad for his health.
;mEM use nahIM samaJA sakawA ki (SarAba) pInA usakI sehawa ke lie hAnikAraka hE
(defrule get44
(declare (salience 400))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 KZawma_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get44  "  ?id "  " ?id1 "  KZawma_kara  )" crlf))
)
;I got through a whole glass of milk
;mEne xUXa kA pUrA gilAsa pI kara KZawma kara xiyA




(defrule get45
(declare (salience 200))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get45  "  ?id "  " ?id1 "  pahuzca  )" crlf))
)

;Which question did you get up to in the test? 
;wuma parIkRA meM kOna se praSna para pahuzce We ?
(defrule get46
(declare (salience 100))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 wejZa_ho\baDZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get46  "  ?id "  " ?id1 "  wejZa_ho\baDZa  )" crlf))
)

;The wind is getting up,it seems there is going to be a storm.
;havA wejZa ho rahI hE,lagawA hE ki wUPAna Ane vAlA hE
(defrule get47
(declare (salience 0))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vyavasWA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get47  "  ?id "  " ?id1 "  vyavasWA_kara  )" crlf))
)

;We must get up a party for his farewell.
;hameM usakI vixAI ke lie pArtI kI vyavasWA karanI cAhie
(defrule get48
(declare (salience -100))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 about)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PEla_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get48  "  ?id "  " ?id1 "  PEla_jA  )" crlf))
)



(defrule get49
(declare (salience -300))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 along)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kAma_calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get49  "  ?id "  " ?id1 "  kAma_calA  )" crlf))
)


(defrule get50
(declare (salience -500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BAga_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get50  "  ?id "  " ?id1 "  BAga_jA  )" crlf))
)



(defrule get51
(declare (salience -700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 lOta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get51  "  ?id "  " ?id1 "  lOta  )" crlf))
)



(defrule get52
(declare (salience -900))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 behind)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 piCadZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get52  "  ?id "  " ?id1 "  piCadZa  )" crlf))
)



(defrule get53
(declare (salience -1100))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 by)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 acCA_mAna_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get53  "  ?id "  " ?id1 "  acCA_mAna_le  )" crlf))
)



(defrule get54
(declare (salience -1300))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 down)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get54  "  ?id "  " ?id1 "  uwara  )" crlf))
)



(defrule get55
(declare (salience -1500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Gusa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get55  "  ?id "  " ?id1 "  Gusa  )" crlf))
)


;He was not in when I got there , so it was a completely wasted journey .
(defrule get56
(declare (salience -1600))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-subject ?id  =(- ?id 1))
(kriyA-aXikaraNavAcI  ?id =(+ ?id 1));I got the news. Added by Shirisha Manju(Suggested by Sukhada)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get56   "  ?id "  pahuzca )" crlf))
)

(defrule get57
(declare (salience -1700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 inside)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 anxara_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get57  "  ?id "  " ?id1 "  anxara_jA  )" crlf))
)



(defrule get58
(declare (salience -1900))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 into)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 praveSa_kara))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get58  "  ?id "  " ?id1 "  praveSa_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  get.clp       get58   "  ?id " meM )" crlf))
)





(defrule get59
(declare (salience -2100))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uwara))
(assert (kriyA_id-object_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get59  "  ?id "  " ?id1 "  uwara  )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  get.clp       get59   "  ?id " se) " crlf)
)




(defrule get60
(declare (salience -2300))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pragawi_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get60  "  ?id "  " ?id1 "  pragawi_kara  )" crlf))
)


(defrule get61
(declare (salience -2500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pawA_cala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get61  "  ?id "  " ?id1 "  pawA_cala  )" crlf))
)


(defrule get62
(declare (salience -2700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 BUla_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get62  "  ?id "  " ?id1 "  BUla_jA  )" crlf))
)



(defrule get63
(declare (salience -3000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 rid)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CutakArA_pA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get63 "  ?id "  " ?id1 "  CutakArA_pA  )" crlf))
)

(defrule get64
(declare (salience -3100))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get64  "  ?id "  " ?id1 "  pUrA_kara  )" crlf))
)


(defrule get65
(declare (salience -3300))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 to)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get65  "  ?id "  " ?id1 "  pahuzca  )" crlf))
)


(defrule get66
(declare (salience -3500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 together)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ekawra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get66  "  ?id "  " ?id1 "  ekawra_kara  )" crlf))
)


(defrule get67
(declare (salience -3700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get67  "  ?id "  " ?id1 "  uTa  )" crlf))
)


;$$$ Modified by 14anu-ban-05 on (04-12-2014)
;Added familiar on (17-01-2015)
;We shall get familiar with some of these forces in later parts of this course.[NCERT]
;inameM se kuCa baloM se hama pATyakrama ke bAxa vAle BAga meM pariciwa hoMge. [MANUAL]
;It began to get dark.[COCA]
;अब अन्धेरा हो रहा .[MANUAL]
(defrule get68
(declare (salience 2601))	;increased salience from -3900 to 2601
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa  ?id1 ?id)	;added by 14anu-ban-05
(id-word ?id1 dark|familiar)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get68   "  ?id "  ho )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (04-12-2014)
;We're getting late to work because of the checkpoint.[COCA]
;हम को  नाके की वजह से काम करने के लिए देरी  हो रहा है . [MANUAL]
(defrule get69
(declare (salience -4000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id ?id1)	;added by 14anu-ban-05
(id-word ?id1 late)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get69   "  ?id "  ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  get.clp       get69   "  ?id " ko )" crlf)
)
)


 ;Modified by sheetal.
(defrule get70
(declare (salience 4950))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_mUla  ?id ?id1)
(id-word ?id1 upset)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (make_verbal_noun ?id1))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id uwwejiw_ho))
;(assert (id-wsd_root_mng ?id uwwejiw_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun   " ?*prov_dir* "  get.clp     get70   "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  get.clp       get70   "  ?id  ?id1 "  uwwejiw_ho )" crlf))
)
 


;Modified by Meena(1.11.10)
;Why does not the shoe get there first , since gravity is pulling harder on it ? 
(defrule get71
(declare (salience -4400))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 home)
(kriyA-aXikaraNavAcI  ?id ?id1) ;kriyA-lupwa_prep_saMbanXI  is now changed to kriyA-aXikaraNavAcI (Modified by Roja 28-12-10 Suggested by Sukhada)
;(or(kriyA-object ?id ?id1)(kriyA-aXikaraNavAcI  ?id ?id1)) ;kriyA-lupwa_prep_saMbanXI  is now changed to kriyA-aXikaraNavAcI (Modified by Roja 28-12-10 Suggested by Sukhada) ;Commented by Manju Suggested by Sukhada
;(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get71   "  ?id "  pahuzca )" crlf))
)





;Modified by Meena(25.6.10)
;The last I heard they were getting a divorce.
;I went to the store, got a gallon of milk and returned the eggs.
(defrule get72
(declare (salience 1500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-of_saMbanXI  ?id1  =(+ ?id1 2))  ;commented by Meena(25.5.10)
(kriyA-object ?id ?id1)
(id-root ?id1 petrol|milk|gallon|divorce)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp       get72   "  ?id "  le )" crlf))
)




;Modified by Meena(1.11.10)
;The five best costumes got prizes.  
(defrule get73
(declare (salience -1500))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id1 noun)
(id-root ?id1 letter|book|prize|this|them|advantage)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get73   "  ?id "  prApwa_kara )" crlf))
)

(defrule get74
(declare (salience -4600))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 freedom)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get74   "  ?id "  pA )" crlf))
)




;Added by Meena(15.9.11)
;He got a topic to speak at the forum. 
;We did not even get a chance to do the programs we wanted to do. 
(defrule get75
(declare (salience 2601))	;increased salience from -1600 to 2601 by 14anu-ban-05 on (04-02-2015)
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 topic|chance)
(kriyA-object ?id ?id1)
(or(saMjFA-to_kqxanwa  ?id1 =(+ ?id1 2))(kriyA-kriyArWa_kriyA  ?id =(+ ?id1 2)))
=>
(retract ?mng)
(assert (kriyA_id-subject_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id mila))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp       get75   "  ?id "  mila )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  get.clp      get75   "  ?id " ko )" crlf))
)





(defrule get76
(declare (salience -4700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) me|us|him|her|them)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get76   "  ?id "  lA_xe )" crlf))
)


(defrule get77
(declare (salience -4900))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 uTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get77  "  ?id "  " ?id1 "  uTa  )" crlf))
)


(defrule get78
(declare (salience -5700))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 rukAvatoM_ke_bAvajUxa_pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get78  "  ?id "  " ?id1 "  rukAvatoM_ke_bAvajUxa_pahuzca  )" crlf))
)




(defrule get79
(declare (salience -6000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bAhara_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get79  "  ?id "  " ?id1 "  bAhara_ho_jA  )" crlf))
)


(defrule get80
(declare (salience -6200))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get80   "  ?id "  prApwa_kara )" crlf))
)


;Added by sheetal(29-12-2009).
(defrule get81
(declare (salience 4950))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) wet)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp      get81   "  ?id "  ho )" crlf))
)
;Do not get wet in the rain .

;$$$ Modified by 14anu-ban-10 on (11-11-2014) 
;#### [COUNTER EXAMPLE] #### As the day dawns , bazaar gets ready in the small and big streets of Goa  .[tourism corpus] ; added by 14anu-ban-10 on (11-11-2014)
;दिन  निकलते  ही  गोवा  की  छोटी-बड़ी  अलियों-गलियों  में  बाजार  लग  जाता  है  ।[tourism corpus] ; added by 14anu-ban-10 on (11-11-2014)
;@@@ Added by Shirisha Manju (28-04-14) Suggested by Chaitanya Sir
;We are getting ready for the movie.
(defrule get_ready
(declare (salience 4000))
(id-word ?id getting) ; added by 14anu-ban-10 on (11-11-2014)
;(id-root ?id get) ; commented by  14anu-ban-10 on (11-11-2014)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) ready)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  wEyAra_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp   get_ready  "  ?id "  " (+ ?id 1) "  wEyAra_ho  )" crlf))
)


;default_sense && category=verb	mila	0
;"get","V","1.milanA"
;I get my salary on the 7th of every month.
;--"2.pahuzcanA"
;Please get to the airport before noon.
;--"3.samaJanA"
;Are you getting my point?
;--"4.rAjI_karanA"
;I will get him to speak to you.
;
;
;@@@ Added by 14anu02 on 28.06.14
;If word gets out about the affair, he will have to resign.
;यदि प्रेम सम्बन्ध के बारे में अफवाह फैलती है, तो उसको त्याग पत्र देना पडेगा . 
(defrule get82
(declare (salience 5000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) word)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) around|out)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  PEla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp   get82  "  ?id "  " (+ ?id 1) "  PEla  )" crlf))
)

;@@@ Added by 14anu-ban-01 on 2-08-14.
;290494:Arising of physical and mental problems on the unavailability of intoxication like the shivering of limbs on not getting alcohol , not getting sleep or getting restless .
;नशा उपलब्ध न होने पर शारीरिक व मानसिक परेशानियों का पैदा होना जैसे शराब न मिलने पर हाथ पैर काँपना , नींद न आना या बेचैनी होना ।
(defrule get83
(declare (salience 5000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?obj)
(id-root ?obj restless)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get83   "  ?id " ho  )" crlf))
)

;@@@ Added by 14anu21 on 28.06.2014
;We have to get to the root of the problem. [oxford]
;हमें समस्या की जड के लिए प्राप्त करना है |(Translation before adding rule)
;हमें समस्या की जड तक पहुँचना है .
(defrule get84
(declare (salience 5000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(to-infinitive  ?to1 ?id)
(kriyA-to_saMbanXI  ?id ?idsam)
(not(id-root ?idsam ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1)  waka_pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp   get84  "  ?id "  " (+ ?id 1) "  waka_pahuzca  )" crlf))
)

;$$$Modified by 14anu-ban-05 on (08-01-2015)
;changed meaning from ' dAlawA_hE' to 'viGna dAla' and used 'affecting_id-affected_ids' in assert and print statements
;@@@ Added by avni(14anu11)
;If it gets in the way of your child's enjoyment of reading and writing, there are ways you can help using everyday items .
;यदि यह आप के बच्चे की पढाई व लिखाई के आनन्दमय पथ पर विघ्न डालता है , तो ऐसे तरीके हैं जिनके द्वारा आप रोजमर्रे की चीजों का प्रयोग करके उस की मदद कर सकते हैं . 
(defrule get91
(declare (salience 5000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 in)
(test (> ?id1 ?id))
(id-cat_coarse ?id verb)
;(or(kriyA-subject  ?id2 ?id)(kriyA-vAkya_viBakwi  ?id ?id3)(kriyA-in_saMbanXI  ?id ?id4)) ;commented by 14anu-ban-05 on (08-01-2015)
(kriyA-in_saMbanXI  ?id ?id2);added by 14anu-ban-05 on (08-01-2015)
(id-root ?id2 way)		;added by 14anu-ban-05 on (08-01-2015)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id dAlawA_hE)) ;commented by 14anu-ban-05 on (08-01-2015) 
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 viGna_dAla)) ;added by 14anu-ban-05 on (08-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get91  "  ?id "  " ?id1 " " ?id2 "  viGna_dAla)" crlf))	
)

;@@@ Added by 14anu-ban-05 on (04-12-2014)
;Don't touch that wire or you'll get a shock. [OALD]
;उस तार को मत छुओ अन्यथा तुम्हें  बिजली का झटका लगेगा. [MANUAL]
(defrule get92
(declare (salience 5000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 shock)

=>
(retract ?mng)
(assert (id-wsd_root_mng ?id laga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get92   "  ?id " laga  )" crlf))
)  

;$$$ Modified by 14anu-ban-05 (09-01-2015) ------changed meaning from 'ho_jAwe' to 'ho_jAwA'
;@@@ Added by 14anu17
;As they get older.
;जब वे वृद्ध हो जाते हैं ,
(defrule get93
(declare (salience -6000))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ho_jAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get93   "  ?id "  ho_jAwA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (03-02-2015)
;According to tradition it is believed that in the 8th century the maximum part of Ladakh had got flooded .	[tourism]
;paramparAnusAra mAnA jAwA hE ki 8vIM saxI meM laxZxAKa kA aXikAMSa BAga jalaplAviwa ho gayA WA . [tourism]
(defrule get94
(declare (salience 5001))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) flood)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) jalaplAviwa_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp  get94  "  ?id "  " + ?id 1 "  jalaplAviwa_ho_jA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-02-2015)
;Not only elders but also kids of three - four years get along with his father for the darshan after having a bath in the cool water .[TOURISM]
;kevala badZe-bujurga hI nahIM apiwu wIna-cAra varRa ke SiSu BI isa SIwala jala se snAna kara Sanixeva ke xarSana ke lie apane piwA ke sAWa cala padZawe hEM .[TOURISM]


(defrule get95
(declare (salience 5002))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word =(+ ?id 1) along)
(kriyA-with_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) cala_padZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get95  "  ?id "  " (+ ?id 1) "  cala_padZa  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-02-2015)
;He was lucky to get away with only a fine.[OALD]
;वह भाग्यशाली था कि केवल जुर्माने से छूट गया.   [manual] 

(defrule get96
(declare (salience 5003))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 away)
(kriyA-with_saMbanXI  ?id ?id2)
(id-root ?id2 fine)		;more constraints can be added
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 CUta_jA))	;changed (+ ?id 1) by 14anu-ban-05 to ?id1 on (19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get96  "  ?id "  " ?id1 "  CUta_jA )" crlf))				
)								;changed (+ ?id 1) by 14anu-ban-05 to ?id1 on (19-03-2015)


;@@@ Added by 14anu-ban-05 on (25-02-2015)
;Thieves got away with computer equipment worth $30 000.	[OALD]
;चोर 30,000 डालर मूल्य  के कम्पयूटर उपकरण  के साथ  चले गये.		[manual]

(defrule get97
(declare (salience 5003))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 away)
(kriyA-with_saMbanXI  ?id ?id2)
(id-root ?id2 equipment)		;more constraints can be added
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 cale_jA))	;changed (+ ?id 1) by 14anu-ban-05 to ?id1 on (19-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get97  "  ?id "  " ?id1 "  cale_jA)" crlf))	;changed (+ ?id 1) by 14anu-ban-05 to ?id1 on (19-03-2015)
)

;@@@ Added by 14anu-ban-05 on (03-03-2015)
;Last year we got through to the final.[CALD]
;पिछले वर्ष हम अन्तिम चरण तक पहुँच गये थे.		[Manual]

(defrule get98
(declare (salience 2850))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 through)
(kriyA-to_saMbanXI  ?id ?id2)
(id-root ?id2 final)		;more constraints can be added
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pahuzca_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get98  "  ?id "  " ?id1 "  pahuzca_jA)" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-03-2015)
;I got home and flaked out on the sofa.[cald]
;मैं घर पहुँचा और सोफे पर ढह गया. [manual]

(defrule get99
(declare (salience 2851))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get99   "  ?id "  pahuzca )" crlf))
)

;@@@ Added by 14anu-ban-05 on (12-03-2015)
;When an external force does work in taking a body from a point to another against a force like spring force or gravitational force, that work gets stored as potential energy of the body. [NCERT]
;aba koI bAhya bala kisI vaswu ko eka biMxu se xUsare biMxu waka, kisI anya bala; jEse-spriMga bala, guruwvIya bala Axi ke viruxXa, le jAwA hE, wo usa bAhya bala xvArA kiyA gayA kArya usa vaswu meM sWiwija UrjA ke rUpa meM saFciwa ho jAwA hE.[NCERT]

(defrule get103
(declare (salience 2852))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) store)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  get.clp 	get103   "  ?id "  jA )" crlf))
)


;@@@ Added by 14anu-ban-05 on (19-03-2015)
;We're hoping to get away for a few days at Easter.[OALD]
;हम ईस्टर पर कुछ दिनों के लिये छुट्टी पर जाने  की आशा कर रहे हैं . 	[manual]

(defrule get104
(declare (salience 5004))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 away)
(kriyA-for_saMbanXI  ?id ?id2)
(id-root ?id2 day|week|fortnight)		;more constraints can be added
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id1  CuttI_para_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get104  "  ?id "  " ?id1 "  CuttI_para_jA)" crlf))
)


;@@@ Added by 14anu-ban-05 on (19-03-2015)
;I won't be able to get away from the office before 7.	[OALD]
;मैं 7 बजे से पहले दफ्तर बाहर जाने मे समर्थ नहीं रहूँगा . 			[manual]

(defrule get105
(declare (salience 5004))
(id-root ?id get)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) away)
(kriyA-from_saMbanXI  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  (+ ?id 1)  bAhara_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " get.clp	get105  "  ?id "  " (+ ?id 1) "  bAhara_jA)" crlf))
)



