;@@@ Added by 14anu-ban-04 (11-03-2015)
;I distrust that man.                   [same clp file]
;मैं उस आदमी पर सन्देह करता हूँ .                    [self]
(defrule distrust2
(declare (salience 4910))
(id-root ?id distrust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMxeha_kara))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* " distrust.clp     distrust2   "  ?id " para  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distrust.clp 	distrust2   "  ?id "  saMxeha_kara )" crlf))
)

;------------------------ Default Rules ----------------------

;"distrust","N","1.aviSvAsa/saMxeha"
(defrule distrust0
(declare (salience 5000))
(id-root ?id distrust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aviSvAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distrust.clp 	distrust0   "  ?id "  aviSvAsa )" crlf))
)

;"distrust","VT","1.aviSvAsa_karanA/saMxeha_karanA"
;I distrust that man
(defrule distrust1
(declare (salience 4900))
(id-root ?id distrust)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aviSvAsa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distrust.clp 	distrust1   "  ?id "  aviSvAsa_kara )" crlf))
)

