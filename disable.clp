;@@@ Added by 14anu-ban-04 (01-04-2015)
;He was disabled in a car accident.                [oald]
;वह एक गाड़ी दुर्घटना में विकलांग हो गया था .                     [self]
(defrule disable1
(declare (salience 30))
(id-root ?id disable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-tam_type ?id passive) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikalAMga_ho))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disable.clp    disable1 "  ?id "  vikalAMga_ho)" crlf))
)

;@@@ Added by 14anu-ban-04 (01-04-2015)
;The gunfire could  disable or kill the pilot.             [olad]
;गोलाबारी विमान चालक को अपाहिज कर सकती या   नष्ट  कर सकती है.               [self]
(defrule disable2 
(declare (salience 20))
(id-root ?id disable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id apAhija_kara))    
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disable.clp    disable2 "  ?id "  apAhija_kara)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04 (01-04-2015)
;Disable the alarm system and then enter the building.                   [cald]      
;सचेतक प्रणाली को निष्क्रिय कीजिए और बाद में इमारत में  प्रवेश कीजिए .                        [self]
(defrule disable0
(declare (salience 10))
(id-root ?id disable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))  
(assert (id-wsd_root_mng ?id niRkriya_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disable.clp     disable0   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disable.clp    disable0 "  ?id " niRkriya_kara)" crlf))
)
