;@@@Added by 14anu-ban-02(07-04-2015)
;We drove to the hospital at breakneck speed.[oald]	;run the sentence on parser no. 3
;हम तेज रफ्तार पर अस्पताल गये .[self] 
(defrule breakneck1 
(declare (salience 100)) 
(id-root ?id breakneck) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 speed)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id wejZa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breakneck.clp  breakneck1  "  ?id "  wejZa )" crlf)) 
) 


;----------------------------default_rules-------------------------------

;@@@Added by 14anu-ban-02(07-04-2015)
;Sentence: The breakneck production of naval vessels during World War II.[mw]	;run the sentence on parser no. 3
;Translation: विश्व युद्ध II के दौरान नौसैनिक जहाजो का अन्धाधुन्ध निर्माण.[self]
(defrule breakneck0 
(declare (salience 0)) 
(id-root ?id breakneck) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id anXAXunXa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  breakneck.clp  breakneck0  "  ?id "  anXAXunXa )" crlf)) 
) 
