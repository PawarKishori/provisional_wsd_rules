

;Added by Meena(16.9.10)
;In the first experiment, a large magnet and a small magnet are weighed separately, and then one magnet is hung from the pan of the top balance so that it is directly above the other magnet.
(defrule pan00
(declare (salience 5000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paladA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp       pan00   "  ?id "  paladA )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;His last novel was panned by the critics. [Cambridge]
;उसके पिछले उपन्यास की आलोचकों द्वारा आलोचना की गई थी.
(defrule pan2
(declare (salience 5000))
(id-root ?id pan)
(id-word ?id panned)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AlocanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp 	pan2   "  ?id "  AlocanA_kara )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (15-12-2014)
;@@@ Added by 14anu01
;My labour has panned out.
;मेरा श्रम सफल हुआ है . 
(defrule pan3
(declare (salience 6000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(id-word =(+ ?id 1) out) ;commented by 14anu-ban-09 on (15-12-2014)
(kriyA-upasarga  ?id ?id1) ;Added by 14anu-ban-09 on (15-12-2014)
(id-root ?id1 out) ;Added by 14anu-ban-09 on (15-12-2014)
;(id-cat_coarse =(+ ?id 1) particle) ;commented by 14anu-ban-09 on (15-12-2014)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id saPala_ho)) ;commented by 14anu-ban-09 on (15-12-2014)
(assert (affecting_id-affected_ids-wsd_group_root_mng  ?id ?id1 saPala_ho)) ;Added by 14anu-ban-09 on (15-12-2014)
(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp 	pan3   "  ?id " saPala_ho )" crlf)) ;commented by 14anu-ban-09 on (15-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  pan.clp 	pan3   "  ?id " "  ?id1 " saPala_ho )" crlf)) ;Added by 14anu-ban-09 on (15-12-2014)
)

;@@@ Added by 14anu-ban-09 on (21-02-2015)
;He panned the camera along the row of faces. 	[oald]
;उसने कैमरा चेहरों की पङ्क्ति के साथ-साथ घुमाया . 		[self] 
;The camera panned back to the audience.	[oald]
;कैमरा वापस श्रोतागण की ओर घुमाया .			[self] 

(defrule pan4
(declare (salience 5000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(kriyA-subject ?id ?id1)(kriyA-object ?id ?id1))
(id-root ?id1 camera)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GumA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp 	pan4   "  ?id "  GumA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (21-02-2015)
;The BSP candidate was panned during his visit to Meerut. 	[oald.com]
;मेरठ के  निकट निरीक्षण करने के दौरान  बीएसपी उम्मीदवार की आलोचना की गयी थी .	[self] 

(defrule pan5
(declare (salience 5000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma  ?id ?id1)
(id-root ?id1 candidate)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id2)
(id-cat_coarse ?id2 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AlocanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp 	pan5   "  ?id "  AlocanA_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (28-02-2015)
;I don't know how things will pan out now the company's been taken over. 	[usingenglish.com]
;मैं नहीं जानता कि चीजें कैसे सुधरेगी अब तो कम्पनी पर कब्जा हो गया हैं.				[self] 
(defrule pan6
(declare (salience 6000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1) 
(id-root ?id1 out) 
(kriyA-subject  ?id ?id2)
(id-root ?id2 thing)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng  ?id ?id1 suXara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  pan.clp 	pan6   "  ?id " "  ?id1 " suXara )" crlf)) 
)

;@@@ Added by 14anu-ban-09 on (16-03-2015)
;The critics panned the movie version of the novel. 	[cald]
;आलोचकों ने उपन्यास के चलचित्र संस्करण की आलोचना करी .		[self] 

(defrule pan7
(declare (salience 5000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 critic|show)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AlocanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp 	pan7   "  ?id "  AlocanA_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (16-03-2015)  ;NOTE- Run on parser no. 8.
;Panned the fish right after catching it. 	[free dictionary.com]
;मछली पकड़ने के बाद ठीक से पकाइए . 			[self] 

(defrule pan8
(declare (salience 5000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 fish|food)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pakA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp 	pan8   "  ?id "  pakA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-04-2015)
;Parser problem. Run on parser no. 14.
;Coat roasting pan with olive oil spray. [coca]
;भर्जन पात्र/भुनने की कढाई पर जैतून के तेल का स्प्रे लगाया . 	[Manual]

(defrule pan9
(declare (salience 5000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 roasting)
=>
(assert (affecting_id-affected_ids-wsd_group_root_mng  ?id ?id1 Barjana_pAwra/Bunane_kI_kaDAI)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  pan.clp 	pan9   "  ?id " "  ?id1 " Barjana_pAwra/Bunane_kI_kaDAI )" crlf)) 
)


;----------------------- Default rules ----------------
(defrule pan0
;(declare (salience 5000))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wavA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp 	pan0   "  ?id "  wavA )" crlf))
)

(defrule pan1
(declare (salience 4900))
(id-root ?id pan)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wasale_meM_kuCa_Xo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  pan.clp 	pan1   "  ?id "  wasale_meM_kuCa_Xo )" crlf))
)


;"pan","V","1.wasale meM kuCa XonA"
;The prospectors living in South Africa pan for gold && live their life.
;--"2."
;The BSP candidate was panned during his visit to Meerut.
;--"3."
;The photographer panned the camera to take perfect snaps.
;
;
