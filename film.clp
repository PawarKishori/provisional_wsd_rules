
(defrule film0
(declare (salience 5000))
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-over_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JillI_se_Daka));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " film.clp film0 " ?id "  JillI_se_Daka )" crlf)) 
)

(defrule film1
(declare (salience 4900))
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-over_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JillI_se_Daka));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " film.clp film1 " ?id "  JillI_se_Daka )" crlf)) 
)

(defrule film2
(declare (salience 4800))
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-over_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JillI_se_Daka));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " film.clp film2 " ?id "  JillI_se_Daka )" crlf)) 
)

(defrule film3
(declare (salience 4700))
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 JillI_se_Daka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " film.clp	film3  "  ?id "  " ?id1 "  JillI_se_Daka  )" crlf))
)

(defrule film4
(declare (salience 4600))
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JillI_se_Daka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  film.clp 	film4   "  ?id "  JillI_se_Daka )" crlf))
)

(defrule film5
(declare (salience 4500))
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sinemA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  film.clp 	film5   "  ?id "  sinemA )" crlf))
)

;"film","N","1.sinemA"
;The film now showing at the theater is very good.
;--"2.kEmare_kI_rIla"
;Have you changed the film in your camera.
;--"3.JillI"
;There is a film of oil on the water of this pond.
;
;
;@@@ Added by 14anu-ban-05 on (27-10-2014)
;The working of telescopes and microscopes, colors exhibited by thin films, etc., are topics in optics.[NCERT]
;xUrabIna (xUraxarSaka) waWA sUkRmaxarSI kI kAryaviXi, pawalI JillI ke rafga, Axi prakASikI ke upaviRaya hEM.[NCERT]
(defrule film6
(declare (salience 4900))
(Domain physics)
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 thin)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JillI))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  film.clp 	film6  "  ?id "   JillI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  film.clp       film6   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (27-10-2014)
;The oleic acid drop spreads into a thin, large and roughly circular film of molecular thickness on water surface.[NCERT]
;olIka amla kI yaha bUnxa jala ke pqRTa ke Upara lagaBaga vqwwAkAra, eka aNu motAI kI Pilma ke rUpa meM PEla jAwI hE.[NCERT]
(defrule film7
(declare (salience 5000))
(Domain physics)
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-into_saMbanXI  ?id1 ?id)
(id-root ?id1 spread)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pilma_ke_rUpa_meM))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  film.clp 	film7  "  ?id "  Pilma_ke_rUpa_meM  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  film.clp       film7   "  ?id "  physics )" crlf))
)

;@@@ Added by 14anu-ban-05 on (27-10-2014)
;Then, we quickly measure the diameter of the thin film to get its area A.[NCERT]
;isa prakAra banI wanu Pilma kA vyAsa mApa kara isakA kRewraPala @A jFAwa kiyA jA sakawA hE.[NCERT]
;If we assume that the film has mono-molecular thickness, then this becomes the size or diameter of a molecule of oleic acid.[NCERT]
;yaxi hama yaha mAna leM ki Pilma eka ekANvika motAI kI hE wo '@t' olIka amla ke aNu kI AmApa aWavA vyAsa bana jAwA hE.[NCERT]
(defrule film8
(declare (salience 4900))
(Domain physics)
(id-root ?id film)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa  ?id ?)		
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pilma))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  film.clp 	film8  "  ?id "  Pilma  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  film.clp       film8   "  ?id "  physics )" crlf))
)

