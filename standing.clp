;@@@Added by 14anu-ban-01 on (28-01-2015) :run on parse no. 38
;It was standing room only at the concert.[oald]
; संगीत-समारोह में केवल खड़े रहने की जगह थी. [self]
(defrule standing3
(declare (salience 3000))
(id-root ?id standing)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) room)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) Kade_rahane_kI_jagaha))
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " standing.clp     standing3  "  ?id "  " (+ ?id 1) " Kade_rahane_kI_jagaha)" crlf)
)

;------------------------------- Default Rules ---------------------
;copied from stand.clp
;Example added by 14anu-ban-01 on (28-01-2015) 
;Russia is one of the standing members of UN.[stand.clp]:run on parse no.6
;रूस यूएन के स्थायी सदस्यों में से एक है. [self]
(defrule standing0
(declare (salience 0))
(id-root ?id standing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sWAyI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  standing.clp  standing0   "  ?id "  sWAyI )" crlf))
)

;copied from stand.clp
;$$$ Modified by 14anu-ban-01 on (28-01-2015)
;He has a good standing in the society.[stand.clp]
;उसकी समाज में (काफी इज़्ज़त/अच्छी प्रतिष्ठा) है.[self]
(defrule standing1
(declare (salience 0))
(id-root ?id standing)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id prawiRTA/ijZjZawa)) ; changed meaning 'sWAI' to 'prawiRTA/ijZjZawa'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "   standing.clp  standing1    "  ?id "  prawiRTA/ijZjZawa)" crlf))
)

;copied from stand.clp
;$$$ Modified by 14anu-ban-01 on (28-01-2015) 
;@@@ Added by 14anu04 on 26-June-2014
;He was at the third position in the standings. [stand.clp]
;वह विजेता क्रम में तीसरे स्थान पर था. [stand.clp]
(defrule standing2
(declare (salience 1000))
;(id-root ?id stand) ; commented by 14anu-ban-01 on (28-01-2015)
(id-word ?id standings)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vijewA_krama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  standing.clp 	standing2   "  ?id "  vijewA_krama )" crlf))
)

