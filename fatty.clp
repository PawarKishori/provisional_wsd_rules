;@@@ Added by Bhagyashri Kulkarni (16-09-2016)
;Eat less fatty food. (health)
;कम वसायुक्त आहार खाइए . 
;This food is fatty.
;यह आहार वसायुक्त है . 
(defrule fatty0
(declare (salience 5200))
(id-word ?id fatty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa ?id1 ?id)(subject-subject_samAnAXikaraNa  ?id1 ?id))
(id-root ?id1 food)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vasAyukwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fatty.clp 	fatty0   "  ?id "  vasAyukwa)" crlf))
)


;@@@Added by Bhagyashri Kulkarni (16-09-2016)
(defrule fatty1
(declare (salience 1000))
(id-word ?id fatty)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id carbIxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  fatty.clp  	fatty1  "  ?id "  carbIxAra)" crlf))
)

