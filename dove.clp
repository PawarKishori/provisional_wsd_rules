;##############################################################################
;#  Copyright (C) 2013-2014 Pramila (pramila3005 @gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################


;@@@ Added by Pramila(BU) on 11-01-2014
;He is a dove indeed.       ;shiksharthi
;वह सचमुच  सीधा है.
(defrule dove0
(declare (salience 4950))
(id-root ?id dove)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dove.clp 	dove0   "  ?id "  sIXA )" crlf))
)

;-----------------------default rules--------------------------------------------
;@@@ Added by Pramila(BU) on 11-01-2014
;A dove is cooing on a tree.       ;shiksharthi
;एक कबूतरी पेड़ पर गुटरगूं कर रही है.
(defrule dove1
(declare (salience 4000))
(id-root ?id dove)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kabUwarI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dove.clp 	dove1   "  ?id "  kabUwarI )" crlf))
)
;
;@@@ Added by 14anu26 [16-06-14]
;She was fed by the Dove.
;वह पवित्र आत्मा के द्वारा पाली गयी थी .
(defrule dove2
(declare (salience 5000))
(id-root ?id dove)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-det_viSeRaNa  ?id ?id1)
(id-root ?id1 the)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paviwra_AwmA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dove.clp 	dove2   "  ?id " paviwra_AwmA)" crlf))
)
