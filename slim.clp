;$$$ Modified by 14anu-ban-01 on (05-01-2015) 
;@@@ Added by 14anu03 on 28-june-2014
;It might cause someone to develop the slimmest suspicion about our true activities.
;यह किसी भी व्यक्ति के मन में हमारी वास्तविक गतिविधियों को लेकर शक पैदा कर सकता है.[Translated by 14anu-ban-01 on (05-01-2015)]
;मालिक के होटल के कमरे के दरवाजा में से अंदर अाने के लिए पर्याप्त बिल्ली के लिए खुला हुआ था .
(defrule slim2
(declare (salience 5500))
(id-root ?id slim)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
(id-root =(+ ?id 1) suspicion);changed 'word' to 'root' by 14anu-ban-01 on (05-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slim.clp 	slim2   "  ?id "  - )" crlf))
)

(defrule slim0
(declare (salience 5000))
(id-root ?id slim)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slim.clp 	slim0   "  ?id "  pawalA )" crlf))
)

(defrule slim1
(declare (salience 4900))
(id-root ?id slim)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slim.clp 	slim1   "  ?id "  pawalA )" crlf))
)

;"slim","Adj","1.pawalA"
;Today girls dream for a slim && trim figure.
;--"2.kama"


;@@@ Added by 14anu-ban-11 on (28-02-2015)
;THe chances of India winning the match are very slim.(slim.clp)
;भारत के  मैच जीतने की सम्भावनाएँ अत्यन्त कम हैं . (self)
(defrule slim3
(declare (salience 5001))
(id-root ?id slim)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective)
(viSeRya-viSeRaka  ?id ?id1)
(id-root ?id1 very)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slim.clp 	slim3   "  ?id "  kama )" crlf))
)
