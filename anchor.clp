;@@@Added by 14anu-ban-02(07-03-2015)
;The ship lay at anchor two miles off the rocky coast.[oald]	;'lay at anchor' is the idiom.(idiom.freedictionary.com)
;जहाज पथरीले समुद्र तट  पर से दो मील दूर लंगर से अटका हुआ पड़ा रहा  . [self] 
(defrule anchor1 
(declare (salience 100)) 
(id-root ?id anchor) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-at_saMbanXI  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 ship|boat)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id lafgara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anchor.clp  anchor1  "  ?id " lafgara )" crlf)) 
) 

;@@@Added by 14anu-ban-02(07-03-2015)
;The anchor of the family.[oald]
;परिवार का सहारा.[self]
(defrule anchor2 
(declare (salience 100)) 
(id-root ?id anchor) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 family)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sahArA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anchor.clp  anchor2  "  ?id " sahArA )" crlf)) 
) 

;@@@Added by 14anu-ban-02(09-03-2015)
;We anchored off the coast of Spain.[oald]
;हमने स्पेन के समुद्र तट पर लन्गर डाला . [self]
(defrule anchor4 
(declare (salience 100)) 
(id-root ?id anchor) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 off)
=> 
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 langara_dAla)) 
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  anchor.clp  anchor4   "  ?id " para )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " anchor.clp  anchor4 "  ?id "  "?id1 "  langara_dAla )" crlf))
)


;@@@Added by 14anu-ban-02(09-03-2015)
;The crane is securely anchored at two points.[oald]
;क्रेन  दो स्थानो पर मजबूती से सहारा देकर स्थापित किया गया है . [self]	;suggested by chaitanaya sir
(defrule anchor5 
(declare (salience 100)) 
(id-root ?id anchor) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-at_saMbanXI  ?id ?id1)
(id-root ?id1 point)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sahArA_xekara_sWApiwa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anchor.clp  anchor5  "  ?id " sahArA_xekara_sWApiwa_kara) )" crlf)) 
) 


;@@@Added by 14anu-ban-02(09-03-2015)
;She anchored the evening news for seven years.[oald]
;उसने सात वर्षों तक शाम के समाचार पढ़े . [self]
(defrule anchor6 
(declare (salience 100)) 
(id-root ?id anchor) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object  ?id ?id1)
(id-root ?id1 news)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id paDZa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anchor.clp  anchor6  "  ?id " paDZa) )" crlf)) 
) 

;------------------------ Default Rules ----------------------

;@@@Added by 14anu-ban-02(07-03-2015)
;Sentence:  An anchor of the program.[self]
;Translation: प्रोग्राम का सन्चालक . [self]
(defrule anchor0 
(declare (salience 0)) 
(id-root ?id anchor) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id noun) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sancAlaka)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anchor.clp  anchor0  "  ?id " sancAlaka )" crlf)) 
) 


;@@@Added by 14anu-ban-02(09-03-2015)
;Her novels are anchored in everyday experience.[oald]
;उसके उपन्यास प्रतिदिन अनुभव में सहारा देते हैं .[self]
(defrule anchor3 
(declare (salience 0)) 
(id-root ?id anchor) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id sahArA_xe)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anchor.clp  anchor3  "  ?id " sahArA_xe)" crlf)) 
) 
