;@@@ Added by 14anu-ban-06  (20-11-2014)
;Because of its relative simplicity and ease with which it can be made use of , the question procedure , as compared to other forms of parliamentary procedure , is becoming increasingly popular with members of Parliament .(parallel corpus)
;प्रश़्नों की प्रक्रिया अपेक्षतया सरल और आसान होने के कारण , यह संसदीय प्रक्रिया के अऩ्य उपायों की तुलना में संसद सदस़्यों में बहुत ज्यादा  प्रिय होती जा रही है .(parallel corpus)
(defrule increasingly0
(declare (salience 0))
(id-root ?id increasingly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahuwa_jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increasingly.clp 	increasingly0   "  ?id "  bahuwa_jyAxA )" crlf))
)
;@@@ Added by 14anu-ban-06  (20-11-2014)
;Even elementary civil rights , the freedoms of thought , speech and association were increasingly violated and restricted .(parallel corpus)
;यहां तक कि अभिव्यक्ति और विचार की आधारभूत स्वतंत्रता और संगठन बनाने के अधिकार का तेजी से हनन किया जाने लगा , उसे सीमित किया जाने लगा .(parallel corpus)
;The levels of evidence (LOE) table has been increasingly used by many surgical journals .(COCA)
;(लोव) साक्ष्य के स्तर सारणी का बहुत शल्य चिकित्सा सम्बन्धी पत्रिकाओं के द्वारा तेजी से उपयोग किया जाता है . (manual)
(defrule increasingly1
(declare (salience 2000))
(id-root ?id increasingly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root ?id1 use|violate)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wejI_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increasingly.clp 	increasingly1   "  ?id "  wejI_se )" crlf))
)
;@@@ Added by 14anu-ban-06  (20-11-2014)
;Those formerly called catalogers are increasingly referred to as metadata librarians and similar labels.(COCA) 
;जो विगत काल में कैटलॊगर्ज कहे जाते हैं वो मेटाडेटा लाइब्रेरियन और समान लेबलों की तरह  ज्यादातर उल्लेख किए जाते हैं .(manual)
;Globally, rickettsioses are increasingly recognized as causes of undifferentiated fever.(COCA)
;व्यापक रूप से, रेकेटसियोस ज्यादातर एक बुखार के कारणों की तरह पहचाना जाता हैं . (manual)
(defrule increasingly2
(declare (salience 2000))
(id-root ?id increasingly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root ?id1 refer|recognize|realize)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jyAxAwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  increasingly.clp 	increasingly2   "  ?id "  jyAxAwara )" crlf))
)


