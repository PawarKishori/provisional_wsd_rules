
;@@@ added by 14anu22
(defrule untamed0
(declare (salience 5000))
(id-word ?id untamed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jaMgalI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  untamed.clp 	untamed0   "  ?id "  jaMgalI )" crlf))
)

;the untamed elephant killed the lion
;अनियंत्रिद हाथी ने सिंह को मार दिया
;@@@ added by 14anu22
;he is an untamed lion.
;वह एक अनियंत्रिदत सिंह है.
(defrule untamed1
(declare (salience 5100))
(id-word ?id untamed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aniyaMwriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  untamed.clp 	untamed1   "  ?id "  aniyaMwriwa )" crlf))
)

;this sentence is not related to this rule. by 14anu-ban-07, (18-12-2014);copied the sentence at untamed1 rule
;@@@ added by 14anu22
;the untamed elephant killed the lion
;अनियंत्रिद हाथी ने सिंह को मार दिया
(defrule untamed2
(declare (salience 5100))
(id-word ?id untamed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aniyaMwriwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  untamed.clp 	untamed2   "  ?id "  aniyaMwriwa )" crlf))
)

