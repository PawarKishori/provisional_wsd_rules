;@@@ Added by 14anu-ban-03 (10-03-2015)
;The cocoon of a caring family. [oald]
;एक परवाह करने वाले परिवार का सुरक्षा कवच . [manual]
(defrule cocoon2
(declare (salience 100))
(id-root ?id cocoon)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 family)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id surakRA_kavaca))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cocoon.clp 	cocoon2   "  ?id "  surakRA_kavaca )" crlf))
)

;-------------------Default Rules-------------------------

;"cocoon","N","1.koyA"
;Cocoon is made by larva insect.
(defrule cocoon0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (10-03-2015)
(id-root ?id cocoon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id koyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cocoon.clp 	cocoon0   "  ?id "  koyA )" crlf))
)

;"cocoon","V","1.xuniyA_se_alaga_rahanA"
;She cocooned herself in her own world.
(defrule cocoon1
(declare (salience 00))    ;salience reduced by 14anu-ban-03 (10-03-2015)
(id-root ?id cocoon)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xuniyA_se_alaga_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  cocoon.clp 	cocoon1   "  ?id "  xuniyA_se_alaga_raha )" crlf))
)

