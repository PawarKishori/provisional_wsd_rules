;@@@ Added by 14anu-ban-07,05-08-2014
;An upward gaze.(oald)
;एक ऊपर की ओर टकटकी.
(defrule upward0
(id-root ?id upward)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Upara_kI_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  upward.clp 	upward0   "  ?id "  Upara_kI_ora )" crlf))
)

;@@@ Added by 14anu-ban-07,05-08-2014
;An upward movement in property prices.(oald)
;संपत्ति की कीमतों में एक उर्ध्व आंदोलन. 
(defrule upward1
(declare (salience 1000))
(id-root ?id upward)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id urXva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  upward.clp 	upward1   "  ?id "  urXva )" crlf))
)

;@@@ Added by 14anu-ban-07,05-08-2014
;In approximately 15th century the construction of a five storey temple was done adjoining that stone , which goes upward gradually along with the height of the stone .(tourism corpus)
;लगभग 15वीं सदी में उसी चट्टान से सटाकर एक पाँच मंजिले मंदिर का निर्माण किया गया , जो क्रमशः चट्टान की ऊँचाई के साथ-साथ ऊपर उठता है ।
(defrule upward2
(declare (salience 1000))
(id-root ?id upward)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Upara_kI_waraPa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  upward.clp 	upward2   "  ?id "  Upara_kI_waraPa )" crlf))
)

;@@@ Added by 14anu-ban-07,05-08-2014
;One can reach the temple located on mountain through the road ascending straight upward .(tourism corpus)
;खड़ी  चढ़ाई  वाले  मार्ग  से  पर्वत  पर  स्थित  मंदिर  में  पहुँचा  जा  सकता  है  ।
(defrule upward3
(declare (salience 1000))
(id-root ?id upward)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root =(- ?id 1) straight)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1) KadZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " upward.clp	 upward3  "  ?id "  " =(- ?id 1)  " KadZI  )" crlf))
)


