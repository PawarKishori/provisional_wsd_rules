;commented by 14anu-ban-02(29-01-2015)
;correct meaning is coming from before6
;@@@ Added by 14anu03 on 16-june-2014
;You should have completed your work before time.
;आपको समय से पहले आपका कार्य पूरा करना चाहिये था .
;(defrule before100
;(declare (salience 5500))
;(id-root ?id before)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 time)
;(test (=(+ ?id 1) ?id1))
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 samaya_se_pahale))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  before.clp     ;before100   "  ?id "  " ?id1 "  samaya_se_pahale )" crlf))
;)

;commented by 14anu-ban-02(29-01-2015)
;correct meaning is coming from before6.
;@@@ Added by 14anu17
;The water in these springs is always above boiling point and before the 1905 earthquake , it is said that there jet of water rose to a ;height of six feet near the temple of Shri Rama , about a furlong away from its present location . 
;(defrule before16
;(declare (salience 4100))
;(id-root ?id before)
;?mng <-(meaning_to_be_decided ?id) 
;(or(id-cat_coarse =(+ ?id 2) number)(id-cat_coarse =(+ ?id 1) number))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id se_pahale))
;(if ?*debug_flag* then	
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before16   "  ?id "   se_pahale )" crlf))
;)

;$$$ Modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith)20-jan-2014
;He puts his work before everything .[oald]
;वह अपने कार्य को सबसे ऊपर रखता है
(defrule before0
(declare (salience 5000))
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(kriyA-before_saMbanXI  ?kri ?id1); added by Garima Singh
(id-word ?id1 everything|anything|something) ; added by Garima Singh
;(kriyA-object  ?kri ?obj);uncomment these lines if any conflict is found
;(id-word ?obj work);uncomment these lines if any conflict is found
;(id-word =(+ ?id 1) everything)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_Upara)) ; changed the meaning from 'ke_pahale' to 'se_Upara' by Garima Singh
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp 	before0   "  ?id "  se_Upara )" crlf))
)

;commented by 14anu-ban-02(29-01-2015)
;correct meaning is coming from before5
;$$$ Modified by 14anu02 on 4.6.14
;He stood before the king.
;वह राजा के सामने खड़ा था.
;(defrule before1
;(declare (salience 5000));salience changed by Garima Singh
;(id-root ?id before)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 2) throne|principal|judge|house|board|witnesses|chair|chairman|king)   ;changed (+ ?id 1) to (+ ?id 2) by 14anu02
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ke_sAmane))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp 	before1   "  ?id "  ke_sAmane )" crlf))
;)


;Added by Meena(1.4.10)
;She worked in a school for a while before joining the college . 
;I should have talked to you before the inviting of John . 
;I should have talked to you before inviting John . 
;(defrule before4
;(declare (salience 5000))
;(id-root ?id before)
;?mng <-(meaning_to_be_decided ?id)
;(or(kriyA-before_saMbanXI  ?id1 =(+ ?id 1))(kriyA-before_saMbanXI  ?id1 =(+ ?id 2)))
;;(kriyA-before_saMbanXI  ?id1 =(+ ?id 3)))
;(or (kriyA-object =(+ ?id 1)|=(+ ?id 2)  ?id2)(kqxanwa-of_saMbanXI  =(+ ?id 2) ?id2))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id se_pahale))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before4   "  ?id "   se_pahale )" crlf))
;)


;$$$ Modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;Added By Sheetal(11-08-10)
;Above rule commented as its very complicated(This rule works for all the above three sentences)
;She worked in a school for a while before joining the college . 
;I should have talked to you before the inviting of John . 
;I should have talked to you before inviting John .
(defrule before4
(declare (salience 5000));salience reduced by Garima Singh as salience of rule5 is decreased
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(kriyA-before_saMbanXI  ?kri ?id1) ; modified by Garima Singh
(id-cat_coarse ?id1 verb)   ;added by Garima Singh
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before4   "  ?id "   se_pahale )" crlf))
)

;$$$ Modified by 14anu-ban-02(05-01-2016)
;He had forgotten the note of that bell, and now its peculiar tinkle seemed to remind him of something and to bring it clearly before him.... (Crime And Punishment)
; वह उस घंटी की आवाज भूल चुका था. सो उसकी अजीब-सी टनटनाहट सुन कर अब उसे ऐसा लगा जैसे उसे कोई चीज याद आ गई हो और वही चीज साफ तौर पर उसके सामने आ गई हो(Crime And Punishment)

;$$$ Modified by 14anu-ban-02 (04-11-2014)
;restricted rule by adding (id-word ?kri laid)
;[COUNTER EXAMPLE]### Along the way, you will find out why blacksmiths heat the iron ring before fitting on the rim of a wooden wheel of a bullock cart and why the wind at the beach often reverses direction after the sun goes down.###
;[COUNTER EXAMPLE]###साथ साथ आप ज्ञात करेंगे क्यों लोहार तप्त करते हैं लोहे की रिंग चढाने के_पहले नेमि_पर  एक_ लकडी_के_पहिये_की एक_बैल_गाडी_के और क्यों हवा समुद्रतट_पर अक्सर उलटती है दिशा सूर्यास्त_ होने_के_बाद..###
;$$$ modified by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 02-jan-2014
;Then the make- believe King weighed the facts laid before him and passed judgment.
;उसके बाद राजा बने हुए लड़के ने उसके सामने रखे तथ्यों को तौला और अपना फैसला सुनाया।
;Added by Meena(30.3.10)
;The hare was once boasting of his speed before the other animals . 
(defrule before5
(declare (salience 4000));salience kept lower than rule4
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id) 
(pada_info (group_head_id ?id1)(preposition ?id))
;(or(kriyA-before_saMbanXI  ?id1 =(+ ?id 1))(kriyA-before_saMbanXI  ?id1 =(+ ?id 2))(kriyA-before_saMbanXI  ?id1 =(+ ?id 3))); commented by Garima Singh
(kriyA-before_saMbanXI  ?kri ?id1) ; added by Garima Singh
(id-root ?kri lay|stand|bring|lose);added by 14anu-ban-02 (04-11-2014)
                    ;'(id-word ?kri laid)' changed into (id-root ?kri lay|stand) and stand is added in the list by 14anu-ban-02(29-01-2015);bring and lose is added in the list by 14anu-ban-02(05-01-2016).
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAmane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before5   "  ?id "   ke_sAmane )" crlf))
)
;conflict-I reached the cinema hall before you.He arrived before me.To be discussed.

;commented by 14anu-ban-02(29-01-2015)
;correct meaning is coming from before5(run on parser 3)
;@@@ Added by 14anu02 on 4.6.14
;The lady made her child stand before the camera.
;महिला ने अपने बच्चे को कैमरे के सामने खड़ा कर दिया.
;(defrule before016
;(declare (salience 4999))
;(id-root ?id before)
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 stand|lay|sit|kneel|bend|put|couch|place|lie|lean)
;(kriyA-before_saMbanXI  ?id1 ?id2)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ke_sAmane))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp 	before016  "  ?id "  ke_sAmane )" crlf))
;)

;Commented by 14anu-ban-02(05-01-2016)
;correct meaning is coming from before5
;@@@ Added by 14anu02 on 4.6.14
;The lady stood before the camera.
;महिला कैमरे के सामने खडी थी.
;(defrule before015
;(declare (salience 5000))
;(id-root ?id before)
;?mng <-(meaning_to_be_decided ?id)
;(id-root =(- ?id 1) stand|lay|sit|kneel|bend|put|couch|place|lie|lean)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ke_sAmane))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp 	before015  "  ?id "  ke_sAmane )" crlf))
;)


;@@@ Added by Aditya and Hardik(25-06-2013),IIT(BHU) batch 2012-2017.
;I have seen him before.
;Have you done this before?
(defrule before7
(declare (salience 5000));salience changed by Garima Singh
(id-last_word ?id before)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp 	before7   "  ?id "  pahale )" crlf))
)

;$$$Modified by 14anu-ban-02(09-04-2016)
;###[COUNTER STATEMENT]Before I decide, I need time to reflect.[sd_verified]
;###[COUNTER STATEMENT]isase pahale ki mEM niScaya karawA hUz, muJe vicAra karane ke lie samaya kI AvaSyakawA hE.[sd_verified]
;@@@ Added by Garima Singh(M.Tech-C.S) 19-nov-2013
;She wanted to put her affairs in order before she died.[cambridge]
;वह मरने से पहले अपने कार्य को व्यवस्थित करना चाहती थी.
(defrule before8
(declare (salience 4998))      ;Salience changed by 14anu02 to avoid conflict with before5
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viBakwi ?id1 ?id)
(id-root ?id1 die)	;Added by 14anu-ban-02(09-04-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before8   "  ?id "  se_pahale )" crlf))
)

;commented by 14anu-ban-02(05-01-2016)
;meaning is coming from before5
;$$$Modified by 14anu-ban-02(29-01-2015)
;'(id-root ?id eye)' changed into (id-root ?id1 eye)
;Before Rajvir's astonished eyes the leaves lost their greenish colour and became brown.[gyananidhi]
;राजबीर की आँखो के सामने पत्तियाँ हरे रंग से गहरे भूरे रंग की हो गयी
;Commented by 14anu02 on 4.6.14
;conflict-I reached the cinema hall before you.He arrived before me.
;@@@ Added by Garima Singh(M.Tech-C.S) 16-dec-2013
;Before Rajvir's astonished eyes the leaves lost their greenish colour and became brown.[gyananidhi]
;राजबीर की आँखो के सामने पत्तियाँ हरे रंग से गहरे भूरे रंग की हो गयी
;(defrule before9
;(declare (salience 5500)); salience kept higher than rule 13
;(id-root ?id before)
;?mng <-(meaning_to_be_decided ?id)
;;(viSeRya-RaRTI_viSeRaNa  ?id1 ?);commented by Garima Singh 17-jan-2014 .conflict ex:Six months before his actual date of retirement, he decided to retire .Added a new rule before12 for the conflict 
;(kriyA-before_saMbanXI  ?kri ?id1)
;(id-root ?id1 eye)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id ke_sAmane))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before9   "  ?id "  ke_sAmane )" crlf))
;)

;$$$Modified by 14anu-ban-02(29-01-2015)
;Sidey stood before her with her head bowed.[oald]
;साईडी सिर झुकाकर उसके सामने खड़ी हो गयी।
;uncommented by 14anu-ban-02(29-01-2015)
;Commented by 14anu02 on 4.6.14
;@@@ Added by Garima Singh(M.Tech-C.S) 30-dec-2013
;Sidey stood before her with her head bowed.[oald]
;साईडी सिर झुकाकर उसके सामने खड़ी हो गयी।
(defrule before10
(declare (salience 5000))
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkya_viBakwi  ?id1 ?id)
(id-root ?id1 bow)  ;added by 14anu-ban-02(29-01-2015)
;(kriyA-samakAlika_kriyA  =(- ?id 1) ?id1)   ;commented by 14anu-ban-02(29-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_sAmane))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before10   "  ?id "  ke_sAmane )" crlf))
)

;@@@ Added by Garima Singh(M.Tech-C.S) 10-jan-2014
;Before he was 25 years old, he had established his position in the world of mathematical research. [oald]
;वह 25 वर्ष का था उससे पेहले ही उन्होने गणित संबंधी शोध की दुनिया में अपना सिक्का जमा लिया था।
(defrule before11
(declare (salience 5000));salience reduced by Garima Singh
(id-root 1 before)
?mng <-(meaning_to_be_decided 1)
(kriyA-vAkya_viBakwi  ?kri 1)
;(kriyA-subject  ?kri ?sub)
(id-word ?kri was)
;(id-word 1  before)
=>
(retract ?mng)
(assert (id-wsd_root_mng 1 usase_pahale_hI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before11   "  1 "  usase_pahale_hI )" crlf))
)

;$$$ Modified by 14anu-ban-02(05-01-2016)
;###[COUNTER STATEMENT] Before Rajvir's astonished eyes the leaves lost their greenish colour and became brown.[gyananidhi]
;###[COUNTER STATEMENT} ;राजबीर की आँखो के सामने पत्तियाँ हरे रंग से गहरे भूरे रंग की हो गयी [before 9]

;@@@ Added by Garima Singh(M.Tech-C.S) 17-jan-2014
;Six months before his actual date of retirement, he decided to retire .
;उसकी  सेवानिवृत्ति की वास्तविक तिथि से  छह महीने  पहले  , उसने इस्तीफा देने का फैसला लिया
(defrule before12
(declare (salience 5000)); salience reduced by Garima Singh.No conflicts found
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id1 ?)
(kriyA-before_saMbanXI  ?kri ?id1)
(id-root ?id1 date) ;Added by 14anu-ban-02(05-01-2016)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before12   "  ?id "  pahale)" crlf)
)
)
;note: se vibhakti should be added to word 'date'. To be discussed with sir.

;commented by 14anu-ban-02(05-01-2016)
;meaning is coming from before6
;@@@ Added by Garima Singh(M.Tech-C.S,Banasthali Vidyapith) 20-jan-2014
;Before the days of printing, our holy books were written on birch bark.
;(defrule before13
;(declare (salience 5000))
;(id-root 1 before)
;?mng <-(meaning_to_be_decided 1)
;(kriyA-before_saMbanXI  ?kri ?id1)
;(id-cat_coarse 1 preposition)
;;(id-root ?id1 day);uncomment this line if conflicts are found and reduce the salience of rule9 to 5000
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng 1 se_pahale))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before13   1  se_pahale)" crlf)
;)
;)

;$$$ Modified by Neha Arya(gurukul) 16-07-2014. --added 'sunrise,sunset'in the list
;The women working in the tea gardensreach the working place even before sunrise.[parallel corpus]
;बागानों  में  काम  करने  वाली  महिलाएँ  सूर्योदय  से  पूर्व  ही  कार्यस्थल  पर  पहुँच  जाती  हैं  ।
;@@@ Added by Garima Singh(M.Tech-C.S,Banasthali Vidyapith) 21-jan-2014
;Your name is before mine on the list.[oald]
;तुम्हारा नाम सूची में मेरे नाम से पहले है
;You are bound by the contract to pay before the end of the month.[oald]
;तुम महीने के अन्त से पहले भुगतान करने के लिये संविदा से बंधे हुए हो
(defrule before14
(declare (salience 5000))
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(kriyA-before_saMbanXI  ?kri ?id1)
(id-word ?id1 mine|yours|hers|end|sunrise|sunset);added 'end' in the list by Garima Singh 1-Feb-2014
;(id-root ?kri is) ; uncomment this line if any conflict is found
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before14   " ?id "  se_pahale)" crlf)
)
)

;@@@ Added by 14anu-ban-02 (04-11-2014)
;Along the way, you will find out why blacksmiths heat the iron ring before fitting on the rim of a wooden wheel of a bullock cart and why the wind at the beach often reverses direction after the sun goes down.[ncert]
;साथ साथ आप ज्ञात करेंगे क्यों लोहार तप्त करते हैं लोहे की रिंग चढाने के_पहले नेमि_पर  एक_ लकडी_के_पहिये_की एक_बैल_गाडी_के और क्यों हवा समुद्रतट_पर अक्सर उलटती है दिशा सूर्यास्त_ होने_के_बाद..[ncert]
(defrule before15
(declare (salience 4000))
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id) 
(kriyA-before_saMbanXI  ?kri ?id1) 
(id-word ?id1 fitting)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pahele))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before15   "  ?id "   ke_pehele )" crlf))
)


;@@@ Added by 14anu-ban-05 (10-09-2015)
;Memon then filed a Writ Petition before the Supreme Court as the issue of oral hearing of review petitions against death sentences was being heard by the Supreme Court.[wikipedia]
;मेमोन् ने तब उच्चतम न्यायालय के समक्ष रिट याचिका सूचना दर्ज की जब मृत्यु वाक्यों के विरुद्ध पुनरवलोकन निवेदनपत्र की मौखिक सुनवाई का विषय उच्चतम न्यायालय के द्वारा
; सुना जा रहा था .[manual]
(defrule before16
(declare (salience 4001))
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id)) 
(kriyA-before_saMbanXI  ?kri ?id1) 
(id-root ?kri file)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_samakRa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp    before16   "  ?id "   ke_samakRa )" crlf))
)

;*********************DEFAULT RULES*******************************************

;Salience reduced by Meena(30.3.10)
(defrule before6
(declare (salience 0));salience reduced by Garima Singh
;(declare (salience 4600))
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp 	before6   "  ?id "  se_pahale )" crlf))
)


;"before","Prep","1.ke_pahale"
;I had known her before you introduced her to me.
;--"2.ke_sAmane"
;They danced before me.
;--"3.ke_Age"
;


(defrule before2
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id conjunction)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id isase_pahale_ki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp 	before2   "  ?id "  isase_pahale_ki )" crlf))
)

;I had known her before you introduced her to me.
(defrule before3
(declare (salience 0));salience reduced by Garima Singh
(id-root ?id before)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pahale))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  before.clp 	before3   "  ?id "  ke_pahale )" crlf))
)

;"before","Adv","1.ke_pahale"
;I reached the cinema hall before you.



;************************EXAMPLES********************************

;Before the days of printing, our holy books were written on birch bark.
;I should have talked to you before inviting John .
;She worked in a school for a while before joining the college . 
;I should have talked to you before the inviting of John .
;I should have talked to you before inviting John .
;The hare was once boasting of his speed before the other animals .
;Then the make- believe King weighed the facts laid before him and passed judgment.
;I had known her before you introduced her to me.
;I reached the cinema hall before you.
;Then the make- believe King weighed the facts laid before him and passed judgment.
;I have seen him before.
;Have you done this before?
;She wanted to put her affairs in order before she died.
;Before Rajvir's astonished eyes the leaves lost their greenish colour and became brown.
;Sidey stood before her with her head bowed.
;Before he was 25 years old, he had established his position in the world of mathematical research.
;Six months before his actual date of retirement, he decided to retire .
