
(defrule stay0
(declare (salience 5000))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-up_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAge_raha));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " stay.clp stay0 " ?id "  jAge_raha )" crlf)) 
)

(defrule stay1
(declare (salience 4900))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 up)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jAge_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stay.clp	stay1  "  ?id "  " ?id1 "  jAge_raha  )" crlf))
)

;@@@ Added by 14anu-ban-11 on (02-12-2014)
;Management of dharamshala is also found for the stay of the people reaching Hastinapur.(toursim) 
;हस्तिनापुर पहुँचे लोगों के ठहरने के लिए धर्मशाला का भी प्रबंध मिलता है .(toursim)
(defrule stay4
(declare (salience 400))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 person)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Tahara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stay.clp 	stay4   "  ?id "  Tahara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (04-02-2015)
;Shepherds on this island complained that their wooden shoes (which had nails) at times stayed stuck to the ground.[NCERT corpus: modified-->changed 'struck' to 'stuck']
;इस द्वीप के गडरियों ने शिकायत की कि उनके लकडी के जूते (जिनमें कीलें लगी हुई थीं), कई बार जमीन से चिपक जाते थे.[NCERT corpus]
(defrule stay6
(declare (salience 4850))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) stuck)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) Pazsa_jA/cipaka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stay.clp 	stay6 "  ?id "  "(+ ?id 1) "  Pazsa_jA/cipaka_jA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (04-02-2015)
;He stayed stuck in trying to get an immigration bill passed.[COCA]
;वह एक आप्रवासी विपत्र पारित करवाने के प्रयास में अटका/फँसा रहा . [self]
(defrule stay7
(declare (salience 4900))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) stuck)
(kriyA-subject  ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) atakA_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " stay.clp 	stay7 "  ?id "  "=(+ ?id 1) "  atakA_raha )" crlf))
)

;@@@ Added by 14anu-ban-05 on (10-09-2015)
;On 1 June 2014, Justices J. Khehar and C. Nagappan imposed a stay of execution while a plea from Memon, that review of death penalties should be heard in an open court rather than in chambers, was heard by a constitution bench of the Supreme Court] which was then extended in December 2014.[wikipedia]
;1 जून  2014 को , न्यायमूर्ति जे खेहर और सी नागप्पन सुप्रीम ने प्राणदण्ड पर रोक लगाया जबकि मेमन से एक दलील ने, कि  मृत्यु दंड की पुनरवलोकन कक्षों में करने के बजाय खुली अदालत  में सुना जाना चाहिए, जिसे सुप्रीम कोर्ट की एक संविधान पीठ ने सुना था] जो दिसम्बर 2014 में तब बढाई गयी थी.[self]
;Memon then filed a mercy petition with the Governor of Maharashtra and then filed a Writ before the Supreme Court of India for a stay on his execution till the mercy petition is decided. [wikipedia]
;मेमोन् ने तब मॉहरॉश्ट्र के राज्यपाल के साथ दया याचिका  दर्ज की और बाद में उसके  फाँसी पर रोक के लिए भारत के उच्चतम न्यायालय के समक्ष रिट सूचना दर्ज की जब तक दया याचिका निश्चित की जाती है . 

(defrule stay8
(declare (salience 5001))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (viSeRya-of_saMbanXI  ?id ?id1)(viSeRya-on_saMbanXI  ?id ?id1))
(id-root ?id1 execution)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stay.clp 	stay8   "  ?id "  roka )" crlf))
)

;---------------------------- Default rules ---------------

;The trial was stayed and then the adoption was dismissed.
;I would make sure my baby stayed home and i talked to her.
;In the evenings she stayed home with us. 
;Interest rates have stayed low through these deficits.
;Added by Abhinav gupta,IIT BHU
(defrule stay3
(declare (salience 4850))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-cat ?id verb_past_participle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ruka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stay.clp 	stay3   "  ?id "  ruka )" crlf))
)

(defrule stay2
(declare (salience 4800))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stay.clp 	stay2   "  ?id "  raha )" crlf))
)

;default_sense && category=verb	raha	0
;"stay","V","1.rahanA"
;You have to stay in that cottage to save your life.
;
;

;@@@ Added by 14anu17
;For safety reasons , your baby is identified by two name labels during his or her stay in hospital .
(defrule stay5
(declare (salience 5000))
(id-root ?id stay)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TaharanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  stay.clp 	stay5   "  ?id "  TaharanA )" crlf))
)

