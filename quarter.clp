;@@@ Added by Shirisha Manju --- Suggested by Sukhada (4-7-14)
;I will meet you at a quarter past five. 
;mEM savA pAzca baje Apase milUzgA.
(defrule quarter0
(declare (salience 5000))
(id-root ?id quarter)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) after|past)
(pada_info (group_head_id ?h)(preposition  =(+ ?id 1)))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) savA))
(assert (id-wsd_viBakwi ?h baje))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " quarter.clp  quarter0  "  ?id "  " (+ ?id 1) "  savA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* " quarter.clp       quarter0  "  ?h "  baje )" crlf)
)

;@@@ Added by 14anu-ban-11 on (18-03-2015)
;The rent is due at the end of each quarter. (oald)
;किराया हर एक त्रिमास के अन्त में देय है . (self)
(defrule quarter2
(declare (salience 50))
(id-root ?id quarter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?id)
(id-root ?id1 end)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wrimAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quarter.clp  quarter2  "  ?id "  wrimAsa)" crlf) 
)
)

;@@@ Added by 14anu-ban-11 on (18-03-2015)
;The historic quarter of the city.(oald)
;शहर का ऐतिहासिक स्थान . (self)
(defrule quarter3
(declare (salience 60))
(id-root ?id quarter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 city)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quarter.clp  quarter3  "  ?id "  sWAna)" crlf) 
)
)


;@@@ Added by 14anu-ban-11 on (18-03-2015)
;We were moved to more comfortable living quarters.(oald)
;हम अधिक आरामदायक आवासों मे चले गये थे . (self)
(defrule quarter4
(declare (salience 70))
(id-root ?id quarter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 living)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  AvAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " quarter.clp  quarter4   "  ?id "  " ?id1 "  AvAsa)" crlf))
)


;@@@ Added by 14anu-ban-11 on (18-03-2015)
;She peeled and quartered an apple. (oald)
;उसने सेब छिला और चार टुकडे किया . (self)
(defrule quarter5
(declare (salience 150))
(id-root ?id quarter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 apple)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAra_tukadZe_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quarter.clp  quarter5  "  ?id "  cAra_tukadZe_kara)" crlf) 
)
)


;@@@ Added by 14anu-ban-11 on (18-03-2015)
;The soldiers were quartered in the town. (oald)
;सैनिक नगर में विशिष्ट स्थान पर तैनात किए गये थे .  (self)
(defrule quarter6
(declare (salience 160))
(id-root ?id quarter)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 town)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta_sWAna_para_wEnAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quarter.clp  quarter6  "  ?id "  viSiRta_sWAna_para_wEnAwa_kara)" crlf) 
)
)

;------------------------------- Default rule ---------------
;@@@ Added by Shirisha Manju --- Suggested by Sukhada (4-7-14)
;The theatre was about three quarters full.
;wiyetara lagaBaga wIna cOWAI BAga BarA huA WA
(defrule quarter1
(id-root ?id quarter)
?mng <-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cOWAI_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quarter.clp  quarter1  "  ?id "  cOWAI_BAga )" crlf) 
)
)
