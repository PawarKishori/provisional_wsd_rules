;@@@ Added by 14anu-ban-06 (26-03-2015)
;A man wearing an ordinary doublet and hose had followed the knights out of the castle.(COCA)
;साधारण कसी हुई जैकेट और पायजामा पहने हुए आदमी ने दुर्ग के बाहर योद्धाओं का अनुसरण किया था . (manual)
(defrule hose2
(declare (salience 5200))
(id-root ?id hose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 wear)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAyajAmA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hose.clp 	hose2   "  ?id "  pAyajAmA )" crlf))
)

;@@@ Added by 14anu-ban-06 (26-03-2015)
;I’ll just hose down the car.(OALD)
;मैं गाडी पर अभी पाइप से पानी डालूँगा .(manual) 
(defrule hose3
(declare (salience 5300))
(id-root ?id hose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 down)
(kriyA-object ?id ?)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pAipa_se_pAnI_dAla))
(assert (kriyA_id-object_viBakwi ?id para))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hose.clp	hose3  "  ?id "  " ?id1 "  pAipa_se_pAnI_dAla  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hose.clp       hose3   "  ?id " para )" crlf))
)


;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;$$$ Modified by 14anu-ban-06 (26-03-2015)
;A length of hose. (OALD);added by 14anu-ban-06 (26-03-2015)
;पाइप की लम्बाई . (manual)
(defrule hose0
(declare (salience 5000))
(id-root ?id hose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAipa));meaning changed from 'rabadZa_yA_plAstika_kI_nalI' to 'pAipa' by 14anu-ban-06 (26-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hose.clp 	hose0   "  ?id "  pAipa )" crlf))
)

;"hose","N","1.rabadZa_yA_plAstika_kI_nalI"
;bagIce meM pAnI pilAne ke kAma meM 'hose'(rabadZa kI nalI) kA upayoga kiyA jAwA hE. 
;

;$$$ Modified by 14anu-ban-06 (26-03-2015)
;Firemen hosed the burning car.(OALD)
;दमकल के सदस्यों ने जलती हुई गाडी पर पाइप से पानी डाला .(manual) 
(defrule hose1
(declare (salience 4900))
(id-root ?id hose)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAipa_se_pAnI_dAla));meaning changed from 'pAipa_se_pAnI_xe' to by 14anu-ban-06 (26-03-2015) 'pAipa_se_pAnI_dAla' 
(assert (kriyA_id-object_viBakwi ?id para));added by 14anu-ban-06 (26-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hose.clp 	hose1   "  ?id "  pAipa_se_pAnI_dAla )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hose.clp       hose1   "  ?id " para )" crlf));added by 14anu-ban-06 (26-03-2015)
)

;"hose","V","1.pAipa_se_pAnI_xenA"
;gereja meM gAdZiyoM ko 'hose'(pAipa se pAnI xekara) saPAI kI jAwI hE.
;
