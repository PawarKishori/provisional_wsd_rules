;------------------------------------------Default Rule---------------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on 31-07-2014
;3326:While counselling alone may suffice for mild cases , others would need both counselling - for patient , parents and teachers - and medication .[Karan Singla]
;हल्के लक्षणों में तो परामर्श से काम चल सकता है , गंभीर मामलं में दोनों की जरूरत पड़े सकती है - बच्चे को दवा की और माता - पिता तथा टीचर को परामर्श की . 
;[Karan Singla]

;4061:Sometimes there can be temporary anxiety or mild hallucinations.[Karan Singla]
;कभी - कभी अस्थायी चिंता और हल्के मति - भ्रम भी होते हैं ।[Karan Singla]

(defrule mild0
(declare (salience 0))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparitive)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id halkA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mild.clp 	mild0   "  ?id "  halkA )" crlf))
)
;-------------------------------------------------------------------------------------------------------------------------------
;@@@ Added by 14anu-ban-01 on 31-07-2014
;beginning in the 1870s , residents from northern states visited florida as tourists to enjoy the state s natural beauty and mild climate .[Karan Singla]
;1870 के दशक के प्रारंभ में , उत्तरी राज्यों के निवासी , पर्यटकों की तरह राज्य के प्राक्रितिक सौंदयर् और सुहावने मौसम का आनंद उठाने फ्लोरिडा आए .[Karan Singla]
(defrule mild1
(declare (salience 3000))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 climate)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suhAvanA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mild.clp 	mild1   "  ?id "  suhAvanA )" crlf))
)


;@@@ Added by 14anu-ban-01 on 31-07-2014
;55532:Mouth and feet of the affected animals should be washed three or four times daily with some mild astringent lotion .[Karan singla]
;रोग पीड़ित जानवरों के मुख और पांवों को प्रतिदिन दिन में 3 या 4 बार सौम्य ऐस्ट्रिंजेंट लोशन से धोया जाना चाहिए .[improvised manually]
(defrule mild2
(declare (salience 3000))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 lotion|shampoo)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sOmya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mild.clp 	mild2  "  ?id "  sOmya )" crlf))
)


;@@@ Added by 14anu-ban-01 on 31-07-2014
;59283:Asperger , which is just mild autism ?[Karan Singla]
;एस्परगर से ग्रसित व्यक्ति मे - जो मन्द औटिस्म का ही एक रूप है ?[Karan Singla]
(defrule mild3
(declare (salience 3000))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 autism|wind)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id maMxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mild.clp 	mild3 "  ?id "  maMxa )" crlf))
)


;@@@ Added by 14anu-ban-01 on 31-07-2014
;76645:Not to interfere with them legislatively is a mild way of saying that the orthodox will continue in every way as before and no change will be permitted .
;विधान सभाओं के जरिये उनमें दखल नहीं देने की बात का मतलब नर्मी से यह कहना है कि रूढ़िवादी लोग अपना सिलसिला पहले जैसा ही जारी रखेंगे और इसमें किसी तरह की तब़्दीली की इजाजत नहीं दी जायेगी .
(defrule mild4
(declare (salience 3000))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 way)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id narma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mild.clp 	mild4 "  ?id "  narma )" crlf))
)


;@@@ Added by 14anu-ban-01 on 31-07-2014
;She should be gentle and mild in temperament .[Karan Singla]
;स़्वभाव से उसे शाऩ्त व विनम्र होना चाहिए .[Karan Singla]
;58520:The only other members of the household were " his mild little wife " and her pet dog .
;इस परिवार के दूसरे सदस्य थे - उसकी विनम्र छोटी - सी पत्नी और उनका पालतू कुत्ता .
(defrule mild5
(declare (salience 3000))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 temperament|mild|boy|wife)
(test (> ?id1 ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vinamra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mild.clp 	mild5 "  ?id " vinamra )" crlf))
)


;@@@Added by 14anu-ban-01 on 31-07-14
;101826:Now , obviously I said this in a fit of mild irritation .
;देखिए , मैंने यह थोड़ा चिढ़कर कहा था .
(defrule mild6
(declare (salience 3000))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 irritation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mild.clp 	mild6 "  ?id "  WodA )" crlf))
)

;@@@ Added by 14anu-ban-08 (10-11-2014)
;Some people (0.6%[26] of the United States population) report that they experience mild to severe allergic reactions to peanut exposure; symptoms can range from watery eyes to anaphylactic shock, which can be fatal if untreated.    [agriculture]
;कुछ लोग (यूनाइटड स्टॆट्स की जनसङ्ख्या का 0.6 %) शिकायत करते हैं कि वे मूँगफली खाने पर मृदु से गंभीर प्रत्यूर्ज प्रतिक्रियाओं का अनुभव करते हैं;लक्षण आँखों से पानी बहने  से लेकर तीव्रगाहिता संबंधी आघात तक हो सकते हैं जो कि अनुपचारित रहें तो घातक हो सकते हैं    [human translation]
(defrule mild7
(declare (salience 3001))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id ?id1)
(id-root ?id1 reaction)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mqxu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  mild.clp 	mild7 "  ?id "  mqxu )" crlf))
)
; @@@ Added by 14anu23 on 30/6/14
;A mild woman, who never shouted. [oxfordlearnersdictionary]
;एक विनम्र स्त्री, जिसने कभी नहीं  चिल्लाया . 
(defrule mild8
(declare (salience 0))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id vinamra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mild.clp       mild8   "  ?id "  vinamra )" crlf))
)


; @@@ Added by 14anu23 on 30/6/14
;It's safe to take a mild sedative. [oxfordlearnersdictionary]
;एक सौम्य उपशामक लेना सुरक्षित है . 
(defrule mild9
(declare (salience 5000))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 soap|sedative|shampoo)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id sOmya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mild.clp       mild9   "  ?id "  sOmya )" crlf))
)


;; @@@ Added by 14anu23 on 30/6/14
;Use a soap that is mild on the skin. [oxfordlearnersdictionary]
;एक ऐसे साबुन का उपयोग कीजिए जो त्वचा पर नरम है . 
(defrule mild10
(declare (salience 5000))
(id-root ?id mild)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-on_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id narama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  mild.clp       mild10   "  ?id "  narama )" crlf))
)
