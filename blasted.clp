;@@@Added by 14anu-ban-02(26-03-2015)
;Sentence: Make your own blasted coffee![oald]	
;Translation: अपनी बेकार कॉफी खुद बनाइए!  [self]
(defrule blasted1 
(declare (salience 100)) 
(id-root ?id blasted) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-viSeRaNa  ?id1 ?id) 
(id-root ?id1 coffee)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id bekAra)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blasted.clp  blasted1  "  ?id "  bekAra )" crlf)) 
) 

;-------------------default_rules----------------------------------
;@@@Added by 14anu-ban-02(26-03-2015)
;Sentence: A blasted heath.[cald]	;run the sentence on parser no. 6
;Translation: एक उजड़ा हुआ झाड़ियों से भरा भूखण्ड . [self]
(defrule blasted0 
(declare (salience 0)) 
(id-root ?id blasted) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id ujadZA_huA)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blasted.clp  blasted0  "  ?id "  ujadZA_huA )" crlf)) 
) 
