;$$$ Modified by Sonam Gupta MTech IT Banasthali 10-2-2014 (added carry in ?id1)
;$$$ Modified by Sonam Gupta MTech IT Banasthali 10-2-2014 (added fly and leave in ?id1)
;Plane flies. [rajpal]
;विमान उड़ा .
;$$$ Modified by Sonam Gupta MTech IT Banasthali 2013 
;They threatened to blow up the plane if their demands were not met. [Gyannidhi]
;उन्होंने विमान धमाके के कारण टुकडे-टुकडे होने के लिये धमकाया यदि उनकी माँगें पूरी नहीँ हुई . 
(defrule plane0
(declare (salience 5000))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 hijack|blow|fighter|passenger|transport|ticket|crash|catch|take|land|board|pass|fly|leave|carry)
(or(kriyA-subject ?id1 ?id)(kriyA-object  ?id1 ?id)(viSeRya-viSeRaNa  ?id1 ?id)(viSeRya-viSeRaNa  ?id ?id1))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane0   "  ?id "  vimAna )" crlf))
)


;$$$ Modified by Sonam Gupta MTech IT Banasthali 10-2-2014 (added (kriyA-on_saMbanXI  ? ?id))
;To get on the plane. [rajpal]
;विमान पर चढना .
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;We'll be boarding the plane in about 20 minutes. [Cambridge]
;हम लगभग 20 मिनटों में विमान चढ रहे होंगे . 
(defrule plane1
(declare (salience 4900))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(or(viSeRya-in_saMbanXI  ?id ?)(kriyA-by_saMbanXI  ? ?id)(kriyA-on_saMbanXI  ? ?id))
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane1   "  ?id "  vimAna )" crlf))
)

;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;The poet's treatment of the subject lifts it to a mystical plane. [Cambridge]
;विषय का कवि का इलाज एक रहस्यवादी स्तर तक इसको उठाता है . 
(defrule plane2
(declare (salience 4800))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or(kriyA-to_saMbanXI  ? ?id)(viSeRya-from_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane2   "  ?id "  swara )" crlf))
)


(defrule plane3
(declare (salience 4700))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samawala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane3   "  ?id "  samawala )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 10-2-2014
;Plane of thoughts. [Rajpal]
;विचारों का स्तर .
(defrule plane4
(declare (salience 5000))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id swara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane4   "  ?id "  swara )" crlf))
)






;default_sense && category=noun	havAI-jahAjZa	0
;"plane","N","1.havAI-jahAjZa"
;Aeroplanes are filled with gasoline.
;--"2.wala"
;A vertical plane was drawn.
;--"3.ranxA"
;The carpenter made the surface of the wood smooth by a plane.
;--"4.canAra"
;Planes have broad leaves && thin bark.
;
;


;@@@ Added by 14anu01
;And was out on a plane to Moscow within an hour.
;और एक दिन में मास्को को बाहर जहाज पर था . 
(defrule plane5
(declare (salience 5000))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-word =(+ ?id 1) to)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jahAja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane5   "  ?id "  jahAja )" crlf))
)

;NOTE:- Parser problem is there in the sentence.'plane' is an adjective but parser is treating it as a noun. The rule is working fine on parser no. 62.
;@@@ Added by 14anu-ban-09 on (05-11-2014)
;The unit for plane angle is radian with the symbol rad and the unit for the solid angle is steradian with the symbol sr.  [NCERT CORPUS]
;samawalIya koNa kA mAwraka rediyana hE jisakA prawIka @rad hE evaM Gana koNa kA mAwraka sterediyana hE jisakA prawIka @sr hE. [NCERT CORPUS]

(defrule plane7
(declare (salience 4900))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 angle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samawalIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane7   "  ?id "  samawalIya )" crlf))
)

;@@@ Added by 14anu-ban-09 on (12-02-2015)
;The planes were on a bombing mission. 		[Hinkhoj.com]
;विमान बमबारी करने के लक्ष्य पर थे.			[Manual] 

(defrule plane8
(declare (salience 4900))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(kriyA-on_saMbanXI  ?id1 ?id2)
(id-root ?id2 mission)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane8   "  ?id "  vimAna )" crlf))
)

;@@@ Added by 14anu-ban-09 on (24-02-2015)
;I caught the next plane to Dublin. 	[oald]
;मैंने डब्लिन के लिये अगला विमान पकडा . 	[self]

(defrule plane9
(declare (salience 5200))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
(kriyA-to_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id2 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane9   "  ?id "  vimAna )" crlf))
)

;@@@ Added by 14anu-ban-09 on (24-02-2015)
;Planes have broad leaves and thin bark. 	[same CLP file]
;चनार में चौड़ी पत्तियाँ और पतला छाल होता हैं . 	  	[self]

(defrule plane10
(declare (salience 5200))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(kriyA-object  ?id1 ?id2)
(id-root ?id2 bark|leaf)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id canAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane10   "  ?id "  canAra )" crlf))
)

;@@@ Added by 14anu-ban-09 on (24-02-2015)
;The carpenter made the surface of the wood smooth by a plane.	[same CLP file]
;बढई ने रन्दे से लकडी की सतह को चिकना बनाया . 	  	[self]

(defrule plane11
(declare (salience 5200))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-by_saMbanXI  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 carpenter)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ranxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane11   "  ?id "  ranxA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (24-02-2015)
;The North American B-25B Mitchell medium bomber was the most suitable plane for the mission.	[coca]
;उत्तरी अमरीकी B-25B मिचल माध्यम विस्फोटकर्ता लक्ष्य के लिए अति योग्य विमान था .  	  	[self]

(defrule plane12
(declare (salience 5200))
(id-root ?id plane)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI ?id ?id1)
(id-root ?id1 mission)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  plane.clp 	plane12   "  ?id "  vimAna )" crlf))
)




