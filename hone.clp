;@@@ Added by 14anu-ban-06 (12-02-2015)
;The knife had been honed to razor sharpness.(OALD)
;चाकू रेजर तीक्ष्णता तक पैना किया गया था . (manual)
(defrule hone0
(declare (salience 0))
(id-root ?id hone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pEnA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hone.clp 	hone0   "  ?id "  pEnA_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (12-02-2015)
;She honed her debating skills at college.(OALD)
;उसने कालेज में अपने वाद विवाद की क्षमता को प्रखर किया . (manual)
(defrule hone1
(declare (salience 2000))
(id-root ?id hone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 skill|talent)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id praKara_kara))
(assert  (id-wsd_viBakwi   ?id1  ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hone.clp 	hone1   "  ?id "  praKara_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  hone.clp      hone1   "  ?id1 " ko )" crlf)
)
)
