;$$$ Modified by 14anu-ban-04 (11-12-2014)        ;changed meaning from 'dUbA' to  'dUba_jA'    
;@@@ Added by 14anu26  [18-06-14]
;The sun dipped below the horizon.
;सूरज क्षितिज के नीचे डूब गया .           ;'डूब' spelling is corrected by 14anu-ban-04 (16-12-2014)
;सूरज क्षितिज के नीचे डुब गया .
(defrule dip2
(declare (salience 5100))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?sub) 
(not(kriyA-object ?id ?))              ;added by 14anu-ban-04 (11-12-2014) 
(id-root ?sub sun)       
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dUba_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dip.clp 	dip2   "  ?id "  dUba_jA)" crlf))
)

;$$$ Modified by Shirisha Manju Suggested by Chaitanya Sir
;@@@ Added by 14anu26  [18-06-14]
;The productivity has dipped.
;उत्पादकता कम हुई है.
(defrule dip3
(declare (salience 5000))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?sub)   ;added by Shirisha Manju 19-12-14   
(id-root ?sub productivity) ;added by Shirisha Manju 19-12-14
(not(kriyA-object ?id ?))   ;added by Shirisha Manju 19-12-14
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dip.clp 	dip3   "  ?id "  kama_ho )" crlf))
)

;@@@ Added by 14anu-ban-04 on 20-08-2014
;There is full fervour of the fair this time also in Garh Mukteshwar and huge crowd of devotees is gathering for the holy dip .  [tourism-corpus]
;इस  बार  भी  गढ़  मुक्तेश्वर  में  मेले  की  भरपूर  गहमागहमी  है  और  पवित्र  डुबकी के  लिए  श्रद्धालुओं  की  भारी  भीड़  जुट  रही  है  ।    [tourism-corpus]
(defrule dip4
(declare (salience 10))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 holy)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dubakI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dip.clp 	dip4   "  ?id "  dubakI )" crlf))
)

;@@@ Added by 14anu02 on 21.6.14
;The land dips to the south.
;भूमि दक्षिण की ओर ढालू होती है . 
(defrule dip5
(declare (salience 5000))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 land)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DAlU_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dip.clp 	dip5   "  ?id " DAlU_ho )" crlf))
)

;@@@ Added by 14anu-ban-04  (04-02-2015)
;A brief dip into history serves to confirm this view.             [oald]
;इतिहास में  एक सरसरी नजर डालना  इस विचार की पुष्टि करने में मदद करता है.              [self]
(defrule dip6
(declare (salience 20))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-into_saMbanXI ?id ?id1)
(pada_info (group_head_id ?id1)(preposition ?id2))
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  ?id2 sarasarI_najZara_dAla))
(assert (make_verbal_noun ?id))
(assert (id-H_vib_mng ?id1 meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  dip.clp      dip6  "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  dip.clp     dip6  "  ?id1 "  meM )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dip.clp	dip6  "  ?id "  "  ?id2 "  sarasarI_najZara_dAla )" crlf))
)

;@@@ Added by 14anu-ban-04 on (04-02-2015)
;She dipped into her purse and took out some coins.            [oald]
;उसने  अपने पर्स में हाथ डाला और कुछ सिक्के बाहर निकाले.                      [self]
(defrule dip7
(declare (salience 4810))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?id1)
(pada_info (group_head_id ?id1)(preposition ?id2))
=>
(retract ?mng)
(assert (id-H_vib_mng ?id1 meM))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2  meM_hAWa_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  dip.clp     dip7  "  ?id1 "  meM )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dip.clp	dip7  "  ?id "  " ?id2 "  meM_hAWa_dAla )" crlf))
)


;@@@ Added by 14anu-ban-04 on (04-02-2015)
;I have only had time to dip into the report.            [oald]
;मेरे पास केवल रिपोर्ट में सरसरी नजर डालने का  समय है .                   [self]
(defrule dip8
(declare (salience 4810))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?id1)
(id-root ?id1 book|magazine|report)
(pada_info (group_head_id ?id1)(preposition ?id2))
=>
(retract ?mng)
(assert (id-H_vib_mng ?id1 meM))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2  sarasarI_najZara_dAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  dip.clp     dip8  "  ?id1 "  meM )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dip.clp	dip8  "  ?id "  " ?id2 "  sarasarI_najZara_dAla )" crlf))
)

;@@@ Added by 14anu-ban-04 on (04-02-2015)
;I had to dip into my savings to pay for the repairs.             [cald]
;दुरुस्ती का भुगतान करने  के लिए मुझे  मेरी बचत  में से   पैसा निकालना पडा .                [self]
(defrule dip9
(declare (salience 4810))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI ?id ?id1)
(id-root ?id1 saving)
(pada_info (group_head_id ?id1)(preposition ?id2))
=>
(retract ?mng)
(assert (id-H_vib_mng ?id1 meM_se))
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 pEsA_nikAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  dip.clp     dip9  "  ?id1 "  meM_se )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " dip.clp	dip9 "  ?id "  " ?id2 "  pEsA_nikAla )" crlf))
)


;-------------------------- Default rules -------------------
(defrule dip0
(declare (salience 0)) ;salience is reduced by 14anu-ban-04 on 20-08-2014
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dubAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dip.clp 	dip0   "  ?id "  dubAva )" crlf))
)

;"dip","N","1.dubAva"
;There was a slight dip in onion price this week.
;

;$$$ Modified by 14anu02  
;I dipped my pen into the ink. 
;मैंने स्याही के अन्दर मेरी कलम डुबाई . 
(defrule dip1
(declare (salience 4900))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?) ;added by 14anu02
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dubA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dip.clp 	dip1   "  ?id "  dubA )" crlf))
)

;"dip","V","1.dubAnA"
;I dipped my pen into the ink.
;--"2.kama_honA"
;The value of rupee has dipped an all time low.
;--"3.nIce_ko_AnA"
;The land dips to the south.
;

;@@@ Added by 14anu02 on 20.6.14
;The sun dipped below the horizon.
;सूरज क्षितिज के नीचे डूबा . 
(defrule dip02
(declare (salience 4800))
(id-root ?id dip)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id dUba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dip.clp 	dip02   "  ?id "  dUba )" crlf))
)


