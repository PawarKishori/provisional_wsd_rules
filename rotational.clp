;@@@ Added by Anita-13-06-2014
;The shifts work on a rotational basis. [cambridge dictionary]
;पाली फेर-बदल आधार पर कार्य करती है ।
(defrule rotational1
(declare (salience 5100))
(id-root ?id rotational)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 basis)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pera_baxala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotational.clp 	rotational1   "  ?id " Pera_baxala )" crlf))
)

;@@@ Added by 14anu-ban-10 on (19-08-2014)
;We shall, in the present chapter, consider rotational motion about a fixed axis only.  
;isa aXyAya meM hama eka sWira akRa ke pariwaH hone vAlI GUrNI gawi kA hI aXyayana karefge.
;इस अध्याय में हम एक स्थिर अक्ष के परितः होने वाली घूर्णी गति का ही अध्ययन करेङ्गे.
(defrule rotational2
(declare (salience 2000))
(id-root ?id rotational)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GUrNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotational.clp 	rotational2   "  ?id " GUrNI)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (19-08-2014)
(defrule rotational0
(declare (salience 0000))
(id-root ?id rotational)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cakarIya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rotational.clp 	 rotational0 "  ?id "  cakarIya )" crlf))
)
