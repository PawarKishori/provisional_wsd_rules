;@@@ Added by 14anu-ban-03 (10-04-2015)
;The congress will vote on the proposals tomorrow. [oald]
;काङ्ग्रेस कल इन प्रस्तावों पर मत देगी . [manual]
(defrule congress0
(declare (salience 00))
(id-root ?id congress)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kAfgresa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  congress.clp 	congress0   "  ?id " kAfgresa )" crlf))
)

;@@@ Added by 14anu-ban-03 (10-04-2015)
;An international congress of trades unions. [oald]
;व्यापार मेल का अन्तर्राष्ट्रीय सम्मेलन . [manual]
(defrule congress1
(declare (salience 10))
(id-root ?id congress)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sammelana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  congress.clp 	congress1   "  ?id " sammelana )" crlf))
)
