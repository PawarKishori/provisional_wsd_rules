
(defrule intimate0
(declare (salience 5000))
(id-root ?id intimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwarafga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intimate.clp 	intimate0   "  ?id "  aMwaraMga )" crlf))
)

(defrule intimate1
(declare (salience 4900))
(id-root ?id intimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GaniRTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intimate.clp 	intimate1   "  ?id "  GaniRTa )" crlf))
)

;default_sense && category=verb	bawalA	0
;"intimate","VT","1.bawalAnA"
;He intimated his willingness to take part in the function.

;$$$ Modified by 14anu-ban-06 (02-12-2014)
;@@@ Added by 14anu11
;इसी तरह यह भी लाजिमी है कि यह नया हिंदुस्तान विज्ञान की दुनिया के साथ गहरा ताल्लुक रखे .
;It is also essential that new India should also come in intimate contact with the world of science .
(defrule intimate2
(declare (salience 6000))
(id-root ?id intimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaharA))  ;spelling corrected from 'ghrA' to 'gaharA' by 14anu-ban-06 (02-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intimate.clp 	intimate2   "  ?id "  gaharA )" crlf))
)

;
;
;@@@ Added by 14anu-ban-06 (02-12-2014)
;It is said that when the jail authorities intimated him that the time for his hanging has come , he told -'Wait , A revolutionary is meeting another one .(parallel corpus)
;कहा जाता है कि जब जेल के अधिकारियों ने उन्हें सूचना दी कि उनके फाँसी का वक्त आ गया है तो उन्होंने कहा - रुको एक क्रांतिकारी दूसरे से मिल रहा है ।(parallel corpus)
;The Indian police were intimated and they began to look for him all over the country .(parallel corpus)
;भारतीय पुलिस को सूचना दे दी गई जिसने पूरे देशमें उनकी तलाश शुरू कर दी .(parallel corpus)
(defrule intimate3
(declare (salience 6000))
(id-root ?id intimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 authority|police)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUcanA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intimate.clp 	intimate3   "  ?id "  sUcanA_xe )" crlf))
)

;@@@ Added by 14anu-ban-06 (02-12-2014)
;After receiving a positive verdict in Barabanki case , Amitabh Bachchan intimated to Maharashtra government that he did not wish to surrender his land in Maval tehsil of Pune district.(parallel corpus)
;बाराबंकी मामले में अपने पक्ष में सकारात्मक फैसला सुनने के बाद बच्चन ने महाराष्ट्र सरकार को सूचित किया कि पुणे जिले की मारवल तहसील में वे अपनी जमीन का आत्मसमर्पण करने के लिए तैयार नहीं हैं ।(parallel corpus)
(defrule intimate4
(declare (salience 6000))
(id-root ?id intimate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-to_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sUciwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  intimate.clp 	intimate4   "  ?id "  sUciwa_kara )" crlf))
)
