;@@@Added by 14anu-ban-02(27-02-2015)
;Sentence: Our anticipated arrival time is 8.30.[oald]	;run the sentence on parser no.2
;Translation: हमारा पूर्वानुमानित आगमन का समय  8.30 है.[self]
(defrule anticipated0 
(declare (salience 0)) 
(id-root ?id anticipate) 
?mng <-(meaning_to_be_decided ?id)
(id-word ?id anticipated) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id pUrvAnumAniwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anticipated.clp  anticipated0  "  ?id "  pUrvAnumAniwa )" crlf)) 
) 

;@@@Added by 14anu-ban-02(27-02-2015)
;The eagerly anticipated movie will be released next month.[oald]	;run the sentence on parser no.2
;उत्सुकतापूर्वक अपेक्षित  फिल्म अगले महीने प्रदर्शित हो जायेगी.[self]
(defrule anticipated1 
(declare (salience 100)) 
(id-root ?id anticipate) 
?mng <-(meaning_to_be_decided ?id)
(id-word ?id anticipated) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 movie|film)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id apekRiwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  anticipated.clp  anticipated1  "  ?id "  apekRiwa )" crlf)) 
) 
