;@@@ Added by 14anu-ban-06 (09-02-2015)
;An incendiary bomb. (OALD)
;दाहक बम . (manual)
(defrule incendiary0
(declare (salience 0))
(id-root ?id incendiary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAhaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incendiary.clp 	incendiary0   "  ?id "  xAhaka )" crlf))
)

;@@@ Added by 14anu-ban-06 (09-02-2015)
;Begala had noticed her incendiary remarks .(COCA)
;बेगॉल ने उसकी उत्तेजक टिप्पणियों पर ध्यान दिया था .(manual)
(defrule incendiary1
(declare (salience 2000))
(id-root ?id incendiary)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 remark)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwejaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  incendiary.clp 	incendiary1   "  ?id "  uwwejaka )" crlf))
)
