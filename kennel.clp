;########################################################################
;#  Copyright (C) 2014-2015 14anu26 (noopur.nigam92@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;########################################################################
;@@@ Addded by 14anu26  [26-06-14]
;Put a dog in a kennel. 
;कुत्ता घर में कुत्ते को रखिए .
(defrule kennel0
(declare (salience 0))
(id-root ?id kennel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuwwA_Gara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kennel.clp 	kennel0   "  ?id " kuwwA_Gara  )" crlf))
)

;@@@ Addded by 14anu26  [26-06-14]
;His breeding kennels have insured that this breed will be perpetuated.
;उसके बच्चे पैदा करने वाले शिकारी कुत्तों के  झुण्ड स्पष्ट कर चुके हैं कि यह नस्ल स्थायी बन जाएगी .  
(defrule kennel1
(declare (salience 100))
(id-root ?id kennel)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) breeding)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SikArI_kuwwoM_ke_JuMda ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  kennel.clp 	kennel1   "  ?id "  SikArI_kuwwoM_ke_JuMda )" crlf))
)

;@@@ Addded by 14anu26  [26-06-14]
;The dogs have been kennelled.
;कुत्ते कुत्ता घर में रख दिये गये है. 
(defrule kennel2
(declare (salience 200))
(id-root ?id kennel)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id kennelled)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kuwwA_Gara_meM_raKa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  kennel.clp 	kennel2   "  ?id "  kuwwA_Gara_meM_raKa )" crlf))
)

