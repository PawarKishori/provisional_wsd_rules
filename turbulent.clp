;@@@ Added by 14anu-ban-07, 20-08-2014
;a turbulent crowd. (oald)
;एक अशांत भीड़.
(defrule turbulent0
(declare (salience 0))
(id-root ?id turbulent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vikRubXa aSAMwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  turbulent.clp 	turbulent0   "  ?id "  aSAMwa  )" crlf))
)

;@@@ Added by 14anu-ban-07, 20-08-2014
;In a turbulent flow the velocity of the fluids at any point in space varies rapidly and randomly with time. (ncert corpus) 
;विक्षुब्ध प्रवाह में किसी बिंदु पर तरल का वेग द्रुत तथा यादृच्छिक रूप से समय में बदलता रहता है.
;Osborne Reynolds (1842-1912) observed that turbulent flow is less likely for viscous fluid flowing at low rates. 
;ऑस्बेर्न रेनल्ड्स (1842 - 1912) ने यह प्रेक्षण किया कि लघु दरों पर प्रवाहित होने वाले श्यान तरलों के लिए प्रक्षुब्ध (विक्षुब्ध) प्रवाह की संभावना कम होती है. 
(defrule turbulent1
(declare (salience 1000))
(id-root ?id turbulent)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 flow)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 vikRubXa_pravAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " turbulent.clp 	turbulent1   "  ?id "  " ?id1 " vikRubXa_pravAha )" crlf))
)

