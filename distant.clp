;@@@ Added by 14anu-ban-04 (05-03-2015)
;My father was a distant, austere man.                  [oald]
;मेरे पिता एक अलग,सख्त आदमी थे .                               [self]
(defrule distant1
(declare (salience 30))
(id-root ?id distant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distant.clp 	distant1  "  ?id "  alaga )" crlf))
)


;@@@ Added by 14anu-ban-04 (05-03-2015)
;She seemed cold and distant.                   [cald]
;वह  गंभीर और विमुख प्रतीत हुई .                         [self]
(defrule distant2
(declare (salience 20))
(id-root ?id distant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vimuKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distant.clp 	distant2  "  ?id "  vimuKa)" crlf))
)


;@@@ Added by 14anu-ban-04 (05-03-2015)
;A distant relative of mine passed away this month.                  [hinkhoj]
;मेरा एक  दूर का रिश्तेदार   इस महीने गुजर गया .                                         [self]
(defrule distant3
(declare (salience 30))
(id-root ?id distant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 relative|cousin|aunt|uncle)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distant.clp 	distant3  "  ?id "  xUra_kA )" crlf))
)

;------------------------------------------------------------default rule ------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (05-03-2015)
;At some point in the distant future I would like to have my own house.            [oald]
;दूर भविष्य में किसी   समय पर मैं मेरे पास  अपना घर होना पसन्द करूँगा .                                    [self]
(defrule distant0
(declare (salience 10))
(id-root ?id distant)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distant.clp 	distant0  "  ?id " xUra)" crlf))
)
