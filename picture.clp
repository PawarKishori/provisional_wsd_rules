
(defrule picture0
(declare (salience 5000))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture0   "  ?id "  ciwra )" crlf))
)

;"picture","N","1.ciwra"
;He painted a beautiful picture.
;--"2.PZoto"
;Her birthday pictures were really nice.
;--"3.sinemA_yA_telivijZana_kA_parxA"
;Our TV has a 45 cm picture.
;--"4.PZilma"
;This is the debut picture of leonardo-di-vinci.
;--"5.varNana"
;Our teacher gave us a vivid picture of  World war-II.
;--"6.mAnasika_ciwra"
;My brother's description helped the police to build up an accurate picture of what had happened.
;--"7.manohara_vaswu"
;My dad was a picture on the day of his anniversary.
;
(defrule picture1
(declare (salience 4900))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kalpanA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture1   "  ?id "  kalpanA_kara )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2103
;I've got a much clearer picture of what's happening now. [Cambridge]
;मेरे पास एक बहुत अधिक स्पष्ट मानस चित्र है अब क्या हो रहा है . 
(defrule picture2
(declare (salience 5500))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?)
(viSeRya-det_viSeRaNa  ?id ?)
(kriyA-object  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnasa_ciwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture2   "  ?id "  mAnasa_ciwra )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2103
;I've got a much clearer picture of what's happening now. [Cambridge]

(defrule picture3
(declare (salience 5500))
(id-word ?id pictures)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-to_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id calaciwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture3   "  ?id "  calaciwra )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-03-2015)
;In the classical picture of an atom, the electron revolves round the nucleus much like the way a planet revolves round the sun. [NCERT CORPUS]
;परमाणु के क्लासिकी चित्रण में, इलेक्ट्रॉन नाभिक के चारों ओर ठीक ऐसे ही परिक्रमा करता है जैसे कि सूर्य के चारों ओर ग्रह परिक्रमा करते हैं.		[NCERT CORPUS]
(defrule picture5
(declare (salience 5500))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 classical)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwraNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture5   "  ?id "  ciwraNa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-03-2015)
;He was often pictured as an outsider. [same clp file]
;वह पराये के रूप में अक्सर चित्रित किया गया था . [manual]
(defrule picture6
(declare (salience 5500))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI  ?id ?id1)
(id-root ?id1 outsider)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ciwriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture6   "  ?id " ciwriwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-03-2015)
;Her birthday pictures were really nice.	[same clp file]
;उसके जन्मदिन फ़ोटो वास्तव में अच्छे थे .			[manual] 
(defrule picture7
(declare (salience 5500))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 birthday)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZoto))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture7   "  ?id "  PZoto )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-03-2015)
;Our TV has a 45 cm picture. 		[same clp file]
;हमारे टेलीविज़न का पर्दा 45 सेंटिमीटर है .  	[manual] 
(defrule picture8
(declare (salience 5500))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
(kriyA-object  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id2 TV|cinema)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture8   "  ?id "  parxA )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (03-03-2015)
;Our teacher gave us a vivid picture of  World war-II.	[same clp file]
;हमारे शिक्षक ने हमें वर्ल्ड वार-II का एक स्पष्ट वर्णन दिया . 		[manual] 
(defrule picture9
(declare (salience 5500))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 war-II)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id varNana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture9   "  ?id "  varNana )" crlf)
)
)

;@@@ Added by 14anu-ban-09 on (03-03-2015)
;This is the debut picture of leonardo-di-vinci.	[same clp file]
;यह leonardo-di-vinci की पहली फिल्म है . 			[manual] 
(defrule picture10
(declare (salience 5500))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
(id-root ?id1 debut)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZilma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture10   "  ?id "  PZilma )" crlf))
)

;@@@ Added by 14anu-ban-09 on (03-03-2015)
;My dad was a picture on the day of his anniversary. 	[same clp file]
;मेरे पिता उसकी वर्षगाँठ के दिन पर मनोहर वस्तु थी . 			[manual] 
(defrule picture11
(declare (salience 5500))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 anniversary)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manohara_vaswu))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  picture.clp 	picture11   "  ?id "  manohara_vaswu )" crlf))
)


;@@@ Added by 14anu-ban-09 on (28-07-2014)
;And along with it take one binoculars and if possible a camera with good zoom to capture pictures. [Parallel Corpus]
;और साथ ही एक दूरबीन और संभव हो तो अच्छे जूम वाला कैमरा , तस्वीरें कैद करने के लिए ।
;Thekkady is the most appropriate place to capture the pictures of wild elephants. [Parallel Corpus]
;जँगली  हाथियों  का  तस्वीरें कैद करने के लिए  तेक्कडि  सबसे  उपयुक्त  स्थान  है  ।

(defrule picture4
(declare (salience 5000))
(id-root ?id picture)
?mng <-(meaning_to_be_decided ?id)
(id-word = (- ?id 2) capture)
(id-cat_coarse ?id noun)
;(id-cat_coarse ?id1 verb)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id (- ?id 2 ) wasvIra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  picture.clp 	picture4   "  ?id " "- ?id 2" wasvIra )" crlf))
)

;"picture","V","1.kalpanA_karanA"
;I tried to picture how our India will be in 21st century.
;--"2.ciwriwa_karanA/ciwra_xvArA_praxarSiwa_karanA"
;He was often pictured as an outsider.
;The fairy was pictured against a background of bright moon && twinkling stars.
;
