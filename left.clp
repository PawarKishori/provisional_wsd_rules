;$$$ Modified by 14anu02 on 1.07.14
;$$$---Modified by Nandin(14-4-14)
;File modified by Meena (22.8.09)
;Take the first left.   ;Added eng-sentence
;प्रथम बाँया मोड़ लीजिए . 
(defrule left0
(declare (salience 5000))
(id-root ?id left)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-cat_coarse ?id noun)
;(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bAzyA_modZa))	;change from 'bAzyeM_modza' to 'bAzyA_modZa' by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  left.clp  	left0   "  ?id " bAzyA_modZa )" crlf))
)

(defrule left1
(declare (salience 4900))
(id-root ?id left)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp 	left1   "  ?id "  bAzyA )" crlf))
)



(defrule left2
(declare (salience 4800))
(id-root ?id left)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp 	left2   "  ?id "  bAzyA )" crlf))
)

;$$$ Modified by 14anu02 on 1.07.14
;@@@----Added by Nandini(14-4-14)
;The Left only has a small chance of winning power.
(defrule left3
(declare (salience 4950))
(id-root ?id left|Left)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa ?id ?id2)		;change from '2' to '?id' and '1' to 'id2' by 14anu02
(id-word ?id2 the)		;added by 14anu02 
(or(kriyA-subject ? ?id)(kriyA-object ? ?id))	;added by 14anu02
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vAma_xala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp 	left3   "  ?id "  vAma_xala )" crlf))
)

;$$$ Modified by 14anu02 on 1.07.14
;@@@----Added by Nandini(14-4-14)
;She was sitting on my left.(old)
;vaha merI bAzyIM ora para bETI WI.
(defrule left4
(declare (salience 5050))
(id-root ?id left)
?mng <-(meaning_to_be_decided ?id)
(kriyA-on_saMbanXI  ?id1 ?id)
;(id-root ?id1 sit|stand|walk|run)	;commented by 14anu02 to make it a general rule
(id-cat_coarse ?id noun)
=>
(retract ?mng)
;(assert (id-wsd_word_mng ?id bAzyIM_ora)) ;commented by 14anu02 
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 2) bAzyIM_ora)) ;added by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " left.clp	left4  "  ?id "  " (- ?id 2) "  bAzyIM_ora  )" crlf))
)

;$$$ Modified by 14anu02 on 1.07.14
;Counter ex-Twist your body to the left, then to the right.(old)
;अपना शरीर बाँयीं ओर मोड़िए फिर दाई ओर.
;@@@----Added by Nandini(14-4-14)
;To the left of the library is the bank.[olad]
;puswakAlaya ki bAzyIM ora bEMka  hE.
(defrule left5
(declare (salience 5080))
(id-root ?id left)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI  ? ?id)	;added by 14anu02
;(viSeRya-of_saMbanXI  ?id ?id1)	;commented by 14anu02
;(id-root ?id1 library|house)	;commented by 14anu02
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 2) bAzyIM_ora)) ;added by 14anu02
;(assert (id-wsd_word_mng ?id bAzyIM_ora)) ;commented by 14anu02
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " left.clp	left5  "  ?id "  " (- ?id 2) "  bAzyIM_ora  )" crlf))
)

;Modified by Meena(2.3.10);added (or(kriyA-object ?id ?id2)(kriyA-in_saMbanXI  ?id ?id2))
;He left in the morning.
;I left the party after seeing Ann there . 
;(defrule left3
;(declare (salience 4300))
;(id-root ?id leave)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id left )
;(kriyA-subject ?id ?id1)
;(or(kriyA-object ?id ?id2)
;(kriyA-in_saMbanXI  ?id ?id2)(kriyA-kriyA_viSeRaNa  ?id ?id2))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id calA_jA))
;(assert (id-H_vib_mng ?id yA))
;(assert (kriyA_id-object_viBakwi ?id se))  ;added by Meena(2.3.10)
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp    ;left3   "  ?id "   calA_jA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* "  left.clp    left3   ;"  ?id "  yA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   ;left.clp       left3   "  ?id " se )" crlf)
;)
;)




;Added by Meena (22.08.09)
;He left all his money to the orphanage.
;(defrule left4
;(declare (salience 4300))
;(id-root ?id leave)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object ?id ?id1)
;(id-root ?id1 money|wealth|house|land|property|business)
;(kriyA-to_saMbanXI  ?id ?id2)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id CodZa_jA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp      ;left4   "  ?id "  CodZa_jA )" crlf)
;)
;)



;The train left on time.
;(defrule left5
;(declare (salience 4200))
;(id-root ?id leave)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 train|bus)
;(kriyA-subject ?id ?id1)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id CUta))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp      ;left5   "  ?id "  CUta )" crlf)
;)
;)


;Modified by Meena(3.3.10)
;I say it is a damn shame that he left . 
;(defrule left6
;(declare (salience 4000))
;(id-root ?id leave)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id left )
;(kriyA-subject ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id calA_jA))
;(assert (id-H_vib_mng ?id yA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp    ;left6  "  ?id "   calA_jA )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng   " ?*prov_dir* "  left.clp    left6   ;"  ?id "  yA )" crlf)
;)
;)

;Added by Aditya and Hardik(5.6.13),IIT(BHU)
;I was left alone.
;We are left behind.
;(defrule left7
;(declare (salience 4100))
;(id-root ?id leave)
;?mng <-(meaning_to_be_decided ?id)
;(id-root = (- ?id 1) be|am) 
;(kriyA-subject ?id ?id1)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id CUta))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp 	;left7   "  ?id "  CUta )" crlf))
;)

;"left","Adj","1.bAzyA"
;Some people write with their left hand. 
;--"2.vAma paMWa"
;The left parties.
;
;
;@@@ Added by 14anu-ban-03 Yashwini Dhaka(18-07-2014)
;There was half an hour left for the last entry. [tourism corpus]
;AKirI prveSa ke lie sirPa AXA GNtA bacA WA. 
(defrule left8
(declare (salience 5050))
(id-root ?id left)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa ?id ?id1)
(id-root ?id1 hour|money)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bacA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  left.clp  left8  " ?id "  bacA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (20-10-2014)
;###COUNTER EXAMPLE The proofs of the above equations are left to you as an exercise.(NCERT)
;###उपरोक्त समीकरणों की व्युत्पत्ति आपके लिए अभ्यास हेतु छोडी जा रही है.(NCERT)
;@@@ Added by 14anu-ban-10 on (13-8-14)
; If we climb straight stairs then we go out of breath , but left to right to left , if we climb crooked like this then no problem will be there . 
;सीढ़ियाँ  यदि  सीधी  चढ़ें  तो  साँस  फूल  जाती  हैं  ,  लेकिन  बाँयें  से  दाँयें  से  बाँयें  ,  इस  तरह  तिरछे  चढ़ते  रहते  तो  कोई  तकलीफ  नहीं  होती  है  ।
(defrule left9
(declare (salience 5900))
(id-root ?id leave)
?mng <-(meaning_to_be_decided ?id)
(kriyA-to_saMbanXI ?id ?id1)
(id-root ?id1 right);added by 14anu-ban-06 (20-10-2014)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp 	left9   "  ?id "  bAzyA )" crlf))
)

;@@@ Added by 14anu-ban-08 (05-12-2014)
;If the number is less than 1, the zero (s) on the right of decimal point but to the left of the first non-zero digit are not significant.  [NCERT]
;यदि कोई सङ्ख्या 1 से छोटी है तो वे शून्य जो दशमलव के दाईं ओर पर प्रथम शून्येतर अङ्क के बाईं ओर हों, सार्थक अङ्क नहीं होते.    [NCERT]
(defrule left10
(declare (salience 5057))
(id-root ?id left)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(pada_info (group_head_id ?id1)(preposition $? ?id2 $?)) 
(id-root ?id2 of)
(id-root ?id1 digit|decimal)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 bAIM_ora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng    " ?*prov_dir* "  left.clp 	left10   "  ?id " "  ?id2 " bAIM_ora )" crlf))
)

;@@@Added by 14anu18 (30-06-14)
;The person on the left is a football player.
;बाँयी तरफ पर व्यक्ति एक फुटबॉल खिलाडी है . 
(defrule left_10
(declare (salience 5000))
(id-root ?id left)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id ?)
(viSeRya-on_saMbanXI ? ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzyI_waraPa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  left.clp 	left_10   "  ?id "  bAzyI_waraPa )" crlf))
)
