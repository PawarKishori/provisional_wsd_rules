;@@@ Added by 14anu-ban-06 30-7-14
;According to Panikar Travel , the Indians go to far off places from North to extreme South and East to West in the form of pilgrimage tours .(Parallel Corpus)
;पानिकर ट्रैवल के अनुसार , तीर्थ यात्राओं के रूप में भारतीय उत्तर से धुर दक्षिण तक और पूरब से पश्चिम ,से दूर स्थानों के लिए जाते हैं ।
(defrule far2
(declare (salience 5100))
(id-root ?id far)
?mng <-(meaning_to_be_decided ?id)
(id-root =(+ ?id 1) off)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) se_xUra ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " far.clp	 far2  "  ?id "  " =(+ ?id 1)  " se_xUra   )" crlf))
)

;@@@ Added by 14anu-ban-05 Prajna Jha on 31.07.2014
;So far, we have not asked the question as to what governs the motion of bodies. 
;aba waka hamane yaha praSna nahIM pUCA hE ki piNdoM kI gawi kA kyA kAraNa hE ?
(defrule far3
(declare (salience 5100))
(id-root ?id far)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA_viSeRaNa-kriyA_viSeRaNa_viSeRaka ?id1 ?id)
(id-root ?id1 so)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id aba_waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  far.clp  far3   "  ?id "    " ?id1 " aba_waka)" crlf))
)

;@@@ Added by 14anu-ban-05 on (04-02-2015)
;Further meetings seem pointless.[wordreference forum]
;आगे की बैठक व्यर्थ लग रहे हैं.		[manual]
(defrule far4
(declare (salience 5101))
(id-word ?id further)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 meeting)		;more constraints can be added
=>
(retract ?mng)
(assert (id-eng-src  ?id further  Word_mng))
(assert (id-wsd_word_mng ?id Age))
(assert  (id-wsd_viBakwi   ?id  kI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  far.clp   far4   "  ?id " Age)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  far.clp   far4   "  ?id1 " kI )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-eng_src   " ?*prov_dir* "  far.clp   far4  "  ?id "  "   further  " Word_mng )" crlf))
)

;---------------- Default rules -------------------------
(defrule far0
(declare (salience 5000))
(id-root ?id far)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  far.clp 	far0   "  ?id "  xUra )" crlf))
)

;"far","Adj","1.xUra"
;She stood at the far end of the street.
;
(defrule far1
(declare (salience 4900))
(id-root ?id far)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  far.clp 	far1   "  ?id "  xUra )" crlf))
)

;@@@Added by 14anu18 (01-07-14)
;This task is far easier than the previous one.
;यह काम पिछले वाले की अपेक्षा कहीं ज्यादा आसान है .
(defrule far1_1
(declare (salience 4900))
(id-root ?id far)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(viSeRya-viSeRaka ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kahIM_jyAxA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  far.clp 	far1_1   "  ?id "  kahiM_jzAxa )" crlf))
)


;"far","Adv","1.xUra"
;How far is the post-office from here.
;--"2.bahuwa"
;Murali has fallen far behind his schedule.
;
