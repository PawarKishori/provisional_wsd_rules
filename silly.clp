
(defrule silly0
(declare (salience 5000))
(id-root ?id silly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bevakUPZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silly.clp 	silly0   "  ?id "  bevakUPZa )" crlf))
)

;$$$Modified by 14anu-ban-01 on (02-01-2015)
;I was rather silly to let the strangers in who robbed me.[silly.clp]
;मैं निःसन्देह ही अल्पबुद्धि था जो मैंने अजनबियों को अन्दर अाने दिया जिन्होंने मुझे लूट लिया.[self]
(defrule silly1
(declare (salience 5100));salience increased from 4900 to 5100 by 14anu-ban-01 on (02-01-2015)
(id-root ?id silly)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ?id ?);Added by 14anu-ban-01 on (02-01-2015)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpabuxXi));corrected "alpabuxxi" to "alpabuxXi"  by 14anu-ban-01 on (02-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silly.clp 	silly1   "  ?id "  alpabuxXi)" crlf))
)

;"silly","Adj","1.alpabuxxi"
;I was rather silly to let the strangers in who robbed me.
;Stop playing silly games, you are old enough to be serious.
;
;

;@@@ Added by 14anu20 on 02.07.2014
;We had to wear these silly little hats.
;हमें ये अव्यावहारिक छोटी टोपियाँ पहननी पडीं .
(defrule silly23_1
(declare (salience 5000))
(id-root ?id silly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 hat|cap|dress|shoe|shirt|skirt|pant|earing)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id avyAvahArika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silly.clp 	silly23_1   "  ?id "  avyAvahArika )" crlf))
)

;$$$Modified by 14anu-ban-01 on (02-01-2015)
;@@@ Added by 14anu20 on 02.07.2014
;I feel silly in these clothes. :run this on parser no. 5
;मैं इन वस्त्रों में मूर्खतापूर्ण महसूस करता हूँ . 
(defrule silly23_2
(declare (salience 5000))
(id-root ?id silly)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(viSeRya-in_saMbanXI  ?id ?);added by  14anu-ban-01 on (02-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mUrKawApUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silly.clp 	silly23_2   "  ?id "  mUrKawApUrNa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (02-01-2015)
;Stop playing silly games, you are old enough to be serious.[silly.clp]
;मूर्खतापूर्ण/बेवकूफा़ना खेल खेलना बन्द करो,तुम बड़े हो गये हो. [self]
(defrule silly2
(declare (salience 5000))
(id-root ?id silly)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 game|talk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bevakUPAnA/mUrKawApUrNa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  silly.clp 	silly2  "  ?id "  bevakUPAnA/mUrKawApUrNa )" crlf))
)
