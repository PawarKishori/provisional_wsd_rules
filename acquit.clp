;@@@Added by 14anu-ban-02(23-02-2015)
;Sentence: The jury acquitted him of murder.[oald]
;Translation: निर्णायक समिति ने उसको खून के अपराध से मुक्त किया.[self]
(defrule acquit1
(declare (salience 100)) 
(id-root ?id acquit) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-object  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id aprAXa_se_mukwa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  acquit.clp  acquit1  "  ?id "  aprAXa_se_mukwa_kara )" crlf)) 
) 

;@@@Added by 14anu-ban-02(23-02-2015)
;Sentence: She was acquitted on all charges.[oald]
;Translation: वह सभी आरोपों से बरी की गयी थी .  [self]
(defrule acquit2
(declare (salience 100)) 
(id-root ?id acquit) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-on_saMbanXI  ?id ?id1) 
(id-root ?id1 charge|ground)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id barI_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  acquit.clp  acquit2  "  ?id "  barI_kara )" crlf)) 
)

;------------------------ Default Rules ---------------------- 

;@@@Added by 14anu-ban-02(23-02-2015)
;Sentence: Both defendants were acquitted.[oald]
;Translation:   दोनों मुल्जि़म रिहा किए गये थे . [self]
(defrule acquit0
(declare (salience 0))
(id-root ?id acquit)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rihA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  acquit.clp  acquit0  "  ?id "  rihA_kara )" crlf))
)

