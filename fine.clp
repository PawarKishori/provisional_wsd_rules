
(defrule fine1
(declare (salience 4900))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 impose)
(kriyA-object ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jurmAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fine.clp 	fine1   "  ?id "  jurmAnA )" crlf))
)


(defrule fine3
(declare (salience 4700))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 tune)
(kriyA-tune_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNa_vivaraNa_meM_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " fine.clp fine3 " ?id "  pUrNa_vivaraNa_meM_jA )" crlf)) 
)

(defrule fine4
(declare (salience 4600))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 tune)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrNa_vivaraNa_meM_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " fine.clp	fine4  "  ?id "  " ?id1 "  pUrNa_vivaraNa_meM_jA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-02-2015)
;He was lucky to get away with only a fine.[OALD]
;वह भाग्यशाली था कि केवल जुर्माने से छूट गया.   [manual] 

(defrule fine6
(declare (salience 5000))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-with_saMbanXI  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jurmAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fine.clp 	fine6   "  ?id "  jurmAnA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-02-2015)
;It was his finest hour as manager of the England team. [OALD]
;इंग्लैंड टीम के प्रबंधक के रूप में उसके लिए सबसे अच्छा समय  था.		[manual]

(defrule fine7
(declare (salience 6000))
(id-word ?id finest)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-word =(+ ?id 1) hour)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_word_mng ?id (+ ?id 1) sabase_acCA_samaya ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_word_mng   " ?*prov_dir* " fine.clp  fine7  "  ?id "  " (+ ?id 1) "  sabase_acCA_samaya   )" crlf))
)

;@@@ Added by 14anu-ban-05 on (25-02-2015)
;She has inherited her mother's fine features.[OALD]
;उसे अपनी मां की सुन्दर  मुखाकृति विरासत में मिला है.	[MANUAL]

(defrule fine8
(declare (salience 5001))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ? ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sunxara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fine.clp 	fine8   "  ?id "  sunxara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (26-02-2015)
;The tombs of Shahjahan and Mumtaz are surrounded by fine meshes .[tourism]
;SAhajahAz Ora mumawAja ke makabare cAroM waraPa se mahIna jAliyoM se Gire hEM .[tourism]
;Fine mesh work in the strong annals of Jehangir Palace , beautiful domes and stone provides the whole architecture an extraordinary wealth .[tourism]
;jahAzgIra mahala kI majabUwa prAcIreM , sunxara CawriyAz Ora pawWara meM mahIna jAliyoM kA kAma mahala ke samUce sWApawya ko asAXAraNa vEBava praxAna karawA hE .[tourism]
(defrule fine9
(declare (salience 5002))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 mesh|work)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mahIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fine.clp 	fine9   "  ?id "  mahIna )" crlf))
)

;@@@ Added by 14anu-ban-05 on (26-02-2015)
;Fine stream of water can be seen in Sonbhadra .[tourism]
;sonaBaxra meM bArIka jala kI XArA xeKI jA sakawI hE .[tourism]

(defrule fine10
(declare (salience 5003))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 stream|engrave|carve)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bArIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fine.clp 	fine10   "  ?id "  bArIka )" crlf))
)


;------------------------ Default Rules ----------------------

;default_sense && category=verb	jurmAnA_lagA	0
;"fine","V","1.jurmAnA_lagAnA"
;The judge made the accused pay a fine for not accepting summons from the court.
(defrule fine5
(declare (salience 4500))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jurmAnA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fine.clp 	fine5   "  ?id "  jurmAnA_lagA )" crlf))
)

(defrule fine2
(declare (salience 4800))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fine.clp 	fine2   "  ?id "  baDZiyA )" crlf))
)

(defrule fine0
(declare (salience 5000))
(id-root ?id fine)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDZiyA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fine.clp 	fine0   "  ?id "  baDZiyA )" crlf))
)
 
