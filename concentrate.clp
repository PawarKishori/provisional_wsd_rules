
(defrule concentrate0
(declare (salience 5000))
(id-root ?id concentrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAMxra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concentrate.clp 	concentrate0   "  ?id "  sAMxra )" crlf))
)

(defrule concentrate1
(declare (salience 4900))
(id-root ?id concentrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XyAna_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concentrate.clp 	concentrate1   "  ?id "  XyAna_xe )" crlf))
)

;"concentrate","V","1.XyAna_xenA"
;Nowadays I can't concentrate in my studies.
;--"2.samAhqwa_karanA"
;These groups concentrate in the inner cities
;
;

;@@@ Added by 14anu-ban-03 on (01-10-2014)
;The force of attraction between a hollow spherical shell of uniform density and a point mass situated outside is just as if the entire mass  of the shell is concentrated at the center of the shell. [NCERT-CORPUS]
;किसी एकसमान घनत्व के खोखले गोलीय खोल तथा खोल के बाहर स्थित किसी बिन्दु द्रव्यमान के बीच आकर्षण बल ठीक - ठाक उतना ही होता है जैसा कि खोल के समस्त द्रव्यमान को उसके केन्द्र पर सङ्केन्द्रित मान कर ज्ञात किया जाता है. [ncert-corpus]
(defrule concentrate2
(declare (salience 5000))
(id-root ?id concentrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI ?id ?id1)
(id-word ?id1 center)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkenxriwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concentrate.clp 	concentrate2   "  ?id "  safkenxriwa_kara )" crlf))
)



;@@@ Added by 14anu-ban-03 (02-03-2015)
;According to this the entire positive charge and most of the mass of the atom is concentrated in a small volume called the nucleus with electrons revolving around the nucleus just as planets revolve around the sun. [ncert]
;isake anusAra, kisI paramANu kA kula XanAveSa waWA aXikAMSa xravyamAna eka sUkRma Ayawana meM safkeMxriwa howA hE jise nABika kahawe hEM Ora isake cAroM ora ilektroYna usI prakAra parikramA karawe hEM jEse sUrya ke cAroM ora graha parikramA karawe hEM.[ncert]
(defrule concentrate3
(declare (salience 5000))
(id-root ?id concentrate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id safkenxriwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  concentrate.clp 	concentrate3   "  ?id "  safkenxriwa_ho )" crlf))
)




