
(defrule rival0
(declare (salience 5000))
(id-root ?id rival)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawixvanxI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rival.clp 	rival0   "  ?id "  prawixvanxI )" crlf))
)

;"rival","N","1.prawixvanxI"
;They are business rivals.
;
(defrule rival1
(declare (salience 4900))
(id-root ?id rival)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id barAbarI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rival.clp 	rival1   "  ?id "  barAbarI_kara )" crlf))
)

;"rival","VT","1.barAbarI_karanA"
;She is a genius, no one can rival her.

;@@@ Added by 14anu-ban-10 on (10-11-2014)
;New research shows peanuts rival the antioxidant content of many fruits. [agriculture domain]
;नए शोध से पता चलता है कि मूँगफली बहुत फलों का ऐन्टीऑक्सडन्ट प्रतिद्वंद्वी  तत्व  है .[manual]
(defrule rival2
(declare (salience 5100))
(Domain agriculture)
(id-root ?id rival)
?mng <-(meaning_to_be_decided ?id) 
(kriyA-subject  ?id ?id1)
(id-root ?id1 peanut)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawixvaMxvI))
(assert (id-domain_type ?id agriculture))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rival.clp 	rival2   "  ?id " prawixvaMxvI)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type   " ?*prov_dir* "  rival.clp 	rival2   "  ?id " agriculture)" crlf))
)
