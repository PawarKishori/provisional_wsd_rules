
;@@@ Added by 14anu-ban-01 on (20-02-2015)
;A substantial chunk of our profits. [cald]
;हमारे लाभ का एक बड़ा हिस्सा . [manual]
(defrule substantial2
(declare (salience 4900))
(id-root ?id substantial)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 chunk|piece|part)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  substantial.clp 	substantial2   "  ?id "  badA )" crlf))
)


;@@@ Added by 14anu-ban-01 on (20-02-2015)
;He lives in a street of substantial houses.[self:with reference to oald]
;वह बड़े और ठोस घरों वाली गली में रहता है . [self]
(defrule substantial3
(declare (salience 4900))
(id-root ?id substantial)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 house|villa|apartment|building|mansion|castle)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA_Ora_Tosa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  substantial.clp 	substantial3   "  ?id "  badA_Ora_Tosa )" crlf))
)

;@@@ Added by 14anu-ban-01 on (20-02-2015)
;The committee were in substantial agreement.[cald]
;समितियाँ  पर्याप्त/(काफी हद तक) सहमति में थीं . [self]
(defrule substantial4
(declare (salience 4900))
(id-root ?id substantial)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 agreement|consent|concordance|compliance)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paryApwa/kAPI_haxa_waka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  substantial.clp 	substantial4  "  ?id " paryApwa/kAPI_haxa_waka )" crlf))
)

;------------------------ Default Rules ----------------------

(defrule substantial0
(declare (salience 5000))
(id-root ?id substantial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sArWaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  substantial.clp 	substantial0   "  ?id "  sArWaka )" crlf))
)

;"substantial","Adj","1.acCI mAwrA meM"
;There was substantial improvement in his health.
(defrule substantial1
(declare (salience 4900))
(id-root ?id substantial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCI_mAwrA_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  substantial.clp 	substantial1   "  ?id "  acCI_mAwrA_meM )" crlf))
)

