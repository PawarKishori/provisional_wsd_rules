;@@@Added by 14anu-ban-02(12-03-2015)
;Sentence: The children were banished from the dining room.[oald]
;Translation: बच्चों को भोजन कक्ष से निकाल दिया गया था . [self]
(defrule banish0 
(declare (salience 0)) 
(id-root ?id banish) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id verb) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nikAla_xe)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  banish.clp  banish0  "  ?id "  nikAla_xe )" crlf)) 
) 


;@@@Added by 14anu-ban-02(12-03-2015)
;He was banished for life.[mw]
;वह जीवनभर के लिए निर्वासित किया गया था . [self]
(defrule banish1 
(declare (salience 100)) 
(id-root ?id banish) 
?mng <-(meaning_to_be_decided ?id) 
(kriyA-for_saMbanXI  ?id ?id1)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id nirvAsiwa_kara)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  banish.clp  banish1  "  ?id "  nirvAsiwa_kara )" crlf)) 
) 
