

;$$$ Modified by 14anu-ban-09 on (12-03-2015)
;NOTE:- Changed meaning from 'gupwa_jAnakArI_ke_saMbanXiwa' to 'gupwa_jAnakArI'.
;We are not privy to the actual plans that our robber makes, so we have no idea how he avoids security cameras and opens combination safes.	[Report set 1.]
;हमारे डाकू बनाते है कि हमे वास्तविक योजनाओं की गुप्त जानकारी नहीं हैं, इसलिए हमें कोई नहीं जानकारी है वह सुरक्षा कैमरों से बचे और संयोग सॆफ्स खोलता है .  [manual]

(defrule privy0
(declare (salience 5000))
(id-root ?id privy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gupwa_jAnakArI)) ;changed meaning from 'gupwa_jAnakArI_ke_saMbanXiwa' to 'gupwa_jAnakArI' by 14anu-ban-09 on (12-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  privy.clp 	privy0   "  ?id "  gupwa_jAnakArI )" crlf))	;changed meaning from 'gupwa_jAnakArI_ke_saMbanXiwa' to 'gupwa_jAnakArI' by 14anu-ban-09 on (12-03-2015)
)

;"privy","Adj","1.gupwa_jAnakArI_ke_saMbanXiwa"
;Ram was privy to the illegal negotiations.
;
(defrule privy1
(declare (salience 4900))
(id-root ?id privy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SOcagqha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  privy.clp 	privy1   "  ?id "  SOcagqha )" crlf))
)

;"privy","N","1.SOcagqha"
;Voluntery organizations are building privies in slum areas.
;
