
;@@@ Added by 14anu-ban-11 on (17-02-2015)
;She scorned the efforts of ameteur singers. (oald)
;उसने ameteur गायकों के प्रयासों का तिरस्कार किया .(self) 
(defrule scorn0
(declare (salience 30))
(id-root ?id scorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 effort)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wiraskAra_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scorn.clp 	scorn0   "  ?id "  wiraskAra_kara)" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi " ?*prov_dir* "  scorn.clp    scorn0  "  ?id " kA )" crlf))
)

;@@@ Added by 14anu-ban-11 on (17-02-2015)
;She scorned for his proposal to marry him. (oald)
;उसने उससे विवाह करने के उसके प्रस्ताव को ठुकरा दिया .
(defrule scorn3
(declare (salience 40))
(id-root ?id scorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI  ?id ?id1)
(id-root ?id1 proposal)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TukarA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scorn.clp 	scorn3   "  ?id "  TukarA_xe)" crlf))
)

;@@@ Added by 14anu-ban-11 on (18-02-2015)
;Baby's scorn was blistering.(coca)
;बच्चे की हँसी  बहुत तेज़ थी .(self) 
(defrule scorn4
(declare (salience 50))
(id-root ?id scorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id1 ?id)
(id-root ?id1 blister)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hazsI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scorn.clp 	scorn4   "  ?id "  hazsI)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (17-02-2015)
;She was unable to hide the scorn in her voice.(oald)
;वह उसकी आवाज में घृणा छिपाने मे असमर्थ थी . [self] 
(defrule scorn1
(declare (salience 20))
(id-root ?id scorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GqNA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scorn.clp 	scorn1   "  ?id "  GqNA)" crlf))
)


;@@@ Added by 14anu-ban-11 on (17-02-2015)
;She scorned their views as old-fashioned.(oald)
;उसने उनके विचारो का मज़ाक उड़ाया जो पुराने ढ़ग के थे. [self]  
(defrule scorn2
(declare (salience 10))
(id-root ?id scorn)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id majAka_udA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scorn.clp 	scorn2   "  ?id "  majAka_udA)" crlf))
)



