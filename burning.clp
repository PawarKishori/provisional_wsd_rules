;@@@ Added by 14anu-ban-02 (19-11-2014)
;It should not be confused with crop residue burning, which burns useless parts of the crop.[agriculture]
;जले  हुए अनाज के अवशेषों से  घबराना नहीं चाहिए,जोकि अनाज का बेकार हिस्सा जला देता हैं.[manual]
(defrule burning1
(declare (salience 100))
(id-root ?id burning)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  burning.clp 	burning1   "  ?id "  jalA_huA )" crlf))
)

;@@@Added by 14anu-ban-02(20-04-2015)
;A burning desire to win.	;run the sentence on parser no. 27
;जीतने की प्रबल इच्छा  . [self]
(defrule burning3
(declare (salience 100))
(id-root ?id burning)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 desire)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prabala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  burning.clp 	burning3   "  ?id "  prabala )" crlf))
)

;@@@Added by 14anu-ban-02(20-04-2015)	;rule does'nt get activated because 'burning' is not treated as adjective by any of the parsers. 
;He's always had a burning ambition to start his own business.[oald]
;उसे  हमेशा  अपना उद्योग शुरु करने की अधिक लालसा  थी . [self]
(defrule burning4
(declare (salience 100))
(id-root ?id burning)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 ambition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  burning.clp 	burning4   "  ?id "  aXika )" crlf))
)

;------------------------default_rules----------------------------------------------

;@@@ Added by 14anu-ban-02 (19-11-2014)
;The pulling down of the Union Jack or the'burning of the American flag , in the belief that you were insulting those countries , is childish .[karan singla]
;The pulling down of the Union Jack or the burning of the American flag , in the belief that you were insulting those countries , is childish .[modified] [karan singla]
;यूनियन जैक को उतारना या अमेरिका के झंडे को जलाने से अगर हम यह समझते हैं कि हम इन मुल़्कों की बेइज़्जती कर रहे हें तो यह बचकानी बात है .[karan singla]
(defrule burning0
(declare (salience 0))
(id-root ?id burning)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalAnA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  burning.clp 	burning0   "  ?id "  jalAnA )" crlf))
)

;@@@Added by 14anu-ban-02(20-04-2015)
;The burning sun.[oald]	;run the sentence on parse no. 3
;जलता हुआ सूर्य .[self] 
(defrule burning2
(declare (salience 0))
(id-root ?id burning)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalawA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  burning.clp 	burning2   "  ?id "  jalawA_huA)" crlf))
)
