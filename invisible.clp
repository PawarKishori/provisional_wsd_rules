
(defrule invisible0
(declare (salience 5000))
(id-root ?id invisible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id axqSya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invisible.clp 	invisible0   "  ?id "  axqSya )" crlf))
)

;$$$ Modified by 14anu-ban-06 (26-02-2015)
;Tourism brings in 40 percent of the island's invisible earnings. (cambridge)
;पर्यटन द्वीप की सेवाओं के माध्यम से कमाई का 40 प्रतिशत लाता है . (manual)
(defrule invisible1
(declare (salience 5100));salience increased from '4900' by 14anu-ban-06 (26-02-2015)
(id-root ?id invisible)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id);added by 14anu-ban-06 (26-02-2015)
(id-root ?id1 earnings);added by 14anu-ban-06 (26-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sevAoM_ke_mAXyama_se));meaning changed from 'axqSya' to 'sevAoM_ke_mAXyama_se' by 14anu-ban-06 (26-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invisible.clp 	invisible1   "  ?id "  sevAoM_ke_mAXyama_se )" crlf))
)

;"invisible","Adj","1.axqSya"
;The moon was invisible in the sky.
;--"2.aprakata"
;
;
