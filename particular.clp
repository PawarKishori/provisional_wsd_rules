;----------------------------------------------Default Rule--------------------------------------------------------------------------------

(defrule particular0
(declare (salience 0000))
(id-root ?id particular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viSiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  particular.clp 	particular0   "  ?id "  viSiRta )" crlf))
)

;-------------------------------------------------------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on 1-8-14
;Why does a railway track have a particular shape like I? 
;rela patarI kI Akqwi @I ke samAna kyoM howI hE ?
(defrule particular1
(declare (salience 5000))
(id-root ?id particular)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(viSeRya-like_saMbanXI ?id1 ?id2)
(id-root ?id1 shape)
(id-root ?id2 I)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  particular.clp 	particular1   "  ?id "  samAna )" crlf))
)
