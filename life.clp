;$$$ Modified by 14anu-ban-08 (08-10-2014) ;Added cat_coarse for PropN
;@@@ Added by Nandini (3-12-13)
;She wrote a life of Mozart.
;usane PropN-mozart-PropN kI jIvanI liKI.
(defrule life1
(declare (salience 100))
(id-root ?id life)
?mng <-(meaning_to_be_decided ?id)
;(kriyA-subject  2 1)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id noun)
(id-cat_coarse ?id1 PropN)   ;Added by 14anu-ban-08 (08-10-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIvanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  life.clp 	life1   "  ?id "  jIvanI)" crlf))
)

;===============default-rule==========
;@@@ Added by Nandini (3-12-13)
;She has a full social life.
;usakA eka pUrA sAmAjika jIvana hE.
(defrule life0
(declare (salience 50))
(id-root ?id life)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIvana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  life.clp 	life0   "  ?id "  jIvana)" crlf))
)

;@@@ Added by Nandini
;Sohan and a few friends just managed to escape with their lives.
;sohana Ora kuCa miwroM ne apanI jAna bacAne mem  basa saPala hue.
(defrule life2
(declare (salience 300))
(id-root ?id life)
?mng <-(meaning_to_be_decided ?id)
(kriyA-with_saMbanXI  ?kri ?id)
(id-root ?kri escape)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  life.clp 	life2   "  ?id "  jAna)" crlf))
)

;$$$Modified by 14anu-ban-08 (12-03-2015)   ;spelling corrected, added " ?id "
;@@@ Added by Nandini(16-1-14)
;The members of the Senate had been named in the Act and appointed for life. [via mail]
;sIneta ke saxasyoM ke kArya meM nAma xie gaye We Ora jivanakAla waka ke liye niyukwa kie gaye We.
;sIneta ke saxasyoM ke kArya meM nAma xie gaye We Ora jIvanakAla waka ke liye niyukwa kie gaye We.  [modified by 14anu-ban-08]
(defrule life3
(declare (salience 400))
(id-root ?id life)
?mng <-(meaning_to_be_decided ?id)
(kriyA-for_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIvanakAla_waka))  ;spelling corrected 'jivanakAla_waka' to 'jIvanakAla_waka' by 14anu-ban-08 (12-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  life.clp 	life3  "  ?id "  jIvanakAla_waka)" crlf))   ;spelling corrected 'jivanakAla_waka' to 'jIvanakAla_waka' and added " ?id " by 14anu-ban-08 (12-03-2015)
)

;@@@ Added by 14anu-ban-01 on 03-08-2014.
;11436:The result was that Hinduism , with a fresh lease of life and vigour , regained its hold over the people and served as the foundation of a new national culture .[Karan Singla]
;परिणाम यह हुआ कि हिंदुत़्व ने नये जीवन की शुरूआत और नयी शाक़्ति के द्वारा , लोगों पर अपना प्रभाव पुन : स़्थापित कर लिया तथा एक नयी राष़्ट्रीय संस़्कृति की नींव का काम किया .[karan Singla]
(defrule life4
(declare (salience 50))
(id-root ?id life)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) of)
(id-word =(- ?id 2) lease)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  (- ?id 1) (- ?id 2)  jIvana_kI_SuruAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " life.clp	life4  "  ?id "  " (- ?id 1) "    " (- ?id 2) " jIvana_kI_SuruAwa  )" crlf))
)

;@@@Added by 14anu-ban-08 (27-02-2015)
;The car crash claimed three lives. [oald]
;गाडी का धमाका तीन जिंदगियाँ की मृत्यु का कारण बना . [self]
(defrule life5
(declare (salience 150))
(id-root ?id life)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 claim)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZiMxagI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  life.clp 	life5   "  ?id "  jZiMxagI)" crlf))
)

