
;$$$ Modified by 14anu-ban-01 on (31-07-14).
;In home also the solution can be prepared .
;घर में भी घोल तैयार कर सकते हैं ।
(defrule solution0
(declare (salience 5000))
(id-root ?id solution)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject ?id1 ?id)
(id-word ?id1 turn|prepare);added prepare by 14anu-ban-01
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solution.clp 	solution0   "  ?id "  Gola )" crlf))
)

;Commented by 14anu-ban-01 on (09-01-2015) because the example doesn't even contain the word solution .
;@@@ Added by 14anu04 on 13-June-2014
;The mixture consists of many liquids.
;घोल में बहुत सारे द्रव्य थे.
;(defrule solution_tmp
;(declare (salience 5000))
;(id-root ?id solution)
;?mng <-(meaning_to_be_decided ?id)
;(id-root ?id1 mix|mixture|liquid|consist)
;;(test (!= ?id1 0))
;(id-cat_coarse ?id noun)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id Gola))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solution.clp 	solution_tmp   "  ?id "  Gola )" crlf))
;)



;@@@ Added by 14anu-ban-01 on (13-09-14).
;Next we lightly sprinkle some lycopodium powder on the surface of water in a large trough and we put one drop of this solution in the water.[NCERT corpus]
;इसके बाद एक बडे नान्द में पानी लेकर, उसके ऊपर लायकोपोडियम पाउडर छिडक कर, लाइकोपोडियम पाउडर की एक पतली फिल्म जल के पृष्ठ के ऊपर बनाते हैं ; फिर ओलीक अम्ल के पहले बनाए गए घोल की एक बून्द इसके ऊपर रखते हैं. [NCERT corpus]
(defrule solution2
(declare (salience 5000))
(id-root ?id solution)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id1 ?id) 
(id-root ?id1 drop)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solution.clp 	solution2   "  ?id "  Gola )" crlf))
)

;@@@ Added by 14anu-ban-01 on (13-09-14).
;281855:Definitely give to drink half a glass solution of salt , sugar , lemon every half an hour .[Karan Singla]
;नमक - चीनी - नीबू का घोल प्रत्येक एक घंटे पर आधा गिलास अवश्य पिलाएँ ।[Karan Singla]
(defrule solution4
(declare (salience 5000))
(id-root ?id solution)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1) 
(id-root ?id1 salt|sugar|lemon|aniseed|cumin|medicine|plaque|antifebrin|argirol);list can be revised whenever required.
(id-cat_coarse ?id1 noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solution.clp 	solution4  "  ?id "  Gola )" crlf))
)

;@@@ Added by 14anu-ban-01 on (14-09-14).
;This rule can be restricted on occurance of any counter example.
;286719:Itching subsides by massaging even 25 percent solution of Benzyl Benzoate properly after bathing and then bathing later .[Karan Singla]
;बैन्जील बैन्जोएट का 25 प्रतिशत घोल भी स्नान करने के बाद अच्छी तरह शरीर पर मलने और बाद में स्नान करने से भी खारिश शान्त हो जाती है ।[Karan Singla]
(defrule solution5
(declare (salience 5000))
(id-root ?id solution)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1) 
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gola))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solution.clp 	solution5  "  ?id "  Gola )" crlf))
)

(defrule solution1
(declare (salience 4900))
(id-root ?id solution)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solution.clp 	solution1   "  ?id "  hala )" crlf))
)


;@@@ Added by 14anu-ban-01 on (13-09-14).
;Problems in physics should be solved without seeing the solution.[anusaaraka: solution.clp]
;भौतिक विज्ञान में समस्याएँ उत्तर को देखे बिना हल की जानी चाहिए . [self]
(defrule solution3
(declare (salience 4900))
(id-root ?id solution)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 see)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwwara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  solution.clp 	solution3   "  ?id "  uwwara )" crlf))
)


;default_sense && category=noun	samasyA_kA_samAXAna_karane_kA_warIkA	0
;"solution","N","1.samasyA_kA_samAXAna_karane_kA_warIkA"
;India should find a solution to the kashmir problem.
;--"2.uwwara"
;Problems in physics should be solved without seeing the soluton.
;--"3.Gola"
;A solution of sugar in water.
;
