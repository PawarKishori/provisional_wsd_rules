
;@@@Added by 14anu-ban-02(25-03-2015)
;He was blithe about the risks to his health.[mw]
;वह उसके स्वास्थ्य को खतरों के बारे में निश्चिंत था . [self]
(defrule blithe1 
(declare (salience 100)) 
(id-root ?id blithe) 
?mng <-(meaning_to_be_decided ?id) 
(viSeRya-about_saMbanXI  ?id ?id1)	;need sentences to restrict the rule
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id niScinwa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blithe.clp  blithe1  "  ?id "  niScinwa )" crlf)) 
) 

;---------------------default_rule-------------------------------

;@@@Added by 14anu-ban-02(25-03-2015)
;Sentence: A blithe and carefree girl.[oald]
;Translation: एक हँसमुख और निश्चिंत लड़की . [self]
(defrule blithe0 
(declare (salience 0)) 
(id-root ?id blithe) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id adjective) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id hazsamuKa)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  blithe.clp  blithe0  "  ?id "  hazsamuKa )" crlf)) 
) 
