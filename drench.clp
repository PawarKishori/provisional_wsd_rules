;@@@ Added by 14anu-ban-04 (26-11-2014)
;The specialty of rains here is that it can drench us any time with drizzling water and that is how it happened .     [tourism-corpus]
;यहाँ  की  बारिश  की  खासियत  है  कि  यह  कभी  भी  हमें  रिमझिम  पानी  की  बूँदों  से  सराबोर  कर  सकती  है  और  हुआ  भी  यही  ।                               [tourism-corpus]
(defrule drench0
(declare (salience 10))
(id-root ?id drench)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarAbora_kara))   
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drench.clp  drench0   "  ?id "  sarAbora_kara)" crlf))
)


;@@@ Added by 14anu-ban-04 (26-11-2014)
;When you will see the large Shani statue located here then you will yourself get drenched in the devotion of Suraputra Shanidev. 
;[tourism-corpus]
;जब  आप  यहाँ  स्थित  विशाल  शनि  प्रतिमा  के  दर्शन  करेंगे  तो  आप  स्वयं  सूर्यपुत्र  शनिदेव  की  भक्ति  में  रम  जाएँगे  ।           [tourism-corpus]
(defrule drench1
(declare (salience 30))
(id-root ?id drench)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 devotion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rama_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drench.clp  drench1   "  ?id "  rama_jA )" crlf))
)


;@@@ Added by 14anu-ban-04 (26-11-2014)
;The best time to tour Jammu is from April to October when the whole of the valley is drenched in greenery .     [tourism-corpus]
;जम्मू  घूमने  का  सबसे  अच्छा  समय  अप्रैल  से  अक्तूबर  तक  है  जब  पूरी  घाटी  हरियाली  से  सराबोर  होती  है  ।                                         [tourism-corpus]
(defrule drench2
(declare (salience 20))
(id-root ?id drench)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sarAbora_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  drench.clp  drench2   "  ?id "  sarAbora_ho)" crlf))
)
