;@@@ Added by 14anu-ban-11 on (10-04-2015)
;A witness for the defence.(oald)
;प्रतिपक्ष के लिए  गवाह.       [self]
(defrule witness2
(declare (salience 5001))
(id-root ?id witness)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-for_saMbanXI  ?id ?id1)
(id-root ?id1 defence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gavAha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  witness.clp 	witness2   "  ?id "  gavAha)" crlf))
)

;@@@ Added by 14anu-ban-11 on (10-04-2015)
;The document was witnessed by a lawyer. (oald)
;दस्तावेज वकील के द्वारा प्रमाणित किये गये थे . (self)
(defrule witness3
(declare (salience 4901))
(id-root ?id witness)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI  ?id ?id1)
(id-root ?id1 lawyer)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramANiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  witness.clp 	witness3   "  ?id "  pramANiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-11 on (10-04-2015)
;His good health is a witness to the success of the treatment.(oald)
;उसका अच्छा स्वास्थ्य इलाज की सफलता का प्रमाण है .(self)
(defrule witness4
(declare (salience 5002))
(id-root ?id witness)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-to_saMbanXI  ?id ?id1)
(id-root ?id1 success)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramANa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  witness.clp 	witness4  "  ?id "  pramANa)" crlf))
)

;----------------------------------------- Default Rules --------------------------------------

(defrule witness0
(declare (salience 5000))
(id-root ?id witness)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAkRI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  witness.clp 	witness0   "  ?id "  sAkRI )" crlf))
)

;"witness","VTI","1.xeKanA"
;You have to witness the scene.
(defrule witness1
(declare (salience 4900))
(id-root ?id witness)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  witness.clp 	witness1   "  ?id "  xeKa )" crlf))
)


