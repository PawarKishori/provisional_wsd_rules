;$$$ Modified by 14anu-ban-08 (11-12-2014)     
;@@@ Added by 14anu05 on 28.06.14
;It is quite a novel idea to use letters for messages.
;सन्देशों के लिए पत्रों का उपयोग करना एक नवीन विचार है.
(defrule novel2
(declare (salience 4900))
(id-root ?id novel)
?mng <-(meaning_to_be_decided ?id)
;(samAsa_viSeRya-samAsa_viSeRaNa  ?id1 ?id)        ;comment this relation by 14anu-ban-08 (11-12-2014)
(viSeRya-viSeRaNa ?id1 ?id)      ; added by 14anu-ban-08 (11-12-2014)
(id-root ?id1 idea)              ;uncomment this constraint by 14anu-ban-08 (11-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id navIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  novel.clp 	novel2   "  ?id "  navIna )" crlf))
)

;----------------------------- Default rules -----------------------

(defrule novel0
(declare (salience 100))
(id-root ?id novel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id navIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  novel.clp 	novel0   "  ?id "  navIna )" crlf))
)

;"novel","Adj","1.navIna"
;The computer produced a completely novel proof of a well-known theorem.
;
(defrule novel1
(declare (salience 100))
(id-root ?id novel)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upanyAsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  novel.clp 	novel1   "  ?id "  upanyAsa )" crlf))
)



;"novel","N","1.upanyAsa"
;Aparna is a voracious reader of novels.
;
