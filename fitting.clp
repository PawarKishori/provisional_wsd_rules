;@@@ Added by 14anu-ban-05 on (05-11-2014)
;The award was a fitting tribute to her years of devoted work.[OALD]
;यह पुरस्कार उसके वर्षों तक  कार्य  प्रति समर्पित रहने के लिए एक उपयुक्त श्रद्धांजलि था.[MANUAL]
(defrule fitting0
(declare (salience 1000))
(id-root ?id fitting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upayukwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fitting.clp     fitting0   "  ?id "  upayukwa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (05-11-2014)
;Along the way, you will find out why blacksmiths heat the iron ring before fitting on the rim of a wooden wheel of a bullock cart and why the wind at the beach often reverses direction after the sun goes down.[NCERT]
;aXyayana karawe Apa yaha BI jFAwa kareMge ki kisI bElagAdI ke lakadI ke pahie kI nemi para lohe kI riMga caDAne se pahale lohAra ise wapwa kyoM karawe hEM, waWA sUrya Cipane ke paScAw samuxra watoM para pavana prAyaH apanI xiSA uwkramiwa kyoM kara lewI hEM ?[NCERT]
(defrule fitting1
(declare (salience 5000))
(Domain physics)
(id-root ?id fitting)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-on_saMbanXI  ?id ?id1)
(id-root ?id1 rim)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id caDAnA))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fitting.clp 	fitting1   "  ?id "  caDAnA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fitting.clp       fitting1   "  ?id "  physics )" crlf))
)

