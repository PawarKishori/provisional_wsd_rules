;@@@ Added by 14anu-ban-04 (16-01-2015)
;Nature has endowed the human eye (retina) with the sensitivity to detect electromagnetic waves within a small range of the electromagnetic spectrum.                        [NCERT-CORPUS]
;प्रकृति ने मानव नेत्र (दृष्टि पटल) को वैद्युतचुम्बकीय स्पेक्ट्रम के एक छोटे परिसर में वैद्युत चुम्बकीय तरङ्गों को सुग्राहिता सहित संसूचित कर सकने योग्य बनाया है.        [NCERT-CORPUS]
;प्रकृति ने वैद्युतचुम्बकीय स्पेक्ट्रम के एक छोटे परास में चुम्बकीय तरङ्गों को   संसूचित करने के लिए मानव नेत्र (दृष्टि पटल) को सुग्राहिता प्रदान की है . [self]
;
(defrule endow1
(declare (salience 200))
(id-root ?id endow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
(pada_info (group_head_id ?id1)(preposition ?id2))
=>
(retract ?mng)  
;(assert (id-wsd_root_mng ?id praxAna_kara))                         
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id2 praxAna_kara)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " endow.clp  endow1  "  ?id "  " ?id2 "  praxAna_kara  )" crlf))
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " endow.clp  endow1  "  ?id "  praxAna_kara  )" crlf))
)

;@@@ Added by 14anu-ban-04 (16-01-2015)
;Our eye is, of course, one of the most important optical device the nature has endowed us with.        [NCERT-CORPUS]
;वास्तव में हमारे नेत्र सबसे महत्वपूर्ण प्रकाशिक युक्तियों में से एक हैं जिनसे प्रकृति ने हमें सम्पन्न किया है.                   [NCERT-CORPUS]
(defrule endow0
(declare (salience 110))
(id-root ?id endow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)  
(kriyA-object ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))     ;added by  14anu-ban-04 on (13-03-2015)              
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMpanna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  endow.clp    endow0   "  ?id " saMpanna_kara)" crlf))
)

;@@@ Added by 14anu-ban-04 (13-03-2015)
;In her will, she endowed a scholarship in the physics department.                    [oald]
;अपनी वसीयत  में, उसने भौतिक विज्ञान विभाग में छात्रवॄत्ति दी .                                          [self]
(defrule endow3
(declare (salience 120))
(id-root ?id endow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 scholarship|money)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  endow.clp    endow3  "  ?id " xe)" crlf))
)


;--------------------- Default rule ---------------

;@@@ Added by 14anu-ban-04 (13-03-2015)
;The state of Michigan has endowed three institutes to do research for industry.                   [cald]
;मिशिगन के राज्य ने उद्योग के लिए  अनुसंधान करने के लिए तीन संस्थाओं को दान दिया है .                                  [self]
(defrule endow2
(declare (salience 100))
(id-root ?id endow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xAna_xe))
(assert (kriyA_id-object_viBakwi ?id ko)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  endow.clp     endow2   "  ?id " ko  )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  endow.clp    endow2  "  ?id " xAna_xe)" crlf))
)


