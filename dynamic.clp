;$$$ Modified by 14anu-ban-04 (16-02-2015)   -----meaning changed from 'gawiSIla' to  'parivarwanaSila' 
;Business innovation is a dynamic process.               [cald]
;व्यापरिक नवीनीकरण एक परिवर्तनशील प्रक्रिया है .                     [self]   
(defrule dynamic0
(declare (salience 5000))
(id-root ?id dynamic)
?mng <-(meaning_to_be_decided ?id)   
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)           ;added by 14anu-ban-04 on (16-02-2015) 
(id-root ?id1 process)                ;added by 14anu-ban-04 on (16-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id parivarwanaSIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dynamic.clp 	dynamic0   "  ?id "  parivarwanaSIla )" crlf))
)

;------------------------ Default Rules ------------------

;"dynamic","Adj","1.gawiSIla"
(defrule dynamic1
(declare (salience 4900))
(id-root ?id dynamic)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gawiSIla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  dynamic.clp 	dynamic1   "  ?id "  gawiSIla )" crlf))
)

;"dynamic","Adj","1.gawiSIla"
;--"2.parivarwanAwmaka 
;Dynamic leadership is required for progress.
;
;
