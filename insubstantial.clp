;@@@ Added by 14anu-ban-06 (16-03-2015)
;An insubstantial meal. (cambridge)
;अपर्याप्त भोजन . (manual)
(defrule insubstantial0
(declare (salience 0))
(id-root ?id insubstantial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id aparyApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  insubstantial.clp  	insubstantial0   "  ?id "  aparyApwa )" crlf))
)

;@@@ Added by 14anu-ban-06 (16-03-2015)
;An insubstantial shadow.(COCA)
;काल्पनिक परछाई . (manual)
;As insubstantial as a shadow. (OALD)
;जितनी परछाई उतना काल्पनिक . (manual)
(defrule insubstantial1
(declare (salience 2000))
(id-root ?id insubstantial)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(or(viSeRya-viSeRaNa ?id1 ?id)(viSeRya-as_saMbanXI ?id ?id1))
(id-root ?id1 image|shadow)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id kAlpanika))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  insubstantial.clp  	insubstantial1   "  ?id "  kAlpanika )" crlf))
)

