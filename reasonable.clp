;@@@ Added by Anita--22.3.2014
;This is because as explained earlier it is reasonable to suppose that individual measurements are as ;likely to overestimate as to underestimate the true value of the quantity. [ncert]
;क्योङ्कि जैसा पहले स्पष्ट किया जा चुका है कि यह मानना युक्तिसङ्गत है कि किसी राशि की व्यष्टिगत माप उस राशि के वास्तविक मान से ;
;उतनी ही अधिआकलित हो सकती है, जितनी उसके अवआकलित होने की सम्भावना होती है ।
(defrule reasonable0
(declare (salience 5000))
(id-root ?id reasonable)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ? ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yukwisMgawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reasonable.clp 	reasonable0   "  ?id " yukwisMgawa )" crlf))
)

;$$$ Modified by Manasa 25-8-2015
;@@@ Added by Anita-3.4.2014
;Any reasonable person would have done exactly as you did. [oxford learner's dictionary]
;जैसा तुमने किया कोई भी विवेकी व्यक्ति ऐसा ही करता ।
(defrule reasonable1
(declare (salience 4900))
(id-root ?id reasonable)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id) ;added ?id1 by Manasa
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str))) ;added by Manasa 25-8-2015
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vivekI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reasonable.clp 	reasonable1   "  ?id " vivekI )" crlf))
)

;@@@ Added by Anita-3.4.2014
;We have reasonable grounds for believing that you are responsible. [oxford learner's dictionary]
;हमारे पास विश्वास करने के लिए उचित आधार हैं कि आप इसके लिए जिम्मेदार हैं ।
(defrule reasonable2
(declare (salience 4950))
(id-root ?id reasonable)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 ground|reason)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reasonable.clp 	reasonable2   "  ?id " uciwa )" crlf))
)

;@@@ Added by Anita-3.4.2014
;The hotel was reasonable, I suppose. [oxford learner's dictionary]
;मैं सोचता हूँ कि होटल ठीक ठाक था ।
(defrule reasonable3
(declare (salience 5100))
(id-root ?id reasonable)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 hotel|restaurant|cafeteria)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka_TAka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reasonable.clp 	reasonable3   "  ?id " TIka_TAka )" crlf))
)

;@@@ Added by14anu-ban-10 on (27-9-2014)
;This approximation is valid so far as the size of the object is much smaller than the distance it moves in a reasonable duration of time.[ncert corpus]
;yaha sannikatana waba waka mAnya howA hE jaba waka vaswu kA AkAra niSciwa samaya anwarAla meM vaswu xvArA calI gaI xUrI kI apekRA paryApwa rUpa se kama howA hE .[ncert corpus]
;यह सन्निकटन तब तक मान्य होता है जब तक वस्तु का आकार निश्चित समय अन्तराल में वस्तु द्वारा चली गई दूरी की अपेक्षा पर्याप्त रूप से कम होता है .
(defrule reasonable4
(declare (salience 5200))
(id-root ?id reasonable)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 duration) ;specification for condition added by 14anu-ban-10  on (5-11-2014) 
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paryApwa_rUpa_se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reasonable.clp 	reasonable4   "  ?id " paryApwa_rUpa_se)" crlf))
)

;@@@Added by 14anu24
;One of the ways you ' accept ' goods is by keeping them , without complaint , after you have had a reasonable time to examine them What is rea;sonable is not fixed ; it depends on all the circumstances .
;किसी सामान को स्वीकार करने का एक तरीका है उसका आप द्वारा जांचे जाने के उचित समय बीतने के बाद भी उसकी शिकायत किए बगैर उसे रखे रहना .
(defrule reasonable5
(declare (salience 5000))
(id-root ?id reasonable)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 time)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reasonable.clp       reasonable5   "  ?id " uciwa )" crlf))
)

;@@@ Added by 14anu17
;We aim to provide quality products at reasonable prices.[oxford]
;हम उचित मूल्यों में उच्चय पदार्थ देने के लिए लक्षित करते हैं . 
(defrule reasonable6
(declare (salience 4900))
(id-root ?id reasonable)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-word =(+ ?id 1)  price|prices)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uciwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reasonable.clp 	reasonable6   "  ?id " uciwa )" crlf))
)
;#####################################default-rule###########################################
;@@@ added by Anita-3.4.2014
;There's a reasonable chance that he'll come.  [oxford learner's dictionary]
; उसके आने की पर्याप्त संभावना है ।
(defrule reasonable_default-rule
(id-root ?id reasonable)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id paryApwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reasonable.clp 	reasonable_default-rule   "  ?id " paryApwa )" crlf))
)


