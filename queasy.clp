
;@@@ Added by 14anu-ban-11 on (24-03-2015)      ;Note:-Working on parser no. 2.
;Travelling by boat makes me queasy.(oald)
;नाव के द्वारा यात्रा मुझे वमनकारी बनाता है . (self)
(defrule queasy0
(declare (salience 10))
(id-root ?id queasy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(object-object_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 me)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vamanakAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  queasy.clp     queasy0  "  ?id "  vamanakAra)" crlf))
)


;---------------------------------Default Rules ------------------------------------------------------

;@@@ Added by 14anu-ban-11 on (24-03-2015)
;In fact she had been feeling tired and queasy for the past few days; and that night, when the leading man laid her on the sofa, suddenly turned her head and, without letting the audience see, vomited quietly down the sofa back.  (bnc-corpus).
;वास्तव में वह पिछले कुछ दिनों  से थकी हुई और बैचेनी महसूस कर रही थी; और उस रात, जब अग्रणी आदमी ने उसे सोफे पर लिटाया,अचानक  अपना सिर मोडकर और, दर्शकों से नजरे बचाकर सोफे के नीचे चुपचाप उल्टी कर दी .(manual)
(defrule queasy1
(declare (salience 00))
(id-root ?id queasy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id becEna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  queasy.clp     queasy1  "  ?id "  becEna)" crlf))
)

