;@@@ Added by 14anu-ban-11 on (16-02-2015)
;The woman scrubbed at her face with a tissue.(oald)
;स्त्री ने टिशू से उसके चेहरे को साफ किया . (self)
(defrule scrub2
(declare (salience 5001))
(id-root ?id scrub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-at_saMbanXI  ?id ?id1)
(id-root ?id1 face)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAPa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrub.clp 	scrub2   "  ?id "  sAPa_kara )" crlf))
)

;----------------------- Default Rules -------------

;"scrub","N","1.JAdZI"
;In our compound we have many ornamental scrub.
(defrule scrub0
(declare (salience 5000))
(id-root ?id scrub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JAdZI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrub.clp 	scrub0   "  ?id "  JAdZI )" crlf))
)

;"scrub","V","1.mArjana_karanA"
;Servent was scrubbing the floor with soap water.
(defrule scrub1
(declare (salience 4900))
(id-root ?id scrub)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mArjana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  scrub.clp 	scrub1   "  ?id "  mArjana_kara )" crlf))
)

