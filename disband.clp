;@@@ Added by 14anu-ban-04 (07-04-2015)
;They set about disbanding the terrorist groups.                 [oald]
;उन्होंने आतंकवादी समूहों को तोड़ने की शुरुआत की.                              [self]
(defrule disband1
(declare (salience 20))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disband)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id ko))
(assert (id-wsd_root_mng ?id woda)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "   disband.clp   disband1   "  ?id " ko )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disband.clp       disband1  "  ?id "  woda )" crlf))
)

;------------------------ Default Rules ---------------------

;@@@ Added by 14anu-ban-04 (07-04-2015)
;She formed a political group which disbanded a year later.                 [oald]
;उसने एक राजनैतिक समूह बनाया जो एक वर्ष बाद विभाजित हुआ .                                  [self]
(defrule disband0
(declare (salience 10))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id disband)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBAjiwa_ho)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  disband.clp       disband0  "  ?id "  viBAjiwa_ho)" crlf))
)



