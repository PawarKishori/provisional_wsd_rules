;$$$ Modified by 14anu02 on 4.7.14
;#### counter ex ##### I need to improve my English.(old)
;Anusaaraka translation-मुझे मेरा अङ्ग्रेजी सुधारनाने की जरूरत है . 
;Translation after correction-मुझे मेरा अङ्ग्रेजी सुधारने की जरूरत है .
;Added by Shirisha Manju (Suggested by Chaitanya Sir 6-6-13)
;Man has constantly made endeavors to improve the quality of communication with other human beings.
(defrule improve0
(declare (salience 5000))
(id-root ?id improve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?obj)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suXAra))
;(assert (make_verbal_noun ?id))	;commented by 14anu02 coz this fact is already present in the rule for 'to'
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  improve.clp    improve0   "  ?id "  suXAra )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "  improve.clp    improve0   "  ?id " )" crlf)
)
)

(defrule improve1
(declare (salience 4900))
(id-root ?id improve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suXara))
(assert (kriyA_id-subject_viBakwi ?id se))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  improve.clp    improve1   "  ?id "  suXara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  improve.clp    improve1   "  ?id " se ) " crlf))
)

;$$$ Modified by 14anu-ban-06 (06-04-2015)
;@@@ Added by Prachi Rathore[1-2-14]
;We've certainly improved on last year's figures.[oald] [parser no.- 3]
;हम पिछले वर्ष के आँकडो में निश्चित रूप से सुधार कर चुके हैं . 
(defrule improve2
(declare (salience 5000))
(id-root ?id improve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(kriyA-on_saMbanXI  ?id ?)	;commented by 14anu-ban-06 (06-04-2015)
(id-word ?id1 on)		;added by 14anu-ban-06 (06-04-2015)
(kriyA-upasarga ?id ?id1)	;added by 14anu-ban-06 (06-04-2015)
(kriyA-object ?id ?)		;added by 14anu-ban-06 (06-04-2015)
=>
(retract ?mng)
;(assert (id-wsd_root_mng ?id suXAra_kara)) ;commented by 14anu-ban-06 (06-04-2015)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 suXAra_kara));added by 14anu-ban-06 (06-04-2015)
(assert (kriyA_id-object_viBakwi ?id meM)) ;added by 14anu-ban-06 (06-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " improve.clp	improve2  "  ?id "  " ?id1 "  suXAra_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  improve.clp       improve2   "  ?id " meM )" crlf));added by 14anu-ban-06 (06-04-2015)
)

;@@@ Added by 14anu-ban-06 (14-10-2014)
;As observations improve in detail and precision or experiments yield new results, theories must account for them, if necessary, by introducing modifications.(NCERT)
;जैसे-जैसे प्रेक्षणों के विस्तृत विवरण तथा परिशुद्धता में सुधार करते हैं, अथवा प्रयोगों द्वारा नए परिणाम प्राप्त होते जाते हैं, वैसे यदि आवश्यक हो तो उन संशोधनों को सन्निविष्ट करके सिद्धान्तों में उनका स्पष्टीकरण किया जाना चाहिए.(manual)
(defrule improve3
(declare (salience 5050))
(id-root ?id improve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suXAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  improve.clp    improve3   "  ?id "  suXAra_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (19-11-2014)
;Soil microorganisms also improve nutrient availability and decrease pathogen and pest activity through competition.(agriculture)
;प्रतिस्पर्धा के माध्यम से मिट्टी के सूक्ष्मजीव पोषक तत्वों की उपलब्धता को बढाते है  और रोगाणु और हानिकारक कीट गतिविधि को घटाते है .(manual)
(defrule improve4
(declare (salience 5050))
(id-root ?id improve)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 availability)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baDA))
(assert  (id-wsd_viBakwi   ?id1  ko)) ;added by 14anu-ban-06 (21-11-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  improve.clp    improve4   "  ?id "  baDA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  improve.clp      improve4   "  ?id1 " ko )" crlf)) 
)

