;$$$ Modified by 14anu-ban-04 (16-03-2015) -- changed meaning svAxiRta as acCA
;There's a delicious smell in here.           [cald]
;यहां पर   अच्छी  सुगन्ध है .                            [self]
(defrule delicious0
(declare (salience 5000))
(id-root ?id delicious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)                       ;added by 14anu-ban-04 on (16-03-2015)
(id-root ?id1 smell)                              ;added by 14anu-ban-04 on (16-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id acCA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicious.clp 	delicious0   "  ?id "  acCA )" crlf))
)

;@@@ Added by 14anu-ban-04 (16-03-2015)
;It was a delicious joke.                    [same clp file]
;यह एक रोचक चुटकुला था .                            [self] 
(defrule delicious2
(declare (salience 5000))
(id-root ?id delicious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 joke|gossip|story)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rocaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicious.clp 	delicious2   "  ?id "  rocaka )" crlf))
)


;@@@ Added by 14anu-ban-04 (17-03-2015)
;A delicious shiver of excitement ran through his body.                   [oald]
;उत्तेजना की एक सुखद लहर उसके शरीर में  उठी .                                       [self] 
(defrule delicious3
(declare (salience 5000))
(id-root ?id delicious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 shiver)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suKaxa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicious.clp 	delicious3   "  ?id " suKaxa )" crlf))
)


;--------------------------------------------------DEFAULT RULE ------------------------------------------------------------------



(defrule delicious1
(declare (salience 4900))
(id-root ?id delicious)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svAxiRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  delicious.clp 	delicious1   "  ?id "  svAxiRta )" crlf))
)

;"delicious","Adj","1.svAxiRta"
;She prepared a delicious meal for the family.
;My mother always prepares delicious food.
;merI mAz hameSA svAxiRta Bojana pakAwI hE.
;--"2.rocaka"
;It was a delicious joke.
;
;
