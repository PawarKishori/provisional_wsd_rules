;@@@ Added by 14anu-ban-06 (09-03-2015)
; Although the tax is unpopular, the government has jibbed at abolishing it completely. (cambridge)
;[parser no. 628]
;यद्यपि कर अलोकप्रिय है, तो सरकार ने पूरी तरह से इसका उन्मूलन करने से इन्कार किया है . (manual)
(defrule jib2
(declare (salience 5000))
(id-root ?id jib)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 at)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 se_inkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " jib.clp	jib2  "  ?id "  " ?id1 "  se_inkAra_kara  )" crlf))
)

;---------------------- Default Rules -------------------

(defrule jib0
(declare (salience 5000))
(id-root ?id jib)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jahAja_kA__wikonA_pAwA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jib.clp 	jib0   "  ?id "  jahAja_kA__wikonA_pAwA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (09-03-2015)
;He jibbed investing any more money in shares. (jib.clp)
;उसने इसके बाद पैसे शेयरो में निवेश करने से इन्कार किया . (manual)
;"jib","V","1.kisI_kArya_ko_jArI_raKane_se_inkAra_karanA"
;He jibbed investing any more money in shares.
(defrule jib1
(declare (salience 4900))
(id-root ?id jib)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id se_inkAra_kara))           ;meaning changed from 'kisI_kArya_ko_jArI_raKane_se_inkAra_kara' to 'inkAra_kara' by 14anu-ban-06 (09-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jib.clp 	jib1   "  ?id "  se_inkAra_kara )" crlf))
)    ;meaning changed from 'kisI_kArya_ko_jArI_raKane_se_inkAra_kara' to 'inkAra_kara' by 14anu-ban-06 (09-03-2015)

