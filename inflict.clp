;@@@ Added by 14anu-ban-06 (03-03-2015)
;When someone deliberately inflicts damage, it is a matter for the police. (OALD)
;जब कोई जानबूझ कर हानि पहुँचाता है, यह पुलिस का विषय है . (manual)
(defrule inflict0
(declare (salience 0))
(id-root ?id inflict)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahuzcA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflict.clp 	inflict0   "  ?id "  pahuzcA )" crlf))
)

;@@@ Added by 14anu-ban-06 (03-03-2015)
;Sorry to inflict myself on you again like this! (OALD)[parser problem]
;फिर से इस तरह स्वतः को आपपर थोपने के लिए खेद! (manual)
(defrule inflict1
(declare (salience 2000))
(id-root ?id inflict)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Wopa))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflict.clp 	inflict1   "  ?id "  Wopa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  inflict.clp       inflict1   "  ?id " ko )" crlf))
)

;@@@ Added by 14anu-ban-06 (03-03-2015)
;They inflicted a humiliating defeat on the home team. (OALD)
;उन्होंने घरेलू दल को एक अपमानजनक शिकस्त दी . (manual)
(defrule inflict2
(declare (salience 2100))
(id-root ?id inflict)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 defeat)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inflict.clp 	inflict2   "  ?id "  xe )" crlf)
)
)
