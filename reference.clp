;$$$ Modified by 14anu-ban-10 on (13-01-2015)
;@@@ Added by Anita-10.4.2014
;My old headteacher said he would write me a glowing reference. [cambridge dictionary]
;मेरे पुराने प्रधानाध्यापक ने कहा कि वह मेरे लिए बहुत अच्छा प्रमाण-पत्र लिखेंगे ।
(defrule reference0
(declare (salience 1000))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id ?id1) 	;added '?id1' by 14anu-ban-10 on (13-01-2015)
(id-root ?id1 glow)          	;added by 14anu-ban-10 on (13-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pramANa_pawra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference0   "  ?id " pramANa_pawra )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (13-01-2015)
;@@@ Added by avni(14anu11)
;The first reference to  mavari occurs about eleven centuries ago
;मुख वीणा का उल्लेख वाद्य यंत्र दक्षिण भारतीय साहित्य में 12वीं शताब्दी के बाद से मिलना प्रारंभ होता है और नागस्वरम् का उल्लेख कम से कम 14वीं शताब्दी से मिलता है . 
(defrule reference9
(declare (salience 5000))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaNa  ?id ?)         ;commented out by 14anu-ban-10 on (13-01-2015)
(id-cat_coarse ?id noun)
;(kriyA-subject  ?id1 ?id)          ;commented out by 14anu-ban-10 on (13-01-2015)
;(viSeRya-det_viSeRaNa  ?id ?id2)      ;commented out by 14anu-ban-10 on (13-01-2015)
(viSeRya-viSeRaNa  ?id ?id3)
(id-root ?id3 first)                  ;added by 14anu-ban-10 on (13-01-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulleKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference9   "  ?id " ulleKa )" crlf))
)

;@@@ Added by Anita-10.4.2014
;I am writing with reference to  your letter of 15 March. [cambridge dictionary]
;मैं आपके 15 मार्च के पत्र के संबंध में लिख रहा हूँ ।
(defrule reference1
(declare (salience 2000))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
;(viSeRya-viSeRaNa  ?id ?)
(kriyA-with_saMbanXI  ?kri ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMbaMXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference1   "  ?id " saMbaMXa )" crlf))
)

;@@@ Added by Anita-10.4.2014
;Knowing what had happened, I avoided making any reference to weddings. [cambridge dictionary]
;यह जानते हुए कि क्या हुआ था ,मैंने शादी की कोई भी टिप्पणी करना टाल दिया ।
(defrule reference2
(declare (salience 3000))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?kri ?id)
(viSeRya-det_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tippaNI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference2   "  ?id " tippaNI )" crlf))
)
;@@@ Added by Anita-10.4.2014
;What's the grid reference of the village on this map? [cambridge dictionary]
;इस नक्शे पर गांव का ग्रिड संकेत क्या है ?
(defrule reference3
(declare (salience 4000))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMkewa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference3   "  ?id " saMkewa )" crlf))1
)

;@@@ Added by 14anu-ban-10 on (24-09-2014)
;The dimensional formulae of a large number and wide variety of physical quantities, derived from the equations representing the relationships among other physical quantities and expressed in terms of base quantities are given in Appendix 9 for your guidance and ready reference.  
;viviXa prakAra kI bahuwa sI BOwika rASiyoM ke vimIya sUwra, jinheM anya BOwika rASiyoM ke maXya sambanXoM ko nirUpiwa karane vAle samIkaraNoM se vyuwpanna waWA mUla rASiyoM ke paxoM meM vyakwa kiyA gayA hE, Apake mArgaxarSana evaM wAwkAlika sanxarBa ke lie pariSiRta - 9 meM xie gae hEM.
;विविध प्रकार की बहुत सी भौतिक राशियों के विमीय सूत्र, जिन्हें अन्य भौतिक राशियों के मध्य सम्बन्धों को निरूपित करने वाले समीकरणों से व्युत्पन्न तथा मूल राशियों के पदों में व्यक्त किया गया है, आपके मार्गदर्शन एवं तात्कालिक सन्दर्भ के लिए परिशिष्ट - 9 में दिए गए हैं.
(defrule reference4
(declare (salience 5000))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
;(or(viSeRya-viSeRaNa ?id ? )(viSeRya-RaRTI_viSeRaNa ?id ? ))     ;commented out by 14anu-ban-10 on (13-01-2015)
(viSeRya-viSeRaNa ?id ?id1 )        ;added by 14anu-ban-10 on (13-01-2015)
(id-root ?id1 ready)                ;added by 14anu-ban-10 on (13-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanxarBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference4   "  ?id " sanxarBa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (24-9-2014)
;Institute for Reference Materials and Measurements
;सन्दर्भ वस्तु एवं मापन संस्थान
(defrule reference5
(declare (salience 5200))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
(samAsa_viSeRya-samAsa_viSeRaNa ? ?id)
(id-cat_coarse ?id PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sanxarBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference5   "  ?id " sanxarBa )" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-09-2014)
;When Pasadena's Rose parade is being discussed then the reference of Doo Dah parade is natural .
;जब पसाडेना की रोज परेड की बात हो तो डू डाह परेड का जिक्र आना स्वभाविक है ।
(defrule reference6
(declare (salience 5400))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ? )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jikra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference6   "  ?id " jikra )" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-09-2014)         -----
;There is also a reference to the garden in a letter written to Shahjahan by Aurangzeb while he was on his way to the Deccan .
;शाहजहां की दक्षिण कूच के दौरान उसे लिखे औरंगजेब के एक पत्र में भी इस बाग का उल्लेख है .
(defrule reference7
(declare (salience 5600))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-to_saMbanXI ?id ?id1 )		;added '?id1' by 14anu-ban-10 on (13-01-2015)
(id-root ?id1 garden)               ;added by 14anu-ban-10 on (13-01-2015)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ulleKa))     ;corrected spelling from 'ulleka' to 'ulleKa' by 14anu-ban-10 on (13-01-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference7   "  ?id " ulleKa )" crlf))               ; corrected spelling from 'ulleka' to 'ulleKa' by 14anu-ban-10 on (13-01-2015)
)

;@@@ Added by 14anu-ban-10 on (25-09-2014)
;The question as to what Hindustani is cannot be determined by a reference to the non - Hindustani speaking areas of India , such as south India .
;हिंदुस़्तानी क़्या है , इस सवाल को , हिंदुस़्तान के उन लोगों के हवाले से तय नहीं किया जा सकता जो हिंदुस़्तानी नहीं बोलते हैं , जैसे दक़्खिनी हिंदुस़्तान के लोग . 
(defrule reference8
(declare (salience 5800))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
(kriyA-by_saMbanXI ? ?id )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id havAle))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference8   "  ?id " havAle )" crlf))
)

;#####################################default-rule####################################

;@@@ Added by Anita-11.4.2014
;He made the whole speech without reference to the notes in front of him. [cambridge dictionary]
;उसने अपने सामने बिना लेख के संदर्भ के पूरा भाषण दिया ।
(defrule reference_default-rule
(declare (salience 0))
(id-root ?id reference)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMxrBa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  reference.clp 	reference_default-rule   "  ?id " saMxrBa )" crlf))
)
