;@@@ Added by 14anu-ban-04 (03-12-2014)
;He distributed the bread amongst his followers.       [wikipedia]
;उसने अपने अनुयायियों के बीच रोटी बाँटी.                [manual]
(defrule distribute0
(declare (salience 100))
(id-root ?id distribute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distribute.clp 	distribute0  "  ?id " bAzta )" crlf))
)

;@@@ Added by 14anu-ban-04 (03-12-2014)
;In Chapter 7, we studied the rotation of the bodies and then realized that the motion of a body depends on how mass is distributed within the body.       [NCERT-CORPUS]
;अध्याय 7 में हमनें पिंडों के घूणन के बारे में पढा और समझा कि किसी पिंड की गति इस बात पर कैसे निर्भर करती है कि पिंड के अन्दर द्रव्यमान किस प्रकार वितरित  है.     [NCERT-CORPUS]
;It is also a very polyphagous species distributed in many regions of Persia, in particular in temperate zones.    [agriculture]
;यह भी  polyphagous प्रजातियाँ हैं जो  फारस  के अनेक क्षेत्रों में  वितरित हैं,विशेषकर मध्यम खण्डों में.            [manual]
(defrule distribute1
(declare (salience 300))
(id-root ?id distribute)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb) 
(or(kriyA-within_saMbanXI ?id ?id1)(kriyA-in_saMbanXI   ?id ?id2))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viwariwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  distribute.clp 	distribute1  "  ?id " viwariwa )" crlf))
)


