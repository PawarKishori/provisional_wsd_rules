
;@@@ Added by 14anu-ban-06 (06-02-2015)
;The people most immediately affected by the drought are the farmers themselves. (cambridge)
;सूखे के द्वारा सबसे अधिक सीधे प्रभावित होने वाले किसान खुद हैं . (manual)
;Counselling is being given to those most immediately affected by the tragedy. (OALD)
;दुःखद घटना के द्वारा सीधे प्रभावित होने वालो को सलाह दी जा रही है . (manual)
(defrule immediately1
(declare (salience 2000))
(id-root ?id immediately)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(kriyA-kriyA_viSeRaNa ?id1 ?id)
(id-root ?id1 affect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sIXe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immediately.clp 	immediately1   "  ?id "  sIXe )" crlf))
)

;@@@ Added by 14anu-ban-06 (06-02-2015) suggested by Vineet Chaitanya sir
;They moved in immediately before Christmas. (cambridge)
;वे क्रिसमस से ठीक पहले  मकान में आकर रहने लग गये . (manual)
;We heard a loud crash from the room immediately above us. (cambridge)
;हमने अपने ठीक ऊपर के कमरे से एक ऊँचे स्वर का धमाका सुना . (manual)
(defrule immediately2
(declare (salience 2500))
(id-root ?id immediately)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root =(+ ?id 1) after|before|above);removed 'adjacent' by 14anu-ban-06 (09-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immediately.clp 	immediately2   "  ?id "  TIka )" crlf))
)

;@@@ Added by 14anu-ban-06 (09-02-2015) 
;There is a row of houses immediately adjacent to the factory.(OALD)
;वहाँ फैक्टरी के ठीक बगल में घरों की कतार हैं.(manual)
(defrule immediately3
(declare (salience 2600))
(id-root ?id immediately)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
(id-root =(+ ?id 1) adjacent)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka))
(assert  (id-wsd_viBakwi   =(+ ?id 1)  meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immediately.clp 	immediately3   "  ?id "  TIka )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_viBakwi   " ?*prov_dir* "  immediately.clp      immediately3   "  =(+ ?id 1) " meM )" crlf)
)
)

;------------------ Default Rules ----------
;@@@ Added by 14anu-ban-06 (06-02-2015)
;She answered almost immediately.(OALD)
;उसने लगभग तत्काल उत्तर दिया .  (manual)
(defrule immediately0
(declare (salience 0))
(id-root ?id immediately)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adverb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id wawkAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  immediately.clp      immediately0   "  ?id "  wawkAla )" crlf))
)


