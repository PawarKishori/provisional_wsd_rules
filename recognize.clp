;$$$ Modified by Bhagyashri Kulkarni (3-11-2016)
;###Do you recognize him? (rapidex)
;###क्या आप उसको पहचानते हैं?
;@@@ Added by Anita--21.2.2014 
;I hadn't seen her for 20 years, but I recognized her immediately. [cambridge.;org/dictionary/learner-;english] [verified-sentence)
;मैं उससे २० वर्षों तक नहीं मिला था , पर मैंने तत्काल उसे पहचान लिया ।
(defrule recognize0
(declare (salience 955))	;increased by Bhagyashri
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
(kriyA-kriyA_viSeRaNa ?id ?id2) ;Added by Bhagyashri
(id-root ?id2 immediately)	;Added by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAna_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize0   "?id "  pahacAna_le )" crlf))
)

;@@@ Added by Anita--21.2.2014              
;Do you recognize this song? [cambridge.;org/dictionary/learner-;english]
;क्या तुम यह गाना जानते हो ?
(defrule recognize1
(declare (salience 10))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 song)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize1   "?id "  jAna )" crlf))
)

;@@@ Added by Anita--21.2.2014
;The international community has refused to recognize  the newly independent nation state. ;[cambridge.;org/dictionary/learner-;english]
;अंतर्राष्ट्रीय समुदाय ने नये स्वतंत्र देश को क़ानूनी रूप से  मान्यता देनें से इंकार कर दिया ।
;(defrule recognize2
;(declare (salience 20))
;(id-root ?id recognize)
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-object  ?id ?)
;(kriyA-subject  ?id ?)
;(kriyA-kriyArWa_kriyA  ? ?id)
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id mAnyawA_xe))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize2   "?id "  mAnyawA_xe )" crlf))
;)

;@@@ by Anita-- 21.2.2014
;The languages recognized as principal languages were Bengali Hindi Urdu and Assamese. [By mail]
;प्रमुख भाषाओं के रूप में बांगला, हिन्दी, उर्दू और असमिया को मान्यता दी गई ।
;pramuKa BARAoM ke jEsA mAnyawA xI huIM BARAez bangAlI hiMxI urxU Ora Assamese WIM. [no. 3 used parser output]
(defrule recognize3
(declare (salience 25))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 language)
(kriyA-as_saMbanXI  ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAnyawA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp         recognize3   "  ?id "  mAnyawA_xe )" crlf))
)


;$$$ Modified by Bhagyashri Kulkarni (3-11-2016)
;Do you recognize him? (rapidex)
;क्या आप उसको पहचानते हैं?
;$$$ Modified by 14anu-ban-10 on (13-01-2015)
;@@@ Added by 14anu20 on 17/06/2014
;She hardly recognized the place.
;उसने मुश्किल से स्थान पहचाना . 
(defrule recognize04
(declare (salience 925))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(or(id-root ?id1 place)(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str))))	;added by 14anu-ban-10 on (13-01-2015) ; Added 'animate' condition by Bhagyashri 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp         recognize04   "  ?id "  pahacAna )" crlf))
)

;@@@ Added by Anita-- 17-06-2014
;He was then recognized as Harish, a resident of the Dharmpura District. [news-dev]
;वह धर्मपुर मण्डल के निवासी हरीश के रूप में पहचाना जाता था  ।
(defrule recognize05
(declare (salience 20))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-as_saMbanXI  ?id ?sam)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAnA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize05   "?id "  pahacAnA_jA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (06-08-2014) 
;Observations since early times recognized stars which appeared in the sky with positions unchanged year after year.  
;Axya kAla ke prekRaNoM xvArA AkASa meM xiKAI xene vAle wAroM kI pahacAna kI gaI, jinakI sWiwi meM sAloMsAla koI parivarwana nahIM howA hE. [ncert]
(defrule recognize4
(declare (salience 5000))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 star)
(kriyA-object  ?id ?id1)
(id-cat_coarse ?id verb)             ;uncommented (id-cat_coarse ?id verb) by 14anu-ban-10 on (08-11-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize4   "?id " pahacAna )" crlf))
)

;@@@ Added by 14anu-ban-10 on (25-10-2014)
;Historically it was the Italian Physicist Galileo (1564-1642) who recognized the fact that all bodies, irrespective of their masses, are accelerated towards the earth with a constant acceleration.  [ncert corpus]
;iwihAsa ke anusAra italI ke BOwika vijFAnI gElIliyo (1564 - 1642) ne isa waWya ko mAnyawA praxAna kI ki saBI piNda, cAhe unake xravyamAna kuCa BI hoM, ekasamAna wvaraNa se pqWvI kI ora wvariwa howe hEM.[ncert corpus]
(defrule recognize6
(declare (salience 5100))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-vAkyakarma ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svIkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize6   "?id "  svIkAra_kara )" crlf))
)

;@@@ Added by 14anu21 on 21.06.2014  (id-root ?id1 face|clock)
;I do not recognize your face.
;मैं आपके चेहरे को स्वीकार नहीं करता हूँ . 
;मैंने आपका चेहरा नहीं पहचाना.
(defrule recognize7
(declare (salience 15))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 face|clock)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize7   "?id "  pahacAna )" crlf))
)

;@@@ Added by 14anu-ban-10 on (06-12-2014)
;These larvae, which are known as white grubs, are easily recognized by their broad and fleshy appearance, white or grayish white color together with the C-shaped body, having well-developed thoracic legs rarely used for locomotion.[agriculture domain]
;ये लार्वा, जो सफेद grubs के रूप में जाना जाता है, आसानी से अपनी व्यापक और मांसल  रूप द्वारा पहचाना जा सकता हैं ,इसका शरीर सफेद या भूरे सफेद रंग का सी आकार का होता है ,इसके पास अच्छी तरह से विकसित थोरैसिक पैर होते है जो कभी कभार  संचलन के लिए इस्तेमाल किये जाते हैं .[manual]
(defrule recognize8
(declare (salience 5200))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kriyA_viSeRaNa  ?id ?id1 )
(id-root ?id1 easily)                    ;added by 14anu-ban-10 on (13-01-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pahacAnA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize8   "?id "  pahacAnA_jA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (02-02-2015)
;Amber town is recognized in the whole world in the context of its famous forts and temples .[tourism corpus]
;आमेर  नगरी  अपने  प्रसिद्ध  किले  और  मंदिर  के  प्रसंग  में  विश्वभर  में  जानी  -  पहचानी  जाती  है  ।[tourism corpus]
(defrule recognize9
(declare (salience 5400))
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI ?id ?id1)
(id-root ?id1 world)               
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAna_pahacAnA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp  recognize9   "?id " jAna_pahacAnA_jA) " crlf))
)
;-----------------------Default-------------------------
;@@@ Added by Anita--21.2.2014
;He sadly recognized that he would die childless. [cambridge.;org/dictionary/learner-;english]
;उसने दुखी मन से यह स्वीकार कर लिया कि वह निसंतान मर जायेगा .
(defrule recognize5
(id-root ?id recognize)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id svIkAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recognize.clp 	recognize5   "?id "  svIkAra_kara )" crlf))
)

