
(defrule phone0
(declare (salience 5000))
(id-root ?id phone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id telIPZona))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  phone.clp 	phone0   "  ?id "  telIPZona )" crlf))
)

;"phone","N","1.telIPZona"
;I have talked to my friend on phone.




;Added by Meena(25.9.09)
;I tried phoning but there was no answer . 
(defrule phone1
(declare (salience 4900))
(id-root ?id phone)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZona_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  phone.clp     phone1   "  ?id "  PZona_kara )" crlf))
)


;
(defrule phone2
(declare (salience 4900))
(id-root ?id phone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PZona_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  phone.clp 	phone2   "  ?id "  PZona_kara )" crlf))
)

;"phone","V","1.PZona_karanA"
;My father asked me to phone for a taxi.
;

;@@@ Added by 14anu01 and 14anu21 on 04.07.2014
;I was just phoning up for fun. 
; मैं सिर्फ मनोरंजन के लिए फोन कर रहा था.
;I was just phoning up for a chat. 
;मैं गपशप के लिए मूल्य के लिए जरा फोन कर रहा था . 
;मैं गपशप करने के लिए फोन कर रहा था . 
 (defrule phone3
(declare (salience 4990))
(id-root ?id phone)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 up)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PZona_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "  phone.clp     phone3   "  ?id " "  ?id1 " PZona_kara )" crlf))
)

