;@@@Added by 14anu-ban-02(16-02-2015)
;Sentence: These scenes amongst the glittering tops of ancient temples and holy shrines located on mountains appear to you even beyond imagination .[total_tourism][run this sentence on parser n0.16]
;Translation: प्राचीन मंदिरों के जगमगाते शिखरों और पहाड़ों पर स्थित पवित्र धर्म-स्थलों के बीच के ये नज़ारे आपको कल्पना से भी परे लगते हैं .[total_tourism]
(defrule amongst0 
(declare (salience 0)) 
(id-root ?id amongst) 
?mng <-(meaning_to_be_decided ?id) 
(id-cat_coarse ?id preposition) 
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id ke_bIca_ke)) 	;meaning changed from ke_bIca_meM to ke_bIca_ke(24-02-2015)
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  amongst.clp  amongst0  "  ?id "  ke_bIca_ke )" crlf)) 
) 

;@@@Added by 14anu-ban-02(16-02-2015)
;You will experience that Deccan odissi is the best and brilliant amongst the world train journeys .[total_tourism]
;आप अनुभव करेंगे कि डेकन ओडिसी विश़्व की रेल यात्राओं में सर्वोत्तम और उत़्कृष़्ट है .[total_tourism]
;Many operators of Jaisalmer arrange horse riding but Prince Trail is superior amongst them .[total_tourism]
;जैसलमेर के कई ऑपरेटर घुड़सवारी की व्यवस्था करते हैं मगर प्रिंसेस ट्रेल इनमें उम्दा है .[total_tourism]
(defrule amongst1 
(declare (salience 100)) 
(id-root ?id amongst) 
?mng <-(meaning_to_be_decided ?id) 
(pada_info (group_head_id ?id2)(preposition ?id))
(viSeRya-amongst_saMbanXI  ?id1 ?id2)
(id-cat_coarse ?id1 adjective)
=> 
(retract ?mng) 
(assert (id-wsd_root_mng ?id meM)) 
(if ?*debug_flag* then 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  amongst.clp  amongst1  "  ?id "  meM )" crlf)) 
) 
