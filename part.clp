;$$$ Modified by 14anu-ban-09 on (08-12-2014)
;Changed meaning from 'aMSkAlika' to 'aMSakAlika' and another meaning 'aMSakAlIna'.
;She's looking for a part time job. [OALD] ;added by 14anu-ban-09 on (08-12-2014)
;vaha aMSakAlika/aMSakAlIna nOkarI DuMDa rahI hEM. [Self] ;added by 14anu-ban-09 on (08-12-2014)
;@@@ Added by 14anu22
(defrule part_time
(declare (salience 6000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) time)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) aMSakAlika/aMSakAlIna)) ;Changed 'aMSkAlika' to 'aMSakAlika' and added another meaning i.e. 'aMSakAlIna' by 14anu-ban-09 on (08-12-2014)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* "  part.clp 	part_time   "  ?id " " (+ ?id 1) "  aMSakAlika/aMSakAlIna )" crlf)) ;Changed 'aMSkAlika' to 'aMSakAlika' and added another meaning i.e. 'aMSakAlIna' by 14anu-ban-09 on (08-12-2014)
)

(defrule part0
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id parting )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id alagAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  part.clp  	part0   "  ?id "  alagAI )" crlf))
)

;"parting","N","1.alagAI"
;I remember my parting from my parents when I was a child.
;
;
(defrule part1
(declare (salience 4900))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 with)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " part.clp	part1  "  ?id "  " ?id1 "  xe  )" crlf))
)

;She doesn't like parting with dresses.
;vaha (kisI ko) apane kapadZe xenA pasaMxa nahIM karawI hE
(defrule part2
(declare (salience 4800))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part2   "  ?id "  BAga )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (02-03-2015)
;Have you learned your part yet?		[oald.com]
;क्या आप अभी तक आपकी भूमिका याद कर चुके हैं?		[self] 
;@@@ Added by Sonam Gupta MTech IT Banasthali 24-1-2014
;He played a prominent part in the campaign. [OALD]
;उसने अभियान में एक महत्वपूर्ण भूमिका निभाई .
(defrule part3
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 play|write|learn)		;added 'learn' by 14anu-ban-09 on (02-03-2015)
(kriyA-object  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUmikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part3   "  ?id "  BUmikA )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 25-1-2014
;Aircraft parts. [Cambridge]
;वायुयान के पुर्जे .
(defrule part4
(declare (salience 4990))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(id-root ?id1 plane|aircraft|machine|mechinery|spare)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id purjA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part4   "  ?id "  purjA )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 25-1-2014
;Slowly her lips parted and she smiled. [Cambridge]
;धीरे से उसके होंठ खुले और वो मुस्कुराई .
(defrule part5
(declare (salience 4990))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 lip)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Kula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part5   "  ?id "  Kula )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 25-1-2014
;That summer, after six years of marriage, we parted. [Cambridge]
;उन गर्मियों में, छः वर्ष के विवाह के बाद, हम अलग हो गए .
(defrule part6
(declare (salience 4990))
(id-root ?id part)
(id-word ?id parted)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part6   "  ?id "  alaga_ho )" crlf))
)

;@@@ Added by 14anu-ban-09 on (19-11-2014)
;It should not be confused with crop residue burning, which burns useless parts of the crop. [Agriculture]
;जले  हुए अनाज के अवशेषों से  घबराना नहीं चाहिए,जोकि अनाज का बेकार हिस्सा जला देता हैं. [Manual]

(defrule part8
(declare (salience 4800))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id ?id1)
(id-root ?id1 useless)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hissA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part8   "  ?id "  hissA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-02-2015)
;I pieced together the parts of broken vase.		[piece.clp]
;मैंने टूटे हुए फूलदान के टुकडे को एक साथ जोड़ा.     		[Self] 

(defrule part9
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 vase)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id tukadA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part9   "  ?id "  tukadA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-02-2015)
;I was given the part of Shri Rama in the play.		[Hinkhoj.com]
;मुझे नाटक में श्री राम का किरदार दिया गया था.	     		[Self] 

(defrule part10
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-cat_coarse ?id1 PropN)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kiraxAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part10   "  ?id "  kiraxAra )" crlf))
)

;@@@ Added by 14anu-ban-09 on (11-02-2015)
;The novel is good in parts.             		[oald.com]
;उपन्यास का अंश अच्छा है .           	     		[Self] 

(defrule part11
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id2 ?id1)
(viSeRya-in_saMbanXI  ?id1 ?id)
(id-root ?id2 novel)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part11   "  ?id "  aMSa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-03-2015)
;We spent part of the time in the museum.            		[oald.com]
;हमने सङ्ग्रहालय में कुछ समय बिताया. 			          	 [Self] 
(defrule part13
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id2))
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 time)
(id-root ?id2 of)
=>
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 ?id2 kuCa_samaya)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* "  part.clp 	part13   "  ?id " " ?id1 " " ?id2 "  kuCa_samaya )" crlf)) 
)

;@@@ Added by 14anu-ban-09 on (02-03-2015)
;She was very good in the part.             		[oald.com]
;वह भूमिका में बहुत अच्छी थी.	          	     		[Self] 
(defrule part14
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI  ?id1 ?id)
(id-root ?id1 good|bad)
(subject-subject_samAnAXikaraNa  ?id2 ?id1)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUmikA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part14   "  ?id "  BUmikA )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-03-2015)
;Fluoride levels of 0.2 parts per million.              		[oald.com]
;0.2 अंश प्रति मिलियन की फ़्लोराइड सतह.	          	     		[Self] 
(defrule part15
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-per_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part15   "  ?id "  aMSa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (02-03-2015)
;Add three parts wine to one part water.             		[oald.com]
;तीन अंश वाइन में एक अंश पानी मिलाए . 	          	     		[Self] 
(defrule part16
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-saMKyA_viSeRaNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aMSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp 	part16   "  ?id "  aMSa )" crlf))
)

;@@@ Added by 14anu-ban-09 on (13-03-2015)
;As part of the centenary celebrations a chain of beacons was lit across the region.       [oald.com]
;शतवर्षीय उत्सव के अन्तरगत आकाशदीप की लड़ी  क्षत्र में सर्वत्र प्रकाशित की गयी थी . 		[manual]
(defrule part17
(declare (salience 5000))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id2))
(viSeRya-of_saMbanXI  ?id ?id1) 
(id-root ?id1 celebration)
(id-root ?id2 of)
(id-word =(- ?id 1) as)
=>
(assert (affecting_id-affected_ids-wsd_group_root_mng (- ?id 1) ?id ?id2 ke_anwaragawa)) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng  " ?*prov_dir* "  part.clp 	part17   "  (- ?id 1) " " ?id " " ?id2 "  ke_anwaragawa )" crlf)) 
)


;--------------Default Rule ----------------
(defrule part7
(declare (salience 4700))
(id-root ?id part)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viBakwa_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  part.clp     part7   "  ?id "  viBakwa_ho )" crlf))
)


;default_sense && category=verb	alaga_ho	
;"part","V","1.alagahonA"
;He was parted from his family after division.
;
;


