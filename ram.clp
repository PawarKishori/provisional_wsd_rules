;$$$ Modified by Shirisha Manju 25-4-14 Suggested by Chaitanya Sir
;removed 'id-cat ?id proper_noun' 
;Ram is a good boy. 
(defrule ram0
(declare (salience 4800))
(id-root ?id ram)
?mng <-(meaning_to_be_decided ?id)
(id-original_word ?id Ram) ; added by Shirisha Manju 25-4-14 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ram.clp 	ram0   "  ?id "  rAma )" crlf))
)

;$$$ Modified by 14anu-ban-10 on (11-12-2014)    ; The  sentence has root problem as root is Ram .
;$$$ Modified by 14anu09 [6-6-14]
;@@@ Added by Shirisha Manju 25-4-14 Suggested by Chaitanya Sir
;They used the Ram machine to break the wall. 
(defrule ram1
(declare (salience 4900))
(id-root ?id ram)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun|PropN) ;added PropN by 14anu-ban-10 on (11-12-2014)
(id-root =(- ?id 1) a|an|the)
(id-root =(+ ?id 1) ~temple);added by 14anu09 Vasu Vardhan[6-6-14] eg. the Ram temple
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BeMdZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ram.clp 	ram1   "  ?id "  BeMdZA )" crlf))
)

;$$$ Modified by 14anu-ban-06 (28-04-2015)
;@@@ Added by 14anu01
;His car rammed into my house.
;उसकी गाडी ने मेरे घर पर टक्कर मारना . 
;उसकी गाडी ने मेरे घर में टक्कर मारी . (manual);added by 14anu-ban-06 (28-04-2015)
(defrule ram4
(declare (salience 5000))  ;added by 14anu-ban-10 on (11-12-2014)
(id-root ?id ram)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-into_saMbanXI  ?id ?)
(not(kriyA-object  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takkara_mAra));meaning changed from 'takkara_mAranA' to 'takkara_mAra' by 14anu-ban-06 (28-04-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ram.clp       ram4   "  ?id "  takkara_mAra )" crlf))
)

;@@@ Added by 14anu-ban-10 on  (11-02-2015)
;Two passengers were injured when their taxi was rammed from behind by a bus. [oald]
;उनकी टैक्सी को एक बस ने पीछे से टक्कर मार दी थी, जब दो यात्रियों घायल हो गए।[self]
(defrule ram5
(declare (salience 5100))  
(id-root ?id ram)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI  ?id ? )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takkara_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ram.clp       ram5   "  ?id "  takkara_mAra)" crlf))
)
;@@@ Added by 14anu-ban-10 on  (11-02-2015)
;She rammed the key into the lock.[oald]
;उसने ताला में कुंजी   कस कर दबा दियी।[self]
(defrule ram6
(declare (salience 5200))  
(id-root ?id ram)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object  ?id ?id1)
(id-root ?id1 key)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kasa_kara_xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ram.clp       ram6   "  ?id "  kasa_kara_xabA)" crlf))
)
;@@@ Added by 14anu-ban-10 on (11-02-2015)
;Ram is a farm animal.[manual]
;मेढ़ा एक फ़ार्म जानवर  है ।[manual]
(defrule ram7
(declare (salience 5300))  
(id-root ?id ram)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id ?id1 )
(id-root ?id1 animal)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id meDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ram.clp       ram7   "  ?id " meDZA)" crlf))
)

;-------------------- Default rules ----------------------------
(defrule ram2
(id-root ?id ram)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kasa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ram.clp 	ram2   "  ?id "  kasa )" crlf))
)

;"ram","VT","1.kasanA/BidZanAra/jAnabUJa_kara_xUsarI_gAdZI_se_takarAnA/TUzsanA"
;Do not ram the thread too hard or else it would break.
;The police van was rammed by a smuggler car.
;He rammed his clothes into a suitcase. 
;

;He has hundreds of rams in his cattle farm.
(defrule ram3
(id-root ?id ram)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BeMdZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ram.clp       ram3   "  ?id "  BeMdZA )" crlf))
)
;"ram","N","1.BeMdZA"
;He has hundreds of rams in his cattle farm.
;--"2.xIvAra_girAne_kI_maSIna"
;They used the Ram machine to break the wall. 
;

