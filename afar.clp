;@@@ Added by Rajini (05-07-2016)
;People from afar used to go to visit him.
;loga xUra xUra se unake xarshan ke lie jAwe We.
(defrule afar1
(declare (salience 3100))
(id-root ?id afar)
?mng<-(meaning_to_be_decided ?id)
(viSeRya-from_saMbanXI  ?id1  ?id)
(id-word ?id1 people)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra_xUra))
(if ?*debug_flag* then
 (printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  afar.clp      afar1   "  ?id "  xUra_xUra )" crlf)
)
)

;---------------------------------- Default Rule ------------------

;@@@ Added by Rajini (05-07-2016)
;This woman has come from afar. 
;yaha swrI xUra se AI. 
(defrule afar0
(declare (salience 3000))
(id-root ?id afar)
?mng<-(meaning_to_be_decided ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xUra))
(if ?*debug_flag* then
  (printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  afar.clp      afar0   "  ?id "  xUra )" crlf)
)
)

 
 
