;@@@ Added by 14anu-ban-08 on (02-09-2014)
;You are already familiar with some direct methods for the measurement of length.   [NCERT]
;म्बाई मापन की कुछ प्रत्यक्ष विधियों से आप पहले ही से परिचित हैं.
(defrule method0
(declare (salience 0))
(id-root ?id method)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id viXi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  method.clp 	method0   "  ?id "  viXi )" crlf))
)

;@@@ Added by 14anu-ban-08 on (02-09-2014)
;Large masses in the universe like planets, stars, etc., based on Newton's law of gravitation can be measured by using gravitational method (See Chapter 8).   [NCERT]
;विश्व में पाए जाने वाले विशाल पिण्डों जैसे ग्रहों, तारों आदि के द्रव्यमान ज्ञात करने के लिए हम न्यूटन के गुरुत्वाकर्षण के नियम का उपयोग करते हैं (देखिए अध्याय 8).
(defrule method1
(declare (salience 200))
(id-root ?id method)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 gravitational)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id niyama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  method.clp 	method1   "  ?id "  niyama )" crlf))
)

