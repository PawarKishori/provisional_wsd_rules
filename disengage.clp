;@@@ Added by 14anu-ban-04 (18-04-2015)
;Put the car in gear, and then slowly disengage the clutch while pressing on the gas pedal.              [merriam-webster]
;गाड़ी  गियर में रखिए, जब कि गैस पैडल को दबाइए  और बाद में  क्लच को धीरे से  छोड़िए .                        [self] 
(defrule disengage1
(declare (salience 30))
(id-root ?id disengage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 clutch)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Coda))
(assert(kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  disengage.clp     disengage1   "  ?id " ko  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disengage.clp 	disengage1   "  ?id "   Coda )" crlf))
)

;@@@ Added by 14anu-ban-04 (18-04-2015)
;She gently disengaged herself from her sleeping son.             [oald]
;उसने  स्वयं को धीरे से अपने  सोते हुए बेटे से  अलग किया .                           [self]
(defrule disengage2
(declare (salience 20))
(id-root ?id disengage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disengage.clp 	disengage2   "  ?id "   alaga_kara )" crlf))
)


;-------------------------------------------------------DEFAULT RULES-----------------------------------------------------------------------

;@@@ Added by 14anu-ban-04 (18-04-2015)
;We saw the booster rockets disengage and fall into the sea.               [oald]
;हमने देखा कि समर्थक रॉकेट अलग होते हैं और समुद्र में गिरते हैं  .                               [self]
(defrule disengage0
(declare (salience 10))
(id-root ?id disengage)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alaga_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  disengage.clp 	disengage0   "  ?id "   alaga_ho )" crlf))
)

