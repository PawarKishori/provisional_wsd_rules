;@@@ Added by 14anu-ban-06 (02-04-2015)
;His task is to impregnate the female and then to disappear.(COCA)
;उसका काम स्त्री को गर्भिणी करना और बाद में गायब होना है .  (manual)
(defrule impregnate1
(declare (salience 2000))
(id-root ?id impregnate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)   
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garBiNI_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impregnate.clp 	impregnate1   "  ?id "  garBiNI_kara )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;@@@ Added by 14anu-ban-06 (02-04-2015)
;This cloth has been impregnated with special chemicals for cleaning computer screens. (cambridge)
;इस कपडे को कम्प्यूटर के पर्दे साफ करने के लिए विशेष रसायनिक द्रव्य से व्याप्त किया जाता है . (manual)
(defrule impregnate0
(declare (salience 0))
(id-root ?id impregnate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyApwa_kara))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  impregnate.clp 	impregnate0   "  ?id "  vyApwa_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  impregnate.clp       impregnate0   "  ?id " ko )" crlf))
)
