
(defrule majority0
(declare (salience 5000))
(id-root ?id majority)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bahumawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  majority.clp 	majority0   "  ?id "  bahumawa )" crlf))
)

;$$$Modified by 14anu-ban-08 (27-03-2015)    ;commented relation, added relation
;The majority of the members voted for the motion.  [same clp file]
;अधिकांश सदस्यों ने व्यापार के लिए मत दिया.  [self]
(defrule majority1
(declare (salience 5001))
(id-root ?id majority)
?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) of )          ;commented by 14anu-ban-08 (27-03-2015)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)       ;added by 14anu-ban-08 (27-03-2015)
(id-root ?id1 member|people)         ;added by 14anu-ban-08 (27-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXikAMSa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  majority.clp 	majority1   "  ?id "  aXikAMSa )" crlf))
)

;default_sense && category=noun	aXikAMSa	0
;"majority","N","1.aXikAMSa"
;The majority of the members voted for the motion.
;--"2.bahumawa"
;This government does not have an absolute majority.
;
;

;@@@Added by 14anu-ban-08 (27-03-2015) 
;The age of majority in Britain was reduced from 21 to 18 in 1970.  [oald]
;ब्रिटेन में 1970 में वयस्कता उम्र 21 से लेकर 18 तक कम कर दी गई हैं.  [self]
(defrule majority2
(declare (salience 5001))
(id-root ?id majority)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 age)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vayaskawA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  majority.clp 	majority2   "  ?id "  vayaskawA )" crlf))
)
