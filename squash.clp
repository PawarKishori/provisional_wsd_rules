;@@@ Added by jagriti(11.1.2014)
;He should not squash inside the hall at once.
;उसको तुरन्त हॉल के अन्दर धकिया कर नहीं घुसना चाहिए .
;How many people are they going to try and squash into this bus? 
;कितने लोग कोशिश करके और कितने धकिया कर जा रहे हैं बस में ?
(defrule squash0
(declare (salience 5000))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(or(and(kriyA-into_saMbanXI ?id1 ?)(conjunction-components  ? ?id1 ?id))(kriyA-inside_saMbanXI ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XakiyA_kara_Gusa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash0   "  ?id "  XakiyA_kara_Gusa )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (28-02-2015)
;@@@ Added by jagriti(11.1.2014)
;He squashed his nose against the window.[oald]
;उसने खिडकी के विरुद्ध उसकी नाक दबाई . 
;उसने खिडकी से अपनी नाक टकरा ली . [Translation improved 14anu-ban-01 on (28-02-2015)]
(defrule squash1
(declare (salience 4900))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))	;added by 14anu-ban-01 on (28-02-2015)
(kriyA-subject ?id ?id1)								;added by 14anu-ban-01 on (28-02-2015)
(id-root ?id2 nose|hand|foot|shoulder|head)						;added by 14anu-ban-01 on (28-02-2015)
(kriyA-object ?id ?id2)									;added by 14anu-ban-01 on (28-02-2015)
;(kriyA-against_saMbanXI ?id ?)		;commented by 14anu-ban-01  on (28-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id takarA_le))	;changed 'xabA' to 'takarA_le' by 14anu-ban-01 on (28-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash1   "  ?id "  takarA_le )" crlf))				;changed 'xabA' to 'takarA_le' by 14anu-ban-01 on (28-02-2015)
)

;@@@ Added by jagriti(11.1.2014)
;The statement was an attempt to squash the rumours.
;उसका बयान अफवाहें दबाने के लिये एक प्रयास था . 
(defrule squash2
(declare (salience 4800))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 rumour|idea|plan|curiosity)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash2   "  ?id "  xabA )" crlf))
)
;@@@ Added by jagriti(11.1.2014)
;Give me two orange squashes, please.
;कृपया मुझे दो नारङ्गी शरबत दीजिए . 
(defrule squash3
(declare (salience 4700))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa ?id ?id1)
(id-root ?id1 orange|lemon|apple|grape)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sarabawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash3   "  ?id "  Sarabawa )" crlf))
)

;$$$ Modified by 14anu-ban-01 on (28-02-2015)
;@@@ Added by jagriti(11.1.2014)
;To play squash.
;स्क्वैश खेलने के लिए.
;स्क्वॉश खेलने के लिए.[Translation improved by 14anu-ban-01 on (28-02-2015)]
(defrule squash4
(declare (salience 4600))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(id-root ?id1 play)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id skvoYSa))	;corrected "skvAzSa" to "skvoYSa" by 14anu-ban-01 on (28-02-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash4   "  ?id "  skvoYSa )" crlf))			;corrected "skvAzSa" to "skvoYSa" by 14anu-ban-01 on (28-02-2015)
)
;@@@ Added by jagriti(11.1.2014)
;To squash a lemon.
;एक नींबू निचोड़ो. 
(defrule squash5
(declare (salience 4500))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 lemon)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nicodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash5   "  ?id "  nicodZa )" crlf))
)


;@@@ Added by 14anu05 GURLEEN BHAKNA on 21.06.14
;She was squashed between the door and the table.
;वह दरवाजे और मेज के बीच दब गई थी.
(defrule squash9
(declare (salience 5000))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "animate.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xaba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash9   "  ?id "  xaba )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (28-02-2015)
;Squash player Joshna Chinappa , 15 , has always competed in an age - group two years senior to her own because she wants to become India ' s first high - ranking professional .
;स्क्वैश खिलडी 15 वर्षीया जोशना चिनप्पा हमेशा अपनी उम्र से दो साल ऊपर के खिलडियों के साथ होडे लेती हैं क्योंकि वे भारत की सर्वोच्च पेशेवर खिलडी कहलना चाहती हैं .
;15 वर्षीय स्क्वॉश  खिलाड़ी  जोशना चिनप्पा ने हमेशा अपनी उम्र से दो साल ऊपर के खिलडियों के साथ मुकाबला किया है क्योंकि वे भारत की सर्वोच्च पेशेवर खिलाड़ी कहलना चाहती हैं .[Translation improved by  14anu-ban-01 on (12-01-2015)]
;@@@ Added by 14anu11
(defrule squash8
(declare (salience 5000))
(id-root ?id squash)
(id-word =(+ ?id 1) player)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id skvoYSa));corrected "sakavAS" to "skvoYSa"by 14anu-ban-01
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash8   "  ?id "  skvoYSa )" crlf));corrected "sakavAS" to "skvoYSa"by 14anu-ban-01
)


;@@@ Added by 14anu-ban-01 on (28-02-2015) 
;Sentence - He dropped dead on the squash court at the age of 43.[cald]
; ४३ वर्ष की आयु में स्क्वॉश कोर्ट में उसकी  मृत्यु  अचानक  हो गयी . [self]    
(defrule squash10
(declare (salience 5000))
(id-root ?id squash)	
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 court|racket)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id skvoYSa))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash10   "  ?id "  skvoYSa )" crlf))
)


;Corrected translation by 14anu-ban-01 on (23-03-2015)
;@@@ Added by 14anu-ban-01 on (28-02-2015) 
;Pumpkins are varieties of squash.[squash.clp: improved sentence-->changed vanities to varieties]
;कद्दू कुम्हड़े की उपजाति है. [self]    
(defrule squash11
(declare (salience 5000))
(id-root ?id squash)	
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 variety)	;changed "vanity" to "variety"
(viSeRya-of_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kumhadZA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash11  "  ?id "  kumhadZA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (28-02-2015) 
;Winter squash have hard skin and orange flesh.[oald]
;शीतऋतु में आने वाले कुम्हड़े का छिलका कड़ा और गूदा नारङ्गी होता है. [self]    
(defrule squash12
(declare (salience 5000))
(id-root ?id squash)	
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 summer|winter)
(samAsa_viSeRya-samAsa_viSeRaNa  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kumhadZA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash12  "  ?id "  kumhadZA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (02-03-2015) 
;If you all squashed up, we could fit an extra person in the car.[cald]
;यदि आप सब खिसक जाऐँ, तो हम गाडी में एक और व्यक्ति को समा सकते हैं . [self]
(defrule squash13
(declare (salience 200))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 up)
(kriyA-upasarga  ?id ?id1)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-subject  ?id ?id2)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Kisaka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* "   squash.clp 	squash13   "  ?id "  " ?id1 "  Kisaka_jA  )" crlf))
)


;@@@ Added by 14anu-ban-01 on (02-03-2015) 
;Tomatoes are easier to squash than potatoes.[squash.clp]
;आलुओं की अपेक्षा टमाटरों को मसलना आसान है. [self] 
(defrule squash14
(declare (salience 200))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(saMjFA-to_kqxanwa  ? ?id)
(kriyA-than_saMbanXI  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id masala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash14   "  ?id "  masala )" crlf))
)


;@@@ Added by 14anu-ban-01 on (02-03-2015) 
;His plans were squashed by his new boss.[squash.clp]--parser no. 107
;उसकी योजनाएँ उसके नये बॉस के द्वारा नकार दी गयी थीं . [self]
(defrule squash15
(declare (salience 300))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 plan|deal|idea|intention|policy|project|proposal|strategy|suggestion|intent|outline|plot)
(kriyA-subject  ?id2 ?id1)
(kriyA-vAkyakarma  ?id2 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nakAra_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash15   "  ?id "  nakAra_xe )" crlf))
)

;@@@ Added by 14anu-ban-01 on (02-03-2015) 
;He tried to squash his ripped jeans into the suitcase while his mother wasn't looking.[cald]
;उसने सूटकेस में अपनी फटी हुई जीन्स ठूँसने का प्रयास किया जब उसकी माँ नहीं देख रही थी .  [self]
(defrule squash16
(declare (salience 200))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 jeans|dress|frock|cloth|saree|apparel|outfit|shirt|pant|coat|towel|garment)
(kriyA-object  ?id ?id1)
(id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))
(kriyA-subject  ?id ?id2)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TUzsa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash16   "  ?id "  TUzsa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (02-03-2015) 
;He put on coffee and sat down at the table to watch the sunrise over the mountain of squashed cars.[coca]
;उसने कॉफी बनाई और पिचकी/कुचली/दबी हुई गाडियों के ढेर पर से सूर्योदय को देखने के लिए मेज पर बैठा . [self]
(defrule squash17
(declare (salience 5000))
(id-root ?id squash)	
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 car|bike|vehicle|bicycle|truck|motorbike|bus)
(viSeRya-viSeRaNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kucalA_huA/picakA_huA/xabA_huA))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash17   "  ?id "  kucalA_huA/picakA_huA/xabA_huA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (02-03-2015) 
;Inside the sealed plastic, his bread and cheese was squashed but fresh.[coca]
;बंद प्लास्टिक के अन्दर,उसके ब्रेड और चीज मसल जाने के बावजूद  ताजे थे.[self] 
(defrule squash18
(declare (salience 200))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 bread|cheese)	;list can be added later 			
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id masala_jA))
(assert (make_verbal_noun ?id))	
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-make_verbal_noun " ?*prov_dir* "   squash.clp 	squash18     "  ?id " )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash18   "  ?id "  masala_jA )" crlf))
)


;...Default rule....

;$$$ Modified by 14anu-ban-01 on (02-03-2015) 
;It's a real squash with six of us in the car. [oald]
;हम छः लोगों के होने से गाड़ी में सच में भीड़ हो गयी है[self: more faithful--squash is a noun here]
;हम छः लोगों के होने से गाड़ी में सच में ठसाठस भर गयी है[self: more natural]
;$$$ Modified by jagriti(11.1.2014)..... meaning changed from ;Sarabawa' to 'TasATasa'
(defrule squash6
(declare (salience 100))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BIda))		;changed "TasATasa" to "BIda" by 14anu-ban-01 on (02-03-2015) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash6   "  ?id "  BIda)" crlf)) ;changed "TasATasa" to "BIda" by 14anu-ban-01 on (02-03-2015) 
)
;"squash","N","1.Sarabawa"
;A chilled glass of orange squash.
;--"2.skvASa{Kela}"
;I enjoy playing squash.
;--"3.kumhadZA"
;Pumpkins are vanities of squash.

(defrule squash7
(declare (salience 100))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BurawA_banA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  squash.clp 	squash7   "  ?id "  BurawA_banA_xe )" crlf))
)

;"squash","V","1.BurawA_banA_xenA"
;Tomatoes are easier to squash than potatoes.
;--"2.wiraskAra_karanA"
;His plans were squashed by his new boss.
;--"3.XakiyA_kara_GusanA"
;You should not try && squash inside the hall at once.
;

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule sub_samA_squash3
(declare (salience 4700))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa ?id ?id1)
(id-root ?id1 orange|lemon|apple|grape)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sarabawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " squash.clp   sub_samA_squash3   "   ?id " Sarabawa )" crlf))
)

;@@@ Added by Sukhada (12-05-14). Automatically generated this rule.
(defrule obj_samA_squash3
(declare (salience 4700))
(id-root ?id squash)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(object-object_samAnAXikaraNa ?id ?id1)
(id-root ?id1 orange|lemon|apple|grape)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Sarabawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng " ?*prov_dir* " squash.clp   obj_samA_squash3   "   ?id " Sarabawa )" crlf))
)
