
;ADded by human
(defrule deck0
(declare (salience 5000))
(id-root ?id deck)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) tape)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ---))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deck.clp 	deck0   "  ?id "  --- )" crlf))
)

(defrule deck1
(declare (salience 4900))
(id-root ?id deck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id deka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deck.clp 	deck1   "  ?id "  deka )" crlf))
)

(defrule deck2
(declare (salience 4800))
(id-root ?id deck)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sajA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deck.clp 	deck2   "  ?id "  sajA )" crlf))
)

;"deck","V","1.sajAnA"
;She decked up nicely for the party.
;
;

;@@@ Added by 14anu05 GURLEEN BHAKNA on 19.06.14
;Noticing a deck of cards on the counter, she paused.
;काउण्टर पर ताशों की गड्डी पर ध्यान देते हुए, वह रुकी.
(defrule deck3
(declare (salience 5500))
(id-root ?id deck)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 card)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gaddI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deck.clp 	deck3   "  ?id "  gaddI )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 19.06.14
;We sat on the top deck of the bus.
;हम बस की ऊपर वाली छत पर बैठे .
(defrule deck4
(declare (salience 5500))
(id-root ?id deck)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 bus)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Cawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deck.clp 	deck4   "  ?id "  Cawa )" crlf))
)

;@@@ Added by 14anu05 GURLEEN BHAKNA on 19.06.14
;He was standing on the deck of the ship at night.
;वह रात में जहाज के ऊपरी भाग पर खडा हुआ था .
(defrule deck5
(declare (salience 5500))
(id-root ?id deck)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI  ?id ?id1)
(id-root ?id1 ship)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id UparI_BAga))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  deck.clp 	deck5   "  ?id "  UparI_BAga )" crlf))
)


