;Added by Meena(22.5.11)
;In fact she had been feeling tired and queasy for the past few days. 
(defrule past00
(declare (salience 4800))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(or(id-root =(+ ?id 1) few|day|year|month|week|hour)(id-root =(+ ?id 2) day|year|month|week|hour))
(or(viSeRya-viSeRaNa =(+ ?id 1) ?id)(viSeRya-viSeRaNa =(+ ?id 2) ?id))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp      past00   "  ?id "  piCalA )" crlf))
)


;Added by human
(defrule past0
(declare (salience 5000))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 month)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past0   "  ?id "  piCalA )" crlf))
)

(defrule past1
(declare (salience 4900))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 week)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past1   "  ?id "  piCalA )" crlf))
)

(defrule past2
(declare (salience 4800))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 year)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past2   "  ?id "  piCalA )" crlf))
)




(defrule past3
(declare (salience 4700))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 day)
(viSeRya-viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id piCalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past3   "  ?id "  piCalA )" crlf))
)

(defrule past4
(declare (salience 4600))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) our)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past4   "  ?id "  ke_pare )" crlf))
)

(defrule past5
(declare (salience 4500))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) their)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past5   "  ?id "  ke_pare )" crlf))
)

(defrule past6
(declare (salience 4400))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) my)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past6   "  ?id "  ke_pare )" crlf))
)

(defrule past7
(declare (salience 4300))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) her)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past7   "  ?id "  ke_pare )" crlf))
)

(defrule past8
(declare (salience 4200))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) his)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past8   "  ?id "  ke_pare )" crlf))
)

(defrule past9
(declare (salience 4100))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) an)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past9   "  ?id "  ke_pare )" crlf))
)

(defrule past10
(declare (salience 4000))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) a)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past10   "  ?id "  ke_pare )" crlf))
)

(defrule past11
(declare (salience 3900))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) the)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past11   "  ?id "  ke_pare )" crlf))
)

(defrule past12
(declare (salience 3800))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) is)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bIwa_gayA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past12   "  ?id "  bIwa_gayA )" crlf))
)

(defrule past13
(declare (salience 3700))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 2) in)
(id-word =(- ?id 1) the)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUwakAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past13   "  ?id "  BUwakAla )" crlf))
)

(defrule past14
(declare (salience 3600))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUwapUrva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past14   "  ?id "  BUwapUrva )" crlf))
)

(defrule past15
(declare (salience 3500))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) the)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUwakAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past15   "  ?id "  BUwakAla )" crlf))
)

(defrule past16
(declare (salience 3400))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 figure)
(viSeRya-of_saMbanXI ?id1 ?id) ;Replaced viSeRya-of_viSeRaNa as viSeRya-of_saMbanXI programatically by Roja 09-11-13
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BUwakAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past16   "  ?id "  BUwakAla )" crlf))
)

; The figures of the past such as Buddha, Lao Tse && Jesus ..
(defrule past17
(declare (salience 3300))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAxa_meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past17   "  ?id "  bAxa_meM )" crlf))
)

(defrule past18
(declare (salience 3200))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pare))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past18   "  ?id "  pare )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (11-03-2015)
;A truck tore past the gates.		[oald]
;ट्रक द्वारों के पास से हो कर तेजी से भागा/भगाया .	[manual]
;@@@ Added by 14anu-ban-05 Prajna Jha MTech CS Banasthali Vidyapith on 14-7-2014
;Mrs. Meadow's pet moved past her and out of the bedroom.[COCA]
;SrImawI Meadow kA pAlawU jAnavara unake pAsa se ho kara Sayana-kakRa ke bAhara calA gayA.
(defrule past20
(declare (salience 6800))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id preposition)
(kriyA-past_saMbanXI ? ?)	;replaced '?id' to '?' by 14anu-ban-09 on (11-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pAsa_se_ho_kara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past20   "  ?id " pAsa_se_ho_kara )" crlf))
)

;@@@ Added by 14anu01 on 27-06-2014
; it is twenty past one. 
;एक  बजकर बीस मिनट है.
(defrule past21
(declare (salience 5000))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(id-cat_coarse =(- ?id 1) number)
(id-cat_coarse =(+ ?id 1) number)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bajakara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past21   "  ?id "  bajakara )" crlf))
)

;;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;;Pussy slept so much that Sidey had grown quite unafraid of her and walked past without bothering to see how near old Pussy was. [Gyannidhi]
;;उसके सोते रहने के कारण दुमछल निडर हो गया था और इस बात की परवाह किये बिना यह देखने के लिये बगल से गुजरा कि बिल्ली उसके कितने पास में हैं।
;(defrule past19
;(declare (salience 3400))
;(id-root ?id past)
;?mng <-(meaning_to_be_decided ?id)
;(id-cat_coarse ?id noun)
;(id-root ?id1 walk)
;(kriyA-object  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (affecting_id-affected_ids-wsd_group_root_mng ?id1 ?id bagala_se_gujarA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " past19.clp  past19  "  ?id1 "  " ?id "  bagala_se_gujarA )" crlf))
;)

;@@@ Added by 14anu-ban-09 on (20-03-2015)
;NOTE-Parser problem. Run on parser no. 16.
;It was past midnight when we got home. 	[oald]
;हम मध्यरात्री के बाद था घर पहुँचे .  			[manual]
(defrule past23
(declare (salience 4900))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id1)(preposition ?id))
(kriyA-past_saMbanXI  ? ?id1)
(id-root ?id1 midnight)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_bAxa ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past23   "  ?id "  ke_bAxa  )" crlf))
)

;@@@ Added by 14anu-ban-09 on (20-03-2015)
;NOTE-Parser problem. Run on parser no. 2.
;There's a bus at twenty minutes past the hour.	[oald]
;एक घण्टा बज के बीस मिनट पर एक बस है .  		[manual]
(defrule past24
(declare (salience 4900))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(viSeRya-past_saMbanXI  ?id1 ?id2)
(id-root ?id1 minute)
(id-root ?id2 hour)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baja_ke ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past24   "  ?id "  baja_ke  )" crlf))
)


;@@@ Added by 14anu-ban-09 on (20-03-2015)
;Cathy flew past me in the corridor. 	[cald]
;कैथी गलियारे में मेरे बगल से तेज़ रफ्तार से निकली.	[manual]
(defrule past25
(declare (salience 4900))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(kriyA-past_saMbanXI  ?id1 ?id2)
(id-root ?id1 fly)
(id-root ?id2 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bagala_se ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past25   "  ?id "  bagala_se  )" crlf))
)


;@@@ Added by 14anu-ban-09 on (20-03-2015)
;NOTE-Run on parser no. 5.
;We live in the house just past the church.	[oald]
;हम गिरजाघर के ठीक आगे वाले घर  में रहते हैं . 		[manual]
(defrule past26
(declare (salience 4900))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(pada_info (group_head_id ?id2)(preposition ?id))
(viSeRya-past_saMbanXI  ?id1 ?id2)
(id-root ?id1 ?str)
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
(id-root ?id2 ?str1)
(test (and (neq (numberp ?str1) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id ke_Age ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past26   "  ?id "  ke_Age )" crlf))
)


;@@@Added by 14anu18
;I don't want to know about his past.
;मैं उसके कल के बारे में जानना नहीं चाहता हूँ 
(defrule past22
(declare (salience 4900))
(id-root ?id past)
?mng <-(meaning_to_be_decided ?id)
(id-word =(- ?id 1) his|her|their|our)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  past.clp 	past22   "  ?id "  kala )" crlf))
)



;"past","Prep","1.pare"
;This issue is past her understanding.
;--"2.se aXika"
;Her grandmother is past eighty five.
;
;
;LEVEL 
;
;
;Headword : past
;
;Examples --
;
;'past' Sabxa kA sUwra banAne kA prayAsa:-
;
;"past","N","1.vigawa" 
;She has been there many times in the past.
;vaha bahuwa bAra vigawa meM vahAz rahI hE.<--BUwakAla
;--"2.awIwa 
;We know nothing of her past.
;hama usake awIwa ke viRaya meM kuCa nahIM jAnawe.<--BUwakAla
;--"3.BUwakAlika(past tense) 
;The past of the verb 'take'is'took'.past tense)
;kriyA Sabxa 'take'kA BUwakAlika Sabxa' took' hE.<--BUwakAlika
;
;"past","adj","1.bIwa gayA" 
;The time for the meeting is past.
;goRTI kA samaya bIwa gayA hE.<--bIwa gayA hE<--BUwakAla
;--"2.piCalA" 
;The past month you had been to Delhi. 
;wuma piCale mahIne xillI gaye We.<--bIwa gayA hE<--BUwakAla
;--"3.BUwapUrva"
;Both past && present students of the college  attended the function.
;kAleja ke PaMkaSana meM xonoM BUwapUrva Ora varwamAna CAwroM ne BAga liyA. <--bIwA huA<--BUwakAlika
;
;"past","prep","1.ke bAxa(waka) 
;It was past midnight when we got home.
;hama loga AXIrAwa ke bAxa Gara pahuzce.<--ke bAxa
;--"2.ke pAra" 
;If you look past the church you'll see the cinema hall. 
;agara wuma carca ke pAra xeKo wo wumhe sinamA hAla xiKAI xegA.<--ke pare<--bAxa meM 
;--"3.ke pare" 
;He didn't get past the first question in the exam.
;parIkRA kA praWama praSna hI usake ximAga ke pare WA.<--ke pare<--bAxa meM
;
;nota:-'past'ke saBI vAkyoM kA nirIkRaNa karane ke uparAnwa isa niRkarRa para pahuzcA jA 
;   sakawA hE ki aXikAMSa saMjFA,kriyA evaM viSeRaNa, vAkyoM kA nABikIya arWa<--BUwakAla   se liyA jA sakawA hE.
;      isake liye sUwra nimna prakAra liKA jA sakawA hE
;        
;sUwra : bAxa_meM[<BUwakAla]
;
;
;
