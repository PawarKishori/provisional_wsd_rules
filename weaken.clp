
;@@@ Added by 14anu-ban-11 on (23-04-2015)
;She felt her legs weaken.(oald)
;उसने महसूस किया उसकी टाँगें शक्तिहीन हैं . (self)
(defrule weaken1
(declare (salience 10))
(id-root ?id weaken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 leg)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SakwihIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weaken.clp 	weaken1   "  ?id "  SakwihIna)" crlf))
)

;@@@ Added by 14anu-ban-11 on (23-04-2015)
;Don't weaken.(oald) 
;निर्बल मत  बनो.(self) 
(defrule weaken2
(declare (salience 20))
(id-root ?id weaken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_niReXaka  ?id ?id1)
(id-root ?id1 not)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nirbala))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weaken.clp 	weaken2   "  ?id "  nirbala)" crlf))
)

;------------------------------- Default Rules ---------------------------------

;@@@ Added by 14anu-ban-11 on (23-04-2015)
;The new evidence weakens the case against her.(oald)
;नया साक्ष्य उसके विरुद्ध मामला कमजोर है . (self)
(defrule weaken0
(declare (salience 00))
(id-root ?id weaken)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  weaken.clp   weaken0   "  ?id "  kamajora)" crlf))
)


