
(defrule find0
(declare (salience 5000))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id finding )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jAzca_pariNAma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  find.clp  	find0   "  ?id "  jAzca_pariNAma )" crlf))
)

;"finding","N","1.jAzca_pariNAma"
;Findings of the enquiry are not in favour of the accused.
;
;
(defrule find1
(declare (salience 4900))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saccAI_kA_pawA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find1  "  ?id "  " ?id1 "  saccAI_kA_pawA_lagA  )" crlf))
)

;He had been stealing for years,but eventually they found him out. 
;vaha sAloM se corI kara rahA WA para unhone saccAI kA pawA lagA liyA
(defrule find2
(declare (salience 4800))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 against)
(kriyA-against_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEsalA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " find.clp find2 " ?id "  PEsalA_kara )" crlf)) 
)

(defrule find3
(declare (salience 4700))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 against)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PEsalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find3  "  ?id "  " ?id1 "  PEsalA_kara  )" crlf))
)

(defrule find4
(declare (salience 4600))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-for_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEsalA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " find.clp find4 " ?id "  PEsalA_kara )" crlf)) 
)

(defrule find5
(declare (salience 4500))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PEsalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find5  "  ?id "  " ?id1 "  PEsalA_kara  )" crlf))
)

(defrule find6
(declare (salience 4400))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " find.clp find6 " ?id "  pawA_kara )" crlf)) 
)

(defrule find7
(declare (salience 4300))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find7  "  ?id "  " ?id1 "  pawA_kara  )" crlf))
)

(defrule find8
(declare (salience 4200))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 thief)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find8   "  ?id "  pawA_lagA )" crlf))
)

(defrule find9
(declare (salience 4100))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " find.clp find9 " ?id "  pawA_kara )" crlf)) 
)

(defrule find10
(declare (salience 4000))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find10  "  ?id "  " ?id1 "  pawA_kara  )" crlf))
)

;Modified by sheetal(26-02-10)
;Police are still hoping to find the dead woman's killer .
(defrule find11
(declare (salience 3900))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pA))
(assert (kriyA_id-object_viBakwi ?id ko));added by sheetal
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find11   "  ?id "  pA )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  find.clp      find11   "  ?id "  ko )" crlf))


(defrule find12
(declare (salience 3800))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Koja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find12   "  ?id "  Koja )" crlf))
)

;"find","N","1.Koja"
;Our new flat is a real find at the rent being charged.
;
;
(defrule find13
(declare (salience 3700))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saccAI_kA_pawA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find13  "  ?id "  " ?id1 "  saccAI_kA_pawA_lagA  )" crlf))
)

;He had been stealing for years,but eventually they found him out. 
;vaha sAloM se corI kara rahA WA para unhone saccAI kA pawA lagA liyA
(defrule find14
(declare (salience 3600))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 against)
(kriyA-against_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEsalA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " find.clp find14 " ?id "  PEsalA_kara )" crlf)) 
)

(defrule find15
(declare (salience 3500))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 against)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PEsalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find15  "  ?id "  " ?id1 "  PEsalA_kara  )" crlf))
)

(defrule find16
(declare (salience 3400))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-for_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PEsalA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " find.clp find16 " ?id "  PEsalA_kara )" crlf)) 
)

(defrule find17
(declare (salience 3300))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 for)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 PEsalA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find17  "  ?id "  " ?id1 "  PEsalA_kara  )" crlf))
)

(defrule find18
(declare (salience 3200))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " find.clp find18 " ?id "  pawA_kara )" crlf)) 
)

(defrule find19
(declare (salience 3100))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find19  "  ?id "  " ?id1 "  pawA_kara  )" crlf))
)

(defrule find20
(declare (salience 3000))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id found )
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pA))
(assert (id-H_vib_mng ?id yA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  find.clp  	find20   "  ?id "  pA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-H_vib_mng  " ?*prov_dir* "  find.clp       find20   "  ?id " yA )" crlf))
)

;given_word=found && word_category=verb	$sWApanA_kara)

;"found","VT","1.sWApanA_karanA"
;Her father founded the hospital in 1958.
;
(defrule find21
(declare (salience 2900))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 thief)
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_lagA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find21   "  ?id "  pawA_lagA )" crlf))
)

(defrule find22
(declare (salience 2800))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pawA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " find.clp find22 " ?id "  pawA_kara )" crlf)) 
)

(defrule find23
(declare (salience 2700))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp	find23  "  ?id "  " ?id1 "  pawA_kara  )" crlf))
)

(defrule find24
(declare (salience 2600))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find24   "  ?id "  pA )" crlf))
)

(defrule find25
(declare (salience 2500))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Koja))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find25   "  ?id "  Koja )" crlf))
)

;"find","N","1.Koja"
;Our new flat is a real find at the rent being charged.
;
;

;@@@ Added by 14anu-ban-05 on (18-08-2014)
;To find the sum A+B, we place vector B so that its tail is at the head of the vector A, as in Fig. 4.4(b).[NCERT physics]
;yoga @A+@B jFAwa karane ke lie ciwra 4.4(@b) ke anusAra hama saxiSa @B isa prakAra raKawe hEM kiu sakI pucCa saxiSa @A ke SIrRa para ho 
(defrule find26
(declare (salience 5000))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(to-infinitive  ?id1 ?id)
(id-root ?id1 to)
(kriyA-object  ?id ?id2)
(id-root ?id2 sum|product|result)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jFAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find26   "  ?id "  jFAwa_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (18-08-2014)
;In addition to finding the facts by observation and experimentation, physicists attempt to discover the laws that summarise (often as mathematical equations) these facts. [NCERT]
;prekRaNoM waWA prayogoM xvArA waWyoM ko Kojane ke sAWa - sAWa BOwika vijFAnI una niyamoM kI Koja karane kA prayAsa karawe hEM jo ina waWyoM kA sAra (prAyaH gaNiwIya samIkaraNoM meM) hoM.
;The predicament was particularly galling when it came to finding suitable husbands for the daughters .[PARALLEL CORPUS]
;यह कष्टकर स्थिति उन सबके लिए और भी बदतर हो गई जब उन्हें अपनी कन्याओं के लिए सुयोग्य वर की खोज करने में कठिनाई होने लगी .[MANUAL TRANSLATION]
(defrule find27
(declare (salience 5000))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(or (and (viSeRya-to_saMbanXI  ?id1 ?id)(id-root ?id1 addition|view)) (and (kriyA-to_saMbanXI  ?id2 ?id)(id-root ?id2 come)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Koja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find27   "  ?id "  Koja_kara )" crlf))
)


;@@@ Added by 14anu-ban-05 on (18-08-2014)
;Along the way, you will find out why blacksmiths heat the iron ring before fitting on the rim of a wooden wheel of a bullock cart and why the wind at the beach often reverses direction after the sun goes down.  [NCERT]
;aXyayana karawe Apa yaha BI jFAwa kareMge ki kisI bElagAdZI ke lakadZI ke pahie kI nemi para lohe kI riMga caDZAne se pahale lohAra ise wapwa kyoM karawe hEM, waWA sUrya Cipane ke paScAw samuxra watoM para pavana prAyaH apanI xiSA uwkramiwa kyoM kara lewI hEM ?
(defrule find28
(declare (salience 5000))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-root ?id1 out)
(kriyA-subject  ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jFAwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " find.clp    find28  "  ?id "  " ?id1 "   jFAwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-08-2014)
;Try to find more such examples and identify the fulcrum, the effort and effort arm, and the load and the load arm of the lever in each case[NCERT]
;kuCa anya uwwolakoM ke uxAharaNa apane pariveSa se DUzDie ; prawyeka ke lie unake Alamba, BAra, BAra - BujA, prayAsa Ora prayAsa - BujA kI pahacAna kIjie.
(defrule find29
(declare (salience 5000))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyArWa_kriyA  ?id1 ?id)
(id-root ?id1 try|want|visit|help|wish)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DUzDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find29   "  ?id "  DUzDa )" crlf))
)

;@@@ Added by 14anu-ban-05 on (18-08-2014)
;We shall find that there is at best a loose correlation between the physical definitions and the physiological pictures these terms generate in our minds.  
;hama yaha xeKefge ki ina paxoM kI BOwika pariBARAoM waWA inake xvArA maswiRka meM bane kAryakIya ciwraNoM ke bIca aXika se aXika yaha sambanXa alpa hI howA hE.
(defrule find30
(declare (salience 5000))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-vAkyakarma  ?id ?id1)
(id-root ?id1 be)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find30   "  ?id "  xeKa )" crlf))
)


;@@@ Added by 14anu-ban-05 on (09-04-2015)
;We need to find a useful role for the volunteers in the campaign. 	[OALD]
;हमें अभियान में स्वयंसेवक के लिए एक उपयोगी कार्य ढूढने की जरूरत है .	[MANUAL]
(defrule find31
(declare (salience 5001))
(id-root ?id find)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(to-infinitive  ?id1 ?id)
(id-root ?id1 to)
(kriyA-object  ?id ?id2)
(id-root ?id2 role)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id DUDa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  find.clp 	find31   "  ?id "  DUDa )" crlf))
)
