
;"frozen","Adj","1.jamI_huI"
;Skating is done on frozen ice.
(defrule freeze0
(declare (salience 5000))
(id-root ?id freeze)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id frozen )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id jamI_huI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  freeze.clp  	freeze0   "  ?id "  jamI_huI )" crlf))
)

;$$$ Modified by 14anu-ban-05 on (11-03-2015)
;She saw someone outside the window and froze.[OALD]
;वह खिड़की के बाहर किसी को देखा और जम गयी. [manual]
(defrule freeze1
(declare (salience 4900))
(id-root ?id freeze)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 he)     			;commented by 14anu-ban-05 on (11-03-2015)
(kriyA-subject ?id ?id1)
(or (id-root ?id2 ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))(id-cat_coarse ?id1 pronoun))	;added by 14anu-ban-05 on (11-03-2015)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jama_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freeze.clp 	freeze1   "  ?id "  jama_jA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (05-11-2014)
;You will also learn what happens when water boils or freezes, and its temperature does not change during these processes even though a great deal of heat is flowing into or out of it.[NCERT]
;Apa yaha BI jAneMge ki kyA howA hE jaba jala ubalawA aWavA jamawA hE waWA ina prakriyAoM kI avaXi meM isake wApa meM parivarwana nahIM howA, yaxyapi kAPI mAwrA meM URmA inake BIwara/inase bAhara pravAhiwa howI hE.[NCERT]
;The pipes have frozen, so we've got no water. [OALD]
;पाइपें जम चुकी हैं, इसलिये हमें पानी नहीं मिला. 		[manual]
;removed (Domain physics) and (assert (id-domain_type ?id physics)) by 14anu-ban-05 on (11-03-2015)
(defrule freeze3
(declare (salience 5000))
;(Domain physics)
(id-root ?id freeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 water|pipe)	;added pipe by 14anu-ban-05 on (11-03-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jama))
;(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freeze.clp 	freeze3   "  ?id "  jama )" crlf)
;(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  freeze.clp       freeze3   "  ?id "  physics )" crlf)
)
)


;@@@ Added by 14anu-ban-05 on (11-03-2015)
;The clothes froze solid on the washing line. [OALD]
;वस्त्र  कपडे सुखाने की रस्सी पर ठोस रुप मे जम गये .		[manual]

(defrule freeze4
(declare (salience 5001))
(id-root ?id freeze)
(id-cat_coarse ?id verb)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) solid)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) Tosa_rupa_meM_jama_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " freeze.clp  freeze4  "  ?id "  " (+ ?id 1) "  Tosa_rupa_meM_jama_jA  )" crlf))
)

;@@@ Added by 14anu-ban-05 on (11-03-2015)
;The smile froze on her lips. [OALD]
;उसके होंठों पर  मुस्कुराहट स्थिर हो गई.  [manual]

(defrule freeze5
(declare (salience 4901))
(id-root ?id freeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 lip)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWira_ho_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freeze.clp 	freeze5   "  ?id "  sWira_ho_jA )" crlf))
)

;@@@ Added by 14anu-ban-05 on (11-03-2015)
;The government has imposed a freeze on wage increases.[cald]
;सरकार ने वेतन वृद्धि पर प्रतिबंध लागू किया है . . [manual]

(defrule freeze6
(declare (salience 4000))
(id-root ?id freeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)	
(id-root ?id1 impose)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prawibaMXa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freeze.clp 	freeze6   "  ?id "  prawibaMXa )" crlf))
)


;@@@ Added by 14anu-ban-05 on (11-03-2015)
;The government has frozen pensions until the end of next year.[cald]
;सरकार ने अगले वर्ष के अन्त तक के लिये निवृत्ति वेतन को रोक दिया हैं . 

(defrule freeze7
(declare (salience 4902))
(id-root ?id freeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)	
(id-root ?id1 pension)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freeze.clp 	freeze7   "  ?id "  roka_xe )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-05 on (11-03-2015)
;A freeze warning was posted for Thursday night. [OALD]
;एक जमाने वाली ठण्ड की चेतावनी गुरुवार की रात के लिए  किया गया है.	[manual]

(defrule freeze9
(declare (salience 3000))
(id-root ?id freeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jamAne_vAlI_TaNda))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freeze.clp 	freeze9   "  ?id "  jamAne_vAlI_TaNda )" crlf))
)

;"freeze","VT","1.jamA_xenA"
;I would freeze to death in this office if the air condition is turned on for 24
;hours.
(defrule freeze2
(declare (salience 4800))
(id-root ?id freeze)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jamA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  freeze.clp 	freeze2   "  ?id "  jamA_xe )" crlf))
)


