;@@@ Added by 14anu-ban-06 (13-04-2015)
;The cold water invigorated him.(OALD)
;ठण्डे पानी ने उसको स्फूर्ति से भर दिया . (manual)
(defrule invigorate2
(declare (salience 5100))
(id-root ?id invigorate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?)
(kriyA-subject ?id ?id1)
(id-root ?id1 water|walk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sPUrwi_se_Bara_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invigorate.clp 	invigorate2   "  ?id "  sPUrwi_se_Bara_xe )" crlf))
)

;@@@ Added by 14anu-ban-06 (13-04-2015)
;We were invigorated by our walk. (cambridge)
;हम हमारी सैर के द्वारा स्फूर्ति से भर गये थे . (manual)
(defrule invigorate3
(declare (salience 5200))
(id-root ?id invigorate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-by_saMbanXI ?id ?id1)
(id-root ?id1 walk)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sPUrwi_se_Bara_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invigorate.clp 	invigorate3   "  ?id "  sPUrwi_se_Bara_jA )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;"invigorating","Adj","1.balavarXaka"
;Invigorating herbs are good for health.
(defrule invigorate0
(declare (salience 5000))
(id-root ?id invigorate)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id invigorating )
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id balavarXaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  invigorate.clp  	invigorate0   "  ?id "  balavarXaka )" crlf))
)

;"invigorate","VT","1.balavarXana_karanA"
;Yoga invigorates health .
(defrule invigorate1
(declare (salience 4900))
(id-root ?id invigorate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id balavarXana_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  invigorate.clp 	invigorate1   "  ?id "  balavarXana_kara )" crlf))
)

