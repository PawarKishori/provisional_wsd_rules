;@@@ Added by 14anu-ban-04  (02-12-2014) 
;A crowd of devotees engulf Haridwar during the time of daily prayer in the evening .             [tourism-corpus]
;हरिद्वार  में  प्रतिदिन  शाम  को  होने  वाली  माँ  गंगा  की  आरती  के  समय  श्रद्धालुओं  की  भीड़  उमड़  पड़ती  है  ।                      [tourism-corpus]
(defrule engulf1
(declare (salience 100))
(id-root ?id engulf)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?sub)     
(id-root ?sub crowd)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id umada_pada))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engulf.clp  engulf1   "  ?id "  umada_pada )" crlf))
)

;@@@ Added by 14anu-ban-04 (02-12-2014)
;At night when darkness engulfs all around then the roaming wild animals moving outside the window can be watched freely .  [tourism-corpus]
;रात  में  जब  चारों  तरफ  अँधेरा  छा  जाता  है  तब  कमरे  की  बंद  खिड़की  से  बाहर  विचरण  करते  जंगली  जानवरों  को  स्वछंद  निहारा  जा  सकता  है  ।       [tourism-corpus]
(defrule engulf2
(declare (salience 200))
(id-root ?id engulf)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 darkness|silence|fog|greenery)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CA_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engulf.clp  engulf2   "  ?id "  CA_jA )" crlf))
)


;@@@ Added by 14anu-ban-04 (02-12-2014)
;Dudhatoli gets engulfed with snow in winters .         [tourism-corpus]
;दूधातोली  सर्दियों में बर्फ से ढक जाता है ।                    [tourism-corpus]
(defrule engulf3
(declare (salience 300))
(id-root ?id engulf)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-with_saMbanXI ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Daka_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engulf.clp  engulf3   "  ?id "  Daka_jA )" crlf))
)

;@@@ Added by 14anu-ban-04 (02-12-2014)
;The charming place Danda Nagaraja is engulfed with the trees of banyan , pipal and dense pine .          [tourism-corpus]
;डांडा नागराजा रमणीक स्थल वट , पीपल और घने चीड़ के पेड़ों से आच्छादित है .              [tourism-corpus]
;डांडा नागराजा रमणीक स्थल है जो पीपल और घने चीड़ के पेड़ों से  घिरा   हुआ है .                [self] 
(defrule engulf4
(declare (salience 300))
(id-root ?id engulf)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-karma ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id GirA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engulf.clp  engulf4   "  ?id "  GirA )" crlf))
)

;@@@ Added by 14anu-ban-04 (09-03-2015)
;We didn't come to know when sleep engulfed us after daylong fatigue in the comfortable warm room. [tourism-corpus]
;दिन  भर  की  थकान  व  आरामदायक  गर्म  कमरे  में  नींद  कब  आई  पता  ही  नहीं  लगा  ।               [tourism-corpus]
;हमें पता ही नहीं चला  कि दिन  भर  की  थकान  के बाद  हमें आरामदायक  गर्म  कमरे  में  नींद कब आई |        [manual]
(defrule engulf5
(declare (salience 400))
(id-root ?id engulf)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 sleep)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id A))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engulf.clp  engulf5  "  ?id "  A" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-04  (02-12-2014) 
;[NOTE] not yet find any example sentence related to this rule.
(defrule engulf0
(declare (salience 90))
(id-root ?id engulf)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nimagna_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  engulf.clp  engulf0   "  ?id "  nimagna_kara )" crlf))
)

