;@@@ Added by 14anu-ban-03 (05-02-2015)
;In our country people of all races,colour,and creed,live in harmony. [hinkhoj]
;हमारे देश में सभी वर्गों ,रंग,और धर्म, के लोग तालमेल से रहते हैं . [Manaul]
(defrule creed0
(declare (salience 00))  
(id-root ?id creed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Xarma))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  creed.clp 	creed0   "  ?id "  Xarma )" crlf))
)

;@@@ Added by 14anu-ban-03 (05-02-2015)
;These art patterns are seen carved on the large stones in the Maitrey and statues of the Dhyani Buddhist creed located at several places like Karshekhar , Vyamakhar , Sumdhung , Sani , Phe , Tonde , Jangla , Muni etc. . [tourism]
;इन कला शैलियों का अनेक स्थानों यथा करशेखर , व्यामाखर , सुमढुंग , सानी , फे , तोण्डे , जंगला , मुनि इत्यादि में स्थित विशाल चट़्टानों पर उत्कीर्ण मैत्रेय तथा ध्यानी बौद्ध मत की अन्य प्रतिमाओं में देखने को मिलता है . [tourism]
(defrule creed1
(declare (salience 00))  
(id-root ?id creed)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id1 ?id)
(id-root ?id1 statue)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mawa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  creed.clp 	creed1   "  ?id "  mawa )" crlf))
)



