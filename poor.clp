;$$$ Modified by 14anu-ban-09 on (10-09-2014)
;Naidu has done us a favour by drawing attention to a problem that could turn into a rich poor division among our states.
;नायडू ने उस समस्या की ओर हमारा ध्यान खींचकर सराहनीय काम किया है जिससे हमारे राज्यों के बीच गरीब - अमीर की खाई चौडी होती है.
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;Most of the world's poorest countries are in Africa.  [Cambridge]
;विश्व के सबसे अधिक गरीब देश ज्यादातर अफ्रीका में हैं .
;$$$ Modified by 14anu17
;The poor man was crying due to lack of money.
;गरीब आदमी पैसे की कमी के कारण रो रहा था . 
(defrule poor0
(declare (salience 5000))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-cat_coarse ?id adjective) ;added by 14anu-ban-09
(or(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))) ;@@@added
(id-root ?id1 farmer|country|person|city|family|division) ;added 'division' by 14anu-ban-09 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garIba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp      poor0   "  ?id "  garIba )" crlf))
)


(defrule poor1
(declare (salience 4900))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 result|performance)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp      poor1   "  ?id "  KarAba )" crlf))
)



;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;That cold sounds terrible, you poor thing! [Cambridge]
;वह ठण्ड भयानक है,आप बेचारे.
(defrule poor2
(declare (salience 4800))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 thing)
(or(viSeRya-viSeRaNa  ?id1 ?id)(kriyA-object  ?id ?id1))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id becArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp      poor2   "  ?id "  becArA )" crlf))
)

;$$$ Modified by 14anu-ban-09 on (10-09-2014)
;$$$ Modified by Sonam Gupta MTech IT Banasthali 2013 (modified relation)
;Poor Health. [OALD] ;Parser problem as poor is an adjective but considering as noun.
;खराब स्वास्थ्य . 
;Added by Meena(12.11.10)
;There is little doubt that poor medical treatment hastened her death.
;para WodA sanxaha hE ki KarAba cikiwsIya upacAra ke calawe usakI mqawyu ho gaI.[ Own Manual translation by 14anu-ban-09 ]
(defrule poor3
(declare (salience 4700))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(or(and(viSeRya-viSeRaNa ?id1 ?id)(id-root ?id1 treatment))(viSeRya-viSeRaNa ?id ?id1)) ;added 'and' condition by 14anu-ban-09
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KarAba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp      poor3   "  ?id "  KarAba )" crlf))
)

;@@@Added by 14anu07 0n 04/07/2014
;Naidu has done us a favour by drawing attention to a problem that could turn into a rich poor division among our states.
;नायडू ने उस समस्या की ओर हमारा ध्यान खींचकर सराहनीय काम किया है जिससे हमारे राज्यों के बीच गरीब - अमीर की खाई चौडी होती है.
(defrule poor11
(declare (salience 5000))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) rich)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (- ?id 1) amIra_garIba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " poor.clp    poor11  "  ?id "  " (- ?id 1) "  amIra_garIba)" crlf))
)

;$$$ Modified by 14anu07
;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;I was always very poor at maths at school.  [Cambridge]
;मैं विद्यालय पर हिसाब पर हमेशा अत्यन्त कमजोर था . 
(defrule poor4
(declare (salience 5600)) ;salience increased '4600' to '5600' by 14anu-ban-09 on (01-01-2015) because rule poor9 was firing.So, to stop it increased the salience.
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative);modified id-cat_coarse to id-cat by 14anu07 
(id-root ?id1 maths|english|hindi|geography|civics|history|computer)
(viSeRya-at_saMbanXI  ?id ?id1)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kamajora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp 	poor4   "  ?id "  kamajora )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 2013
;At last month's meeting, attendance was poor. [Cambridge]
;पिछले महीने की सभा में, उपस्थिती कम थी.
(defrule poor5
(declare (salience 4500))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
;(id-root ?id1 attendance|presence|crowd)
(or(subject-subject_samAnAXikaraNa  ? ?id)(viSeRya-in_saMbanXI  ?id ?))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kama))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp 	poor5   "  ?id "  kama )" crlf))
)


;Salience reduced by Meena(12.11.10)
(defrule poor6
(declare (salience 4400))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-cat ?id adjective|adjective_comparative|adjective_superlative)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp 	poor6   "  ?id "  xIna )" crlf))
)

;"poor","Adj","1.xIna/xarixra"
;According to me, being poor is a curse for a human being.
;--"2.alpa"
;This soil is poor in nutrients.
;--"3.KarAba"
;They sold very poor quality wool.
;--"4.aBAgA/becArA"
;The poor girl has nothing to eat.
;


;$$$ Modified by 14anu-ban-01 on (31-03-2016): merged poor7 and poor12
;And she declared that out of love for the poor she had gotten her family to go against convention . 
;और उसने घोषित किया कि गरीबों के प्रति प्रेम के कारण उसने परम्परा के विरुद्ध जाने के लिए उसके परिवार को प्रेरित किया था .	[self-improvised]
;They provided food and shelter for the poor.
;उन्होंने गरीबों के लिए आहार और आश्रय उपलब्ध कराया .	[self-improvised]
;We should help the poor. 	[sd_verified]
;हमें गरीबों की सहायता करनी चाहिए . 	[self-improvised]
;Added by Meena(17.3.10)
;And she declared that out of love for the poor she had gotten her family to go against convention . 
(defrule poor7
(declare (salience 4700))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) the)
(not(id-cat_coarse =(+ ?id 1) noun))
(viSeRya-det_viSeRaNa ?id =(- ?id 1))
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id garIboM/xInoM))	;added meaning garIboM by 14anu-ban-01 
(assert (id-wsd_number ?id p))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  poor.clp      poor7   "  ?id "  garIboM/xInoM )" crlf)	;added meaning garIboM by 14anu-ban-01 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_number   " ?*prov_dir* "  poor.clp      poor7   "  ?id "  p )" crlf))
)

;Merged poor12 with poor7 by 14anu-ban-01 on (31-03-2016)
;;$$$ Modified by 14anu17
;;And she declared that out of love for the poor she had gotten her family to go against convention . 
;;और उसने घोषित किया कि गरीब के प्रति प्रेम के कारण प्रेम के बाहर उसने परम्परा के विरुद्ध जाने के लिए उसके परिवार को प्रेरित किया था . 
;;They provided food and shelter for the poor.
;;उन्होंने गरीब के लिए आहार और सुरक्षा व्यवस्था किए .
;(defrule poor12
;(declare (salience 4900))
;(id-root ?id poor)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id ?id1) 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id garIba))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp    poor12   "  ?id "  garIba )" crlf))
;)



;@@@ Added by 14anu11
;The person who took up a particular job should do it with sincerity and should share a part of his earnings with the jangomas and the poor .
;व्यक्ति जो भी काम अखितयार करे उसे ईमानदारी से करे तथा उसका एक हिस्सा जंगमों को और गरीबों को दे .
(defrule poor13
(declare (salience 5500))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) the)
(not(id-cat_coarse =(+ ?id 1) noun))
(viSeRya-det_viSeRaNa ?id =(- ?id 1))
(kriyA-with_saMbanXI  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id garIbo))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng   " ?*prov_dir* "  poor.clp      poor13   "  ?id "  garIbo )" crlf)
))

;Salience reduced by Meena(17.3.10)
(defrule poor8
(declare (salience 0))
;(declare (salience 4900))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp 	poor8   "  ?id "  xIna )" crlf))
)


;@@@ Added by Sonam Gupta MTech IT Banasthali 5-2-2014
;If it were urged that the tax-payers of Bengal were too poor to be able to pay for the 
;advantages of such an improved education, the answer of the Commission was that, Bengal 
;was too poor to be able to afford the waste of ability which was caused by the. present system. [gyannidhi]
;यदि इस बात पर ज़ोर दिया जाता कि बंगाल के करदाता इतनी विकसित  शितक्षा प्रणाली के लाभ की कीमत चुकाने के लिए बपहुत निर्धन थे, 
;तो आयोग का उत्तर था कि बंगाल इतना निर्धन है कि वह वर्तमान पद्धति के कारण होने वाले क्षमता के अपव्यय को बर्दाश्त नहीं कर सकता।
(defrule poor9
(declare (salience 5500))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaka  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garIba))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp 	poor9   "  ?id "  garIba )" crlf))
)

;@@@ Added by 14anu-ban-09 on (05-08-2014)
;Gases are poor thermal conductors while liquids have conductivities intermediate between solids and gases.
;gEseM alpa URmA cAlaka howI hEM waWA xravoM kI cAlakawA TosoM waWA gEsoM ke bIca kI howI hE.
;Recall that gases are poor conductors, and note the low thermal conductivity of air in the Table 11.5.  
;yAxa kIjie gEseM alpa cAlaka howI hEM waWA sAraNI 11.5 se vAyu kI nimna URmA cAlakawA nota kIjie.

(defrule poor10
(declare (salience 5000))
(id-root ?id poor)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 conductor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id alpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  poor.clp      poor10   "  ?id "  alpa )" crlf))
)
;"poor","N","1.xIna/xarixra"
;According to me, being poor is a curse for a human being.
;--"2.alpa"
;This soil is poor in nutrients.
;--"3.KarAba"
;They sold very poor quality wool.
;--"4.aBAgA/becArA"
;The poor girl has nothing to eat.
;
