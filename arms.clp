;##############################################################################
;#  Copyright (C) 2002-2005 Garima Singh (gsingh.nik@gmail.com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################
;Commented by 14anu07
;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 
;He asked the shepherd to let him hold one of the lambs, and, clutching it in his arms, began to walk with the shepherd.[gyananidhi]
;उसने  भेड़ के एक बच्चे को पकड़ने के लिए शेफर्ड पूछा, और, अपनी बाहों में उसे पकड़कर शेफर्ड के साथ चलना शुरू किया.
;(defrule arms1
;(declare (salience 5000))
;?mng <-(meaning_to_be_decided ?id)
;(kriyA-in_saMbanXI  ?kri ?id)
;(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
;(id-root ?kri clutch|hold|put|carry|tie) ;this condition will be needed if any counter example is found 
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id bAzha))
;(assert (id-wsd_root ?id arm))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arms.clp 	arms1   "  ?id "  bAzha )" crlf))
;)

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 17-jan-2014
;@@@ Modified by Karishma Singh MNNIT Allahabad on 12/06/2014
;She discovered her lover in the arms of another woman. 
;वह एक और स्त्री की बाँहों में उसके प्रेमी को पाई . 
(defrule arms2
(declare (salience 5000))
?mng <-(meaning_to_be_decided ?id)
(id-root ?id arms)
;(viSeRya-in_saMbanXI  ?id1 ?id)
;(viSeRya-RaRTI_viSeRaNa  ?id ?id2)
(kriyA-in_saMbanXI  ?id1 ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAzha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arms.clp 	arms2   "  ?id "  bAzha )" crlf))
)

;@@@ Added by 14anu-ban-02(10-09-2014)
;If you bring back your arms closer to your body, the angular speed increases again.[ncert]
;यदि आप अपनी भुजाओं को फिर शरीर के पास ले आयें तो कोणीय चाल फिर से बढ जाती है.[ncert]
(defrule arms3
(declare (salience 1000))
?mng <-(meaning_to_be_decided ?id)
(viSeRya-RaRTI_viSeRaNa  ?id ?id1)
(id-root ?id1 your)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id BujAoM))
(assert (id-wsd_root ?id arm))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arms.clp 	arms3   "  ?id "  BujAoM )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root   " ?*prov_dir* "  arms.clp      arms3   "  ?id "  arm )" crlf)
)

;@@@Added by 14anu-ban-02(23-02-2015)
;These 'engineers built a chariot on which they placed a magnetic figure with arms outstretched.[ncert 12_05]
;इन 'इंजीनियरों' ने एक रथ बनाया जिस पर उन्होंने चुम्बक की बनी हुई एक प्रतिमा लगाई, जिसका एक हाथ बाहर फैला हुआ था.[ncert]
(defrule arms4
(declare (salience 1000))
?mng <-(meaning_to_be_decided ?id)
(viSeRya-kqxanwa_viSeRaNa  ?id ?id1)	;need examples to restrict the rule.
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAWa))
(assert (id-wsd_root ?id arm))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arms.clp 	arms4   "  ?id "  hAWa )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root   " ?*prov_dir* "  arms.clp      arms4   "  ?id "  arm )" crlf)
)



;****************DEFAULT RULES**************************

;@@@ Added by Garima Singh(M.Tech-C.S, Banasthali Vidyapith) 
(defrule arms0
(declare (salience 0))
(id-root ?id arms)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haWiyAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  arms.clp 	arms0   "  ?id "  haWiyAra )" crlf))
)

