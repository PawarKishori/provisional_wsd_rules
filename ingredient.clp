;@@@ Added by 14anu-ban-06 (13-11-2014)
;But you have to have enough ingredients to make it complicated .(parallel corpus)
;पर आपके पास पर्याप्त उपादान होने चाहिए ताकि इसे आगे जटिल बनाया जा सके .(parallel corpus)
(defrule ingredient0
(declare (salience 0))
(id-root ?id ingredient)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id upAxAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ingredient.clp 	ingredient0   "  ?id "  upAxAna )" crlf))
)

;@@@ Added by 14anu-ban-06 (13-11-2014)
;On the crest of the dome and umbrellas there are famous ingredient of traditional Iranian and Indian architecture that looks good on a Metal urn .(parallel corpus)
;गुम्बद एवं छतरियों के शिखर पर परंपरागत फारसी एवं हिंदू वास्तु कला का प्रसिद्ध घटक एक धात्विक कलश किरीटरूप में शोभायमान है ।(parallel corpus)
;The adornment is made out from the ingredients of either ` Rogan', or ` Gachkari'or by way of carving and by studding of gems .(parallel corpus)
;अलंकरण घटक रोगन या गचकारी से अथवा नक्काशी एवं रत्न जड़ कर निर्मित हैं ।(parallel corpus)
(defrule ingredient1
(declare (salience 2000))
(id-root ?id ingredient)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ingredient.clp 	ingredient1   "  ?id "  Gataka )" crlf))
)

;@@@ Added by 14anu-ban-06 (13-11-2014)
;For example, soybeans are the primary ingredient in many dairy product substitutes (e.g., soy milk,margarine, soy ice cream, soy yogurt, soy cheese, and soy cream cheese) and meat alternatives (e.g. veggie burgers).(agriculture)
;उदाहरण के लिए, सोयाबीन अनेक दैनिक उत्पाद के विकल्प की मुख्य सामग्री है (उदाहरण, सोया दूध, सोया का दही, कृत्रिम मक्खन, सोया पनीर, और सोया मलाई पनीर) और माँस के विकल्प (उदाहरण, वेजी बर्गर).(manual)
(defrule ingredient2
(declare (salience 2100))
(id-root ?id ingredient)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?id1)
(id-root ?id1 substitute)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAmagrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ingredient.clp 	ingredient2   "  ?id "  sAmagrI )" crlf))
)

;@@@ Added by 14anu-ban-06 (11-02-2015)
;This ingredient guards your teeth against decay.(wordreference forum )
;यह घटक क्षय के विरुद्ध आपके दाँतों की रक्षा करते हैं.(manual)
(defrule ingredient3
(declare (salience 2500))
(id-root ?id ingredient)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject ?id1 ?id)
(kriyA-against_saMbanXI ?id1 ?)
(id-root ?id1 guard|protect)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Gataka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  ingredient.clp 	ingredient3   "  ?id "  Gataka )" crlf))
)
