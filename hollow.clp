;Added by Prachi Rathore[25-11-13]
;   Her cheeks have gone hollow after her illness.
;   usakI bImArI ke bAxa usake gAla picaka gaye hEM

(defrule hollow2
(declare (salience 5000))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 cheek)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id picakA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow2   "  ?id "  picakA_huA )" crlf))
)

;@@@ Added by 14anu-ban-06 (19-03-2015)
;The snake was sitting in the hollow at the edge of the garden.(hollow.clp)[parser no. -2]
;साँप उद्यान के किनारे पर गड्ढे में बैठा था . (manual)
(defrule hollow3
(declare (salience 5100))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-at_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gadDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow3   "  ?id "  gadDA )" crlf))
)


;@@@ Added by 14anu-ban-06 (19-03-2015)
;Hollow eyes.(OALD)
;धंसी हुईं आँखें. (manual)
(defrule hollow4
(declare (salience 5100))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 eye)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id XaMsA_huA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow4   "  ?id "  XaMsA_huA )" crlf))
)

;@@@ Added by 14anu-ban-06 (19-03-2015)
;She carefully held the little bird in the hollow of her hand.(hollow.clp)
;उसने सावधानी से उसके हाथ की हथेली के बीच की जगह में छोटी चिडिया को पकडा . (manual)
(defrule hollow5
(declare (salience 5200))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 hand)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id haWelI_ke_bIca_kI_jagaha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow5   "  ?id "  haWelI_ke_bIca_kI_jagaha )" crlf))
)

;@@@ Added by 14anu-ban-06 (19-03-2015)
;The bird flew up && sat in the hollow of the tree.(hollow.clp)
;चिडिया ऊपर उडी और पेड के कोटर में बैठी . (manual)
(defrule hollow6
(declare (salience 5300))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 tree)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kotara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow6   "  ?id "  kotara )" crlf))
)

;@@@ Added by 14anu-ban-06 (19-03-2015)
;The stairs have been hollowed by centuries of use.(OALD)
;सीढियों में  शताब्दियों के उपयोग से गड्ढे पड गये है .(manual) 
(defrule hollow8
(declare (salience 2000))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 stair)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gadDA_padZa))
(assert (kriyA_id-subject_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow8   "  ?id "  gadDA_padZa )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hollow.clp       hollow8   "  ?id " meM )" crlf))
)

;@@@ Added by 14anu-ban-06 (19-03-2015)
;Hollow out the cake and fill it with cream. (OALD)
;केक में छेद कीजिए और उसको मलाई के साथ भरिए .(manual)  
(defrule hollow9
(declare (salience 2500))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Cexa_kara))
(assert (kriyA_id-object_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hollow.clp	hollow9  "  ?id "  " ?id1 "  Cexa_kara  )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hollow.clp       hollow9   "  ?id " meM )" crlf))
)

;@@@ Added by 14anu-ban-06 (20-03-2015)
;The cave has been hollowed out of the mountainside.(OALD) [parser problem]
;गुफा पहाडी ढाल को खोखला कर के बनाई गयी है . (manual)
(defrule hollow10
(declare (salience 2600))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(kriyA-of_saMbanXI ?id ?)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 KoKalA_kara_ke_banA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " hollow.clp	hollow10  "  ?id "  " ?id1 "  KoKalA_kara_ke_banA  )" crlf)
)
)

;@@@ Added by 14anu-ban-06 (20-03-2015)
;The dog found a hollow in the ground to hide in from the wind. (cambridge)[parser no. - 3]
;कुत्ते को हवा से में छिपने के लिए जमीन में गड्ढा मिला . (manual)
(defrule hollow11
(declare (salience 5400))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object ?id1 ?id)
(kriyA-in_saMbanXI ?id1 ?id2)
(id-root ?id2 ground|floor)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gadDA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow11   "  ?id "  gadDA )" crlf))
)
;-------------------------- Default rules ------------------------

;@@@ Added by 14anu-ban-06 (19-03-2015)
;They hollowed the log to make a canoe.(merriam-webster)
;उन्होंने डोंगी बनाने के लिए तने को खोखला किया . (manual)
(defrule hollow7
(declare (salience 0))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KoKalA_kara))
(assert (kriyA_id-object_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow7   "  ?id "  KoKalA_kara )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  hollow.clp       hollow7   "  ?id " ko )" crlf))
)

(defrule hollow0
(declare (salience 5000))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KoKalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow0   "  ?id "  KoKalA )" crlf))
)

(defrule hollow1
(declare (salience 4900))
(id-root ?id hollow)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id KoKalA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hollow.clp 	hollow1   "  ?id "  KoKalA )" crlf))
)

;"hollow","Adj","1.KoKalA"
;yaha bAzsa 'hollow' hE.
;
;LEVEL 
;Headword : hollow
;
;Examples --
;
;1. "KoKalA" 
;  (a)  The termites have eaten the tree hollow.
;       xImaka ne pedZa ko KA kara KoKalA kara xiyA
;  (b)  They heard a strange hollow groan at night.
;       rAwa meM unheM eka ajIba KoKalI karAha sunAI xI
;  (c)  She could make out that he made a hollow promise.
;       use pawA cala gayA WA ki usakA vAxA KoKalA WA. 
;  (d)  We are not scared of your hollow threats.
;       wumhArI KoKalI XamakiyoM se hama nahIM darawe.
;   
;2. "picakA_huA"
;   Her cheeks have gone hollow after her illness.
;   usakI bImArI ke bAxa usake gAla picaka gaye hEM
;
;3. "gadDA"
;   The snake was sitting in the hollow at the edge of the garden.
;   sAzpa bagIce ke kinAre ke gadDe meM bETA WA.
;
;4. "CotI KAI" 
;   They lay in the shady hollow for many hours.
;   ve kaI GaNte CAyAxAra CotI KAI meM lete rahe.
;
;5. "baMxa_haWelI_ke_bIca_kI_jagaha"
;   She carefully held the little bird in the hollow of her hand.
;   usane CotI cidZiyA ko apane cullU meM XyAna se pakade raKA
;
;6. "kotara" 
;   The bird flew up && sat in the hollow of the tree.
;   cidZiyA udZa gayI Ora pedaz kI kotara meM bETa gayI.
;
;
;uparaliKiwa vAkyoM meM "hollow" ke viBinna prayogoM meM aXikawara "KoKalA" kA arWa
;A rahA hE, Ora hama ise mUlArWa mAna sakawe hEM. 
;
;vAkya 6. meM "hollow" kA jo arWa "kotara" A rahA hE. kotara meM KoKalA hone 
;kA prakqwiguNa howA hE. wo aba hama "KoKalA" ke arWa se "kotara" kA arWa prApwa 
;kara sakawe hEM.
;
;TIka isI waraha, vAkya 3. meM "hollow" kA jo arWa "gadDA" A rahA hE usakA
;BI eka prAkqwika guNa hE KoKalApana. wo aba hama "gadDA" ke arWa ko BI "KoKalA" ke
;arWa se prApwa kara sakawe hEM. 
;
;vAkya 2. meM, "hollow" kA prayoga , "picakA huA honA" ke arWa meM ho rahA hE.
;kisI vaswu meM picakane kI waBI guMjAiSa howI hE jaba vaha KoKalI ho, Ora koI vaswu
;jaba picaka jAwI hE, waba usameM gadDe ho jAwe hEM.
;wo aba, vAkya 3. meM "hollow" kA jo arWa prApwa ho rahA hE, usake saMyojana se 
;hama vAkya 2. ke saMxarBa meM "hollow" kA arWa "KoKalA" prApwa kara sakawe hEM.
;
;KoKalA -- > picakA huA -- > gadDA
;
;ina alaga xiKanevAle arWo meM "KoKalA" kA BAva isa prakAra samaJa sakawe hEM:
;
;
;                  KoKalA (guNI) 
;                    |                          
;                  KoKalA (guNa)  =   KoKalApana <--   
;                    |                        |  
;                    | (sAmarWya)             | (guNa)        
;                    |                        |
;                 picakA huA --------------gadDA
;          			(pariNAma)	
;
;							
;aba hama "hollow" kA arWa isa prakAra xe sakawe hEM :
;
;sUwra : KoKalA`
;

