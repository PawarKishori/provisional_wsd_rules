;@@@ Added by 14anu04 on 28-June-2014
;He has proved he can carry through on his promises. 
;उसने सिद्ध किया है कि वह अपने वादों को अन्त तक निभा सकता है. 
(defrule carry_tmp2
(declare (salience 5000))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(id-word =(+ ?id 2) on|with)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 aMwa_waka_niBA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry_tmp2  "  ?id "  " ?id1 "  aMwa_waka_niBA  )" crlf))
)

;@@@ Added by 14anu04 on 28-June-2014
;I got carried away and started shouting at the television.[Oxford]
;मैंनें आपा खो दिया और दूरदर्शन पर चिल्लाना शुरु कर दिया. 
(defrule carry_tmp
(declare (salience 5000))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 away)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1  ApA_Ko_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry_tmp  "  ?id "  " ?id1 "  ApA_Ko_xe  )" crlf))
)

(defrule carry0
(declare (salience 5000))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 forward)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jodZa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry0  "  ?id "  " ?id1 "  jodZa  )" crlf))
)

;This sum is wrong because you haven't carried the 4 forward.
;yaha savAla galawa hE kyoMki wumane isameM cAra nahIM jodZA
(defrule carry1
(declare (salience 4900))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saPala_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry1  "  ?id "  " ?id1 "  saPala_ho  )" crlf))
)

;It was a difficult situation,but she carried it off well.
;yaha eka gazBIra parisWiwi WI lekina vaha usameM saPala rahI



;$$$ Modified by 14anu-ban-03 (12-03-2015)   ;working on parser no.- 4
;They must carry on working.  [same clp]
;unhe apanA kAma jArI raKanA cAhie.   [same clp]
(defrule carry2
(declare (salience 5001))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)   
(kriyA-object ?id ?id2)	 ;added  by 14anu-ban-03 (12-03-2015)
(id-root ?id2 work)   	 ;added  by 14anu-ban-03 (12-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jArI_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry2  "  ?id "  " ?id1 "  jArI_raKa  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (12-03-2015)   ;working on parser no.- 2
;The children often carry on in the absence of a teacher.  [same clp]
;bacce aksara aXyApaka kI anupasWiwi meM burA vyavahAra karawe hEM. [same clp]
(defrule carry3
(declare (salience 5001))   ;salience increased by 14anu-ban-03 (12-03-2015)
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-in_saMbanXI  ?id ?id2)  ;added  by 14anu-ban-03 (12-03-2015)
(id-root ?id2 absence)   ;added  by 14anu-ban-03 (12-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 burA_vyavahAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry3  "  ?id "  " ?id1 "  burA_vyavahAra_kara  )" crlf))
)

;$$$ Modified by 14anu-ban-03 (12-03-2015)   ;working on parser no.- 2
;He was shouting and carrying on. [oald]
;वह चिल्ला रहा था और बहस कर रहा था . [manual]
(defrule carry4
(declare (salience 5000))   ;salience increased by 14anu-ban-03 (12-03-2015)
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(kriyA-subject ?id ?id2)  ;added  by 14anu-ban-03 (12-03-2015)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))  ;added  by 14anu-ban-03 (12-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahasa_kara))  ;meaning changed from 'prema_saMbaMXa_sWApiwa_kara' to 'bahasa_kara' by 14anu-ban-03 (12-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry4  "  ?id "  " ?id1 "  bahasa_kara  )" crlf))
)

;She's been carrying on with her teacher. 
;usane apane aXyApaka ke sAWa prema saMbaMXa sWApiwa kie. 

;$$$ Modified by 14anu-ban-03 (12-03-2015)   ;working on parser no.- 3
;I can't carry on this relationship anymore.  [same clp]
;mEM isa saMbaMXa ko Ora aXika nahIM calA sakawA.  [same clp]
(defrule carry5
(declare (salience 5001))     ;salience increased by 14anu-ban-03 (12-03-2015)
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)   
(kriyA-object ?id ?id2)    ;added 'id2' by 14anu-ban-03 (12-03-2015) 
(id-root ?id2 relationship)    ;added by 14anu-ban-03 (12-03-2015) 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 calA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry5  "  ?id "  " ?id1 "  calA  )" crlf))
)


(defrule carry6
(declare (salience 4400))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kara)); Replaced 'pUrA_ho' with 'kara' by Manju(24-08-13) Suggested by Chaitanya Sir
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry6  "  ?id "  " ?id1 "  kara  )" crlf))
)

;The work would be carried out in ten days.
;kAma xasa xinoM meM pUrA ho jAegA
;kAma xasa xinoM meM kiyA jAegA
(defrule carry7
(declare (salience 4300))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 over)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jArI_raKa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry7  "  ?id "  " ?id1 "  jArI_raKa  )" crlf))
)

;They must carry on working.
;unhe apanA kAma jArI raKanA cAhie

;$$$ Modified by 14anu-ban-03 (12-03-2015)   ;working on parser no.- 6
;It was my luck that carried me through.  [same clp]
;yaha merI kismawa WI jisane mere kAma ko pUrA karane meM merI maxaxa kI.  [same clp]
;यह मेरा भाग्य था जिसने मुझे सफलतापूर्वक बाहर निकाला . [manual]  by 14anu-ban-03 (12-03-2015)
(defrule carry8
(declare (salience 4200))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?id2)    ;added '?id2' by 14anu-ban-03 (12-03-2015)
(id-root ?id2  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "human.gdbm" ?str)))  ;added  by 14anu-ban-03 (12-03-2015)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 saPalawApUrvaka_bAhara_nikAla))  ;meaning changed from 'kisI_kAma_ko_pUrA_karane_meM_maxaxa_kara' to 'saPalawApUrvaka_bAhara_nikAla' by 14anu-ban-03 (12-03-2015)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry8  "  ?id "  " ?id1 "  saPalawApUrvaka_bAhara_nikAla  )" crlf))
)


(defrule carry9
(declare (salience 4100))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(kriyA-object ?id ?)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry9  "  ?id "  " ?id1 "  pUrA_kara  )" crlf))
)

;The work would be carried out in ten days.
;yaha kAma xasa xinoM meM pUrA ho jAegA
(defrule carry10
(declare (salience 4000))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-back_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id smaraNa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " carry.clp carry10 " ?id "  smaraNa_kara )" crlf)) 
)

(defrule carry11
(declare (salience 3900))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 back)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 smaraNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry11  "  ?id "  " ?id1 "  smaraNa_kara  )" crlf))
)

(defrule carry12
(declare (salience 3800))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 forward)
(kriyA-forward_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_le_jA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " carry.clp carry12 " ?id "  Age_le_jA )" crlf)) 
)

(defrule carry13
(declare (salience 3700))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 forward)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry13  "  ?id "  " ?id1 "  Age_le_jA  )" crlf))
)

(defrule carry14
(declare (salience 3600))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-off_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jIwa));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " carry.clp carry14 " ?id "  jIwa )" crlf)) 
)

(defrule carry15
(declare (salience 3500))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 off)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 jIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry15  "  ?id "  " ?id1 "  jIwa  )" crlf))
)

(defrule carry16
(declare (salience 3400))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-on_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Age_baDZA));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " carry.clp carry16 " ?id "  Age_baDZA )" crlf)) 
)

(defrule carry17
(declare (salience 3300))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 on)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 Age_baDZA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry17  "  ?id "  " ?id1 "  Age_baDZA  )" crlf))
)

(defrule carry18
(declare (salience 3200))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-out_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id pUrNa_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " carry.clp carry18 " ?id "  pUrNa_kara )" crlf)) 
)

(defrule carry19
(declare (salience 3100))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 out)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 pUrNa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry19  "  ?id "  " ?id1 "  pUrNa_kara  )" crlf))
)

(defrule carry20
(declare (salience 3000))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-through_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaTina_samaya_meM_kisI_kI_sahAyawA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " carry.clp carry20 " ?id "  kaTina_samaya_meM_kisI_kI_sahAyawA_kara )" crlf)) 
)

;commented by 14anu-ban-03 (12-03-2015) because it is same as carry20
;(defrule carry21
;(declare (salience 2900))
;(id-root ?id carry)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 through)
;(kriyA-through_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kaTina_samaya_meM_kisI_kI_sahAyawA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " carry.clp carry21 " ?id "  kaTina_samaya_meM_kisI_kI_sahAyawA_kara )" crlf)) 
;)


;commented by 14anu-ban-03 (12-03-2015) because it is same as carry20
;(defrule carry22
;(declare (salience 2800))
;(id-root ?id carry)
;?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 through)
;(kriyA-through_saMbanXI ?id ?) ;Automatically modified kriyA-upasarga to kriyA-prep_saMbanXI by Sukhada's program. 
;(id-cat_coarse ?id verb)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kaTina_samaya_meM_kisI_kI_sahAyawA_kara));Automatically modified 'affecting_id-affected_ids-wsd_group_root_mng ?id ?id1' to 'id-wsd_root_mng ?id ' by Sukhada's program. 
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* " carry.clp carry22 " ?id "  kaTina_samaya_meM_kisI_kI_sahAyawA_kara )" crlf)) 
;)

(defrule carry23
(declare (salience 2700))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 through)
(kriyA-upasarga ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 kaTina_samaya_meM_kisI_kI_sahAyawA_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp	carry23  "  ?id "  " ?id1 "  kaTina_samaya_meM_kisI_kI_sahAyawA_kara  )" crlf))
)

;@@@ Added by 14anu-ban-03 (14-10-2014)
;In our experience, force is needed to push, carry or throw objects, deform or break them.[ncert]
;हम सभी का यह अनुभव है कि वस्तुओं को धकेलने, ले जाने अथवा फेङ्कने, निरूपित करने अथवा उन्हें तोडने के लिए बल की आवश्यकता होती है.[ncert]
(defrule carry26
(declare (salience 3000))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(kriyA-kqxanwa_karma ?id1 ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  carry.clp 	carry26   "  ?id "  le_jA )" crlf))
)

;$$$ Modified by 14anu-ban-03 (10-12-2014)
;@@@ Added by 14anu19(26-06-2014)
;He was almost carried away by the current .
;वह प्रवाह से लगभग बहाया गया था .  
(defrule carry27
(declare (salience 5000))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-upasarga  ?id ?id1)
(id-word ?id1 away)
(kriyA-by_saMbanXI  ?id ?id2)  ;added by 14anu-ban-03 (10-12-2014)
(id-root ?id2 current)  ;added by 14anu-ban-03 (10-12-2014)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id ?id1 bahA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " carry.clp  carry27  "  ?id "  " ?id1 "  bahA  )" crlf))
)

;@@@ Added by 14anu-ban-03 (13-03-2015)
;One day, he was crossing through the forest carrying some woods.(report- Set7-76-77.pdf)
;एक दिन,वह कुछ लकडियाँ उठाते हुए जङ्गल से गुज़र  रहा था .(manual)
(defrule carry28
(declare (salience 5000))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)
(id-root ?id1 wood)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  carry.clp 	carry28   "  ?id "  uTA )" crlf))
)


;particle_away_- && category=verb	udA_le_jA	10.1800282442072
; He carried away many pieces of furniture.
;PP_null_away && category=verb	udA_le_jA	10.1800282442072
(defrule carry24
(declare (salience 2600))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uTA_le_jA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  carry.clp 	carry24   "  ?id "  uTA_le_jA )" crlf))
)

(defrule carry25
(declare (salience 2500))
(id-root ?id carry)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id baMxUka_kI_sImA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  carry.clp 	carry25   "  ?id "  baMxUka_kI_sImA )" crlf))
)

;"carry","N","1.baMxUka_kI_sImA"
;The carry of Bofors gun is superb.
;
;
;LEVEL 
;
;
; carry-               "sUwra" (nibanXa)
;-----------
;
;"carry","VT","1.le_jAnA/DonA"
;You must carry your camping gear
;--"2.garBavawI_honA"
;She was carrying their second child.
;--"3.boJa_DonA"
;The bridge has to carry lot of traffic.
;--"4.samarWana_honA"
;The bill was carried by 250 votes to 200.
;--"5.praBAviwa_karanA"
;His speech on poverty carried the audience.
;--"6.calana"
;She carries herself very well.
;
;"carry","N","1.baMxUka_kI_sImA"
;The carry of Bofors gun is superb.
;-------------------------------------------------
;
;sUwra : vahana`{garBa}/banxUka_kI_sImA
;------------------------------
;
;upariliKiwa saBI vAkyoM meM `vahana' Sabxa kA arWa sAmAnya xIKawA hE .
; 
;spaRtawA ke lie `garBa' xiyA gayA hE .
; 
;`vahana' hI `banxUka kI sImA' ke arWa meM parivarwiwa huA hE .
;kAraNa- banxUka vahAz waka usa gole yA anya kisI ko Do sakawA hE . 
;isa arWa meM rUDa hone ke kAraNa usa arWa kA xiyA jAnA samBamawaH uciwa hE . 
;              
;
