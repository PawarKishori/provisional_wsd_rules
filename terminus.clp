;@@@ Added by 14anu-ban-07 ,12-08-2014
(defrule terminus0
(declare (salience 0))
(id-word ?id terminus)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id anwima_steSana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  terminus.clp 	terminus0   "  ?id "  anwima_steSana )" crlf))
)


;@@@ Added by 14anu-ban-07 ,12-08-2014
;basa addA :
;The bus terminus is in 'Ortigia island' so we got down there itself. (parallel corpus)
;बस अड्डा ’और्टीजिया द्वीप’ में है अतः वहीं उतरे ।
;We had breakfast at the bus terminus. (parallel corpus)
;बस अड्डे पर पहुँच कर हमने नाश्ता किया ।
(defrule terminus1
(declare (salience 1000))
(id-word ?id terminus)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) bus)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(- ?id 1)  basa_addA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " terminus.clp	 terminus1  "  ?id "  " =(- ?id 1)  " basa_addA  )" crlf))
)

