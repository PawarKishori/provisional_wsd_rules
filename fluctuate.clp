
;@@@ Added by 14anu-ban-05 on (22-10-2014)
;Temperatures can fluctuate by as much as 10 degrees.[OALD]
;तापमान में  ज्यादा से ज्यादा  10 डिग्री का उतार-चढ़ाव हो सकता है.[MANUAL]
(defrule fluctuate0
(declare (salience 1000))
(id-root ?id fluctuate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id  uwAra-caDZAva))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  fluctuate.clp  	fluctuate0   "  ?id "   uwAra-caDZAva )" crlf))
)

;@@@ Added by 14anu-ban-05 on (22-10-2014)
;Bernoulli's equation also does not hold for non-steady or turbulent flows, because in that situation velocity and pressure are constantly fluctuating in time.[NCERT]
;asWira aWavA vikRoBa pravAha meM BI barnUlI samIkaraNa kAma nahIM AwA kyoMki isameM vega waWA xAba samaya meM lagAwAra asWira rahawe hEM.[NCERT]

(defrule fluctuate1
(declare (salience 5000))
(Domain physics)
(id-root ?id fluctuate)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 velocity|pressure|temperature|voltage)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id asWira))
(assert (id-domain_type ?id physics))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  fluctuate.clp 	fluctuate1  "  ?id "   asWira )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-domain_type  " ?*prov_dir* "  fluctuate.clp       fluctuate1   "  ?id "  physics )" crlf))
)

