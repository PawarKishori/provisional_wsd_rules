
(defrule inferior0
(declare (salience 5000))
(id-root ?id inferior)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id nikqRta))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inferior.clp 	inferior0   "  ?id "  nikqRta )" crlf))
)

(defrule inferior1
(declare (salience 4900))
(id-root ?id inferior)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id aXInasWa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inferior.clp 	inferior1   "  ?id "  aXInasWa )" crlf))
)

;"inferior","N","1.aXInasWa"
;Ravi is Ram's inferior.

;$$$ Modified by 14anu-ban-06 (19-02-2015)
;These products are inferior to those we bought last year.(cambridge)
;ये उत्पाद उनसे कम अच्छे हैं जिन्हें हमने पिछले वर्ष खरीदा . (manual)
;@@@ Added by 14anu11
;Kayaka also implied that no occupation was inferior or superior to another .
;कायक की धारणा में यह भी निहित था कि कोई भी पेशा दूसरे से छोटा या बडा नहीं .
(defrule inferior2
(declare (salience 5050))
(id-root ?id inferior)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(subject-subject_samAnAXikaraNa  ?id1 ?id)
(id-root ?id1 occupation);added by 14anu-ban-06 (19-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id CotA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  inferior.clp 	inferior2   "  ?id "  CotA )" crlf))
)


;
;
