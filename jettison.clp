;@@@ Added by 14anu-ban-06 (18-03-2015)
;The station has jettisoned educational broadcasts. (cambridge)
;स्टेशन ने शैक्षिक प्रसारण को रद्द किया है . (manual)
;We've had to jettison our trip because of David's accident.(cambridge)
;हम डॆविड की दुर्घटना की वजह से हमारी यात्रा को रद्द कर रहे हैं. (manual)
(defrule jettison1
(declare (salience 2000))
(id-root ?id jettison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 trip|journey|broadcast)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id raxxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jettison.clp 	jettison1   "  ?id "  raxxa_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (18-03-2015)
;He was jettisoned as team coach after the defeat.(OALD)
;उसको हार के बाद दल प्रशिक्षक के रूप से हटा दिया गया था . (manual)
(defrule jettison2
(declare (salience 2200))
(id-root ?id jettison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-as_saMbanXI ?id ?id1)
(id-root ?id1 coach|teacher|judge)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hatA_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jettison.clp 	jettison2   "  ?id "  hatA_xe )" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-06 (18-03-2015)
;The captain was forced to jettison the cargo and make an emergency landing.(cambridge)
;कप्तान को माल फेंक देने और एमर्जेंसी अवतरण करने के लिए मजबूर किया गया था . (manual)
(defrule jettison0
(declare (salience 0))
(id-root ?id jettison)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Pefka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  jettison.clp         jettison0   "  ?id "  Pefka_xe )" crlf))
)

