;@@@ Added by 14anu-ban-07,04-08-2014
;All wheels used in transportation have rolling motion.(ncert corpus)
;यातायात में इस्तेमाल होने वाले सभी पहियों की गति लोटनिक गति होती है.
(defrule transportation0
(id-root ?id transportation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAwAyAwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " transportation.clp 	transportation0   "  ?id "  yAwAyAwa )" crlf))
)

;@@@ Added by 14anu-ban-07,04-08-2014
;During the World War transportation was prevalent.
;विश्व युद्ध के दौरान देशनिकाला प्रचलित था . 
(defrule transportation1
(declare (salience 1000))
(id-root ?id transportation)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(subject-subject_samAnAXikaraNa  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xeSanikAlA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " transportation.clp 	transportation1   "  ?id "  xeSanikAlA )" crlf))
)
