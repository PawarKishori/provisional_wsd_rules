
(defrule market0
(declare (salience 5000))
(id-root ?id market)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id marketing )
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id bikrI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  market.clp  	market0   "  ?id "  bikrI )" crlf))
)

;given_word=marketing && word_category=noun	$vipaNana)

;"marketing","N","1.vipaNana"
;We should adopt high powered marketing strategies.

;$$$ Modified by Manasa ( 26-02-2016 ) : meaning changed from bAjAra to vyApAra
;Indeed the market in which the firms sell their products could not have been functioning without the demand coming from the households.
; वास्तव में वह  व्यापार  जिसमें व्यापारिक कम्पनियाँ उनके उत्पाद बिकती हैं परिवारों से आते हुए माँग के बिना कार्य हो सकता है नहीं करता हो  
(defrule market1
(declare (salience 4900))
(id-root ?id market)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyApAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  market.clp 	market1   "  ?id "  vyApAra )" crlf))
)
; Hindi meaning "hAta" in this rule is changed as "bAjAra" because of the more familiarity by Sukhada. Date: 21/0309
;default_sense && category=noun	hAta	0
;"market","N","1.hAta"
;She has gone to the market to buy vegetables.
;


;$$$ Modified by Bhagyashri Kulkarni (9-11-2016)
;Who will go to the market? (rapidex)
;बाजार कौन जाएगा?
;@@@ Added by Shruti Singh M.Tech(CS) Banasthali University(06-09-2016)
;I saw a girl in the market.
;मैंने बाजार में लडकी को देखा . 
(defrule market2
(declare (salience 6000))
(id-root ?id market)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(or (kriyA-to_saMbanXI  ?id1 ?id)(kriyA-in_saMbanXI  ?id1 ?id))
(id-root ?id1 go|see) ;changed the condition from 'word' to 'root' by Bhagyashri
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAjAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  market.clp    market2   "  ?id "  bAjAra )" crlf))
)


