
;$$$ Modified by 14anu-ban-05 on (24-04-2015)
;Added by sheetal(10/6/10)
;He never really recovered from the shock of his wife's death .
(defrule recover0
(declare (salience 5000))
(id-root ?id recover)
?mng <-(meaning_to_be_decided ?id)
(kriyA-from_saMbanXI  ?id ?id1)		;added by 14anu-ban-05 on (24-04-2015)
(id-root ?id1 shock|death)            ;changed ' id-word' to 'id-root' and added 'death' by 14anu-ban-05 on (24-04-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uBara_pA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recover.clp       recover0   "  ?id "  uBara_pA )" crlf))
)

;@@@ Added by 14anu-ban-10 on (19-11-2014)
;Nutrients from residue are not fully recovered by crops. [agriculture]
;फसलों द्वारा अवशेषों से पोषक तत्वों को पूरी तरह से  पुन:प्राप्ति नहीं किया है. [manual]
(defrule recover1
(declare (salience 4900))		;decreased salience from 5100 to 4900 by 14anu-ban-05 on (24-04-2015)
(id-root ?id recover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prApwi_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recover.clp       recover1   "  ?id "  prApwi_kara)" crlf))
)


;@@@ Added by 14anu-ban-05 on (24-04-2015)
;He's still recovering from his operation.	[OALD]
;वह शल्य चिकित्सा से अभी भी ठीक हो रहा है . 		[MANUAL]
(defrule recover2
(declare (salience 5001))		
(id-root ?id recover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-from_saMbanXI  ?id ?id1)
(id-word ?id1 operation|surgery|illness)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id TIka_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recover.clp       recover2   "  ?id "  TIka_ho)" crlf))
)

;@@@ Added by 14anu-ban-05 on (24-04-2015)
;The economy is at last beginning to recover.[OALD]
;अर्थ प्रबन्धन में  सुधार आखिरकार आरम्भ हो गया है . [MANUAL]
(defrule recover3
(declare (salience 5001))		
(id-root ?id recover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?)
(to-infinitive  ?to ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suXAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recover.clp       recover3   "  ?id "  suXAra)" crlf))
)


;@@@ Added by 14anu-ban-05 on (24-04-2015)
;He is unlikely to ever recover his legal costs.[OALD]
;उसके कानूनी खर्चे को कभी भी पुनः प्राप्त  करने की संभावना नहीं है. 	[MANUAL]
(defrule recover4
(declare (salience 5003))		
(id-root ?id recover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 ?str) 
(test (and (neq (numberp ?str) TRUE) (neq (gdbm_lookup_p "animate.gdbm" ?str) TRUE)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id punaH_prApwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recover.clp       recover4   "  ?id "  punaH_prApwa_kara)" crlf))
)

;@@@ Added by 14anu-ban-05 on (24-04-2015)
;She seemed upset but quickly recovered herself.[OALD]
;वह दुखी प्रतीत हुई परन्तु स्वयं को जल्दी से  सम्भाला. [MANUAL]
(defrule recover5
(declare (salience 5002))		
(id-root ?id recover)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?)
(kriyA-subject  ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samBAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  recover.clp       recover5   "  ?id "  samBAla)" crlf))
)
