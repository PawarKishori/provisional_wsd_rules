;This rule cannot be fired as the relations mapped in stanford and relation table are wrong.
;@@@ Added by Manasa 25-8-2015
;This is the output from the British sector of the North sea.(camb)
;ये उत्र साागर के ब्रिटिशश  क्षेत्रकाा   उत्पादनन है़ |
(defrule sector1
(declare (salience 5000)) 
(id-root ?id sector)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(samAsa_viSeRya-samAsa_viSeRaNa ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kRewra ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sector.clp     sector1   "  ?id "  kRewra  )" crlf))
)

;@@@ Added by Manasa 25-8-2015
;This sector remained confined only to the railways.
;yaha SAKA kevala relave taka sImiwa hez.
(defrule sector2
(declare (salience 0))
(id-root ?id sector)
?mng <-(meaning_to_be_decided ?id)  
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SAKA ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  sector.clp     sector2   "  ?id "  SAKA  )" crlf))
)


