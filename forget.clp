;##############################################################################
;#  Copyright (C) 2013-2014 Pramila (pramila3005 at gmail dot com)
;#
;#  This program is free software; you can redistribute it and/or
;#  modify it under the terms of the GNU General Public License
;#  as published by the Free Software Foundation; either
;#  version 2 of the License, or (at your option) any later
;#  version.
;#
;#  This program is distributed in the hope that it will be useful,
;#  but WITHOUT ANY WARRANTY; without even the implied warranty of
;#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;#  GNU General Public License for more details.
;#
;#  You should have received a copy of the GNU General Public License
;#  along with this program; if not, write to the Free Software
;#  Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;
;##############################################################################

;I forget how much they paid for it. ;corrected spelling by Bhagyashri
;मुझे याद नहीं आ रहा उन्होनें कितने पैसे दिए थे.
(defrule forget0
(declare (salience 5000))
(id-root ?id forget)
?mng <-(meaning_to_be_decided ?id)
(kriyA-subject  ?id ?id1)
(id-word ?id1 i)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id yAxa_nahIM_A))
(assert (kriyA_id-subject_viBakwi ?id ko))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  forget.clp 	forget0   "  ?id "  yAxa_nahIM_A )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  forget.clp 	forget0     "  ?id " ne )" crlf);added by Shirisha Manju 5-11-16
)
)


;@@@ Added by Bhagyashri Kulkarni (3-11-2016)
;I can never forget it, can I?  (rapidex)
;मैं यह कभी नहीं भुल सकता हूँ, है ना ? 
(defrule forget1
(declare (salience 5000))
(id-root ?id forget)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_niReXaka ?id ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Bula))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  forget.clp 	forget1   "  ?id "  Bula )" crlf))
)


