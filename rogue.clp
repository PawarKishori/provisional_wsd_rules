;@@@ Added by 14anu-ban-10 on (28-02-2015) 
;He is a rogue elephant,that is why he is kept apart from others.[hinkhoj]
;वह एक अड़ियल जानवर हाथी है ,तभी वह  दूसरो से  अलग रखा हुआ है . [manual] 
(defrule rogue1
(declare (salience 200))
(id-root ?id rogue)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) elephant)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id (+ ?id 1) adiZyala_jAnavara ))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " rogue.clp  rogue1  "  ?id "  " (+ ?id 1) "  adiZyala_jAnavara   )" crlf))
)

;@@@ Added by 14anu-ban-10 on (28-02-2015)
;A rogue gene.[oald]
;एक शरारती  जिन. [manual]
(defrule rogue2
(declare (salience 300))
(id-root ?id rogue)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 gene)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SarArawI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rogue.clp 	rogue2  "  ?id " SarArawI)" crlf))
)

;@@@ Added by 14anu-ban-10 on (28-02-2015)
;A rogue police officer.[oald]
;एक  शठ पुलिस अधिकारी . [manual]
(defrule rogue3
(declare (salience 300))
(id-root ?id rogue)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 officer)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id SaTa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rogue.clp 	rogue3  "  ?id " SaTa)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-10 on (28-02-2015)
;He is a rogue.[hinkhoj]
;वह आवारा है .[manual] 
(defrule rogue0
(declare (salience 100))
(id-root ?id rogue)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AvArA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  rogue.clp    rogue0   "  ?id " AvArA)" crlf))
)

