
;@@@ Added by 14anu-ban-11 on (29-01-2015)
;The cold water quenched his thirst.(hinkhoj)
;ठण्डे पानी ने उसकी प्यास बुझाई .(anusaaraka) 
(defrule quench0
(declare (salience 10))
(id-root ?id quench)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id buJAI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quench.clp 	quench0   "  ?id "  buJAI )" crlf))
)


;@@@ Added by 14anu-ban-11 on (29-01-2015)
;Don't quench the enormity of this moment.(coca)
;इस क्षण की नृशंसता को मत दबाइए . 
;We quench our feelings of guilt.(oxford dic) 
;हम अपराध की अपनी संवेदनाएँ दबाते हैं. (self)
(defrule quench1
(declare (salience 20))
(id-root ?id quench)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(viSeRya-of_saMbanXI  ?id1 ?id2)
(id-root ?id2 moment|guilt)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id xabA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  quench.clp 	quench1   "  ?id "  xabA )" crlf))
)

