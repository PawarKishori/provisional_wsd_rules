

;---------------------------------DEFAULT RULE-------------------------------------------------

;@@@ Added by 14anu-ban-09 on (14-03-2015)
;NOTE-Example sentence need to be added.

(defrule organise0
(declare (salience 000))
(id-root ?id organise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vyavasWiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organise.clp 	organise0   "  ?id "  vyavasWiwa )" crlf))
)

;-----------------------------------------------------------------------------------------------

;@@@ Added by 14anu-ban-09 on (14-03-2015)
;Considered a cradle of civilization, Ancient Egypt experienced some of the earliest developments of writing, agriculture, urbanisation, organised religion and central government in history.		[Report set 3]
;समाज को विकास की भूमि मानना,  ऎन्चन्ट इजिप्ट कुछ  शुरू शुरू के विकासों में लेखन , कृषि वर्ग, नगरीकरण, संगठित धर्म और  इतिहास में केंद्रीय-सरकार का अनुभव किया .[self]	; Tranlation needs futher improvement 

(defrule organise1
(declare (salience 1000))
(id-root ?id organise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective) 
(viSeRya-viSeRaNa  ?id1 ?id)
(id-root ?id1 religion)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMgaTiwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  organise.clp 	organise1   "  ?id "  saMgaTiwa )" crlf))
)
