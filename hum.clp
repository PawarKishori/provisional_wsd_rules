;@@@ Added by 14anu-ban-06 (26-03-2015)
;The computers were humming in the background.(OALD)
;सङ्गणक पीछे थोडी आवाज कर रहे थे . (manual)
(defrule hum2
(declare (salience 5100))
(id-root ?id hum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1 computer|wire)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodZI_AvAja_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hum.clp 	hum2   "  ?id "  WodZI_AvAja_kara )" crlf))
)

;@@@ Added by 14anu-ban-06 (26-03-2015)
;The bar was really humming last night. (cambridge)
;शराबघर में पिछली रात वास्तव में चहल पहल हो रही थी . (manual)
(defrule hum3
(declare (salience 5200))
(id-root ?id hum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject ?id ?id1)
(id-root ?id1  ?str&:(and (not (numberp ?str))(gdbm_lookup_p "place.gdbm" ?str)))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cahala_pahala_ho))
(assert (kriyA_id-subject_viBakwi ?id meM))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hum.clp 	hum3   "  ?id "  cahala_pahala_ho )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-subject_viBakwi   " ?*prov_dir* "  hum.clp      hum3   "  ?id " meM )" crlf))
)

;xxxxxxxxxxxx Default Rule xxxxxxxxxx

;"hum","N","1.guMjana"
;BOroM kA'hum' kAno meM gUzja rahA hE.
(defrule hum0
(declare (salience 5000))
(id-root ?id hum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id guMjana))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hum.clp 	hum0   "  ?id "  guMjana )" crlf))
)

;"hum","V","1.gunagunAnA"
;rAhula gIwa ko'hum' rahA WA.
(defrule hum1
(declare (salience 4900))
(id-root ?id hum)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gunagunA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  hum.clp 	hum1   "  ?id "  gunagunA )" crlf))
)

