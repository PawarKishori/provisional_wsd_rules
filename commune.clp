;@@@ Added by 14anu-ban-03 (24-03-2015)
;I tried to make commune.  [oald]
;मैंने बातचीत करने का प्रयास किया . [manual]
(defrule commune2
(declare (salience 10))
(id-root ?id commune)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id1 ?id)
(id-root ?id1 make)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwacIwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " commune.clp 	commune2  "  ?id "   bAwacIwa )" crlf))
)


;@@@ Added by 14anu-ban-03 (24-03-2015)
;She refused to communing with him before the wedding. [oald]
;उसने विवाहोत्सव से पहले उसके साथ रहने से मना कर दिया . [manual]
(defrule commune3
(declare (salience 10))
(id-root ?id commune)
?mng <-(meaning_to_be_decided ?id)
(kriyA-before_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sAWa_raha))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " commune.clp 	commune3  "  ?id "   sAWa_raha )" crlf))
)

;-----------------------------------------------Default Rules------------------------------------

;@@@ Added by 14anu-ban-03 (24-03-2015)
;He's living in a religious commune.  [M-W]
;वह एक धार्मिक समुदाय में रह रहा है . [manual]
(defrule commune0
(declare (salience 00))
(id-root ?id commune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samuxAya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " commune.clp   commune0  "  ?id "   samuxAya )" crlf))
)

;@@@ Added by 14anu-ban-03 (24-03-2015)
;He spent much of this time communing with nature. [oald]
;उसने अपना ज्यादातर समय प्रकृति के साथ बातचीत करते हुए बिताया. [manual]
(defrule commune1
(declare (salience 00))
(id-root ?id commune)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id bAwacIwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* " commune.clp   commune1  "  ?id "   bAwacIwa_kara )" crlf))
)


