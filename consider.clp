
(defrule consider0
(declare (salience 5000))
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id considering )
(id-cat_coarse ?id preposition)
=>
(retract ?mng)
(assert (id-wsd_word_mng ?id XyAna_meM_raKa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  consider.clp  	consider0   "  ?id "  XyAna_meM_raKa_kara )" crlf))
)

;$$$ Modified by Preeti(28-2-14) --- added is_being_en|are_en in the list and also added +1 adjective
;Who do you consider responsible for the accident? 
;Apa xurGatanA ke liye kisako uwwaraxAyI mAnawe hEM?
;The company is being actively considered as a potential partner. 
;kampanI ko sakriya rUpa se eka sAmarWya sAJIxAra kI waraha  mAnA jA rahI hE. 
;These workers are considered as a high-risk group. 
;Ina majaxUroM ko eka ucca joKima samUha kI waraha mAnA gayA hEM.
;@@@ Added by Roja Suggested by Chaitanya Sir(17-08-13)
;Teaching is considered an apt calling for women.
;Jumma masjid is considered to be one of the most beautiful mosques in western india. 
(defrule consider_is_en
(declare (salience 4950))
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(or (root-verbchunk-tam-chunkids consider ? is_en|is_being_en|are_en $? ?id $?)(id-cat_coarse =(+ ?id 1) adjective))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp  consider_is_en   "  ?id "  mAna )" crlf))
)

;$$$ Modified by 14anu-ban-03 (01-08-2014) 
; Meaning changed from 'samaJa' to 'mAna'
;In a good number of situations in real-life, the size of objects can be neglected and they can be considered as point-like objects without much error. 
;vAswavika jIvana meM bahuwa - sI sWiwiyoM meM vaswuoM ke AmApa (sAija) kI upekRA kI jA sakawI hE Ora binA aXika wruti ke unheM eka binxu - vaswu mAnA jA sakawA hE.
;Modified by Meena(8.3.11) (merged the rule consider2 in consider1)
(defrule consider1
(declare (salience 4900))
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(id-word =(+ ?id 1) as|to)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp 	consider1   "  ?id "  mAna )" crlf))
)

;It was considered as one of the youngest.
;It was considered to be one of the youngest.
;"considering","Prep","1.XyAna_meM_raKa_kara"
;She has done very well in exams considering her illness.
;
;$$$ Modified by 14anu13  26-06-14
;The Hindus consider the plants as a species of animal as Plato also thinks that the plants have a sense , because they have the faculty of distinguishing between that which suits them and that which is detrimental to them .
;हिन्दू पौधों को पशु की प्रजाति ही समझते हैं जैसा कि प्लेटों का भी विचार था कि पौधों में भी बोध होता है क्योंकि उनमें यह भेद करने की क्षमता होती है कि उनके लिए क्या अनुकूल है और क्या प्रतिकूल .
(defrule consider3
(declare (salience 4700))
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(id-word ?id1 plan|plants)         ;added "plants" by 14anu13
(kriyA-object ?id ?id1)
;(id-cat_coarse ?id verb) this and the above line are automatically modified using a program by Sukhada
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp 	consider3   "  ?id "  samaJa )" crlf))
)

;$$$ Modified by 14anu-ban-03 (05-12-2014)
;Devikulam area is also considered excellent for fishing . [total tourism]
;maCalI pakadZane ke liye BI xevIkulama kRewra uwwama samaJA jAwA hE . [total tourism]
(defrule consider4
(declare (salience 5000))  ;salience increased by  14anu-ban-03 (05-12-2014)
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
;(id-word ?id1 good)    ;commented by 14anu-ban-03 (05-12-2014)
(id-cat_coarse ?id verb)
(kriyA-for_saMbanXI ?id ?id1) ;added by 14anu-ban-03 (05-12-2014)
(id-root ?id1 fishing)  ;added by 14anu-ban-03 (05-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id samaJa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp 	consider4   "  ?id "  samaJa )" crlf))
)


;@@@ Added by 14anu-ban-03 (28-02-2015)
;They considered the possibility of a strike. [same clp file]
;उन्होंने स्ट्राइक की सम्भावना पर विचार किया . [manual]
(defrule consider06
(declare (salience 4400))
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(kriyA-object ?id ?id1)  
(id-root ?id1 possibility)  
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (kriyA_id-object_viBakwi ?id para)) 
(assert (id-wsd_root_mng ?id vicAra_kara))  
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  consider.clp 	consider06  "  ?id " para )" crlf) 
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp 	consider06  "  ?id "  vicAra_kara )" crlf))
)


;@@@ Added by Preeti(28-2-14)
;He considers himself an expert on the subject. 
;vaha svayaM ko viRaya meM nipuNa mAnawA hE.
;$$$ Modifed by 14anu19
;compassion word is added in list.
(defrule consider7
(declare (salience 4950))
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 behaviour|attitude|compassion)  ;compassion word is added (by 14anu19(26-06-2014))
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp  consider7   "  ?id "  mAna )" crlf))
)


;$$$ Modified by 14anu13 on 02-07-14   [added condition (and(id-root ?id1 this)(kriyA-object ?id  ?id1)]
;Therefore they fast on this day in the state of the most punctilious cleanness , and they stay awake all the night , considering this as an obligatory performance , though in reality it is not obligatory .
;यही कारण है कि वे पूर्णत : स्वच्छता का पालन करते हुए उस दिन व्रत रखते हैं , रात - भर जागते हैं - क्योंकि वे ऐसा करना अनिवार्य मानते हैं , यद्यपि वास्तव में यह अनिवार्य नहीं है |
;@@@ Added by Preeti(28-2-14)
;Some give it the height of one yojana , others more ; some consider it as quadrangular , others as an octagon .
;कुछ इसकी ऊंचाई एक योजन बताते हैं , और कुछ इससे अधिक ; कुछ इसका आकार चतुर्भुजीय मानते हैं तो दूसरे अष्टभुजीय |
(defrule consider8
(declare (salience 5000))
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(or(kriyA-as_saMbanXI 	?id  ?)(and(id-root ?id1 this)(kriyA-object ?id  ?id1)))
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp  consider8   "  ?id "  mAna )" crlf))
)

;$$$ Modified by 14anu-ban-03 (04-12-2014)
;If we consider solid conductors, then of course the atoms are tightly bound to each other so that the current is carried by the negatively charged electrons.[ncert]
;यदि हम ठोस चालक पर विचार करें तो वास्तव में इनमें परमाणु आपस में निकट रूप से, कस कर आबद्ध होते हैं जिसके कारण ऋण आवेशित इलेक्ट्रॉन विद्युत धारा का वहन करते हैं.[ncert]
;@@@ Added by 14anu07 on 28/06/2014
(defrule consider08
(declare (salience 5000))  ;salience increased by 14anu-ban-03 (04-12-2014)
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object ?id ?id1)
(id-root ?id1 application|conductor|case)  ;added 'conductor|case' by 14anu-ban-03 (04-12-2014)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp 	consider08   "  ?id "  vicAra_kara )" crlf))
)


;@@@ Added by 14anu07 on 28/06/2014
(defrule consider9
(declare (salience 4400))
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(id-root =(+ ?id 1) how)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp 	consider9  "  ?id "  vicAra_kara)" crlf))
)

;@@@ Added by 14anu-ban-03 (05-12-2014)
;Hence in the situation considered, there will be a current for a very short while and no current thereafter. [ncert]
;इस प्रकार विचाराधीन परिस्थिति में बहुत अल्प समय के लिए विद्युत धारा बहेगी और उसके पश्चात कोई धारा नहीं होगी. [ncert]
(defrule consider10
(declare (salience 5000))  
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(viSeRya-kqxanwa_viSeRaNa ?id1 ?id)
(id-root ?id1 situation)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id vicArAXIna))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp     consider10   "  ?id "  vicArAXIna )" crlf))
)


;@@@ Added by 14anu-ban-03 (27-02-2015)
;Hence, the radiation emitted can be considered due to individual atoms rather than because of interactions between atoms or molecules.[ncert]
;अतः, उत्सर्जित विकिरण, परमाणुओं अथवा अणुओं के बीच अन्योन्य क्रियाओं के परिणामस्वरूप नहीं, बल्कि व्यष्टिगत परमाणुओं के कारण माना जा सकता है. [ncert]
;अतः, उत्सर्जित विकिरण, परमाणुओं अथवा अणुओं के बीच अन्योन्य क्रियाओं के परिणामस्वरूप नहीं, बल्कि व्यष्टिगत परमाणुओं के कारण गौर किया जा सकता है. [manual]
(defrule consider11
(declare (salience 5000))  
(id-root ?id consider)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-kriyA_viSeRaNa ?id ?id1)
(id-root ?id1 hence)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gOra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp     consider11   "  ?id "  gOra_kara )" crlf))
)


;--------------------------- Default rule ----------------

;$$$ Modified by 14anu-ban-03 (27-02-2015)
;@@@ Added by Preeti(28-2-14)
;He considers himself an expert on the subject. 
;vaha svayaM ko viRaya meM nipuNa mAnawA hE.
;$$$ Modified by 14anu19(26-06-2014)
(defrule consider6
(declare (salience 00))   ;salience reduced by 14anu-ban-03 (27-02-2015)
(id-root ?id consider)   
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
;(id-word ?id considers) ;commented by 14anu-ban-03 (27-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id mAna))
(assert (kriyA_id-object_viBakwi ?id ko))  ;object_viBakwi is added.(by 14anu19)
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  consider.clp  consider6   "  ?id "  mAna )" crlf))
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  consider.clp      consider6   "  ?id " ko )" crlf)
)
;default_sense && category=verb	vicAra_kara	0
;"consider","VT","1.vicAra_karanA"
;They considered the possibility of a strike.
;Your application is being considered.
;His name is being considred for this post.
;He stood considering the lake.
;
;
;LEVEL 
;Headword : consider
;
;Examples --
;
;1. We have considered your application carefully but cannot offer you a job.
;hama Apake prArWanA pawra ko sAvaXAnI se vicAra kara {XyAna xe} cuke hEM, kinwu hama Apako kAma nahIM xe sakawe hEM. 
;2. Have you considered how to get there ?
;kyA Apane XyAna xiyA ki vahAz kEse jAezge ?
;3. We must consider the feelings of others.
;hameM xUsaro kI BAvanAo ko XyAna xenA cAhie.
;4. You must consider his youth.
;Apako usake yOvana ko XyAnameM raKanA cAhie.
;5. I consider you foolish.
;mEM wumheM mUrKa samaJawA hUz.
;
;uparaliKiwa vAkyoM meM, 1 - 4 vAkyoM meM, "consider" Sabxa kA arWa "XyAna_xenA" yA "XyAna raKanA" EsA A rahA hE. XyAna xene para usapara vicAra BI kiyA jAwA hE.
;wo "consider" Sabxa kA arWa hama "XyAna_xenA[vicArapUrvaka]" Ese xe sakawe hEM.
;kinwu vAkya 5 meM "consider" kA arWa "samaJanA" EsA A rahA hE.
;wo aba hama "consider" Sabxa ke lie sUwra kuCa isa prakAra xe sakawe hEM :
;
;sUwra : "XyAna_xenA{vicArapUrvaka}^samaJanA"
; 
