;@@@ Added by 14anu-ban-03 (11-03-2015)
;A collar of a dog. [oald]
;कुत्ते के गले का पट्टा . [manual]
(defrule collar2
(declare (salience 100))
(id-root ?id collar)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-of_saMbanXI ?id ?id1)
(id-root ?id1 dog)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id gale_kA_pattA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  collar.clp 	collar2   "  ?id "  gale_kA_pattA )" crlf))
)


;@@@ Added by 14anu-ban-03 (11-03-2015)
;I was collared in the street by a woman doing a survey. [oald]
;मैं निरीक्षण करते हुए स्त्री के द्वारा सडक पर रोका गया था . [manual]
(defrule collar3
(declare (salience 200))  
(id-root ?id collar)
?mng <-(meaning_to_be_decided ?id)
(kriyA-in_saMbanXI ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id roka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  collar.clp 	collar3   "  ?id "  roka )" crlf))
)

;@@@ Added by 14anu-ban-03 (11-03-2015)    ;run on parse no.- 3
;A diamond collar. [cald]
;हीरे का हार. [manual]
(defrule collar4
(declare (salience 100))
(id-root ?id collar)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 diamond|gold|silver)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id hAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  collar.clp 	collar4   "  ?id "  hAra )" crlf))
)

;---------------------Default Rules-----------------

;"collar","N","1.garaxanI"
;The collar of his shirt is always dirty.
(defrule collar0
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (11-03-2015)
(id-root ?id collar)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id garaxanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  collar.clp 	collar0   "  ?id "  garaxanI )" crlf))
)

;"collar","V","1.giraPwAra_karanA"
;The policeman collared the rowdy.
(defrule collar1
(declare (salience 00))  ;salience reduced by 14anu-ban-03 (11-03-2015)
(id-root ?id collar)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id giraPwAra_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  collar.clp 	collar1   "  ?id "  giraPwAra_kara )" crlf))
)

;"collar","V","1.giraPwAra_karanA"
;The policeman collared the rowdy.
;;"collar","N","1.garaxanI"
;The collar of his shirt is always dirty.
;--"2.gale_kA_pattA"
;A dog with a collar is treated as a pet dog by the municipality.
;

