;@@@ Added by 14anu-ban-11 on (23-02-2015)
;He found himself snared in a web of intrigue.(oald)
;उसने स्वयं को षड्यन्त्र के जाल में फँसा हुआ पाया. (self)
(defrule snare3
(declare (salience 20))
(id-root ?id snare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-in_saMbanXI  ?id ?id1)
(id-root ?id1 web)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PazsA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snare.clp 	snare3   "  ?id "  PazsA_ho)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (20-02-2015)
;The rabbit's foot was caught in a snare.(oald) 
;खरगोश का पाँव जाल में फँस  गया था . (self)
(defrule snare0
(declare (salience 10))
(id-root ?id snare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snare.clp    snare0   "  ?id "  jAla)" crlf))
)

;@@@ Added by 14anu-ban-11 on (20-02-2015)
;How did you snare him?(coca)
;आपने उसको कैसे फँसाया? (self)
(defrule snare2
(declare (salience 10))
(id-root ?id snare)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id PazsA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  snare.clp    snare2   "  ?id "  PazsA)" crlf))
)

