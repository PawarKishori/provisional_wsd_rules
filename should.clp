;@@@ Added by 14anu-ban-01 on (03-12-2014)
;Should a pest reach an unacceptable level, mechanical methods are the first options.[agriculture]
;यदि कोई विनाशकारी कीट एक अस्वीकरणीय स्तर तक बढ जाता है तो यांत्रिकी विधियाँ ही प्रथम विकल्प होती हैं. [self]
(defrule should0
(declare (salience 0))
(id-root 1 should)
?mng <-(meaning_to_be_decided 1)
(id-cat_coarse 1 verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng 1 yaxi/agara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  should.clp  should0  "  1 " yaxi/agara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (03-12-2014)
;Should I help him?[self:with reference to oald]
;क्या मुझे उसकी सहायता करनी चाहिये? [self]
(defrule should1
(id-root ?id should)
?mng <-(meaning_to_be_decided ?id)
(id-root 1 should)
(id-last_word ?id1 ?)
(id-right_punctuation  ?id1 PUNCT-QuestionMark)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id cAhiye))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_word_mng  " ?*prov_dir* "  should.clp  should1  "  ?id " cAhiye )" crlf))
)

