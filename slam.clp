;@@@ Added by 14anu-ban-11 on (21-03-2015)
;She slammed down the phone angrily.(oald)
;उसने क्रोध से टेलीफोन नीचे की ओर जोर से पटक दिया . (self)
(defrule slam2
(declare (salience 10))
(id-root ?id slam)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?id1)
(id-root ?id1 phone)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZora_se_pataka_xe))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slam.clp 	slam2   "  ?id "  jZora_se_pataka_xe)" crlf))
)

;@@@ Added by 14anu-ban-11 on (21-03-2015)
;I slammed on the brake, but it was too late.(oald)
;मैंने ब्रेक पर जोर से मारा, परन्तु बहुत देर हो गयी थी. (self)
(defrule slam3
(declare (salience 20))
(id-root ?id slam)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-on_saMbanXI  ?id ?id1)
(id-root ?id1 brake)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZora_se_mAra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slam.clp 	slam3   "  ?id "  jZora_se_mAra)" crlf))
)


;@@@ Added by 14anu-ban-11 on (21-03-2015)
;The government has been slammed for failing to take firm action against drinking and driving.(oald)
;सरकार का पीने और  चालन के विरुद्ध दृढ कार्य करने मे असफल होने के कारण कटु आलोचना हुई  .(self) 
(defrule slam4
(declare (salience 30))
(id-root ?id slam)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-subject  ?id ?id1)
(id-root ?id1 government)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id katu_AlocanA_ho))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slam.clp 	slam4   "  ?id "  katu_AlocanA_ho)" crlf))
)

;------------------------ Default Rules ----------------------

;@@@ Added by 14anu-ban-11 on (21-03-2015)
;The front door closed with a slam.(oald)
;मुख्य द्वार को जोर से बन्द किया . (self)
(defrule slam0
(declare (salience 00))
(id-root ?id slam)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZora))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slam.clp     slam0   "  ?id "  jZora)" crlf))
)


;@@@ Added by 14anu-ban-11 on (21-03-2015)
;She slammed the door after her. (oald)
;उसने अपने बाद दरवाजा जोर से बन्द किया . (self)
(defrule slam1
(declare (salience 01))
(id-root ?id slam)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jZora_se_baMxa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  slam.clp     slam1   "  ?id "  jZora_se_baMxa_kara)" crlf))
)

