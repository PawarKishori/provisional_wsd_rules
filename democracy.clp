;@@@ Added by 14anu-ban-04 (21-02-2015)
;There is complete democracy in India.                  [hinkhoj]
;भारत में पूर्ण प्रजातन्त्र राज्य है .                                 [self]
(defrule democracy1
(declare (salience 20))
(id-root ?id democracy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-in_saMbanXI ?id ?id1)
(id-cat_coarse ?id1 PropN) 
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id prajAwanwra_rAjya))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  democracy.clp 	democracy1   "  ?id "  prajAwanwra_rAjya )" crlf)
)
)


;@@@ Added by 14anu-ban-04 (21-02-2015)
;The early 1990s saw the spread of democracy in Eastern Europe.                   [cald] 
;1990 के दशक में पूर्वी यूरोप में लोकतंत्र  के फैलाव को देखा .                                         [self]
(defrule democracy0
(declare (salience 10))
(id-root ?id democracy)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id lokawanwra))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  democracy.clp 	democracy0   "  ?id "    lokawanwra )" crlf)
)
)
