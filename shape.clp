
(defrule shape0
(declare (salience 5000))
(id-root ?id shape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Akqwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shape.clp 	shape0   "  ?id "  Akqwi )" crlf))
)

; Changed from bAhyAkqwi to Akqwi -- Amba
;"shape","N","1.bAhyAkqwi"
;--"2.AkAra"
;The new building  is in S shape.
;--"3.CAyA"
;I could just see two dim shapes in the gloom.
;--"4.sWiwi"
;What shape is the team in after its defeat?
;
(defrule shape1
(declare (salience 4900))
(id-root ?id shape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkAra_le))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shape.clp 	shape1   "  ?id "  AkAra_le )" crlf))
)

;"shape","V","1.AkAra_lenA"
;He shaped his career successfully.
;

;@@@ Added by 14anu21 and 14anu18 on 19.06.2014
;Will new technology change the shape of broadcasting?
;क्या नयी प्रौद्योगिकी प्रसारण की आकृति बदलेगी? 
;क्या नयी प्रौद्योगिकी प्रसारण का रूप बदलेगी? 
(defrule shape2
(declare (salience 5000))
(id-root ?id shape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-object  ?id1 ?id)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id rUpa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shape.clp 	shape2  "  ?id "  rUpa )" crlf))
)


;$$$ Modified by 14anu-ban-01 on (30-12-2014)
;@@@ Added by 14anu21 and 14anu17 on 19.06.2014
;This tool is used for shaping wood.
;यह औजार लकडी को आकार देने के लिए प्रयोग किया जाता है . 
;Their stampede shaped the ridges and valleys upon the mountain sides .
;उनकी भगदड ने पर्वत तरफ के ऊपर ढालू टीले और घाटियाँ आकार दिए . 
;उनकी भागदौड़ ने पर्वत के ऊपर की ओर के ढालू टीलों और घाटियों को आकार दिया.[translation improved by 14anu-ban-01 on (30-12-2014)]
(defrule shape3
(declare (salience 4900))
(id-root ?id shape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
(kriyA-object  ?id ?idobj)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkAra_xe))
(assert (kriyA_id-object_viBakwi ?id ko));added by 14anu-ban-01 on (30-12-2014) 
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   " ?*prov_dir* "  shape.clp 	shape3    "  ?id " ko )" crlf);added by 14anu-ban-01 on (30-12-2014)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shape.clp 	shape3   "  ?id "  AkAra_xe )" crlf))
)

;@@@ Added by 14anu-ban-11 on (30-03-2015)
;What shape is the team in after its defeat?(hinkhoj)
;उनकी हार के बाद  दल मे क्या स्थिति है? (self)
(defrule shape5
(declare (salience 5001))
(id-root ?id shape)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-viSeRaNa  ?id1 ?id)
(kriyA-subject  ?id1 ?id2)
(id-root ?id1 be)
(id-root ?id2 team)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id sWiwi))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  shape.clp 	shape5  "  ?id "  sWiwi)" crlf))
)
