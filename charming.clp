;@@@ Added by 14anu-ban-03 (31-01-2015)
;A charming personality. [shabdkosh]
;एक आकर्षक व्यक्तित्व . [anusaaraka]
(defrule charming1
(declare (salience 100))
(id-root ?id charming)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-viSeRaNa ?id1 ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id AkarRaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  charming.clp     charming1   "  ?id "  AkarRaka )" crlf))
)

;@@@ Added by 14anu-ban-03 (11-02-2015)
;Her daughter is endowed with charming manners. [hinkhoj]
;उसकी बेटी मनमोहक अंदाज से संपन्न है  . [self]  ;note- meaning of manner is not sure, so translation need to be modified later..
(defrule charming2
(declare (salience 200))
(id-root ?id charming)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
(viSeRya-viSeRaNa ?id1 ?id)
(id-root ?id1 manner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id manamohaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  charming.clp     charming2   "  ?id "  manamohaka )" crlf))
)

;------------------- Default Rules ----------------

;@@@ Added by 14anu-ban-03 (31-01-2015)
;The climate here is fascinating and charming . [tourism]
;yahAz kI jalavAyu manamohaka evaM suhAvanI hE .  [manual]
(defrule charming0
(declare (salience 00))
(id-root ?id charming)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id suhAvanI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  charming.clp     charming0   "  ?id "  suhAvanI )" crlf))
)



