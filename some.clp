

;Added by Meena(10.5.10)
;The box contained many books , some of which were badly damaged .
(defrule some_of_which
(declare (salience 4900))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(id-word  =(+ ?id 1) of)
(id-word  =(+ ?id 2) which)
(id-cat_coarse  =(- ?id 1) noun)
(saMjFA-saMjFA_samAnAXikaraNa  =(- ?id 1) ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2)  jinameM_se_kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " some.clp some_of_which  "  ?id "  " (+ ?id 1) " " (+ ?id 2) "  jinameM_se_kuCa  )" crlf))
)





;Added by Meena(10.5.10)
;The box contained many books , some of them were badly damaged .
(defrule some_of_them
(declare (salience 4900))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(id-word  =(+ ?id 1) of)
(id-word  =(+ ?id 2) them)
(id-cat_coarse  =(- ?id 1) noun)
(saMjFA-saMjFA_samAnAXikaraNa  =(- ?id 1) ?id)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id =(+ ?id 1) =(+ ?id 2)  unameM_se_kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " some.clp some_of_them  "  ?id "  " (+ ?id 1) " " (+ ?id 2) "  unameM_se_kuCa  )" crlf))
)







;viSeRya=officer && category=adjective	koI	100
;Evidence??
;Commented by 14anu-ban-01 on (23-09-14) as this rule is firing only because of higher salience than 'some1'.At salience=5000 some1 is always firing and this rule isn't firing.
;###Have some more vegetables.[oald] Here,vegetables are countable.
;थोडी और सब्ज़ियाँ लो.
;Here are some news that you might be intrested in.
;(defrule some0
;(declare (salience 5000))
;(id-root ?id some)
;?mng <-(meaning_to_be_decided ?id)
;(id-word =(+ ?id 1) ?str)
;;Test for ?str is made as argument for the gdbm_lookup_p function passed should be always a string                           (if number passed as argument gdbm_lookup_p causes to Segmentation fault)
;(id-word =(+ ?id 1) ?str&:(and (not (numberp ?str))(gdbm_lookup_p "uncountable.gdbm" ?str)))
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id WodA))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp 	some0   "  ?id "  WodA )" crlf))
;)
;Can I offer you some wine?

;Added by Meena(19.11.09)
;It had only a tenth of the sun's mass but showed some wobbling which could be due to planets in its orbit .
;(defrule some1
;(declare (salience 5000));salience increased from 4900 by 14anu-ban-01 on (23-09-14)
;(id-root ?id some)
;?mng <-(meaning_to_be_decided ?id)
;(viSeRya-det_viSeRaNa  ?id1 ?id)
;=>
;(retract ?mng)
;(assert (id-wsd_root_mng ?id kuCa))
;(if ?*debug_flag* then
;(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some1   "  ?id "  kuCa )" crlf))
;)



(defrule some2
(declare (salience 0))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp 	some2   "  ?id "  kuCa )" crlf))
)

;Would you like some biscuits?

;$$$ Modified by 14anu-ban-01 on (23-09-14). -- added English sentence with Translation 
;These chocolates are really tasty,would you like to have some?
;ये चॉकलेट वास्तव में स्वादिष्ट हैं, आप कुछ लेना पसन्द करेंगे? 
;Salience reduced by Meena(19.11.09)
(defrule some3
;(declare (salience 4800))
(declare (salience 0))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id determiner)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp 	some3   "  ?id "  kuCa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-09-14)
;There's still some wine in the bottle.[oald]
;बोतल में अभी भी थोडी वाइन है.
;Have some more vegetables.[oald]
;थोडी और सब्ज़ियाँ लो.
;Would you like some milk in your coffee?[oald]
;क्या आप अपनी कॉफी में थोड़ा दूध लेना पसन्द करेंगे?
(defrule some4
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-word ?id1 milk|wine|vegetables|powder);
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some4   "  ?id "  WodA )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-09-14)
;I like some modern music.[oald]
;मैं कुछ ही आधुनिक संगीत पसन्द करता हूँ.[self]
(defrule some5
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-root ?id1 music);
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kuCa_hI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some5   "  ?id "  kuCa_hI )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-09-14)
;There is some hope that things will improve.[oald]
;थोडी बहुत आशा है कि चीज़ें सुधर/बदल जायेंगी.[self]
(defrule some6
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-root ?id1 hope);
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id WodI_bahuwa))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some6   "  ?id "  WodI_bahuwa )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-09-2014)
;She won a competition in some newspaper.[oald]
;उसने किसी अखबार में एक स्पर्धा जीती थी.[self]
;Here it does not matter if some number multiplies the right side of this formula, because that does not affect its dimensions. [NCERT corpus]	;added by 14anu-ban-01 on (10-02-2015)
;यहाँ इसका कोई अर्थ नहीं है कि सूत्र के दक्षिण पक्ष को किसी सङ्ख्या से गुणा किया गया है, क्योंकि ऐसा करने से विमाएँ प्रभावित नहीं होतीं	;added by 14anu-ban-01 on (11-02-2015)
(defrule some7
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-word ?id1 newspaper|number)		;added 'number' to the list by 14anu-ban-01 on (11-02-2015)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some7   "  ?id "  kisI )" crlf))
)


;@@@ Added by 14anu-ban-01 on (23-09-14)
;I'll see you again some time, I'm sure.[oald]
; मैं आपसे फिर कभी मिलूँगा.[self]
(defrule some8
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(id-root =(- ?id 1) again)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-word ?id1 time)
=>
(retract ?mng)
(assert (affecting_id-affected_ids-wsd_group_root_mng ?id  =(- ?id 1) =(+ ?id 1) Pira_kaBI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-affecting_id-affected_ids-wsd_group_root_mng   " ?*prov_dir* " some.clp	 some8  "  ?id "  " =(- ?id 1)"  " =(+ ?id 1) "  Pira_kaBI )" crlf))
)

;@@@ Added by 14anu-ban-01 on (23-09-14)
;He's in some kind of trouble.[oald]
;वह किसी न किसी तरह की परेशानी में है [self]
(defrule some9
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-word ?id1 kind)
(viSeRya-of_saMbanXI ?id1 ?)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kisI_na_kisI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some9   "  ?id "  kisI_na_kisI)" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-09-14)
;We've known each other for some years now.[oald];meaning given with reference to oald
;हम एक दूसरे को कई सालों से जानते हैं[self]
(defrule some10
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-word ?id1 years)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kaI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some10   "  ?id "  kaI)" crlf))
)

;@@@ Added by 14anu-ban-01 on (24-09-14)
;It was with some surprise that I heard the news.[oald]
;मुझे यह समाचार सुनकर बडा/काफी आश्चर्य हुआ.[self]
(defrule some11
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-word ?id1 surprise)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id badA/kAPI))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some11   "  ?id "  badA/kAPI)" crlf))
)

;@@@ Added by 14anu-ban-01 on (21-01-2016)
;A section at the back of the warehouse was partitioned off to provide some privacy. [oald]
;गोदाम के पीछे वाले भाग को  एकान्तता के लिए अलग कर दिया गया था .[self]
(defrule some12
(declare (salience 5000))
(id-root ?id some)
?mng <-(meaning_to_be_decided ?id)
(viSeRya-det_viSeRaNa  ?id1 ?id)
(id-word ?id1 privacy|peace)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id -))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  some.clp      some12   "  ?id "  -)" crlf))
)


;"some","Det","1.kuCa"
;Please take some milk.
;
;
