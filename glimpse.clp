;@@@ Added by 14anu-ban-05 on (21-01-2015)
;In this procession of Mysore besides different glimpses the decorated elephants are center of special attraction .[tourism]
;mEsUra ke isa julUsa meM Binna - Binna JAzkiyoM ke alAvA saje hAWI viSeRa AkarRaNa kA keMxra howe hEM .[tourism]
(defrule glimpse2
(declare (salience 5001))
(id-root ?id glimpse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(viSeRya-of_saMbanXI  ?id1 ?)
(viSeRya-besides_saMbanXI  ? ?id)
(id-root ?id1 procession)	;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JAzki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glimpse.clp 	glimpse2   "  ?id "  JAzki )" crlf))
)

;@@@ Added by 14anu-ban-05 on (21-01-2015)
;Glimpse of this Bangalore can be seen in this museum .[tourism]
;isa saMgrahAlaya meM baMgalOra kI JAzkI xeKI jA sakawI hE .[tourism]
(defrule glimpse3
(declare (salience 5001))
(id-root ?id glimpse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
(kriyA-subject  ?id2 ?id)
(kriyA-in_saMbanXI  ?id2 ?id1)
(id-root ?id1 museum)		;more constraints can be added
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id JAzki))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glimpse.clp 	glimpse3   "  ?id "  JAzki )" crlf))
)

;---------------------- Default Rules ----------------

(defrule glimpse0
(declare (salience 5000))
(id-root ?id glimpse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id noun)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id Jalaka))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glimpse.clp 	glimpse0   "  ?id "  Jalaka )" crlf))
)

;"glimpse","N","1.Jalaka"
;The crowd had a glimpse of their favourite star.
;
(defrule glimpse1
(declare (salience 4900))
(id-root ?id glimpse)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id jalxI_se_eka_najZara_dZAla))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  glimpse.clp 	glimpse1   "  ?id "  jalxI_se_eka_najZara_dZAla )" crlf))
)

;"glimpse","V","1.jalxI_se_eka_najZara_dZAlanA"
;I glimpsed outside from the window. 
;
