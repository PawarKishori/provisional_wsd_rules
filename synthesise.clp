;@@@ Added by 14anu-ban-01 on (07-02-2015)
;The liver synthesises vitamins.[self]
;लीवर विटामिन का संश्लेषण करता है[self]
(defrule synthesise0
(declare (salience 0))
(id-root ?id synthesise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id saMSleRaNa_kara))
(assert (kriyA_id-object_viBakwi ?id kA))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-kriyA_id-object_viBakwi   "?*prov_dir* "  synthesise.clp       synthesise0   "  ?id " kA )" crlf)
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  synthesise.clp  	synthesise0   "  ?id "  saMSleRaNa_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (07-02-2015)
;The drug was synthesised very carefully.[self:wrt collins dictionary]
;यह दवा अत्यन्त सावधानी से उत्पादित की गयी थी.[self]
(defrule synthesise1
(declare (salience 1000))
(id-root ?id synthesise)
?mng <-(meaning_to_be_decided ?id)
(id-root ?id1 drug|medicine|product)	;list can be added
(kriyA-subject ?id ?id1)
(id-cat_coarse ?id verb)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id uwpAxiwa_kara))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng  " ?*prov_dir* "  synthesise.clp  	synthesise1   "  ?id "  uwpAxiwa_kara )" crlf))
)

;@@@ Added by 14anu-ban-01 on (04-12-2014)
;If the vehicle is going too fast, a synthesised voice tells the driver to slow down.[collins dictionary]
;यदि वाहन बहुत तेजी से जा रहा है, तो एक कृत्रिम आवाज चालक को गति धीमी करने का निर्देश देती है.[self]
(defrule synthesise2
(declare (salience 0))
(id-root ?id synthesise)
?mng <-(meaning_to_be_decided ?id)
(id-cat_coarse ?id adjective)
=>
(retract ?mng)
(assert (id-wsd_root_mng ?id kqwrima))
(if ?*debug_flag* then
(printout wsd_fp "(dir_name-file_name-rule_name-id-wsd_root_mng   " ?*prov_dir* "  synthesise.clp 	synthesise2   "  ?id "  kqwrima)" crlf))
)
